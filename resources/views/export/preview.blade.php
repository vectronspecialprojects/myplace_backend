<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
    .cell {
        border: thick solid #000000;
    }

    .cell-top {
        border-top: thick solid #000000;
    }

    .cell-bottom {
        border-bottom: thick solid #000000;
    }

    .cell-right {
        border-right: thick solid #000000;
    }

    .yellow {
        background-color: #ffff00;
    }

    .gray {
        background-color: #808080;
    }

    .light-gray {
        background-color: #a9a9a9
    }

    .orange {
        background-color: #ff7f50;
    }

    .leaf-green {
        background-color: #7cfc00;
    }

    .center {
        vertical-align: middle;
    }

    .bold {
        font-weight: bold;
    }

    .wrapped {
        wrap-text: true;
    }
</style>

<tr></tr>

<tr class="bold">
    <td class="center" height="30" colspan="8" align="center"> App Purchases </td>
</tr>
@if(!empty($orders))

    <?php
    $total_order = 0;
    ?>

@foreach($orders as $order)

<tr class="bold">
    <td height="30" colspan="8" align="left">{{$order['product_name']}}</td>
</tr>

<tr class="bold">
    <td class="cell gray center" height="20" width="20" align="center">QTY</td>
    <td class="cell gray center" height="20" width="20" align="center">PRICE</td>
    <td class="cell gray center" height="20" width="25" align="center">FIRST NAME</td>
    <td class="cell gray center" height="20" width="25" align="center">LAST NAME</td>
    <td class="cell gray center" height="20" width="20" align="center">MOBILE</td>
    <td class="cell gray center" height="20" width="20" align="center">EMAIL</td>
    <td class="cell gray center" height="20" width="20" align="center">PURCHASED AT</td>
    <td class="cell gray center" height="20" width="20" align="center">SUBTOTAL</td>
</tr>

@foreach ($order["data"] as $row)


<tr>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['order_detail']['qty']}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ toMoneyConverter($row['order_detail']['unit_price'] )}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['first_name']}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['last_name']}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['user']['mobile']}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['user']['email']}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ date("jS F, Y H:i", strtotime($row['order_detail']['created_at'])) }}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ toMoneyConverter($row['order_detail']['subtotal_unit_price'])}}</td>
</tr>

    <?php
        $total_order += $row['order_detail']['subtotal_unit_price'];
    ?>

@endforeach

@endforeach

<tr>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">TOTAL</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ toMoneyConverter($total_order)}}</td>
</tr>

@endif

@if(!empty($gifts))

<tr></tr>

<tr class="bold">
    <td class="center" height="30" colspan="8" align="center"> Gift Certificates </td>
</tr>

<?php
$total_gifts = 0;
?>

@foreach($gifts as $order)

    <tr class="bold">
        <td height="30" colspan="8" align="left">{{$order['product_name']}}</td>
    </tr>

    <tr class="bold">
        <td class="cell gray center" height="20" width="20" align="center">QTY</td>
        <td class="cell gray center" height="20" width="20" align="center">PRICE</td>
        <td class="cell gray center" height="20" width="25" align="center">FIRST NAME</td>
        <td class="cell gray center" height="20" width="25" align="center">LAST NAME</td>
        <td class="cell gray center" height="20" width="20" align="center">MOBILE</td>
        <td class="cell gray center" height="20" width="20" align="center">EMAIL</td>
        <td class="cell gray center" height="20" width="20" align="center">PURCHASED AT</td>
        <td class="cell gray center" height="20" width="20" align="center">SUBTOTAL</td>
    </tr>

    @foreach ($order["data"] as $row)


        <tr>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['order_detail']['qty']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ toMoneyConverter($row['order_detail']['unit_price'] )}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['first_name']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['last_name']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['user']['mobile']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row['member']['user']['email']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ date("jS F, Y H:i", strtotime($row['order_detail']['created_at'])) }}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ toMoneyConverter($row['order_detail']['subtotal_unit_price'])}}</td>

        </tr>

        <?php
            $total_gifts += $row['order_detail']['subtotal_unit_price'];
        ?>

    @endforeach

@endforeach

<tr>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">TOTAL</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center"></td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ toMoneyConverter($total_gifts)}}</td>
</tr>


@endif
</html>