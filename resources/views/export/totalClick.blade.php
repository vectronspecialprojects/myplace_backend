<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css" >
    .cell {
        border: thick solid #000000;
    }

    .cell-top {
        border-top: thick solid #000000;
    }

    .cell-bottom {
        border-bottom: thick solid #000000;
    }

    .cell-right {
        border-right: thick solid #000000;
    }

    .yellow {
        background-color: #ffff00;
    }

    .gray {
        background-color: #808080;
    }

    .light-gray {
        background-color: #a9a9a9
    }

    .orange {
        background-color: #ff7f50;
    }

    .leaf-green {
        background-color: #7cfc00;
    }

    .center {
        vertical-align: middle;
    }

    .bold {
        font-weight: bold;
    }

    .wrapped {
        wrap-text: true;
    }
</style>

<table class="table table-striped">
<tbody>
<tr></tr>

<tr class="bold" >
    <td class="center" height="30" colspan="3" style="text-align: center; font-weight: bold;"> 
        Total Clicks Report<br>
    </td>
    <td class="center" height="30" style="text-align: center; font-weight: bold;">
        Display: <br>
    </td>
    <td class="center" height="30" style="text-align: center; font-weight: bold;">
        All Time <br>
    </td>
</tr>


@if(!empty($values))

    <?php
    $total_order = 0;
    ?>


    <tr class="bold">
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">ID</td>
        <td class="cell gray center" height="20" width="50" align="center" style="background-color: #808080;">NAME</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">TYPE</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">STATUS</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">VALUE</td>
    </tr>

    @foreach($values as $value)

        <tr>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['id']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="50" align="center">{{$value['name']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['type']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['status']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['value']}}</td>
        </tr>

    @endforeach


@endif
</tbody>
</table>

</html>