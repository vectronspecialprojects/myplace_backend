<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            /*font-weight: 100;*/
            font-family: 'Lato';
            background-color: black;
        }

        .container {
          height: 100vh;
        }

        .content {
          margin-top: 10%;
        }

        .title {
            font-size: 96px;
            white-space: nowrap;
        }

        .control-label{
          color: #eee;
          margin-bottom: 0;
        }

        .white-label{
            color: white;
        }

        .button {
            width: 100%;
            height: 30px;
            color: white!important;
            background-color: black;
        }

        .button:disabled {
            background-color: #666;
        }

        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            color: #000000;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            color: #000000;
        }
        :-ms-input-placeholder { /* IE 10+ */
            color: #000000;
        }
        :-moz-placeholder { /* Firefox 18- */
            color: #000000;
        }

        .help-block {
            color: white !important;
        }

        input {
            color: #000000 !important;
            font-size:17px !important;
            font-weight: bold !important;
        }

        select {
            color: #000000 !important;
            font-size:17px !important;
            font-weight: bold !important;
        }

        option {
            color: #000000 !important;
            font-size:17px !important;
            font-weight: bold !important;
        }

        .round-input{
            border-radius: 20px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content row">
      <div class="col-sx-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
          <img id="logo" src="{{$company_logo}}" class="img-responsive" style="margin: 0 auto; max-width:100%;" alt="Logo">
        <h1 class="text-center" style="color: white; white-space: nowrap;">REGISTER</h1>

          <form action="" method="post" id="myForm" autocomplete="off">
                <input type="hidden" id="app_token" name="app_token" value="{{$app_token}}">
                <input type="hidden" id="app_secret" name="app_secret" value="{{$app_secret}}">
                <input type="hidden" id="token" name="token" value="{{$token}}">
                
                @php
                $tier_hidden = 1;
                $state_hidden = 1;

                
                if ($member_default_tier_option == 'by_venue')
                

                if ($member_default_tier_option == 'by_venue_tag')
                $state_hidden = 1;
                else
                $state_hidden = 0;

                @endphp
                
                @foreach($bepozcustomfield as $key => $bcf)
                @if ($bcf['displayInApp'] == true)
                    @if ($bcf['fieldType'] == 'Text')                            
                        @if ($bcf['id'] == 'email')
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="email" class="control-label">Email</label>
                                @endif
                                <input type="email" name="email" id="email" placeholder="Email" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" readonly required value="{{$email}}">
                                <strong><span id="helpEmail" class="help-block"></span></strong>
                            </div>
                        @else
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="{{$bcf['id']}}" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <input type="text" name="" id="{{$bcf['id']}}" placeholder="{{$bcf['fieldDisplayTitle']}}" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                <strong><span id="helpFirstname" class="help-block"></span></strong>
                            </div>
                        @endif
                    @elseif ($bcf['fieldType'] == 'Dropdown')
                        @if ($bcf['id'] == 'state')
                            @php
                            $state_hidden = 0
                            @endphp
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="{{$bcf['id']}}" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <select name="state" id="state" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                    @foreach($bcf['multiValues'] as $key => $value)
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <strong><span id="helpState" class="help-block"></span></strong>
                            </div>
                        @elseif ($bcf['id'] == 'tier')
                            @php
                            $tier_hidden = 0
                            @endphp
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="tier_id" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <select name="tier_id" id="tier_id" placeholder="{{$bcf['fieldDisplayTitle']}}" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                    @foreach($tiers as $key => $value)
                                        <option value="{{$value['id']}}">{{$value["name"]}}</option>
                                    @endforeach
                                </select>
                                <strong><span id="helpTier" class="help-block"></span></strong>
                            </div>
                        @else
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="{{$bcf['id']}}" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <select name="{{$bcf['id']}}" id="{{$bcf['id']}}" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                    @foreach($bcf['multiValues'] as $key => $value)
                                        <option value="{{$value}}" @if ($key === 1) selected @endif>{{$value}}</option>
                                    @endforeach
                                </select>
                                <strong><span id="helpOther" class="help-block"></span></strong>
                            </div>
                        @endif
                    @elseif ($bcf['fieldType'] == 'Number')
                        @if ($bcf['id'] == 'post_code')
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="{{$bcf['id']}}" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <input type="number" name="{{$bcf['id']}}" id="{{$bcf['id']}}" placeholder="{{$bcf['fieldDisplayTitle']}}" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required  onKeyPress="if(this.value.length==4) return false;">
                                <strong><span id="helpPostcode" class="help-block"></span></strong>
                            </div>
                        @elseif ($bcf['id'] == 'mobile')
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="{{$bcf['id']}}" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <input type="number" name="{{$bcf['id']}}" id="{{$bcf['id']}}" placeholder="{{$bcf['fieldDisplayTitle']}}" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required maxlength="10" onKeyPress="if(this.value.length==10) return false;">
                                <strong><span id="helpMobile" class="help-block"></span></strong>
                            </div>
                        @else
                            
                        @endif
                    @elseif ($bcf['fieldType'] == 'Date')
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="{{$bcf['id']}}" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <input type="date" name="{{$bcf['id']}}" id="{{$bcf['id']}}" placeholder="{{$bcf['fieldDisplayTitle']}}" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                <strong><span id="helpDOB" class="help-block"></span></strong>
                            </div>
                    @elseif ($bcf['fieldType'] == 'Venue')
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="{{$bcf['id']}}" class="control-label">{{$bcf['fieldDisplayTitle']}}</label>
                                @endif
                                <select name="venue_id" id="venue_id" placeholder="{{$bcf['fieldDisplayTitle']}}" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                    @foreach($venues as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                                <strong><span id="helpVenue" class="help-block"></span></strong>
                            </div>
                    @elseif ($bcf['fieldType'] == 'Password')
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="password" class="control-label">New Password</label>
                                @endif
                                <input type="password" name="password" id="password" placeholder="Password" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                <strong><span id="helpPassword" class="help-block"></span></strong>
                            </div>
                            <div class="form-group">
                                @if ($form_floating_title == 'false')
                                <label for="retry_password" class="control-label">Confirm Password</label>
                                @endif
                                <input type="password" name="retry_password" id="retry_password" placeholder="Confirm Password" class="form-control @if($form_input_style == 'rounded_rectangle') round-input @endif" required>
                                <strong><span id="helpRetry" class="help-block"></span></strong>
                            </div>
                    @endif
                @endif 
                @endforeach

                @if ($tier_hidden)
                    <input type="hidden" id="tier_id" name="tier_id" value="">                
                @endif

                @if ($state_hidden)
                    <input type="hidden" id="state" name="state" value="">                
                @endif

                <div class="checkbox">
                    <label class="white-label">
                        <input id="agreed" name="agreed" type="checkbox" required> By creating an account, you agree to {{$company_name}} <a href="{{$terms}}" target="_blank" class="white-label">Terms and Conditions</a> and <a href="{{$policy}}" target="_blank" class="white-label">Privacy Policy</a>
                    </label>
                </div>

                <input type="submit" id="sub-btn" class="button  @if($form_input_style == 'rounded_rectangle') round-input @endif" value="SIGN UP" disabled>
                
                <br><br><br><br><br><br><br>
          </form>

      </div>
    </div>
</div>

<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
  <script type="text/javascript">
    (function(){
        var form = $('#myForm');
        var venue_id = $('#venue_id');
        // var venue_select = $('#venue_select');
        var first_name = $('#first_name');
        var last_name = $('#last_name');
        var email = $('#email');
        var dob = $('#dob');
        var mobile = $('#mobile');
        var postcode = $('#post_code');
        var state = $('#state');
        var tier_id = $('#tier_id');
        var pwInput = $('#password');
        var rePwInput = $('#retry_password');
        var btn = $('#sub-btn');
        var agreed = $('#agreed');
       
        var post_code_needed = false;
        var bepozcustomfield = @json($bepozcustomfield);
        var bcfLength = bepozcustomfield.length;
        for (var i = 0; i < bcfLength; i++) {
            // console.log(bepozcustomfield[i]);
            if (bepozcustomfield[i]["id"] == "post_code") {                
                console.log("detect post_code");
                if (bepozcustomfield[i]["displayInApp"]) {
                    post_code_needed = true;
                }
            }
            
            if (bepozcustomfield[i]["id"] == "state") {                
                console.log("detect state");
            }
            
            // if postcode exist but state not exist, no need postcode autocomplete
            // need check tier selection by venue, venue state/tag, or default tier
        }

        // var tiers = @json($tiers);
        // console.log(tiers);

        
        var member_default_tier_option = @json($member_default_tier_option);
        var member_default_tier_detail = @json($member_default_tier_detail);
        var default_tier = @json($default_tier);


        tier_id.prop('disabled', false);

        if (member_default_tier_option == "no_default") {

        }
        else if (member_default_tier_option == "specific_tier") {
            // console.log("specific_tier");
            tier_id.prop('disabled', true);
            tier_id.val(default_tier);            
        }
        
        var labelvenue = $("#helpVenue");
        var labelfirstname = $("#helpFirstname");
        var labellastname = $("#helpLastname");
        var labeldob = $("#helpDOB");
        var labelmobile = $("#helpMobile");
        var labelpass = $("#helpPassword");
        var labelretry = $("#helpRetry");
        var labelemail = $("#helpEmail");
        var labelpostcode = $("#helpPostcode");
        var labelstate = $("#helpState");
        var labeltier = $("#helpTier");

        function deepValidation() {
            var pass = true;
            
            labelvenue.text("");
            labeldob.text("");
            labelmobile.text("");
            labelpass.text("");
            labelretry.text("");
            labelemail.text("");
            labelfirstname.text("");
            labellastname.text("");
            labelpostcode.text("");
            labelstate.text("");
            labeltier.text("");

            //   console.log(venue_id.val());
            //   console.log(venue_select.val());
            // console.log("venue val");
            

            if (venue_id.val()) {
                
                console.log("venue val");
                // console.log(member_default_tier_option);

                if (member_default_tier_option == "by_venue") {
                    var defaultTierLength = member_default_tier_detail.length;
                    for (var i = 0; i < defaultTierLength; i++) {
                        // console.log(member_default_tier_detail[i]);
                        
                        if  ( venue_id.val() == member_default_tier_detail[i].id ) {
                            console.log(member_default_tier_detail[i].tier);
                            tier_id.val(member_default_tier_detail[i].tier);                        
                            // labelvenue.text(member_default_tier_detail[i].tier);
                        }
                        
                        // if postcode exist but state not exist, no need postcode autocomplete
                        // need check tier selection by venue, venue state/tag, or default tier
                    }
                }

            }
            else {
                pass = false;
                console.log("not pass Venue");
                labelvenue.text("Venue is required");
            }

            if (!first_name.val()) {
                pass = false;
                console.log("not pass First name");
                labelfirstname.text("First name is required");
            }

            if (!last_name.val()) {
                pass = false;
                console.log("not pass Last name");
                labellastname.text("Last name is required");
            }

            if (dob.val()) {
                var now = moment();
                var dobs = moment(dob.val());
                if(now.diff(dobs, 'years', true) < 18) {
                    pass = false;
                    console.log("not pass Date Of Birth");
                    labeldob.text("Please enter your date of birth and you must be over 18 years old.");
                }
            }
            else {
                pass = false;
                console.log("not pass Date Of Birth");
                labeldob.text("Date Of Birth is required");
            }

            if (mobile.val()) {
                var phonePattern = /^0[0-8]\d{8}$/g;
                //phone number is not valid. Please notice that you don't need to check if it's empty or null since Regex checks it for you anyways
                if (!phonePattern.test(mobile.val())) {
                    pass = false;
                    labelmobile.text("Please enter a valid mobile number e.g 0400000000");
                }
            }
            else {
                pass = false;
                console.log("not pass Mobile");
                labelmobile.text("Mobile number is required");
            }


            if (post_code_needed) {
                if (postcode.val()) {
                    var postcodePattern = /^\d{4}$/g;
                    //Post code is not valid. Please notice that you don't need to check if it's empty or null since Regex checks it for you anyways
                    if (!postcodePattern.test(postcode.val())) {
                        pass = false;
                        labelpostcode.text("Please enter a valid Post code e.g 1111");
                    } else {
                        var stateSelected = getStateFromPostCode(postcode.val());
                        state.val(stateSelected);
                        // labelpostcode.text("Post code ");
                    }
                }
                else {
                    pass = false;
                    console.log("not pass Post code");
                    labelpostcode.text("Post code is required");
                }
            }
            

            if (member_default_tier_option == "by_venue_tag") {
                if (state.val()) {
                    labelstate.text("State");
                    // console.log(member_default_tier_option);
                    // console.log("state");
                        
                        var defaultTierLength = member_default_tier_detail.length;
                        for (var i = 0; i < defaultTierLength; i++) {
                            // console.log(member_default_tier_detail[i]);
                            
                            if  ( state.val() == member_default_tier_detail[i].name ) {
                                // console.log(member_default_tier_detail[i]);
                                tier_id.val(member_default_tier_detail[i].tier);
                            }
                            
                            // if postcode exist but state not exist, no need postcode autocomplete
                            // need check tier selection by venue, venue state/tag, or default tier
                        }
                    
                }
                else {
                    pass = false;
                    console.log("not pass State");
                    labelstate.text("State is required");
                }
            }


            if (pwInput.val()) {
                var strongRegex = new RegExp("^(?=.*\\d)(?=.*[a-z]).{6,}$");
                
                if(!(strongRegex.test(pwInput.val()))) {
                    pass = false;
                    labelpass.text("Password must have at least 6 characters with at least 1 letter and 1 number");
                }

            }
            else {
                pass = false;
                console.log("not pass pwInput");
                labelpass.text("Password is required");
            }

            if (rePwInput.val() && (pwInput.val() !== rePwInput.val())) {
                pass = false;
                console.log("not pass rePwInput");
                labelretry.text("Passwords do not match");
            }

            if (!agreed.is(':checked')) {
                pass = false;
                console.log("not pass agreed");
            }

            // if (email.val()) {
            //     var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            //     if(!re.test(email.val())){
            //         pass = false;
            //         labelemail.text("Please enter a valid email");
            //     }
            // }

            console.log("tier_id " + tier_id.val());
            return pass;
        }

        function getStateFromPostCode(postCode) {
            switch (postCode) {
                case 4825:
                return 'NT'
                case 872:
                return 'SA'
                case 2611:
                return 'NSW'
                case 2620:
                return 'ACT'
                case 3585:
                case 3586:
                case 3644:
                case 3691:
                case 3707:
                case 4380:
                case 4377:
                case 4383:
                case 4385:
                return 'NSW'
                default:
                if (
                    (postCode >= 1000 && postCode <= 2599) ||
                    (postCode >= 2619 && postCode <= 2899) ||
                    (postCode >= 2921 && postCode <= 2999)
                ) {
                    return 'NSW'
                } else if (
                    (postCode >= 200 && postCode <= 299) ||
                    (postCode >= 2600 && postCode <= 2618) ||
                    (postCode >= 2900 && postCode <= 2920)
                ) {
                    return 'ACT'
                } else if ((postCode >= 3000 && postCode <= 3999) || (postCode >= 8000 && postCode <= 8999)) {
                    return 'VIC'
                } else if ((postCode >= 4000 && postCode <= 4999) || (postCode >= 9000 && postCode <= 9999)) {
                    return 'QLD'
                } else if ((postCode >= 5000 && postCode <= 5799) || (postCode >= 5800 && postCode <= 5999)) {
                    return 'SA'
                } else if ((postCode >= 6000 && postCode <= 6797) || (postCode >= 6800 && postCode <= 6999)) {
                    return 'WA'
                } else if ((postCode >= 7000 && postCode <= 7799) || (postCode >= 7800 && postCode <= 7999)) {
                    return 'TAS'
                } else if ((postCode >= 800 && postCode <= 899) || (postCode >= 900 && postCode <= 999)) {
                    return 'NT'
                } else {
                    return ''
                }
            }
        }

        // handle event for select change
        venue_id.change(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        first_name.keyup(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        last_name.keyup(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        mobile.keyup(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        email.keyup(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        dob.change(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        postcode.keyup(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        state.change(function(){
            // console.log("state change");
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        pwInput.keyup(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        rePwInput.keyup(function(e){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

        agreed.change(function(){
            if (deepValidation()) {
                btn.prop('disabled', false);
            }
            else {
                btn.prop('disabled', true);
            }
        });

  })();
  </script>
</body>
</html>
