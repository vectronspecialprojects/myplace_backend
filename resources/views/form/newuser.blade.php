<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            /*font-weight: 100;*/
            font-family: 'Lato';
            background-color: black;
        }

        .container {
          height: 100vh;
        }

        .content {
          margin-top: 10%;
        }

        .title {
            font-size: 96px;
            white-space: nowrap;
        }

        .control-label{
          color: #eee;
          margin-bottom: 0;
        }

        .white-label{
            color: white;
        }

        .button {
            width: 100%;
            height: 30px;
            color: white!important;
            background-color: black;
        }

        .button:disabled {
            background-color: #666;
        }

        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            color: #000000;
        }
        ::-moz-placeholder { /* Firefox 19+ */
            color: #000000;
        }
        :-ms-input-placeholder { /* IE 10+ */
            color: #000000;
        }
        :-moz-placeholder { /* Firefox 18- */
            color: #000000;
        }

        .help-block {
            color: white !important;
        }

        input {
            color: #000000 !important;
            font-size:17px !important;
            font-weight: bold !important;
        }

        select {
            color: #000000 !important;
            font-size:17px !important;
            font-weight: bold !important;
        }

        option {
            color: #000000 !important;
            font-size:17px !important;
            font-weight: bold !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content row">
      <div class="col-sx-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
          <img id="logo" src="{{$company_logo}}" class="img-responsive" style="margin: 0 auto; max-width:100%;" alt="Logo">
        <h1 class="text-center" style="color: white; white-space: nowrap;">REGISTER</h1>

          <form action="{{$url}}" method="post" id="myForm" autocomplete="off">
              <input type="hidden" id="app_token" name="app_token" value="{{$app_token}}">
              <input type="hidden" id="app_secret" name="app_secret" value="{{$app_secret}}">
              <input type="hidden" id="token" name="token" value="{{$token}}">
              <div class="form-group">
                  <label for="venue_id" class="control-label">Favourite Cowch Location</label>
                  <select name="venue_id" id="venue_id" class="form-control" required>
                      @foreach($venues as $key => $venue)
                          <option value="{{$key}}" @if ($key === 1) selected @endif>{{$venue}}</option>
                      @endforeach
                  </select>
                  <strong><span id="helpVenue" class="help-block"></span></strong>
              </div>


              <div class="form-group">
                  <label for="first_name" class="control-label">First Name</label>
                  <input type="text" name="first_name" id="first_name" class="form-control" required>
                  <strong><span id="helpFirstname" class="help-block"></span></strong>
              </div>

              <div class="form-group">
                  <label for="last_name" class="control-label">Last Name</label>
                  <input type="text" name="last_name" id="last_name" class="form-control" required>
                  <strong><span id="helpLastname" class="help-block"></span></strong>
              </div>

              <div class="form-group">
                  <label for="mobile" class="control-label">Mobile Phone</label>
                  <input type="text" name="mobile" id="mobile" class="form-control" required maxlength="10">
                  <strong><span id="helpMobile" class="help-block"></span></strong>
              </div>

              <div class="form-group">
                  <label for="email" class="control-label">Email</label>
                  <input type="email" name="email" id="email" class="form-control" readonly required value="{{$email}}">
                  <strong><span id="helpEmail" class="help-block"></span></strong>
              </div>

              <div class="form-group">
                  <label for="dob" class="control-label">Date of Birth</label>
                  <input type="date" name="dob" id="dob" class="form-control" required>
                  <strong><span id="helpDOB" class="help-block"></span></strong>
              </div>

              <div class="form-group">
                  <label for="postcode" class="control-label">Post Code</label>
                  <input type="text" name="postcode" id="postcode" class="form-control" required maxlength="4">
                  <strong><span id="helpPostcode" class="help-block"></span></strong>
              </div>

              <div class="form-group">
                  <label for="password" class="control-label">New Password</label>
                  <input type="password" name="password" id="password" class="form-control" required>
                  <strong><span id="helpPassword" class="help-block"></span></strong>
              </div>
              <div class="form-group">
                  <label for="retry_password" class="control-label">Confirm Password</label>
                  <input type="password" name="retry_password" id="retry_password" class="form-control" required>
                  <strong><span id="helpRetry" class="help-block"></span></strong>
              </div>

              <div class="checkbox">
                  <label class="white-label">
                      <input id="agreed" name="agreed" type="checkbox" required> By creating an account, you agree to {{$company_name}} <a href="{{$terms}}" target="_blank" class="white-label">Terms and Conditions</a> and <a href="{{$policy}}" target="_blank" class="white-label">Privacy Policy</a>
                  </label>
              </div>

              <input type="submit" id="sub-btn" class="button" value="SIGN UP" disabled>
          </form>

      </div>
    </div>
</div>

<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
  <script type="text/javascript">
  (function(){
    var form = $('#myForm');
    var venue_id = $('#venue_id');
    var first_name = $('#first_name');
    var last_name = $('#last_name');
    var email = $('#email');
    var dob = $('#dob');
    var mobile = $('#mobile');
    var postcode = $('#postcode');
    var pwInput = $('#password');
    var rePwInput = $('#retry_password');
    var btn = $('#sub-btn');
    var agreed = $('#agreed');

      var labelvenue = $("#helpVenue");
      var labelfirstname = $("#helpFirstname");
      var labellastname = $("#helpLastname");
      var labeldob = $("#helpDOB");
      var labelmobile = $("#helpMobile");
      var labelpass=$("#helpPassword");
      var labelretry = $("#helpRetry");
      var labelemail = $("#helpEmail");
      var labelpostcode = $("#helpPostcode");

      function deepValidation() {
          var pass = true;
          
          labelvenue.text("");
          labeldob.text("");
          labelmobile.text("");
          labelpass.text("");
          labelretry.text("");
          labelemail.text("");
          labelfirstname.text("");
          labellastname.text("");
          labelpostcode.text("");

          if (!venue_id.val()) {
              pass = false;
              labelvenue.text("Venue is required");
          }

          if (!first_name.val()) {
              pass = false;
              labelfirstname.text("First name is required");
          }

          if (!last_name.val()) {
              pass = false;
              labellastname.text("Last name is required");
          }

          if (dob.val()) {
              var now = moment();
              var dobs = moment(dob.val());
              if(now.diff(dobs, 'years', true) < 19) {
                  pass = false;
                  labeldob.text("Please enter your date of birth and you must be over 18 years old.");
              }
          }
          else {
            pass = false;
            labeldob.text("Date Of Birth is required");
          }

          if (mobile.val()) {
              var phonePattern = /^0[0-8]\d{8}$/g;
              //phone number is not valid. Please notice that you don't need to check if it's empty or null since Regex checks it for you anyways
              if (!phonePattern.test(mobile.val())) {
                  pass = false;
                  labelmobile.text("Please enter a valid mobile number e.g 0400000000");
              }
          }
          else {
            pass = false;
            labelmobile.text("Mobile number is required");
          }

          if (postcode.val()) {
              var postcodePattern = /^\d{4}$/g;
              //Post code is not valid. Please notice that you don't need to check if it's empty or null since Regex checks it for you anyways
              if (!postcodePattern.test(postcode.val())) {
                  pass = false;
                  labelpostcode.text("Please enter a valid Post code e.g 1111");
              }
          }
          else {
            pass = false;
            labelpostcode.text("Post code is required");
          }

          if (pwInput.val()) {
              var strongRegex = new RegExp("^(?=.*\\d)(?=.*[a-z]).{6,}$");
              
              if(!(strongRegex.test(pwInput.val()))) {
                  pass = false;
                  labelpass.text("Password must have at least 6 characters with at least 1 letter and 1 number");
              }

          }
          else {
            pass = false;
            labelpass.text("Password is required");
          }

          if (rePwInput.val() && (pwInput.val() !== rePwInput.val())) {
              pass = false;
              labelretry.text("Passwords do not match");
          }

          if (!agreed.is(':checked')) {
              pass = false;
          }

              // if (email.val()) {
          //     var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          //     if(!re.test(email.val())){
          //         pass = false;
          //         labelemail.text("Please enter a valid email");
          //     }
          // }

          return pass;
      }

    venue_id.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    first_name.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    last_name.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    mobile.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    email.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    dob.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    postcode.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    pwInput.keyup(function(){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

    rePwInput.keyup(function(e){
        if (deepValidation()) {
            btn.prop('disabled', false);
        }
        else {
            btn.prop('disabled', true);
        }
    });

      agreed.change(function(){
          if (deepValidation()) {
              btn.prop('disabled', false);
          }
          else {
              btn.prop('disabled', true);
          }
      });

  })();
  </script>
</body>
</html>
