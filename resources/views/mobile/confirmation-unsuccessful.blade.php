<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
            background-color: <?php echo $mobile_confirmation_unsuccessful_bg_color; ?>;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
            margin: 0 60px 0 60px;
        }

        .title {
            font-size: 24px;
            color: <?php echo $mobile_confirmation_unsuccessful_font_color; ?>;
        }

        .details {
            font-size: 64px;
            color: <?php echo $mobile_confirmation_unsuccessful_font_color; ?>;
        }

        .button {
            margin-top: 20px;
            width: 100%;
            height: 200px;
            color: <?php echo $mobile_confirmation_unsuccessful_button_font_color; ?>;
            background-color: <?php echo $mobile_confirmation_unsuccessful_button_bg_color; ?>;
            font-size: 64px;
            border-color: <?php echo $mobile_confirmation_unsuccessful_button_border_color; ?>;
        }
    </style>

    <script>
        function close_window() {
            var win = window.open("about:blank", "_self");
            win.close();
        }
    </script>
</head>
<body>
<div class="container">
    <div class="content">
        <img id="logo" src="{{$company_logo}}" class="img-responsive" style="margin: 0 auto; max-width:100%;" alt="Logo">
        <br><br><br><br><br><br><br><br><br>
        <div class="details"><?php echo $mobile_confirmation_unsuccessful_message; ?></div>
        <br><br><br><br><br>
        <div><button class="button" onclick="close_window();">CLOSE</button></div>
    </div>
</div>
</body>
</html>
