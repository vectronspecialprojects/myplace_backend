<style>

    @page {
        size: A4;
    }

    .invoice-box {
        /*max-width:800px;*/
        /*margin:auto;*/
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        /*line-height:inherit;*/
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom:20px;
    }

    .invoice-box table tr.top table td.title {
        font-size:45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom:40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom:20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
</style>

<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <h2><img id="logo" src="{{$company_logo}}" style="max-width: 300px; max-height: 100px" alt="Logo"></h2>
                            <span>{{$company_name}}</span><br/>
                            <span>{{$company_address1}}</span><br/>
                            <span>{{$company_address2}}</span><br/>
                            <span><strong>ABN:</strong> {{$company_abn}}</span>
                        </td>

                        <td>
                            <h2>Tax invoice</h2>
                            @if (isset($bepoz_transaction_ids))
                                <span>Trans. ID: {{ $bepoz_transaction_ids }}</span><br/>
                            @endif

                            <span>Ordered: {{ date('F d, Y', strtotime($order->ordered_at ))}}</span><br/>
                            <span><strong>To</strong> {{$member->first_name}} {{$member->last_name}} #:{{$member->bepoz_account_id}}</span><br/>
                            <span>{{$user->email}}</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>

                        </td>

                        <td>
                            {{--<span>Member ID #:{{$member->bepoz_account_id}}</span><br/>--}}
                            {{--<span>{{$member->first_name}} {{$member->last_name}}</span><br/>--}}
                            {{--<span>{{$user->email}}</span>--}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td>Item</td>
            <td>Price</td>
        </tr>

        @foreach ($order_details as $i => $order_detail)
        <tr class=@if ($i == count($order_details)-1) "item last" @else "item" @endif >
            <td>{{$order_detail->qty}}
                <small>x</small> {{$order_detail->product_name}}</td>
            <td>
                @if ($order->type == "point")
                    {{($order_detail->qty * $order_detail->point_price)}} pts
                @elseif ($order->type == "cash" || $order->type == "free")
                    AUD {{ toMoneyConverter(($order_detail->qty * $order_detail->unit_price))}}
                @elseif ($order->type == "mix")
                    AUD {{ toMoneyConverter(($order_detail->qty * $order_detail->unit_price))}}
                @endif
            </td>
        </tr>
        @endforeach

        <tr class="total">
            <td></td>

            <td>
                @if($order->type == "mix")
                    Point used: {{ $order->total_point + 0 }} pts<br />
                @endif
                Total: @if ($order->type == "point")
                    {{$order->actual_total_point + 0}} pts
                @elseif ($order->type == "cash" || $order->type == "free")
                    AUD {{toMoneyConverter($order->actual_total_price)}}
                @elseif ($order->type == "mix")
                    AUD {{toMoneyConverter($order->actual_total_price)}}
                @endif
            </td>
        </tr>

        <tr>
            <td colspan="2"><strong>
                    @if ($order->type == "cash")
                        The total price includes GST ({{ toMoneyConverter(calculateGST($order->actual_total_price)) }})
                    @elseif ($order->type == "mix")
                        The total price includes GST ({{ toMoneyConverter(calculateGST($order->actual_total_price)) }})
                    @endif
                </strong></td>
        </tr>

        <tr>
            <td colspan="2">
                @if (!is_null($order->comments))
                    <hr />
                    <strong>Member's comment:</strong><br>{{$order->comments}}
                @endif
            </td>
        </tr>
    </table>
</div>
