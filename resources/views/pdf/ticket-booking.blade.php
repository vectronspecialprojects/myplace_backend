<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <META http-equiv="X-UA-Compatible" content="IE=8">
    <STYLE type="text/css">

        html {
            margin: 0px;
        }

        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        body {
            margin-top: 0px;
            margin-left: 0px;
            font-family: sans-serif
        }

        .full-image {
            width: 100%;
        }

        .ft1 {
            font: bold 16px 'Segoe UI';
            color: #595959;
            line-height: 21px;
            font-family: sans-serif;
        }

        .ft2 {
            font: 14px 'Segoe UI';
            color: #595959;
            /*line-height: 18px;*/
            font-family: sans-serif;
        }

        .ft3 {
            font: 10px 'Segoe UI';
            color: #595959;
            /*line-height: 18px;*/
            font-family: sans-serif;
        }

        .ft8 {
            font: bold 14px 'Segoe UI';
            color: #595959;
            /*line-height: 17px;*/
            font-family: sans-serif;
        }

        .p1 {
            text-align: left;
            padding-left: 25px;
            margin-top: 15px;
            margin-bottom: 0px;
        }

        .p15 {
            text-align: left;
            padding-left: 75px;
            margin-top: 15px;
            margin-bottom: 0px;
        }

        .p2 {
            text-align: left;
            padding-left: 25px;
            margin-top: 7px;
            margin-bottom: 0px;
        }

        .p3 {
            text-align: left;
            padding-left: 25px;
            margin-top: 8px;
            margin-bottom: 0px;
        }

        .pticket {
            text-align: left;
            padding-left: 15px;
            margin-top: 15px;
            margin-bottom: 0px;
        }

        .p13 {
            text-align: center;
            white-space: nowrap;
        }

        .p14 {
            text-align: center;
            margin-bottom: 0px;
        }

        .p16 {
            text-align: center;
            margin-bottom: 0px;
        }

        .td4 {
            padding: 0px;
            margin: 0px;
            vertical-align: bottom;
        }

        .td5 {
            padding: 0px;
            margin: 0px;
            vertical-align: bottom;
        }

        .tdicon {
            text-align: center;
        }

        .tdright {
            text-align: right;
            padding-right: 15px;
        }

        /*.p-margin-bottom {margin-bottom: 25px;}*/

        .t1 {
            width: 100%;
            font: bold 13px 'Segoe UI';
            color: #595959;
        }

        .header {
            position: fixed;
            top: 0px;
        }

        .header2 {
            position: fixed;
            top: 335px;
        }

        .tfooter {
            width: 100%;
            font: bold 13px 'Segoe UI';
            color: #595959;
            margin: 0;
            position: fixed;
            bottom: 135px;
        }

        .icon {
            max-width: 30px;
        }

        .badge {
            max-width: 75px;
        }

        .td50 {
            width: 50%;
        }

        .td55 {
            width: 55%;
        }

        .td35 {
            width: 35%;
        }

        .td10 {
            width: 10%;
        }

        .td40 {
            width: 40%;
        }

        .td60 {
            width: 60%;
        }

        .td12 {
            width: 12%;
        }

        .td53 {
            width: 53%;
        }

        .hr-dot {
            border-top: 2px dashed #8c8b8b;
        }

        .lookup {
            padding-left: 13px;
        }

        .image-center {
            text-align: center;
            vertical-align: middle;
        }

        .page-break {
            page-break-after: always;
        }
    </STYLE>
</HEAD>

<body>
    <header class="header">
        <div id="p1dimg1" >
            @if($header_setting === 'true')
                <img class="full-image" src="./images/header-background-small.png">
            @else
                <img class="full-image" src="{{ $header_image }}" />
            @endif
        </div>

        <table cellpadding=0 cellspacing=0 class="t1">
            <tr style="background-color: #eff0f1;">
                <td class="td12 image-center" style="padding-bottom: 10px; padding-left: 15px;" align="center">
                    <img src="{{ $company_logo }}" style="max-width: 90px; max-height:150px;"/>
                </td>
                <td class="td53">
                    <p class="p2 ft2" style="font-size: 23px;color: #6AC8C8;"><strong>{{ $venue_name }}</strong></p>
                    <p class="p3 ft2">{{ $venue_address }}</p>
                    <p class="p3 ft2">{{ $venue_address2 }}</p>
                </td>
                <td class="td35 tdright">
                    <p class="p2 ft2"><SPAN class="ft2">Ph: </SPAN>{{ $venue_contact }}</p>
                    <p class="p3 ft2"><SPAN class="ft2">E: </SPAN>{{ $venue_email }}</p>
                </td>
            </tr>
        </table>

        <div style="background-color: #6AC8C8; padding-left: 25px; padding-bottom: 10px; padding-top: 10px">
            <p class="p1" style="margin-top:0; font-size: 20px; color: white;"><strong>ORDER NUMBER: {{ $order_id }}</strong></p>
        </div>

        <table cellpadding=0 cellspacing=0 class="t1" style="padding-left: 20px;">
            <tr>
                <td class="td60" style="padding-bottom: 10px;">
                    <p class="p1 ft2" style="font-size: 18px; margin-top:10px;">Event</p>
                    <p class="p2 ft2"><strong>{{ $event_name }}</strong></p>
                    <p class="p2 ft2" style="margin-top: 0"><SPAN class="ft2">Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN>{{ $event_date }}</p>
                    <p class="p2 ft2" style="margin-top: 0"><SPAN class="ft2">Time: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN>{{ $event_time }}</p>
                    <p class="p2 ft2"><strong>Venue: &nbsp;&nbsp;&nbsp;{{ $venue_name }} </strong></p>
                    <p class="p3 ft2" style="margin-top: 0"><SPAN class="ft2">Address: &nbsp;</SPAN>{{ $venue_address }}</p>
                    <p class="p3 ft2" style="margin-top: 0"><SPAN class="ft2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN>{{ $venue_address2 }}</p>
                </td>
                <td class="td40" style="padding-bottom: 10px; ">
                    <p class="p1 ft2 " style="font-size: 18px; margin-top:10px;">Purchase Details</p>
                    <p class="p2 ft2 "><strong>Purchaser: {{ $member->first_name . ' ' . $member->last_name }}</strong></p>
                    <p class="p2 ft2 " style="margin-top: 0">Amount: &nbsp;{{ $ticket_count }}<small>x</small> ticket(s)</p>
                    <p class="p2 ft2 ">&nbsp;</p>
                    
                </td>
            </tr>
        </table>

        <div style="padding-left: 25px; padding-bottom: 10px; padding-top: 10px; border-top: 2px #595959 solid">
            <p class="p1" style="margin-top:0; font-size: 18px;">Scan individual tickets below</p>
        </div>
    </header>

    <footer class="tfooter">
        <table cellpadding=0 cellspacing=0>
            <tr style="background-color: #eff0f1">
                <td class="tr3 td4 td50"><p class="p13 ft8">FOLLOW US ON</p></td>
                <td class="tr3 td5 td50"><p class="p13 ft8">DOWNLOAD OUR APP ON</p></td>
            </tr>
            <tr style="background-color: #eff0f1">
                <td class="tdicon td50">
                    <img class="icon" src="./images/facebook.png">
                    <img class="icon" src="./images/twitter.png">
                    <img class="icon" src="./images/google.png">
                    <img class="icon" src="./images/Linkedin.png">
                    <img class="icon" src="./images/instagram.png">
                </td>
                <td class="tdicon td50">
                    <img class="badge" src="./images/appstore.png">
                    <img class="badge" src="./images/googleplay.png">
                </td>
            </tr>
            <tr style="background-color: #eff0f1">
                <td colspan="2"
                    style="padding-left: 20px; padding-right: 20px; padding-bottom:20px; background-color: #eff0f1; margin-top:0; padding-top:0;">
                    <p class="p14 ft3">Do you have a question? Please refer to the FAQs or call the {{ $venue_name }}
                        Customer Service Centre on {{ $venue_contact }}. Vectron Systems, Unit
                        <NOBR>5/63-71</NOBR>
                        Boundary Rd, North Melbourne VIC 3051, Australia https://www.vectron.com.au tel: 1300 123 456
                    </p>
                </td>
            </tr>
        </table>
    </footer>



    <?php
        $voucher_per_row = 0;
        $voucher_per_page = 0;
        $voucher_grand_total = 0;
    ?>
    @foreach ($vouchers as $i => $voucher)

        @if($voucher_per_page === 0)
            <table cellpadding=0 cellspacing=0 class="t1 p-margin-bottom" style="margin-top: 680px; padding-top: 10px; padding-left: 40px; border-spacing: 0; border-collapse: collapse;">
        @endif

        @if($voucher_per_row === 0)
                <tr style="margin: 0px; padding: 0px;">
                    <td style="margin: 0px; padding: 0px;">
        @endif
                        
                        <div style="border: 1px dotted #555555; padding: 15px; display: inline-block">
                            {!! $voucher['name'] !!}<br/>
                            <img style="max-height: 50px; padding-left: 5px;" src=".{!! $voucher['image'] !!}"><br/>
                            <span class="lookup">{!! $voucher['lookup'] !!}</span>
                        </div>

                <?php
                        $voucher_per_row++;
                        $voucher_per_page++;
                        $voucher_grand_total++;
                ?>

            @if($voucher_per_row === 5)
                <?php
                    $voucher_per_row = 0;
                ?>
                    </td>
                </tr>

            @endif

            @if($voucher_per_page === 10)
                <?php
                    $voucher_per_page = 0;
                ?>
                </table>
                <div class="page-break"></div>

            @endif

    @endforeach

    @if($voucher_per_page > 0)
            </td>
        </tr>
    </table>
    @endif



</body>
</html>
