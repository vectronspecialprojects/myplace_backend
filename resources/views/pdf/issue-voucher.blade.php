<?php
    date_default_timezone_set(config('app.timezone'));
?>
<style>
    .barcode-box {
        font-size: 16px;
        font-weight: 400;
        min-height: 100px;
        overflow: hidden;
        z-index: 1;
        position: relative;
        background: #fff;
        border-radius: 2px;
        box-sizing: border-box;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
        padding: 5px;
    }
    
    .barcode-title {
        color: #FFFFFF;
        height: 176px;
        background: black;
    }
</style>

    <div class="barcode-box">
        <div>
            <h2>{{$barcodes['voucher']->order_detail->product_name}}</h2>
        </div>
        <div style="padding: 10px">{!! $barcodes['image'] !!}</div>
        <span><small><strong>Valid Until:</strong> @if ($barcodes['voucher']->expire_date == 0 || \Carbon\Carbon::parse($barcodes['voucher']->expire_date)->year == 1) No expiry
                date. @else {{date("jS F, Y", strtotime($barcodes['voucher']->expire_date)) }} @endif</small></span><br/>
        <span><small><strong>Voucher Code:</strong> {{ $barcodes['voucher']->barcode }}</small></span>
    </div>
