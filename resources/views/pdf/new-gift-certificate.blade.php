<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <META http-equiv="X-UA-Compatible" content="IE=8">
    <STYLE type="text/css">

        html {
            margin: 0px;
        }

        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        body {
            margin-top: 0px;
            margin-left: 0px;
            font-family: sans-serif
        }

        .full-image {
            width: 100%;
            max-height: 350px;
        }

        .ft0 {
            font: bold 15px 'Segoe UI';
            color: #67c8c7;
            line-height: 29px;
            font-family: sans-serif;
            text-align: center;
        }

        .ft1 {
            font: bold 13px 'Segoe UI';
            color: #595959;
            line-height: 18px;
            font-family: sans-serif;
        }

        .ft2 {
            font: 13px 'Segoe UI';
            color: #595959;
            line-height: 18px;
            font-family: sans-serif;
        }

        .ft3 {
            font: bold 16px 'Calibri';
            color: #67c8c7;
            font-family: sans-serif;
        }

        .ft6 {
            font: 18px 'Calibri';
            color: #3f3f3f;
            line-height: 16px;
            font-family: sans-serif;
        }

        .ft8 {
            font: bold 15px 'Segoe UI';
            color: #595959;
            line-height: 17px;
            font-family: sans-serif;
        }

        .p0 {
            margin-top: 15px;
        }

        .p1 {
            text-align: left;
            padding-left: 51px;
            margin-top: 35px;
            margin-bottom: 0px;
        }

        .p2 {
            text-align: left;
            padding-left: 51px;
            margin-top: 7px;
            margin-bottom: 0px;
        }

        .p3 {
            text-align: left;
            padding-left: 51px;
            margin-top: 8px;
            margin-bottom: 0px;
        }

        .p13 {
            text-align: center;
            white-space: nowrap;
        }

        .p14 {
            text-align: center;
            margin-top: 20px;
            margin-bottom: 0px;
            padding-left: 51px;
            padding-right: 51px;
        }

        .p16 {
            text-align: center;
            margin-top: 24px;
            margin-bottom: 0px;
        }

        .td4 {
            padding: 0px;
            margin: 0px;
            vertical-align: bottom;
        }

        .td5 {
            padding: 0px;
            margin: 0px;
            vertical-align: bottom;
        }

        .tdicon {
            text-align: center;
        }

        .tdright {
            text-align: right;
            padding-right: 40px;
        }

        .p-margin-bottom {
            margin-bottom: 15px;
        }

        .t1 {
            width: 100%;
            font: bold 13px 'Segoe UI';
            color: #595959;
        }

        .t2 {
            width: 100%;
            text-align: center;
            font-family: sans-serif;
            margin-top: 5px;
        }

        .icon {
            max-width: 50px;
        }

        .bepoz-logo {
            max-height: 40px;
            padding-left: 40px;
        }

        .badge {
            max-width: 100px;
        }

        .td50 {
            width: 50%;
        }

        .td25 {
            width: 25%;
        }

        .hr-dot {
            border-top: 2px dashed #8c8b8b;
        }

        .lookup {
            padding-right: 60px;
        }

    </STYLE>
</HEAD>

<body>
<div id="page_1">
    <div id="p1dimg1">
        @if($header_setting === 'true')
            <img class="full-image" src="./images/header-background.png">
        @else
            <img class="full-image" src="{{ $header_image }}"/>
        @endif
    </div>

    <table class="t2">
        <tr>
            <td>
                <p class="p0 ft0">ENJOY YOUR GIFT!</p>
            </td>
        </tr>
    </table>
    <table cellpadding=0 cellspacing=0 class="t1">
        <tr>
            <td class="td50 tdicon ft6"><span class="ft3">FROM</span> {{ $member->first_name .' '. $member->last_name }}
            </td>
            <td class="td50 tdicon ft6"><span class="ft3">TO</span> {{ $gf->full_name }}</td>
        </tr>
    </table>

    <p class="p1 ft2 p-margin-bottom"><SPAN class="ft1">Message: </SPAN><br/>
        {{ $gf->note }}
    </p>

    <hr class="hr-dot"/>

    <table cellpadding=0 cellspacing=0 class="t1 p-margin-bottom">
        <tr>
            <td class="td25">
                <img class="bepoz-logo" src="{{ $company_logo }}">
            </td>
            <td class="td50">
                <p class="p0 ft0">GIFT VOUCHER DETAILS</p>
            </td>
            <td class="td25 tdright">
                <p class="p0 ft8">#{{$gf->order_id}}</p>
            </td>
        </tr>
    </table>

    <table cellpadding=0 cellspacing=0 class="t1 p-margin-bottom">
        <tr>
            <td class="td50">
                <p class="p1 ft2"><SPAN class="ft1">Owner: </SPAN>{{ $gf->full_name }}</p>
                <p class="p2 ft2"><SPAN class="ft1">Type: </SPAN>Gift Certificate ({{ $voucher->name }})</p>
                <p class="p3 ft2"><SPAN class="ft1">Value: $</SPAN>{{ $gf->value }}</p>
                <p class="p2 ft2"><SPAN class="ft1">Redeem by: </SPAN>{{ $gf->expiry_date }}</p>
            </td>
            <td class="td50 tdright">
                <img src="./{!! $barcode['image'] !!}"><br/>
                <span class="lookup">{!! $gf->lookup !!}</span>
            </td>
        </tr>
    </table>

    <hr/>

    <p class="p14 ft2">Do you have a question? Please refer to the FAQs or call the {{ $venue_name }} Customer Service
        Centre on {{$venue_contact}}. {{ $venue_name }}, {{ $venue_address }}, Australia.
    </p>
</div>
</body>
</html>
