<?php

use Illuminate\Database\Seeder;

class EmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->emailSetting('welcome', 'Welcome Email', 'welcome-vectron', 'Welcome','750609');
        $this->emailSetting('confirmation', 'Confirmation Email', 'confirmation-vectron', 'Please confirm your email','750660');
        $this->emailSetting('reset', 'Reset Password Email', 'reset-vectron', 'Reset your password','');
        $this->emailSetting('reset_successful', 'Reset Successful Email', 'reset-successful-vectron', 'Resetting Password is Successful','');
        $this->emailSetting('enquiry', 'Enquiry Email', 'enquiry-vectron', 'Thank you for your enquiry','');
        $this->emailSetting('enquiry_admin', 'Admin Enquiry Email', 'enquiry-admin-vectron', 'You just received an enquiry','');
        $this->emailSetting('invoice', 'Invoice Email', 'invoice-vectron', 'Here is your Invoice','');
        $this->emailSetting('receipt', 'Receipt Email', 'receipt-vectron', 'Here is your Receipt','');
//        $this->emailSetting('pickup', 'Chauffeur Request Email', 'car-pickup-request', 'New Car Pickup Request','');
        $this->emailSetting('referral', 'Refer a Friend Email', 'refer-a-friend', 'Bepoz Referral program','');
        $this->emailSetting('contactus_admin', 'Admin Contact Us Email', 'contactus-admin-vectron', 'You just received a message','');
        $this->emailSetting('contactus', 'Contact Us Email', 'contactus-vectron', 'Thank you for contacting us','');
//        $this->emailSetting('reservation', 'Reservation Email', 'reservation-vectron', 'Here is your ticket.','');
        $this->emailSetting('signup_admin', 'Signup Admin Email', 'signup-admin-vectron', 'A new member joined recently !!','');
//        $this->emailSetting('reservation_admin', 'Reservation Admin Email', 'reservation-admin-vectron', 'A member made a booking recently !!','');
//        $this->emailSetting('followup', 'Follow Up Email', 'followup-vectron', 'Hi, we just left a message!','');
//        $this->emailSetting('reservation_confirmation', 'Booth Confirmation Email', 'booth-confirmation-vectron', 'Please confirm your booking','');
//        $this->emailSetting('booking_enquiry', 'Booking Enquiry Email', 'booking-enquiry-vectron', 'You just received a booking enquiry','');
        $this->emailSetting('inviter_referal', 'Inviter referral Email', 'inviter-referal', 'Thank you for referring friend','');
        $this->emailSetting('reminder_referral', 'Reminder referral Email', 'reminder-friend-referral', 'Reminder: Bepoz Referral program','');
//        $this->emailSetting('downgrade_admin', 'Downgrade Notification Admin Email', 'downgrade-admin-vectron', 'A member has been downgraded','');
//        $this->emailSetting('downgrade', 'Downgrade Notification Email', 'downgrade-vectron', 'You have been downgraded !!','');
//        $this->emailSetting('upgrade_tier_admin', 'Upgrade Membership Notification Email', 'upgrade-tier-admin-vectron', 'A member just made an upgrade !!','');
        $this->emailSetting('helpdesk', 'Helpdesk Email', 'helpdesk-vectron', 'A question from customer !!','');
        $this->emailSetting('gift_certificate_receiver', 'Gift Certificate Email', 'Gift-Voucher-Receiver', 'A Bepoz eGift Card for You !!','');
        $this->emailSetting('gift_certificate_sender', 'Gift Certificate Email', 'Gift-Voucher-Sender', 'Here is your receipt !!','');
        $this->emailSetting('ticket_booking_backend', 'Ticket Booking Email', 'ticket-voucher-receipt-vectron', 'Here is your booking ticket and receipt !!','');
        $this->emailSetting('system_check', 'System Check Email', 'system-check-vectron', 'Here is Bepoz PC disconnected !!','');
        $this->emailSetting('reminder_ticket', 'Reminder Ticket', 'reminder-ticket', 'Reminder for Ticket(s)','');
        $this->emailSetting('reminder_voucher', 'Reminder Voucher', 'reminder-voucher', 'Reminder for voucher(s)','');
        $this->emailSetting('import_member', 'Import Member', 'import-member', 'Welcome with your temporary password','');
        $this->emailSetting('membership_downgrade_notification', 'Membership Downgrade Notification', 'membership-downgrade-notification', 'Membership Downgrade Notification','');
        $this->emailSetting('resend_membership_details', 'Resend Membership Details', 'resend-membership-details', 'Resend Membership Detail(s)','');
        $this->emailSetting('confirmation_change_email', 'Confirmation Change Email', 'confirmation-change-email', 'Confirmation Change Email','');

    }

    function emailSetting($uid, $EmailName, $TemplateName, $Subject, $TemplateID)
    {
        $email = \App\Email::where('uid', $uid)->first();
        if (is_null($email)) {
            $email = new \App\Email();
            $email->name = $EmailName;
            $email->uid = $uid;
            $email->category = 'system';
            $email->save();

            $setting = \App\MandrillSetting::where('email_id', $email->id)
                ->where('key', 'is_used')
                ->first();

            if (is_null($setting)) {
                $setting = new \App\MandrillSetting();
                $setting->key = 'is_used';
                $setting->value = 'true';
                $setting->type = 'true';
                $setting->email_id = $email->id;
                $setting->save();
            }

            $setting = \App\MandrillSetting::where('email_id', $email->id)
                ->where('key', 'template')
                ->first();

            if (is_null($setting)) {
                $setting = new \App\MandrillSetting();
                $setting->key = 'template';
                $setting->value = $TemplateName;
                $setting->type = 'string';
                $setting->email_id = $email->id;
                $setting->save();
            }

            $setting = \App\MandrillSetting::where('email_id', $email->id)
                ->where('key', 'subject')
                ->first();

            if (is_null($setting)) {
                $setting = new \App\MandrillSetting();
                $setting->key = 'subject';
                $setting->value = $Subject;
                $setting->type = 'string';
                $setting->email_id = $email->id;
                $setting->save();
            }

            $setting = \App\MandrillSetting::where('email_id', $email->id)
                ->where('key', 'template_id')
                ->first();

            if (is_null($setting)) {
                $setting = new \App\MandrillSetting();
                $setting->key = 'template_id';
                $setting->value = $TemplateID;
                $setting->type = 'string';
                $setting->email_id = $email->id;
                $setting->save();
            }

        }
    }

}
