<?php

use Illuminate\Database\Seeder;

class DBCleanUp extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->truncate();
        \Illuminate\Support\Facades\DB::table('password_resets')->truncate();
        \Illuminate\Support\Facades\DB::table('addresses')->truncate();
        \Illuminate\Support\Facades\DB::table('user_roles')->truncate();
        \Illuminate\Support\Facades\DB::table('members')->truncate();
        \Illuminate\Support\Facades\DB::table('jobs')->truncate();
        \Illuminate\Support\Facades\DB::table('failed_jobs')->truncate();
        \Illuminate\Support\Facades\DB::table('voucher_setups')->truncate();
        \Illuminate\Support\Facades\DB::table('bepoz_jobs')->truncate();
        \Illuminate\Support\Facades\DB::table('member_vouchers')->truncate();
        \Illuminate\Support\Facades\DB::table('bepoz_failed_jobs')->truncate();
        \Illuminate\Support\Facades\DB::table('order_details')->truncate();
        \Illuminate\Support\Facades\DB::table('orders')->truncate();
        \Illuminate\Support\Facades\DB::table('system_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('point_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('point_rewards')->truncate();
        \Illuminate\Support\Facades\DB::table('notifications')->truncate();
        \Illuminate\Support\Facades\DB::table('answer_member')->truncate();
        \Illuminate\Support\Facades\DB::table('favorite_listing')->truncate();
        \Illuminate\Support\Facades\DB::table('like_listing')->truncate();
        \Illuminate\Support\Facades\DB::table('creditcard_transactions')->truncate();
        \Illuminate\Support\Facades\DB::table('member_tier')->truncate();
        \Illuminate\Support\Facades\DB::table('listing_enquiries')->truncate();
        \Illuminate\Support\Facades\DB::table('bepoz_pushed_points_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('bepoz_pushed_vouchers_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('bepoz_pushed_account_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('friend_referrals')->truncate();
        \Illuminate\Support\Facades\DB::table('contact_uses')->truncate();
        \Illuminate\Support\Facades\DB::table('push_notification_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('claimed_promotions')->truncate();
        \Illuminate\Support\Facades\DB::table('staff')->truncate();
        \Illuminate\Support\Facades\DB::table('bepoz_transactions')->truncate();
        \Illuminate\Support\Facades\DB::table('member_system_notification')->truncate();
        \Illuminate\Support\Facades\DB::table('bepoz_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('member_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('social_logins')->truncate();
        \Illuminate\Support\Facades\DB::table('visit_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('gift_certificates')->truncate();
        \Illuminate\Support\Facades\DB::table('payment_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('stripe_customers')->truncate();
        \Illuminate\Support\Facades\DB::table('mail_logs')->truncate();
        \Illuminate\Support\Facades\DB::table('pending_jobs')->truncate();

        \App\Setting::where('key', 'friend_referral_voucher_setups_id')->update(['value' => '0']);
        \App\Listing::where('status', 'active')->update(['click_count' => 0]);

    }
}
