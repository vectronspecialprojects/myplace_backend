<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUser('dev@vectron.com.au', 'secret', "Dev", "Vectron");
        $this->createUser('admin@vectron.com.au', 'secret', "Admin", "Vectron");
    }

    public function createUser($email, $password, $first_name, $last_name)
    {
        $user = \App\User::where('email', $email)->first();
        if (is_null($user)) {
            $user = new \App\User();
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->email_confirmation = true;
            $user->save();
        }

        $userRole = \App\UserRoles::where('user_id', $user->id)->first();
        if (is_null($userRole)) {
            $userRole = new \App\UserRoles();
            $userRole->user_id = $user->id;
            $userRole->role_id = 4;
            $userRole->save();
        }

        $member = \App\Member::where('user_id', $user->id)->first();
        if (is_null($member)) {
            $member = new \App\Member();
            $member->first_name = $first_name;
            $member->last_name = $last_name;
            $member->user_id = $user->id;
            $member->bepoz_account_id = 0;
            $member->bepoz_account_number = 0;
            $member->bepoz_account_card_number = 0;
            $member->save();
        }

        $staff = \App\Staff::where('user_id', $user->id)->first();
        if (is_null($staff)) {
            $staff = new \App\Staff();
            $staff->user_id = $user->id;
            $staff->first_name = $first_name;
            $staff->last_name = $last_name;
            $staff->active = true;
            $staff->save();
        }

        $memberTier = \App\MemberTiers::where('member_id', $member->id)->first();
        if (is_null($memberTier)) {
            $memberTier = new \App\MemberTiers();
            $memberTier->member_id = $member->id;
            $memberTier->tier_id = 1;
            $memberTier->save();
        }
    }
}
