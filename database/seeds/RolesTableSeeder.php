<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRole('admin', 'Best for business owners and company administrators, with limited access.',
            true, true, true, true,
            true, true,
            true, true, true, true,
            true, true, true, true,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true,
            true, true, true, true,
            true,
            true, true,
            true, true,
            true, true, true, true,
            false, true, false, false,
            true, true, false, false,
            true, false, true, true,
            false
        );

        $this->createRole('staff', 'Best for people who need partial access to the BackPanel.',
            true, true, true, false,
            true, false,
            true, true, true, false,
            true, true, true, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            false, true, true, false,
            false, false, false, false,
            true, true, true, false,
            true, true, false,
            true, true, true, false,
            true,
            true, true,
            true, true,
            true, true, true, false,
            false, true, false, false,
            true, true, false, false,
            false, false, true, true,
            false
        );

        $this->createRole('member', 'Best for people who do not need the BackPanel access.',
            false, false, false, false,
            false, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false,
            false, false, false, false,
            false,
            false, false,
            false, false,
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            true, false, false, false,
            false, false, false, false,
            false
        );

        $this->createRole('bepoz_admin', 'Best for developers or people primarily using the BackPanel.',
            true, true, true, true,
            true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true,
            true, true, true, true,
            true,
            true, true,
            true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true, true, true, true,
            true
        );
    }

    function createRole($name, $description,
                        $listing_create, $listing_read, $listing_update, $listing_delete,
                        $setting_read, $setting_update,
                        $product_create, $product_read, $product_update, $product_delete,
                        $voucher_create, $voucher_read, $voucher_update, $voucher_delete,
                        $role_create, $role_read, $role_update, $role_delete,
                        $user_create, $user_read, $user_update, $user_delete,
                        $user_role_create, $user_role_read, $user_role_update, $user_role_delete,
                        $member_create, $member_read, $member_update, $member_delete,
                        $staff_create, $staff_read, $staff_update, $staff_delete,
                        $notification_create, $notification_read, $notification_update, $notification_delete,
                        $enquiry_read, $enquiry_update, $enquiry_delete,
                        $faq_create, $faq_read, $faq_update, $faq_delete,
                        $friend_referral_read,
                        $transaction_read, $transaction_export,
                        $report_read, $report_export,
                        $survey_create, $survey_read, $survey_update, $survey_delete,
                        $venue_create, $venue_read, $venue_update, $venue_delete,
                        $frontend_access, $backend_access, $root_access, $venue_management,
                        $log_read, $log_delete, $send_broadcast, $resend_confirmation,
                        $admin_update
    )
    {
        $role = new \App\Role();
        $role->name = $name;
        $role->description = $description;
        $role->save();

        $role_control = new \App\RoleControl();
        $role_control->role_id = $role->id;
        $role_control->listing_create = $listing_create;
        $role_control->listing_read = $listing_read;
        $role_control->listing_update = $listing_update;
        $role_control->listing_delete = $listing_delete;

        $role_control->setting_read = $setting_read;
        $role_control->setting_update = $setting_update;

        $role_control->product_create = $product_create;
        $role_control->product_read = $product_read;
        $role_control->product_update = $product_update;
        $role_control->product_delete = $product_delete;

        $role_control->voucher_create = $voucher_create;
        $role_control->voucher_read = $voucher_read;
        $role_control->voucher_update = $voucher_update;
        $role_control->voucher_delete = $voucher_delete;

        $role_control->user_create = $user_create;
        $role_control->user_read = $user_read;
        $role_control->user_update = $user_update;
        $role_control->user_delete = $user_delete;

        $role_control->role_create = $role_create;
        $role_control->role_read = $role_read;
        $role_control->role_update = $role_update;
        $role_control->role_delete = $role_delete;

        $role_control->user_role_create = $user_role_create;
        $role_control->user_role_read = $user_role_read;
        $role_control->user_role_update = $user_role_update;
        $role_control->user_role_delete = $user_role_delete;

        $role_control->member_create = $member_create;
        $role_control->member_read = $member_read;
        $role_control->member_update = $member_update;
        $role_control->member_delete = $member_delete;

        $role_control->staff_create = $staff_create;
        $role_control->staff_read = $staff_read;
        $role_control->staff_update = $staff_update;
        $role_control->staff_delete = $staff_delete;

        $role_control->notification_create = $notification_create;
        $role_control->notification_read = $notification_read;
        $role_control->notification_update = $notification_update;
        $role_control->notification_delete = $notification_delete;

        $role_control->enquiry_read = $enquiry_read;
        $role_control->enquiry_update = $enquiry_update;
        $role_control->enquiry_delete = $enquiry_delete;

        $role_control->faq_create = $faq_create;
        $role_control->faq_read = $faq_read;
        $role_control->faq_update = $faq_update;
        $role_control->faq_delete = $faq_delete;

        $role_control->friend_referral_read = $friend_referral_read;
        $role_control->transaction_read = $transaction_read;
        $role_control->transaction_export = $transaction_export;

        $role_control->report_read = $report_read;
        $role_control->report_export = $report_export;

        $role_control->survey_create = $survey_create;
        $role_control->survey_read = $survey_read;
        $role_control->survey_update = $survey_update;
        $role_control->survey_delete = $survey_delete;

        $role_control->venue_create = $venue_create;
        $role_control->venue_read = $venue_read;
        $role_control->venue_update = $venue_update;
        $role_control->venue_delete = $venue_delete;

        $role_control->frontend_access = $frontend_access;
        $role_control->backend_access = $backend_access;
        $role_control->root_access = $root_access;
        $role_control->venue_management = $venue_management;

        $role_control->log_read = $log_read;
        $role_control->log_delete = $log_delete;

        $role_control->send_broadcast = $send_broadcast;
        $role_control->resend_confirmation = $resend_confirmation;

        $role_control->admin_update = $admin_update;

        $role_control->save();
    }
}
