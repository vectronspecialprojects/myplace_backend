<?php

use Illuminate\Database\Seeder;

class PatternsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('patterns')->insert([
            'pattern' => '1XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '2XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '3XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '4XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '5XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '6XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '7XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '8XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '9XXXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => 'APPXXXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => 'APP1XXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '1XXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '2XXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '3XXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '4XXXXX',
        ]);

        DB::table('patterns')->insert([
            'pattern' => '1XXXXXXX',
        ]);
    }
}
