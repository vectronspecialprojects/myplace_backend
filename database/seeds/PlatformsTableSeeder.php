<?php

use Illuminate\Database\Seeder;

class PlatformsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('platforms')->insert([
            'name' => 'desktop',
            'app_id' => '0bb2a71c-6d77-4c19-8de5-1cfbec7f77fe',
            'app_token' => '1cfbec7f77fe',
            'app_secret' => 'dd3e6890b29f24b53bbe4774f608671b',
            'version' => '1.0',
            'platform' => 'desktop'
        ]);

        DB::table('platforms')->insert([
            'name' => 'mobile',
            'app_id' => 'ca7b57d1-27de-460c-b355-12a3e1a732ee',
            'app_token' => '12a3e1a732ee',
            'app_secret' => '2da489f7ac6cce34ddcda02ef126270b',
            'version' => '1.0',
            'platform' => 'mobile'
        ]);

        DB::table('platforms')->insert([
            'name' => 'web',
            'app_id' => '1f9c8ae5-9355-42b4-a8cb-039e114b4c1d',
            'app_token' => '039e114b4c1d',
            'app_secret' => '4e4eefa6aa11d01af794aed39b37781f',
            'version' => '1.0',
            'platform' => 'web'
        ]);
    }
}
