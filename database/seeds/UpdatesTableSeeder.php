<?php

use Illuminate\Database\Seeder;

class UpdatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = \App\Member::all();
        foreach ($members as $member) {
            $member->token = md5(time() . $member->id);
            $member->save();
        }
    }
}
