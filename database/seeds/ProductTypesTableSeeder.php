<?php

use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createType('Voucher',
            [
                "name" => "Voucher",
                "source" => 'bepoz',
                "type" => 'voucher'
            ]
        );

        $this->createType('Ticket',
            [
                "name" => "Ticket",
                "source" => 'bepoz',
                "type" => 'ticket'
            ]
        );

        $this->createType('F&B Voucher',
            [
                "name" => "F&B Voucher",
                "source" => 'bepoz',
                "type" => 'voucher'
            ]
        );

        $this->createType('Product',
            [
                "name" => "Product",
                "source" => 'bepoz',
                "type" => 'product'
            ]
        );
        
    }

    function createType($name, $key)
    {
        $lt = \App\ProductType::where('name', $name)->first();
        if (is_null($lt)) {
            $lt = new \App\ProductType();
            $lt->name = $name;
            $lt->key = $key;
            $lt->save();
        }
    }
}
