<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateVoucherSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_setups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bepoz_voucher_setup_id'); // bepoz voucher setup id
            $table->string('name'); // name from bepoz voucher setup table
            $table->boolean('inactive')->default(true);
            $table->integer('expiry_type')->nullable();
            $table->integer('expiry_number')->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->integer('voucher_type')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('voucher_setups');
    }
}
