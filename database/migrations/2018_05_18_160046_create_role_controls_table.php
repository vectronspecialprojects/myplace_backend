<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRoleControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_controls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('role_id');

            /**
             * BackPanel Settings
             */

            $table->boolean('listing_create')->default(false);
            $table->boolean('listing_read')->default(false);
            $table->boolean('listing_update')->default(false);
            $table->boolean('listing_delete')->default(false);

            $table->boolean('setting_read')->default(false);
            $table->boolean('setting_update')->default(false);

            $table->boolean('voucher_create')->default(false);
            $table->boolean('voucher_read')->default(false);
            $table->boolean('voucher_update')->default(false);
            $table->boolean('voucher_delete')->default(false);

            $table->boolean('product_create')->default(false);
            $table->boolean('product_read')->default(false);
            $table->boolean('product_update')->default(false);
            $table->boolean('product_delete')->default(false);

            $table->boolean('role_create')->default(false);
            $table->boolean('role_read')->default(false);
            $table->boolean('role_update')->default(false);
            $table->boolean('role_delete')->default(false);

            $table->boolean('user_create')->default(false);
            $table->boolean('user_read')->default(false);
            $table->boolean('user_update')->default(false);
            $table->boolean('user_delete')->default(false);

            // grant access

            $table->boolean('user_role_create')->default(false);
            $table->boolean('user_role_read')->default(false);
            $table->boolean('user_role_update')->default(false);
            $table->boolean('user_role_delete')->default(false);

            $table->boolean('member_create')->default(false);
            $table->boolean('member_read')->default(false);
            $table->boolean('member_update')->default(false);
            $table->boolean('member_delete')->default(false);

            $table->boolean('staff_create')->default(false);
            $table->boolean('staff_read')->default(false);
            $table->boolean('staff_update')->default(false);
            $table->boolean('staff_delete')->default(false);

            $table->boolean('notification_create')->default(false);
            $table->boolean('notification_read')->default(false);
            $table->boolean('notification_update')->default(false);
            $table->boolean('notification_delete')->default(false);

            $table->boolean('enquiry_read')->default(false);
            $table->boolean('enquiry_update')->default(false);
            $table->boolean('enquiry_delete')->default(false);

            $table->boolean('faq_create')->default(false);
            $table->boolean('faq_read')->default(false);
            $table->boolean('faq_update')->default(false);
            $table->boolean('faq_delete')->default(false);

            $table->boolean('friend_referral_read')->default(false);

            $table->boolean('transaction_read')->default(false);
            $table->boolean('transaction_export')->default(false);

            $table->boolean('report_read')->default(false);
            $table->boolean('report_export')->default(false);

            $table->boolean('survey_create')->default(false);
            $table->boolean('survey_read')->default(false);
            $table->boolean('survey_update')->default(false);
            $table->boolean('survey_delete')->default(false);

            $table->boolean('venue_create')->default(false);
            $table->boolean('venue_read')->default(false);
            $table->boolean('venue_update')->default(false);
            $table->boolean('venue_delete')->default(false);

            $table->boolean('frontend_access')->default(false);
            $table->boolean('backend_access')->default(false);
            $table->boolean('root_access')->default(false);
            $table->boolean('venue_management')->default(false);

            $table->boolean('log_read')->default(false);
            $table->boolean('log_delete')->default(false);

            $table->boolean('resend_confirmation')->default(false);
            $table->boolean('send_broadcast')->default(false);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_controls');
    }
}
