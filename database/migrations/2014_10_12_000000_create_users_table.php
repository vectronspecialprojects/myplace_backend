<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique()->index();
            $table->string('password')->nullable();
            $table->string('mobile')->nullable()->index();
            $table->string('email_confirmation_token')->nullable()->index();
            $table->boolean('email_confirmation')->default(false);
            $table->string('sms_confirmation_code')->nullable()->index();
            $table->boolean('sms_confirmation')->default(false);
            $table->enum('status', ['active', 'inactive'])->default('active');

            $table->dateTime('email_confirmation_sent')->nullable(); // email confirmation sent
            $table->dateTime('email_confirmed_at')->nullable(); // confirmed timestamp
            $table->dateTime('sms_confirmation_sent')->nullable(); // sms confirmation sent
            $table->dateTime('sms_confirmed_at')->nullable(); // confirmed timestamp

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
