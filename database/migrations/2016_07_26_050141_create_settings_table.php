<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('key')->unique()->index();
            $table->string('value')->nullable();
            $table->text('extended_value')->nullable();
            $table->string('table_name')->nullable();
            $table->string('value_type')->nullable(); // boolean / string / int
            $table->text('description')->nullable();
            $table->string('category')->nullable()->index();
            $table->string('hint')->nullable();
            $table->string('default_value')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
