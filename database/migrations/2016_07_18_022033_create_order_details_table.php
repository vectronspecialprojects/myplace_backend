<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('qty')->default(0);
            $table->string('product_name')->nullable();
            $table->string('voucher_name')->nullable();
            $table->longText('voucher_lookup')->nullable();
            $table->date('stock_date')->nullable();
            $table->decimal('unit_price', 10, 2)->nullable()->default(0);
            $table->decimal('point_price', 10, 2)->nullable()->default(0);
            $table->decimal('subtotal_unit_price', 10, 2)->nullable()->default(0);
            $table->decimal('subtotal_point_price', 10, 2)->nullable()->default(0);
            $table->integer('reward_point_ratio')->nullable()->default(0);
            $table->integer('point_reward')->nullable()->default(0);

            $table->decimal('individual_discounted_unit_price', 10, 2)->nullable()->default(0);
            $table->decimal('individual_discounted_point_price', 10, 2)->nullable()->default(0);
            $table->decimal('paid_subtotal_unit_price', 10, 2)->nullable()->default(0);
            $table->decimal('paid_subtotal_point_price', 10, 2)->nullable()->default(0);

            $table->bigInteger('order_id')->index();
            $table->bigInteger('product_id')->nullable()->index();
            $table->bigInteger('listing_id')->nullable()->index();
            $table->integer('product_type_id')->nullable()->index();
            $table->string('voucher_setups_id')->nullable()->index();
            $table->string('bepoz_product_number')->nullable();
            $table->string('category')->nullable(); // treat this product as voucher or ticket
            $table->bigInteger('secondary_voucher_setups_id')->nullable()->index();
            $table->boolean('immediately_issued')->default(true);
            $table->enum('status', ['expired', 'not_started', 'pending', 'in_progress', 'successful', 'failed', 'cancelled'])->default('not_started');

            $table->longText('payload')->nullable(); // store any key in json format

            $table->text('bepoz_transaction_id')->nullable();
            $table->bigInteger('venue_id')->default(0);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_details');
    }
}
