<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateMemberTableCustom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->dateTime('last_update')->nullable()->after('login_token');
            $table->string('community_points')->nullable()->after('login_token');
            $table->string('udid')->nullable()->after('login_token');
            $table->string('language')->nullable()->after('login_token');
            $table->string('state_name')->nullable()->after('login_token');
            $table->bigInteger('state_id')->default(0)->after('login_token');
            $table->string('salutation')->nullable()->after('login_token');
            $table->boolean('opt_out_marketing')->default(false)->after('login_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function ($table) {
            $table->dropColumn(['last_update']);
            $table->dropColumn(['community_points']);
            $table->dropColumn(['udid']);
            $table->dropColumn(['language']);
            $table->dropColumn(['state_name']);
            $table->dropColumn(['state_id']);
            $table->dropColumn(['salutation']);
            $table->dropColumn(['opt_out_marketing']);
        });
    }
}
