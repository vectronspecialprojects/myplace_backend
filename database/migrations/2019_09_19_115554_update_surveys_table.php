<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surveys', function (Blueprint $table) {
            $table->string('reward_type')->after('date_expiry');
            $table->bigInteger('voucher_setup_reward')->default(0)->after('point_reward');
            $table->text('payload')->after('date_expiry');
            $table->text('extra_setting')->after('date_expiry');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surveys', function ($table) {
            $table->dropColumn(['reward_type']);
            $table->dropColumn(['voucher_setup_reward']);
            $table->dropColumn(['payload']);
            $table->dropColumn(['extra_Setting']);
        });
    }
}
