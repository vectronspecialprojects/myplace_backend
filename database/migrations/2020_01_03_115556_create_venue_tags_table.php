<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenueTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('display_order_type')->nullable();
            $table->bigInteger('display_order')->default(0);
            $table->boolean('status')->default(false);
            $table->string('color')->nullable();
            $table->string('font_color')->nullable();
            $table->string('click_color')->nullable();
            $table->string('click_font_color')->nullable();
            $table->string('selected_color')->nullable();
            $table->string('selected_font_color')->nullable();
            $table->longText('style')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('venue_tags');
    }
}
