<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateBepozFailedJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bepoz_failed_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('queue');
            $table->longText('payload');
            $table->timestamp('failed_at')->useCurrent();
            $table->string('job_uid')->index();
            $table->integer('job_count')->default(0);
            $table->date('processing_date')->nullable(); // set schedule to process the job
            $table->boolean('immediately_process')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bepoz_failed_jobs');
    }
}
