<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateVenueTableYourorderIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venues', function (Blueprint $table) {
            // $table->string('your_order_key')->nullable()->after('ios_link');
            // $table->string('your_order_api')->nullable()->after('ios_link');
            $table->boolean('your_order_integration')->default(false)->after('ios_link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venues', function ($table) {
            // $table->dropColumn(['your_order_key']);
            // $table->dropColumn(['your_order_api']);
            $table->dropColumn(['your_order_integration']);
        });
    }
}
