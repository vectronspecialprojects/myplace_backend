<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('mode')->nullable();
            $table->string('old_email')->nullable();
            $table->string('new_email')->nullable();
            $table->string('old_mobile')->nullable();
            $table->string('new_mobile')->nullable();
            $table->string('old_guid')->nullable();
            $table->string('new_guid')->nullable();
            $table->string('hash_key')->nullable();
            $table->string('token')->nullable();

            $table->boolean('email_confirmation')->default(false);
            $table->dateTime('email_confirmation_sent')->nullable(); // email confirmation sent
            $table->dateTime('email_confirmed_at')->nullable(); // confirmed timestamp email

            $table->boolean('mobile_confirmation')->default(false);
            $table->dateTime('mobile_confirmation_sent')->nullable(); // mobile confirmation sent
            $table->dateTime('mobile_confirmed_at')->nullable(); // confirmed timestamp mobile

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_changes');
    }
}
