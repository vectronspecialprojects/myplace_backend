<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePromoAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('venue_id')->nullable();
            $table->string('member_id')->nullable();
            $table->string('listing_id')->nullable();
            $table->string('bepoz_account_id')->nullable();
            $table->dateTime('date_updated')->nullable();
            $table->string('prize_promotion_id')->nullable();
            $table->string('bepoz_prize_promo_id')->nullable();
            $table->string('qty_won_total')->nullable();
            $table->string('qty_last_day')->nullable();
            $table->string('date_last_day')->nullable();
            $table->string('accumulation')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promo_accounts');
    }
}
