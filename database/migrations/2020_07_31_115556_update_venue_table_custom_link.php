<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateVenueTableCustomLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venues', function (Blueprint $table) {
            $table->string('link3')->nullable()->after('ios_link');
            $table->string('link2')->nullable()->after('ios_link');
            $table->string('link1')->nullable()->after('ios_link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venues', function ($table) {
            $table->dropColumn(['link3']);
            $table->dropColumn(['link2']);
            $table->dropColumn(['link1']);
        });
    }
}
