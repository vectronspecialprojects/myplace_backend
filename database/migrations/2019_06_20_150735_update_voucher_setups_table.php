<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateVoucherSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('voucher_setups', function (Blueprint $table) {
            $table->integer('voucher_apply')->nullable()->after('voucher_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_setups', function ($table) {
            $table->dropColumn(['voucher_apply']);
        });
    }
}
