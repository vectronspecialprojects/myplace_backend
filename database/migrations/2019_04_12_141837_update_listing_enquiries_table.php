<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateListingEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('listing_enquiries', function (Blueprint $table) {
            $table->boolean('contact_me')->nullable()->after('staff_id');
            $table->integer('rating')->nullable()->after('contact_me');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('listing_enquiries');
    }
}
