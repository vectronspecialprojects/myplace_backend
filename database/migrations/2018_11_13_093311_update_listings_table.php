<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('listings', function (Blueprint $table) {
        $table->dateTime('sell_datetime_start')->nullable()->after('datetime_end');
        $table->dateTime('sell_datetime_end')->nullable()->after('sell_datetime_start');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listings');
    }
}
