<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateBepozJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bepoz_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('queue');
            $table->longText('payload');
            $table->tinyInteger('attempts')->unsigned()->default(0);
            $table->tinyInteger('reserved')->unsigned()->default(0);
            $table->unsignedInteger('reserved_at')->nullable();
            $table->unsignedInteger('freed_at')->nullable();
            $table->unsignedInteger('available_at');
            $table->unsignedInteger('created_at');
            $table->string('job_uid')->index()->nullable();
            $table->string('failed_job_uid')->index()->nullable();
            $table->index(['queue', 'reserved', 'reserved_at']);
            $table->boolean('immediately_process')->default(true);
            $table->date('processing_date')->nullable(); // set schedule to process the job
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bepoz_jobs');
    }
}
