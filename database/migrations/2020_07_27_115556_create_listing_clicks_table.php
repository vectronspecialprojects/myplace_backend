<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_clicks', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('listing_id')->default(0);
            $table->string('listing_name')->nullable();
            $table->bigInteger('listing_type_id')->default(0);
            $table->bigInteger('member_id')->default(0);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listing_clicks');
    }
}
