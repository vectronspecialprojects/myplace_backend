<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateListingsTableMembership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('listings', function (Blueprint $table) {        
        $table->boolean('prize_promotion_enable')->default(false)->after('prize_promotion_id');

        $table->string('point_price')->nullable()->after('max_limit_per_day_per_account');
        $table->string('dollar_price')->nullable()->after('max_limit_per_day_per_account');
        $table->string('product_price_option')->nullable()->after('max_limit_per_day_per_account');

        $table->string('membership_downgrade_tier')->nullable()->after('max_limit_per_day_per_account');
        $table->date('membership_expiry_date')->nullable()->after('max_limit_per_day_per_account');
        $table->string('bepoz_account_group')->nullable()->after('max_limit_per_day_per_account');
        $table->string('membership_tier')->nullable()->after('max_limit_per_day_per_account');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('listings', function ($table) {
        $table->dropColumn(['prize_promotion_enable']);
        $table->dropColumn(['point_price']);
        $table->dropColumn(['dollar_price']);
        $table->dropColumn(['product_price_option']);
        $table->dropColumn(['membership_downgrade_tier']);
        $table->dropColumn(['membership_expiry_date']);
        $table->dropColumn(['bepoz_account_group']);
        $table->dropColumn(['membership_tier']);
      });
    }
}
