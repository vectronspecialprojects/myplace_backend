<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateListingsTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('listings', function (Blueprint $table) {
            $table->string('venue_name')->nullable()->after('venue_id');
            $table->string('prize_promotion_id')->nullable()->after('payload');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listings', function ($table) {
            $table->dropColumn(['venue_name']);
        });
    }
}
