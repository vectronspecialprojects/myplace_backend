<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamingSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gaming_sessions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sessionToken')->nullable();
            $table->string('sessionTimeout')->nullable();
            $table->longText('response')->nullable();
            $table->longText('value')->nullable();
            $table->longText('payload')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gaming_sessions');
    }
}
