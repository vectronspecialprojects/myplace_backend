<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateListingScheduleListingTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_schedule_listing_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('listing_schedule_id')->unsigned()->index();
            $table->integer('listing_type_id')->unsigned()->index();
            $table->integer('tier_id')->nullable()->index();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listing_schedule_listing_type');
    }
}
