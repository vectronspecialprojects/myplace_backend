<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTiersTableMembership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tiers', function (Blueprint $table) {
            $table->string('downgrade_group_id')->nullable()->after('venue_id');
            $table->string('downgrade_tier')->nullable()->after('venue_id');
            $table->string('expiry_date_option')->nullable()->after('venue_id');
            $table->dateTime('specific_date')->nullable()->after('venue_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tiers', function ($table) {
            $table->dropColumn(['downgrade_group_id']);
            $table->dropColumn(['downgrade_tier']);
            $table->dropColumn(['expiry_date_option']);
            $table->dropColumn(['specific_date']);
        });
    }
}
