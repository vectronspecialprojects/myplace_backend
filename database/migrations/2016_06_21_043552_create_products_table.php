<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('desc_short');
            $table->text('desc_long')->nullable();
            $table->string('image')->nullable();
            $table->decimal('unit_price', 10, 2)->default(0);
            $table->decimal('point_price', 10, 2)->default(0);
            $table->decimal('point_get', 10, 2)->default(0);
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->boolean('system_point_ratio')->default(true);
            $table->integer('product_type_id')->nullable();
            $table->boolean('is_free')->default(false);
            $table->string('key')->nullable();
            $table->string('bepoz_product_id')->nullable();
            $table->string('bepoz_product_number')->nullable();
            $table->string('bepoz_voucher_setup_id')->nullable();
            $table->string('bepoz_payment_name')->nullable();
            $table->decimal('gift_value', 10, 2)->default(0);
            $table->string('category')->nullable(); // treat as voucher or ticket
            $table->bigInteger('venue_id')->default(0);

            $table->boolean('is_hidden')->default(false);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
