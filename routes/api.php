<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group([
//     'middleware' => ['GrahamCampbell\Throttle\Http\Middleware\ThrottleMiddleware:10000,1'],
// ],  function ($router) {

    /**
     * API: websignup (called by 3rd party system APi)
     * not require app token etc.
     * Method: POST
     * Parameters:
     *  - member_group (required)
     *  - expiry (required)
     *  - member_number (required)
     *  - mobile (required)
     *  - email (required)
     *  - firstname (required)
     *  - lastname (required)
     *  - barcode (required)
     * 
     */
    Route::post('/websignup', 'Admin\MemberController@websignup');

    Route::get('/', 'AuthenticationController@index');

    /**
     * Draw Reset-Password Form
     * Token needed
     */
    Route::get('/resetform/{token}', 'ForgotPasswordController@showResetPasswordForm');

    /**
     * API: get server info
     *
     * Method: GET
     */
    // Route::get('/serverinfo', 'AuthenticationController@serverinfo');

    /**
     * API: get version
     *
     * Method: GET
     */
    Route::get('/version', 'AuthenticationController@version');

    /**
     * API: get tester
     *
     * Method: GET
     */
    Route::get('/tester', 'AuthenticationController@tester');

    /**
     * API: get testerGamingAccountLowest
     *
     * Method: GET
     */
    Route::get('/testerGamingAccountLowest', 'AuthenticationController@testerGamingAccountLowest');

    /**
     * API: get testerGamingAccountCount
     *
     * Method: GET
     */
    Route::get('/testerGamingAccountCount', 'AuthenticationController@testerGamingAccountCount');

    /**
     * API: get testerEmailConfirmationSuccess
     *
     * Method: GET
     */
    Route::get('/testerEmailConfirmationSuccess', 'AuthenticationController@testerEmailConfirmationSuccess');

    /**
     * API: get testerEmailConfirmationUnsuccess
     *
     * Method: GET
     */
    Route::get('/testerEmailConfirmationUnsuccess', 'AuthenticationController@testerEmailConfirmationUnsuccess');

    /**
     * API: get testerMobileConfirmationSuccess
     *
     * Method: GET
     */
    Route::get('/testerMobileConfirmationSuccess', 'AuthenticationController@testerMobileConfirmationSuccess');

    /**
     * API: get testerMobileConfirmationUnsuccess
     *
     * Method: GET
     */
    Route::get('/testerMobileConfirmationUnsuccess', 'AuthenticationController@testerMobileConfirmationUnsuccess');

    /**
     * API: post testerigt
     *
     * Method: POST
     */
    Route::post('/testerigt', 'AuthenticationController@testerigt');

    /**
     * API: post testerigt
     *
     * Method: POST
     */
    Route::post('/testerigtwsdl', 'AuthenticationController@testerigtwsdl');

    /**
     * API: post testerigt
     *
     * Method: POST
     */
    Route::get('/testerodyssey', 'AuthenticationController@testerodyssey');

    /**
     * API: manualsync
     *
     * Method: GET
     */
    // Route::get('/manualsync/{memberid}', 'AuthenticationController@manualsync');

    /**
     * API: retrieve all setting data
     *
     * Method: GET
     */
    Route::get('/setting', 'AuthenticationController@index');

    /**
     * API: retrieve a specific setting data
     *
     * Method: GET
     */
    Route::get('/setting/{key}', 'Admin\SettingController@show');


    /**
     * API: Login (this is to go admin access panel)
     *
     * Method: POST
     * Parameters:
     *  - email (required)
     *  - password (required)
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/login', 'AuthenticationController@login');

    /**
     * API: Sign In (this is to go to member app)
     *
     * Method: POST
     * Parameters:
     *  - email (required)
     *  - password (required)
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/signin', 'AuthenticationController@signin');

    /**
     * API: quickSignIn (this is to go to member app)
     *
     * Method: POST
     * Parameters:
     *  - login_token (required)
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/quickSignIn', 'AuthenticationController@quickSignIn');

    /**
     * API: loginSSO (this is to go to member app and register to service app)
     *
     * Method: POST
     * Parameters:
     *  - email (required)
     *  - password (required)
     *
     * Returns:
     *  - token (JWT)
     *  - id_token
     *  - access_token
     *  - refresh_token
     */
    Route::post('/loginSSO', 'AuthenticationController@loginSSO');

    /**
     * API: Sign Up
     *
     * Method: POST
     * Parameters:
     *  - first_name (required)
     *  - last_name (required)
     *  - mobile (optional)
     *  - email (required)
     *  - password (required)
     *  - password_confirmation (required)
     *  - membership (optional) : normal
     *  - photo (optional) : file
     *  - addresses (optional) : json
     *  - card_number (optional) : FOR CALink
     *
     *  addresses: [
     *      {
     *          category: home/postal/office,
     *          payload: optional
     *      },
     *      {
     *       .
     *       }
     *  ]
     *
     *
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/signup', 'AuthenticationController@signup');

    /**
     * API: Sign Up
     *
     * Method: POST
     * Parameters:
     *  - first_name (required)
     *  - last_name (required)
     *  - mobile (optional)
     *  - email (required)
     *  - password (required)
     *  - password_confirmation (required)
     *  - membership (optional) : normal
     *  - photo (optional) : file
     *  - addresses (optional) : json
     *  - card_number (optional) : FOR CALink
     *
     *  addresses: [
     *      {
     *          category: home/postal/office,
     *          payload: optional
     *      },
     *      {
     *       .
     *       }
     *  ]
     *
     *
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/signupMatch', 'AuthenticationController@signupMatch');

    /**
     * API: Sign Up IGT
     *
     * Method: POST
     * Parameters:
     *  - first_name (required)
     *  - last_name (required)
     *  - mobile (optional)
     *  - email (required)
     *  - password (required)
     *  - password_confirmation (required)
     *  - membership (optional) : normal
     *  - photo (optional) : file
     *  - addresses (optional) : json
     *  - card_number (optional) : FOR CALink
     *
     *  addresses: [
     *      {
     *          category: home/postal/office,
     *          payload: optional
     *      },
     *      {
     *       .
     *       }
     *  ]
     *
     *
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/signupIGT', 'AuthenticationController@signupIGT');

    /**
     * API: Sign Up Odyssey
     *
     * Method: POST
     * Parameters:
     *  - first_name (required)
     *  - last_name (required)
     *  - mobile (optional)
     *  - email (required)
     *  - password (required)
     *  - password_confirmation (required)
     *  - membership (optional) : normal
     *  - photo (optional) : file
     *  - addresses (optional) : json
     *  - card_number (optional) : FOR CALink
     *
     *  addresses: [
     *      {
     *          category: home/postal/office,
     *          payload: optional
     *      },
     *      {
     *       .
     *       }
     *  ]
     *
     *
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/signupOdyssey', 'AuthenticationController@signupOdyssey');

    /**
     * API: Sign Up or Sign In for social
     *
     * Method: POST
     * Parameters:
     *  - first_name (required)
     *  - last_name (required)
     *  - mobile (optional)
     *  - email (required)
     *  - type (required) eq: facebook / gmail / twitter / linkedln
     *
     * Returns:
     *  - jwt_token
     */
    Route::post('/social/login', 'AuthenticationController@socialLogin');

    /**
     * API: Confirm Email
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('confirm/{token}', 'AuthenticationController@confirm');

    /**
     * API: Confirm Email
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('confirmMatch/{token}', 'AuthenticationController@confirmMatch');

    /**
     * API: Confirm Email IGT
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('confirmIGT/{token}', 'AuthenticationController@confirmIGT');

    /**
     * API: Confirm Email Odyssey
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('confirmOdyssey/{token}', 'AuthenticationController@confirmOdyssey');

    /**
     * API: Confirm Change Email 
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('confirmChangeEmail/{token}', 'AuthenticationController@confirmChangeEmail');

    /**
     * API: confirm Mobile
     *
     * Method: POST
     */
    Route::get('/confirmMobile/{token}', 'AuthenticationController@confirmMobile');

    /**
     * API: Send Reset Password Request
     *
     * Method: GET change to POST
     * Parameter:
     *  - email (required)
     */
    Route::post('/reset', 'ForgotPasswordController@resetPasswordRequest');

    /**
     * API: Perform Reset Password Request
     *
     * Method: POST
     * Parameters:
     *  - {token} (required) >> on the link
     *  - password (required)
     */
    Route::post('/reset/{token}', 'ForgotPasswordController@resetPassword');

    /**
     * API: Send Reset Password Request
     *
     * Method: GET
     * Parameter:
     *  - email (required)
     */
    Route::post('/resetAdminPassword', 'ForgotPasswordController@resetAdminPasswordRequest');

    //  /**
    //   * API: Send Verification Code for Reset Password using sms
    //   *
    //   * Method: GET
    //   * Parameters:
    //   *  - mobile (required)
    //   *
    //   */
    //  Route::get('/mobile/reset', 'ForgotPasswordController@sendForgotPasswordVerificationCode');

    //  /**
    //   * API: Verify the code
    //   *
    //   * Method: GET
    //   * Parameters:
    //   *  - code (required)
    //   *
    //   * Returns:
    //   *  - token (md5 token on the for resetting password url)
    //   */
    //  Route::post('/mobile/reset/verify', 'ForgotPasswordController@verifyCode');

    /**
     * API: open unSubscribe friend referral form
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('unsubscribe/{token}', 'AuthenticationController@showUnsubscribeFFForm');

    /**
     * API: unSubscribe
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::post('unsubscribe/{token}', 'AuthenticationController@unsubscribe');

    /**
     * API: open unSubscribe mail form
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('unsubscribeMail/{token}', 'AuthenticationController@showUnsubscribeForm');

    /**
     * API: unSubscribe mail
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::post('unsubscribeMail/{token}', 'AuthenticationController@unsubscribeMail');

    /**
     * API: show accept friend referral form
     *
     * Method: GET
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::get('acceptReferral/{token}', 'AuthenticationController@showFriendReferralForm');

    /**
     * API: accept Referral
     *
     * Method: POST
     * Parameters:
     *  - {token} (required) >> on the link
     */
    Route::post('confirmReferral/{token}', 'AuthenticationController@acceptFriendReferral');

    /**
     * Return tier options
     *
     * Method: GET change to POST
     */
    Route::post('/tiers', 'Member\TierController@index');

    /**
     * Return tier and signup options
     *
     * Method: GET change to POST
     */
    Route::post('/userProfileSetting', 'Member\TierController@index');

    /**
     * Return App Layout settings
     *
     * Method: GET change to POST
     */
    Route::post('/appSettings', 'Member\SettingController@layoutSettings');


    /**
     * API: Return hour and location
     *
     * Method: GET change to POST
     */
    Route::post('/gamingsetting', 'Member\SettingController@gamingSetting');


    /**
     * API: Return hour and location
     *
     * Method: GET
     */
    Route::get('/gamingstatelist/{countryID}', 'Member\SettingController@gamingStateList');

    /**
     * Return receipt
     */
    Route::get('/receipt/{token}', 'Member\TransactionController@generateReceipt');

    /**
     * Return receipt
     */
    Route::get('/receiptBooking/{token}', 'Member\TransactionController@generateReceiptBooking');

    /**
     * Return ticket
     */
    Route::get('/ticket/{token}', 'Member\UserVoucherController@generateTicket');

    /**
     * Return ticket
     */
    Route::get('/ticketBooking/{token}', 'Member\UserVoucherController@generateTicketBooking');

    /**
     * Return giftCertificate
     */
    Route::get('/giftCertificate/{token}', 'Member\UserVoucherController@generateGiftCertificate');

    /**
     * Return giftCertificate
     */
    Route::get('/giftCertificateBooking/{token}', 'Member\UserVoucherController@generateGiftCertificateBooking');

    /**
     * API: Return about
     *
     * Method: GET change to POST
     */
    Route::post('/about', 'Member\SettingController@about');

    /**
     * API: Return legals
     *
     * Method: GET change to POST
     */
    Route::post('/legals', 'Member\SettingController@legals');

    /**
    * API: Return Imports
    *
    */
    Route::post('/import', 'Admin\MemberController@importUsers');

    /**
    * API: Return Imports Onesignal
    *
    */
    Route::post('/importOnesignal', 'Admin\MemberController@importUsersOnesignal');

    /**
    * API: Return dummyMember
    *
    */
    Route::post('/dummyMember', 'Admin\MemberController@dummyMember');

    /**
    * API: Return confirmDummyMember
    *
    */
    Route::post('/confirmDummyMember', 'Admin\MemberController@confirmDummyMember');

    /**
     * API: trigger matching isBepozAccountCreated
     *
     * Method: GET change to POST
     */
    Route::post('/isBepozAccountCreatedManual', 'Member\UserController@isBepozAccountCreated');

    /*
    |--------------------------------------------------------------------------
    | Admin API+ Routes
    |--------------------------------------------------------------------------
    |
    | Require 'admin' privilege to access the API
    | From these APIs, user can perform CRUD requests.
    |
    |
    */

    /**
     * API: for admin user only
     *
     * Parameters:
     *  - token (required) >> JWT token
     */
    Route::group(['prefix' => 'admin', 'middleware' => ['jwt.auth', 'role.before', 'role.after'], 'roles' => ['admin', 'staff', 'bepoz_admin']], function () {


        //--------------------------------------------------------------------------
        // GENERATE OTP
        //--------------------------------------------------------------------------
        Route::post('/generateOTP', 'Admin\OTPController@generateOTP');

        //--------------------------------------------------------------------------
        // VERIFY OTP
        //--------------------------------------------------------------------------
        Route::post('/verifyOTP', 'Admin\OTPController@verifyOTP');

        //--------------------------------------------------------------------------
        // GOOGLE AUTHENTICATOR
        //--------------------------------------------------------------------------
        Route::post('/requestURI', 'Admin\OTPController@requestURI');

        /*
            |--------------------------------------------------------------------------
            | Validate Token
            |--------------------------------------------------------------------------
            */

        Route::post('/validateToken', 'Admin\UserController@validateJWTToken');

        /*
        |--------------------------------------------------------------------------
        | User
        |--------------------------------------------------------------------------
        */


        /**
         * API: show all users
         *
         * Method: GET
         */
        Route::get('/user', 'Admin\UserController@index')->middleware(['access:user_read']);

        /*
        |--------------------------------------------------------------------------
        | Role Controls
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all role controls
         *
         * Method: GET
         */
        Route::get('/role', 'Admin\RoleControlController@index')->middleware(['access:role_read']);

        //  /**
        //   * API: create a role
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - name (required)
        //   *  - description (required)
        //   *  - payload (required)
        //   */
        //  Route::post('/role', 'Admin\RoleControlController@store')->middleware(['access:role_create']);

        //  /**
        //   * API: delete a role
        //   *
        //   * Method: DELETE
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   */
        Route::delete('/role/{id}', 'Admin\RoleControlController@destroy')->middleware(['access:role_delete']);

        /**
         * API: retrieve a role
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/role/{id}', 'Admin\RoleControlController@show')->middleware(['access:role_read']);

        /**
         * API: update a role
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (required)
         *  - description (required)
         *  - payload (required)
         */
        Route::put('/role/{id}', 'Admin\RoleControlController@update')->middleware(['access:role_update']);


        /*
        |--------------------------------------------------------------------------
        | Listing Types
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all listing types
         *
         * Method: GET
         */
        Route::get('/listingType', 'Admin\ListingTypeController@index');

        /**
         * API: create a listing type
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - type (required)
         *  - display (required)
         *  - external (required)
         *  - multiple (required)
         */
        Route::post('/listingType', 'Admin\ListingTypeController@store')->middleware(['access:root_access']);

        /**
         * API: delete a listing type
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/listingType/{id}', 'Admin\ListingTypeController@destroy')->middleware(['access:root_access']);

        /**
         * API: retrieve listing type
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/listingType/{id}', 'Admin\ListingTypeController@show');

        /**
         * API: update a listing type
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (required)
         *  - type (required)
         *  - display (required)
         *  - external (required)
         *  - multiple (required)
         */
        Route::put('/listingType/{id}', 'Admin\ListingTypeController@update')->middleware(['access:root_access']);


        /**
         * API: hide a listing type
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         */
        Route::post('/listingTypeHide', 'Admin\ListingTypeController@hide')->middleware(['access:root_access']);

        /**
         * API: unHide a listing type
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         */
        Route::post('/listingTypeUnHide', 'Admin\ListingTypeController@unHide')->middleware(['access:root_access']);
        /*
        |--------------------------------------------------------------------------
        | Listing Schedules
        |--------------------------------------------------------------------------
        */

        //  /**
        //   * API: show all listing schedules
        //   *
        //   * Method: GET
        //   */
        //  Route::get('/listingSchedule', 'Admin\ListingScheduleController@index');

        //  /**
        //   * API: retrieve data for a specific schedule
        //   *
        //   * Method: GET
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   */
        //  Route::get('/listingSchedule/{id}', 'Admin\ListingScheduleController@show');

        //  /**
        //   * API: retrieve data for editing a specific schedule
        //   *
        //   * Method: GET
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   */
        //  Route::get('/listingSchedule/{id}/edit', 'Admin\ListingScheduleController@edit');

        //  /**
        //   * API: create a schedule
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - listing_id (required)
        //   *  - status (optional) >> active / inactive
        //   *  - date_start (required)
        //   *  - date_end (required)
        //   *  - date_timezone (required)
        //   */
        //  Route::post('/listingSchedule', 'Admin\ListingScheduleController@store');

        //  /**
        //   * API: update a schedule
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   *  - status (optional) >> active / inactive
        //   *  - date_start (required)
        //   *  - date_end (required)
        //   *  - date_timezone (required)
        //   */
        //  Route::post('/listingSchedule/{id}', 'Admin\ListingScheduleController@update');

        //  /**
        //   * API: delete a schedule
        //   *
        //   * Method: DELETE
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   */
        //  Route::delete('/listingSchedule/{id}', 'Admin\ListingScheduleController@destroy');

        /*
        |--------------------------------------------------------------------------
        | Reports
        |--------------------------------------------------------------------------
        */

        Route::get('/systemLogs', 'Admin\ReportController@createSystemLogs')->middleware(['access:log_read']);

        Route::post('/clearLogs', 'Admin\ReportController@clearSystemLogs')->middleware(['access:log_delete']);

        Route::get('/getBepozLogs', 'Admin\ReportController@getBepozLogs')->middleware(['access:log_read']);

        Route::get('/getGamingLogs', 'Admin\ReportController@getGamingLogs')->middleware(['access:log_read']);

        Route::post('/registeredMembersReport', 'Admin\ReportController@getRegisteredMembers')->middleware(['access:report_read']);

        Route::post('/previewReport', 'Admin\ReportController@createPreview')->middleware(['access:report_read']);

        Route::post('/generateSurveyReport', 'Admin\SurveyController@generateSurveyReport')->middleware(['access:report_export']);

        Route::get('/getExportSurvey', 'Admin\SurveyController@getExportSurvey')->middleware(['access:report_export']);

        /*
        |--------------------------------------------------------------------------
        | Mail Logs
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all mail logs
         *
         * Method: GET
         */
        Route::get('/paginateMailLog', 'Admin\MandrillSettingController@paginateLogs')->middleware(['access:report_read']);

        /*
        |--------------------------------------------------------------------------
        | Roles (Controls)
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all roles
         *
         * Method: GET
         * Parameters:
         *  - type_id (optional) -> filter the result
         */
        Route::get('/role', 'Admin\RoleControlController@index')->middleware(['access:role_read']);

        /**
         * API: show all roles (paginate)
         *
         * Method: GET
         */
        Route::get('/paginateRole', 'Admin\RoleControlController@paginateRole')->middleware(['access:role_read']);

        /*
        |--------------------------------------------------------------------------
        | Listings
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all listings
         *
         * Method: GET
         * Parameters:
         *  - type_id (optional) -> filter the result
         */
        Route::get('/listing', 'Admin\ListingController@index')->middleware(['access:listing_read']);

        /**
         * API: show all listings
         *
         * Method: GET
         * Parameters:
         *  - type_id (optional) -> filter the result
         */
        Route::get('/paginateListing', 'Admin\ListingController@paginateListing')->middleware(['access:listing_read']);

        /**
         * API: (advanced) search listings
         *
         * Method: POST
         * Parameters:
         *  - keyword (optional)
         *  - start_date (optional)
         *  - end_date (optional)
         *  - status (optional)
         *  - type_id (optional) -> filter the result
         */
        Route::post('/searchListing', 'Admin\ListingController@search')->middleware(['access:listing_read']);

        //  /**
        //   * API: preview  listings
        //   * >> display based on schedules
        //   *
        //   * Method: GET
        //   * Parameters:
        //   *  - display_id (required)
        //   */
        //  Route::get('/previewListing', 'Member\ListingController@index');

        /**
         * API: retrieve data for a specific event
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/listing/{id}', 'Admin\ListingController@show')->middleware(['access:listing_read']);

        //  /**
        //   * API: retrieve data for editing a specific event
        //   *
        //   * Method: GET
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   */
        //  Route::get('/listing/{id}/edit', 'Admin\ListingController@edit');

        /**
         * API: create a listing
         *
         * Method: POST
         * Parameters:
         *  - listing_type_id (required)
         *  - name (required)
         *  - heading (required)
         *  - comment (required)
         *  - desc_short (required)
         *  - desc_long (optional)
         *  - image_banner (optional) >> BLOB
         *  - image_square (optional) >> BLOB
         *  - status (required) >> draft / active / inactive
         *  - date_start (optional)
         *  - date_end (optional)
         *  - date_timezone (required when you specify date_start and date_end)
         *  - schedules (optional)
         *  - product (optional)
         */
        Route::post('/listing', 'Admin\ListingController@store')->middleware(['access:listing_create']);

        /**
         * API: update a listing
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - listing_type_id (required)
         *  - name (required)
         *  - heading (required)
         *  - comment (required)
         *  - desc_short (required)
         *  - desc_long (optional)
         *  - image_banner (optional) >> BLOB
         *  - image_square (optional) >> BLOB
         *  - status (required) >> draft / active / inactive
         *  - date_start (optional)
         *  - date_end (optional)
         *  - date_timezone (required when you specify date_start and date_end)
         *  - schedules (optional)
         *  - products (optional)
         */
        Route::post('/listing/{id}', 'Admin\ListingController@update')->middleware(['access:listing_update']);

        /**
         * API: delete a listing
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/listing/{id}', 'Admin\ListingController@destroy')->middleware(['access:listing_delete']);


        /**
         * API: Change listing status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - status (required)
         */
        Route::post('/changeListingStatus', 'Admin\ListingController@changeStatus')->middleware(['access:listing_update']);

        /**
         * API: duplicate listing
         *
         * Method: POST
         * Parameters:
         *  - listing_id (required)
         */
        Route::post('/duplicateListing', 'Admin\ListingController@duplicateListing')->middleware(['access:listing_create']);

        /**
         * API: Get listing count
         *
         * Method: GET
         */
        Route::get('/countListing', 'Admin\ListingController@countListing');

        /**
         * API: Check Product on Listing
         *
         * Method: POST
         */
        Route::post('/checkListingProduct', 'Admin\ListingController@checkListingProduct');

        /**
         * API: show all active listings
         *
         * Method: GET
         * Parameters:
         *  - type_id (optional) -> filter the result
         */
        Route::get('/activeListing', 'Admin\ListingController@activeListing')->middleware(['access:listing_read']);

        /**
         * API: Arrange Display Order for Listing
         *
         * Method: POST
         */
        Route::post('/displayOrder', 'Admin\ListingController@displayOrder');


        //  /**
        //   * API: Get export
        //   *
        //   * Method: GET
        //   */
        //  Route::get('/exportListing', 'Admin\ListingController@exportListing');

        /*
        |--------------------------------------------------------------------------
        | ListingTag
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all ListingTags
         *
         * Method: GET
         */
        Route::get('/listingTag', 'Admin\ListingTagController@index')->middleware(['access:listing_read']);

        /**
         * API: show all ListingTags (paginated)
         *
         * Method: GET
         */
        Route::get('/paginateListingTag', 'Admin\ListingTagController@paginateListingTag')->middleware(['access:listing_read']);

        /**
         * API: create a ListingTag
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - location (required)
         *  - ip_address (required)
         *  - mac (required)
         */
        Route::post('/listingTag', 'Admin\ListingTagController@store')->middleware(['access:listing_create']);

        /**
         * API: delete a ListingTag
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/listingTag/{id}', 'Admin\ListingTagController@destroy')->middleware(['access:listing_delete']);

        /**
         * API: search ListingTag
         *
         * Method: POST
         * Parameters:
         *  - keyword (required)
         */
        Route::post('/searchListingTag', 'Admin\ListingTagController@search')->middleware(['access:listing_read']);
        
        /**
         * API: retrieve ListingTag
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/listingTag/{id}', 'Admin\ListingTagController@show')->middleware(['access:listing_read']);

        /**
         * API: update a ListingTag
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - location (optional)
         *  - ip_address (optional)
         *  - mac (optional)
         */
        Route::put('/listingTag/{id}', 'Admin\ListingTagController@update')->middleware(['access:listing_update']);

        /**
         * API: Change ListingTag status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/changeListingTagStatus', 'Admin\ListingTagController@changeStatus')->middleware(['access:listing_update']);

        /**
         * API: Change ListingTag status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/listingTagDisplayOrder', 'Admin\ListingTagController@displayOrder')->middleware(['access:listing_update']);

        /*
        |--------------------------------------------------------------------------
        | Schedule ListingTag
        |--------------------------------------------------------------------------
        */

        //  /**
        //   * API: create a ListingTag-schedule
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - listing_schedule_id (required)
        //   *  - ListingTag_id (required)
        //   */
        //  Route::post('/scheduleListingTag', 'Admin\ScheduleListingTagController@store');


        /*
        |--------------------------------------------------------------------------
        | Listing Pivot Tag
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all ListingPivotTags
         *
         * Method: GET
         */
        Route::get('/listingPivotTag', 'Admin\ListingPivotTagController@index')->middleware(['access:listing_read']);

        /**
         * API: create a ListingPivotTag
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - location (required)
         *  - ip_address (required)
         *  - mac (required)
         */
        Route::post('/listingPivotTag', 'Admin\ListingPivotTagController@store')->middleware(['access:listing_create']);

        /**
         * API: delete a ListingPivotTag
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/listingPivotTag/{id}', 'Admin\ListingPivotTagController@destroy')->middleware(['access:listing_delete']);

        /**
         * API: search ListingPivotTag
         *
         * Method: POST
         * Parameters:
         *  - keyword (required)
         */
        Route::post('/searchListingPivotTag', 'Admin\ListingPivotTagController@search')->middleware(['access:listing_read']);
        
        /**
         * API: retrieve ListingPivotTag
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/listingPivotTag/{id}', 'Admin\ListingPivotTagController@show')->middleware(['access:listing_read']);
        
        /**
         * API: update a ListingPivotTag
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - location (optional)
         *  - ip_address (optional)
         *  - mac (optional)
         */
        Route::put('/listingPivotTag/{id}', 'Admin\ListingPivotTagController@update')->middleware(['access:listing_update']);

        /**
         * API: Change ListingPivotTag status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/changeListingPivotTagStatus', 'Admin\ListingPivotTagController@changeStatus')->middleware(['access:listing_update']);

        /*
        |--------------------------------------------------------------------------
        | Schedule ListingPivotTag
        |--------------------------------------------------------------------------
        */

        // /**
        //   * API: create a ListingPivotTag-schedule
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - listing_schedule_id (required)
        //   *  - ListingPivotTag_id (required)
        //   */
        // Route::post('/scheduleListingPivotTag', 'Admin\ScheduleListingPivotTagController@store');

        /*
        |--------------------------------------------------------------------------
        | Venue
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all venues
         *
         * Method: GET
         */
        Route::get('/venue', 'Admin\VenueController@index')->middleware(['access:venue_read']);

        /**
         * API: show all venues (paginated)
         *
         * Method: GET
         */
        Route::get('/paginateVenue', 'Admin\VenueController@paginateVenue')->middleware(['access:venue_read']);

        /**
         * API: create a venue
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - location (required)
         *  - ip_address (required)
         *  - mac (required)
         */
        Route::post('/venue', 'Admin\VenueController@store')->middleware(['access:venue_create']);

        /**
         * API: delete a venue
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/venue/{id}', 'Admin\VenueController@destroy')->middleware(['access:venue_delete']);

        /**
         * API: search venue
         *
         * Method: POST
         * Parameters:
         *  - keyword (required)
         */
        Route::post('/searchVenue', 'Admin\VenueController@search')->middleware(['access:venue_read']);

        /**
         * API: retrieve venue
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/venue/{id}', 'Admin\VenueController@show')->middleware(['access:venue_read']);

        /**
         * API: update a venue
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - location (optional)
         *  - ip_address (optional)
         *  - mac (optional)
         */
        Route::put('/venue/{id}', 'Admin\VenueController@update')->middleware(['access:venue_update']);

        /**
         * API: Change venue status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/changeVenueStatus', 'Admin\VenueController@changeStatus')->middleware(['access:venue_update']);

        /**
         * API: Change venue status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - display_order (required, array)
         */
        Route::post('/allVenueDisplayOrder', 'Admin\VenueController@allVenueDisplayOrder')->middleware(['access:venue_read']);

        /**
         * API: Change venue Display Order 
         *
         * Method: POST
         * Parameters:
         *  - display_order (required, array)
         */
        Route::post('/venueDisplayOrder', 'Admin\VenueController@displayOrder')->middleware(['access:venue_update']);


        /*
        |--------------------------------------------------------------------------
        | Schedule Venue
        |--------------------------------------------------------------------------
        */

        //  /**
        //   * API: create a venue-schedule
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - listing_schedule_id (required)
        //   *  - venue_id (required)
        //   */
        //  Route::post('/scheduleVenue', 'Admin\ScheduleVenueController@store');


        /*
        |--------------------------------------------------------------------------
        | VenueTag
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all VenueTags
         *
         * Method: GET
         */
        Route::get('/venueTag', 'Admin\VenueTagController@index')->middleware(['access:venue_read']);

        /**
         * API: show all Venue Tags With Pivot
         *
         * Method: GET
         */
        Route::get('/venueTagWithPivot', 'Admin\VenueTagController@venueTagWithPivot')->middleware(['access:venue_read']);

        /**
         * API: show all VenueTags (paginated)
         *
         * Method: GET
         */
        Route::get('/paginateVenueTag', 'Admin\VenueTagController@paginateVenueTag')->middleware(['access:venue_read']);

        /**
         * API: create a VenueTag
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - location (required)
         *  - ip_address (required)
         *  - mac (required)
         */
        Route::post('/venueTag', 'Admin\VenueTagController@store')->middleware(['access:venue_create']);

        /**
         * API: delete a VenueTag
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/venueTag/{id}', 'Admin\VenueTagController@destroy')->middleware(['access:venue_delete']);

        /**
         * API: search VenueTag
         *
         * Method: POST
         * Parameters:
         *  - keyword (required)
         */
        Route::post('/searchVenueTag', 'Admin\VenueTagController@search')->middleware(['access:venue_read']);

        /**
         * API: retrieve VenueTag
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/venueTag/{id}', 'Admin\VenueTagController@show')->middleware(['access:venue_read']);

        /**
         * API: update a VenueTag
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - location (optional)
         *  - ip_address (optional)
         *  - mac (optional)
         */
        Route::put('/venueTag/{id}', 'Admin\VenueTagController@update')->middleware(['access:venue_update']);

        /**
         * API: Change VenueTag status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/changeVenueTagStatus', 'Admin\VenueTagController@changeStatus')->middleware(['access:venue_update']);

        /**
         * API: Change VenueTag Hide In App -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/changeVenueTagHideInApp', 'Admin\VenueTagController@changeHideInApp')->middleware(['access:venue_update']);

        /**
         * API: Change VenueTag status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/venueTagDisplayOrder', 'Admin\VenueTagController@displayOrder')->middleware(['access:venue_update']);

        /**
         * API: venue Tag Search Venue ID
         *
         * Method: POST
         * Parameters:
         *  - venue_id (required)
         */
        Route::post('/venueTagSearchVenueID', 'Admin\VenueTagController@venueTagSearchVenueID')->middleware(['access:venue_read']);

        /*
        |--------------------------------------------------------------------------
        | Schedule VenueTag
        |--------------------------------------------------------------------------
        */

        //  /**
        //   * API: create a VenueTag-schedule
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - listing_schedule_id (required)
        //   *  - VenueTag_id (required)
        //   */
        //  Route::post('/scheduleVenueTag', 'Admin\ScheduleVenueTagController@store');


        /*
        |--------------------------------------------------------------------------
        | Venue Pivot Tag
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all VenuePivotTags
         *
         * Method: GET
         */
        Route::get('/venuePivotTag', 'Admin\VenuePivotTagController@index')->middleware(['access:venue_read']);

        /**
         * API: create a VenuePivotTag
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - location (required)
         *  - ip_address (required)
         *  - mac (required)
         */
        Route::post('/venuePivotTag', 'Admin\VenuePivotTagController@store')->middleware(['access:venue_create']);

        /**
         * API: delete a VenuePivotTag
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/venuePivotTag/{id}', 'Admin\VenuePivotTagController@destroy')->middleware(['access:venue_delete']);

        /**
         * API: search VenuePivotTag
         *
         * Method: POST
         * Parameters:
         *  - keyword (required)
         */
        Route::post('/searchVenuePivotTag', 'Admin\VenuePivotTagController@search')->middleware(['access:venue_read']);

        /**
         * API: retrieve VenuePivotTag
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/venuePivotTag/{id}', 'Admin\VenuePivotTagController@show')->middleware(['access:venue_read']);

        /**
         * API: update a VenuePivotTag
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - location (optional)
         *  - ip_address (optional)
         *  - mac (optional)
         */
        Route::put('/venuePivotTag/{id}', 'Admin\VenuePivotTagController@update')->middleware(['access:venue_update']);

        /**
         * API: Change VenuePivotTag status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/changeVenuePivotTagStatus', 'Admin\VenuePivotTagController@changeStatus')->middleware(['access:venue_update']);

        /*
        |--------------------------------------------------------------------------
        | Schedule VenuePivotTag
        |--------------------------------------------------------------------------
        */

        // /**
        //   * API: create a VenuePivotTag-schedule
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - listing_schedule_id (required)
        //   *  - VenuePivotTag_id (required)
        //   */
        // Route::post('/scheduleVenuePivotTag', 'Admin\ScheduleVenuePivotTagController@store');


        /*
        |--------------------------------------------------------------------------
        | Tag
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all tags
         *
         * Method: GET
         */
        Route::get('/tag', 'Admin\TagController@index');

        /**
         * API: create a tag
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - style (optiona | JSON)
         */
        Route::post('/tag', 'Admin\TagController@store');

        /**
         * API: delete a tag
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/tag/{id}', 'Admin\TagController@destroy');

        /**
         * API: retrieve tag
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/tag/{id}', 'Admin\TagController@show');

        /**
         * API: update a tag
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - style (optional | json)
         */
        Route::put('/tag/{id}', 'Admin\TagController@update');

        /*
        |--------------------------------------------------------------------------
        | Dashboard
        |--------------------------------------------------------------------------
        */
        Route::post('/newDashboard', 'Admin\DashboardController@newDashboard');

        Route::get('/getWeeklySales', 'Admin\DashboardController@getWeeklySales');

        Route::get('/getDailySales', 'Admin\DashboardController@getDailySales');

        Route::get('/getMonthlySales', 'Admin\DashboardController@getMonthlySales');

        Route::get('/getQuarterlySales', 'Admin\DashboardController@getQuarterlySales');

        Route::get('/getYearlySales', 'Admin\DashboardController@getYearlySales');

        Route::get('/getQuarterlyYearlySales', 'Admin\DashboardController@getQuarterlyYearlySales');

        Route::post('/getJoinedMembers', 'Admin\DashboardController@getJoinedMembers');

        Route::post('/getTransactionsThisMonth', 'Admin\DashboardController@getTransactionsThisMonth');

        Route::get('/retrievePromotions', 'Admin\DashboardController@retrievePromotions');

        Route::get('/getWeeklyEventCounts', 'Admin\DashboardController@getWeeklyEventCounts');

        Route::get('/getSpecialEventCounts', 'Admin\DashboardController@getSpecialEventCounts');

        Route::post('/getListingCounts', 'Admin\DashboardController@getListingCounts');

        Route::get('/getExportReport', 'Admin\DashboardController@getExportReport');

        Route::get('/getExportPromotions', 'Admin\DashboardController@getExportPromotions');

        /*
        |--------------------------------------------------------------------------
        | Members
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all members
         *
         * Method: GET
         */
        Route::get('/member', 'Admin\MemberController@index')->middleware(['access:member_read']);

        /**
         * API: create a member
         *
         * Method: POST
         * Parameters:
         *  - last_name (required)
         *  - first_name (required)
         *  - phone (optional)
         *  - mobile (optional)
         *  - birthday (optional)
         *  - email (required)
         *  - password (required)
         *  - addresses (optional)
         *  - status (optional)
         *  - file (optional) >> profile photo BLOB
         */
        Route::post('/member', 'Admin\MemberController@create')->middleware(['access:member_create']);

        /**
         * API: update a member
         *
         * Method: POST
         * Parameters:
         *  - last_name (optional)
         *  - first_name (optional)
         *  - phone (optional)
         *  - mobile (optional)
         *  - birthday (optional)
         *  - email (optional)
         *  - password (optional)
         *  - addresses (optional)
         *  - status (optional)
         *  - file (optional) >> profile photo BLOB
         */
        Route::post('/member/{id}', 'Admin\MemberController@update')->middleware(['access:member_update']);

        /**
         * API: show specific member
         *
         * Method: GET
         */
        Route::get('/member/{id}', 'Admin\MemberController@show')->middleware(['access:member_read']);

        /**
         * API: show all transactions
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/member/{id}/transaction', 'Admin\MemberController@transaction')->middleware(['access:member_read']);

        /**
         * API: show all issue vouchers
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/member/{id}/voucher', 'Admin\MemberController@issuedVoucher')->middleware(['access:member_read']);

        //  /**
        //   * API: Change member status -> bulk mode
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - original_ids (required)
        //   *  - new_ids (required)
        //   */
        //  Route::post('/memberBulk', 'Admin\MemberController@bulk')->middleware(['access:member_update']);

        /**
         * API: Send confirmation emails again
         *
         * Method: POST
         */
        Route::post('/resendConfirmationEmails', 'Admin\MemberController@resendConfirmations')->middleware(['access:resend_confirmation']);

        /**
         * API: Send confirmation emails again per member
         *
         * Method: POST
         */
        Route::post('/resendConfirmationMember', 'Admin\MemberController@resendConfirmationMember')->middleware(['access:resend_confirmation']);

        /**
         * API: Inactive member
         *
         * Method: POST
         */
        Route::post('/inactiveMember', 'Admin\MemberController@inactiveMember')->middleware(['access:member_update']);

        /**
         * API: Member search
         *
         * Method: POST
         * Parameters:
         *  - first_name (optional)
         *  - last_name (optional)
         *  - email (optional)
         *  - mobile (optional)
         */
        Route::post('/memberSearch', 'Admin\MemberController@search')->middleware(['access:member_read']);

        /**
         * API: show all logs
         *
         * Method: GET
         */
        Route::get('/memberLogs', 'Admin\MemberController@paginateMemberLogs')->middleware(['access:log_read']);

        /**
         * API: show all prizePromotion
         *
         * Method: GET
         */
        Route::get('/prizePromotion', 'Admin\MemberController@prizePromotion');

        /**
         * API: trigger 1 Member Custom Field update Bepoz with cron
         *
         * Method: GET
         */
        Route::get('/triggerMemberCustomField', 'Admin\MemberController@triggerMemberCustomField')->middleware(['access:member_update']);

        /**
         * API: trigger All Member Custom Field update Bepoz with cron
         *
         * Method: GET
         */
        Route::get('/triggerAllMemberCustomField', 'Admin\MemberController@triggerAllMemberCustomField')->middleware(['access:member_update']);

        /**
         * API: Member Suspend
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         */
        Route::post('/memberSuspend', 'Admin\MemberController@memberSuspend')->middleware(['access:member_update']);

        /*
        |--------------------------------------------------------------------------
        | Staff
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all staff
         *
         * Method: GET
         */
        Route::get('/staff', 'Admin\StaffController@index')->middleware(['access:staff_read']);

        /**
         * API: show all staff
         *
         * Method: GET
         */
        Route::get('/paginateStaff', 'Admin\StaffController@paginateStaff')->middleware(['access:staff_read']);

        /**
         * API: create a member
         *
         * Method: POST
         * Parameters:
         *  - last_name (required)
         *  - first_name (required)
         *  - phone (optional)
         *  - mobile (optional)
         *  - birthday (optional)
         *  - email (required)
         *  - password (required)
         *  - active (required)
         *  - file (optional) >> profile photo BLOB
         */
        Route::post('/staff', 'Admin\StaffController@create')->middleware(['access:staff_create']);

        /**
         * API: show specific staff
         *
         * Method: GET
         */
        Route::get('/staff/{id}', 'Admin\StaffController@show')->middleware(['access:staff_read']);

        /**
         * API: update a staff
         *
         * Method: POST
         * Parameters:
         *  - id
         *  - last_name (required)
         *  - first_name (required)
         *  - phone (optional)
         *  - mobile (optional)
         *  - birthday (optional)
         *  - email (required)
         *  - password (required)
         *  - active (required)
         *  - file (optional) >> profile photo BLOB
         */
        Route::post('/staff/{id}', 'Admin\StaffController@update')->middleware(['access:staff_update']);


        /**
         * API: delete a staff
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/staff/{id}', 'Admin\StaffController@destroy')->middleware(['access:staff_delete']);

        /**
         * API: Change staff status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - active (required)
         */
        Route::post('/changeStaffStatus', 'Admin\StaffController@changeStatus')->middleware(['access:staff_update']);
        /*
        |--------------------------------------------------------------------------
        | Addresses
        |--------------------------------------------------------------------------
        */

        //  /**
        //   * API: show all addresses
        //   *
        //   * Method: GET
        //   */
        //  Route::get('/address', 'Admin\AddressController@index');


        /**
         * API: show member addresses
         *
         * Method: GET
         */
        Route::get('/address/{id}', 'Admin\AddressController@show');

        /*
        |--------------------------------------------------------------------------
        | Products Types
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all product types
         *
         * Method: GET
         */
        Route::get('/productType', 'Admin\ProductTypeController@index');

        //  /**
        //   * API: create a product type
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - name (required)
        //   *  - description (optional)
        //   */
        //  Route::post('/productType', 'Admin\ProductTypeController@store');

        //  /**
        //   * API: delete a product type
        //   *
        //   * Method: DELETE
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   */
        //  Route::delete('/productType/{id}', 'Admin\ProductTypeController@destroy');

        /**
         * API: retrieve a product type
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/productType/{id}', 'Admin\ProductTypeController@show');

        //  /**
        //   * API: update a product type
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   *  - name (required)
        //   *  - description (optional)
        //   */
        //  Route::put('/productType/{id}', 'Admin\ProductTypeController@update');

        /*
        |--------------------------------------------------------------------------
        | Products
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all products
         *
         * Method: GET
         */
        Route::get('/product', 'Admin\ProductController@index')->middleware(['access:voucher_read']);

        /**
         * API: retrieve data for a specific product
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/product/{id}', 'Admin\ProductController@show')->middleware(['access:voucher_read']);

        /**
         * API: delete a product
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/product/{id}', 'Admin\ProductController@destroy')->middleware(['access:voucher_delete']);

        /**
         * API: create a product
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - desc_short (required)
         *  - desc_long (optional)
         *  - image (optional)
         *  - unit_price (optional)
         *  - point_price (required)
         *  - point_get (optional)
         *  - status (optional)
         *  - visible_from (optional)
         *  - visible_to (optional)
         */
        Route::post('/product', 'Admin\ProductController@store')->middleware(['access:voucher_read']);

        /**
         * API: update a product
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - desc_short (optional)
         *  - desc_long (optional)
         *  - image (optional)
         *  - unit_price (optional)
         *  - point_price (optional)
         *  - point_get (optional)
         *  - status (optional)
         *  - visible_from (optional)
         *  - visible_to (optional)
         */
        Route::post('/product/{id}', 'Admin\ProductController@update')->middleware(['access:voucher_update']);

        //  /**
        //   * API: Change product status -> bulk mode
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - original_ids (required)
        //   *  - new_ids (required)
        //   */
        //  Route::post('/productBulk', 'Admin\ProductController@bulk');

        //  /**
        //   * API: (advanced) search listings
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - keyword (optional)
        //   *  - status (optional)
        //   *  - product_type_id (optional) -> filter the result
        //   */
        //  Route::post('/searchProduct', 'Admin\ProductController@search');

        /**
         * API: Change product status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - status (required)
         */
        Route::post('/changeProductStatus', 'Admin\ProductController@changeStatus')->middleware(['access:voucher_update']);

        /**
         * API: show all products
         *
         * Method: GET
         */
        Route::get('/paginateProduct', 'Admin\ProductController@paginateProduct')->middleware(['access:voucher_read']);

        /**
         * API: duplicate product
         *
         * Method: POST
         * Parameters:
         *  - product_id (required)
         */
        Route::post('/duplicateProduct', 'Admin\ProductController@cloneProduct')->middleware(['access:voucher_create']);


        /*
        |--------------------------------------------------------------------------
        | Transactions
        |--------------------------------------------------------------------------
        */

        /**
         * API: retrieve all transaction data
         *
         * Method: GET
         */
        Route::get('/transaction', 'Admin\TransactionController@index')->middleware(['access:transaction_read']);

        /**
         * API: retrieve data for a specific transaction
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/transaction/{id}', 'Admin\TransactionController@show')->middleware(['access:transaction_read']);


        /**
         * API: download PDF
         *
         * Method: GET
         */
        Route::get('/invoice/{id}', 'Admin\TransactionController@generateInvoice')->middleware(['access:transaction_export']);

        /*
        |--------------------------------------------------------------------------
        | Settings
        |--------------------------------------------------------------------------
        */

        /**
         * API: retrieve all setting data
         *
         * Method: GET
         */
        Route::get('/setting', 'Admin\SettingController@index')->middleware(['access:setting_read']);

        /**
         * API: retrieve a specific setting data
         *
         * Method: GET
         */
        Route::get('/setting/{key}', 'Admin\SettingController@show')->middleware(['access:setting_read']);

        /**
         * API: retrieve all pattern data
         *
         * Method: GET
         */
        Route::get('/pattern', 'Admin\Setting\PatternController@index')->middleware(['access:setting_read']);

        /**
         * API: update setting
         *
         * Method: PUT
         * Parameters:
         *  - {id} (required) >> on the link
         *  - value (optional)
         */
        Route::put('/setting/{key}', 'Admin\SettingController@update')->middleware(['access:setting_update']);

        /**
         * API: update multiple settings
         *
         * Method: PUT
         * Parameters:
         *  - settings (required)
         */
        Route::post('/setting', 'Admin\SettingController@multipleUpdates')->middleware(['access:setting_update']);

        /**
         * API: Verify unlock key
         *
         * Method: PUT
         * Parameters:
         *  - settings (required)
         */
        Route::post('/unlockKey', 'Admin\SettingController@unlockKey')->middleware(['access:root_access']);

        /**
         * API: update multiple tier settings
         *
         * Method: PUT
         * Parameters:
         *  - settings (required)
         */
        Route::post('/tierSetting', 'Admin\TierController@tierSetting')->middleware(['access:root_access']);

        /**
         * API: retrieve all distinct progress and status
         *
         * Method: GET
         */
        Route::get('/distinctProgressStatus', 'Admin\SettingController@getDistinctProgressStatus')->middleware(['access:setting_read']);

        /**
         * API: upload gallery photo
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/uploadGalleryPhoto', 'Admin\SettingController@uploadImage')->middleware(['access:setting_update']);

        /**
         * API: upload friend referral background photo
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/uploadImageFFBackground', 'Admin\SettingController@uploadImageFFBackground')->middleware(['access:setting_update']);

        /**
         * API: upload invoice logo
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/uploadInvoiceLogo', 'Admin\SettingController@uploadInvoiceLogo')->middleware(['access:setting_update']);


        /**
         * API: upload invoice header logo
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/uploadInvoiceHeaderLogo', 'Admin\SettingController@uploadInvoiceHeaderLogo')->middleware(['access:setting_update']);

        /**
         * API: upload Confirmation logo
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/uploadConfirmationLogo', 'Admin\SettingController@uploadConfirmationLogo')->middleware(['access:setting_update']);

        /**
         * API: upload venue image
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/uploadVenueImage', 'Admin\VenueController@uploadVenueImage')->middleware(['access:venue_update']);

        /**
         * API: send helpdesk email
         *
         * Method: POST
         * Parameters:
         *  - from (required)
         *  - to (required)
         *  - cc
         *  - message (required)
         *  - subject (required)
         */
        Route::post('/sendHelpDesk', 'Admin\SettingController@sendHelpdeskEmail');

        /**
         * API: Clear stripe customer objects
         *
         */
        Route::post('/clearStripeCustomers', 'Admin\SettingController@clearStripeCustomers')->middleware(['access:root_access']);

        /**
         * API: download manual
         *
         * Method: GET
         */
        Route::get('/manual', 'Admin\SettingController@getDownload')->middleware(['access:setting_read']);

        /*
        |--------------------------------------------------------------------------
        | Friend Referrals
        |--------------------------------------------------------------------------
        */

        /**
         * API: retrieve all referral data
         *
         * Method: GET
         */
        Route::get('/paginateFriendReferrals', 'Admin\FriendReferralController@paginateFriendReferral')->middleware(['access:friend_referral_read']);

        /**
         * API: show who refer
         *
         * Method: POST
         */
        Route::post('/showFriendReferrals', 'Admin\FriendReferralController@show')->middleware(['access:friend_referral_read']);

        /*
        |--------------------------------------------------------------------------
        | Mandrill Settings
        |--------------------------------------------------------------------------
        */

        /**
         * API: retrieve all setting data
         *
         * Method: GET
         */
        Route::get('/mandrillSetting', 'Admin\MandrillSettingController@index')->middleware(['access:setting_read']);

        /**
         * API: retrieve a specific setting data
         *
         * Method: GET
         */
        Route::get('/mandrillSetting/{id}', 'Admin\MandrillSettingController@show')->middleware(['access:setting_read']);

        /**
         * API: update setting
         *
         * Method: PUT
         * Parameters:
         *  - {id} (required) >> on the link
         *  - value (optional)
         */
        Route::put('/mandrillSetting/{key}', 'Admin\MandrillSettingController@update')->middleware(['access:setting_update']);

        /*
        |--------------------------------------------------------------------------
        | Email
        |--------------------------------------------------------------------------
        */

        /**
         * API: retrieve all email data
         *
         * Method: GET
         */
        Route::get('/email', 'Admin\EmailController@index')->middleware(['access:setting_read']);

        /**
         * API: retrieve a specific setting data
         *
         * Method: GET
         */
        Route::get('/email/{id}', 'Admin\EmailController@show')->middleware(['access:setting_read']);

        /**
         * API: create setting
         *
         * Method: POST
         * Parameters:
         *  - name (required)
         *  - settings (required)
         */
        Route::post('/email', 'Admin\EmailController@store')->middleware(['access:setting_update']);

        /**
         * API: update setting
         *
         * Method: PUT
         * Parameters:
         *  - {id} (required) >> on the link
         *  - name (optional)
         *  - settings (optional)
         */
        Route::put('/email/{id}', 'Admin\EmailController@update')->middleware(['access:setting_update']);

        /**
         * API: delete an email setting
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/email/{id}', 'Admin\EmailController@destroy')->middleware(['access:setting_update']);


        /*
        |--------------------------------------------------------------------------
        | Survey
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all survey
         *
         * Method: GET
         */
        Route::get('/survey', 'Admin\SurveyController@index')->middleware(['access:survey_read']);

        /**
         * API: create a survey
         *
         * Method: POST
         * Parameters:
         *  - description (required)
         *  - title (required)
         *  - status (required)
         */
        Route::post('/survey', 'Admin\SurveyController@store')->middleware(['access:survey_create']);

        /**
         * API: delete a survey
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/survey/{id}', 'Admin\SurveyController@destroy')->middleware(['access:survey_delete']);

        /**
         * API: retrieve survey
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/survey/{id}', 'Admin\SurveyController@show')->middleware(['access:survey_read']);

        /**
         * API: update a survey
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - title (optional)
         *  - description (optional)
         *  - status (optional)
         */
        Route::put('/survey/{id}', 'Admin\SurveyController@update')->middleware(['access:survey_update']);

        /**
         * API: show all surveys
         *
         * Method: GET
         */
        Route::get('/paginateSurvey', 'Admin\SurveyController@paginateSurvey')->middleware(['access:survey_read']);


        /**
         * API: retrieve survey
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/surveyDetail/{id}', 'Admin\SurveyController@showDetails')->middleware(['access:survey_read']);

        /**
         * API: check a survey question
         *
         * Method: POST
         * Parameters:
         *  - survey_id (required)
         *  - question_id (required)
         */
        Route::post('/checkSurveyQuestion', 'Admin\SurveyController@checkQuestionSurvey')->middleware(['access:survey_read']);

        /**
         * API: check a survey
         *
         * Method: POST
         * Parameters:
         *  - survey_id (required)
         */
        Route::post('/checkSurvey', 'Admin\SurveyController@checkSurvey')->middleware(['access:survey_read']);

        /*
        |--------------------------------------------------------------------------
        | Survey: Question
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all questions
         *
         * Method: GET
         */
        Route::get('/question', 'Admin\QuestionController@index')->middleware(['access:survey_read']);

        /**
         * API: create a question
         *
         * Method: POST
         * Parameters:
         *  - question (required)
         *  - multiple_choice (required)
         */
        Route::post('/question', 'Admin\QuestionController@store')->middleware(['access:survey_create']);

        /**
         * API: delete a question
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/question/{id}', 'Admin\QuestionController@destroy')->middleware(['access:survey_delete']);

        /**
         * API: retrieve a question
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/question/{id}', 'Admin\QuestionController@show')->middleware(['access:survey_read']);

        /**
         * API: update a question
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - question (optional)
         *  - multiple_choice (optional)
         */
        Route::put('/question/{id}', 'Admin\QuestionController@update')->middleware(['access:survey_update']);

        /**
         * API: check a question
         *
         * Method: POST
         * Parameters:
         *  - question_id (required)
         */
        Route::post('/checkQuestion', 'Admin\QuestionController@checkQuestion')->middleware(['access:survey_read']);

        /*
        |--------------------------------------------------------------------------
        | Survey: Answer
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all answers
         *
         * Method: GET
         */
        Route::get('/answer', 'Admin\AnswerController@index')->middleware(['access:survey_read']);

        /**
         * API: create an answer
         *
         * Method: POST
         * Parameters:
         *  - answer (required)
         */
        Route::post('/answer', 'Admin\AnswerController@store')->middleware(['access:survey_create']);

        /**
         * API: delete an answer
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/answer/{id}', 'Admin\AnswerController@destroy')->middleware(['access:survey_delete']);

        /**
         * API: retrieve an answer
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/answer/{id}', 'Admin\AnswerController@show')->middleware(['access:survey_read']);

        /**
         * API: update a answer
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - answer (optional)
         */
        Route::put('/answer/{id}', 'Admin\AnswerController@update')->middleware(['access:survey_update']);

        /**
         * API: check a question
         *
         * Method: POST
         * Parameters:
         *  - question_id (required)
         */
        Route::post('/checkAnswer', 'Admin\AnswerController@checkAnswer')->middleware(['access:survey_read']);


        /*
        |--------------------------------------------------------------------------
        | Tier
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all tiers
         *
         * Method: GET
         */
        Route::get('/tier', 'Admin\TierController@index');

        /*
        |--------------------------------------------------------------------------
        | Exports
        |--------------------------------------------------------------------------
        */

        Route::get('/exportMember', 'Admin\ExportController@registeredMembers')->middleware(['access:report_export']);

        Route::get('/exportPreview', 'Admin\ExportController@downloadPreview')->middleware(['access:report_export']);


        /*
            |--------------------------------------------------------------------------
            | Listing Enquiry
            |--------------------------------------------------------------------------
            */

        /**
         * API: show all enquiries
         *
         * Method: GET
         */
        Route::get('/listingEnquiry', 'Admin\ListingEnquiryController@index')->middleware(['access:enquiry_read']);

        /**
         * API: delete an enquiry
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/listingEnquiry/{id}', 'Admin\ListingEnquiryController@destroy')->middleware(['access:enquiry_delete']);

        /**
         * API: retrieve single enquiry
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/listingEnquiry/{id}', 'Admin\ListingEnquiryController@show')->middleware(['access:enquiry_read']);

        /**
         * API: update an enquiry
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - answer (optional)
         *  - status (optional)
         */
        Route::put('/listingEnquiry/{id}', 'Admin\ListingEnquiryController@update')->middleware(['access:enquiry_update']);

        /**
         * API: mark it as done
         *
         * Method: POST
         * Parameters:
         *  - listing_id (required)
         *  - is_answered (required)
         */
        Route::post('/markItAsDone', 'Admin\ListingEnquiryController@markItAsDone')->middleware(['access:enquiry_update']);

        /**
         * API: save comment
         *
         * Method: POST
         * Parameters:
         *  - listing_id (required)
         *  - comment (required)
         */
        Route::post('/saveComment', 'Admin\ListingEnquiryController@saveComment')->middleware(['access:enquiry_update']);

        Route::get('/getExportEnquiry', 'Admin\ListingEnquiryController@getExportEnquiry');

        /*
        |--------------------------------------------------------------------------
        | FAQ
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all faq
         *
         * Method: GET
         */
        Route::get('/faq', 'Admin\FaqController@index')->middleware(['access:faq_read']);

        /**
         * API: create a faq
         *
         * Method: POST
         * Parameters:
         *  - question (required)
         *  - answer (required)
         *  - status (required)
         */
        Route::post('/faq', 'Admin\FaqController@store')->middleware(['access:faq_create']);

        /**
         * API: delete a faq
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/faq/{id}', 'Admin\FaqController@destroy')->middleware(['access:faq_delete']);

        /**
         * API: retrieve faq
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/faq/{id}', 'Admin\FaqController@show')->middleware(['access:faq_read']);

        /**
         * API: update a faq
         *
         * Method: POST
         * Parameters:
         *  - {id} (required) >> on the link
         *  - question (optional)
         *  - answer (optional)
         *  - status (optional)
         */
        Route::put('/faq/{id}', 'Admin\FaqController@update')->middleware(['access:faq_update']);

        /**
         * API: Change faq status -> single mode
         *
         * Method: POST
         * Parameters:
         *  - id (required)
         *  - status (required)
         */
        Route::post('/changeFaqStatus', 'Admin\FaqController@changeStatus')->middleware(['access:faq_update']);

        /*
        |--------------------------------------------------------------------------
        | Password
        |--------------------------------------------------------------------------
        */

        /**
         * API: change password
         *
         * Method: PUT
         * Parameters:
         *  - old_password (required)
         *  - new_password (required)
         *  - new_password_confirmation (required)
         *
         * Returns:
         *  - jwt_token
         */
        Route::put('/password', 'Admin\UserController@changePassword');

        /*
        |--------------------------------------------------------------------------
        | OneSignal
        |--------------------------------------------------------------------------
        */

        /**
         * API: Send push notification to a specific user
         *
         * Method: POST
         * Parameters:
         *  - member_id (required)
         *  - message (required)
         */
        Route::post('/pushNotification', 'Admin\OneSignalController@send')->middleware(['access:send_broadcast']);

        /**
         * API: Broadcast push notification to all active users
         *
         * Method: POST
         * Parameters:
         *  - message (required)
         */
        Route::post('/broadcastNotification', 'Admin\OneSignalController@broadcastMessage')->middleware(['access:send_broadcast']);

        /**
         * API: Broadcast push notification to group
         *
         * Method: POST
         * Parameters:
         *  - message (required)
         *  - tier_id (required)
         */
        Route::post('/broadcastGroupNotification', 'Admin\OneSignalController@broadcastGroupMessage')->middleware(['access:send_broadcast']);

        /*
        |--------------------------------------------------------------------------
        | System Notifications
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all notifications
         *
         * Method: GET
         */
        Route::get('/systemNotification', 'Admin\SystemNotificationController@index')->middleware(['access:notification_read']);


        /**
         * API: show all notifications (paginate)
         *
         * Method: GET
         */
        Route::get('/paginateSystemNotification', 'Admin\SystemNotificationController@paginateSystems')->middleware(['access:notification_read']);

        /**
         * API: create a SystemNotification
         *
         * Method: POST
         * Parameters:
         *  - message (required)
         *  - title (required)
         *  - action (required)
         *  - type (required)
         */
        Route::post('/systemNotification', 'Admin\SystemNotificationController@store')->middleware(['access:notification_create']);

        /**
         * API: update a SystemNotification
         *
         * Method: PUT
         * Parameters:
         *  - id (on the link)
         *  - message (required)
         *  - title (required)
         *  - action (required)
         *  - type (required)
         */
        Route::put('/systemNotification/{id}', 'Admin\SystemNotificationController@update')->middleware(['access:notification_update']);

        /**
         * API: delete a SystemNotification
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::delete('/systemNotification/{id}', 'Admin\SystemNotificationController@destroy')->middleware(['access:notification_delete']);

        /**
         * API: retrieve SystemNotification
         *
         * Method: GET
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::get('/systemNotification/{id}', 'Admin\SystemNotificationController@show')->middleware(['access:notification_read']);

        /**
         * API: Send push notification to a specific user
         *
         * Method: POST
         * Parameters:
         *  - member_id (required)
         *  - notification_id (required)
         */
        Route::post('/sendSystemNotification', 'Admin\MemberController@sendSystemNotification')->middleware(['access:send_broadcast']);

        /**
         * API: Send push notification to all
         *
         * Method: POST
         * Parameters:
         *  - notification_id (required)
         */
        Route::post('/sendBroadcastSystemNotification', 'Admin\MemberController@sendBroadcastSystemNotification')->middleware(['access:send_broadcast']);

        /**
         * API: Send push notification to group
         *
         * Method: POST
         * Parameters:
         *  - tier_id (required)
         *  - notification_id (required)
         */
        Route::post('/sendBroadcastGroupSystemNotification', 'Admin\MemberController@sendBroadcastGroupSystemNotification')->middleware(['access:send_broadcast']);

        /*
        |--------------------------------------------------------------------------
        | Notifications
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all notifications
         *
         * Method: GET
         */
        Route::get('/notification', 'Admin\NotificationController@index');

        /**
         * API: Clear all notifications
         *
         * Method: POST
         */
        Route::post('/clearNotification', 'Admin\NotificationController@clearAll');


        //////////////////////////////////////////////////////////////
        //                          BOOKING TICKET                  //
        //////////////////////////////////////////////////////////////

        /**
         * API: get Event List For Calendar
         *
         * Method: POST
         * Parameters:
         * - month (REQUIRED)
         * - year (REQUIRED)
         */
        Route::post('/getEventListForCalendar', 'Admin\BookingTicketController@getEventListForCalendar');

        /**
         * API: get Event List For Calendar
         *
         * Method: GET
         * Parameters:
         * - month (REQUIRED)
         * - year (REQUIRED)
         */
        Route::get('/getEventListForCalendar', 'Admin\BookingTicketController@getEventListForCalendar');

        /**
         * API: get Event List
         *
         * Method: POST
         */
        Route::post('/getEventList', 'Admin\BookingTicketController@getEventList');

        /**
         * API: get Event List
         *
         * Method: GET
         */
        Route::get('/getEventList', 'Admin\BookingTicketController@getEventList');

        /**
         * API: confirm Booking
         *
         * Method: POST
         * Parameters:
         * - order_token (REQUIRED)
         * - option: (REQUIRED)
         */
        Route::post('/confirmBooking', 'Admin\BookingTicketController@confirmBooking');


        /**
         * API: create Booking
         *
         * Method: POST
         * Parameters:
         * - listing_id (REQUIRED)
         * - products: (REQUIRED)(array of JSON) -> listing product with qty
         *    example
                 $scope.order = [
                 {
                 qty: 1,
                    product: {
                        id: 3,
                        qty: 1
                    }
                },
                {
                qty: 1,
                    product: {
                        id: 6,
                        qty: 1
                    }
                }
                ];

            * - first_name(REQUIRED)
            * - last_name(REQUIRED)
            * - email(REQUIRED)
            * - Mobile(REQUIRED)
            * - create_member (boolean) (REQUIRED)
            * - total_paid (REQUIRED)
            *    example
                $scope.total =
                {
                cash: 25.00
                };
            */
        Route::post('/createBooking', 'Admin\BookingTicketController@createBooking');


        /**
         * API: update Booking
         *
         * Method: POST
         * Parameters:
         * - order_id (REQUIRED)
         * = products: (REQUIRED) (array of JSON)   -> listing product with qty
         *    example
                 $scope.order = [
                 {
                 qty: 1,
                    product: {
                        id: 3,
                        qty: 1
                    }
                },
                {
                qty: 1,
                    product: {
                        id: 6,
                        qty: 1
                    }
                }
                ];
            * - first_name(REQUIRED)
            * - last_name(REQUIRED)
            * - email(REQUIRED)
            * - Mobile(REQUIRED)
            */
        Route::post('/updateBooking', 'Admin\BookingTicketController@updateBooking');


        /**
         * API: cancel Booking
         *
         * Method: POST
         * Parameters:
         * - order_id (REQUIRED)
         */
        Route::post('/cancelBooking', 'Admin\BookingTicketController@cancelBooking');


        /**
         * API: resend Booking
         *
         * Method: POST
         * Parameters:
         * - order_id (REQUIRED)
         */
        Route::post('/resendBooking', 'Admin\BookingTicketController@resendBooking');

        /**
         * API: resend Booking
         *
         * Method: GET
         * Parameters:
         * - order_id (REQUIRED)
         */
        Route::get('/resendBooking', 'Admin\BookingTicketController@resendBooking');


        /**
         * API: find Booking
         *
         * Method: POST
         * Parameters:
         * - listing_id (event ID) (REQUIRED)
         * - order_id (Reference ID) (OPTIONAL)
         * - email (OPTIONAL)
         * - phone_number (OPTIONAL)
         * - name (OPTIONAL)
         */
        Route::post('/findBooking', 'Admin\BookingTicketController@findBooking');

        /**
         * API: find Booking
         *
         * Method: GET
         * Parameters:
         * - listing_id (event ID) (REQUIRED)
         * - order_id (Reference ID) (OPTIONAL)
         * - email (OPTIONAL)
         * - phone_number (OPTIONAL)
         * - name (OPTIONAL)
         *
         *  Return
         * - order
         * - order details
         * - voucher
         * - listing tables
         */
        Route::get('/findBooking', 'Admin\BookingTicketController@findBooking');


        /**
         * API: get Booking
         *
         * Method: GET
         * Parameters:
         * - order_id (REQUIRED)
         * Return
         * - order
         * - order details
         * - voucher
         * - listing tables
         */
        Route::post('/getBooking', 'Admin\BookingTicketController@getBooking');

        /**
         * API: Refresh Gaming IGT Setting
         *
         * Method: GET
         */
        Route::get('/refreshGamingSettingIGT', 'Admin\SettingController@refreshGamingSettingIGT');

        /**
         * API: Refresh Gaming Odyssey Setting
         *
         * Method: GET
         */
        Route::get('/refreshGamingSettingOdyssey', 'Admin\SettingController@refreshGamingSettingOdyssey');

        /**
         * API: Restore Gaming Default
         *
         * Method: GET
         */
        Route::get('/restoreGamingDefault', 'Admin\SettingController@restoreGamingDefault');

        /**
         * API: Check custom field setting
         *
         * Method: GET
         */
        Route::get('/callCustomFieldOdyssey', 'Admin\SettingController@callCustomFieldOdyssey');

    });




    /*
    |--------------------------------------------------------------------------
    | Member Routes
    |--------------------------------------------------------------------------
    |
    | With specified roles/privileges, user can access the API.
    | Most of the APIs will be used for mobile APP.
    |
    |
    */

    /**
     * API: for member user only
     *
     * Parameters:
     *  - token (required) >> JWT token
     */
    Route::group(['prefix' => 'member', 'middleware' => ['jwt.auth', 'role.before', 'visit'], 'roles' => ['*']], function () {

        /**
         * API: loginSSO (this is to go to member app and register to service app)
         *
         * Method: POST
         * Parameters:
         *  - id_token (required)
         *
         * Returns:
         *  - status
         */
        Route::post('/verifyIDToken', 'Member\UserController@verifyIDToken');

        /**
         * API: refresh SSO (this is to go to member app)
         *
         * Method: POST
         * Parameters:
         *  - refresh_token (required)
         *
         * Returns:
         *  - jwt_token
         */
        Route::post('/refreshSSO', 'Member\UserController@refreshSSO');

        /*
        |--------------------------------------------------------------------------
        | Venue
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all venues
         *
         * Method: GET change to POST
         */
        Route::post('/venue', 'Member\VenueController@memberVenue');

        /*
        |--------------------------------------------------------------------------
        | Listing type
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all listing types
         *
         * Method: GET change to POST
         */
        Route::post('/listingType', 'Member\ListingTypeController@index');

        /*
        |--------------------------------------------------------------------------
        | Listings
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all promotions
         * >> display based on schedules
         *
         * Method: GET change to POST
         */
        Route::post('/promotions', 'Member\ListingController@retrievePromotions');

        /**
         * API: show all listings
         * >> display based on schedules
         *
         * Method: GET change to POST
         * Parameters:
         *  - display_id (required)
         */
        Route::post('/listing', 'Member\ListingController@index');

        /**
         * API: show all listings
         * >> display raw listing
         *
         * Method: GET change to POST
         * Parameters:
         *  - type_id (required)
         */
        Route::post('/rawListing', 'Member\ListingController@displayListings');

        /**
         * API: show a detailed listing
         *
         * Method: GET change to POST
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::post('/listing/{id}', 'Member\ListingController@show');

        /**
         * API: retrieve favorite listings
         *
         * Method: GET change to POST
         */
        Route::post('/favoriteList', 'Member\ListingController@retrieveFavorites');

        /**
         * API: set listing's favorite/non-favorite
         *
         * Method: POST
         * Parameters:
         *  - listing_id (required)
         *  - status (required) eq. 'favorite' or 'nonfavorite'
         */
        Route::post('/favorite', 'Member\ListingController@favorite');

        /**
         * API: like/dislike listing
         *
         * Method: POST
         * Parameters:
         *  - listing_id (required)
         *  - status (required) eq. 'like' or 'dislike'
         */
        Route::post('/like', 'Member\ListingController@like');

        /**
         * API: submit an enquiry about listing
         *
         * Method: POST
         * Parameters:
         *  - listing_id (required)
         *  - subject (required) max. 255 chars
         *  - message (required)
         */
        Route::post('/enquire', 'Member\ListingController@enquire');

        /**
         * API: submit an feedback
         *
         * Method: POST
         * Parameters:
         *  - rating (required)
         *  - contact_me (optional)
         *  - message (optional)
         */
        Route::post('/feedback', 'Member\ListingController@feedback');

        /**
        * API: Display or create request for friend referrals
        *
        */
        // Route::resource('/friendreferral', 'Member\FriendReferralController', ['only' => ['index', 'store', 'show']]);
        
        
        /**
        * API: Display or create request for friend referrals
        *
        */
        Route::post('/friendreferral', 'Member\FriendReferralController@store');

        /*
        |--------------------------------------------------------------------------
        | Profile
        |--------------------------------------------------------------------------
        */

        /**
         * API: Send Verification Code via SMS
         *
         * Method: GET
         */
        Route::get('/verify', 'AuthenticationController@sendVerificationCode');

        /**
         * API: Verify the code
         *
         * Method: POST
         * Parameters:
         *  - code (required)
         */
        Route::post('/verify', 'AuthenticationController@verifyCode');

        /**
         * API: change password
         *
         * Method: PUT
         * Parameters:
         *  - old_password (required)
         *  - new_password (required)
         *
         * Returns:
         *  - jwt_token
         */
        Route::put('/password', 'Member\UserController@changePassword');

        /**
         * API: show profile
         *
         * Method: GET change to POST
         */
        Route::post('/isBepozAccountCreated', 'Member\UserController@isBepozAccountCreated');

        /**
         * API: show profile
         *
         * Method: GET change to POST
         */
        Route::post('/profile', 'Member\UserController@index');

        /**
         * API: update profile
         *
         * Method: PUT
         * Parameters:
         *  - first_name (optional)
         *  - last_name (optional)
         *  - dob (optional)
         *  - phone (optional)
         *  - mobile (optional)
         *
         * Returns:
         *  - jwt_token
         */
        Route::put('/profile', 'Member\UserController@updateProfile');

        /**
         * API: add/change photo profile
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/profile/photo', 'Member\UserController@uploadPhoto');

        /**
         * API: add/change extra photo
         *
         * Method: POST
         * Parameters:
         *  - photo (required)
         */
        Route::post('/profile/extraPhoto', 'Member\UserController@uploadExtraPhoto');

        /**
         * API: change email
         *
         * Method: POST
         * Parameters:
         *  - email (required)
         *  - password (required) : current password
         */
        Route::post('/profile/email', 'Member\UserController@changeEmail');

        /**
         * API: Resend confirmation email
         *
         * Method: POST
         */
        Route::post('/resendConfirmationEmail', 'Member\UserController@resendConfirmations');

        /**
         * API: Resend confirmation SMS
         *
         * Method: POST
         */
        Route::post('/resendConfirmationSMS', 'Member\UserController@resendConfirmationSMS');
        
        /**
         * API: set Onesignal PlayerID
         *
         * Method: GET change to POST
         */
        Route::post('/onesignalPlayerID', 'Member\UserController@onesignalPlayerID');
        
        /**
         * API: get community Points
         *
         * Method: GET change to POST
         */
        Route::post('/communityPoints', 'Member\UserController@communityPoints');

        /**
         * API: get Connect With Us
         *
         * Method: GET change to POST
         */
        Route::post('/connectWithUs', 'Member\UserController@connectWithUs');

        /**
         * API: get Pond Hoppers Info
         *
         * Method: GET change to POST
         */
        Route::post('/pondHoppersInfo', 'Member\UserController@pondHoppersInfo');

        /*
        |--------------------------------------------------------------------------
        | User Vouchers
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all issued vouchers
         *
         * Method: GET change to POST
         * Parameters:
         *  - event_id (optional)
         */
        Route::post('/userVoucher', 'Member\UserVoucherController@index');

        /**
         * API: show all issued tickets
         *
         * Method: GET change to POST
         */
        Route::post('/userTickets', 'Member\UserVoucherController@showAllTickets');

        /**
         * API: retrieve data for a specific issued voucher
         *
         * Method: GET change to POST
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::post('/userVoucher/{id}', 'Member\UserVoucherController@show');

        /**
         * API: show all group of issued vouchers
         *
         * Method: GET change to POST
         */
        Route::post('/groupVoucher', 'Member\UserVoucherController@ticketGroup');

        /**
         * API: show all issued stamp card
         *
         * Method: GET change to POST
         */
        Route::post('/stampCardWon', 'Member\UserVoucherController@stampCardWon');


        /*
        |--------------------------------------------------------------------------
        | Address
        |--------------------------------------------------------------------------
        */

        /**
         * API: show addresses
         *
         * Method: GET change to POST
         */
        Route::post('/addressList', 'Member\AddressController@index');

        /**
         * API: add address
         *
         * Method: POST
         * Parameters:
         *  - street (optional)
         *  - recipient_name (optional)
         *  - unit_number (optional)
         *  - postcode (optional)
         *  - state (optional)
         *  - country (optional)
         *  - distance (optional)
         *  - category (optional) -> eq. home / postal / office
         *  - type (optional) -> eq. flat / house / apartment / other
         *  - suburb (optional)
         *  - payload (optional)
         */
        Route::post('/address', 'Member\AddressController@store');

        /**
         * API: update address
         *
         * Method: PUT
         * Parameters:
         *  - {$id} >> address_id (required)
         *  - street (optional)
         *  - recipient_name (optional)
         *  - unit_number (optional)
         *  - postcode (optional)
         *  - state (optional)
         *  - country (optional)
         *  - distance (optional)
         *  - category (optional) -> eq. home / postal / office
         *  - type (optional) -> eq. flat / house / apartment / other
         *  - suburb (optional)
         *  - payload (optional)
         */
        Route::put('/address/{id}', 'Member\AddressController@update');

        /**
         * API: delete address
         *
         * Method: DELETE
         * Parameters:
         *  - {id} (required) >> address_id
         */
        Route::delete('/address/{id}', 'Member\AddressController@destroy');


        /*
        |--------------------------------------------------------------------------
        | Products
        |--------------------------------------------------------------------------
        */

        //  /**
        //   * API: show all products
        //   *
        //   * Method: GET
        //   */
        //  Route::get('/product', 'Member\ProductController@index');

        //  /**
        //   * API: retrieve data for a specific product
        //   *
        //   * Method: GET
        //   * Parameters:
        //   *  - {id} (required) >> on the link
        //   */
        //  Route::get('/product/{id}', 'Member\ProductController@show');

        /*
            |--------------------------------------------------------------------------
            | FAQ
            |--------------------------------------------------------------------------
            */

        /**
         * API: show all faqs
         *
         * Method: GET change to POST
         */
        Route::post('/faq', 'Member\FaqController@index');



        /*
            |--------------------------------------------------------------------------
            | Contact US
            |--------------------------------------------------------------------------
            */

        //  /**
        //   * API: send contact us
        //   *
        //   * Method: POST
        //   * Parameters:
        //   *  - method (required): email / phone
        //   *  - time_of_call (optional): format: HH:MM:SS
        //   *  - message (required)
        //   */
        //  Route::post('/contactus', 'Member\ContactUsController@store');


        /*
        |--------------------------------------------------------------------------
        | Transactions
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all transactions
         *
         * Method: GET change to POST
         */
        Route::post('/transaction', 'Member\TransactionController@index');


        /**
         * API: place an order
         *
         * Method: POST
         * Parameters:
         *  - order (required) >> json
         *  - type (required) eq: cash / point
         *  - total (required)
         *  - listing_id (optional)
         *  - comments (optional)
         *
         * Structure:
         *  // multi item
         *  [
                {
                    "product": {
                        "id": 21,
                        "name": "Topgun 22 Oct Buffet & Drinks for ADULT",
                        "desc_short": "Topgun 22 Oct Buffet & Drinks for ADULT Short",
                        "desc_long": "Topgun 22 Oct Buffet & Drinks for ADULT Long",
                        "image": "https://meadows-imgs.s3.amazonaws.com/product/21/Banners8.jpg",
                        "unit_price": "90.00",
                        "point_price": "132.00",
                        "point_get": "2.00",
                        "status": "active",
                    }
                },
                {
                    "product": {
                        "id": 22,
                        "name": "Topgun 22 Oct Buffet ONLY for ADULT",
                        "desc_short": "Topgun 22 Oct Buffet ONLY for ADULT Short",
                        "desc_long": "Topgun 22 Oct Buffet ONLY for ADULT Long",
                        "image": "https://meadows-imgs.s3.amazonaws.com/product/22/Banners9.jpg",
                        "unit_price": "45.00",
                        "point_price": "55.00",
                        "point_get": "7.00",
                        "status": "active",
                    }
                }
            ]
            */
        Route::post('/placeOrder', 'Member\TransactionController@placeOrder')->middleware(['email.verify', 'bepoz.verify']);


        /**
         * API: confirm an order
         *
         * Method: POST
         * Parameters:
         *  - order_token (required)
         *  - option (required) eq: confirmed / cancelled
         */
        Route::post('/orderConfirm', 'Member\TransactionController@confirmOrder')->middleware(['email.verify', 'bepoz.verify']);


        /**
         * API: use stripe to pay
         *
         * Method: POST
         * Parameters:
         *  - stripe_token (required)
         *  - order_token (required)
         *  - use_saved_card (required | boolean) Option to choose existing card
         *  - save_card (required | boolean) Option to store their card
         *
         **/
        Route::post('/stripe', 'Member\TransactionController@useStripe')->middleware(['email.verify', 'bepoz.verify']);


        /**
         * API: claim promotion(s)
         *
         * Method: POST
         * Parameters:
         *  - promotion_data (required)
         *
         **/
        Route::post('/claimPromotion', 'Member\TransactionController@claimPromotion')->middleware(['email.verify', 'bepoz.verify']);


        /**
         * API: purchase gift certificate
         *
         * Method: POST
         * Parameters:
         *  - data (required)
         *  - full_name (required)
         *  - email (required)
         *  - note
         *
         **/
        Route::post('/purchaseGiftCertificate', 'Member\TransactionController@purchaseGiftCertificate')->middleware(['email.verify', 'bepoz.verify']);

        /*
        |--------------------------------------------------------------------------
        | Settings
        |--------------------------------------------------------------------------
        */

        /**
         * API: Return point ratio
         *
         * Method: GET
         */
        Route::get('/pointRatio', 'Member\SettingController@getPointRatio');

        /**
         * API: Return home setting
         *
         * Method: GET change to POST
         */
        Route::post('/homeSetting', 'Member\SettingController@getHomeSetting');

        /**
         * API: Return about
         *
         * Method: GET change to POST
         */
        Route::post('/about', 'Member\SettingController@about');

        /**
         * API: Return friend Referral message
         *
         * Method: GET change to POST
         */
        Route::post('/referralMessage', 'Member\SettingController@referralMessage');

        /**
         * API: Return legals
         *
         * Method: GET change to POST
         */
        Route::post('/legals', 'Member\SettingController@legals');

        //  /**
        //   * API: Return hour and location
        //   *
        //   * Method: GET
        //   */
        //  Route::get('/hourandlocation', 'Member\SettingController@hourAndLocation');

        /**
         * API: Return stripe info
         *
         * Method: GET change to POST
         */
        Route::post('/stripeInfo', 'Member\SettingController@retrieveStripe');

        /**
         * API: Return stripe key
         *
         * Method: GET change to POST
         */
        Route::post('/stripeKey', 'Member\SettingController@retrieveStripeKey');

        /*
        |--------------------------------------------------------------------------
        | Notifications
        |--------------------------------------------------------------------------
        */

        /**
         * API: show all notifications
         *
         * Method: GET change to POST
         */
        Route::post('/notification', 'Member\NotificationController@index');

        /**
         * API: Clear a notification
         *
         * Method: POST
         */
        Route::post('/notification/{id}', 'Member\NotificationController@clear');

        /*
        |--------------------------------------------------------------------------
        | Point Reward
        |--------------------------------------------------------------------------
        */

        /**
         * API: Return point ratio
         *
         * Method: POST
         * Parameters:
         * - reward_type (required) eq: facebook / twitter
         * - claimed_at (required) eq: 1479851974 ( moment timestamp)
         * - event (required) eq: like
         */
        Route::post('/claimReward', 'Member\PointRewardController@claimReward');


        /*
            |--------------------------------------------------------------------------
            | Survey
            |--------------------------------------------------------------------------
            */

        /**
         * API: show all survey
         *
         * Method: GET change to POST
         */
        Route::post('/surveyAll', 'Member\SurveyController@index');

        /**
         * API: retrieve a survey
         *
         * Method: GET change to POST
         * Parameters:
         *  - {id} (required) >> on the link
         */
        Route::post('/survey/{id}', 'Member\SurveyController@show');

        /**
         * API: answer a question from survey
         *
         * Method: POST
         * Parameters:
         *  - survey (required) : json object

            sample:
            {
                "id": 1,
                "title": "Survey 1: Uni details",
                "description": "<div><!--block-->This is for new members only<br>Uni questionnaire</div>",
                "status": "active",
                "never_expired": 1,
                "date_expiry": null,
                "questions": [
                    {
                        "id": 1,
                        "question": "Where do you study?",
                        "multiple_choice": 1,
                        "answer_id": 1,  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< answer ID (required)
                        "answer": "any", <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< another answer (optional)
                        "pivot": {
                        "survey_id": 1,
                        "question_id": 1,
                        "question_order": 1,
                        "created_at": "2017-01-16 03:46:02",
                        "updated_at": "2017-01-16 03:46:02"
                        },
                        "answers": [
                        {
                            "id": 1,
                            "answer": "Melbourne University",
                            "pivot": {
                            "question_id": 1,
                            "answer_id": 1,
                            "created_at": "2017-01-16 03:43:46",
                            "updated_at": "2017-01-16 03:43:46"
                            }
                        },
                        {
                            "id": 2,
                            "answer": "RMIT",
                            "pivot": {
                            "question_id": 1,
                            "answer_id": 2,
                            "created_at": "2017-01-16 03:43:46",
                            "updated_at": "2017-01-16 03:43:46"
                            }
                        },
                        {
                            "id": 6,
                            "answer": "Victoria University",
                            "pivot": {
                            "question_id": 1,
                            "answer_id": 6,
                            "created_at": "2017-01-16 03:43:46",
                            "updated_at": "2017-01-16 03:43:46"
                            }
                        }
                    ]
                    },
                    {
                        "id": 3,
                        "question": "What course are you studying?",
                        "multiple_choice": 1,
                        "answer_id": 67,
                        "pivot": {
                            "survey_id": 1,
                            "question_id": 3,
                            "question_order": 0,
                            "created_at": "2017-01-18 22:55:28",
                            "updated_at": "2017-01-18 22:55:28"
                        },
                        "answers": [
                            {
                                "id": 67,
                                "answer": "Science",
                                "pivot": {
                                "question_id": 3,
                                "answer_id": 67,
                                "created_at": "2017-01-18 05:51:15",
                                "updated_at": "2017-01-18 05:51:15"
                                }
                            },
                            {
                                "id": 68,
                                "answer": "Commerce",
                                "pivot": {
                                "question_id": 3,
                                "answer_id": 68,
                                "created_at": "2017-01-18 05:51:15",
                                "updated_at": "2017-01-18 05:51:15"
                                }
                            }
                        ]
                    },
                    {
                        "id": 4,
                        "question": "What year of uni are you in?",
                        "multiple_choice": 1,
                        "answer_id": 85,
                        "pivot": {
                            "survey_id": 1,
                            "question_id": 4,
                            "question_order": 0,
                            "created_at": "2017-01-18 22:55:28",
                            "updated_at": "2017-01-18 22:55:28"
                        },
                        "answers": [
                            {
                                "id": 85,
                                "answer": "First Year",
                                "pivot": {
                                "question_id": 4,
                                "answer_id": 85,
                                "created_at": "2017-01-18 05:54:49",
                                "updated_at": "2017-01-18 05:54:49"
                                }
                            }
                        ]
                    }
                ]
            }
        */

        Route::post('/survey', 'Member\SurveyController@store')->middleware(['email.verify', 'bepoz.verify']);

        /**
         * API: change Preferred Venue
         *
         * Method: GET to POST
         * Parameters:
         *  - venue_id (required)
         */
        Route::post('/changePreferredVenue', 'Member\UserController@changePreferredVenue')->middleware(['email.verify', 'bepoz.verify']);

        /**
         * API: change Mobile
         *
         * Method: POST
         */
        Route::post('/changeMobile', 'Member\UserController@changeMobile');

        /**
         * API: shoplist
         *
         * Method: post
         * Parameters:
         *  - 
         */
        Route::post('/shoplist', 'Member\ListingController@shoplist');

        /**
         * API: shop
         *
         * Method: post
         * Parameters:
         *  - 
         */
        Route::post('/shop', 'Member\TransactionController@shop')->middleware(['email.verify', 'bepoz.verify']);

        /**
         * API: membershiptierslist
         *
         * Method: post
         * Parameters:
         *  - 
         */
        Route::post('/membershiptierslist', 'Member\ListingController@membershiptierslist');

        /**
         * API: claimtiers
         *
         * Method: post
         * Parameters:
         *  - 
         */
        Route::post('/membershiptierupgrade', 'Member\TransactionController@membershiptierupgrade')->middleware(['email.verify', 'bepoz.verify']);


    });

    Route::group(['prefix' => 'custom', 'middleware' => [], 'roles' => [] ], function () {

    });

    /**
     * API: Return about
     *
     * Method: GET
     */
    Route::get('/member/about', 'Member\SettingController@about');

    /**
     * API: Return hour and location
     *
     * Method: GET
     */
    Route::get('/member/hourandlocation', 'Member\SettingController@hourAndLocation');

    /**
     * API: Bepoz
     * for Dev.
     */
    Route::group(['prefix' => 'bepoz'], function () {

        /**
         * API: Retrieve Account
         *
         * Method: POST
         */
        Route::post('/bepozAccountGet', 'BepozController@bepozAccountGet');

        /**
         * API: List all bepoz group
         *
         * Method: POST
         */
        Route::get('/bepozGroups', 'BepozController@bepozGroups');

        /**
         * API: Sync Voucher Setup
         *
         * Method: POST
         */
        Route::post('/voucherSetups', 'BepozController@voucherSetups');

        /**
         * API: List all Voucher Setups
         *
         * Method: GET
         */
        Route::get('/voucherSetups', 'BepozController@index');

        /**
         * API: Sync prize Promotions
         *
         * Method: POST
         */
        //Route::post('/prizePromotions', 'BepozController@voucherSetups');

        /**
         * API: List all prize Promotions
         *
         * Method: GET
         */
        Route::get('/prizePromotions', 'BepozController@prizePromotions');

        //  /**
        //   * API: sync account manually
        //   *
        //   * Method: GET
        //   */
        //  Route::get('/syncAccount', 'BepozController@syncAccount');

        //  /**
        //   * API: List all running + failed jobs
        //   */
        //  Route::get('/jobs', 'BepozController@getRunningJobs');

        /**
         * API: Get Bepoz Version
         *
         * Method: GET
         */
        Route::get('/versionGet', 'BepozController@versionGet');

        /**
         * API: bepoz Products Search
         *
         * Method: GET
         */
        Route::get('/bepozProductsSearch', 'BepozController@bepozProductsSearch');
        
        /**
         * API: Get Bepoz Version
         *
         * Method: GET
         */
        Route::get('/bepozCustomFieldsMetaGet', 'BepozController@bepozCustomFieldsMetaGet');
        
        /**
         * API: Get Bepoz Venue Names
         *
         * Method: GET
         */
        Route::get('/bepozVenueNames', 'BepozController@bepozVenueNames');
        
        /**
         * API: Get Bepoz Operator List
         *
         * Method: GET
         */
        Route::get('/bepozOperatorList', 'BepozController@bepozOperatorList');
        
        /**
         * API: Get Bepoz Workstation List
         *
         * Method: GET
         */
        Route::get('/bepozWorkstationList', 'BepozController@bepozWorkstationList');

    });

    /*
    |--------------------------------------------------------------------------
    | AJAX Routes
    |--------------------------------------------------------------------------
    |
    | This is the list of AJAX requests.
    | It does not need any privilege to perform the requests.
    | Returns JSON format.
    |
    */

    /**
     * API: Perform check email
     *
     * Method: GET
     * Parameters:
     *  - email (required)
     *
     * Method: GET
     */
    Route::get('/email/check', 'AuthenticationController@checkEmail');

    /**
     * API: Perform check accountSearch
     *
     * Method: POST
     * Parameters:
     *  - AccountID (required)
     *  - AccNumber (required)
     */
    Route::post('/accountSearch', 'AuthenticationController@accountSearch');

    /**
     * API: Perform check accountSearchEmailMobile
     *
     * Method: POST
     * Parameters:
     *  - email (required)
     *  - mobile (required)
     */
    Route::post('/accountSearchEmailMobile', 'AuthenticationController@accountSearchEmailMobile');

    /*
    |--------------------------------------------------------------------------
    | Venue
    |--------------------------------------------------------------------------
    */

    /**
     * API: show all venues
     *
     * Method: GET change to POST
     */
    Route::post('/venue', 'Member\VenueController@index');

    /**
     * API: return instruction
     *
     * Method: POST
     * Parameters:
     */
    Route::post('/instruction', 'AuthenticationController@instruction');


    /**
     * API: Resend Details Email
     *
     * Method: POST
     */
    Route::post('/resendDetailsEmail', 'AuthenticationController@resendDetailsEmail');

    /**
     * API: Resend Details SMS
     *
     * Method: POST
     */
    Route::post('/resendDetailsSMS', 'AuthenticationController@resendDetailsSMS');

// });

