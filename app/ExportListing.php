<?php

namespace App;

use App\Listing;
use App\VenueTag;
use App\Venue;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\FromArray;
// use Maatwebsite\Excel\Concerns\FromCollection;

class ExportListing implements FromView
{
    public function __construct($values)
    {
        $this->values = $values;
    }

    public function view(): View
    {
        return view('export.totalClick', [
            'values' => $this->values
        ]);
    }
}

// class Exports implements FromArray
// {
//     public function array(): array
//     {
//         $listings = Listing::where('id', '>', '0')->get();


//         $values = array();
//         foreach ($listings as $listing) {
//             $listing_type = "";
//             if( $listing->listing_type_id == 1){
//                 $listing_type = "Special Events";
//             } else if( $listing->listing_type_id == 2){
//                 $listing_type = "Regular Events";
//             }

//             $values[] = array(
//                 "id" => $listing->id,
//                 "name" => $listing->name,
//                 "type" => $listing_type,
//                 "status" => $listing->status,
//                 "value" => $listing->click_count
//             );
//         }

//         return $values;
        
//     }
// }

// class Exports implements FromCollection
// {
//     public function collection()
//     {
//         $listings = Listing::where('listing_type_id', 1)->get();


//         $values = array();
//         foreach ($listings as $listing) {
//             $listing_type = "";
//             if( $listing->listing_type_id == 1){
//                 $listing_type = "Special Events";
//             } else if( $listing->listing_type_id == 2){
//                 $listing_type = "Regular Events";
//             }

//             $values[] = array(
//                 "id" => $listing->id,
//                 "name" => $listing->name,
//                 "type" => $listing_type,
//                 "status" => $listing->status,
//                 "value" => $listing->click_count
//             );
//         }


//         // $venueTags = VenueTag::where('id', '>', '0')->get();
//         // return $venueTags;

//         // $venues = Venue::where('id', '>', '0')->get();
//         // return $venues;

//         $listings = Listing::select('id','name','extra_settings')
//             ->where('id', '>', '0')->get();
//         return $listings;

//     }
// }