<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'emails';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function settings()
    {
        return $this->hasMany('App\MandrillSetting');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($email) { // before delete() method call this

            foreach ($email->settings as $setting) {
                $setting->delete();
            }

        });

    }
}
