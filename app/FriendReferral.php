<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FriendReferral extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'email', 'full_name', 'token', 'member_id', 'has_made_a_purchase', 'accepted_at', 'reward', 'point',
        'mobile', 'reminder_interval', 'sent_at', 'expiry_date', 'is_accepted', 'is_rewarded', 'has_joined',
        'reward_option', 'referrers_membership', 'voucher_setups_id', 'new_member_id', 'issued_at', 'joined_at'];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    //
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function new_member()
    {
        return $this->belongsTo('App\Member', 'new_member_id');
    }

    /**
     * Get the voucher setup.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucher_setup()
    {
        return $this->belongsTo('App\VoucherSetups', 'voucher_setups_id');
    }

}
