<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    // use SoftDeletes;

    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'mobile', 'email_confirmation', 'email_confirmation_token', 'sms_confirmation_code', 'sms_confirmation'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'status', 'user_roles', 'password', 'created_at', 'updated_at', 'deleted_at', 'sms_verification_code', 'email_confirmation_token', 'sms_confirmation_code'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the roles a user has
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_roles()
    {
        return $this->hasMany('App\UserRoles');
    }

    /**
     * Get staff status
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function staff()
    {
        return $this->hasOne('App\Staff');
    }

    /**
     * Get member status
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function member()
    {
        return $this->hasOne('App\Member');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id')->wherePivot('deleted_at', null)->withTimestamps();
    }

    /**
     * Get the social logins.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function social_logins()
    {
        return $this->hasMany('App\SocialLogin');
    }


    /**
     * Find out the User Role
     * The expected input: 'Admin', 'Staff', or 'Member'
     *
     * @param $roleName
     * @return bool
     */
    public function is($roleName)
    {
        foreach ($this->user_roles as $user_role) {
            if ($user_role->role->name == $roleName) {
                return true;
            }
        }

        return false;
    }

    /**
     * Assign member role to user
     *
     * @param $first_name
     * @param $last_name
     */
    public function makeMember($first_name, $last_name, $dob)
    {
        if (!$this->is('member')) {

            // Create member object

            $member = new Member();
            $member->user_id = $this->id;
            $member->first_name = $first_name;
            $member->last_name = $last_name;
            $member->dob = $dob;
            $member->save();
        }
    }

    /**
     * Generate Random Code for SMS Verification
     *
     * @return int
     */
    public function generateSMSVerificationCode()
    {
        $code = mt_rand(100000, 999999);

        $this->sms_verification_code = $code;

        $this->save();

        return $code;
    }

    /**
     * Generate Token for Email Confirmation
     *
     * @return string
     */
    public function generateEmailConfirmationToken()
    {
        $token = md5($this->email);

        $this->email_confirmation_token = $token;

        $this->save();

        return $token;
    }

    /**
     * Set email confirmed for social-login
     *
     * @param $type
     */
    public function confirmSocialEmail($type)
    {
        if ($type == 'gmail') {
            $this->gmail = true;
        } elseif ($type == 'facebook') {
            $this->facebook = true;
        } elseif ($type == 'twitter') {
            $this->twitter = true;
        } elseif ($type == 'linkedln') {
            $this->linkedln = true;
        }
        $this->email_confirmation = true;
        $this->email_confirmed_at = Carbon::now(config('app.timezone'));

        $this->save();
    }

    /**
     * Set email confirmed for social-login
     *
     * @param $type
     */
    public function checkSocialEmail($type)
    {
        if ($type == 'gmail' && !$this->gmail) {
            $this->gmail = true;
        } elseif ($type == 'facebook' && !$this->facebook) {
            $this->facebook = true;
        }
        $this->email_confirmation = true;

        $this->save();
    }
}
