<?php

namespace App\Console\Commands\Cloud;

use App\BepozFailedJob;
use App\BepozJob;
use App\Notification;
use App\Role;
use App\SystemLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class CheckReservedJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-reserved-jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check reserved jobs. Make it unreserved again if already waited too long';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $now = time();
            // Log::warning($now);
            $timefree = $now - 15 * 60;
            // Log::warning($timefree);

            $jobs = BepozJob::where('reserved', '=', 1)
                ->where('reserved_at', '<', $timefree)
                ->chunk(100, function ($jobs) {
                    foreach ($jobs as $bepoz_job) {
                        // Log::warning($bepoz_job);

                        $bepoz_job->free();
                        // $bepoz_job->save();
                    }
                }
                );

        } catch (\Exception $e) {
            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Checking reserved jobs is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'CheckReservedJobs.php';
            $log->save();

            return false;
        }

    }
}
