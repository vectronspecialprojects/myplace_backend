<?php

namespace App\Console\Commands\Cloud;

use App\Email;
use App\FriendReferral;
use App\Jobs\SendReminderEmail;
use App\Member;
use App\Platform;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminders to all invitees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $invitees = FriendReferral::where('has_joined', false)
                ->where('new_member_id', 0)
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('users')
                        ->whereNull('users.deleted_at')
                        ->whereRaw('users.email = friend_referrals.email');
                })
                ->chunk(50, function ($invitees) {
                    foreach ($invitees as $friend) {

                        $intervals = \GuzzleHttp\json_decode($friend->reminder_interval);
                        $send_email = false;
                        if (!empty($intervals)) {
                            $created_at = Carbon::parse($friend->created_at, config('app.timezone'));

                            foreach ($intervals as $interval) {
                                $clone_created_at = $created_at->copy();
                                $clone_created_at->addDays(intval($interval));

                                if ($clone_created_at->isToday()) {
                                    $send_email = true;
                                    break;
                                }
                            }

                        } else {
                            $send_email = true;
                        }

                        if ($send_email) {
                            $member = Member::find($friend->member_id);
                            $user = $member->user;

                            dispatch(new SendReminderEmail($user, $friend));

                        }

                    }
                }
                );

            /*
            $invitees = FriendReferral::where('has_joined', false)
                ->where('new_member_id', 0)
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('users')
                        ->whereNull('users.deleted_at')
                        ->whereRaw('users.email = friend_referrals.email');
                })
                ->get();

            foreach ($invitees as $friend) {

                $intervals = \GuzzleHttp\json_decode($friend->reminder_interval);
                $send_email = false;
                if (!empty($intervals)) {
                    $created_at = Carbon::parse($friend->created_at, config('app.timezone'));

                    foreach ($intervals as $interval) {
                        $clone_created_at = $created_at->copy();
                        $clone_created_at->addDays(intval($interval));

                        if ($clone_created_at->isToday()) {
                            $send_email = true;
                            break;
                        }
                    }

                } else {
                    $send_email = true;
                }

                if ($send_email) {
                    $member = Member::find($friend->member_id);
                    $user = $member->user;

                    dispatch(new SendReminderEmail($user, $friend));

                }

            }
            */

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}
