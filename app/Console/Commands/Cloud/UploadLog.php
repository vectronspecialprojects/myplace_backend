<?php

namespace App\Console\Commands\Cloud;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UploadLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload laravel log to amazon s3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        try {
            $location = storage_path('logs/');
            $filename = "laravel.log";
            $command = "stat -c%s $location$filename";
            $size = exec($command);

            $new_log = 'log_' . Carbon::now(config('app.timezone'))->format('d_m_Y_h_i_s');

            // 1 mb upload
            if (intval($size) > 1000000) {
                Storage::disk('s3')->put('log/' . $new_log, file_get_contents($location . $filename), 'public');
                $command = "echo '' > $location$filename";
                exec($command);
                echo "Log Uploaded successful.";
            } else {
                echo "No need to upload, too small.";
            }
        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}
