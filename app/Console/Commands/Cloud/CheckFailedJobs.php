<?php

namespace App\Console\Commands\Cloud;

use App\BepozFailedJob;
use App\BepozJob;
use App\Notification;
use App\Role;
use App\SystemLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class CheckFailedJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-failed-jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check failed jobs. Send back again to bepoz jobs table';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $failed_jobs = BepozFailedJob::where('job_count', '<', 100)
                ->get();

            foreach ($failed_jobs as $failed_job) {
                $uid = Uuid::generate(4);

                $job = BepozJob::where('failed_job_uid', $failed_job->job_uid)->first();
                if (is_null($job)) {

                    $job = BepozJob::where('payload', $failed_job->payload)
                        ->where('queue', $failed_job->queue)
                        ->first();

                    if (is_null($job)) {
                        $job = new BepozJob();
                        $job->queue = $failed_job->queue;
                        $job->payload = $failed_job->payload;
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->failed_job_uid = $uid;
                        $job->save();

                        $failed_job->job_count = intval($failed_job->job_count) + 1;
                        $failed_job->job_uid = $uid;
                        $failed_job->save();
                    }

                }
            }

            return true;
        } catch (\Exception $e) {
            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Checking failed jobs is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'CheckFailedJobs.php';
            $log->save();

            return false;
        }

    }
}
