<?php

namespace App\Console\Commands\Cloud;


use App\Listing;
use App\ListingProduct;
use App\ListingSchedule;
use App\ListingScheduleTag;
use App\ListingScheduleType;
use App\ListingScheduleVenue;
use App\ListingType;
use App\Member;
use App\MemberLog;
use App\MemberSystemNotification;
use App\MemberTiers;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Platform;
use App\PaymentLog;
use App\PointLog;
use App\Product;
use App\ProductType;
use App\Setting;
use App\SystemLog;
use App\SystemNotification;
use App\User;
use App\Jobs\SendReminderEmail;
use App\Jobs\SendOneSignalNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendNotificationListing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-notification-listing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification of listing to all or targeted member';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {

            $today = Carbon::now(config('app.timezone'));
            //$today = '2019-06-05 00:00:00';

            //Log::info("Listing notification ".$today);

            $company_name = Setting::where('key', 'company_name')->first()->value;
            //$listingSchedule = ListingSchedule::find($request->input('listing_schedule_id'));
            $listingSchedules = ListingSchedule::with('listing')->where('status', 'active')
                ->whereRaw('
                        "' . $today . '" BETWEEN `date_start` AND `date_end` ')
                ->get();

            foreach ($listingSchedules as $schedule) {
                //Log::warning($schedule->id."   ".$schedule->date_start."   ".$schedule->date_end);

                $listing = $schedule->listing;
                //Log::warning($schedule->listing_id);

                if (!is_null($listing)) {
                    //Log::warning(print_r($listing->extra_settings,true));

                    //$listing->extra_settings = \GuzzleHttp\json_decode( $listing->extra_settings );
                    //$extra_settings = \GuzzleHttp\json_decode($listing->extra_settings);
                    $extra_settings = $listing->extra_settings;


                    $contents = "";
                    //$listing->extra_settings = \GuzzleHttp\json_decode($listing->extra_settings);
                    if (isset($extra_settings->triggerNotif)) {
                        if ($extra_settings->triggerNotif === "true") {
                            if ($extra_settings->notifBroadcastDone === "false") {
                                // Log::warning("Listing " . $listing->id . " " . print_r($extra_settings, true));

                                if ($extra_settings->notifType === "push") {
                                    if ($extra_settings->notifContentType === "default") {
                                        /*
                                         * BUG TO CHECK
                                         */
                                        if (isset($extra_settings->notifContent)) {
                                            $contents = $extra_settings->notifContent;
                                        }
                                    } else if ($listing->extra_settings->notifContentType === "custom") {
                                        /*
                                         * BUG TO CHECK
                                         */
                                        if (isset($extra_settings->notifContent)) {
                                            $contents = $extra_settings->notifContent;
                                        }
                                    }
                                } else if ($extra_settings->notifType === "system") {
                                    //send notification
                                    $sn = SystemNotification::find($extra_settings->notification_id);
                                    $contents = $sn->message;
                                }


                                //check targeting tier
                                if (isset($extra_settings->notifTier)) {
                                    if (!is_null($extra_settings->notifTier) && is_array($extra_settings->notifTier)) {
                                        //Log::info("extra_settings->notifTier ".$extra_settings->notifTier);
                                        //$notifTier = \GuzzleHttp\json_decode($extra_settings->notifTier);
                                        //Log::info("extra_settings->notifTier ".$notifTier);

                                        /*
                                         * BUG TO CHECK
                                         */
                                        if (!is_array($extra_settings->notifTier)) {
                                            $extra_settings->notifTier = explode(',', $extra_settings->notifTier);
                                        }

                                        $tiermembers = MemberTiers::with('member.user')
                                            ->whereIn('tier_id', $extra_settings->notifTier)
                                            ->where('date_expiry', '>=', $today)
                                            ->chunk(100, function ($tiermembers) use ($extra_settings, $contents, $company_name) {

                                                $tags = [];
                                                $system_send_all = array();
                                                $now = Carbon::now(config('app.timezone'));

                                                foreach ($tiermembers as $tiermember) {
                                                    //get all member have tier
                                                    $member = $tiermember->member;
                                                    $user = $member->user;

                                                    $tags[] = ["key" => "email", "relation" => "=", "value" => $user->email];
                                                    $tags[] = ["operator" => "OR"];

                                                    if ($extra_settings->notifType === "system") {
                                                        $system_send = array(
                                                            'member_id' => $tiermember->member->id,
                                                            'system_notification_id' => $extra_settings->notification_id,
                                                            'status' => 'unread',
                                                            'sent_at' => $now
                                                        );
                                                        array_push($system_send_all, $system_send);
                                                    }
                                                }

                                                if ($extra_settings->notifType === "system") {
                                                    // INSERT SYSTEM NOTIFICATION TO LOCAL DATABASE
                                                    MemberSystemNotification::insert($system_send_all);
                                                }

                                                // REMOVE LAST OR
                                                array_pop($tags);
                                                $dataNotif = [
                                                    "tags" => $tags,
                                                    "contents" => ["en" => $contents],
                                                    "headings" => ["en" => $company_name],
                                                    "data" => ["category" => "member"],
                                                    "ios_badgeType" => "Increase",
                                                    "ios_badgeCount" => 1
                                                ];
                                                // Log::info($data);

                                                // SEND API REQUEST TO ONESIGNAL
                                                // OneSignalFacade::postNotification($data);
                                                dispatch(new SendOneSignalNotification($dataNotif));
                                            }
                                            );
                                    }

                                } else {
                                    //if target tier not set

                                }

                                //set true so it will not sent again
                                $extra_settings->notifBroadcastDone = "true";
                                $extra_settings->notifBroadcastDoneDateTime = Carbon::now(config('app.timezone'))->toDateTimeString();

                                $listing->extra_settings = $extra_settings;
                                $listing->save();

                                //Log::warning("After ".$listing->id." ".print_r($listing->extra_settings, true));
                            }

                        }
                    } else {

                    }
                }

            }

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}
