<?php

namespace App\Console\Commands\Cloud;

use App\Email;
use App\Listing;
use App\ListingSchedule;
use App\Member;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Platform;
use App\Setting;
use App\User;
use App\VoucherSetups;
use App\Jobs\SendReminderVoucherEmail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendReminderVoucher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-reminder-voucher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminders to all who have voucher nearly expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $reminder_voucher = Setting::where('key', 'reminder_voucher')->first()->value;

            //check from setting this feature enabled
            if ($reminder_voucher == "true") {
                // Log::warning("reminder voucher");
                $date_now = Carbon::now();
                // $date_now = Carbon::createFromFormat('Y-m-d H:i', '2019-08-15 11:48');
                // $date_now = Carbon::createFromFormat('Y-m-d H:i', '2020-08-31 11:00');
                // $date_now = Carbon::createFromFormat('Y-m-d H:i', '2019-10-14 12:30');

                $reminder_voucher_interval = \GuzzleHttp\json_decode(Setting::where('key', 'reminder_voucher_interval')->first()->extended_value);

                $target_dates = [];
                $string = "";
                $app = app();
                foreach ($reminder_voucher_interval as $interval) {
                    // Log::warning($interval);
                    $new_date = clone $date_now;
                    $new_date->addDays($interval);

                    $object = $app->make('stdClass');
                    $object->date = $new_date->format('Y-m-d');
                    $object->interval = $interval;
                    array_push($target_dates, $object);
                    // $string .= "'".$new_date->format('Y-m-d')."',";

                }

                // Log::warning($target_dates);

                $reminder_voucher_time = \GuzzleHttp\json_decode(Setting::where('key', 'reminder_voucher_time')->first()->extended_value);

                $send_now = false;
                foreach ($reminder_voucher_time as $time) {
                    $datetime_reminder = Carbon::createFromFormat("Y-m-d H:i a", $date_now->format("Y-m-d") . " " . $time);

                    if ($date_now->format("Y-m-d H:i a") === $datetime_reminder->format("Y-m-d H:i a")) {
                        // Log::warning("sending reminder");
                        $send_now = true;
                    }
                }


                if ($send_now) {
                    foreach ($target_dates as $target_date) {
                        // Log::warning($target_date->date);
                        // Log::warning($target_date->interval);
                        // Log::warning($target_date);

                        $memberVouchers = MemberVouchers::with('member.user')
                            ->where(function ($query) {
                                $query->whereHas('order_detail', function ($query) {
                                    $query->where('product_type_id', '<>', 2);
                                });
                                $query->orWhere('member_vouchers.category', 'bepoz');
                            })
                            ->where('redeemed', 0)
                            ->where('amount_left', '<>', 0)
                            ->whereDate("expire_date", '=', $target_date->date)
                            ->chunk(100, function ($memberVouchers) use ($target_date) {
                                foreach ($memberVouchers as $memberVoucher) {

                                    // Log::warning($memberVoucher);
                                    if ($memberVoucher->member) {
                                        // Log::warning($memberVoucher->member->user);
                                        // Log::warning($memberVoucher->name);
                                        // Log::warning($target_date->interval);
                                        // Log::warning($memberVoucher->expire_date);
                                        // Log::warning($memberVoucher);

                                        dispatch(new SendReminderVoucherEmail($memberVoucher->member->user,
                                            $memberVoucher->name, $target_date->interval, $memberVoucher->expire_date));

                                    }

                                }
                            }
                            );
                    }
                }

            }

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}

/*
foreach ($reminder_voucher_interval as $interval) {
    // Log::warning($interval);
    $new_date = clone $date_now;
    $new_date->addDays($interval);

    // array_push($target_dates, $new_date->format('Y-m-d'));
    // $string .= "'".$new_date->format('Y-m-d')."',";

}

$solution = substr($string,0,-1);

// $memberVouchers = MemberVouchers::with('member.user')
//     ->whereRaw("date(expire_date) in (". $solution .")" )
*/
