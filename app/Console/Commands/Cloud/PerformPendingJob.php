<?php

namespace App\Console\Commands\Cloud;

use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Order;
use App\PendingJob;
use App\Setting;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PerformPendingJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute-pending-job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform pending jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            $jobs = PendingJob::where('attempts', '<', 3)
                ->chunk(50, function ($jobs) use ($email_server) {
                    foreach ($jobs as $job) {
                        $payload = \GuzzleHttp\json_decode($job->payload, true);
                        $extra_payload = \GuzzleHttp\json_decode($job->extra_payload, true);

                        if ($email_server === "mandrill") {

                            if ($mx->init()) {
                                $job->attempts = intval($job->attempts) + 1;
                                $job->save();

                                $mx->send($extra_payload['category'], $extra_payload['email'], $extra_payload['email'], $payload);
                            }

                        } else if ($email_server === "mailjet") {

                            if ($mjx->init()) {
                                $job->attempts = intval($job->attempts) + 1;
                                $job->save();

                                $mjx->send($extra_payload['category'], $extra_payload['email'], $extra_payload['email'], $payload);
                            }

                        }

                    }
                }
                );

            /*
            $jobs = PendingJob::where('attempts', '<', 3)
                ->limit(10)
                ->get();

            foreach ($jobs as $job) {
                $payload = \GuzzleHttp\json_decode($job->payload, true);
                $extra_payload = \GuzzleHttp\json_decode($job->extra_payload, true);

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $job->attempts = intval($job->attempts) + 1;
                        $job->save();

                        $mx->send($extra_payload['category'], $extra_payload['email'], $extra_payload['email'], $payload);
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $job->attempts = intval($job->attempts) + 1;
                        $job->save();

                        $mjx->send($extra_payload['category'], $extra_payload['email'], $extra_payload['email'], $payload);
                    }

                }

            }
            */

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}
