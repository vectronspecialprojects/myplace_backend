<?php

namespace App\Console\Commands\Cloud;

use App\Email;
use App\Listing;
use App\ListingSchedule;
use App\Member;
use App\Order;
use App\OrderDetail;
use App\Platform;
use App\Setting;
use App\User;
use App\Jobs\SendReminderTicketEmail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendReminderTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-reminder-ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminders to all who buy ticket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $reminder_ticket = Setting::where('key', 'reminder_ticket')->first()->value;

            //check from setting this feature enabled
            if ($reminder_ticket == "true") {
                //Log::warning("reminder_ticket");
                $date_now = Carbon::now();
                //$date_now = Carbon::createFromFormat('Y-m-d', '2018-12-30 10:00:00');
                //$date_now = Carbon::createFromFormat('Y-m-d H:i', '2018-12-30 17:00');
                //$date_now = Carbon::createFromFormat('Y-m-d H:i', '2019-07-04 21:00');
                //$date_now = Carbon::createFromFormat('Y-m-d H:i', '2019-07-04 07:00');
                //$date_now = Carbon::createFromFormat('Y-m-d H:i', '2019-06-23 21:00');

                $listings = Listing::where('listing_type_id', '1')->where('status', 'active')->get();
                $listings_send = array();
                foreach ($listings as $listing) {

                    $extra_settings = $listing->extra_settings;

                    if (isset($extra_settings->reminder_ticket_enable)) {
                        //Log::warning($listing->name);
                        //$extra_settings = \GuzzleHttp\json_encode($listing->extra_settings);
                        if (isset($extra_settings->reminder_ticket_interval)) {

                            $reminder_ticket_interval = $extra_settings->reminder_ticket_interval;
                            //sort descending
                            rsort($reminder_ticket_interval);

                            if (!isset($extra_settings->reminder_done)) {
                                $extra_settings->reminder_done = array();
                            }

                            //Log::warning($listing->datetime_start);

                            foreach ($reminder_ticket_interval as $interval) {
                                //Log::warning($interval);
                                //$listing->extra_settings->reminder_done = ['interval' => $interval];

                                $date_reminder = new Carbon($listing->datetime_start);
                                $date_reminder->addDays(-$interval);

                                //Log::warning($interval);
                                //Log::warning($date_reminder);

                                $reminder_ticket_time = $extra_settings->reminder_ticket_time;

                                foreach ($reminder_ticket_time as $time_reminder) {

                                    //$reminder_ticket_time
                                    $datetime_reminder = Carbon::createFromFormat("Y-m-d H:i a", $date_reminder->format("Y-m-d") . " " . $time_reminder);

                                    if ($date_now->format("Y-m-d H:i a") === $datetime_reminder->format("Y-m-d H:i a")) {
                                        //Log::warning($date_now->format("Y-m-d H:i a")." ".$datetime_reminder->format("Y-m-d H:i a"));


                                        //if ( $extra_settings->reminder_done){

                                        $done = false;
                                        foreach ($extra_settings->reminder_done as $reminder_detail) {
                                            if ($reminder_detail->interval == $interval && $reminder_detail->time == $time_reminder) {
                                                $done = true;
                                            }
                                        }

                                        if (!$done) {
                                            $datetime = Carbon::now()->format('Y-m-d H:i:s');
                                            $new_done = array("interval" => $interval, "time" => $time_reminder, "datetime" => $datetime);

                                            //$extra_settings->reminder_done->push($new_done);

                                            //flag done
                                            array_push($extra_settings->reminder_done, $new_done);
                                            $data = array("id" => $listing->id, "interval" => $interval, "time" => $time_reminder, "datetime" => $datetime);

                                            array_push($listings_send, $data);

                                            //Log::warning($date_now->format("Y-m-d H:i a") );
                                            //Log::warning($datetime_reminder->format("Y-m-d H:i a"));
                                            //Log::warning($listing->datetime_start);

                                        }
                                        //}

                                    }
                                }
                            }

                        }
                    }

                    $listing->extra_settings = $extra_settings;

                    $listing->save();

                }

                //Log::warning($listings_send);

                foreach ($listings_send as $listing_send) {
                    //Log::warning($listing_send['id']);

                    //load order details only from selected listing
                    //$orderDetails = OrderDetail::with('order.member.user')->where("listing_id", $listing_send['id'])->get();


                    $orderDetails = OrderDetail::with('order.member.user')->where("listing_id", $listing_send['id'])
                        ->chunk(100, function ($orderDetails) use ($listing_send) {
                            foreach ($orderDetails as $orderDetail) {
                                //Log::warning($orderDetail);
                                //Log::warning($orderDetail->order);
                                //Log::warning($orderDetail->order->member->user);
                                //Log::warning($orderDetail->listing->name);

                                dispatch(new SendReminderTicketEmail(
                                    $orderDetail->order->member->user, $orderDetail->listing->name,
                                    $listing_send['interval'], $orderDetail->listing->datetime_start));
                            }
                        });
                }

            }

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}
