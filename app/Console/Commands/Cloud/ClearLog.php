<?php

namespace App\Console\Commands\Cloud;

use App\BepozLog;
use App\SystemLog;
use App\User;
use App\Jobs\Job;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ClearLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(5, 15);
        sleep($sleep);
        try {
            // CLEAR BEPOZ LOG
            $checkQuery = Carbon::now()->subMonths(3);
            BepozLog::where('created_at', '<=', $checkQuery->toDateTimeString())
                ->limit(500)
                ->forceDelete();

        } catch (\Exception $e) {
            Log::warning($e);
        }

        sleep($sleep);
        try {
            // CLEAR SYSTEM LOG
            $checkQuery = Carbon::now()->subMonths(3);
            SystemLog::where('created_at', '<=', $checkQuery->toDateTimeString())
                ->limit(500)
                ->forceDelete();
        } catch (\Exception $e) {
            Log::warning($e);
        }

    }
}
