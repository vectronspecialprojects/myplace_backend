<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\BepozTransaction;
use App\Helpers\Bepoz;
use App\Member;
use App\Order;
use App\Product;
use App\Setting;
use App\SystemLog;
use App\Venue;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class CreateTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Transaction in bepoz';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {

            $checkBepozConnection = $bepoz->SystemCheck();

            if ($checkBepozConnection) {

                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', 'create-transaction')
                    ->chunk(100, function ($jobs) use ($bepoz) {
                        // Reserve the selected jobs first before modification
                        foreach ($jobs as $bepoz_job) {
                            $bepoz_job->setJobUID(Uuid::generate());
                            $bepoz_job->reserve();
                        }

                        foreach ($jobs as $bepoz_job) {

                            if ($bepoz_job->attempts() < 3) {

                                try {
                                    $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                    $bepoz_job->save();

                                    $bepoz_payload = \GuzzleHttp\json_decode($bepoz_job->payload);
                                    $member = Member::find($bepoz_payload->member_id);
                                    $order = Order::with('order_details')->find($bepoz_payload->order_id);

                                    if (intval($order->venue_id) === 0) {
                                        $paymentName = Setting::where('key', 'bepoz_payment_name')->first()->value;
                                        $tillID = Setting::where('key', 'bepoz_till_id')->first()->value;
                                        $operatorID = Setting::where('key', 'bepoz_operator_id')->first()->value;
                                    } else {
                                        $venue = Venue::find($order->venue_id);
                                        if (is_null($venue)) {
                                            $paymentName = Setting::where('key', 'bepoz_payment_name')->first()->value;
                                            $tillID = Setting::where('key', 'bepoz_till_id')->first()->value;
                                            $operatorID = Setting::where('key', 'bepoz_operator_id')->first()->value;
                                        } else {
                                            $paymentName = $venue->bepoz_payment_name;
                                            $operatorID = $venue->bepoz_operator_id;
                                            $tillID = $venue->bepoz_till_id;
                                        }
                                    }

                                    $trainingMode = Setting::where('key', 'bepoz_training_mode')->first()->value;

                                    $payload = \GuzzleHttp\json_decode($order->payload);
                                    if ($payload->transaction_type == 'place_order') {
                                        $complete_transaction = true;
                                        $transaction_id_collection = array();

                                        foreach ($order->order_details as $order_detail) {


                                            if ($order_detail->voucher_setups_id != 0) {
                                                // purchase voucher

                                                $vs = VoucherSetups::find($order_detail->voucher_setups_id);
                                                if (!is_null($vs)) {
                                                    // gift certificate
                                                    if ($vs->voucher_type == 10) {
                                                        if (is_null($order_detail->bepoz_transaction_id)) {
                                                            $id_collection = array();
                                                            for ($i = 0; $i < $order_detail->qty; $i++) {
                                                                $transLine = [];
                                                                $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                    'PaymentName' => 'GIFTCERT_SELL',
                                                                    'AccountID' => $member->bepoz_account_id
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentName' => $paymentName,
                                                                    'AccountID' => $member->bepoz_account_id,
                                                                    'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $bepoz_transaction = new BepozTransaction();
                                                                $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                                $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                                $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                                $temp3 = $bepoz->TransactionCreate($transLine);
                                                                if ($temp3) {
                                                                    $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                    $bepoz_transaction->status = "successful";

                                                                    $id_collection[] = $temp3['Data1'];
                                                                    $transaction_id_collection[] = $temp3['Data1'];

                                                                } else {
                                                                    $bepoz_transaction->status = "failed";

                                                                    $log = new SystemLog();
                                                                    $log->type = 'bepoz-job-error';
                                                                    $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                    $log->payload = $bepoz_job->payload;
                                                                    $log->message = 'Unable to create a transaction data.';
                                                                    $log->source = 'CreateTransaction.php';
                                                                    $log->save();

                                                                    $complete_transaction = false;
                                                                }

                                                                $bepoz_transaction->save();
                                                            }

                                                            $temp = implode(",", $id_collection);
                                                            $order_detail->bepoz_transaction_id = $temp;
                                                            $order_detail->save();

                                                        } else {
                                                            $id_collection = explode(",", $order_detail->bepoz_transaction_id);
                                                            $transaction_id_collection = array_merge($transaction_id_collection, $id_collection);
                                                            if (count($id_collection) != $order_detail->qty) {
                                                                for ($i = count($id_collection); $i < $order_detail->qty; $i++) {
                                                                    $transLine = [];
                                                                    $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                                    $itemEncode = [
                                                                        'Training' => $trainingMode == "true" ? true : false,
                                                                        'OperatorID' => $operatorID,
                                                                        'TillID' => $tillID,
                                                                        'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                        'OrderID' => $orderID,
                                                                        'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                        'PaymentName' => 'GIFTCERT_SELL',
                                                                        'AccountID' => $member->bepoz_account_id
                                                                    ];
                                                                    $transLine[] = $itemEncode;

                                                                    $itemEncode = [
                                                                        'Training' => $trainingMode == "true" ? true : false,
                                                                        'OperatorID' => $operatorID,
                                                                        'TillID' => $tillID,
                                                                        'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                        'OrderID' => $orderID,
                                                                        'PaymentName' => $paymentName,
                                                                        'AccountID' => $member->bepoz_account_id,
                                                                        'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                                    ];
                                                                    $transLine[] = $itemEncode;

                                                                    $bepoz_transaction = new BepozTransaction();
                                                                    $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                                    $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                                    $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                                    $temp3 = $bepoz->TransactionCreate($transLine);
                                                                    if ($temp3) {
                                                                        $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                        $bepoz_transaction->status = "successful";

                                                                        $id_collection[] = $temp3['Data1'];
                                                                        $transaction_id_collection[] = $temp3['Data1'];

                                                                    } else {
                                                                        $bepoz_transaction->status = "failed";

                                                                        $log = new SystemLog();
                                                                        $log->type = 'bepoz-job-error';
                                                                        $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                        $log->payload = $bepoz_job->payload;
                                                                        $log->message = 'Unable to create a transaction data.';
                                                                        $log->source = 'CreateTransaction.php';
                                                                        $log->save();

                                                                        $complete_transaction = false;
                                                                    }

                                                                    $bepoz_transaction->save();
                                                                }
                                                            }
                                                            $temp = implode(",", $id_collection);
                                                            $order_detail->bepoz_transaction_id = $temp;
                                                            $order_detail->save();
                                                        }
                                                    } else {

                                                        if (is_null($order_detail->bepoz_product_number) || $order_detail->bepoz_product_number === '0') {
                                                            continue;
                                                        }

                                                        // not gift certificate
                                                        $id_collection = array();

                                                        $transLine = [];
                                                        $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                        $itemEncode = [
                                                            'Training' => $trainingMode == "true" ? true : false,
                                                            'OperatorID' => $operatorID,
                                                            'TillID' => $tillID,
                                                            'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                            'OrderID' => $orderID,
                                                            'ProdNumber' => $order_detail->bepoz_product_number,
                                                            'QtySold' => $order_detail->qty,
                                                            'PaymentName' => $paymentName,
                                                            'AccountID' => $member->bepoz_account_id,
                                                            'Gross' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                            'Nett' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                            'PaymentAmount' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', '')
                                                        ];
                                                        $transLine[] = $itemEncode;

                                                        $bepoz_transaction = new BepozTransaction();
                                                        $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                        $bepoz_transaction->order_detail_id = $order_detail->id;
                                                        $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                        $temp3 = $bepoz->TransactionCreate($transLine);
                                                        if ($temp3) {
                                                            $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                            $bepoz_transaction->status = "successful";

                                                            $id_collection[] = $temp3['Data1'];
                                                            $transaction_id_collection[] = $temp3['Data1'];

                                                        } else {
                                                            $bepoz_transaction->status = "failed";

                                                            $log = new SystemLog();
                                                            $log->type = 'bepoz-job-error';
                                                            $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                            $log->payload = $bepoz_job->payload;
                                                            $log->message = 'Unable to create a transaction data.';
                                                            $log->source = 'CreateTransaction.php';
                                                            $log->save();

                                                            $complete_transaction = false;
                                                        }

                                                        $bepoz_transaction->save();

                                                        $temp = implode(",", $id_collection);
                                                        $order_detail->bepoz_transaction_id = $temp;
                                                        $order_detail->save();
                                                    }
                                                }
                                            } else {
                                                // not voucher
                                                $id_collection = array();

                                                if (is_null($order_detail->bepoz_product_number) || $order_detail->bepoz_product_number === '0') {
                                                    continue;
                                                }

                                                $transLine = [];
                                                $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                $itemEncode = [
                                                    'Training' => $trainingMode == "true" ? true : false,
                                                    'OperatorID' => $operatorID,
                                                    'TillID' => $tillID,
                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                    'OrderID' => $orderID,
                                                    'ProdNumber' => $order_detail->bepoz_product_number,
                                                    'QtySold' => $order_detail->qty,
                                                    'PaymentName' => $paymentName,
                                                    'AccountID' => $member->bepoz_account_id,
                                                    'Gross' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                    'Nett' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                    'PaymentAmount' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', '')
                                                ];
                                                $transLine[] = $itemEncode;

                                                $bepoz_transaction = new BepozTransaction();
                                                $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                $bepoz_transaction->order_detail_id = $order_detail->id;
                                                $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                $temp3 = $bepoz->TransactionCreate($transLine);
                                                if ($temp3) {
                                                    $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                    $bepoz_transaction->status = "successful";

                                                    $id_collection[] = $temp3['Data1'];
                                                    $transaction_id_collection[] = $temp3['Data1'];

                                                } else {
                                                    $bepoz_transaction->status = "failed";

                                                    $log = new SystemLog();
                                                    $log->type = 'bepoz-job-error';
                                                    $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                    $log->payload = $bepoz_job->payload;
                                                    $log->message = 'Unable to create a transaction data.';
                                                    $log->source = 'CreateTransaction.php';
                                                    $log->save();

                                                    $complete_transaction = false;
                                                }

                                                $bepoz_transaction->save();


                                                $temp = implode(",", $id_collection);
                                                $order_detail->bepoz_transaction_id = $temp;
                                                $order_detail->save();
                                            }

                                        }

                                        $order->bepoz_transaction_ids = \GuzzleHttp\json_encode($transaction_id_collection);
                                        $order->save();

                                        if ($complete_transaction) {
                                            if (!is_null($bepoz_job->failed_job_uid)) {
                                                $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                if (!is_null($failed_job)) {
                                                    $failed_job->delete();
                                                }
                                            }

                                            $bepoz_job->dispatch();
                                        } else {
                                            $bepoz_job->free();
                                        }

                                    } elseif ($payload->transaction_type == 'gift_certificate') {
                                        $complete_transaction = true;
                                        $transaction_id_collection = array();

                                        foreach ($order->order_details as $order_detail) {
                                            if ($order_detail->voucher_setups_id != 0) {
                                                // purchase voucher

                                                $vs = VoucherSetups::find($order_detail->voucher_setups_id);
                                                if (!is_null($vs)) {
                                                    if ($vs->voucher_type == 10) {
                                                        if (is_null($order_detail->bepoz_transaction_id)) {
                                                            $id_collection = array();
                                                            for ($i = 0; $i < $order_detail->qty; $i++) {
                                                                $transLine = [];
                                                                $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                    'PaymentName' => 'GIFTCERT_SELL',
                                                                    'AccountID' => $member->bepoz_account_id
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentName' => $paymentName,
                                                                    'AccountID' => $member->bepoz_account_id,
                                                                    'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $bepoz_transaction = new BepozTransaction();
                                                                $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                                $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                                $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                                $temp3 = $bepoz->TransactionCreate($transLine);
                                                                if ($temp3) {
                                                                    $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                    $bepoz_transaction->status = "successful";

                                                                    $id_collection[] = $temp3['Data1'];
                                                                    $transaction_id_collection[] = $temp3['Data1'];

                                                                } else {
                                                                    $bepoz_transaction->status = "failed";

                                                                    $log = new SystemLog();
                                                                    $log->type = 'bepoz-job-error';
                                                                    $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                    $log->payload = $bepoz_job->payload;
                                                                    $log->message = 'Unable to create a transaction data.';
                                                                    $log->source = 'CreateTransaction.php';
                                                                    $log->save();

                                                                    $complete_transaction = false;
                                                                }

                                                                $bepoz_transaction->save();
                                                            }

                                                            $temp = implode(",", $id_collection);
                                                            $order_detail->bepoz_transaction_id = $temp;
                                                            $order_detail->save();

                                                        } else {
                                                            $id_collection = explode(",", $order_detail->bepoz_transaction_id);
                                                            $transaction_id_collection = array_merge($transaction_id_collection, $id_collection);
                                                            if (count($id_collection) != $order_detail->qty) {
                                                                for ($i = count($id_collection); $i < $order_detail->qty; $i++) {
                                                                    $transLine = [];
                                                                    $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                                    $itemEncode = [
                                                                        'Training' => $trainingMode == "true" ? true : false,
                                                                        'OperatorID' => $operatorID,
                                                                        'TillID' => $tillID,
                                                                        'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                        'OrderID' => $orderID,
                                                                        'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                        'PaymentName' => 'GIFTCERT_SELL',
                                                                        'AccountID' => $member->bepoz_account_id
                                                                    ];
                                                                    $transLine[] = $itemEncode;

                                                                    $itemEncode = [
                                                                        'Training' => $trainingMode == "true" ? true : false,
                                                                        'OperatorID' => $operatorID,
                                                                        'TillID' => $tillID,
                                                                        'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                        'OrderID' => $orderID,
                                                                        'PaymentName' => $paymentName,
                                                                        'AccountID' => $member->bepoz_account_id,
                                                                        'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                                    ];
                                                                    $transLine[] = $itemEncode;

                                                                    $bepoz_transaction = new BepozTransaction();
                                                                    $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                                    $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                                    $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                                    $temp3 = $bepoz->TransactionCreate($transLine);
                                                                    if ($temp3) {
                                                                        $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                        $bepoz_transaction->status = "successful";

                                                                        $id_collection[] = $temp3['Data1'];
                                                                        $transaction_id_collection[] = $temp3['Data1'];

                                                                    } else {
                                                                        $bepoz_transaction->status = "failed";

                                                                        $log = new SystemLog();
                                                                        $log->type = 'bepoz-job-error';
                                                                        $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                        $log->payload = $bepoz_job->payload;
                                                                        $log->message = 'Unable to create a transaction data.';
                                                                        $log->source = 'CreateTransaction.php';
                                                                        $log->save();

                                                                        $complete_transaction = false;
                                                                    }

                                                                    $bepoz_transaction->save();
                                                                }
                                                            }
                                                            $temp = implode(",", $id_collection);
                                                            $order_detail->bepoz_transaction_id = $temp;
                                                            $order_detail->save();
                                                        }
                                                    } else {
                                                        $bepoz_job->dispatch();
                                                    }
                                                }
                                            } else {
                                                // not gift voucher

                                                $bepoz_job->dispatch();
                                            }

                                        }

                                        $order->bepoz_transaction_ids = \GuzzleHttp\json_encode($transaction_id_collection);
                                        $order->save();

                                        if ($complete_transaction) {
                                            if (!is_null($bepoz_job->failed_job_uid)) {
                                                $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                if (!is_null($failed_job)) {
                                                    $failed_job->delete();
                                                }
                                            }

                                            $bepoz_job->dispatch();
                                        } else {
                                            $bepoz_job->free();
                                        }
                                    }


                                } catch
                                (\Exception $e) {
                                    $log = new SystemLog();
                                    $log->humanized_message = 'Creating Transaction is failed. Please check the error message';
                                    $log->type = 'bepoz-job-error';
                                    $log->payload = $bepoz_job->payload;
                                    $log->message = $e;
                                    $log->source = 'CreateTransaction.php';
                                    $log->save();

                                    $bepoz_job->free();
                                }
                            } else {

                                if (is_null($bepoz_job->failed_job_uid)) {
                                    $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                        ->where('queue', $bepoz_job->queue)
                                        ->first();

                                    if (is_null($failed_job)) {
                                        $failed_job = new BepozFailedJob();
                                        $failed_job->queue = $bepoz_job->queue;
                                        $failed_job->payload = $bepoz_job->payload;
                                        $failed_job->job_uid = Uuid::generate(4);
                                        $failed_job->save();
                                    }
                                }

                                // Maximum attempt log no longer needed
                                // $log = new SystemLog();
                                // $log->type = 'bepoz-job-error';
                                // $log->payload = $bepoz_job->payload;
                                // $log->humanized_message = 'Maximum attempt of creating transaction has been reached.';
                                // $log->source = 'CreateTransaction.php';
                                // $log->save();

                                $bepoz_job->dispatch();
                            }
                        }
                    }
                    );

                /*
                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', 'create-transaction')
                    ->limit(100)
                    ->get();

                if (!$jobs->isEmpty()) {
                    // Reserve the selected jobs first before modification
                    foreach ($jobs as $bepoz_job) {
                        $bepoz_job->setJobUID(Uuid::generate());
                        $bepoz_job->reserve();
                    }

                    foreach ($jobs as $bepoz_job) {

                        if ($bepoz_job->attempts() < 3) {

                            try {
                                $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                $bepoz_job->save();

                                $bepoz_payload = \GuzzleHttp\json_decode($bepoz_job->payload);
                                $member = Member::find($bepoz_payload->member_id);
                                $order = Order::with('order_details')->find($bepoz_payload->order_id);

                                if (intval($order->venue_id) === 0) {
                                    $paymentName = Setting::where('key', 'bepoz_payment_name')->first()->value;
                                    $tillID = Setting::where('key', 'bepoz_till_id')->first()->value;
                                    $operatorID = Setting::where('key', 'bepoz_operator_id')->first()->value;
                                } else {
                                    $venue = Venue::find($order->venue_id);
                                    if (is_null($venue)) {
                                        $paymentName = Setting::where('key', 'bepoz_payment_name')->first()->value;
                                        $tillID = Setting::where('key', 'bepoz_till_id')->first()->value;
                                        $operatorID = Setting::where('key', 'bepoz_operator_id')->first()->value;
                                    } else {
                                        $paymentName = $venue->bepoz_payment_name;
                                        $operatorID = $venue->bepoz_operator_id;
                                        $tillID = $venue->bepoz_till_id;
                                    }
                                }

                                $trainingMode = Setting::where('key', 'bepoz_training_mode')->first()->value;

                                $payload = \GuzzleHttp\json_decode($order->payload);
                                if ($payload->transaction_type == 'place_order') {
                                    $complete_transaction = true;
                                    $transaction_id_collection = array();

                                    foreach ($order->order_details as $order_detail) {


                                        if ($order_detail->voucher_setups_id != 0) {
                                            // purchase voucher

                                            $vs = VoucherSetups::find($order_detail->voucher_setups_id);
                                            if (!is_null($vs)) {
                                                // gift certificate
                                                if ($vs->voucher_type == 10) {
                                                    if (is_null($order_detail->bepoz_transaction_id)) {
                                                        $id_collection = array();
                                                        for ($i = 0; $i < $order_detail->qty; $i++) {
                                                            $transLine = [];
                                                            $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                            $itemEncode = [
                                                                'Training' => $trainingMode == "true" ? true : false,
                                                                'OperatorID' => $operatorID,
                                                                'TillID' => $tillID,
                                                                'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                'OrderID' => $orderID,
                                                                'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                'PaymentName' => 'GIFTCERT_SELL',
                                                                'AccountID' => $member->bepoz_account_id
                                                            ];
                                                            $transLine[] = $itemEncode;

                                                            $itemEncode = [
                                                                'Training' => $trainingMode == "true" ? true : false,
                                                                'OperatorID' => $operatorID,
                                                                'TillID' => $tillID,
                                                                'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                'OrderID' => $orderID,
                                                                'PaymentName' => $paymentName,
                                                                'AccountID' => $member->bepoz_account_id,
                                                                'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                            ];
                                                            $transLine[] = $itemEncode;

                                                            $bepoz_transaction = new BepozTransaction();
                                                            $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                            $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                            $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                            $temp3 = $bepoz->TransactionCreate($transLine);
                                                            if ($temp3) {
                                                                $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                $bepoz_transaction->status = "successful";

                                                                $id_collection[] = $temp3['Data1'];
                                                                $transaction_id_collection[] = $temp3['Data1'];

                                                            } else {
                                                                $bepoz_transaction->status = "failed";

                                                                $log = new SystemLog();
                                                                $log->type = 'bepoz-job-error';
                                                                $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                $log->payload = $bepoz_job->payload;
                                                                $log->message = 'Unable to create a transaction data.';
                                                                $log->source = 'CreateTransaction.php';
                                                                $log->save();

                                                                $complete_transaction = false;
                                                            }

                                                            $bepoz_transaction->save();
                                                        }

                                                        $temp = implode(",", $id_collection);
                                                        $order_detail->bepoz_transaction_id = $temp;
                                                        $order_detail->save();

                                                    } else {
                                                        $id_collection = explode(",", $order_detail->bepoz_transaction_id);
                                                        $transaction_id_collection = array_merge($transaction_id_collection, $id_collection);
                                                        if (count($id_collection) != $order_detail->qty) {
                                                            for ($i = count($id_collection); $i < $order_detail->qty; $i++) {
                                                                $transLine = [];
                                                                $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                    'PaymentName' => 'GIFTCERT_SELL',
                                                                    'AccountID' => $member->bepoz_account_id
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentName' => $paymentName,
                                                                    'AccountID' => $member->bepoz_account_id,
                                                                    'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $bepoz_transaction = new BepozTransaction();
                                                                $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                                $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                                $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                                $temp3 = $bepoz->TransactionCreate($transLine);
                                                                if ($temp3) {
                                                                    $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                    $bepoz_transaction->status = "successful";

                                                                    $id_collection[] = $temp3['Data1'];
                                                                    $transaction_id_collection[] = $temp3['Data1'];

                                                                } else {
                                                                    $bepoz_transaction->status = "failed";

                                                                    $log = new SystemLog();
                                                                    $log->type = 'bepoz-job-error';
                                                                    $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                    $log->payload = $bepoz_job->payload;
                                                                    $log->message = 'Unable to create a transaction data.';
                                                                    $log->source = 'CreateTransaction.php';
                                                                    $log->save();

                                                                    $complete_transaction = false;
                                                                }

                                                                $bepoz_transaction->save();
                                                            }
                                                        }
                                                        $temp = implode(",", $id_collection);
                                                        $order_detail->bepoz_transaction_id = $temp;
                                                        $order_detail->save();
                                                    }
                                                } else {

                                                    if (is_null($order_detail->bepoz_product_number) || $order_detail->bepoz_product_number === '0') {
                                                        continue;
                                                    }

                                                    // not gift certificate
                                                    $id_collection = array();

                                                    $transLine = [];
                                                    $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                    $itemEncode = [
                                                        'Training' => $trainingMode == "true" ? true : false,
                                                        'OperatorID' => $operatorID,
                                                        'TillID' => $tillID,
                                                        'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                        'OrderID' => $orderID,
                                                        'ProdNumber' => $order_detail->bepoz_product_number,
                                                        'QtySold' => $order_detail->qty,
                                                        'PaymentName' => $paymentName,
                                                        'AccountID' => $member->bepoz_account_id,
                                                        'Gross' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                        'Nett' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                        'PaymentAmount' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', '')
                                                    ];
                                                    $transLine[] = $itemEncode;

                                                    $bepoz_transaction = new BepozTransaction();
                                                    $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                    $bepoz_transaction->order_detail_id = $order_detail->id;
                                                    $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                    $temp3 = $bepoz->TransactionCreate($transLine);
                                                    if ($temp3) {
                                                        $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                        $bepoz_transaction->status = "successful";

                                                        $id_collection[] = $temp3['Data1'];
                                                        $transaction_id_collection[] = $temp3['Data1'];

                                                    } else {
                                                        $bepoz_transaction->status = "failed";

                                                        $log = new SystemLog();
                                                        $log->type = 'bepoz-job-error';
                                                        $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                        $log->payload = $bepoz_job->payload;
                                                        $log->message = 'Unable to create a transaction data.';
                                                        $log->source = 'CreateTransaction.php';
                                                        $log->save();

                                                        $complete_transaction = false;
                                                    }

                                                    $bepoz_transaction->save();

                                                    $temp = implode(",", $id_collection);
                                                    $order_detail->bepoz_transaction_id = $temp;
                                                    $order_detail->save();
                                                }
                                            }
                                        } else {
                                            // not voucher
                                            $id_collection = array();

                                            if (is_null($order_detail->bepoz_product_number) || $order_detail->bepoz_product_number === '0') {
                                                continue;
                                            }

                                            $transLine = [];
                                            $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                            $itemEncode = [
                                                'Training' => $trainingMode == "true" ? true : false,
                                                'OperatorID' => $operatorID,
                                                'TillID' => $tillID,
                                                'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                'OrderID' => $orderID,
                                                'ProdNumber' => $order_detail->bepoz_product_number,
                                                'QtySold' => $order_detail->qty,
                                                'PaymentName' => $paymentName,
                                                'AccountID' => $member->bepoz_account_id,
                                                'Gross' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                'Nett' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', ''),
                                                'PaymentAmount' => number_format((float)$order_detail->unit_price * $order_detail->qty, 2, '.', '')
                                            ];
                                            $transLine[] = $itemEncode;

                                            $bepoz_transaction = new BepozTransaction();
                                            $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                            $bepoz_transaction->order_detail_id = $order_detail->id;
                                            $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                            $temp3 = $bepoz->TransactionCreate($transLine);
                                            if ($temp3) {
                                                $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                $bepoz_transaction->status = "successful";

                                                $id_collection[] = $temp3['Data1'];
                                                $transaction_id_collection[] = $temp3['Data1'];

                                            } else {
                                                $bepoz_transaction->status = "failed";

                                                $log = new SystemLog();
                                                $log->type = 'bepoz-job-error';
                                                $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                $log->payload = $bepoz_job->payload;
                                                $log->message = 'Unable to create a transaction data.';
                                                $log->source = 'CreateTransaction.php';
                                                $log->save();

                                                $complete_transaction = false;
                                            }

                                            $bepoz_transaction->save();


                                            $temp = implode(",", $id_collection);
                                            $order_detail->bepoz_transaction_id = $temp;
                                            $order_detail->save();
                                        }

                                    }

                                    $order->bepoz_transaction_ids = \GuzzleHttp\json_encode($transaction_id_collection);
                                    $order->save();

                                    if ($complete_transaction) {
                                        if (!is_null($bepoz_job->failed_job_uid)) {
                                            $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                            if (!is_null($failed_job)) {
                                                $failed_job->delete();
                                            }
                                        }

                                        $bepoz_job->dispatch();
                                    } else {
                                        $bepoz_job->free();
                                    }

                                } elseif ($payload->transaction_type == 'gift_certificate') {
                                    $complete_transaction = true;
                                    $transaction_id_collection = array();

                                    foreach ($order->order_details as $order_detail) {
                                        if ($order_detail->voucher_setups_id != 0) {
                                            // purchase voucher

                                            $vs = VoucherSetups::find($order_detail->voucher_setups_id);
                                            if (!is_null($vs)) {
                                                if ($vs->voucher_type == 10) {
                                                    if (is_null($order_detail->bepoz_transaction_id)) {
                                                        $id_collection = array();
                                                        for ($i = 0; $i < $order_detail->qty; $i++) {
                                                            $transLine = [];
                                                            $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                            $itemEncode = [
                                                                'Training' => $trainingMode == "true" ? true : false,
                                                                'OperatorID' => $operatorID,
                                                                'TillID' => $tillID,
                                                                'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                'OrderID' => $orderID,
                                                                'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                'PaymentName' => 'GIFTCERT_SELL',
                                                                'AccountID' => $member->bepoz_account_id
                                                            ];
                                                            $transLine[] = $itemEncode;

                                                            $itemEncode = [
                                                                'Training' => $trainingMode == "true" ? true : false,
                                                                'OperatorID' => $operatorID,
                                                                'TillID' => $tillID,
                                                                'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                'OrderID' => $orderID,
                                                                'PaymentName' => $paymentName,
                                                                'AccountID' => $member->bepoz_account_id,
                                                                'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                            ];
                                                            $transLine[] = $itemEncode;

                                                            $bepoz_transaction = new BepozTransaction();
                                                            $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                            $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                            $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                            $temp3 = $bepoz->TransactionCreate($transLine);
                                                            if ($temp3) {
                                                                $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                $bepoz_transaction->status = "successful";

                                                                $id_collection[] = $temp3['Data1'];
                                                                $transaction_id_collection[] = $temp3['Data1'];

                                                            } else {
                                                                $bepoz_transaction->status = "failed";

                                                                $log = new SystemLog();
                                                                $log->type = 'bepoz-job-error';
                                                                $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                $log->payload = $bepoz_job->payload;
                                                                $log->message = 'Unable to create a transaction data.';
                                                                $log->source = 'CreateTransaction.php';
                                                                $log->save();

                                                                $complete_transaction = false;
                                                            }

                                                            $bepoz_transaction->save();
                                                        }

                                                        $temp = implode(",", $id_collection);
                                                        $order_detail->bepoz_transaction_id = $temp;
                                                        $order_detail->save();

                                                    } else {
                                                        $id_collection = explode(",", $order_detail->bepoz_transaction_id);
                                                        $transaction_id_collection = array_merge($transaction_id_collection, $id_collection);
                                                        if (count($id_collection) != $order_detail->qty) {
                                                            for ($i = count($id_collection); $i < $order_detail->qty; $i++) {
                                                                $transLine = [];
                                                                $orderID = Carbon::now(config('app.timezone'))->timestamp;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentAmount' => -(number_format((float)$order_detail->unit_price, 2, '.', '')),
                                                                    'PaymentName' => 'GIFTCERT_SELL',
                                                                    'AccountID' => $member->bepoz_account_id
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $itemEncode = [
                                                                    'Training' => $trainingMode == "true" ? true : false,
                                                                    'OperatorID' => $operatorID,
                                                                    'TillID' => $tillID,
                                                                    'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
                                                                    'OrderID' => $orderID,
                                                                    'PaymentName' => $paymentName,
                                                                    'AccountID' => $member->bepoz_account_id,
                                                                    'PaymentAmount' => number_format((float)$order_detail->unit_price, 2, '.', '')
                                                                ];
                                                                $transLine[] = $itemEncode;

                                                                $bepoz_transaction = new BepozTransaction();
                                                                $bepoz_transaction->order_id = $bepoz_payload->order_id;
                                                                $bepoz_transaction->order_detail_id = $order_detail->id . '-' . $i;
                                                                $bepoz_transaction->payload = \GuzzleHttp\json_encode($transLine);
                                                                $temp3 = $bepoz->TransactionCreate($transLine);
                                                                if ($temp3) {
                                                                    $bepoz_transaction->bepoz_transaction_id = $temp3['Data1'];
                                                                    $bepoz_transaction->status = "successful";

                                                                    $id_collection[] = $temp3['Data1'];
                                                                    $transaction_id_collection[] = $temp3['Data1'];

                                                                } else {
                                                                    $bepoz_transaction->status = "failed";

                                                                    $log = new SystemLog();
                                                                    $log->type = 'bepoz-job-error';
                                                                    $log->humanized_message = 'Creating Transaction is failed. Please check log.';
                                                                    $log->payload = $bepoz_job->payload;
                                                                    $log->message = 'Unable to create a transaction data.';
                                                                    $log->source = 'CreateTransaction.php';
                                                                    $log->save();

                                                                    $complete_transaction = false;
                                                                }

                                                                $bepoz_transaction->save();
                                                            }
                                                        }
                                                        $temp = implode(",", $id_collection);
                                                        $order_detail->bepoz_transaction_id = $temp;
                                                        $order_detail->save();
                                                    }
                                                } else {
                                                    $bepoz_job->dispatch();
                                                }
                                            }
                                        } else {
                                            // not gift voucher

                                            $bepoz_job->dispatch();
                                        }

                                    }

                                    $order->bepoz_transaction_ids = \GuzzleHttp\json_encode($transaction_id_collection);
                                    $order->save();

                                    if ($complete_transaction) {
                                        if (!is_null($bepoz_job->failed_job_uid)) {
                                            $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                            if (!is_null($failed_job)) {
                                                $failed_job->delete();
                                            }
                                        }

                                        $bepoz_job->dispatch();
                                    } else {
                                        $bepoz_job->free();
                                    }
                                }


                            } catch
                            (\Exception $e) {
                                $log = new SystemLog();
                                $log->humanized_message = 'Creating Transaction is failed. Please check the error message';
                                $log->type = 'bepoz-job-error';
                                $log->payload = $bepoz_job->payload;
                                $log->message = $e;
                                $log->source = 'CreateTransaction.php';
                                $log->save();

                                $bepoz_job->free();
                            }
                        } else {

                            if (is_null($bepoz_job->failed_job_uid)) {
                                $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                    ->where('queue', $bepoz_job->queue)
                                    ->first();

                                if (is_null($failed_job)) {
                                    $failed_job = new BepozFailedJob();
                                    $failed_job->queue = $bepoz_job->queue;
                                    $failed_job->payload = $bepoz_job->payload;
                                    $failed_job->job_uid = Uuid::generate(4);
                                    $failed_job->save();
                                }
                            }

                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->payload = $bepoz_job->payload;
                            $log->humanized_message = 'Maximum attempt of creating transaction has been reached.';
                            $log->source = 'CreateTransaction.php';
                            $log->save();

                            $bepoz_job->dispatch();
                        }
                    }
                }
                */

            }

        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Creating Transaction is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'CreateTransaction.php';
            $log->save();

        }
    }
}
