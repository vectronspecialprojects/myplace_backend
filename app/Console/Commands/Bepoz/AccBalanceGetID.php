<?php

namespace App\Console\Commands\Bepoz;

use App\Member;
use App\Setting;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\SystemLog;
use App\Jobs\Job;
use App\Helpers\Bepoz;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AccBalanceGetID extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'acc-balance-get-id {memberid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call Bepoz function AccBalanceGet with Bepoz ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            if (empty($this->argument('memberid'))) {
                // add delay to prevent duplicate
                sleep(30);
            } else {

                $member = Member::find($this->argument('memberid'));
                $user = $member->user;

                if (!is_null($user->member->bepoz_account_id)) {
                    if (intval($user->member->bepoz_account_id) > 0) {
                        $venue_number = Setting::where('key', 'venue_number')->first()->value;
                        $balance = null;

                        if ($venue_number == 'single') {
                            // $bepoz_api = Setting::where('key', 'bepoz_api')->first()->value;
                            // $bepoz_mac_key = Setting::where('key', 'bepoz_mac_key')->first()->value;
                            try {
                                $balance = $bepoz->AccBalanceGet($user->member->bepoz_account_id);
                            } catch (\Exception $e) {
                                Log::error($e);
                            }
                            // Log::info("balance venue single");

                        } else if ($venue_number == 'multiple') {
                            $venue_count = DB::table('venues')->count();

                            if ($venue_count > 0) {
                                if (intval($user->member->current_preferred_venue) > 0) {
                                    $venue = Venue::find($user->member->current_preferred_venue);
                                    if ($venue->gaming_system_point) {
                                        // Log::info("venue gaming_system_point");
                                        if ($venue->bepoz_api != "") {
                                            // Log::info("venue bepoz_api not empty");

                                            // Log::info($balance);
                                            try {
                                                $balance = $bepoz->VenueAccBalanceGet($user->member->current_preferred_venue,
                                                    $user->member->bepoz_account_id);
                                            } catch (\Exception $e) {
                                                Log::error($e);
                                            }

                                        } else {
                                            $log = new SystemLog();
                                            $log->type = 'bepoz-job-error';
                                            $log->humanized_message = 'Get acc balance is failed. Please check the error message';
                                            $log->message = 'Venue ' . $venue->name . ' not setup BEPOZ API URL correctly.';
                                            $log->source = 'AccBalanceGetID.php';
                                            $log->save();
                                        }
                                    }
                                } else {
                                    // Log::info($user->member->id . " venue 0");
                                }
                            } else {
                                $log = new SystemLog();
                                $log->type = 'setting-error';
                                $log->humanized_message = 'Venue number set multiple but venuw still empty.';
                                $log->payload = 'AccBalanceGetID.php';
                                $log->message = 'Venue invalid Setting.';
                                $log->source = 'AccBalanceGetID.php';
                                $log->save();
                            }

                            // Log::info("balance venue multiple");

                        }

                        if ($balance) {
                            // Log::info("balance not error");

                            if (intval($balance['AccBalance']['PointBalance']) >= 0) {
                                $user->member->points = intval($balance['AccBalance']['PointBalance']) / 100;
                            } else {
                                $log = new SystemLog();
                                $log->type = 'bepoz-job-error';
                                $log->humanized_message = 'Get acc balance receive negative point result';
                                $log->message = 'Call Bepoz AccBalanceGet from venue ' . $venue->name . ' is get negative value.';
                                $log->source = 'AccBalanceGetID.php';
                                $log->save();
                            }

                            if (intval($balance['AccBalance']['AccountBalance']) >= 0) {
                                $user->member->balance = intval($balance['AccBalance']['AccountBalance']) / 100;
                            } else {
                                $log = new SystemLog();
                                $log->type = 'bepoz-job-error';
                                $log->humanized_message = 'Get acc balance receive negative balance result';
                                $log->message = 'Call Bepoz AccBalanceGet from venue ' . $venue->name . ' is get negative value.';
                                $log->source = 'AccBalanceGetID.php';
                                $log->save();
                            }

                            $user->member->save();
                        } else {
                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->humanized_message = 'Get acc balance is failed. Please check the error message';
                            $log->message = 'Call Bepoz AccBalanceGet from venue ' . $venue->name . ' is failed. Please check connection or setting in the Venue BEPOZ API URL and MAC key';
                            $log->source = 'AccBalanceGetID.php';
                            $log->save();
                        }

                    } else {
                        // Log::info("bepoz_account_id 0");
                    }
                } else {
                    // Log::info("bepoz_account_id null");
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }
}
