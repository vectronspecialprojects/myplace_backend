<?php

namespace App\Console\Commands\Bepoz;

use App\Helpers\Bepoz;
use App\Jobs\SendDowngradeEmail;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberTiers;
use App\Setting;
use App\Tier;
use Carbon\Carbon;
use FontLib\Table\Type\name;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DowngradeMember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'downgrade-member';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downgrade Members';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {

            $tier_downgrade = Tier::find(1);
            $groupID = $tier_downgrade->bepoz_group_id;
            $template = $tier_downgrade->bepoz_template_id;
            $company_name = Setting::where('key', 'company_name')->first()->value;

            $member_tiers = MemberTiers::whereDate('date_expiry', '<=', Carbon::now(config('app.timezone'))->toDateString())
                ->where('tier_id', '>', '1')
                ->chunk(50, function ($member_tiers) use ($bepoz, $tier_downgrade, $groupID, $template, $company_name) {
                    
                    foreach ($member_tiers as $member_tier) {
                        // Log::info($member_tier);

                        $member = Member::with('user')->find($member_tier->member_id);

                        // Load data from Bepoz Account
                        $account = $bepoz->AccountFullGet($member->bepoz_account_id);

                        $request = $account['AccountFull'];
                        $request['GroupID'] = $groupID;
                        $request['DateTimeExpiry'] = '0001-01-01T00:00:00';

                        unset($request['AddressID']);
                        unset($request['CommentID']);
                        // Log::info($request);

                        $result = $bepoz->AccUpdate($request, $template);

                        if ($result){
                            dispatch(new SendDowngradeEmail($member->user));

                            $member_tier->tier_id = 1;
                            $member_tier->date_expiry = null;
                            $member_tier->save();

                            $data = [
                                // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                "contents" => ["en" => "Your membership is expired. You have been downgraded to ". $tier_downgrade->name],
                                "headings" => ["en" => $company_name],
                                "data" => [],
                                "ios_badgeType" => "Increase",
                                "ios_badgeCount" => 1
                            ];
                            dispatch(new SendOneSignalNotification($data));
                        }

 
                    }
                }
            );

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}
