<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\Setting;
use App\SystemLog;
use App\Tier;
use Illuminate\Console\Command;
use App\Helpers\Bepoz;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class SyncAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Bepoz Account';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            $ebet_gaming = Setting::where('key', 'ebet_gaming')->first()->value;

            $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;

            $bepozcustomfield_enable = Setting::where('key', 'bepozcustomfield_enable')->first()->value;

            $setting = Setting::where('key', 'bepozcustomfield')->first();

            $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);

            if ($gaming_system_enable == "false") {
                // NORMAL LOGIC
                $checkBepozConnection = $bepoz->SystemCheck();

                if ($checkBepozConnection) {
                    $app_flag_id_bepoz = Setting::where('key', 'app_flag_id_bepoz')->first()->value;
                    $app_flag_name_bepoz = Setting::where('key', 'app_flag_name_bepoz')->first()->value;

                    $jobs = BepozJob::where('reserved', '=', 0)
                        ->where('queue', '=', 'sync-account')
                        ->chunk(100, function ($jobs) use ($bepoz, $allCustomField, $gaming_system_enable, $bepozcustomfield_enable,
                            $app_flag_id_bepoz, $app_flag_name_bepoz) {

                            // Reserve the selected jobs first before the execution
                            foreach ($jobs as $bepoz_job) {
                                $bepoz_job->setJobUID(Uuid::generate());
                                $bepoz_job->reserve();
                            }

                            foreach ($jobs as $bepoz_job) {
                                // Log::error("SyncAccount");Log::error($bepoz_job->payload);
                                if ($bepoz_job->attempts() < 3) {
                                    $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                    $bepoz_job->save();

                                    try {
                                        $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                        $member = Member::find($data->member_id);

                                        // ALREADY HAVE BEPOZ ID THEN SKIP
                                        if (!is_null($member->bepoz_account_id)) {
                                            $bepoz_job->dispatch();
                                            continue;
                                        }

                                        $groupID = $member->member_tier->tier->bepoz_group_id;
                                        $template = $member->tiers()->first()->bepoz_template_id;
                                        $tier = $member->tiers()->first();

                                        // Create or Update Bepoz Account
                                        // search Bepoz Account from email
                                        $result = $bepoz->accountSearch(8, $member->user->email);
                                        $complete = false;
                                        $test = false;

                                        if ($result) {

                                            if ($result['IDList']['Count'] == 0) {

                                                // no account, get from create member backpanel
                                                $request = array();
                                                // AccountID 0 means create new so need group ID
                                                $request['AccountID'] = 0;

                                                // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                // if ($tier->manual_entry) {
                                                //     //use account number set from backpanel
                                                //     $accNumber = $member->bepoz_account_number;
                                                // } else 
                                                
                                                if ($gaming_system_enable == "true") {
                                                    //use account number set from backpanel
                                                    $accNumber = $member->bepoz_account_number;
                                                } else {
                                                    // $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                                    $accNumber = null;
                                                }
                                                $request['AccNumber'] = $accNumber;
                                                $request['CardNumber'] = $accNumber;
                                                $request['GroupID'] = $groupID;
                                                $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                $request['DateBirth'] = $member->dob;
                                                $request['Mobile'] = $member->user->mobile;
                                                $request['PhoneHome'] = $member->phone;
                                                $request['Email1st'] = $member->user->email;
                                                $request['PCode'] = $member->postcode;
                                                // NEW FEATURE opt out marketing from publinc
                                                $request['DoNotEmail'] = $member->opt_out_marketing;
                                                $request['DoNotSMS'] = $member->opt_out_marketing;

                                                // $test = $bepoz->AccUpdate($request, $template);
                                                $length = strlen($tier->boundary_start);
                                                $range = $tier->boundary_start.",".$tier->boundary_end;

                                                // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                // if ($tier->manual_entry) {
                                                //     //use account number set from backpanel, DISABLE AUTOGENERATE
                                                //     $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                // } else 
                                                
                                                if ($gaming_system_enable == "true") {
                                                    //use account number set from backpanel, DISABLE AUTOGENERATE
                                                    $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                } else {
                                                    // ENABLE AUTOGENERATE
                                                    $test = $bepoz->CreateAccount($request, $template, 1, $length, $range, null, 1);
                                                }

                                                if ($test) {
                                                    $member->bepoz_account_status = "OK";
                                                    $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                    $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                    $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                    $complete = true;
                                                }

                                            } else if ($result['IDList']['Count'] == 1) {
                                                // 1 account
                                                $temp = $bepoz->AccountFullGet($result['IDList']['ID']);

                                                if ($temp['AccountFull']['Status'] == 0) { // if the status is active

                                                    // acc active
                                                    if (empty($temp['AccountFull']['AccNumber'])) {

                                                        // acc active, no acc number, get from create member backpanel
                                                        $request = array();
                                                        // AccountID 0 means create new so need group ID
                                                        $request['AccountID'] = 0;

                                                        // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                        // if ($tier->manual_entry) {
                                                        //     //use account number set from backpanel
                                                        //     $accNumber = $member->bepoz_account_number;
                                                        // } else 
                                                        
                                                        if ($gaming_system_enable == "true") {
                                                            //use account number set from backpanel
                                                            $accNumber = $member->bepoz_account_number;
                                                        } else {
                                                            // $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                                            $accNumber = null;
                                                        }
                                                        $request['AccNumber'] = $accNumber;
                                                        $request['CardNumber'] = $accNumber;
                                                        $request['GroupID'] = $groupID;
                                                        $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                        $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                        $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                        $request['DateBirth'] = $member->dob;
                                                        $request['Mobile'] = $member->user->mobile;
                                                        $request['PhoneHome'] = $member->phone;
                                                        $request['Email1st'] = $member->user->email;
                                                        $request['PCode'] = $member->postcode;
                                                        // NEW FEATURE opt out marketing from publinc
                                                        $request['DoNotEmail'] = $member->opt_out_marketing;
                                                        $request['DoNotSMS'] = $member->opt_out_marketing;

                                                        // $test = $bepoz->AccUpdate($request, $template);
                                                        $length = strlen($tier->boundary_start);
                                                        $range = $tier->boundary_start.",".$tier->boundary_end;

                                                        // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                        // if ($tier->manual_entry) {
                                                        //     //use account number set from backpanel, DISABLE AUTOGENERATE
                                                        //     $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                        // } else 
                                                        
                                                        if ($gaming_system_enable == "true") {
                                                            //use account number set from backpanel, DISABLE AUTOGENERATE
                                                            $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                        } else {
                                                            // ENABLE AUTOGENERATE
                                                            $test = $bepoz->CreateAccount($request, $template, 1, $length, $range, null, 1);
                                                        }

                                                        if ($test) {

                                                            $member->bepoz_account_status = "OK";
                                                            $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                            $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                            $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                            $complete = true;
                                                        }

                                                    } else {

                                                        // there is acc number
                                                        $request = $temp['AccountFull'];
                                                        // EXISTING ACCOUNT NO NEED CHANGE GROUP ID
                                                        // $request['GroupID'] = $groupID;
                                                        // $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                        $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                        $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                        $request['DateBirth'] = $member->dob;
                                                        $request['Mobile'] = $member->user->mobile;
                                                        $request['PhoneHome'] = $member->phone;
                                                        $request['PCode'] = $member->postcode;
                                                        // NEW FEATURE opt out marketing from publinc
                                                        $request['DoNotEmail'] = $member->opt_out_marketing;
                                                        $request['DoNotSMS'] = $member->opt_out_marketing;
        
                                                        // EXISTING ACC NEED CHANGE CARD NUMBER
                                                        $CardNumberLength = strlen($request['CardNumber']);
                                                        if ($tier->force_card_range){
                                                            if ($CardNumberLength == 4 || $CardNumberLength == 5 || $CardNumberLength == 6 || $CardNumberLength == 7){
                                                                $newCardNumber = $this->bepozNextAccountNumber($bepoz, $tier);
                                                                $newCardNumberLength = strlen($newCardNumber);
                                                                $tierLength = strlen($tier->boundary_start);
                                                                if ( intval($newCardNumber) > 0 && $tierLength == $newCardNumberLength) {
                                                                    $request['AccNumber'] = $newCardNumber;
                                                                    $request['CardNumber'] = $newCardNumber;
                                                                } else {
                                                                    // DO NOTHING
                                                                }
                                                            }
                                                        }

                                                        unset($request['AddressID']);
                                                        unset($request['CommentID']);
                                                        // $test = $bepoz->AccUpdate($request, $template);
                                                        // $length = strlen($tier->boundary_start);
                                                        // $range = $tier->boundary_start.",".$tier->boundary_end;
                                                        $test = $bepoz->UpdateAccount($request, $template);

                                                        if ($test) {

                                                            $tier = Tier::where('bepoz_group_id', $test['AccountFull']['GroupID'])
                                                                ->where('is_active', true)
                                                                ->first();

                                                            $mt = MemberTiers::where('member_id', $member->id)->first();
                                                            $mt->date_expiry = $test['AccountFull']['DateTimeExpiry'];
                                                            if (!is_null($tier)) {
                                                                $mt->tier_id = $tier->id;
                                                            }
                                                            $mt->save();

                                                            // $pts = $this->getBalance($bepoz, $test['AccountFull']['AccountID']);
                                                            // $member->points = $pts["PointBalance"];
                                                            // $member->balance = $pts["AccountBalance"];
                                                            // $member->points = (intval($test['AccountFull']['PointsEarned']) - intval($test['AccountFull']['PointsRedeemed']) - intval($test['AccountFull']['PointsExpired'])) / 100;
                                                            // $member->balance = $test['AccountFull']['AccountBalance'] / 100;
                                                            $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                            $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                            $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                            $member->bepoz_existing_account = true;
                                                            $member->bepoz_account_status = "OK";
                                                            $complete = true;
                                                        }

                                                    }

                                                } else {

                                                    // acc not active, no acc number, get from create member backpanel
                                                    $request = array();
                                                    // AccountID 0 means create new so need group ID
                                                    $request['AccountID'] = 0;

                                                    // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                    // if ($tier->manual_entry) {
                                                    //     //use account number set from backpanel
                                                    //     $accNumber = $member->bepoz_account_number;
                                                    // } else 
                                                    
                                                    if ($gaming_system_enable == "true") {
                                                        //use account number set from backpanel
                                                        $accNumber = $member->bepoz_account_number;
                                                    } else {
                                                        // $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                                        $accNumber = null;
                                                    }
                                                    $request['AccNumber'] = $accNumber;
                                                    $request['CardNumber'] = $accNumber;
                                                    $request['GroupID'] = $groupID;
                                                    $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                    $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                    $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                    $request['DateBirth'] = $member->dob;
                                                    $request['Mobile'] = $member->user->mobile;
                                                    $request['PhoneHome'] = $member->phone;
                                                    $request['Email1st'] = $member->user->email;
                                                    $request['PCode'] = $member->postcode;
                                                    // NEW FEATURE opt out marketing from publinc
                                                    $request['DoNotEmail'] = $member->opt_out_marketing;
                                                    $request['DoNotSMS'] = $member->opt_out_marketing;

                                                    // $test = $bepoz->AccUpdate($request, $template);
                                                    $length = strlen($tier->boundary_start);
                                                    $range = $tier->boundary_start.",".$tier->boundary_end;

                                                    // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                    // if ($tier->manual_entry) {
                                                    //     //use account number set from backpanel, DISABLE AUTOGENERATE
                                                    //     $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                    // } else 
                                                    
                                                    if ($gaming_system_enable == "true") {
                                                        //use account number set from backpanel, DISABLE AUTOGENERATE
                                                        $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                    } else {
                                                        // ENABLE AUTOGENERATE
                                                        $test = $bepoz->CreateAccount($request, $template, 1, $length, $range, null, 1);
                                                    }

                                                    if ($test) {

                                                        $member->bepoz_account_status = "OK";
                                                        $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                        $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                        $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                        $complete = true;
                                                    }
                                                }

                                            } else {

                                                // many acc-s
                                                $created = false;
                                                foreach ($result['IDList']['ID']['int'] as $id) {
                                                    $temp = $bepoz->AccountFullGet($id);

                                                    if ($temp['AccountFull']['Status'] == 0) { // if the status is active

                                                        // there is active acc
                                                        if (empty($temp['AccountFull']['AccNumber'])) {

                                                            // acc active, no acc number, get from create member backpanel
                                                            $request = array();
                                                            // AccountID 0 means create new so need group ID
                                                            $request['AccountID'] = 0;

                                                            // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                            // if ($tier->manual_entry) {
                                                            //     //use account number set from backpanel
                                                            //     $accNumber = $member->bepoz_account_number;
                                                            // } else 
                                                            
                                                            if ($gaming_system_enable == "true") {
                                                                //use account number set from backpanel
                                                                $accNumber = $member->bepoz_account_number;
                                                            } else {
                                                                // $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                                                $accNumber = null;
                                                            }
                                                            $request['AccNumber'] = $accNumber;
                                                            $request['CardNumber'] = $accNumber;
                                                            $request['GroupID'] = $groupID;
                                                            $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                            $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                            $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                            $request['DateBirth'] = $member->dob;
                                                            $request['Mobile'] = $member->user->mobile;
                                                            $request['PhoneHome'] = $member->phone;
                                                            $request['Email1st'] = $member->user->email;
                                                            $request['PCode'] = $member->postcode;
                                                            // NEW FEATURE opt out marketing from publinc
                                                            $request['DoNotEmail'] = $member->opt_out_marketing;
                                                            $request['DoNotSMS'] = $member->opt_out_marketing;

                                                            // $test = $bepoz->AccUpdate($request, $template);
                                                            $length = strlen($tier->boundary_start);
                                                            $range = $tier->boundary_start.",".$tier->boundary_end;
                                                            // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                            // if ($tier->manual_entry) {
                                                            //     //use account number set from backpanel, DISABLE AUTOGENERATE
                                                            //     $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                            // } else 
                                                            
                                                            if ($gaming_system_enable == "true") {
                                                                //use account number set from backpanel, DISABLE AUTOGENERATE
                                                                $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                            } else {
                                                                // ENABLE AUTOGENERATE
                                                                $test = $bepoz->CreateAccount($request, $template, 1, $length, $range, null, 1);
                                                            }

                                                            if ($test) {

                                                                $member->bepoz_account_status = "OK";
                                                                $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                                $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                                $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                                $complete = true;
                                                            }

                                                        } else {

                                                            // there is acc number
                                                            $request = $temp['AccountFull'];
                                                            // EXISTING ACCOUNT NO NEED CHANGE GROUP ID
                                                            // $request['GroupID'] = $groupID;
                                                            // $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                            $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                            $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                            $request['DateBirth'] = $member->dob;
                                                            $request['Mobile'] = $member->user->mobile;
                                                            $request['PhoneHome'] = $member->phone;
                                                            $request['PCode'] = $member->postcode;
                                                            // NEW FEATURE opt out marketing from publinc
                                                            $request['DoNotEmail'] = $member->opt_out_marketing;
                                                            $request['DoNotSMS'] = $member->opt_out_marketing;

                                                            // EXISTING ACC NEED CHANGE CARD NUMBER
                                                            $CardNumberLength = strlen($request['CardNumber']);
                                                            if ($tier->force_card_range){
                                                                if ($CardNumberLength == 4 || $CardNumberLength == 5 || $CardNumberLength == 6 || $CardNumberLength == 7){
                                                                    $newCardNumber = $this->bepozNextAccountNumber($bepoz, $tier);
                                                                    $newCardNumberLength = strlen($newCardNumber);
                                                                    $tierLength = strlen($tier->boundary_start);
                                                                    if ( intval($newCardNumber) > 0 && $tierLength == $newCardNumberLength) {
                                                                        $request['AccNumber'] = $newCardNumber;
                                                                        $request['CardNumber'] = $newCardNumber;
                                                                    } else {
                                                                        // DO NOTHING
                                                                    }
                                                                }
                                                            }

                                                            unset($request['AddressID']);
                                                            unset($request['CommentID']);
                                                            // $result_update = $bepoz->AccUpdate($request, $template);
                                                            // $length = strlen($tier->boundary_start);
                                                            // $range = $tier->boundary_start.",".$tier->boundary_end;
                                                            $result_update = $bepoz->UpdateAccount($request, $template);

    
                                                            if ($result_update) {

                                                                $tier = Tier::where('bepoz_group_id', $result_update['AccountFull']['GroupID'])
                                                                    ->where('is_active', true)
                                                                    ->first();

                                                                $mt = MemberTiers::where('member_id', $member->id)->first();
                                                                $mt->date_expiry = $result_update['AccountFull']['DateTimeExpiry'];
                                                                if (!is_null($tier)) {
                                                                    $mt->tier_id = $tier->id;
                                                                }
                                                                $mt->save();

                                                                // $pts = $this->getBalance($bepoz, $result_update['AccountFull']['AccountID']);
                                                                // $member->points = $pts["PointBalance"];
                                                                // $member->balance = $pts["AccountBalance"];
                                                                // $member->points = (intval($test['AccountFull']['PointsEarned']) - intval($test['AccountFull']['PointsRedeemed']) - intval($test['AccountFull']['PointsExpired'])) / 100;
                                                                // $member->balance = $test['AccountFull']['AccountBalance'] / 100;
                                                                $member->bepoz_account_number = $result_update['AccountFull']['AccNumber'];
                                                                $member->bepoz_account_card_number = $result_update['AccountFull']['CardNumber'];
                                                                $member->bepoz_account_id = $result_update['AccountFull']['AccountID'];
                                                                $member->bepoz_existing_account = true;
                                                                $member->bepoz_account_status = "OK";
                                                                $complete = true;
                                                            }

                                                        }
                                                        $created = true;
                                                        break;
                                                    } else {

                                                        // account not active, no acc number, get from create member backpanel
                                                        $request = array();
                                                        // AccountID 0 means create new so need group ID
                                                        $request['AccountID'] = 0;

                                                        // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                        // if ($tier->manual_entry) {
                                                        //     //use account number set from backpanel
                                                        //     $accNumber = $member->bepoz_account_number;
                                                        // } else 
                                                        
                                                        if ($gaming_system_enable == "true") {
                                                            //use account number set from backpanel
                                                            $accNumber = $member->bepoz_account_number;
                                                        } else {
                                                            // $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                                            $accNumber = null;
                                                        }
                                                        $request['AccNumber'] = $accNumber;
                                                        $request['CardNumber'] = $accNumber;
                                                        $request['GroupID'] = $groupID;
                                                        $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                        $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                        $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                        $request['DateBirth'] = $member->dob;
                                                        $request['Mobile'] = $member->user->mobile;
                                                        $request['PhoneHome'] = $member->phone;
                                                        $request['Email1st'] = $member->user->email;
                                                        $request['PCode'] = $member->postcode;
                                                        // NEW FEATURE opt out marketing from publinc
                                                        $request['DoNotEmail'] = $member->opt_out_marketing;
                                                        $request['DoNotSMS'] = $member->opt_out_marketing;

                                                        // $test = $bepoz->AccUpdate($request, $template);
                                                        $length = strlen($tier->boundary_start);
                                                        $range = $tier->boundary_start.",".$tier->boundary_end;
                                                        
                                                        // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                        // if ($tier->manual_entry) {
                                                        //     //use account number set from backpanel, DISABLE AUTOGENERATE
                                                        //     $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                        // } else 
                                                        
                                                        if ($gaming_system_enable == "true") {
                                                            //use account number set from backpanel, DISABLE AUTOGENERATE
                                                            $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                        } else {
                                                            // ENABLE AUTOGENERATE
                                                            $test = $bepoz->CreateAccount($request, $template, 1, $length, $range, null, 1);
                                                        }

                                                        if ($test) {
                                                            $member->bepoz_account_status = "OK";
                                                            $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                            $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                            $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                            $created = true;
                                                            $complete = true;
                                                        }

                                                        break;
                                                    }
                                                }

                                                if (!$created) {

                                                    // no valid account, get account number from create member backpanel
                                                    $request = array();
                                                    // AccountID 0 means create new so need group ID
                                                    $request['AccountID'] = 0;

                                                    // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                    // if ($tier->manual_entry) {
                                                    //     //use account number set from backpanel
                                                    //     $accNumber = $member->bepoz_account_number;
                                                    // } else 
                                                    
                                                    if ($gaming_system_enable == "true") {
                                                        //use account number set from backpanel
                                                        $accNumber = $member->bepoz_account_number;
                                                    } else {
                                                        // $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                                        $accNumber = null;
                                                    }
                                                    $request['AccNumber'] = $accNumber;
                                                    $request['CardNumber'] = $accNumber;
                                                    $request['GroupID'] = $groupID;
                                                    $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                    $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                    $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                    $request['DateBirth'] = $member->dob;
                                                    $request['Mobile'] = $member->user->mobile;
                                                    $request['PhoneHome'] = $member->phone;
                                                    $request['Email1st'] = $member->user->email;
                                                    $request['PCode'] = $member->postcode;
                                                    // NEW FEATURE opt out marketing from publinc
                                                    $request['DoNotEmail'] = $member->opt_out_marketing;
                                                    $request['DoNotSMS'] = $member->opt_out_marketing;

                                                    // $test = $bepoz->AccUpdate($request, $template);
                                                    $length = strlen($tier->boundary_start);
                                                    $range = $tier->boundary_start.",".$tier->boundary_end;
                                                    
                                                    // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                    // if ($tier->manual_entry) {
                                                    //     //use account number set from backpanel, DISABLE AUTOGENERATE
                                                    //     $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                    // } else 
                                                    
                                                    if ($gaming_system_enable == "true") {
                                                        //use account number set from backpanel, DISABLE AUTOGENERATE
                                                        $test = $bepoz->CreateAccount($request, $template, 0, $length, $range, null, 1);
                                                    } else {
                                                        // ENABLE AUTOGENERATE
                                                        $test = $bepoz->CreateAccount($request, $template, 1, $length, $range, null, 1);
                                                    }

                                                    if ($test) {
                                                        $member->bepoz_account_status = "OK";
                                                        $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                        $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                        $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                        $complete = true;
                                                    }

                                                }
                                            }

                                            if ($complete) {
                                                $member->save();

                                                //FEATURE FROM VARSITY NEED MAKE GLOBAL SETTING
                                                // $this->setCustomField($bepoz, $member, "Sponsorship Code");
                                                // $this->setCustomField($bepoz, $member, "Promo Code");

                                                if (!is_null($bepoz_job->failed_job_uid)) {
                                                    $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                    if (!is_null($failed_job)) {
                                                        $failed_job->delete();
                                                    }
                                                }

                                                // NEED SEND BEPOZ ID TO SSO
                                                $this->sendSSO($member);

                                                if (isset($data->type)) {
                                                    $option = Setting::where('key', 'reward_after_signup_option')->first();
                                                    $type = Setting::where('key', 'reward_after_signup_type')->first();

                                                    if (!is_null($option) && !is_null($type)) {

                                                        if ($option->value == 'true' && $type->value == 'point') {
                                                            $point = 0;

                                                            $ml = new MemberLog();
                                                            $ml->member_id = $member->id;
                                                            if ($data->type === 'facebook') {
                                                                $point = Setting::where('key', 'facebook_point_reward')->first()->value;
                                                                $ml->message = "This member got point rewards from social login ($data->type)";

                                                            } elseif ($data->type === 'gmail') {
                                                                $point = Setting::where('key', 'google_point_reward')->first()->value;
                                                                $ml->message = "This member got point rewards from social login ($data->type)";

                                                            } elseif ($data->type === 'email') {
                                                                $point = Setting::where('key', 'reward_after_signup_point')->first()->value;
                                                                $ml->message = "This member got point rewards from signup";
                                                            }


                                                            $ml->after = \GuzzleHttp\json_encode([
                                                                'earn' => $point
                                                            ]);
                                                            $ml->save();

                                                            // sync point
                                                            $data = array(
                                                                "point" => abs(floatval($point)),
                                                                "id" => $member->bepoz_account_id,
                                                                "member_id" => $member->id,
                                                                "status" => 'earn'
                                                            );

                                                            $job = new BepozJob();
                                                            $job->queue = "sync-point";
                                                            $job->payload = \GuzzleHttp\json_encode($data);
                                                            $job->available_at = time();
                                                            $job->created_at = time();
                                                            $job->save();

                                                            $member->points = floatval($member->points) + floatval($point);
                                                            $member->save();

                                                            $company_name = Setting::where('key', 'company_name')->first()->value;

                                                            $data = [
                                                                // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                                "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                                "contents" => ["en" => "You got point rewards (from signup)! $point pts. is added into your account shortly"],
                                                                "headings" => ["en" => $company_name],
                                                                "data" => ["category" => "email"],
                                                                "ios_badgeType" => "Increase",
                                                                "ios_badgeCount" => 1
                                                            ];

                                                            $job = (new SendOneSignalNotification($data))->delay(360);
                                                            dispatch($job);
                                                        }


                                                    }


                                                }

                                                if ($bepozcustomfield_enable == "true") {
                                                    // SEND BEPOZ CUSTOM FIELD AT ONCE
                                                    $resultCustomFields = $this->customFieldsSet($member, $allCustomField, $bepoz, 
                                                        $app_flag_id_bepoz, $app_flag_name_bepoz);
                                                    // Log::warning($resultCustomFields);
                                                }

                                                $bepoz_job->dispatch();
                                            } else {
                                                $log = new SystemLog();
                                                $log->type = 'bepoz-job-error';
                                                $log->humanized_message = 'Syncing account is failed. Please check log.';
                                                $log->payload = $bepoz_job->payload;
                                                $log->message = $test;
                                                $log->source = 'SyncAccount.php';
                                                $log->save();

                                                // try again
                                                $bepoz_job->free();

                                            }

                                        } else {
                                            $log = new SystemLog();
                                            $log->type = 'bepoz-job-error';
                                            $log->humanized_message = 'Syncing account is failed. Please check log.';
                                            $log->payload = $bepoz_job->payload;
                                            $log->message = $result;
                                            $log->source = 'SyncAccount.php';
                                            $log->save();

                                            // try again
                                            $bepoz_job->free();

                                        }
                                    } catch (\Exception $e) {
                                        $log = new SystemLog();
                                        $log->type = 'bepoz-job-error';
                                        $log->humanized_message = 'Syncing account is failed. Please check the error message.';
                                        $log->message = $e;
                                        $log->payload = $bepoz_job->payload;
                                        $log->source = 'SyncAccount.php';
                                        $log->save();

                                        $bepoz_job->free();
                                    }

                                } else {
                                    if (is_null($bepoz_job->failed_job_uid)) {

                                        $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                            ->where('queue', $bepoz_job->queue)
                                            ->first();

                                        if (is_null($failed_job)) {
                                            $failed_job = new BepozFailedJob;
                                            $failed_job->queue = $bepoz_job->queue;
                                            $failed_job->payload = $bepoz_job->payload;
                                            $failed_job->job_uid = Uuid::generate(4);
                                            $failed_job->save();
                                        }
                                    }

                                    // Maximum attempt log no longer needed
                                    // $log = new SystemLog();
                                    // $log->type = 'bepoz-job-error';
                                    // $log->humanized_message = 'Syncing account is failed. Maximum attempts has been reached.';
                                    // $log->source = 'SyncAccount.php';
                                    // $log->payload = $bepoz_job->payload;
                                    // $log->save();

                                    $bepoz_job->dispatch();

                                }

                            }
                        }
                    );

                }

            } else if ($gaming_system_enable == "true") {
                // DOING NOTHING, app will trigger matching account

            }

        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Syncing account is failed. Please check the error message.';
            $log->message = $e;
            $log->source = 'SyncAccount.php';
            $log->save();

        }

    }

    protected function getNewAccNumber($tier)
    {

        $rangeValue = "(bepoz_account_number BETWEEN '$tier->boundary_start' AND '$tier->boundary_end')";

        $member = Member::whereNotNull('bepoz_account_number')
            ->whereRaw('(CHAR_LENGTH(bepoz_account_number) >= ' . strlen($tier->boundary_start) . ' AND CHAR_LENGTH(bepoz_account_number) <= ' . strlen($tier->boundary_end) . ')')
            ->whereRaw($rangeValue)
            ->whereHas('member_tier.tier', function ($query) use ($tier) {
                $query->where('id', '=', $tier->id);
            })
            ->orderBy('id', 'desc')
            ->first();

        if (is_null($member)) {
            if (!empty($tier->boundary_start)) {
                $newACC = $tier->boundary_start;
            } else {
                $newACC = "0";
            }

            $newACC++;
        } else {
            $member->bepoz_account_number++;
            $newACC = $this->tierLimitChecker($tier, $member->bepoz_account_number);
        }

        if (!empty($tier->boundary_start)) {
            if (strlen($newACC) < strlen($tier->boundary_start)) {
                $newACC = str_pad($newACC, strlen($tier->boundary_start), '0', STR_PAD_LEFT);
            }
        }

        return $newACC;
    }

    protected
    function recursiveAccNumber($id, $bepoz, $tier)
    {
        if ($bepoz->accountSearch(4, $id)['IDList']['Count'] == 0) {
            return $id;
        } else {
            $id++;
            if (!empty($tier->boundary_start)) {
                if (strlen($id) < strlen($tier->boundary_start)) {
                    $id = str_pad($id, strlen($tier->boundary_start), '0', STR_PAD_LEFT);
                }
            }
            return $this->recursiveAccNumber($id, $bepoz, $tier);
        }
    }

    protected function tierLimitChecker($tier, $id)
    {
        // if (intval($id) >= intval($tier->boundary_start) && intval($id) <= intval($tier->boundary_end)) {
        //     return $id;
        // } else {
        //     return $tier->boundary_start;
        // }

        if (strnatcmp($id, $tier->boundary_start) >= 0 && strnatcmp($id, $tier->boundary_end) <= 0) {
            return $id;
        } else {
            return $tier->boundary_start;
        }
    }

    function checkEmoji($str)
    {
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        preg_match($regexEmoticons, $str, $matches_emo);
        if (!empty($matches_emo[0])) {
            return false;
        }

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        preg_match($regexSymbols, $str, $matches_sym);
        if (!empty($matches_sym[0])) {
            return false;
        }

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        preg_match($regexTransport, $str, $matches_trans);
        if (!empty($matches_trans[0])) {
            return false;
        }

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        preg_match($regexMisc, $str, $matches_misc);
        if (!empty($matches_misc[0])) {
            return false;
        }

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        preg_match($regexDingbats, $str, $matches_bats);
        if (!empty($matches_bats[0])) {
            return false;
        }

        return true;
    }

    function setCustomField($bepoz, $member, $CustomFieldKey)
    {

        $setting = Setting::where('key', 'bepozcustomfield')->first();

        $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);
        $CustomFieldNumber = "0";

        foreach ($allCustomField as $data) {
            if ($data->key == $CustomFieldKey) {
                $CustomFieldNumber = $data->field;
            }
        }

        //Log::info($CustomFieldNumber);
        $dataset = "";
        // if ($CustomFieldKey == "Sponsorship Code") {
        //     $dataset = $member->sponsorship_code;
        // } else if ($CustomFieldKey == "Promo Code") {
        //     $dataset = $member->promo_code;
        // }

        $param = ['AccountID' => $member->bepoz_account_id,
            'CustomFieldNumber' => $CustomFieldNumber,
            'DataSet' => $dataset
        ];
        $customFieldResult = $bepoz->AccCustomFieldSet($param);
    }

    public function customFieldsSet($member, $allCustomField, $bepoz, $app_flag_id_bepoz, $app_flag_name_bepoz)
    {
        $customFields = array();

        $memberCustomFields = \GuzzleHttp\json_decode($member->payload);

        foreach ($allCustomField as $cf) {
            $typeCode = 0;
            switch ($cf->fieldType) {
                case "Text":
                    $typeCode = 3;
                    break;
                case "Number":
                    $typeCode = 2;
                    break;
                case "Date":
                    $typeCode = 1;
                    break;
                case "Dropdown":
                    $typeCode = 3;
                    break;
                case "Checkbox":
                    $typeCode = 3;
                    break;
                case "Toggle":
                    $typeCode = 0;
                    break;
            }

            $bepozFieldNum = $cf->bepozFieldNum;
            if (strlen(trim($cf->bepozFieldNum)) == 3) {
                $bepozFieldNum = substr($cf->bepozFieldNum, -2);
            }

            $customFields['CustomField'][] = array(
                'FieldName' => $cf->fieldName,
                'FieldType' => $typeCode,
                'FieldValue' => array_column($memberCustomFields, $cf->id)[0],
                'FieldNum' => $bepozFieldNum
            );
        }
        
        if ($app_flag_id_bepoz != "false") {
            $customFieldUpdate = true;

            $customFields['CustomField'][] = array(
                'FieldName' => $app_flag_name_bepoz,
                'FieldType' => 0,
                'FieldValue' => true,
                'FieldNum' => $app_flag_id_bepoz
            );
        }

        $EntityType = 0;
        $EntityID = $member->bepoz_account_id;

        $customFieldResult = $bepoz->CustomFieldsSet($EntityType, $EntityID, $customFields);

        return $customFieldResult;
    }

    function sendSSO($member)
    {

        //NEW LOGIC SEND INTEGRATION BASED ON VENUE
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datasend = array(
            "email" => $member->user->email,
            "bepoz_id" => $member->bepoz_account_id,
            "bepoz_account_number" => $member->bepoz_account_number,
            "bepoz_account_card_number" => $member->bepoz_account_card_number,
            "guid" => $member->login_token,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/updateAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::warning("SyncAccount sendSSO");
        // Log::warning($content);
        // Log::warning($httpCode);

        if ($content) {
            $payload = \GuzzleHttp\json_decode($content);

            if (isset($payload->status) && isset($payload->error)) {
                if ($payload->status == "error" && $payload->error == "user_not_found") {
                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Syncing account is failed. Please check the error message.';
                    $log->message = $payload->error;
                    $log->source = 'SyncAccount.php';
                    $log->save();
                }

                if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Syncing account is failed. Please check the error message.';
                    $log->message = $payload->error;
                    $log->source = 'SyncAccount.php';
                    $log->save();
                }
            }

            // SUCCESS
            $log = new SystemLog();
            $log->type = 'bepoz-job';
            $log->humanized_message = 'Syncing account SSO is success';
            $log->message = "Email = " . $member->user->email
                . "\nBepoz ID = " . $member->bepoz_account_id
                . "\nBepoz Account Number = " . $member->bepoz_account_number
                . "\nBepoz Account Card Number = " . $member->bepoz_account_card_number;
            $log->source = 'SyncAccount.php';
            $log->save();

            $ml = new MemberLog();
            $ml->member_id = $member->id;
            $ml->message = "Syncing account SSO is success"
                . "\nEmail = " . $member->user->email
                . "\nBepoz ID = " . $member->bepoz_account_id
                . "\nBepoz Account Number = " . $member->bepoz_account_number
                . "\nBepoz Account Card Number = " . $member->bepoz_account_card_number;
            $ml->after = \GuzzleHttp\json_encode($member->user);
            $ml->save();

        } else {
            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Syncing account is failed. Please check the error message.';
            $log->message = "SSO connection problem";
            $log->source = 'SyncAccount.php';
            $log->save();
        }

    }

    private function getBalance($bepoz, $account_id)
    {
        $result = array(
            "PointBalance" => 0,
            "AccountBalance" => 0
        );

        try {
            $temp = $bepoz->AccBalanceGet($account_id);
            if ($temp) {
                $result['PointBalance'] = intval($temp['AccBalance']['PointBalance']) / 100;
                $result['AccountBalance'] = intval($temp['AccBalance']['AccountBalance']) / 100;
            }
        } catch (\Exception $exception) {

        }

        return $result;
    }

    function bepozNextAccountNumber($bepoz, $tier) {

        $account_group = $tier->bepoz_group_id;
        $max_length = strlen($tier->boundary_start);
        $range = $tier->boundary_start.",".$tier->boundary_end;

        $result = $bepoz->NextAccountNumber($account_group, $max_length, $range);

        if ($result){
            $idPad = str_pad($result['Data1'], strlen($tier->boundary_start), '0', STR_PAD_LEFT);
            return $idPad;
        }

        return false;
    }

}
