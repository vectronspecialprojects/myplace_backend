<?php

namespace App\Console\Commands\Bepoz;


use App\ClaimedPromotion;
use App\PendingJob;
use App\Helpers\Bepoz;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberVouchers;
use App\Setting;
use App\SystemLog;
use App\VoucherSetups;
use Carbon\Carbon;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SystemCheckEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system-check-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check there is connection error and email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */

    public function handle(Bepoz $bepoz, MandrillExpress $mx, MailjetExpress $mjx)
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $timeNow = Carbon::now(config('app.timezone'));
            $timeFIveMinutesAgo = $timeNow->copy()->subMinute(5);

            $log = SystemLog::where('type', 'bepoz-connection-error')
                ->where('created_at', '>', $timeFIveMinutesAgo)->first();

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if (!is_null($log)) {
                $logtime = Carbon::parse($log->created_at)->format('D, M d, Y H:i:s');
                $setting = Setting::where('key', 'system_check_email')->first();
                $email_send = \GuzzleHttp\json_decode($setting->extended_value);

                foreach ($email_send as $row) {
                    $mx = new MandrillExpress;
                    $data = array(
                        'FIRSTNAME' => 'ADMIN',
                        'TIME' => (string)$logtime
                    );

                    if ($email_server === "mandrill") {

                        if ($mx->init()) {
                            $mx->send('system_check', 'ADMIN', $row, $data);
                        } else {
                            $pj = new PendingJob();
                            $pj->payload = \GuzzleHttp\json_encode($data);
                            $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                'email' => $row,
                                'category' => 'system-check-vectron',
                                'name' => 'ADMIN'
                            ));
                            $pj->queue = "email";
                            $pj->save();
                        }

                    } else if ($email_server === "mailjet") {

                        if ($mjx->init()) {
                            $mjx->send('system_check', 'ADMIN', $row, $data);
                        } else {
                            $pj = new PendingJob();
                            $pj->payload = \GuzzleHttp\json_encode($data);
                            $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                'email' => $row,
                                'category' => 'system-check-vectron',
                                'name' => 'ADMIN'
                            ));
                            $pj->queue = "email";
                            $pj->save();
                        }

                    }

                }

            }

        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'system-check-email-error';
            $log->humanized_message = 'Check Bepoz Connection Status is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SystemCheckEmail.php';
            $log->save();

        }

    }

}
