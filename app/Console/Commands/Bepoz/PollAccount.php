<?php

namespace App\Console\Commands\Bepoz;

use App\FriendReferral;
use App\Helpers\Bepoz;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\MyplaceCache;
use App\Setting;
use App\SystemLog;
use App\Tier;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PollAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poll-account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cloud accounts based on the data given';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        try {
            // Log::info("PollAccount START");
            // Log::info(Carbon::now(config('app.timezone')));

            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
            $last_execution_time = Setting::where('key', 'bepoz_account_last_successful_execution_time')->first();
            $datetime = new Carbon($last_execution_time->value);

            $payload = [];
            $payload['procedure'] = "AccountsUpdated";
            // $payload['parameters'] = [$last_execution_time->value];
            $payload['parameters'] = [$datetime->format('Y-m-d H:i:s')];

            // $cache = MyplaceCache::where('key', 'member_ids')->first();
            // $payload['procedure'] = "AccountsUpdatedMembers";
            // $payload['parameters'] = [$datetime->format('Y-m-d H:i:s'), $cache->value];

            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::error($content);
            // Log::error($httpCode);


            if ($httpCode >= 200 && $httpCode < 300) {
                // pass
                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);
                    if (!$payload->error) {
                        $last_execution_time->value = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
                        $last_execution_time->save();

                        // $payloadReady = array_filter($payload->data, function($k) {
                        //     return isset($k->AccountID) && !empty($k->AccountID);
                        // });
                        // Log::warning(print_r($payloadReady, true));

                        $payloadReady = $payload->data;
                        // Log::warning(print_r($payloadReady, true));


                        $chunkSize = 300;
                        $iChunk = 0;
                        $payloadChunks = array_chunk($payloadReady, $chunkSize, true);
                        // CHUNK BEPOZ RESULT
                        foreach ($payloadChunks as &$payloadChunk) {

                            
                            $bepoz_ids = array_column($payloadChunk, 'accountid');
                            // Log::warning(print_r($bepoz_ids, true));


                            foreach ($payloadChunk as $key => &$rowChunk) {
                                $rowChunk->foundmyplace = "false";
                            }


                            $members = Member::whereIn('bepoz_account_id', $bepoz_ids)
                                ->chunk(100, function ($members) use (&$payloadChunk, $bepoz_ids, $iChunk, $chunkSize) {
    
                                    // FILTER VOUCHER WITH BEPOZ ID
                                    foreach ($members as $member) {
                                        // Log::critical("bepoz_account_id ". $member->bepoz_account_id);

                                        $found_key_payload = array_search($member->bepoz_account_id, $bepoz_ids);
                                        if ($found_key_payload >= 0) {
                                            $indexTarget = $iChunk * $chunkSize + $found_key_payload;
                                            // Log::critical($indexTarget);

                                            // Log::critical("found_key_payload ". $found_key_payload);
                                            // Log::critical("indexTarget ". $indexTarget);

                                            $data = $payloadChunk[$indexTarget];
                                            $payloadChunk[$indexTarget]->foundmyplace = "true";

                                            // Log::critical(print_r($data, true));

                                            $this->memberUpdate($member, $data);
                                        }

                                    }

                                }
                            );
                            
                            $iChunk++;
                        }

                    }
                } else {
                    //    Log::error(curl_error($ch));
                    //    Log::error(curl_errno($ch));

                    //    $log = new SystemLog();
                    //    $log->type = 'bepoz-job-error';
                    //    $log->humanized_message = 'Polling account is failed. Please check the error message';
                    //    $log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                    //    $log->source = 'PollAccount.php';
                    //    $log->save();
                }
            } else {
                // 404
                // Log::error($httpCode);

                // $log = new SystemLog();
                // $log->type = 'bepoz-job-error';
                // $log->humanized_message = 'Polling account is failed. Please check the error message';
                // $log->message = \GuzzleHttp\json_encode(array("error" => '404 Not Found', "error_code" => $httpCode));
                // $log->source = 'PollAccount.php';
                // $log->save();
            }

            // Log::info("PollAccount END");
            // Log::info(Carbon::now(config('app.timezone')));
            
        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Polling account is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'PollAccount.php';
            $log->save();

        }
    }


    protected function memberUpdate($member, $data)
    {
        
        $tier = Tier::where('bepoz_group_id', $data->groupid)
            ->where('is_active', true)
            ->first();

        // Log::warning($tier);
        // Log::warning(print_r($data, true));

            if (!is_null($member)) {
                // if (!isset($data->email) || empty($data->email)) {
                //     continue;
                // }

                if (strtolower($data->email) == strtolower($member->user->email)) {
                    // Log::warning($data);

                    $user = $member->user;
                    $cloned = $user->member->replicate();

                    $updateAccount = false;

                    if (!$tier->dont_update_from_bepoz) {
                        // $member->user->email = empty($data->email) ? "guest+".Carbon::now('UTC')->timestamp."@vectron.com.au" : $data->email;
                        if ($member->first_name != $data->firstname) {
                            $updateAccount = true;
                            $member->first_name = $data->firstname;
                            // Log::info($member->first_name." first_name ".$data->firstname);
                        }

                        if ($member->last_name != $data->lastname) {
                            $updateAccount = true;
                            $member->last_name = $data->lastname;
                            // Log::info($member->last_name." lastname ".$data->lastname);
                        }
                    }

                    if ($member->bepoz_account_number != $data->accnumber) {
                        $updateAccount = true;
                        $member->bepoz_account_number = $data->accnumber;
                        // Log::info($member->bepoz_account_number." accnumber ".$data->accnumber);
                    }

                    if ($member->bepoz_account_card_number != $data->cardnumber) {
                        $updateAccount = true;
                        $member->bepoz_account_card_number = $data->cardnumber;
                        // Log::info($member->bepoz_account_card_number." cardnumber ".$data->cardnumber);
                    }

                    
                    if ($member->points != (intval($data->points) / 100) ) {
                        $updateAccount = true;
                        $member->points = intval($data->points) / 100;
                        // Log::info($member->points." points ".intval($data->points) / 100);
                    }

                    if ($member->phone != $data->phonehome) {
                        $updateAccount = true;
                        $member->phone = $data->phonehome;
                        // Log::info($member->phone." phonehome ".$data->phonehome);
                    }

                    $emptyoldDOB = empty($member->dob);
                    $emptynewDOB = empty($data->datebirth);

                    if ( $emptyoldDOB || $emptynewDOB) {
                        // no need update
                        // Log::info("no need update datebirth ");
                    } else {
                        $oldDOB = new Carbon($member->dob);
                        $newDOB = new Carbon($data->datebirth);

                        if ( $oldDOB->ne($newDOB) ) {
                            $updateAccount = true;
                            $member->dob = $data->datebirth;
                            // Log::info($member->dob." datebirth ".$data->datebirth);
                        }
                    }

                    if ($member->balance != (intval($data->balance) / 100)) {
                        $updateAccount = true;
                        $member->balance = intval($data->balance) / 100;
                        // Log::info($member->balance." balance ".intval($data->balance) / 100);
                    }

                    if (isset($data->usecalinkbal)) {
                        if ($member->bepoz_UseCAlinkBalance != $data->usecalinkbal) {
                            $updateAccount = true;
                            $member->bepoz_UseCAlinkBalance = $data->usecalinkbal;
                            // Log::info($member->bepoz_UseCAlinkBalance." usecalinkbal ".$data->usecalinkbal);
                        }
                    }

                    if (isset($data->usecalinkpnts)) {
                        if ($member->bepoz_UseCALinkPoints != $data->usecalinkpnts) {
                            $updateAccount = true;
                            $member->bepoz_UseCALinkPoints = $data->usecalinkpnts;
                            // Log::info($member->bepoz_UseCALinkPoints." usecalinkpnts ".$data->usecalinkpnts);
                        }
                    }

                    if (intval($member->bepoz_count_visits) !== intval($data->countvisits)) {
                        $friend_referral = FriendReferral::where('has_made_a_purchase', false)
                            ->where('new_member_id', $member->id)
                            ->where('is_rewarded', false)
                            ->where('is_accepted', true)
                            ->where('has_joined', true)
                            ->where('reward_option', 'after_purchase')
                            ->first();

                        if (!is_null($friend_referral)) {
                            $friend_referral->has_made_a_purchase = true;
                            $friend_referral->made_purchase_at = Carbon::now(config('app.timezone'));
                            $friend_referral->save();
                        }
                    }

                    if ( intval($member->bepoz_count_visits) != intval($data->countvisits) ) {
                        $updateAccount = true;
                        $member->bepoz_count_visits = $data->countvisits;
                        // Log::info($member->bepoz_count_visits." countvisits ".$data->countvisits);
                    }

                    $bepoz_status = "";
                    switch (intval($data->status)) {
                        case 0:
                            $bepoz_status = "OK";                                            
                            break;
                        case 1:
                            $bepoz_status = "On Hold";
                            break;
                        case 2:
                            $bepoz_status = "Pending";
                            break;
                        case 3:
                            $bepoz_status = "Suspended";
                            break;
                        case 4:
                            $bepoz_status = "Inactive";
                            break;
                        case 5:
                            $bepoz_status = "Deceased";
                            break;
                        default:
                            $bepoz_status = "OK";
                    }

                    if ( $member->bepoz_account_status != $bepoz_status){
                        $updateAccount = true;
                        $member->bepoz_account_status = $bepoz_status;
                        // Log::info($member->bepoz_account_status." bepoz_account_status ".$data->bepoz_status);
                    }

                    if ($updateAccount) {
                        $member->save();
                    }

                    if ( $member->user->mobile != $data->mobile){
                        $updateAccount = true;
                        $member->user->mobile = $data->mobile;
                        $member->user->save();
                        // Log::info($member->user->mobile." mobile ".$data->mobile);
                    }
                    

                    $mt = MemberTiers::where('member_id', $member->id)->first();
                    $mt->date_expiry = $data->datetimeexpiry;
                    if (!is_null($tier)) {
                        if ( $mt->tier_id != $tier->id){
                            $updateAccount = true;
                            $mt->tier_id = $tier->id;    
                            $mt->save();
                            // Log::info($mt->tier_id." tier_id ".$tier->id);
                        }
                    }

                    if ($updateAccount) {                                            
                        $ml = new MemberLog();
                        $ml->before = $cloned->toJson();
                        $ml->member_id = $user->member->id;
                        $ml->message = 'System (Bepoz) updated member '.$member->id . ' details. ';
                        $ml->after = $user->member->toJson();
                        $ml->save();
                    }

                } else {

                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Polling account member ' . $member->id . ' is failed. Please check the error message';
                    $log->message = "Email from Bepoz doesn't match email from database";
                    $log->source = 'PollAccount.php';
                    $log->save();

                    // $this->changeBepozID($member);
                }
            }

    }

    protected function changeBepozID($member)
    {
        // FIND THE RIGHT BEPOZ ID BY EMAIL        
        $bepoz = new Bepoz;
        $result = $bepoz->accountSearch(8, $member->user->email);

        if ($result) {
            if ($result['IDList']['Count'] == 0) {
                // NO ACCOUNT FOUND JUST CONTINUE
                $log = new SystemLog();
                $log->type = 'bepoz-job-error';
                $log->humanized_message = 'Polling account member '.$member->id.' is failed. Please check the error message';
                $log->message = "changeBepozID None Account found with email ". $member->user->email;
                $log->source = 'PollAccount.php';
                $log->save();
            } else if ($result['IDList']['Count'] == 1) {
                $temp = $bepoz->AccountFullGet($result['IDList']['ID']);

                if ($temp) {
                    if ($temp['AccountFull']['Status'] == 0) {
                        $clonedmember = $member->replicate();
                        $member->bepoz_account_id = $temp['AccountFull']['AccountID'];
                        $member->save();

                        $log = new SystemLog();
                        $log->type = 'bepoz-job-error';
                        $log->humanized_message = 'Polling account member '.$member->id.' is changeBepozID';
                        $log->message = "changeBepozID from ". $clonedmember->bepoz_account_id ." to ". $temp['AccountFull']['AccountID'];
                        $log->source = 'PollAccount.php';
                        $log->save();
                    }

                } else {
                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Polling account member '.$member->id.' is failed. Please check the error message';
                    $log->message = "changeBepozID Bepoz problem ";
                    $log->source = 'PollAccount.php';
                    $log->save();
                }
            } else {
                $log = new SystemLog();
                $log->type = 'bepoz-job-error';
                $log->humanized_message = 'Polling account member '.$member->id.' is failed. Please check the error message';
                $log->message = "changeBepozID Multiple Account found with email ". $member->user->email;
                $log->source = 'PollAccount.php';
                $log->save();
            }
        } else {
            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Polling account member '.$member->id.' is failed. Please check the error message';
            $log->message = "changeBepozID Bepoz problem ";
            $log->source = 'PollAccount.php';
            $log->save();
        }
    }

}
