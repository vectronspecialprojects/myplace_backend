<?php

namespace App\Console\Commands\Bepoz;

use App\Address;
use App\BepozFailedJob;
use App\BepozJob;
use App\Member;
use App\ListingType;
use App\MemberTiers;
use App\Order;
use App\OrderDetail;
use App\Setting;
use App\SystemLog;
use App\Tier;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Helpers\Bepoz;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class UpdateAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-bepoz-account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Bepoz Account';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            // Log::info("Update Bepoz Account");

            $ebet_gaming = Setting::where('key', 'ebet_gaming')->first()->value;

            if ($ebet_gaming === 'false') {

                $checkBepozConnection = $bepoz->SystemCheck();

                if ($checkBepozConnection) {

                    // Log::info("Update Bepoz Account checkBepozConnection");

                    $app_flag_id_bepoz = Setting::where('key', 'app_flag_id_bepoz')->first()->value;
                    $app_flag_name_bepoz = Setting::where('key', 'app_flag_name_bepoz')->first()->value;
                    
                    $customfieldpayload = [];
                    $standardfieldpayload = [];
                    // LOAD BEPOZ CUSTOM FIELD BASED ON SETTING
                    $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first()->extended_value;                    
                    $bepozcustomfielddecode = \GuzzleHttp\json_decode($bepozcustomfield);
                    
                    // Log::info($bepozcustomfielddecode);

                    if (sizeof($bepozcustomfielddecode)) {
                       
                        // Log::info("sizeof bepozcustomfielddecode");
                        
                        foreach ($bepozcustomfielddecode as $cf) {
                           
                            // Log::info(print_r($cf, true));

                            if ($cf->editOnProfile) {
                                if ( $bepoz->isCustomField($cf) ) {
                                    $customfieldpayload[] = array(
                                        'id' => $cf->id,
                                        'value' => '',
                                        'fieldName' => $cf->fieldName,
                                        'fieldType' => $cf->fieldType,
                                        'bepozFieldNum' => $cf->bepozFieldNum
                                    );
                                } else {
                                    $standardfieldpayload[] = array(
                                        'id' => $cf->id,
                                        'value' => '',
                                        'fieldName' => $cf->fieldName,
                                        'fieldType' => $cf->fieldType,
                                        'bepozFieldNum' => $cf->bepozFieldNum
                                    );
                                }
                            }
                        }
                    }

                    // Log::info("customfieldpayload");
                    // Log::info($customfieldpayload);
                    // Log::info("standardfieldpayload");
                    // Log::info($standardfieldpayload);


                    $jobs = BepozJob::where('reserved', '=', 0)
                        ->where('queue', '=', 'update-account')
                        ->chunk(50, function ($jobs) use ($bepoz, $app_flag_id_bepoz, $app_flag_name_bepoz,
                            $customfieldpayload, $standardfieldpayload) {
                            // Reserve the selected jobs first before the execution
                            foreach ($jobs as $bepoz_job) {
                                $bepoz_job->setJobUID(Uuid::generate());
                                $bepoz_job->reserve();
                            }

                            foreach ($jobs as $bepoz_job) {

                                if ($bepoz_job->attempts() < 3) {
                                    $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                    $bepoz_job->save();

                                    try {

                                        // Log::info('update-account');

                                        $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                        $member = Member::with('user')->find($data->member_id);
                                        $tier = $member->tiers()->first();

                                        $groupID = $member->tiers()->first()->bepoz_group_id;
                                        $template = $member->tiers()->first()->bepoz_template_id;

                                        if (!is_null($member->bepoz_account_id) && $member->bepoz_account_id !== 'null') {

                                            // Log::info('bepoz_account_id');

                                            if ($member->payload){
                                                $allInputpayload = \GuzzleHttp\json_decode($member->payload, true);
                                                
                                                if (array_key_exists(0, $allInputpayload)) {
                                                    // flatten arrays
                                                    $mergeinput = array_merge(...array_values($allInputpayload));
                                                    $allInputpayload = $mergeinput;

                                                }
                                                // Log::info($allInputpayload);
                                                // Log::info($allInputpayload['first_name']);

                                            }
                                            
                                            // Log::info(print_r($mergeinput, true));
                                            // Log::info($mergeinput['first_name']);
                                            // Log::info($allInputpayload['first_name']);

                                            // Create or Update Bepoz Account
                                            $result = $bepoz->AccountFullGet($member->bepoz_account_id);
                                            // Log::warning($result);
                                            
                                            if ($result) {
                                                // Need check the membership upgrade transaction done by this member
                                                // FEATURE FROM COWCH
                                                $orders = Order::with('listing')
                                                    ->where('member_id', $member->id)
                                                    ->whereExists(function ($query) {
                                                        $query->select(DB::raw(1))
                                                            ->from('listings')
                                                            ->where('listings.listing_type_id', '10')
                                                            ->whereRaw('listings.id = orders.listing_id')
                                                            ->where('listings.status', 'active')
                                                            ->whereNull('listings.deleted_at');
                                                    })
                                                    ->get();

                                                if ( !is_null($orders) ){                                                        
                                                    $level = 1;
                                                    foreach ($orders as $orderKey => $order) {
                                                        // if (intval($order->listing->membership_tier) > $level){
                                                            $level = $order->listing->membership_tier;
                                                            $purchase_date = $order->created_at;
                                                        // }
                                                    }


                                                    $tier = Tier::find($level);
                                                    $member_tier = MemberTiers::where('member_id', $member->id)->first();
                                                    $member_tier->tier_id = $level;

                                                    if ($tier->expiry_date_option == "annual"){
                                                        $expiry_date = new Carbon($purchase_date);
                                                        $expiry_date->addYears(1);
                                                        $member_tier->date_expiry = $expiry_date;
                                                    }
                                                    else if ($tier->expiry_date_option == "on_permanently"){
                                                        
                                                    }
                                                    else if ($tier->expiry_date_option == "months"){
                                                        $expiry_date = new Carbon($purchase_date);
                                                        $expiry_date->addMonths($tier->months);
                                                        $member_tier->date_expiry = $expiry_date;
                                                    }
                                                    else if ($tier->expiry_date_option == "days"){
                                                        $expiry_date = new Carbon($purchase_date);
                                                        $expiry_date->addDays($tier->days);
                                                        $member_tier->date_expiry = $expiry_date;
                                                    }
                                                    else if ($tier->expiry_date_option == "specific_date"){
                                                        $member_tier->date_expiry = $tier->specific_date;
                                                    }

                                                    $member_tier->save();

                                                    $groupID = $member->tiers()->first()->bepoz_group_id;
                                                }

                                                $request = $result['AccountFull'];
                                                // $request['AccNumber'] = $member->status == 'active' ? $member->bepoz_account_number : $member->bepoz_account_number . 'X';
                                                // $request['CardNumber'] = $member->status == 'active' ? $member->bepoz_account_card_number : $member->bepoz_account_card_number . 'X';
                                                
                                                // Log::warning("CardNumber");
                                                // Log::warning($request['CardNumber']);
                                                // EXISTING ACC NEED CHANGE CARD NUMBER
                                                // FEATURE FROM COWCH
                                                $CardNumberLength = strlen($request['CardNumber']);
                                                // Log::warning("CardNumberLength");
                                                // Log::warning($CardNumberLength);

                                                if ($tier->force_card_range){
                                                    // Log::warning("force_card_range");
                                                    if ($CardNumberLength == 4 || $CardNumberLength == 5 || $CardNumberLength == 6 || $CardNumberLength == 7){
                                                        // Log::warning("CardNumberLength 4-7");
                                                        $newCardNumber = $this->bepozNextAccountNumber($bepoz, $tier);
                                                        $newCardNumberLength = strlen($newCardNumber);
                                                        $tierLength = strlen($tier->boundary_start);
                                                        // $request['CardNumber'] = $newCardNumber;
                                                        if ( intval($newCardNumber) > 0 && $tierLength == $newCardNumberLength) {
                                                            // Log::warning("CardNumberLength change");
                                                            $request['AccNumber'] = $member->status == 'active' ? $newCardNumber : $member->bepoz_account_card_number . 'X';
                                                            $request['CardNumber'] = $member->status == 'active' ? $newCardNumber : $member->bepoz_account_card_number . 'X';
                                                            $member->bepoz_account_number = $newCardNumber;
                                                            $member->bepoz_account_card_number = $newCardNumber;
                                                            $member->save();
                                                        } else {
                                                            
                                                        }
                                                    }
                                                }

                                                // CHECK MEMBERSHIP UPGRADE IS ENABLE
                                                $membershipUpgrade = ListingType::find(10);
                                                $key = $membershipUpgrade->key;
                                                if ($key->hide_this == false) {
                                                    $request['GroupID'] = $groupID;
                                                }

                                    
                                                foreach ($standardfieldpayload as $sf) {
                                                    Log::warning($sf);
                                                    $bepozFieldName = $bepoz->bepozAccField($sf['bepozFieldNum']);
                                                    
                                                    if (array_key_exists($sf['id'], $allInputpayload)) {
                                                        if ($bepozFieldName == "") {
                                                            // NO NEED
                                                        } else if ($bepozFieldName == "Tier") {
                                                            // NO NEED
                                                        } else if ($bepozFieldName == "Password") {
                                                            // NO NEED
                                                        } else if ($bepozFieldName == "DateBirth") {
                                                            $request[$bepozFieldName] = Carbon::createFromFormat('d/m/Y', $sf['value'])->format('Y-m-d');
                                                        } else {
                                                            $request[$bepozFieldName] = $allInputpayload[$sf['id']];
                                                        }
                                                    }
                                                }

                                                //$request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                if ($request['FirstName'] == "") {
                                                    $request['FirstName'] = str_replace('"', '', $member->first_name);
                                                }
                                                if ($request['LastName'] == "") {
                                                    $request['LastName'] = str_replace('"', '', $member->last_name);
                                                }
                                                // $request['DateBirth'] = Carbon::createFromFormat('d/m/Y', $request->input('dob'))->format('Y-m-d');
                                                if ($request['Mobile'] == "") {
                                                    $request['Mobile'] = $member->user->mobile;
                                                }
                                                $request['PhoneHome'] = $member->phone;
                                                // $request['Email1st'] = $member->user->email;
                                                $request['PCode'] = $member->postcode;
                                                $request['Status'] = 0;
                                                
                                                // $address = Address::where('member_id', $member->id)->first();
                                                // if (!is_null($address)) {
                                                //     $payload = \GuzzleHttp\json_decode($address->payload);
                                                //     $request['Street1'] = isset($payload->display->unit) ? $payload->display->unit : "";
                                                //     $request['Street2'] = isset($payload->display->street) ? $payload->display->street : "";
                                                //     //$request['Street3'] = $payload->display->street;
                                                //     $request['State'] = isset($payload->display->state) ? $payload->display->state : "";
                                                //     $request['City'] = isset($payload->display->suburb) ? $payload->display->suburb : "";
                                                //     $request['PCode'] = isset($payload->display->postcode) ? $payload->display->postcode : "";
                                                // }

                                                if (!is_null($member->member_tier->date_expiry)) {
                                                    $date_expiry = Carbon::parse($member->member_tier->date_expiry);
                                                    $date_expiry->setTimezone(config('app.timezone'));
                                                    $request['DateTimeExpiry'] = $date_expiry->format('Y-m-d\TH:i:s');
                                                }

                                                unset($request['AddressID']);
                                                unset($request['CommentID']);

                                                // $result = $bepoz->AccUpdate($request, $template);
                                                // NEW WAY
                                                $result = $bepoz->UpdateAccount($request, $template);
                                                // $result = true;
                                                
                                                Log::info("Update Account Request");
                                                // Log::info(print_r($request, true));

                                                if ($result) {
                                                    
                                                    Log::info("Update Account Result");
                                                    // Log::info(print_r($result, true));

                                                    // // FEATURE FROM VARSITY, NEED MAKE GLOBAL VARIABLE
                                                    // $promo_code_enable = Setting::where('key', 'promo_code_enable')->first()->value;
                                                    // if ( $promo_code_enable == "true" ) {
                                                    //     $this->setCustomField($bepoz, $member, "Sponsorship Code");
                                                    // }

                                                    // $sponsorship_code_enable = Setting::where('key', 'sponsorship_code_enable')->first()->value;
                                                    // if ( $sponsorship_code_enable == "true" ) {
                                                    //     $this->setCustomField($bepoz, $member, "Promo Code");
                                                    // }

                                                    //UPDATE CUSTOM FIELD
                                                    $setting = Setting::where('key', 'bepozcustomfield')->first();
                                                    $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);

                                                    $customFieldUpdate = true;
                                                    $customFields = array();
                                                    
                                                    $app_flag_id_bepoz = Setting::where('key', 'app_flag_id_bepoz')->first()->value;
                                                    $app_flag_name_bepoz = Setting::where('key', 'app_flag_name_bepoz')->first()->value;
                                                    
                                                    if ($app_flag_id_bepoz != "false") {
                                                        $customFieldUpdate = true;

                                                        $customFields['CustomField'][] = array(
                                                            'FieldName' => $app_flag_name_bepoz,
                                                            'FieldType' => 0,
                                                            'FieldValue' => true,
                                                            'FieldNum' => $app_flag_id_bepoz
                                                        );
                                                    }

                                                    foreach ($customfieldpayload as $cf) {
                                                        $typeCode = 0;
                                                        switch ($cf["fieldType"]) {
                                                            case "Text":
                                                                $typeCode = 3;
                                                                break;
                                                            case "Number":
                                                                $typeCode = 2;
                                                                break;
                                                            case "Date":
                                                                $typeCode = 1;
                                                                break;
                                                            case "Dropdown":
                                                                $typeCode = 3;
                                                                break;
                                                            case "Checkbox":
                                                                $typeCode = 3;
                                                                break;
                                                            case "Toggle":
                                                                $typeCode = 0;
                                                                break;
                                                        }

                                                        $bepozFieldNum = $cf["bepozFieldNum"];
                                                        if (strlen(trim($cf["bepozFieldNum"])) == 3) {
                                                            $bepozFieldNum = substr($cf["bepozFieldNum"], -2);
                                                        }

                                                        if (array_key_exists($cf['id'], $allInputpayload)) {
                                                            // echo print_r($cf, true)."<br><br>";
                                                            $customFields['CustomField'][] = array(
                                                                'FieldName' => $cf["fieldName"],
                                                                'FieldType' => $typeCode,
                                                                'FieldValue' => $allInputpayload[$cf['id']],
                                                                'FieldNum' => $bepozFieldNum
                                                            );
                                                        }
                                                    }

                                                    if (!$customFieldUpdate) {
                                                        
                                                        // ALL DONE NO NEED SEND CUSTOM FIELD
                                                        $bepoz_job->dispatch();
                                                        
                                                    } else {
                                                        
                                                        // $CustomFieldNumber = $preffered_venue_update_custom_field_enable;
                                                        // $CustomFieldKey = "PreferredVenue";
                                                        
                                                        // $param = ['AccountID' => $member->bepoz_account_id,
                                                        //     'CustomFieldNumber' => $CustomFieldNumber,
                                                        //     'DataSet' => $member->current_preferred_venue_name
                                                        // ];
                                                        // $customFieldResult = $bepoz->AccCustomFieldSet($param);

                                                        // Log::info("Update Account customFields");
                                                        // foreach ($customFields['CustomField'] as $cf) {
                                                        //     Log::info($cf);
                                                        // }


                                                        $EntityType = 0;
                                                        $EntityID = $member->bepoz_account_id;

                                                        $customFieldResult = $bepoz->CustomFieldsSet($EntityType, $EntityID, $customFields);
                                                        // $customFieldResult = false;

                                                        // Log::warning($customFieldResult);
                                                        if ($customFieldResult) {
                                                            if (!is_null($bepoz_job->failed_job_uid)) {
                                                                $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                                if (!is_null($failed_job)) {
                                                                    $failed_job->delete();
                                                                }
                                                            }

                                                            $bepoz_job->dispatch();
                                                        } else {
                                                            $log = new SystemLog();
                                                            $log->type = 'bepoz-job-error';
                                                            $log->humanized_message = 'Updating account Custom Field is failed. Please check log.';
                                                            $log->payload = $bepoz_job->payload;
                                                            $log->message = \GuzzleHttp\json_encode($result);
                                                            $log->source = 'UpdateAccount.php';
                                                            $log->save();

                                                            // try again
                                                            $bepoz_job->free();
                                                        }

                                                    }

                                                    
                                                } else {
                                                    $log = new SystemLog();
                                                    $log->type = 'bepoz-job-error';
                                                    $log->humanized_message = 'Updating account is failed. Please check log.';
                                                    $log->payload = $bepoz_job->payload;
                                                    $log->message = \GuzzleHttp\json_encode($result);
                                                    $log->source = 'UpdateAccount.php';
                                                    $log->save();

                                                    // try again
                                                    $bepoz_job->free();
                                                }


                                            } else {
                                                $log = new SystemLog();
                                                $log->type = 'bepoz-job-error';
                                                $log->humanized_message = 'Updating account is failed. Please check log.';
                                                $log->payload = $bepoz_job->payload;
                                                $log->message = \GuzzleHttp\json_encode($result);
                                                $log->source = 'UpdateAccount.php';
                                                $log->save();

                                                // try again
                                                $bepoz_job->free();

                                            }
                                        } else {
                                            $bepoz_job->free();
                                        }

                                    } catch
                                    (\Exception $e) {
                                        $log = new SystemLog();
                                        $log->type = 'bepoz-job-error';
                                        $log->humanized_message = 'Updating account is failed. Please check the error message.';
                                        $log->message = $e;
                                        $log->payload = $bepoz_job->payload;
                                        $log->source = 'UpdateAccount.php';
                                        $log->save();

                                        $bepoz_job->free();
                                    }

                                } else {
                                    if (is_null($bepoz_job->failed_job_uid)) {

                                        $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                            ->where('queue', $bepoz_job->queue)
                                            ->first();

                                        if (is_null($failed_job)) {
                                            $failed_job = new BepozFailedJob;
                                            $failed_job->queue = $bepoz_job->queue;
                                            $failed_job->payload = $bepoz_job->payload;
                                            $failed_job->job_uid = Uuid::generate(4);
                                            $failed_job->save();
                                        }
                                    }

                                    // Maximum attempt log no longer needed
                                    // $log = new SystemLog();
                                    // $log->type = 'bepoz-job-error';
                                    // $log->humanized_message = 'Updating account is failed. Maximum attempts has been reached.';
                                    // $log->source = 'UpdateAccount.php';
                                    // $log->payload = $bepoz_job->payload;
                                    // $log->save();

                                    $bepoz_job->dispatch();

                                }

                            }
                        }
                        );

                }

            }

        } catch (\Exception $e) {

            // Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Updating account is failed. Please check the error message.';
            $log->message = $e;
            $log->source = 'UpdateAccount.php';
            $log->save();
        }

    }

    
    function bepozNextAccountNumber($bepoz, $tier) {

        $account_group = $tier->bepoz_group_id;
        $max_length = strlen($tier->boundary_start);
        $range = $tier->boundary_start.",".$tier->boundary_end;

        $result = $bepoz->NextAccountNumber($account_group, $max_length, $range);

        if ($result){
            $idPad = str_pad($result['Data1'], strlen($tier->boundary_start), '0', STR_PAD_LEFT);
            return $idPad;
        }

        return false;
    }

    function setCustomField($bepoz, $member, $CustomFieldKey)
    {

        $setting = Setting::where('key', 'bepozcustomfield')->first();

        $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);
        $CustomFieldNumber = "0";

        foreach ($allCustomField as $data) {
            if ($data->key == $CustomFieldKey) {
                $CustomFieldNumber = $data->field;
            }
        }

        //Log::info($CustomFieldNumber);
        $dataset = "";
        // if ($CustomFieldKey == "Sponsorship Code") {
        //     $dataset = $member->sponsorship_code;
        // } else if ($CustomFieldKey == "Promo Code") {
        //     $dataset = $member->promo_code;
        // }

        // CHECK EMPTY
        if (!empty($dataset)) {
            $param = ['AccountID' => $member->bepoz_account_id,
                'CustomFieldNumber' => $CustomFieldNumber,
                'DataSet' => $dataset
            ];

            $customFieldResult = $bepoz->AccCustomFieldSet($param);

            if ($customFieldResult) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }

        return false;
    }

}
