<?php

namespace App\Console\Commands\Bepoz;


use App\ClaimedPromotion;
use App\Helpers\Bepoz;
use App\Jobs\SendOneSignalNotification;
use App\Listing;
use App\ListingProduct;
use App\Member;
use App\MemberLog;
use App\MemberSystemNotification;
use App\MemberVouchers;
use App\MyplaceCache;
use App\Order;
use App\OrderDetail;
use App\Platform;
use App\Product;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\SystemLog;
use App\SystemNotification;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class PollVoucher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poll-voucher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cloud vouchers based on the data given';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            // Log::info("PollVoucher START");
            // Log::info(Carbon::now(config('app.timezone')));

            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
            $platform = Platform::where('name', 'web')->first();
            $tc = Setting::where('key', 'invoice_logo')->first();

            $last_execution_time = Setting::where('key', 'bepoz_voucher_last_successful_execution_time')->first();
            $datetime = new Carbon($last_execution_time->value);
            // $datetime->addHours(-3);
            $datetime->addMinutes(-30);
            // Log::info($last_execution_time->value);

            $payload = [];
            $payload['procedure'] = "VoucherUpdated";
            $payload['parameters'] = [ $datetime->format('Y-m-d H:i:s') ];

            // $cache = MyplaceCache::where('key', 'member_ids')->first();
            // $payload['procedure'] = "VoucherUpdatedMembers";
            // $payload['parameters'] = [$datetime->format('Y-m-d H:i:s'), $cache->value];

            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::warning('poll-voucher');
            // Log::warning($httpCode);
            // Log::warning($content);

            if ($httpCode >= 200 && $httpCode < 300) {
                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);
                    if (!$payload->error) {

                        $last_execution_time->value = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
                        $last_execution_time->save();

                        // Log::warning(print_r($payload->data, true));

                        $company_name = Setting::where('key', 'company_name')->first()->value;
                        $pos_voucher_enable = Setting::where('key', 'pos_voucher_enable')->first()->value;

                        $chunkSize = 300;
                        $iChunk = 0;
                        $payloadChunks = array_chunk($payload->data, $chunkSize, true);
                        // CHUNK BEPOZ RESULT
                        foreach ($payloadChunks as &$payloadChunk) {

                            $voucher_ids = array_column($payloadChunk, 'VoucherID');
                            
                            foreach ($payloadChunk as $key => &$rowChunk) {
                                $rowChunk->foundmyplace = "false";
                                $rowChunk->memberFoundCreate = "false";
                            }

                            // Log::critical("payloadChunk");
                            // Log::critical(print_r($payloadChunk, true));
                            // Log::critical(print_r($voucher_ids, true));

                            // UPDATE VOUCHER FOUND IN MYPLACE
                            $memberVouchers = MemberVouchers::whereIn('voucher_id', $voucher_ids)
                                ->chunk(100, function ($memberVouchers) use (&$payloadChunk, $voucher_ids, $iChunk, $chunkSize) {

                                    // Log::critical(print_r($vouchers, true));

                                    // UPDATE VOUCHER
                                    foreach ($memberVouchers as $memberVoucher) {                                        
                                        $found_key_payload = array_search($memberVoucher->voucher_id, $voucher_ids);
                                        if ($found_key_payload >= 0) {
                                            $indexTarget = $iChunk * $chunkSize + $found_key_payload;

                                            // Log::critical("found_key_payload ". $found_key_payload);
                                            // Log::critical("indexTarget ". $indexTarget);

                                            $data = $payloadChunk[$indexTarget];
                                            $payloadChunk[$indexTarget]->foundmyplace = "true";

                                            $updateVoucher = false;
                                            
                                            if (!$memberVoucher->redeemed){
                                                if ($memberVoucher->amount_left != $data->AmountLeft) {
                                                    if ($data->AmountLeft > 0) {
                                                        $memberVoucher->redeemed = false;

                                                        $cp = ClaimedPromotion::where('member_voucher_id', $memberVoucher->id)->first();
                                                        if (!is_null($cp)) {
                                                            $cp->is_used = false;
                                                            $cp->save();
                                                        }
                                                    } else {
                                                        $memberVoucher->redeemed = true;

                                                        $cp = ClaimedPromotion::where('member_voucher_id', $memberVoucher->id)->first();
                                                        if (!is_null($cp)) {
                                                            $cp->is_used = true;
                                                            $cp->save();
                                                        }
                                                    }

                                                    $updateVoucher = true;
                                                }

                                                if ($data->StopRedeems == 1) {
                                                    if ( !$memberVoucher->redeemed ) {
                                                        $memberVoucher->redeemed = true;
                                                        $cp = ClaimedPromotion::where('member_voucher_id', $memberVoucher->id)->first();
                                                        if (!is_null($cp)) {
                                                            $cp->is_used = true;
                                                            $cp->save();
                                                        }
                                                        $updateVoucher = true;
                                                    }
                                                }

                                                if ($memberVoucher->used_value != $data->UsedValue) {
                                                    $memberVoucher->used_value = $data->UsedValue;
                                                    $updateVoucher = true;
                                                }

                                                if ($memberVoucher->amount_left != $data->AmountLeft) {
                                                    $memberVoucher->amount_left = $data->AmountLeft;
                                                    $updateVoucher = true;
                                                }

                                                if ($memberVoucher->lookup != $data->Lookup) {
                                                    $memberVoucher->lookup = $data->Lookup;
                                                    $updateVoucher = true;
                                                }

                                                if ($memberVoucher->bepoz_prize_promo_id != $data->PrizePromoID) {
                                                    $memberVoucher->bepoz_prize_promo_id = $data->PrizePromoID;
                                                    $updateVoucher = true;
                                                }
                                                
                                                $prize = PrizePromotion::where('bepoz_prize_promo_id', $data->PrizePromoID)->first();
                                                if (!is_null($prize)) {
                                                    if ($memberVoucher->prize_promotion_id != $prize->id) {
                                                        $updateVoucher = true;
                                                        $memberVoucher->prize_promotion_id = $prize->id;
                                                    }
                                                }

                                                $raw_barcode = "99" . str_repeat("0", 10 - strlen($data->Lookup)) . $data->Lookup;
                                                $new_barcode = $raw_barcode . $this->check_digit($raw_barcode);

                                                if ($memberVoucher->barcode != $new_barcode) {
                                                    $updateVoucher = true;
                                                    $memberVoucher->barcode = $new_barcode;
                                                }

                                                if (strtotime($data->DateExpiry) && !is_null($data->DateExpiry) ){
                                                    if ($memberVoucher->expire_date != Carbon::parse($data->DateExpiry)){
                                                        $updateVoucher = true;
                                                        $memberVoucher->expire_date = strtotime($data->DateExpiry) && !is_null($data->DateExpiry) ? Carbon::parse($data->DateExpiry) : null;                                
                                                    }
                                                }

                                                if (strtotime($data->UsedDate) && !is_null($data->UsedDate) ) {                                        
                                                    if ($memberVoucher->used_date != Carbon::parse($data->UsedDate)){
                                                        $updateVoucher = true;
                                                        $memberVoucher->used_date = strtotime($data->UsedDate) && !is_null($data->UsedDate) ? Carbon::parse($data->UsedDate) : null;
                                                    }
                                                }

                                                if ($updateVoucher) {
                                                    $memberVoucher->save();
                                                }
                                                
                                                // Log::critical("updateVoucher ".$updateVoucher);
                                            }
                                            
                                        
                                        }
                                        
                                    }

                                }
                            );

                            $iChunk++;
                        };
                        
                        // CREATE VOUCHER DIRECTLY FROM BEPOZ
                        if ($pos_voucher_enable == "true") {

                            $payloadCreate = array_filter($payload->data, function($k) {
                                return $k->foundmyplace == 'false';
                            });
                            // Log::warning("payloadCreate");
                            // Log::warning(print_r($payloadCreate, true));

                            $bepoz_ids = array_column($payloadCreate, 'AccountID');

                            $members = Member::whereIn('bepoz_account_id', $bepoz_ids)
                                ->chunk(100, function ($members) use (&$payloadCreate, $payload) {

                                    // FILTER VOUCHER WITH BEPOZ ID
                                    foreach ($members as $member) {
                                        
                                        $payloadMemberCreate = array_filter($payloadCreate, function($k) use ($member) {
                                            return $k->AccountID == $member->bepoz_account_id;
                                        });
                                        
                                        foreach ($payloadMemberCreate as &$rowCreate) {
                                            $rowCreate->memberFoundCreate = "true";
                                        }
                                    }
                                }
                            );

                            $voucherCreate = array_filter($payload->data, function($k) {
                                return $k->memberFoundCreate == 'true';
                            });


                            // CREATING VOUCHER
                            // Log::info("CREATING VOUCHER");
                            foreach ($voucherCreate as $data) {
                                // Log::info(print_r($data, true));

                                //if memberVoucher not exist so create one, load free voucher from bepoz
                                if (intval($data->AccountID) !== 0) {
                                    $member = Member::where('bepoz_account_id', $data->AccountID)->first();
                                    if (!is_null($member)) {
                                        // new vouchers issued from BEPOZ, like buy 5 get 1 free

                                        $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                        //if VoucherSetups not found then create new one
                                        if (is_null($vs)) {
                                            $voucherSetupInformation = $bepoz->VoucherSetupGet($data->VoucherSetupID);
                                            //Log::info($voucherSetupInformation);
                                            
                                            if ($voucherSetupInformation) {
                                                DB::beginTransaction();
                                                $vs = new VoucherSetups;
                                                $vs->bepoz_voucher_setup_id = $data->VoucherSetupID;
                                                $vs->name = $voucherSetupInformation['VoucherSetup']['Name'];
                                                $vs->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                                                $vs->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                                                $vs->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                                                $vs->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                                                $vs->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                                                $vs->voucher_apply = intval($voucherSetupInformation['VoucherSetup']['VoucherApply']);
                                                $vs->save();
                                                DB::commit();
                                            } else {
                                                $log = new SystemLog();
                                                $log->type = 'bepoz-job-error';
                                                $log->humanized_message = 'Polling voucher setup is failed. Please check the error message';
                                                $log->message = 'Failed to reach bepoz connection';
                                                $log->source = 'PollVoucher.php';
                                                $log->save();
                                            }
                                        }

                                        //load VoucherSetups again
                                        $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                        if (!is_null($vs)) {

                                            $product = Product::where('bepoz_voucher_setup_id', $vs->id)->first();

                                            $createBepozVoucher = false;

                                            if (is_null($product)) {
                                                $product = new Product();
                                                $product->name = $vs->name;
                                                $product->bepoz_voucher_setup_id = $vs->id;
                                                $product->product_type_id = '1';
                                                $product->is_free = '1';
                                                $product->status = 'active';
                                                $product->category = 'voucher';
                                                $product->payload = '[{"triggerNotif":true,"notifType":"push","notifContentType":"custom","notifContent":"Your ' . $vs->name . ' is ready","notifTier":[]}]';
                                                $product->image = $tc->value;
                                                $product->desc_short = "";

                                                $product->save();

                                                $createBepozVoucher = true;
                                            } else {
                                                // NO NEED TO SEE PRODUCT STATUS active
                                                // if ($product->status == 'active') {
                                                //     $createBepozVoucher = true;
                                                // } else {
                                                //     $createBepozVoucher = false;
                                                // }

                                                $createBepozVoucher = true;
                                            }

                                            if ($createBepozVoucher) {

                                                // Log::warning("Product ".$product->id." createBepozVoucher");
                                                // Log::warning(print_r($data, true));
                                                DB::beginTransaction();
                                                //create Transaction
                                                //create Order
                                                $order = new Order();
                                                $order->member_id = $member->id;
                                                $order->type = 'cash';
                                                $order->expired = time();
                                                $order->order_status = 'confirmed';
                                                $order->payment_status = 'successful';
                                                $order->voucher_status = 'successful';
                                                $order->transaction_type = 'bepoz';
                                                $order->payload = '{"app_token":"' . $platform->app_token . '","app_secret":"' . $platform->app_secret .
                                                    '","transaction_type":"bepoz","notifSend":"false"}';
                                                // $order->save();
                                                $payload = \GuzzleHttp\json_encode($order);
                                                $token = md5($payload);
                                                $order->token = $token;
                                                $order->ip_address = '0.0.0.0';
                                                $order->save();

                                                //create memberVoucher
                                                $memberVoucher = new MemberVouchers();
                                                $memberVoucher->member_id = $member->id;
                                                $memberVoucher->name = $vs->name;
                                                $memberVoucher->voucher_setup_id = $vs->id;

                                                //create order detail
                                                $orderDetail = new OrderDetail();
                                                $orderDetail->qty = 1;
                                                $orderDetail->product_name = $vs->name;
                                                $orderDetail->voucher_name = $vs->name;
                                                $orderDetail->voucher_lookup = $data->Lookup;
                                                $orderDetail->order_id = $order->id;
                                                $orderDetail->voucher_setups_id = $vs->id;
                                                $orderDetail->category = 'bepoz';
                                                $orderDetail->payload = '{"notifSend":"false"}';

                                                $listingProduct = ListingProduct::where('product_id', $product->id)->first();
                                                if (!is_null($listingProduct)) {
                                                    $orderDetail->listing_id = $listingProduct->listing_id;
                                                } else {

                                                }

                                                $orderDetail->product_id = $product->id;
                                                $orderDetail->product_type_id = $product->product_type_id;
                                                $orderDetail->save();

                                                $memberVoucher->category = 'bepoz';
                                                $memberVoucher->voucher_id = $data->VoucherID;
                                                $memberVoucher->lookup = $data->Lookup;
                                                $memberVoucher->voucher_type = $data->VoucherType;
                                                $memberVoucher->voucher_apply = $data->VoucherApply;
                                                $memberVoucher->unlimited_use = filter_var($data->UnlimitedUse, FILTER_VALIDATE_BOOLEAN);
                                                $memberVoucher->maximum_discount = $data->MaximumDiscount;
                                                $memberVoucher->claim_venue_id = $data->ClaimVenueID;
                                                $memberVoucher->claim_store_id = $data->ClaimStoreID;
                                                $memberVoucher->used_count = $data->UsedCount;
                                                $memberVoucher->used_value = $data->UsedValue;
                                                $memberVoucher->used_trans_id = $data->UsedTransID;
                                                $memberVoucher->amount_left = $data->AmountLeft;
                                                $memberVoucher->amount_issued = $data->AmountIssued;
                                                $memberVoucher->status = 'successful';
                                                $memberVoucher->order_id = $order->id;
                                                $memberVoucher->order_details_id = $orderDetail->id;

                                                $memberVoucher->bepoz_prize_promo_id = $data->PrizePromoID;
                                                $prize = PrizePromotion::where('bepoz_prize_promo_id', $data->PrizePromoID)->first();
                                                if (!is_null($prize)) {
                                                    $memberVoucher->prize_promotion_id = $prize->id;
                                                }

                                                $raw_barcode = "99" . str_repeat("0", 10 - strlen($data->Lookup)) . $data->Lookup;
                                                $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                                $memberVoucher->issue_date = Carbon::parse($data->IssuedDate);
                                                $memberVoucher->expire_date = strtotime($data->DateExpiry) && !is_null($data->DateExpiry) ? Carbon::parse($data->DateExpiry) : null;
                                                $memberVoucher->save();

                                                $memberVoucher->voucherSetupInformation = \GuzzleHttp\json_encode($vs);
                                                //create memberlog
                                                $ml = new MemberLog();
                                                $ml->member_id = $member->id;
                                                $ml->message = "This member got new free voucher";
                                                $ml->after = \GuzzleHttp\json_encode($memberVoucher);
                                                $ml->save();

                                                $contents = "Your " . $memberVoucher->name . " voucher is ready";

                                                if (!$product->payload) {
                                                    $product->payload = '[{"triggerNotif":true,"notifType":"push","notifContentType":"default","notifContent":"Your ' . $vs->name . ' is ready","notifTier":[]}]';
                                                    $product->save();
                                                    // Log::warning("payload not available");
                                                    //load again
                                                    $notif = \GuzzleHttp\json_decode($product->payload);
                                                }

                                                DB::commit();

                                                $notif = \GuzzleHttp\json_decode($product->payload);

                                                if (isset($notif[0]->notifType)) {
                                                    if ($notif[0]->notifType === "system") {
                                                        $msn = new MemberSystemNotification();
                                                        $msn->member_id = $member->id;
                                                        $msn->system_notification_id = $notif[0]->notification_id;
                                                        $msn->status = 'unread';
                                                        $msn->sent_at = Carbon::now(config('app.timezone'));
                                                        $msn->save();

                                                        $sn = SystemNotification::find($notif[0]->notification_id);
                                                        $contents = $sn->message;
                                                    }

                                                    if ($notif[0]->notifType === "push") {
                                                        $contents = $notif[0]->notifContent;
                                                    }

                                                    $temp = [
                                                        // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                        "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                        //"contents" => ["en" => "Your voucher ($memberVoucher->name) is ready"],
                                                        "contents" => ["en" => $contents],
                                                        "headings" => ["en" => $company_name],
                                                        "data" => ["category" => "voucher"],
                                                        "ios_badgeType" => "Increase",
                                                        "ios_badgeCount" => 1
                                                    ];
                                                    //Log::warning($product);
                                                    //Log::warning($temp);

                                                    // dispatch(new SendOneSignalNotification($temp));

                                                }

                                            }
                                        }

                                    }
                                }
                                
                            }
                        }

                    }
                } else {
                    //$log = new SystemLog();
                    //$log->type = 'bepoz-job-error';
                    //$log->humanized_message = 'Polling voucher is failed. Please check the error message';
                    //$log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                    //$log->source = 'PollVoucher.php';
                    //$log->save();
                }
            } else {
                //$log = new SystemLog();
                //$log->type = 'bepoz-job-error';
                //$log->humanized_message = 'Polling voucher is failed. Please check the error message';
                //$log->message = \GuzzleHttp\json_encode(array("error" => '404 Not Found', "error_code" => $httpCode));
                //$log->source = 'PollVoucher.php';
                //$log->save();
            }

            // Log::info("PollVoucher END");
            // Log::info(Carbon::now(config('app.timezone')));

        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Polling voucher is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'PollVoucher.php';
            $log->save();

        }
    }

    protected function check_digit($lookup)
    {
        $sum = 0;
        $codeString = str_split($lookup);

        for ($i = 0; $i < 12; $i++) {
            if (($i % 2) == 0)
                $sum += $codeString[$i];
            else
                $sum += (3 * $codeString[$i]);
        }

        $sum = $sum % 10;

        if ($sum > 0)
            $sum = 10 - $sum;
        else
            $sum = 0;

        return $sum;
    }

}
