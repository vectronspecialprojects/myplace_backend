<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\Setting;
use App\SystemLog;
use App\Tier;
use Illuminate\Console\Command;
use App\Helpers\Bepoz;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

class PollAccountTotalSaved extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poll-account-total-saved';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update cloud total saved by accounts based on the data given';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
            $poll_account_total_saved = Setting::where('key', 'poll_account_total_saved')->first()->value;

            if ($poll_account_total_saved == "true") {
                //Log::warning("poll_account_total_saved");
                $last_execution_time = Setting::where('key', 'bepoz_account_total_saved_last_successful_execution_time')->first();
                $datetime = new Carbon($last_execution_time->value);
                $datetime->addHours(-1);

                //$datetime = Carbon::createFromFormat('Y-m-d H:i', '2019-01-01 00:00');

                //$payload = [];
                //$payload['procedure'] = "GetDiscount";
                //$payload['parameters'] = [$datetime->format('Y-m-d H:i:s'), '0'];

                $payload = [];
                $payload['procedure'] = "LastTransactionAccount";
                //$payload['procedure'] = "AccountsUpdated";
                $payload['parameters'] = [$datetime->format('Y-m-d H:i:s')];

                //Log::warning($payload);

                $post_content = \GuzzleHttp\json_encode($payload);
                $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                curl_setopt($ch, CURLOPT_ENCODING, "gzip");

                $content = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                //Log::warning($content);
                //Log::error($httpCode);


                if ($httpCode >= 200 && $httpCode < 300) {
                    // pass
                    if ($content) {
                        $payload = \GuzzleHttp\json_decode($content);
                        if (!$payload->error) {
                            //Log::warning($content);
                            //Log::warning($payload);

                            $missingaccountid = false;
                            foreach ($payload->data as $data) {
                                if (!isset($data->accountid)) {
                                    continue;
                                }

                                //Log::warning($data->accountid);
                                if (isset($data->accountid)) {
                                    $member = Member::with('user')->where('bepoz_account_id', $data->accountid)->first();
                                    if (!is_null($member)) {

                                        $payloadAccount = [];
                                        $payloadAccount['procedure'] = "GetDiscountAccount";
                                        $payloadAccount['parameters'] = [$data->accountid];

                                        $post_content_account = \GuzzleHttp\json_encode($payloadAccount);
                                        $encryptedAccount = strtoupper(hash_hmac('sha1', $post_content_account, $mac, false));


                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encryptedAccount, 'Content-Type: application/json'));
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($ch, CURLOPT_URL, $url);
                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content_account);
                                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                                        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                                        curl_setopt($ch, CURLOPT_ENCODING, "gzip");

                                        $contentAccount = curl_exec($ch);
                                        $httpCodeAccount = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                        curl_close($ch);

                                        if ($httpCodeAccount >= 200 && $httpCodeAccount < 300) {
                                            // pass
                                            if ($contentAccount) {
                                                //Log::warning($contentAccount);

                                                $payloadSaved = \GuzzleHttp\json_decode($contentAccount);

                                                if (!$payloadSaved->error) {
                                                    foreach ($payloadSaved->data as $dataAccount) {
                                                        //Log::warning($dataAccount->TotalSaved);
                                                        if (isset($dataAccount->TotalSaved)) {
                                                            $member->saved = intval($dataAccount->TotalSaved);
                                                            $member->save();
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $log = new SystemLog();
                                            $log->type = 'bepoz-job-error';
                                            $log->humanized_message = 'Polling account total saved is failed. Please check the error message';
                                            $log->message = "Polling data member '.$data->accountid.' from bepoz return http code " . $httpCodeAccount;
                                            $log->source = 'PollAccountTotalSaved.php';
                                            $log->save();
                                        }

                                    }
                                } else {
                                    $missingaccountid = true;
                                }
                            }

                            $last_execution_time->value = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
                            $last_execution_time->save();

                            if ($missingaccountid) {
                                $log = new SystemLog();
                                $log->type = 'bepoz-job-error';
                                $log->humanized_message = 'Polling account total saved is failed. Please check the error message';
                                $log->message = "accountid not found, please check setting for Bepoz LastTransactionAccount stored procedure, or connection";
                                $log->source = 'PollAccountTotalSaved.php';
                                $log->save();
                            }

                        }
                    } else {
                        //Log::error(curl_error($ch));
                        //Log::error(curl_errno($ch));

                        //$log = new SystemLog();
                        //$log->type = 'bepoz-job-error';
                        //$log->humanized_message = 'Polling account total saved is failed. Please check the error message';
                        //$log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                        //$log->source = 'PollAccountTotalSaved.php';
                        //$log->save();
                    }
                } else {
                    // 404
                    //Log::error($httpCode);

                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Polling account total saved is failed. Please check the error message';
                    $log->message = \GuzzleHttp\json_encode(array("error" => 'LastTransactionAccount ' . $httpCode . ' Not Found', "error_code" => $httpCode));
                    $log->source = 'PollAccountTotalSaved.php';
                    $log->save();
                }
            }


        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Polling account total saved is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'PollAccountTotalSaved.php';
            $log->save();

        }
    }
}
