<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Helpers\Bepoz;
use App\Member;
use App\PointLog;
use App\SystemLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class SyncPointBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-point-balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync member point balance in bepoz';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {

            $checkBepozConnection = $bepoz->SystemCheck();

            if ($checkBepozConnection) {

                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', 'sync-point-balance')
                    ->chunk(50, function ($jobs) use ($bepoz) {
                        // Reserve the selected jobs first before modification
                        foreach ($jobs as $bepoz_job) {
                            $bepoz_job->setJobUID(Uuid::generate());
                            $bepoz_job->reserve();
                        }

                        foreach ($jobs as $bepoz_job) {

                            if ($bepoz_job->attempts() < 3) {
                                $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                $bepoz_job->save();

                                try {
                                    $member = Member::where('bepoz_account_id', $bepoz_job->payload)->first();
                                    $temp = $bepoz->AccBalanceGet($bepoz_job->payload);

                                    if ($temp) {
                                        // Log
                                        $pl = new PointLog();
                                        $pl->member_id = $member->id;
                                        $pl->points_before = $member->points;
                                        $pl->points_after = intval($temp['AccBalance']['PointBalance']) / 100;
                                        $pl->desc_short = "Got request from Bepoz to refresh member's point balances.";
                                        $pl->desc_long = "New balances for Member: " . $member->id . " is " . (intval($temp['AccBalance']['PointBalance']) / 100) . " points.";
                                        $pl->save();

                                        $member->points = intval($temp['AccBalance']['PointBalance']) / 100;
                                        $member->balance = intval($temp['AccBalance']['AccountBalance']) / 100;
                                        $member->save();

                                        if (!is_null($bepoz_job->failed_job_uid)) {
                                            $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                            if (!is_null($failed_job)) {
                                                $failed_job->delete();
                                            }
                                        }

                                        $bepoz_job->dispatch();
                                    } else {
                                        $log = new SystemLog();
                                        $log->type = 'bepoz-job-error';
                                        $log->humanized_message = 'Syncing point balance is failed. Please check log.';
                                        $log->payload = $bepoz_job->payload;
                                        $log->message = $temp;
                                        $log->source = 'SyncPointBalance.php';
                                        $log->save();

                                        $bepoz_job->free();
                                    }

                                } catch (\Exception $e) {
                                    $log = new SystemLog();
                                    $log->humanized_message = 'Syncing point balance is failed. Please check the error message';
                                    $log->type = 'bepoz-job-error';
                                    $log->payload = $bepoz_job->payload;
                                    $log->message = $e;
                                    $log->source = 'SyncPointBalance.php';
                                    $log->save();

                                    $bepoz_job->free();
                                }
                            } else {

                                if (is_null($bepoz_job->failed_job_uid)) {

                                    $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                        ->where('queue', $bepoz_job->queue)
                                        ->first();

                                    if (is_null($failed_job)) {
                                        $failed_job = new BepozFailedJob;
                                        $failed_job->queue = $bepoz_job->queue;
                                        $failed_job->payload = $bepoz_job->payload;
                                        $failed_job->job_uid = Uuid::generate(4);
                                        $failed_job->save();
                                    }
                                }

                                // Maximum attempt log no longer needed
                                // $log = new SystemLog();
                                // $log->type = 'bepoz-job-error';
                                // $log->payload = $bepoz_job->payload;
                                // $log->humanized_message = 'Maximum attempt of syncing point balance has been reached.';
                                // $log->source = 'SyncPointBalance.php';
                                // $log->save();

                                $bepoz_job->dispatch();
                            }
                        }
                    }
                    );

                /*
                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', 'sync-point-balance')
                    ->limit(25)
                    ->get();

                if (!$jobs->isEmpty()) {
                    // Reserve the selected jobs first before modification
                    foreach ($jobs as $bepoz_job) {
                        $bepoz_job->setJobUID(Uuid::generate());
                        $bepoz_job->reserve();
                    }

                    foreach ($jobs as $bepoz_job) {

                        if ($bepoz_job->attempts() < 3) {
                            $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                            $bepoz_job->save();

                            try {
                                $member = Member::where('bepoz_account_id', $bepoz_job->payload)->first();
                                $temp = $bepoz->AccBalanceGet($bepoz_job->payload);

                                if ($temp) {
                                    // Log
                                    $pl = new PointLog();
                                    $pl->member_id = $member->id;
                                    $pl->points_before = $member->points;
                                    $pl->points_after = intval($temp['AccBalance']['PointBalance']) / 100;
                                    $pl->desc_short = "Got request from Bepoz to refresh member's point balances.";
                                    $pl->desc_long = "New balances for Member: " . $member->id . " is " . (intval($temp['AccBalance']['PointBalance']) / 100) . " points.";
                                    $pl->save();

                                    $member->points = intval($temp['AccBalance']['PointBalance']) / 100;
                                    $member->save();

                                    if (!is_null($bepoz_job->failed_job_uid)) {
                                        $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                        if (!is_null($failed_job)) {
                                            $failed_job->delete();
                                        }
                                    }

                                    $bepoz_job->dispatch();
                                } else {
                                    $log = new SystemLog();
                                    $log->type = 'bepoz-job-error';
                                    $log->humanized_message = 'Syncing point balance is failed. Please check log.';
                                    $log->payload = $bepoz_job->payload;
                                    $log->message = $temp;
                                    $log->source = 'SyncPointBalance.php';
                                    $log->save();

                                    $bepoz_job->free();
                                }

                            } catch (\Exception $e) {
                                $log = new SystemLog();
                                $log->humanized_message = 'Syncing point balance is failed. Please check the error message';
                                $log->type = 'bepoz-job-error';
                                $log->payload = $bepoz_job->payload;
                                $log->message = $e;
                                $log->source = 'SyncPointBalance.php';
                                $log->save();

                                $bepoz_job->free();
                            }
                        } else {

                            if (is_null($bepoz_job->failed_job_uid)) {

                                $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                    ->where('queue', $bepoz_job->queue)
                                    ->first();

                                if (is_null($failed_job)) {
                                    $failed_job = new BepozFailedJob;
                                    $failed_job->queue = $bepoz_job->queue;
                                    $failed_job->payload = $bepoz_job->payload;
                                    $failed_job->job_uid = Uuid::generate(4);
                                    $failed_job->save();
                                }
                            }

                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->payload = $bepoz_job->payload;
                            $log->humanized_message = 'Maximum attempt of syncing point balance has been reached.';
                            $log->source = 'SyncPointBalance.php';
                            $log->save();

                            $bepoz_job->dispatch();
                        }
                    }
                }
                */
            }


        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Syncing point balance is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SyncPointBalance.php';
            $log->save();


        }
    }
}
