<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\MyplaceCache;
use App\Setting;
use App\SystemLog;
use App\PrizePromotion;
use App\PromoAccount;
use App\Tier;
use Illuminate\Console\Command;
use App\Helpers\Bepoz;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

class PollStampCard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poll-stamp-card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update stamp card based on the data given';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        try {
            // Log::info("PollStampCard START");
            // Log::info(Carbon::now(config('app.timezone')));
            
            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;

            //Log::warning("poll_account_total_saved");
            $last_execution_time = Setting::where('key', 'bepoz_stamp_card_last_successful_execution_time')->first();
            $datetime = new Carbon($last_execution_time->value);
            $datetime->addHours(-1);

            //$datetime = Carbon::createFromFormat('Y-m-d H:i', '2019-01-01 00:00');

            $payload = [];
            $payload['procedure'] = "AccPromoUpdated";
            //$payload['procedure'] = "AccountsUpdated";
            $payload['parameters'] = [$datetime->format('Y-m-d H:i:s')];

            // $cache = MyplaceCache::where('key', 'member_ids')->first();
            // $payload['procedure'] = "AccPromoUpdatedMembers";
            // $payload['parameters'] = [$datetime->format('Y-m-d H:i:s'), $cache->value];
            //Log::warning($payload);

            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::warning("PollStampCard");
            // Log::warning($payload);
            // Log::warning($content);
            // Log::error($httpCode);

            if ($httpCode >= 200 && $httpCode < 300) {
                // pass
                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);

                    // Log::warning($payload->error);

                    if ($payload->error === true) {
                        // Log::warning("error detect boolean");
                    }

                    if ($payload->error === "true") {
                        // Log::warning("error detect string");
                    }

                    if ($payload->error === false) {
                        // Log::warning("no error boolean");
                    }

                    if ($payload->error === "false") {
                        // Log::warning("no error string");
                    }

                    if ($payload->error === false) {
                        // Log::warning($content);
                        // Log::warning($payload);

                        $last_execution_time->value = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
                        $last_execution_time->save();

                        
                        $missingAccountID = false;

                        $payloadReady = array_filter($payload->data, function($k) {
                            return isset($k->AccountID) && !empty($k->AccountID);
                        });
                        // Log::warning(print_r($payloadReady, true));

                        $chunkSize = 300;
                        $iChunk = 0;
                        $payloadChunks = array_chunk($payloadReady, $chunkSize, true);
                        // CHUNK BEPOZ RESULT
                        foreach ($payloadChunks as &$payloadChunk) {

                            
                            $AccPromoIDS = array_column($payloadChunk, 'AccountID');
                            
                            foreach ($payloadChunk as $key => &$rowChunk) {
                                $rowChunk->foundmyplace = "false";
                                $rowChunk->memberFoundCreate = "false";
                            }


                            $myplacePromoAccounts = PromoAccount::whereIn('bepoz_account_id', $AccPromoIDS)
                                ->chunk(100, function ($myplacePromoAccounts) use (&$payloadChunk, $AccPromoIDS, $iChunk, $chunkSize) {
    
                                    // FILTER VOUCHER WITH BEPOZ ID
                                    foreach ($myplacePromoAccounts as $promo_accounts) {
                                        $found_key_payload = array_search($promo_accounts->bepoz_account_id, $AccPromoIDS);
                                        if ($found_key_payload >= 0) {
                                            $indexTarget = $iChunk * $chunkSize + $found_key_payload;

                                            // Log::critical("found_key_payload ". $found_key_payload);
                                            // Log::critical("indexTarget ". $indexTarget);

                                            $data = $payloadChunk[$indexTarget];
                                            $payloadChunk[$indexTarget]->foundmyplace = "true";

                                            if (isset($data->AccountID)) {
                                                if (intval($data->AccountID) != 0) {
                                                    
                                                    if (is_null($promo_accounts)) {
                                                        $promo_accounts = new PromoAccount;
                                                    }
                
                                                    $needUpdate = false;
                
                                                    if ($promo_accounts->venue_id != $data->VenueID) {
                                                        $promo_accounts->venue_id = $data->VenueID;
                                                        $needUpdate = true;
                                                    }
                
                                                    if ($promo_accounts->bepoz_account_id != $data->AccountID) {
                                                        $promo_accounts->bepoz_account_id = $data->AccountID;
                                                        $member = Member::where('bepoz_account_id', $data->AccountID)->first();
                                                        if (!is_null($member)) {
                                                            $promo_accounts->member_id = $member->id;
                                                        } else {
                                                            continue;
                                                        }
                                                        $needUpdate = true;
                                                    }
                
                                                    if ($promo_accounts->bepoz_prize_promo_id != $data->PrizePromoID) {
                                                        $promo_accounts->bepoz_prize_promo_id = $data->PrizePromoID;
                                                        $prizePromotion = PrizePromotion::where('bepoz_prize_promo_id', $data->PrizePromoID)->first();
                                                        if (!is_null($prizePromotion)) {
                                                            $promo_accounts->prize_promotion_id = $prizePromotion->id;
                                                        }
                                                        $needUpdate = true;
                                                    }
                
                                                    if ($promo_accounts->qty_won_total != $data->QtyWonTotal) {
                                                        $promo_accounts->qty_won_total = $data->QtyWonTotal;
                                                        $needUpdate = true;
                                                    }
                
                                                    if ($promo_accounts->qty_last_day != $data->QtyLastDay) {
                                                        $promo_accounts->qty_last_day = $data->QtyLastDay;
                                                        $needUpdate = true;
                                                    }
                
                                                    if ($promo_accounts->date_last_day != $data->DateLastday) {
                                                        $promo_accounts->date_last_day = $data->DateLastday;
                                                        $needUpdate = true;
                                                    }
                
                                                    if ($promo_accounts->accumulation != intval($data->Accumulation) / 100) {
                                                        $promo_accounts->accumulation = intval($data->Accumulation) / 100;
                                                        $needUpdate = true;
                                                    }
                
                                                    if ($needUpdate) {
                                                        $promo_accounts->date_updated = $data->DateUpdated;
                                                        $promo_accounts->save();
                                                    }
                
                                                    //Log::warning($data->accountid);
                
                                                }
                                            } else {
                                                $missingAccountID = true;
                                            }
                                        }

                                    }
                                }
                            );
                        }

                        $payloadCreate = array_filter($payload->data, function($k) {
                            return $k->foundmyplace == 'false';
                        });
                        // Log::warning("payloadCreate");
                        // Log::warning(print_r($payloadCreate, true));

                        $bepoz_ids = array_column($payloadCreate, 'AccountID');

                        $members = Member::whereIn('bepoz_account_id', $bepoz_ids)
                            ->chunk(100, function ($members) use (&$payloadCreate, $payload) {

                                // FILTER VOUCHER WITH BEPOZ ID
                                foreach ($members as $member) {
                                    // Log::warning($member);

                                    $payloadMemberCreate = array_filter($payloadCreate, function($k) use ($member) {
                                        return $k->AccountID == $member->bepoz_account_id;
                                    });
                                    
                                    foreach ($payloadMemberCreate as &$rowCreate) {
                                        $rowCreate->memberFoundCreate = "true";
                                    
                                        $promo_accounts = new PromoAccount;
                                        $promo_accounts->venue_id = $rowCreate->VenueID;
                                        $promo_accounts->member_id = $member->id;
                                        $promo_accounts->qty_won_total = $rowCreate->QtyWonTotal;
                                        $promo_accounts->bepoz_prize_promo_id = $rowCreate->PrizePromoID;
                                        $prizePromotion = PrizePromotion::where('bepoz_prize_promo_id', $rowCreate->PrizePromoID)->first();
                                        if (!is_null($prizePromotion)) {
                                            $promo_accounts->prize_promotion_id = $prizePromotion->id;
                                        }
                                        $promo_accounts->qty_last_day = $rowCreate->QtyLastDay;
                                        $promo_accounts->date_last_day = $rowCreate->DateLastday;
                                        $promo_accounts->accumulation = intval($rowCreate->Accumulation) / 100;
                                        $promo_accounts->date_updated = $rowCreate->DateUpdated;
                                        $promo_accounts->save();

                                    }
                                }
                            }
                        );
                        
                        if ($missingAccountID) {
                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->humanized_message = 'Polling Stamp Card is failed. Please check the error message';
                            $log->message = "missing Account ID";
                            $log->source = 'PollStampCard.php';
                            $log->save();
                        }

                    } else {
                        $log = new SystemLog();
                        $log->type = 'bepoz-job-error';
                        $log->humanized_message = 'Polling Stamp Card is failed. Please check the error message';
                        $log->message = \GuzzleHttp\json_encode(array("error" => $payload->data));
                        $log->source = 'PollStampCard.php';
                        $log->save();
                    }


                } else {
                    //Log::error(curl_error($ch));
                    //Log::error(curl_errno($ch));

                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Polling Stamp Card is failed. Please check the error message';
                    $log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                    $log->source = 'PollStampCard.php';
                    $log->save();
                }
            } else {
                // 404
                //Log::error($httpCode);

                $log = new SystemLog();
                $log->type = 'bepoz-job-error';
                $log->humanized_message = 'Poll Stamp Card is failed. Please check the error message';
                $log->message = \GuzzleHttp\json_encode(array("error" => 'AccPromoUpdated ' . $httpCode . ' Not Found', "error_code" => $httpCode));
                $log->source = 'PollStampCard.php';
                $log->save();
            }

            // Log::info("PollStampCard END");
            // Log::info(Carbon::now(config('app.timezone')));
            
        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Polling Stamp Card is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'PollStampCard.php';
            $log->save();

        }
    }



}
