<?php

namespace App\Console\Commands\Bepoz;

use App\SystemLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Helpers\Bepoz;
use App\VoucherSetups;
use Illuminate\Support\Facades\Log;

class SyncVoucherSetups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-voucher-setups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync bepoz voucher-setups to the cloud';

    /**
     * Execute the console command.
     *
     * @param Bepoz $bepoz
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            $checkBepozConnection = $bepoz->SystemCheck();

            if ($checkBepozConnection) {

                $result = $bepoz->IDListGet(13); // issue voucher

                if (!$result) {
                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->message = $result;
                    $log->humanized_message = 'Syncing voucher setup is failed. Please check log.';
                    $log->source = 'SyncVoucherSetups.php';
                    $log->save();
                } else {
                    if (intval($result['IDList']['Count']) > 0) {
                        for ($i = 0; $i < count($result['IDList']['ID']['int']); $i++) {
                            $voucherSetupInformation = $bepoz->VoucherSetupGet($result['IDList']['ID']['int'][$i]);
                            $voucherSetup = VoucherSetups::where('bepoz_voucher_setup_id', '=', $result['IDList']['ID']['int'][$i])->first();

                            if (is_null($voucherSetup)) {
                                // create
                                $voucherSetup = new VoucherSetups;
                                $voucherSetup->bepoz_voucher_setup_id = $result['IDList']['ID']['int'][$i];
                                $voucherSetup->name = $result['IDList']['Name']['string'][$i];
                                $voucherSetup->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                                $voucherSetup->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                                $voucherSetup->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                                $voucherSetup->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                                $voucherSetup->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                                $voucherSetup->save();
                            } else {
                                // update
                                $voucherSetup->name = $result['IDList']['Name']['string'][$i];
                                $voucherSetup->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                                $voucherSetup->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                                $voucherSetup->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                                $voucherSetup->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                                $voucherSetup->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                                $voucherSetup->save();
                            }

                        }
                    }

                }
            }


        } catch (\Exception $e) {
            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Syncing voucher setup is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SyncVoucherSetups.php';
            $log->save();

        }

    }
}
