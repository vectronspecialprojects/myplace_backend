<?php

namespace App\Console\Commands\Bepoz;

use App\ClaimedPromotion;
use App\GiftCertificate;
use App\Listing;
use App\ListingProduct;
use App\Member;
use App\MemberLog;
use App\MemberSystemNotification;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Platform;
use App\Product;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\SystemLog;
use App\SystemNotification;
use App\VoucherSetups;

use App\Jobs\Job;
use App\Jobs\SendOneSignalNotification;
use App\Helpers\Bepoz;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class VoucherUpdatedIndividual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher-updated-individual {memberid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call Bepoz stored procedure VoucherUpdatedIndividual with Bepoz ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            if (empty($this->argument('memberid'))) {
                // add delay to prevent duplicate
                sleep(30);
            } else {

                $member = Member::find($this->argument('memberid'));
                $user = $member->user;

                if (!is_null($user->member->bepoz_account_id)) {
                    if (intval($user->member->bepoz_account_id) > 0) {
                        try {
                            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
                            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
                            $platform = Platform::where('name', 'web')->first();
                            $tc = Setting::where('key', 'invoice_logo')->first();

                            // $last_execution_time = Setting::where('key', 'bepoz_voucher_last_successful_execution_time')->first();
                            // $datetime = new Carbon($last_execution_time->value);
                            // $datetime->subDays(300);
                            // Log::info($datetime);

                            $payload = [];
                            $payload['procedure'] = "VoucherUpdatedIndividual";
                            $payload['parameters'] = [$user->member->bepoz_account_id];

                            $post_content = \GuzzleHttp\json_encode($payload);
                            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);

                            // Log::warning($httpCode);
                            // Log::warning($content);

                            
                            if ($httpCode >= 200 && $httpCode < 300) {
                                if ($content) {
                                    $payload = \GuzzleHttp\json_decode($content);
                                    if (!$payload->error) {

                                        $company_name = Setting::where('key', 'company_name')->first()->value;
                                        $pos_voucher_enable = Setting::where('key', 'pos_voucher_enable')->first()->value;
                                        // Log::info($pos_voucher_enable);

                                        // CACHE PRODUCT
                                        // $productCache = [];
                                        // $voucherSetupCache = [];

                                        foreach ($payload->data as $data) {
                                            if (!isset($data->VoucherID)) {
                                                continue;
                                            }
                                            // Log::warning(print_r($data, true));

                                            $memberVoucher = MemberVouchers::where('voucher_id', $data->VoucherID)->first();

                                            //if memberVoucher exist, update status
                                            if (!is_null($memberVoucher)) {
                                                // Log::info("VoucherUpdatedIndividual update");
                                                if ($data->AmountLeft > 0) {
                                                    $memberVoucher->redeemed = false;

                                                    $cp = ClaimedPromotion::where('member_voucher_id', $memberVoucher->id)->first();
                                                    if (!is_null($cp)) {
                                                        $cp->is_used = false;
                                                        $cp->save();
                                                    }
                                                } else {
                                                    $memberVoucher->redeemed = true;

                                                    $cp = ClaimedPromotion::where('member_voucher_id', $memberVoucher->id)->first();
                                                    if (!is_null($cp)) {
                                                        $cp->is_used = true;
                                                        $cp->save();
                                                    }
                                                }


                                                if ($data->StopRedeems == 1) {
                                                    $memberVoucher->redeemed = true;

                                                    $cp = ClaimedPromotion::where('member_voucher_id', $memberVoucher->id)->first();
                                                    if (!is_null($cp)) {
                                                        $cp->is_used = true;
                                                        $cp->save();
                                                    }
                                                }

                                                $memberVoucher->used_value = $data->UsedValue;
                                                $memberVoucher->amount_left = $data->AmountLeft;
                                                $memberVoucher->lookup = $data->Lookup;
                                                $memberVoucher->bepoz_prize_promo_id = $data->PrizePromoID;
                                                $prize = PrizePromotion::where('bepoz_prize_promo_id', $data->PrizePromoID)->first();
                                                if (!is_null($prize)) {
                                                    $memberVoucher->prize_promotion_id = $prize->id;
                                                }

                                                $raw_barcode = "99" . str_repeat("0", 10 - strlen($data->Lookup)) . $data->Lookup;
                                                $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);

                                                $memberVoucher->expire_date = strtotime($data->DateExpiry) && !is_null($data->DateExpiry) ? Carbon::parse($data->DateExpiry) : null;
                                                $memberVoucher->used_date = strtotime($data->UsedDate) && !is_null($data->UsedDate) ? Carbon::parse($data->UsedDate) : null;
                                                
                                                $gf = GiftCertificate::where('lookup', $data->Lookup)->first();
                                                if ( !is_null($gf) ) {
                                                    // Log::info("VoucherUpdatedIndividual update GiftCertificate");
                                                    $memberVoucher->category = 'gift_certificate';
                                                }

                                                $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                                if (!is_null($vs)) {
                                                    $memberVoucher->voucher_setup_id = $vs->id;
                                                }
                                                
                                                $memberVoucher->save();

                                            } else {
                                                if ( $pos_voucher_enable == "true" ) {
                                                    
                                                    // Log::info("VoucherUpdatedIndividual create");
                                                    //if memberVoucher not exist so create one, load free voucher from bepoz
                                                    if (intval($data->AccountID) !== 0) {
                                                        $member = Member::where('bepoz_account_id', $data->AccountID)->first();
                                                        if (!is_null($member)) {
                                                            // new vouchers issued from BEPOZ, like buy 5 get 1 free
                                                            //Log::info($last_execution_time->value);
                                                            // Log::info(print_r($data, true));

                                                            $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                                            //if VoucherSetups not found then create new one
                                                            if (is_null($vs)) {
                                                                
                                                                // array_key_exists();
                                                                // $voucherSetupCache = [];
                                                                $voucherSetupInformation = $bepoz->VoucherSetupGet($data->VoucherSetupID);
                                                                //Log::info($voucherSetupInformation);
                                                                //
                                                                if ($voucherSetupInformation) {
                                                                    DB::beginTransaction();
                                                                    $vs = new VoucherSetups;
                                                                    $vs->bepoz_voucher_setup_id = $data->VoucherSetupID;
                                                                    $vs->name = $voucherSetupInformation['VoucherSetup']['Name'];
                                                                    $vs->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                                                                    $vs->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                                                                    $vs->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                                                                    $vs->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                                                                    $vs->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                                                                    $vs->voucher_apply = intval($voucherSetupInformation['VoucherSetup']['VoucherApply']);
                                                                    $vs->save();
                                                                    DB::commit();
                                                                } else {
                                                                    $log = new SystemLog();
                                                                    $log->type = 'bepoz-job-error';
                                                                    $log->humanized_message = 'Polling voucher setup is failed. Please check the error message';
                                                                    $log->message = 'Failed to reach bepoz connection';
                                                                    $log->source = 'VoucherUpdatedIndividual.php';
                                                                    $log->save();
                                                                }
                                                            }

                                                            //load VoucherSetups again
                                                            $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                                            if (!is_null($vs)) {

                                                                $product = Product::where('bepoz_voucher_setup_id', $vs->id)->first();

                                                                $createBepozVoucher = false;

                                                                if (is_null($product)) {
                                                                    $product = new Product();
                                                                    $product->name  = $vs->name;
                                                                    $product->bepoz_voucher_setup_id  = $vs->id;
                                                                    $product->product_type_id  = '1';
                                                                    $product->is_free  = '1';
                                                                    $product->status  = 'active';
                                                                    $product->category  = 'voucher';
                                                                    $product->payload  = '[{"triggerNotif":true,"notifType":"push","notifContentType":"custom","notifContent":"Your '.$vs->name.' is ready","notifTier":[]}]';
                                                                    $product->image  = $tc->value;
                                                                    $product->desc_short  = "";

                                                                    $product->save();

                                                                    $createBepozVoucher = true;
                                                                } else {
                                                                    if ($product->status == 'active' ) {
                                                                        $createBepozVoucher = true;
                                                                    } else {
                                                                        $createBepozVoucher = false;
                                                                    }
                                                                }

                                                                // BLOCK ALL INACTIVE VOUCHER
                                                                if ($data->AmountLeft > 0) {
                                                                    $createBepozVoucher = true;
                                                                } else {
                                                                    $createBepozVoucher = false;
                                                                }
        
                                                                if ($data->StopRedeems == 1) {
                                                                    $createBepozVoucher = false;
                                                                }

                                                                if ($createBepozVoucher) {

                                                                    // Log::warning("Product ".$product->id." createBepozVoucher");
                                                                    // Log::warning(print_r($data, true));
                                                                    DB::beginTransaction();
                                                                    //create Transaction
                                                                    //create Order
                                                                    $order = new Order();
                                                                    $order->member_id  = $member->id;
                                                                    $order->type  = 'cash';
                                                                    $order->expired  = time();
                                                                    $order->order_status  = 'confirmed';
                                                                    $order->payment_status  = 'successful';
                                                                    $order->voucher_status  = 'successful';
                                                                    $order->transaction_type  = 'bepoz';
                                                                    $order->payload  = '{"app_token":"'. $platform->app_token .'","app_secret":"'. $platform->app_secret .
                                                                        '","transaction_type":"bepoz","notifSend":"false"}';
                                                                    // $order->save();
                                                                    $payload = \GuzzleHttp\json_encode($order);
                                                                    $token = md5($payload);
                                                                    $order->token = $token;
                                                                    $order->ip_address = '0.0.0.0';
                                                                    $order->save();

                                                                    //create memberVoucher
                                                                    $memberVoucher = new MemberVouchers();
                                                                    $memberVoucher->member_id = $member->id;
                                                                    $memberVoucher->name = $vs->name;
                                                                    $memberVoucher->voucher_setup_id = $vs->id;

                                                                    //create order detail
                                                                    $orderDetail = new OrderDetail();
                                                                    $orderDetail->qty = 1;
                                                                    $orderDetail->product_name = $vs->name;
                                                                    $orderDetail->voucher_name = $vs->name;
                                                                    $orderDetail->voucher_lookup = $data->Lookup;
                                                                    $orderDetail->order_id  = $order->id;
                                                                    $orderDetail->voucher_setups_id  = $vs->id;
                                                                    $orderDetail->category = 'bepoz';
                                                                    $orderDetail->payload  = '{"notifSend":"false"}';


                                                                    $listingProduct = ListingProduct::where('product_id', $product->id)->first();
                                                                    if (!is_null($listingProduct)) {
                                                                        $orderDetail->listing_id  = $listingProduct->listing_id;
                                                                    } else {

                                                                    }

                                                                    $orderDetail->product_id  = $product->id;
                                                                    $orderDetail->product_type_id  = $product->product_type_id;
                                                                    $orderDetail->save();

                                                                    $gf = GiftCertificate::where('lookup', $data->Lookup)->first();
                                                                    if ( !is_null($gf) ) {
                                                                        // Log::info("VoucherUpdatedIndividual create GiftCertificate");
                                                                        $memberVoucher->category = 'gift_certificate';
                                                                    } else {                                                                        
                                                                        $memberVoucher->category = 'bepoz';
                                                                    }

                                                                    $memberVoucher->voucher_id = $data->VoucherID;
                                                                    $memberVoucher->lookup = $data->Lookup;
                                                                    $memberVoucher->voucher_type = $data->VoucherType;
                                                                    $memberVoucher->voucher_apply = $data->VoucherApply;
                                                                    $memberVoucher->unlimited_use = filter_var($data->UnlimitedUse, FILTER_VALIDATE_BOOLEAN);
                                                                    $memberVoucher->maximum_discount = $data->MaximumDiscount;
                                                                    $memberVoucher->claim_venue_id = $data->ClaimVenueID;
                                                                    $memberVoucher->claim_store_id = $data->ClaimStoreID;
                                                                    $memberVoucher->used_count = $data->UsedCount;
                                                                    $memberVoucher->used_value = $data->UsedValue;
                                                                    $memberVoucher->used_trans_id = $data->UsedTransID;
                                                                    $memberVoucher->amount_left = $data->AmountLeft;
                                                                    $memberVoucher->amount_issued = $data->AmountIssued;
                                                                    $memberVoucher->status = 'successful';
                                                                    $memberVoucher->order_id  = $order->id;
                                                                    $memberVoucher->order_details_id  = $orderDetail->id;

                                                                    $memberVoucher->bepoz_prize_promo_id = $data->PrizePromoID;
                                                                    $prize = PrizePromotion::where('bepoz_prize_promo_id', $data->PrizePromoID)->first();
                                                                    if (!is_null($prize)) {
                                                                        $memberVoucher->prize_promotion_id = $prize->id;
                                                                    }

                                                                    $raw_barcode = "99" . str_repeat("0", 10 - strlen($data->Lookup)) . $data->Lookup;
                                                                    $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                                                    $memberVoucher->issue_date = Carbon::parse($data->IssuedDate);
                                                                    $memberVoucher->expire_date = strtotime($data->DateExpiry) && !is_null($data->DateExpiry) ? Carbon::parse($data->DateExpiry) : null;
                                                                    $memberVoucher->save();

                                                                    $memberVoucher->voucherSetupInformation = \GuzzleHttp\json_encode($vs);
                                                                    //create memberlog
                                                                    $ml = new MemberLog();
                                                                    $ml->member_id = $member->id;
                                                                    $ml->message = "This member got new free voucher";
                                                                    $ml->after = \GuzzleHttp\json_encode($memberVoucher);
                                                                    $ml->save();

                                                                    $contents = "Your ".$memberVoucher->name." voucher is ready";

                                                                    if (!$product->payload){
                                                                        $product->payload  = '[{"triggerNotif":true,"notifType":"push","notifContentType":"default","notifContent":"Your '.$vs->name.' is ready","notifTier":[]}]';
                                                                        $product->save();
                                                                        // Log::warning("payload not available");
                                                                        //load again
                                                                        $notif = \GuzzleHttp\json_decode($product->payload);
                                                                    }

                                                                    DB::commit();

                                                                    $notif = \GuzzleHttp\json_decode($product->payload);

                                                                    if (isset($notif[0]->notifType)){
                                                                        if ($notif[0]->notifType === "system" ){
                                                                            $msn = new MemberSystemNotification();
                                                                            $msn->member_id = $member->id;
                                                                            $msn->system_notification_id = $notif[0]->notification_id;
                                                                            $msn->status = 'unread';
                                                                            $msn->sent_at = Carbon::now(config('app.timezone'));
                                                                            $msn->save();

                                                                            $sn = SystemNotification::find($notif[0]->notification_id);
                                                                            $contents = $sn->message;
                                                                        }

                                                                        if ($notif[0]->notifType === "push" ){
                                                                            $contents = $notif[0]->notifContent;
                                                                        }

                                                                        $temp = [
                                                                            // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                                            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                                            //"contents" => ["en" => "Your voucher ($memberVoucher->name) is ready"],
                                                                            "contents" => ["en" => $contents],
                                                                            "headings" => ["en" => $company_name],
                                                                            "data" => ["category" => "voucher"],
                                                                            "ios_badgeType" => "Increase",
                                                                            "ios_badgeCount" => 1
                                                                        ];
                                                                        //Log::warning($product);
                                                                        //Log::warning($temp);

                                                                        dispatch(new SendOneSignalNotification($temp));

                                                                    }

                                                                }
                                                            }

                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                } else {
                                    //$log = new SystemLog();
                                    //$log->type = 'bepoz-job-error';
                                    //$log->humanized_message = 'Polling voucher is failed. Please check the error message';
                                    //$log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                                    //$log->source = 'VoucherUpdatedIndividual.php';
                                    //$log->save();
                                }
                            } else {
                                //$log = new SystemLog();
                                //$log->type = 'bepoz-job-error';
                                //$log->humanized_message = 'Polling voucher is failed. Please check the error message';
                                //$log->message = \GuzzleHttp\json_encode(array("error" => '404 Not Found', "error_code" => $httpCode));
                                //$log->source = 'VoucherUpdatedIndividual.php';
                                //$log->save();
                            }
                            

                        } catch (\Exception $e) {

                            // Log::warning($e);

                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->humanized_message = 'Polling voucher is failed. Please check the error message';
                            $log->message = $e;
                            $log->source = 'VoucherUpdatedIndividual.php';
                            $log->save();

                        }
                    } else {
                        // Log::info("bepoz_account_id 0");
                    }
                } else {
                    // Log::info("bepoz_account_id null");
                }
            }

        } catch (\Exception $e) {
            // Log::error($e);
        }
    }

    
    protected function check_digit($lookup)
    {
        $sum = 0;
        $codeString = str_split($lookup);

        for ($i = 0; $i < 12; $i++) {
            if (($i % 2) == 0)
                $sum += $codeString[$i];
            else
                $sum += (3 * $codeString[$i]);
        }

        $sum = $sum % 10;

        if ($sum > 0)
            $sum = 10 - $sum;
        else
            $sum = 0;

        return $sum;
    }
}
