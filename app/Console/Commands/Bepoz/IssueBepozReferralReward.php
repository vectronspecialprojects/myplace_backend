<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\FriendReferral;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberVouchers;
use App\Setting;
use App\SystemLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Helpers\Bepoz;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class IssueBepozReferralReward extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'issue-bepoz-referral-reward';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Issue Bepoz Referral Voucher';

    /**
     * Execute the console command.
     *
     * @param Bepoz $bepoz
     */
    public function handle(Bepoz $bepoz)
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {

            $checkBepozConnection = $bepoz->SystemCheck();

            if ($checkBepozConnection) {

                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', "issue-friend-referral-bepoz-voucher")
                    ->chunk(100, function ($jobs) use ($bepoz) {
                        // Reserve the selected jobs first before modification
                        foreach ($jobs as $bepoz_job) {
                            $bepoz_job->setJobUID(Uuid::generate());
                            $bepoz_job->reserve();
                        }

                        foreach ($jobs as $bepoz_job) {

                            if ($bepoz_job->attempts() < 3) {
                                $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                $bepoz_job->save();

                                DB::beginTransaction();

                                try {

                                    $data = \GuzzleHttp\json_decode($bepoz_job->payload);

                                    $voucherSetup = VoucherSetups::find($data->voucher_setups_id);
                                    $member = Member::find($data->member_id);

                                    if (!is_null($member->bepoz_account_id) && $member->bepoz_account_id !== 'null') {
                                        $result = $bepoz->VoucherIssue($voucherSetup->bepoz_voucher_setup_id, $member->bepoz_account_id, null);
                                        if ($result && isset($result['Voucher'])) {
                                            $memberVoucher = MemberVouchers::find($data->member_voucher_id);
                                            $memberVoucher->voucher_id = $result['Voucher']['VoucherID'];
                                            $memberVoucher->lookup = $result['Voucher']['Lookup'];
                                            // $memberVoucher->voucher_setup_id = $result['Voucher']['VoucherSetupID'];
                                            $memberVoucher->voucher_setup_id = $data->voucher_setups_id;
                                            $memberVoucher->voucher_type = $result['Voucher']['VoucherType'];
                                            $memberVoucher->unlimited_use = filter_var($result['Voucher']['UnlimitedUse'], FILTER_VALIDATE_BOOLEAN);
                                            $memberVoucher->maximum_discount = $result['Voucher']['MaximumDiscount'];
                                            $memberVoucher->claim_venue_id = $result['Voucher']['ClaimVenueID'];
                                            $memberVoucher->claim_store_id = $result['Voucher']['ClaimStoreID'];
                                            $memberVoucher->used_count = $result['Voucher']['UsedCount'];
                                            $memberVoucher->used_value = $result['Voucher']['UsedValue'];
                                            $memberVoucher->used_trans_id = $result['Voucher']['UsedTransID'];
                                            $memberVoucher->amount_left = $result['Voucher']['AmountLeft'];
                                            $memberVoucher->amount_issued = $result['Voucher']['AmountIssued'];
                                            $memberVoucher->status = 'successful';

                                            $raw_barcode = "99" . str_repeat("0", 10 - strlen($result['Voucher']['Lookup'])) . $result['Voucher']['Lookup'];
                                            $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                            $memberVoucher->issue_date = Carbon::parse($result['Voucher']['IssuedDate']);
                                            $memberVoucher->expire_date = strtotime($result['Voucher']['DateExpiry']) ? Carbon::parse($result['Voucher']['DateExpiry']) : null;
                                            $memberVoucher->save();

                                            if (!is_null($bepoz_job->failed_job_uid)) {
                                                $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                if (!is_null($failed_job)) {
                                                    $failed_job->delete();
                                                }
                                            }

                                            $fr = FriendReferral::find($data->referral_id);
                                            $fr->issued_at = Carbon::now(config('app.timezone'));
                                            $fr->save();

                                            $company_name = Setting::where('key', 'company_name')->first()->value;

                                            $data = [
                                                // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                "contents" => ["en" => "Your voucher reward (from Referral Friend) is ready"],
                                                "headings" => ["en" => $company_name],
                                                "data" => ["category" => "voucher"],
                                                "ios_badgeType" => "Increase",
                                                "ios_badgeCount" => 1
                                            ];
                                            dispatch(new SendOneSignalNotification($data));

                                            $bepoz_job->dispatch();

                                        } else {
                                            $log = new SystemLog();
                                            $log->type = 'bepoz-job-error';
                                            $log->humanized_message = 'Issuing bepoz referral voucher is failed. Please check log.';
                                            $log->payload = $bepoz_job->payload;
                                            $log->message = $result;
                                            $log->source = 'IssueBepozReferralReward.php';
                                            $log->save();

                                            $bepoz_job->free();
                                        }
                                    } else {
                                        $bepoz_job->free();
                                    }


                                    DB::commit();

                                } catch (\Exception $e) {
                                    DB::rollback();

                                    $log = new SystemLog();
                                    $log->humanized_message = 'Issuing bepoz referral voucher is failed. Please check the error message';
                                    $log->type = 'bepoz-job-error';
                                    $log->payload = $bepoz_job->payload;
                                    $log->message = $e;
                                    $log->source = 'IssueBepozReferralReward.php';
                                    $log->save();

                                    $bepoz_job->free();

                                }


                            } else {
                                DB::beginTransaction();

                                // Maximum attempt log no longer needed
                                // $log = new SystemLog();
                                // $log->type = 'bepoz-job-error';
                                // $log->payload = $bepoz_job->payload;
                                // $log->humanized_message = 'Maximum attempt of issuing bepoz referral voucher has been reached.';
                                // $log->source = 'IssueBepozReferralReward.php';
                                // $log->save();

                                if (is_null($bepoz_job->failed_job_uid)) {
                                    $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                        ->where('queue', $bepoz_job->queue)
                                        ->first();

                                    if (is_null($failed_job)) {
                                        $failed_job = new BepozFailedJob;
                                        $failed_job->queue = $bepoz_job->queue;
                                        $failed_job->payload = $bepoz_job->payload;
                                        $failed_job->job_uid = Uuid::generate(4);
                                        $failed_job->save();
                                    }
                                }

                                $bepoz_job->dispatch();

                                DB::commit();
                            }
                        }
                    }
                    );

                /*
                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', "issue-friend-referral-bepoz-voucher")
                    ->limit(10)
                    ->get();

                if (!$jobs->isEmpty()) {

                    // Reserve the selected jobs first before modification
                    foreach ($jobs as $bepoz_job) {
                        $bepoz_job->setJobUID(Uuid::generate());
                        $bepoz_job->reserve();
                    }

                    foreach ($jobs as $bepoz_job) {

                        if ($bepoz_job->attempts() < 3) {
                            $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                            $bepoz_job->save();

                            DB::beginTransaction();

                            try {

                                $data = \GuzzleHttp\json_decode($bepoz_job->payload);

                                $voucherSetup = VoucherSetups::find($data->voucher_setups_id);
                                $member = Member::find($data->member_id);

                                if (!is_null($member->bepoz_account_id) && $member->bepoz_account_id !== 'null') {
                                    $result = $bepoz->VoucherIssue($voucherSetup->bepoz_voucher_setup_id, $member->bepoz_account_id, null);
                                    if ($result) {
                                        $memberVoucher = MemberVouchers::find($data->member_voucher_id);
                                        $memberVoucher->voucher_id = $result['Voucher']['VoucherID'];
                                        $memberVoucher->lookup = $result['Voucher']['Lookup'];
                                        $memberVoucher->voucher_setup_id = $result['Voucher']['VoucherSetupID'];
                                        $memberVoucher->voucher_type = $result['Voucher']['VoucherType'];
                                        $memberVoucher->unlimited_use = filter_var($result['Voucher']['UnlimitedUse'], FILTER_VALIDATE_BOOLEAN);
                                        $memberVoucher->maximum_discount = $result['Voucher']['MaximumDiscount'];
                                        $memberVoucher->claim_venue_id = $result['Voucher']['ClaimVenueID'];
                                        $memberVoucher->claim_store_id = $result['Voucher']['ClaimStoreID'];
                                        $memberVoucher->used_count = $result['Voucher']['UsedCount'];
                                        $memberVoucher->used_value = $result['Voucher']['UsedValue'];
                                        $memberVoucher->used_trans_id = $result['Voucher']['UsedTransID'];
                                        $memberVoucher->amount_left = $result['Voucher']['AmountLeft'];
                                        $memberVoucher->amount_issued = $result['Voucher']['AmountIssued'];
                                        $memberVoucher->status = 'successful';

                                        $raw_barcode = "99" . str_repeat("0", 10 - strlen($result['Voucher']['Lookup'])) . $result['Voucher']['Lookup'];
                                        $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                        $memberVoucher->issue_date = Carbon::parse($result['Voucher']['IssuedDate']);
                                        $memberVoucher->expire_date = strtotime($result['Voucher']['DateExpiry']) ? Carbon::parse($result['Voucher']['DateExpiry']) : null;
                                        $memberVoucher->save();

                                        if (!is_null($bepoz_job->failed_job_uid)) {
                                            $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                            if (!is_null($failed_job)) {
                                                $failed_job->delete();
                                            }
                                        }

                                        $fr = FriendReferral::find($data->referral_id);
                                        $fr->issued_at = Carbon::now(config('app.timezone'));
                                        $fr->save();

                                        $company_name = Setting::where('key', 'company_name')->first()->value;

                                        $data = [
                                            // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                            "contents" => ["en" => "Your voucher reward (from Referral Friend) is ready"],
                                            "headings" => ["en" => $company_name],
                                            "data" => ["category" => "voucher"],
                                            "ios_badgeType" => "Increase",
                                            "ios_badgeCount" => 1
                                        ];
                                        dispatch(new SendOneSignalNotification($data));

                                        $bepoz_job->dispatch();

                                    } else {
                                        $log = new SystemLog();
                                        $log->type = 'bepoz-job-error';
                                        $log->humanized_message = 'Issuing bepoz referral voucher is failed. Please check log.';
                                        $log->payload = $bepoz_job->payload;
                                        $log->message = $result;
                                        $log->source = 'IssueBepozReferralReward.php';
                                        $log->save();

                                        $bepoz_job->free();
                                    }
                                }
                                else {
                                    $bepoz_job->free();
                                }


                                DB::commit();

                            } catch (\Exception $e) {
                                DB::rollback();

                                $log = new SystemLog();
                                $log->humanized_message = 'Issuing bepoz referral voucher is failed. Please check the error message';
                                $log->type = 'bepoz-job-error';
                                $log->payload = $bepoz_job->payload;
                                $log->message = $e;
                                $log->source = 'IssueBepozReferralReward.php';
                                $log->save();

                                $bepoz_job->free();

                            }


                        } else {
                            DB::beginTransaction();

                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->payload = $bepoz_job->payload;
                            $log->humanized_message = 'Maximum attempt of issuing bepoz referral voucher has been reached.';
                            $log->source = 'IssueBepozReferralReward.php';
                            $log->save();

                            if (is_null($bepoz_job->failed_job_uid)) {
                                $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                    ->where('queue', $bepoz_job->queue)
                                    ->first();

                                if (is_null($failed_job)) {
                                    $failed_job = new BepozFailedJob;
                                    $failed_job->queue = $bepoz_job->queue;
                                    $failed_job->payload = $bepoz_job->payload;
                                    $failed_job->job_uid = Uuid::generate(4);
                                    $failed_job->save();
                                }
                            }

                            $bepoz_job->dispatch();

                            DB::commit();
                        }
                    }
                }
                */
            }


        } catch (\Exception $e) {

            Log::warning($e);
            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Issuing bepoz referral voucher is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'IssueBepozReferralReward.php';
            $log->save();


        }

    }

    /**
     * Return check digit of the barcode
     *
     * @param $lookup
     * @return int
     */
    protected function check_digit($lookup)
    {
        $sum = 0;
        $codeString = str_split($lookup);

        for ($i = 0; $i < 12; $i++) {
            if (($i % 2) == 0)
                $sum += $codeString[$i];
            else
                $sum += (3 * $codeString[$i]);
        }

        $sum = $sum % 10;

        if ($sum > 0)
            $sum = 10 - $sum;
        else
            $sum = 0;

        return $sum;
    }
}
