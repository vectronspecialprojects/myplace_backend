<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Helpers\Bepoz;
use App\Member;
use App\PointLog;
use App\SystemLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class UpdateBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update member point / balance in bepoz';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {

            $checkBepozConnection = $bepoz->SystemCheck();

            if ($checkBepozConnection) {
                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', 'update-balance')
                    ->chunk(50, function ($jobs) use ($bepoz) {
                        // Reserve the selected jobs first before modification
                        foreach ($jobs as $bepoz_job) {
                            $bepoz_job->setJobUID(Uuid::generate());
                            $bepoz_job->reserve();
                        }

                        foreach ($jobs as $bepoz_job) {

                            if ($bepoz_job->attempts() < 3) {
                                $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                $bepoz_job->save();

                                try {
                                    $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                    $member = Member::find($data->member_id);

                                    if (!is_null($member->bepoz_account_id) && $member->bepoz_account_id !== 'null') {
                                        $result = $bepoz->AccountFullGet($member->bepoz_account_id);

                                        if ($result) {
                                            $nwData = array();
                                            $nwData['AccountID'] = $member->bepoz_account_id;
                                            $nwData['DateTimeActivity'] = Carbon::now(config('app.timezone'))->toAtomString();
                                            $nwData['ShiftID'] = 0;
                                            $nwData['VenueID'] = 0;
                                            $nwData['TransactionID'] = 0;
                                            $nwData['GrossTurnover'] = 0;
                                            $nwData['NettTurnover'] = 0;
                                            $nwData['CostTurnover'] = 0;
                                            $nwData['AccountCharged'] = 0;
                                            $nwData['AccountPaid'] = 0;
                                            if ($data->original_balance !== $data->balance) {
                                                if (floatval($data->original_balance) > floatval($data->balance)) {
                                                    $nwData['AccountCharged'] = intval(floatval($data->original_balance) * 100) - intval(floatval($data->balance) * 100);
                                                } else {
                                                    $nwData['AccountPaid'] = intval(floatval($data->balance) * 100) - intval(floatval($data->original_balance) * 100);
                                                }
                                            }

                                            $nwData['PointsEarned'] = 0;
                                            $nwData['PointsRedeemed'] = 0;
                                            if ($data->original_points !== $data->points) {
                                                if (floatval($data->original_points) > floatval($data->points)) {
                                                    $nwData['PointsRedeemed'] = intval(floatval($data->original_points) * 100) - intval(floatval($data->points) * 100);
                                                } else {
                                                    $nwData['PointsEarned'] = intval(floatval($data->points) * 100) - intval(floatval($data->original_points) * 100);
                                                }
                                            }

                                            $nwData['JoiningFee'] = 0;
                                            $nwData['RenewalFee'] = 0;

                                            $temp = $bepoz->BalanceUpdate($nwData);

                                            if ($temp) {
                                                // Log

                                                if (!is_null($bepoz_job->failed_job_uid)) {
                                                    $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                    if (!is_null($failed_job)) {
                                                        $failed_job->delete();
                                                    }
                                                }

                                                $bepoz_job->dispatch();
                                            } else {
                                                $log = new SystemLog();
                                                $log->type = 'bepoz-job-error';
                                                $log->humanized_message = 'Updating balance is failed. Please check the error message';
                                                $log->payload = $bepoz_job->payload;
                                                $log->message = $temp;
                                                $log->source = 'UpdateBalance.php';
                                                $log->save();

                                                $bepoz_job->free();
                                            }
                                        } else {
                                            $bepoz_job->free();
                                        }
                                    } else {
                                        $bepoz_job->free();
                                    }

                                } catch (\Exception $e) {
                                    $log = new SystemLog();
                                    $log->humanized_message = 'Updating balance is failed. Please check the error message';
                                    $log->type = 'bepoz-job-error';
                                    $log->payload = $bepoz_job->payload;
                                    $log->message = $e;
                                    $log->source = 'UpdateBalance.php';
                                    $log->save();

                                    $bepoz_job->free();
                                }
                            } else {

                                if (is_null($bepoz_job->failed_job_uid)) {

                                    $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                        ->where('queue', $bepoz_job->queue)
                                        ->first();

                                    if (is_null($failed_job)) {
                                        $failed_job = new BepozFailedJob;
                                        $failed_job->queue = $bepoz_job->queue;
                                        $failed_job->payload = $bepoz_job->payload;
                                        $failed_job->job_uid = Uuid::generate(4);
                                        $failed_job->save();
                                    }
                                }

                                // Maximum attempt log no longer needed
                                // $log = new SystemLog();
                                // $log->type = 'bepoz-job-error';
                                // $log->payload = $bepoz_job->payload;
                                // $log->humanized_message = 'Maximum attempt of updating balance has been reached.';
                                // $log->source = 'UpdateBalance.php';
                                // $log->save();

                                $bepoz_job->dispatch();
                            }
                        }
                    }
                );

                /*
                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', 'update-balance')
                    ->limit(5)
                    ->get();

                if (!$jobs->isEmpty()) {
                    // Reserve the selected jobs first before modification
                    foreach ($jobs as $bepoz_job) {
                        $bepoz_job->setJobUID(Uuid::generate());
                        $bepoz_job->reserve();
                    }

                    foreach ($jobs as $bepoz_job) {

                        if ($bepoz_job->attempts() < 3) {
                            $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                            $bepoz_job->save();

                            try {
                                $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                $member = Member::find($data->member_id);

                                if (!is_null($member->bepoz_account_id) && $member->bepoz_account_id !== 'null') {
                                    $result = $bepoz->AccountFullGet($member->bepoz_account_id);

                                    if ($result) {
                                        $nwData = array();
                                        $nwData['AccountID'] = $member->bepoz_account_id;
                                        $nwData['DateTimeActivity'] = Carbon::now(config('app.timezone'))->toAtomString();
                                        $nwData['ShiftID'] = 0;
                                        $nwData['VenueID'] = 0;
                                        $nwData['TransactionID'] = 0;
                                        $nwData['GrossTurnover'] = 0;
                                        $nwData['NettTurnover'] = 0;
                                        $nwData['CostTurnover'] = 0;
                                        $nwData['AccountCharged'] = 0;
                                        $nwData['AccountPaid'] = 0;
                                        if ($data->original_balance !== $data->balance) {
                                            if (floatval($data->original_balance) > floatval($data->balance)) {
                                                $nwData['AccountCharged'] = intval(floatval($data->original_balance) * 100) - intval(floatval($data->balance) * 100);
                                            } else {
                                                $nwData['AccountPaid'] = intval(floatval($data->balance) * 100) - intval(floatval($data->original_balance) * 100);
                                            }
                                        }

                                        $nwData['PointsEarned'] = 0;
                                        $nwData['PointsRedeemed'] = 0;
                                        if ($data->original_points !== $data->points) {
                                            if (floatval($data->original_points) > floatval($data->points)) {
                                                $nwData['PointsRedeemed'] = intval(floatval($data->original_points) * 100) - intval(floatval($data->points) * 100);
                                            } else {
                                                $nwData['PointsEarned'] = intval(floatval($data->points) * 100) - intval(floatval($data->original_points) * 100);
                                            }
                                        }

                                        $nwData['JoiningFee'] = 0;
                                        $nwData['RenewalFee'] = 0;

                                        $temp = $bepoz->BalanceUpdate($nwData);

                                        if ($temp) {
                                            // Log

                                            if (!is_null($bepoz_job->failed_job_uid)) {
                                                $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                if (!is_null($failed_job)) {
                                                    $failed_job->delete();
                                                }
                                            }

                                            $bepoz_job->dispatch();
                                        } else {
                                            $log = new SystemLog();
                                            $log->type = 'bepoz-job-error';
                                            $log->humanized_message = 'Updating balance is failed. Please check the error message';
                                            $log->payload = $bepoz_job->payload;
                                            $log->message = $temp;
                                            $log->source = 'UpdateBalance.php';
                                            $log->save();

                                            $bepoz_job->free();
                                        }
                                    }
                                    else {
                                        $bepoz_job->free();
                                    }
                                }
                                else {
                                    $bepoz_job->free();
                                }

                            } catch (\Exception $e) {
                                $log = new SystemLog();
                                $log->humanized_message = 'Updating balance is failed. Please check the error message';
                                $log->type = 'bepoz-job-error';
                                $log->payload = $bepoz_job->payload;
                                $log->message = $e;
                                $log->source = 'UpdateBalance.php';
                                $log->save();

                                $bepoz_job->free();
                            }
                        } else {

                            if (is_null($bepoz_job->failed_job_uid)) {

                                $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                    ->where('queue', $bepoz_job->queue)
                                    ->first();

                                if (is_null($failed_job)) {
                                    $failed_job = new BepozFailedJob;
                                    $failed_job->queue = $bepoz_job->queue;
                                    $failed_job->payload = $bepoz_job->payload;
                                    $failed_job->job_uid = Uuid::generate(4);
                                    $failed_job->save();
                                }
                            }

                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->payload = $bepoz_job->payload;
                            $log->humanized_message = 'Maximum attempt of updating balance has been reached.';
                            $log->source = 'UpdateBalance.php';
                            $log->save();

                            $bepoz_job->dispatch();
                        }
                    }
                }
                */
            }


        } catch (\Exception $e) {
            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Updating balance is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'UpdateBalance.php';
            $log->save();

        }
    }
}
