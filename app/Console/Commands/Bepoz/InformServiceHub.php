<?php

namespace App\Console\Commands\Bepoz;


use App\Helpers\Bepoz;
use App\Http\Controllers\AuthenticationController;
use App\Setting;
use App\SystemLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class InformServiceHub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inform-service-hub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send settings to hub';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            
            if ( !is_null(env('APP_URL')) && (env('APP_URL') != "") ) {
                // Log::warning("InformServiceHub Merchant App");

                $company_name = Setting::where('key', 'company_name')->first()->value;
                $merchant_app_hub_url = Setting::where('key', 'merchant_app_hub_url')->first()->value;
                $myplace_backpanel_url = Setting::where('key', 'myplace_backpanel_url')->first()->value;
                $bepoz_status = Setting::where('key', 'bepoz_status')->first()->value;
                $checkBepozStatus = $bepoz->Status();
                $getXMLHeader = $bepoz->getXMLHeader();
                $getHttpCode = $bepoz->getHttpCode();
                $versionGet = $bepoz->VersionGet();

                $onlyVersion = app('App\Http\Controllers\AuthenticationController')->onlyVersion();

                // Log::info($company_name);
                // Log::info($checkBepozStatus);
                // Log::info($getXMLHeader);
                // Log::info($getXMLHeader['Data1']);
                // Log::info($getHttpCode);
                // Log::info($onlyVersion);
                // Log::info($versionGet);
                
                //SEND MERCHANT APP
                $payload = array(
                    'token' => md5(env('APP_URL')),
                    // 'claims' => "",
                    'name' => $company_name,
                    'endpoint' => env('APP_URL'),
                    'bepoz_version' => $versionGet['Data1'],
                    'dll_version' => $getXMLHeader['Data1'],
                    'be_version' => $onlyVersion,
                    'bp_link' => $myplace_backpanel_url,
                    'bepoz_status' => $bepoz_status
                );

                // Log::info($payload);
                // Log::info($merchant_app_hub_url . "/mp");

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $merchant_app_hub_url . "/mp");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                $content = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                // Log::warning($content);
                // Log::warning($httpCode);

                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);

                    if (isset($payload->status) && isset($payload->error)) {

                    }

                } else {

                }
            }

        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'service-hub-connection-error';
            $log->humanized_message = 'Send Service Hub is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'InformServiceHub.php';
            $log->save();

        }

    }

}
