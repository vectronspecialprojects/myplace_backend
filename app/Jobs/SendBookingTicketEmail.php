<?php

namespace App\Jobs;

use App\Email;
use App\GiftCertificate;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Listing;
use App\Member;
use App\Order;
use App\OrderDetail;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SystemLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Milon\Barcode\DNS1D;

class SendBookingTicketEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $order;

    /**
     * Create a new job instance.
     *
     * SendBookingTicketEmail constructor.
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();
                
            $user = $this->user;
            $order_sent = $this->order;

            $order = Order::with(['order_details_ticket_success', 'order_details_credit_voucher_success'])
                ->where('id', $order_sent->id)->first();

            $company_name = Setting::where('key', 'company_name')->first()->value;
            $platform = Platform::where('name', 'web')->first();

            $url = config('bucket.APP_URL') . '/api/receiptBooking/' . $order->token . '?app_token=' . $platform->app_token;

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;
            
            $data = array(
                'FIRST_NAME' => $user->member->first_name,
                'LAST_NAME' => $user->member->last_name,
                'PURCHASE_DATE' => date('F d, Y', strtotime($order->created_at)),
                'CURRENT_YEAR' => date("Y"),
                'COMPANY' => $company_name,
                'EXPIRY_DATE' => Carbon::now(config('app.timezone'))->addDays(90)->format('F d, Y'),
                'VENUE_NAME' => $company_name,
                'VENUE_CONTACT' => $company_name,
                'PURCHASER_NAME' => $user->member->first_name . ' ' . $user->member->last_name,
                'RECEIPT_ID' => (string) $order->id,
                'URL' => $url,
                'VOUCHER_TYPE' => 'VOUCHER'
            );

            if (intval($order->listing_id) > 0) {
                $listing = Listing::find($order->listing_id);
                if (!is_null($listing)) {
                    if ($listing->listing_type_id === 1) {
                        $data['TICKET_URL'] = config('bucket.APP_URL') . '/api/ticketBooking/' . $order->token . '?app_token=' . $platform->app_token;
                        $data['TRANSACTION_ID'] = (string) $order->id;
                        $data['EVENT_NAME'] = $listing->heading;
                        $data['VOUCHER_TYPE'] = 'TICKET';
                        
                        $qty_ticket = OrderDetail::where('order_id', $order->id)->where('product_type_id', '2')->sum('qty');
                        $data['QTY'] = $qty_ticket;
                        
                        $qty_credit_voucher = OrderDetail::where('order_id', $order->id)->where('product_type_id', '3')->sum('qty');
                                                
                        //if order has F&B voucher 
                        //if (!is_null($order->order_details_credit_voucher_success)) {
                        //if ( $order->order_details_credit_voucher ) {
                        if ( $qty_credit_voucher > 0 ) {
                                                    
                            $data['VOUCHER_URL'] = config('bucket.APP_URL') . '/api/giftCertificateBooking/' . 
                                $order->token . '?app_token=' . $platform->app_token;
                            
                            //$member = Member::find($gf->purchaser_id);
                            //$payload = \GuzzleHttp\json_decode($gf->payload);
                            //$vs = VoucherSetups::find($payload->voucher_setups_id);
                            
                            $gf = GiftCertificate::where('token', $order->token)->first();
                            
                            $credit_vouchers = OrderDetail::where('order_id', $order->id)->where('product_type_id', '3')->get();
                            
                            $data['VOUCHER_VALUE'] = "";
                            foreach ($credit_vouchers as $key=>$credit_voucher) {
                                $data['VOUCHER_VALUE'] .= "$".$credit_voucher->unit_price;
                                if ( $qty_credit_voucher > 1 && $key < $qty_credit_voucher-1 ) {
                                    $data['VOUCHER_VALUE'] .= ", ";
                                }
                            }
                            //$data['TRANSACTION_ID'] = $order->id;
                            if($gf){
                                $data['VOUCHER_EXPIRE_DATE'] = $gf->expiry_date;
                            }

                            $data['QTY_CREDIT'] = $qty_credit_voucher;
                        }
                        
                    }
                }
            }

            $result = "";

            if ($email_server === "mandrill") {
                            
                if ($mx->init()) {
                    $mx->send('ticket_booking_backend', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'receipt',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $result = $mjx->send('ticket_booking_backend', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'receipt',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            DB::commit();
            
            return $result;

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending booking ticket and receipt is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Jobs/SendBookingTicketEmail.php';
            $log->save();
        }

    }
}
