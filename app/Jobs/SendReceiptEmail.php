<?php

namespace App\Jobs;

use App\Email;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Listing;
use App\Member;
use App\Order;
use App\OrderDetail;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SystemLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Milon\Barcode\DNS1D;

class SendReceiptEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $order;

    /**
     * Create a new job instance.
     *
     * SendReceiptEmail constructor.
     * @param User $user
     * @param Order $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();

            $user = $this->user;
            $order = $this->order;

            $company_name = Setting::where('key', 'company_name')->first()->value;
            $platform = Platform::where('name', 'web')->first();

            $url = config('bucket.APP_URL') . '/api/receipt/' . $order->token . '?app_token=' . $platform->app_token;

            $data = array(
                'FIRST_NAME' => $user->member->first_name,
                'LAST_NAME' => $user->member->last_name,
                'PURCHASE_DATE' => date('F d, Y', strtotime($order->created_at)),
                'CURRENT_YEAR' => date("Y"),
                'COMPANY' => $company_name,
                'EXPIRY_DATE' => Carbon::now(config('app.timezone'))->addDays(90),
                'VENUE_NAME' => $company_name,
                'PURCHASER_NAME' => $user->member->first_name . ' ' . $user->member->last_name,
                'RECEIPT_ID' => $order->id,
                'URL' => $url,
                'VOUCHER_TYPE' => 'VOUCHER'
            );

            if (intval($order->listing_id) > 0) {
                $listing = Listing::find($order->listing_id);
                if (!is_null($listing)) {
                    if ($listing->listing_type_id === 1) {
                        $data['TICKET_URL'] = config('bucket.APP_URL') . '/api/ticket/' . $order->token . '?app_token=' . $platform->app_token;
                        $data['TRANSACTION_ID'] = $order->id;
                        $data['EVENT_NAME'] = $listing->heading;
                        $data['VOUCHER_TYPE'] = 'TICKET';

                        $qty = 0;
                        foreach ($order->order_details as $order_detail) {
                            $qty += intval($order_detail->qty);
                        }

                        $data['QTY'] = $qty;
                    }
                }
            }

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('receipt', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'receipt',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('receipt', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'receipt',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }


            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending receipt email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendReceiptEmail.php';
            $log->save();
        }

    }
}
