<?php

namespace App\Jobs;

use App\Email;
use App\FriendReferral;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Jobs\Job;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SystemLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SendReminderTicketEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $ticket;
    protected $interval;
    protected $event_date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, String $ticket, String $interval, String $event_date)
    {
        $this->user = $user;
        $this->ticket = $ticket;
        $this->interval = $interval;
        $this->event_date = $event_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();

            $user = $this->user;
            $ticket = $this->ticket;
            $interval = $this->interval;
            $event_date = $this->event_date;
            //$date_now = Carbon::now();

            $company_name = Setting::where('key', 'company_name')->first()->value;

            $platform = Platform::where('name', 'web')->first();
            
            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;
            
            $data = array(
                'NAME' => $user->member->first_name.' '.$user->member->last_name,
                'TICKET' => $ticket,
                'INTERVAL' => $interval,
                'DATE' => (string) $event_date
            );
                        
            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('reminder_ticket', $user->member->first_name.' '.$user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $friend->email,
                        'category' => 'reminder_ticket',
                        'name' => $friend->name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('reminder_ticket', $user->member->first_name.' '.$user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $friend->email,
                        'category' => 'reminder_ticket',
                        'name' => $friend->name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending reminder ticket email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendReminderTicketEmail.php';
            $log->save();
        }

    }
}
