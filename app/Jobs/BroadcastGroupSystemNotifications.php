<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Member;
use App\MemberSystemNotification;
use App\MemberTiers;
use App\Setting;
use App\SystemLog;
use App\SystemNotification;
use App\Tier;
use App\Venue;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
// use jmrieger\OneSignal\OneSignalFacade;
// use jmrieger\OneSignal\OneSignalClient;
use OneSignal;
use berkayk\OneSignal\OneSignalFacade;
use berkayk\OneSignal\OneSignalClient;
use App\Jobs\SendOneSignalNotification;

class BroadcastGroupSystemNotifications extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $notificationID;
    protected $tiersID;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tiersID, $notificationID)
    {
        $this->tiersID = $tiersID;
        $this->notificationID = $notificationID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $company_name = Setting::where('key', 'company_name')->first()->value;

            $sn = SystemNotification::find($this->notificationID);
            
            $today = Carbon::now(config('app.timezone'));

            //get all member id from tier
            // $tiermembers = MemberTiers::with('member.user')
            //     ->whereIn('tier_id', $this->tiersID)
            //     // ->where('date_expiry', '>=', $today)
            //     ->chunk(100, function ($tiermembers) use ( $sn, $company_name ) {
                    
            //         $tags = [];                    
            //         $system_send_all = array();
            //         $now = Carbon::now(config('app.timezone'));

            //         foreach ($tiermembers as $tiermember) {

            //             if ( !is_null($tiermember->member) ){
            //                 //Log::info($tiermember->id);
            //                 //get all member have tier
            //                 $member = $tiermember->member;
            //                 $user = $member->user;

            //                 $tags[] = ["key" => "email", "relation" => "=", "value" => $user->email];
            //                 $tags[] = ["operator" => "OR"];

            //                 $system_send = array(
            //                     'member_id' => $tiermember->member->id,
            //                     'system_notification_id' => $sn->id,
            //                     'status' => 'unread',
            //                     'sent_at' => $now
            //                 );
            //                 array_push($system_send_all, $system_send);

            //             }

            //         }

            //         // INSERT SYSTEM NOTIFICATION TO LOCAL DATABASE
            //         MemberSystemNotification::insert($system_send_all);
                    
            //         // REMOVE LAST OR
            //         array_pop($tags);
            //         $dataNotif = [
            //             "tags" => $tags,
            //             "contents" => ["en" => $sn->message],
            //             "headings" => ["en" => $company_name],
            //             "data" => ["category" => "member"],
            //             "ios_badgeType" => "Increase",
            //             "ios_badgeCount" => 1
            //         ];
            //         // Log::info($data);

            //         // SEND API REQUEST TO ONESIGNAL
            //         // OneSignalFacade::postNotification($data);
            //         // dispatch(new SendOneSignalNotification($dataNotif));

            // });

            // Log::info("BroadcastGroupSystemNotifications START");
            // Log::info(Carbon::now(config('app.timezone')));

            // $tiermembers = MemberTiers::with('member.user')
            //     ->whereIn('tier_id', $this->tiersID)
            //     // ->where('date_expiry', '>=', $today)
            //     ->chunk(2000, function ($tiermembers) use ( $sn, $company_name ) {
                    
            //         $system_send_all = array();
            //         $now = Carbon::now(config('app.timezone'));

            //         foreach ($tiermembers as $tiermember) {

            //             if ( !is_null($tiermember->member) ){
            //                 //Log::info($tiermember->id);
            //                 //get all member have tier
            //                 $member = $tiermember->member;
                            
            //                 if (!empty($member->onesignal)) {
            //                     $player_ids[] = $member->onesignal;
            //                 }
                            
            //                 $system_send = array(
            //                     'member_id' => $tiermember->member->id,
            //                     'system_notification_id' => $sn->id,
            //                     'status' => 'unread',
            //                     'sent_at' => $now
            //                 );
            //                 array_push($system_send_all, $system_send);

            //             }

            //         }

            //         // INSERT SYSTEM NOTIFICATION TO LOCAL DATABASE
            //         MemberSystemNotification::insert($system_send_all);
                    
            //         // SEND API REQUEST TO ONESIGNAL
            //         $dataNotif = [
            //             "contents" => ["en" => $sn->message],
            //             "headings" => ["en" => $company_name],
            //             "data" => ["category" => "member"],
            //             "include_player_ids" => $player_ids,
            //             "ios_badgeType" => "Increase",
            //             "ios_badgeCount" => 1
            //         ];
            //         Log::info($dataNotif);

            //         // $job = (new SendOneSignalNotification($dataNotif))->delay(150);
            //         // dispatch($job);
            // });

            // Log::info("BroadcastGroupSystemNotifications END");
            // Log::info(Carbon::now(config('app.timezone')));

            //get all member id from tier
            $tiermembers = MemberTiers::with('member.user')
                ->whereIn('tier_id', $this->tiersID)
                // ->where('date_expiry', '>=', $today)
                ->chunk(100, function ($tiermembers) use ( $sn, $company_name ) {

                    $system_send_all = array();
                    $now = Carbon::now(config('app.timezone'));

                    foreach ($tiermembers as $tiermember) {

                        if ( !is_null($tiermember->member) ){
                            //Log::info($tiermember->id);
                            //get all member have tier
                            $member = $tiermember->member;
                            $user = $member->user;

                            $system_send = array(
                                'member_id' => $tiermember->member->id,
                                'system_notification_id' => $sn->id,
                                'status' => 'unread',
                                'sent_at' => $now
                            );
                            array_push($system_send_all, $system_send);

                        }

                    }

                    // INSERT SYSTEM NOTIFICATION TO LOCAL DATABASE
                    MemberSystemNotification::insert($system_send_all);

            });

            $tiers = Tier::whereIn('id', $this->tiersID)->get();

            $tags = [];
            foreach ($tiers as $tier) {
                if ( !is_null($tier) ){
                    // Log::info($tier->id);                    
                    $tags[] = ["key" => "tier_id", "relation" => "=", "value" => $tier->id];
                    $tags[] = ["operator" => "OR"];
                    $tags[] = ["key" => "tier_name", "relation" => "=", "value" => $tier->name];
                    $tags[] = ["operator" => "OR"];
                }
            }

            // REMOVE LAST OR
            array_pop($tags);
            $dataNotif = [
                "tags" => $tags,
                "contents" => ["en" => $sn->message],
                "headings" => ["en" => $company_name],
                "data" => ["category" => "member"],
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            ];
            // Log::info($dataNotif);

            // SEND API REQUEST TO ONESIGNAL
            dispatch(new SendOneSignalNotification($dataNotif));

        } catch (\Exception $e) {

            $log = new SystemLog();
            $log->type = 'broadcast-system-notifications-error';
            $log->humanized_message = 'Something wrong. Cannot send push notifications to user.';
            $log->message = $e;
            $log->source = 'BroadcastSystemNotifications.php';
            $log->save();
        }
    }
}
