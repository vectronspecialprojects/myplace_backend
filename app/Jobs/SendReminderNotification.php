<?php

namespace App\Jobs;

use App\Jobs\SendOneSignalNotification;
use App\Setting;
use App\SystemLog;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
// use jmrieger\OneSignal\OneSignalFacade;
use OneSignal;
use berkayk\OneSignal\OneSignalFacade;

class SendReminderNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;

        try {
            $company_name = Setting::where('key', 'company_name')->first()->value;
            $contents = "Please check your email to validate your account before use.";

            // OneSignalFacade::postNotification([
            //     "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
            //     "contents" => ["en" => "Please check your email to validate your account before use."],
            //     "headings" => ["en" => $company_name],
            //     "data" => ["category" => "email"],
            //     "ios_badgeType" => "Increase",
            //     "ios_badgeCount" => 1
            // ]);
            
            $temp = [
                "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
                "contents" => ["en" => $contents],
                "headings" => ["en" => $company_name],
                "data" => ["category" => "email"],
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            ];

            dispatch(new SendOneSignalNotification($temp));

        } catch (\Exception $e) {
            $log = new SystemLog();
            $log->humanized_message = 'Failed sending push notification. Please check the error message';
            $log->type = 'onesignal-error';
            $log->message = $e;
            $log->source = 'SendReminderNotification.php';
            $log->save();
        }
    }
}
