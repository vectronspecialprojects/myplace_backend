<?php

namespace App\Jobs;

use App\SystemLog;
use App\Setting;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
// use jmrieger\OneSignal\OneSignalFacade;
// use jmrieger\OneSignal\OneSignalClient;
use OneSignal;
use berkayk\OneSignal\OneSignalFacade;
use berkayk\OneSignal\OneSignalClient;
use Illuminate\Support\Facades\Log;

class SendOneSignalNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $result = OneSignal::sendNotificationCustom($this->data);
            // Log::info(print_r($result, true));
        } catch (\Exception $e) {
            $log = new SystemLog();
            $log->type = 'one-signal-error';
            $log->humanized_message = 'Something wrong. Cannot send push notifications to user.';
            $log->message = $e;
            $log->source = 'SendOneSignalNotification.php';
            $log->save();
        }

    }
}
