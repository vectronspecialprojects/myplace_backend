<?php

namespace App\Jobs;

use App\ContactUs;
use App\Email;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Jobs\Job;
use App\PendingJob;
use App\Setting;
use App\SystemLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendContactUsEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $contact_us;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, ContactUs $contact_us)
    {
        $this->user = $user;
        $this->contact_us = $contact_us;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();

            $user = $this->user;
            $contact_us = $this->contact_us;
            $company_name = Setting::where('key', 'company_name')->first()->value;

            // send to member

            $data = array(
                'LAST_NAME' => $user->member->last_name,
                'FIRST_NAME' => $user->member->first_name,
            );

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('contactus', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'contactus',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('contactus', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'contactus',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            // send to admin
            $admin_email = Setting::where('key', 'admin_email')->first()->extended_value;
            $admin_name = Setting::where('key', 'admin_full_name')->first()->value;

            if (!is_null($admin_email)) {
                $emails = \GuzzleHttp\json_decode($admin_email, true);
                if (!empty($emails)) {
                    $created_at = Carbon::parse($contact_us->created_at);

                    $data = array(
                        'FIRST_NAME' => $user->member->first_name,
                        'LAST_NAME' => $user->member->last_name,
                        'CURRENT_YEAR' => date("Y"),
                        'COMPANY' => $company_name,
                        'ADMIN_NAME' => $admin_name,
                        'EMAIL' => $user->email,
                        'PHONE' => $user->mobile,
                        'METHOD' => $contact_us->method,
                        'MESSAGE' => $contact_us->message,
                        'TIME_OF_CALL' => is_null($contact_us->time_of_call) ? "No time specified" : $contact_us->time_of_call,
                        'CREATED_AT' => $created_at->format('l jS \\of F Y h:i:s A')
                    );

                    foreach ($emails as $email) {
                        if ($email_server === "mandrill") {

                            if ($mx->init()) {
                                $mx->send('contactus_admin', $admin_name, $email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $email,
                                    'category' => 'contactus_admin',
                                    'name' => $admin_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }

                        } else if ($email_server === "mailjet") {
                            
                            if ($mjx->init()) {
                                $mjx->send('contactus_admin', $admin_name, $email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $email,
                                    'category' => 'contactus_admin',
                                    'name' => $admin_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }
            
                        }
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending contact us email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendContactUsEmail.php';
            $log->save();
        }

    }
}
