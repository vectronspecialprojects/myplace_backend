<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Member;
use App\MemberTiers;
use App\MemberSystemNotification;
use App\Setting;
use App\SystemLog;
use App\SystemNotification;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
// use jmrieger\OneSignal\OneSignalFacade;
// use jmrieger\OneSignal\OneSignalClient;
use OneSignal;
use berkayk\OneSignal\OneSignalFacade;
use berkayk\OneSignal\OneSignalClient;
use App\Jobs\SendOneSignalNotification;

class BroadcastSystemNotifications extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $notificationID;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notificationID)
    {
        $this->notificationID = $notificationID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            // $members = Member::all();
            $company_name = Setting::where('key', 'company_name')->first()->value;

            $sn = SystemNotification::find($this->notificationID);

            //get all member
            // $members = Member::chunk(100, function ($members) use ( $sn, $company_name ) {
            //     $tags = [];                    
            //     $system_send_all = array();
            //     $now = Carbon::now(config('app.timezone'));

            //     foreach ($members as $member) {

            //         if ( !is_null($member) ){
            //             //Log::info($tiermember->id);
            //             $user = $member->user;

            //             $tags[] = ["key" => "email", "relation" => "=", "value" => $user->email];
            //             $tags[] = ["operator" => "OR"];

            //             $system_send = array(
            //                 'member_id' => $member->id,
            //                 'system_notification_id' => $sn->id,
            //                 'status' => 'unread',
            //                 'sent_at' => $now
            //             );
            //             array_push($system_send_all, $system_send);

            //         }

            //     }

            //     // INSERT SYSTEM NOTIFICATION TO LOCAL DATABASE
            //     MemberSystemNotification::insert($system_send_all);
                
            //     // REMOVE LAST OR
            //     array_pop($tags);
            //     $dataNotif = [
            //         "tags" => $tags,
            //         "contents" => ["en" => $sn->message],
            //         "headings" => ["en" => $company_name],
            //         "data" => ["category" => "member"],
            //         "ios_badgeType" => "Increase",
            //         "ios_badgeCount" => 1
            //     ];
            //     // Log::info($dataNotif);

            //     // SEND API REQUEST TO ONESIGNAL
            //     // dispatch(new SendOneSignalNotification($dataNotif));

            // });

            // Log::info("BroadcastSystemNotifications START");
            // Log::info(Carbon::now(config('app.timezone')));

            // //get all member
            // $members = Member::chunk(2000, function ($members) use ( $sn, $company_name ) {
            //     $player_ids = [];                    
            //     $system_send_all = array();
            //     $now = Carbon::now(config('app.timezone'));

            //     foreach ($members as $member) {

            //         if ( !is_null($member) ){
            //             //Log::info($member->id);
            //             if ( !is_null($member) ){
            //                 if (!empty($member->onesignal)) {
            //                     $player_ids[] = $member->onesignal;
            //                 }
            //             }

            //             $system_send = array(
            //                 'member_id' => $member->id,
            //                 'system_notification_id' => $sn->id,
            //                 'status' => 'unread',
            //                 'sent_at' => $now
            //             );
            //             array_push($system_send_all, $system_send);

            //         }

            //     }

            //     // INSERT SYSTEM NOTIFICATION TO LOCAL DATABASE
            //     MemberSystemNotification::insert($system_send_all);
                
            //     // SEND API REQUEST TO ONESIGNAL
            //     $dataNotif = [
            //         "contents" => ["en" => $sn->message],
            //         "headings" => ["en" => $company_name],
            //         "data" => ["category" => "member"],
            //         "include_player_ids" => $player_ids,
            //         "ios_badgeType" => "Increase",
            //         "ios_badgeCount" => 1
            //     ];
            //     Log::info($dataNotif);

            //     // $job = (new SendOneSignalNotification($dataNotif))->delay(150);
            //     // dispatch($job);

            // });
            
            // Log::info("BroadcastSystemNotifications END");
            // Log::info(Carbon::now(config('app.timezone')));

            //get all member id from tier
            $members = Member::chunk(100, function ($members) use ( $sn, $company_name ) {

                $system_send_all = array();
                $now = Carbon::now(config('app.timezone'));

                foreach ($members as $member) {

                    if ( !is_null($member) ){
                        $system_send = array(
                            'member_id' => $member->id,
                            'system_notification_id' => $sn->id,
                            'status' => 'unread',
                            'sent_at' => $now
                        );
                        array_push($system_send_all, $system_send);
                    }
                }
                // Log::info($system_send_all);
                // INSERT SYSTEM NOTIFICATION TO LOCAL DATABASE
                MemberSystemNotification::insert($system_send_all);

            });

            $tiers = Tier::all();

            $tags = [];
            foreach ($tiers as $tier) {
                if ( !is_null($tier) ){
                    // Log::info($tier->id);                    
                    $tags[] = ["key" => "tier_id", "relation" => "=", "value" => $tier->id];
                    $tags[] = ["operator" => "OR"];
                    $tags[] = ["key" => "tier_name", "relation" => "=", "value" => $tier->name];
                    $tags[] = ["operator" => "OR"];
                }
            }
            // Log::info($tiers);

            // REMOVE LAST OR
            array_pop($tags);
            $dataNotif = [
                "tags" => $tags,
                "contents" => ["en" => $sn->message],
                "headings" => ["en" => $company_name],
                "data" => ["category" => "member"],
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            ];
            // Log::info($dataNotif);
            
            // SEND API REQUEST TO ONESIGNAL
            dispatch(new SendOneSignalNotification($dataNotif));

        } catch (\Exception $e) {

            $log = new SystemLog();
            $log->type = 'broadcast-system-notifications-error';
            $log->humanized_message = 'Something wrong. Cannot send push notifications to user.';
            $log->message = $e;
            $log->source = 'BroadcastSystemNotifications.php';
            $log->save();
        }
    }
}
