<?php

namespace App\Helpers;

use App\Helpers\Contracts\PondHoppersContract;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class PondHoppers implements PondHoppersContract
{
    // variable for REST api client
    private $url;
    private $uniqueKey;
    private $ch;

    private $headers;

    /**
     * PondHoppers constructor.
     */
    public function __construct()
    {
        $this->url = Setting::where('key', 'pond_hoppers_url')->first()->value;
        $this->uniqueKey = Setting::where('key', 'pond_hoppers_unique_key')->first()->value;
        
        $this->headers = array(
            "Content-type" => "text/plain",
        );

        $this->ch = curl_init();
    }

    public function POINTS($memberNumber, $venueId)
    {
        $action = "POINTS";

        $bodystring = '{
            "memberNumber": "'.$memberNumber.'",
            "uniqueKey": "'.$this->uniqueKey.'",
            "venueId": "'.$venueId.'",
            "action": "'.$action.'"
        }';

        return $this->callAPI($this->headers, $bodystring);
    }

    public function PLEDGE($memberNumber, $venueId)
    {
        $action = "PLEDGE";

        $bodystring = '{
            "memberNumber": "'.$memberNumber.'",
            "uniqueKey": "'.$this->uniqueKey.'",
            "venueId": "'.$venueId.'",
            "action": "'.$action.'"
        }';

        return $this->callAPI($this->headers, $bodystring);
    }

    public function CONTACT($memberNumber, $venueId)
    {
        $action = "CONTACT";

        $bodystring = '{
            "memberNumber": "'.$memberNumber.'",
            "uniqueKey": "'.$this->uniqueKey.'",
            "venueId": "'.$venueId.'",
            "action": "'.$action.'"
        }';

        return $this->callAPI($this->headers, $bodystring);
    }

    protected function callAPI($headers, $body)
    {
        $url = $this->url;

        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("PondHoppers");
        // Log::info($this->url);
        // Log::info($httpCode);
        // Log::info($content);

        if ($httpCode >= 200 && $httpCode < 300) {
            if ($content) {
                $result = $content;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }

        return $result;
    }


}
