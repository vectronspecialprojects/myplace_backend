<?php

namespace App\Helpers;

use App\Helpers\Contracts\PayWayContract;
use App\Setting;

class PayWay implements PayWayContract
{
    // variable for REST api client
    private $publishableKey;
    private $secretKey;
    private $url;
    private $merchantId;

    /**
     * Bepoz constructor.
     */
    public function __construct()
    {
        $this->publishableKey = env('PAYWAY_PUBLISHABLE');
        $this->secretKey = env('PAYWAY_SECRET');
        $this->url = env('PAYWAY_URL');
        $this->merchantId = env('PAYWAY_MERCHANT_ID');
    }

    public function checkNetwork()
    {
        $url = "";
        return $this->callGetAPI($url);
    }

    public function createSingleUseToken($cardNumber, $expiryDateMonth, $expiryDateYear, $cvn, $cardholderName)
    {
        $data = array(
            "paymentMethod" => "creditCard",
            "cardNumber" => $cardNumber,
            "expiryDateMonth" => $expiryDateMonth,
            "expiryDateYear" => $expiryDateYear,
            "cvn" => $cvn,
            "cardholderName" => $cardholderName
        );

        $key = "publishable";
        $url = "/single-use-tokens";
        return $this->callPostAPI($url, $data, $key, null);
    }

    public function createCustomer($token)
    {
        $data = array(
            "singleUseTokenId" => $token,
            "merchantId" => $this->merchantId
        );

        $key = "secret";
        $url = "/customers";
        return $this->callPostAPI($url, $data, $key, null);
    }

    public function transaction($uuid, $customerNumber, $amount, $orderNumber)
    {
        $data = array(
            "customerNumber" => $customerNumber,
            "transactionType" => "payment",
            "principalAmount" => $amount,
            "currency" => "aud",
            "merchantId" => $this->merchantId
        );

        if (isset($orderNumber)) {
            $data['orderNumber'] = $orderNumber;
        }

        $key = "secret";
        $url = "/transactions";
        return $this->callPostAPI($url, $data, $key, $uuid);

    }

    protected function callGetAPI($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); // basic authetication
        curl_setopt($ch, CURLOPT_USERNAME, $this->publishableKey);
        curl_setopt($ch, CURLOPT_URL, $this->url . $url);
        $content = curl_exec($ch);

        return $content;
    }

    protected function callPostAPI($url, $data, $key, $uuid)
    {
        $header = array(
            'Accept: application/json'
        );

        if (!is_null($uuid)) {
            $header[] = 'Idempotency-Key: ' . $uuid;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); // basic authetication
        curl_setopt($ch, CURLOPT_USERNAME, $key == "secret" ? $this->secretKey : $this->publishableKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->url . $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $content = curl_exec($ch);

        return $content;
    }

}