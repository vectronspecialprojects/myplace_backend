<?php

namespace App\Helpers;

use App\BepozLog;
use App\Helpers\Contracts\BepozContract;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use LSS\Array2XML;
use LSS\XML2Array;

class Mailjet
{
    // variable for REST api client
    private $apikey;
    private $secretkey;
    private $url;
    private $training;
    private $operator_id;
    private $till_id;
    private $ch;

    /**
     * Bepoz constructor.
     */
    public function __construct()
    {
        Array2XML::init('1.0', 'UTF-8');
        
        $this->apikey = Setting::select('value')->where('key', 'mailjet_api_key')->first()->value;
        $this->secretkey = Setting::select('value')->where('key', 'mailjet_secret_key')->first()->value;
        //$this->url = "https://api.mailjet.com/v3/REST/";
        //$this->url = "https://api.mailjet.com/v3.1/REST/";
        $this->url = "https://api.mailjet.com/";

        $this->ch = curl_init();
    }
    
    public function key()
    {
        return "APIKEY : ".$this->apikey. " SECRET KEY : ".$this->secretkey;
    }

    public function user()
    {
        $urlComplete = $this->url . "v3/REST/user";

        return $this->callAPI($urlComplete, "", "");
    }

    public function template()
    {
        $urlComplete = $this->url . "v3/REST/template";

        return $this->callAPI($urlComplete, "", "");
    }


    public function send( $data )
    {
        $urlComplete = $this->url . "v3.1/send";

        return $this->callAPI($urlComplete, "POST", $data);
    }

    protected function callAPI( $urlComplete, $method, $data )
    {
        $ch = $this->ch;
        
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array($this->apikey. ' : ' . $this->secretkey));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->apikey.":".$this->secretkey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $urlComplete );
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        //curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        if ( $method === "POST"){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data );
        }

        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result = $content;
        curl_close($ch);
        
        return $result;
    }
    
}