<?php

function toMoneyConverter($val, $symbol = '$', $r = 2)
{

    $n = $val;
    $sign = ($n < 0) ? '-' : '';
    $i = $n = number_format(abs($n), $r);
    $j = (($j = strlen($i)) > 3) ? $j % 3 : 0;

    return $symbol . $sign . ($j ? substr($i, 0, $j) : '') . preg_replace('/(\d{3})(?=\d)/', "$1" . ',', substr($i, $j));

}

function toMoneyConverterBooking($val, $symbol = '$')
{
    return $symbol . number_format( (float) $val, 2, '.', '' );
}

function calculateGST($total)
{
    return floatval($total) / 11;
}

function getSocialLoginTypes($types)
{
    if ($types->isEmpty()) {
        return 'app';
    } else {
        return implode(", ", $types->pluck('type')->toArray());
    }
}