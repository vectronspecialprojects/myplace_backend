<?php

namespace App\Helpers\Contracts;

Interface SMSContract
{
    public function sendSMS($number, $message);
}