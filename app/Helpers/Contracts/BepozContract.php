<?php

namespace App\Helpers\Contracts;

Interface BepozContract
{
    public function SystemCheck();

    public function AccountSearch($searchField, $data);

    public function AccountFullGet($accountID);

    public function AccountGet($data);

    public function AccCreateUpdate($accountID, $firstname, $lastname, $email, $accnumber, $cardnumber, $groupID, $template);

    public function AccUpdate($AccountFull);

    public function VoucherSetupGet($id);

    public function VoucherGet($id, $lookup);

    public function VoucherIssue($voucherSetupId, $bepoz_account_id, $lookup);

    public function ProductsSearch($data1, $data2, $data3, $data4, $data5, $data6);

    public function ProductGet($id);

    public function ProdPriceGet($id);

    public function TransactionCreate($data);
}