<?php

namespace App\Helpers\Contracts;

Interface MailjetExpressContract
{
    public function init();

    public function send($category, $name, $email, $data);
}