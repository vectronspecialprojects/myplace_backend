<?php

namespace App\Helpers\Contracts;

Interface MandrillExpressContract
{
    public function init();

    public function send($category, $name, $email, $data);
}