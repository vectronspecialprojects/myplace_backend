<?php

namespace App\Helpers\Contracts;

Interface PondHoppersContract
{
    public function POINTS($memberNumber, $venueId);
    public function PLEDGE($memberNumber, $venueId);
    public function CONTACT($memberNumber, $venueId);
}
