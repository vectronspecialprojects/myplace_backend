<?php

namespace App\Helpers;

use App\GamingLog;
use App\GamingSession;
use App\Helpers\Contracts\MetropolisARCOdysseyContract;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class MetropolisARCOdyssey implements MetropolisARCOdysseyContract
{
    // variable for REST api client
    private $key;
    private $url;
    private $training;
    private $operator_id;
    private $till_id;
    private $ch;

    private $headers;
    private $fromSystem;
    private $toSystem;
    private $messageId;
    private $dateTimeSent;
    private $commandClass;
    private $vendorCode;
    private $deviceCode;
    private $sharedKey;
    private $fieldId;


    /**
     * MetropolisARCOdyssey constructor.
     */
    public function __construct()
    {
        $this->url = Setting::where('key', 'odyssey_url')->first()->value;

        $this->headers = array(
            "Content-type" => "application/json",
        );

        $this->fromSystem = Setting::where('key', 'odyssey_from_system')->first()->value;
        $this->toSystem = Setting::where('key', 'odyssey_to_system')->first()->value;
        $this->messageId = Setting::where('key', 'odyssey_message_id')->first()->value;
        $this->dateTimeSent = Carbon::now()->format('Y-m-d\TH:i:s');
//        $this->dateTimeSent = Setting::where('key', 'odyssey_datetime_sent')->first()->value;

        $this->commandClass = Setting::where('key', 'odyssey_command_class')->first()->value;
        $this->vendorCode = Setting::where('key', 'odyssey_vendor_code')->first()->value;
        $this->deviceCode = Setting::where('key', 'odyssey_device_code')->first()->value;
        $this->sharedKey = Setting::where('key', 'odyssey_shared_key')->first()->value;
        $this->fieldId = Setting::where('key', 'odyssey_custom_field_id')->first()->value;

        $this->ch = curl_init();
    }


    public function ManageToken()
    {
        $gamingSessionLast = GamingSession::orderBy('id', 'DESC')->first();
        $reconnect = false;

        if (is_null($gamingSessionLast)) {
            $reconnect = true;
        } else {
            $token = $gamingSessionLast->sessionToken;

            $resultGetSessionInfo = $this->GetSessionInfo($token);
            $sessionInfoDecode = \GuzzleHttp\json_decode($resultGetSessionInfo);

            // Log::info(print_r($sessionInfoDecode, true));

            if (isset($sessionInfoDecode->body->result->SessionEnd)) {
                $SessionEnd = Carbon::parse($sessionInfoDecode->body->result->SessionEnd);

                $now = Carbon::now(config('app.timezone'));
                $expiredSession = Carbon::parse($gamingSessionLast->created_at)->addSeconds($gamingSessionLast->sessionTimeout);

                // $date_diff = $now->diffInSeconds($SessionEnd, false);
                $date_diff = $now->diffInSeconds($expiredSession, false);

                // Log::error($date_diff);

                if ($date_diff < 0) {
                    $reconnect = true;
                    // Log::info("sessionInfoDecode SessionEnd expired");
                }
            } else if (isset($sessionInfoDecode->body->errorCode)) {
                $reconnect = true;
                // Log::info("sessionInfoDecode error");
            } else {
                // Log::info("sessionInfoDecode not error");
                $reconnect = false;
            }
        }
        // Log::info("reconnect ".$reconnect);

        if ($reconnect) {
            $resultConnect = $this->Connect();
            $resultChallengeResponse = $this->ChallengeResponse($resultConnect);

            $challengeDecode = \GuzzleHttp\json_decode($resultChallengeResponse);

            // Log::info(print_r($challengeDecode, true));

            $sessionToken = $challengeDecode->body->result->sessionToken;
            $sessionTimeout = $challengeDecode->body->result->sessionTimeout;

            $gamingSession = new GamingSession;
            $gamingSession->sessionToken = $sessionToken;
            $gamingSession->sessionTimeout = $sessionTimeout;
            $gamingSession->save();

            $token = $gamingSession->sessionToken;
        }

        return $token;
    }

    public function Connect()
    {
        $commandName = "Connect";

        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent
            ],
            "body" => [
                'commandClass' => "Communication",
                'commandName' => $commandName,
                'parameter' => [
                    'vendorCode' => $this->vendorCode,
                    'deviceCode' => $this->deviceCode
                ]
            ]
        );

        return $this->callAPI($commandName, $this->headers, $body);
    }

    public function ChallengeResponse($connectResult)
    {
        $commandName = "ChallengeResponse";

        $connectDecode = \GuzzleHttp\json_decode($connectResult);

        // Log::info(print_r($connectDecode, true));

        $challenge = $connectDecode->body->result->challenge;
        $offset = $connectDecode->body->result->offset;
        $length = $connectDecode->body->result->length;

        $challengeString = substr($challenge, $offset, $length);

        $sha1 = sha1($this->sharedKey . $challengeString, true);
        $response = base64_encode($sha1);

        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent
            ],
            "body" => [
                'commandClass' => "Communication",
                'commandName' => $commandName,
                'parameter' => [
                    'response' => $response,
                    'challenge' => $challenge,
                    'offset' => $offset,
                    'length' => $length
                ]
            ]
        );

        // Log::info($body);

        return $this->callAPI($commandName, $this->headers, $body);
    }


    public function AddMember($sessionToken, $Gender, $Title, $FirstName, $LastName, $PreferredName, $BirthDate, $Occupation,
                              $AddressType, $AddressLine1, $Suburb, $PostCode, $State)
    {
        $commandName = "AddMember";

        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent,
                "sessionToken" => $sessionToken
            ],
            "body" => [
                'commandClass' => "MembershipClass",
                'commandName' => $commandName,
                'parameter' => [
                    '$json' => [
                        '$type' => "M1.MemberDetail, M1.Core.Shared",
                        'MemberProfile' => [
                            'Gender' => $Gender,
                            'Title' => $Title,
                            'FirstName' => $FirstName,
                            'LastName' => $LastName,
                            'PreferredName' => $PreferredName,
                            'BirthDate' => $BirthDate,
                            'Occupation' => $Occupation
                        ],
                        'AddressLists' => [
                            [
                                'AddressType' => $AddressType,
                                'AddressLine1' => $AddressLine1,
                                'Suburb' => $Suburb,
                                'PostCode' => $PostCode
                            ]

                            // 'State' => $State,
                            // 'AddressType' => $AddressType,
                            // 'AddressLine1' => $AddressLine1,
                            // 'Suburb' => $Suburb,
                            // 'PostCode' => $PostCode
                        ]
                    ]
                ]
            ]
        );

        return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetSessionInfo($sessionToken)
    {
        $commandName = "GetSessionInfo";

        $fromSystem = "BePoz";
        $toSystem = "arc_svr";
        $messageId = "0";
        $dateTimeSent = "2021-05-31T23:59:59";

        $commandName = "GetSessionInfo";

        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent,
                "sessionToken" => $sessionToken
            ],
            "body" => [
                'commandClass' => "Communication",
                'commandName' => $commandName
            ]
        );

        return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetLookupValues($sessionToken, $ListType)
    {
        // Log::info($ListType);
        $commandName = "GetLookupValues";
        $commandVersion = "1";

        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent,
                "sessionToken" => $sessionToken
            ],
            "body" => [
                'commandClass' => "MembershipClass",
                'commandName' => $commandName,
                'commandVersion' => $commandVersion,
                'parameter' => [
                    'ListType' => $ListType
                ]
            ]
        );


        return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetCustomFields($sessionToken)
    {
        $commandName = "GetCustomFields";
        $commandVersion = "1";

        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent,
                "sessionToken" => $sessionToken
            ],
            "body" => [
                'commandClass' => "MembershipClass",
                'commandName' => $commandName,
                'commandVersion' => $commandVersion
            ]
        );

        return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMember($sessionToken, $BadgeNo)
    {
        $commandName = "GetMember";
        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent,
                "sessionToken" => $sessionToken
            ],
            "body" => [
                'commandClass' => "MembershipClass",
                'commandName' => $commandName,
                'parameter' => [
                    "BadgeNo" => $BadgeNo
                ]
            ]
        );

        return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMemberNames()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMemberDetail()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMemberAddresses()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMemberContactChannels()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMemberCustomFields()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMemberPhoto()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function GetMembers()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function AddPreliminaryMember($sessionToken, $Gender, $Title, $FirstName, $LastName, $PreferredName, $BirthDate, $Occupation,
                                         $AddressType, $AddressLine1, $Suburb, $PostCode, $State, $EmailAddress, $Mobilephone, $venue)
    {
        $commandName = "AddPreliminaryMember";

        $body = array(
            "header" => [
                "fromSystem" => $this->fromSystem,
                "toSystem" => $this->toSystem,
                "messageId" => $this->messageId,
                "dateTimeSent" => $this->dateTimeSent,
                "sessionToken" => $sessionToken
            ],
            "body" => [
                'commandClass' => "MembershipClass",
                'commandName' => $commandName,
                'parameter' => [
                    '$Member' => [
                        '__Object__Type' => "M1.Member",
                        'Detail' => [
                            'Gender' => $Gender,
                            'Title' => $Title,
                            'FirstName' => $FirstName,
                            'LastName' => $LastName,
                            'PreferredName' => $PreferredName,
                            'BirthDate' => $BirthDate,
                            'Occupation' => $Occupation,
                            'Proposer' => $venue
                        ],
                        'Addresses' => [
                            [
                                'AddressType' => 'HOME',
                                'Line1' => $AddressLine1,
                                'Suburb' => $Suburb,
                                'State' => $State,
                                'PostCode' => $PostCode
                            ]
                        ],
                        'ContactChannels' => [
                            [
                                'ChannelType' => "EMAILADDRESS",
                                'value' => $EmailAddress
                            ],
                            [
                                'ChannelType' => "MOBILEPHONE",
                                'value' => $Mobilephone
                            ]
                        ],
                        'CustomFields' => [
                            [
                                'FieldId' => $this->fieldId,
                                'Value' => $venue
                            ]
                        ]
                        // 'AddressType' => $AddressType,
                        // 'AddressLine1' => $AddressLine1,
                        // 'Suburb' => $Suburb,
                        // 'State' => $State,
                        // 'PostCode' => $PostCode
                    ]
                ]
            ]
        );

        return $this->callAPI($commandName, $this->headers, $body);
    }

    public function UpdateMemberDetail()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function UpdateMemberAddress()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function UpdateMemberContactChannels()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function UpdateMemberCustomFields()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function Address()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function ContactChannel()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function CustomField()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function MemberCustomField()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function Image()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function Member()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function MemberDetail()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        //return $this->callAPI($commandName, $this->headers, $body);
    }

    public function MemberBasic()
    {
        $commandName = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";


        //return $this->callAPI($commandName, $this->headers, $body);
    }

    protected function callAPI($commandName, $headers, $body)
    {
        $url = $this->url;

        // Log::info($headers);
        // Log::info($body);

        $bodysend = \GuzzleHttp\json_encode($body);
        // Log::info($bodysend);

        // PHP cURL  for https connection with auth
        $ch = $this->ch;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $bodysend);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("MetropolisARCOdyssey");
        // Log::info($this->url);
        // Log::info($httpCode);
        // Log::info($content);

        if ($httpCode >= 200 && $httpCode < 300) {
            if ($content) {
                $result = $content;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }

        $log = new GamingLog();
        $log->request_type = $commandName;
        $log->request = $bodysend;
        $log->response = $content;
        $log->save();

        return $result;
    }


}
