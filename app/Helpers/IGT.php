<?php

namespace App\Helpers;

use App\GamingLog;
use App\Helpers\Contracts\IGTContract;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use LSS\Array2XML;
use LSS\XML2Array;
use SoapClient;
use CodeDredd\Soap\Facades\Soap;

class IGT implements IGTContract
{
    // variable for REST api client
    private $key;
    private $url;
    private $ch;
    private $client;
    private $soapSiteID;
    private $soapUser;
    private $soapPassword;

    /**
     * Bepoz constructor.
     */
    public function __construct()
    {
        Array2XML::init('1.0', 'UTF-8');


        $this->ch = curl_init();

        // $this->url = "https://192.168.1.20:8011/club/services/membershipService?wsdl";
        // $this->url = "https://192.168.1.20:8011/club/services/membershipService";
        // $this->soapSiteID = 0;
        // $this->soapUser = "BEPOZ";
        // $this->soapPassword = "0xBEFE35E1,0xABD99F4A,0xFDE289CA,0xD90DB07E,0x58549375";
        
        $this->url = Setting::where('key', 'gaming_system_url')->first()->value;
        $this->soapSiteID = Setting::where('key', 'gaming_site_id')->first()->value;
        $this->soapUser = Setting::where('key', 'gaming_username')->first()->value;
        $this->soapPassword = Setting::where('key', 'gaming_password')->first()->value;

    }

    public function SystemCheck()
    {
        $url = $this->url;

        $ch = $this->ch;
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        // curl_setopt($ch, CURLOPT_POST, true);

        
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("IGT SystemCheck WSDL");
        // Log::info($this->url);
        // Log::info($httpCode);
        // Log::info($content);

        return $content;
    }

    public function customSoap($url, $xml_post_string)
    {        
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: ", 
            "Content-length: ".strlen($xml_post_string),
        ); //SOAPAction: your op URL

        // PHP cURL  for https connection with auth
        $ch = $this->ch;
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($httpCode >= 200 && $httpCode < 300) {
            if ($content) {
                
            } else {
                
            }
        } else {
            // $timestamp = Carbon::now(config('app.timezone'))->timestamp;
            // if ($timestamp % 3 == 0) {
            //     Log::info('Bepoz.php(536): 404 Not Found. No Bepoz Connection.');
            // }
            // $result = false;
        }
        
        $log = new GamingLog();
        $log->request_type = "Custom Call";
        $log->request = $xml_post_string;
        $log->response = $content;
        $log->save();

        return $content;
    }

    protected function callAPI($requestType, $param)
    {
        //Data, connection, auth
        // $dataFromTheForm = $_POST['fieldName']; // request data from the form
        
        $url = $this->url; // asmx URL of WSDL
        $soapSiteID = $this->soapSiteID;
        $soapUser = $this->soapUser;
        $soapPassword = $this->soapPassword;
        
        // Log::info($param);

        // xml post structure
        $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
        xmlns:v1="http://www.igta.com/club/services/membership/v1_0" 
        xmlns:v11="http://www.igta.com/club/services/membership/types/v1_0">
            <soapenv:Header>
            <v1:loginDetails>
                <v11:siteId>'.$soapSiteID.'</v11:siteId>
                <v11:username>'.$soapUser.'</v11:username>
                <v11:password>'.$soapPassword.'</v11:password>
            </v1:loginDetails>
            </soapenv:Header>
            <soapenv:Body>'.$param.'</soapenv:Body>
        </soapenv:Envelope>';   // data from the form, e.g. some ID number

        $headers = array(
                    "Content-type: text/xml;charset=\"utf-8\"",
                    "Accept: text/xml",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    "SOAPAction: ", 
                    "Content-length: ".strlen($xml_post_string),
                ); //SOAPAction: your op URL


        // PHP cURL  for https connection with auth
        $ch = $this->ch;
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("IGT");
        // Log::info($this->url);
        // Log::info($httpCode);
        // Log::info($content);

        // $result = $content;
        $responseType = $requestType."Response";

        if ($httpCode >= 200 && $httpCode < 300) {
            if ($content) {
                $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $content);
                $simplexml = simplexml_load_string($clean_xml);
                $response = $simplexml->Body->$responseType;

                // $result = XML2Array::createArray($response);

                $result = $response;
            } else {
                $result = false;                    
            }
        } else {            
            // $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $content);
            // $simplexml = simplexml_load_string($clean_xml);
                
            // $result = $simplexml->Body;
            $result = false;
        }

        $log = new GamingLog();
        $log->request_type = $responseType;
        $log->request = $xml_post_string;
        $log->response = $content;
        $log->save();

        return $result;
    }

    public function GetLanguageTypesListRequest()
    {
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetLanguageTypesListRequest";
        $mac = "";
        $xml = "";

        // return $this->provider('GetLanguageTypesListRequest', $query);

        return $this->callAPI($requestType, $mac, $xml);
    }


    public function GetAddressTypesList(){
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetAddressTypesList";
        $param = "<v11:GetAddressTypesListRequest/>";

        return $this->callAPI($requestType, $param);

        // return $this->provider('GetLanguageTypesListRequest', $query);

    }

    public function GetAffiliationTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetAffiliationTypesList";
        $param = "<v11:GetAffiliationTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetAttractionTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetAttractionTypesList";
        $param = "<v11:GetAttractionTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetContactMethodTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetContactMethodTypesList";
        $param = "<v11:GetContactMethodTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetCountriesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetCountriesList";
        $param = "<v11:GetCountriesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetEmailTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetEmailTypesList";
        $param = "<v11:GetEmailTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetFrequentFlyerTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetFrequentFlyerTypesList";
        $param = "<v11:GetFrequentFlyerTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetGendersList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetGendersList";
        $param = "<v11:GetGendersListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetGenerationsList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetGenerationsList";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetIdentificationTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetIdentificationTypesList";
        $param = "<v11:GetIdentificationTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetInterestsList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetInterestsList";
        $param = "<v11:GetInterestsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetLanguageTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetLanguageTypesList";
        $param = "<v11:GetLanguageTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMandatoryFields(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMandatoryFields";
        $param = "<v11:GetMandatoryFieldsRequest/>";

        return $this->callAPI($requestType, $param);

        // return $this->callSoapClient($requestType);
    }


    public function GetMembershipStatusList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMembershipStatusList";
        $param = "<v11:GetMembershipStatusListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetNotificationMethodsList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetNotificationMethodsList";
        $param = "<v11:GetNotificationMethodsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetPaymentMethodList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetPaymentMethodList";
        $param = "<v11:GetPaymentMethodListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetPhoneTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetPhoneTypesList";
        $param = "<v11:GetPhoneTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetSegmentationCodeTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetSegmentationCodeTypesList";
        $param = "<v11:GetSegmentationCodeTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetSiteDetailsList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetSiteDetailsList";
        $param = "<v11:GetSiteDetailsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetStatesList($countryID){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetStatesList";
        $param = "<v11:GetStatesListRequest>
            <v11:countryId>".$countryID."</v11:countryId>
            </v11:GetStatesListRequest>";

        return $this->callAPI($requestType, $param);
    }


    public function GetSuffixesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetSuffixesList";
        $param = "<v11:GetSuffixesListRequest/>";

        return $this->callAPI($requestType, $param);
    }

    public function GetTitlesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetTitlesList";
        $param = "<v11:GetTitlesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetZipCodeDetailsList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetZipCodeDetailsList";
        $param = "<v11:GetZipCodeDetailsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function CreateMember($siteId, $membershipId, $termId, $memberNumber, $firstName, $lastName, $preferredName,
        $address, $zipCode, $addressCountryId, $addressStateId, $phoneNumber, $email){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "CreateMember";
        $param = "<v11:CreateMemberRequest>
            <v11:siteId>".$siteId."</v11:siteId>
            <v11:membership>
                <v11:membershipId>".$membershipId."</v11:membershipId>
                <v11:termId>".$termId."</v11:termId>
                <v11:memberNumber>".$memberNumber."</v11:memberNumber>
            </v11:membership>
            <v11:personal>
                <v11:firstName>".$firstName."</v11:firstName>
                <v11:lastName>".$lastName."</v11:lastName>
                <v11:preferredName>".$preferredName."</v11:preferredName>
            </v11:personal>
            <v11:contact>
                <v11:contactMethodId>1</v11:contactMethodId>
                <v11:addressList>
                    <v11:typeId>1</v11:typeId>
                    <v11:line1>".$address."</v11:line1>
                    <v11:zipCode>".$zipCode."</v11:zipCode>
                    <v11:countryId>".$addressCountryId."</v11:countryId>
                    <v11:mailingAddress>true</v11:mailingAddress>
                    <v11:creditAddress>true</v11:creditAddress>
                    <v11:badAddress>false</v11:badAddress>
                    <v11:verified>false</v11:verified>
                    <v11:city>Melbourne</v11:city>
                    <v11:stateId>".$addressStateId."</v11:stateId>
                </v11:addressList>
                <v11:phoneList>
                    <v11:typeId>1</v11:typeId>
                    <v11:preferred>true</v11:preferred>
                    <v11:number>".$phoneNumber."</v11:number>
                </v11:phoneList>
                <v11:emailList>
                    <v11:typeId>1</v11:typeId>
                    <v11:address>".$email."</v11:address>
                    <v11:preferred>true</v11:preferred>
                </v11:emailList>
            </v11:contact>
        </v11:CreateMemberRequest>";

        return $this->callAPI($requestType, $param);
    }


    public function GetPlayerMembershipDetails($siteId, $memberNumber){
        // Log::info($memberNumber);
        // Log::info($siteId);
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetPlayerMembershipDetails";
        $param = "<v11:GetGenerationsListRequest/>";
        $param = "<v11:GetPlayerMembershipDetailsRequest>
                <v11:siteId>".$siteId."</v11:siteId>
                <v11:memberNumber>".$memberNumber."</v11:memberNumber>
            </v11:GetPlayerMembershipDetailsRequest>";


        return $this->callAPI($requestType, $param);
    }


    public function GetMemberAddressList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberAddressList";
        $param = "<v11:GetMemberAddressListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMemberPhoneList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberPhoneList";
        $param = "<v11:GetMemberPhoneListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMemberEmailList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberEmailList";
        $param = "<v11:GetMemberEmailListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberStatus(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberStatus";
        $param = "<v11:UpdateMemberStatusRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberSuspended(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberSuspended";
        $param = "<v11:UpdateMemberSuspendedRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberCardId(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberCardId";
        $param = "<v11:UpdateMemberCardIdRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberCardPIN(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberCardPIN";
        $param = "<v11:UpdateMemberCardPINRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberDirectMarketingPreferences(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberDirectMarketingPreferences";
        $param = "<v11:UpdateMemberDirectMarketingPreferencesRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberIdentification(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberIdentification";
        $param = "<v11:UpdateMemberIdentificationRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function SearchMember(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "SearchMember";
        $param = "<v11:SearchMemberRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetAvailableMemberNumbersList($membershipId){
        // Log::info($membershipId);
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetAvailableMemberNumbersList";
        $param = "<v11:GetAvailableMemberNumbersListRequest>
                <v11:membershipId>".$membershipId."</v11:membershipId>
            </v11:GetAvailableMemberNumbersListRequest>";

        return $this->callAPI($requestType, $param);
    }


    public function GetAvailableMemberNumbersListWithHint(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetAvailableMemberNumbersListWithHint";
        $param = "<v11:GetAvailableMemberNumbersListWithHintRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetNextAvailableMemberNumber($membershipId){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetNextAvailableMemberNumber";
        $param = "<v11:GetNextAvailableMemberNumberRequest>
            <v11:membershipId>".$membershipId."</v11:membershipId>
            </v11:GetNextAvailableMemberNumberRequest>";

        return $this->callAPI($requestType, $param);
    }


    public function MemberNumberHold($membershipId, $memberNumber){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "MemberNumberHold";
        $param = "<v11:MemberNumberHoldRequest>
            <v11:membershipId>".$membershipId."</v11:membershipId>
            <v11:memberNumber>".$memberNumber."</v11:memberNumber>
            </v11:MemberNumberHoldRequest>";

        return $this->callAPI($requestType, $param);
    }

    public function GetMemberInvoices(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberInvoices";
        $param = "<v11:GetMemberInvoicesRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMemberInvoicePaymentHistory(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberInvoicePaymentHistory";
        $param = "<v11:GetMemberInvoicePaymentHistoryRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function MembershipPaymentInsert(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "MembershipPaymentInsert";
        $param = "<v11:MembershipPaymentInsertRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberAddress(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberAddress";
        $param = "<v11:UpdateMemberAddressRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberPhone(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberPhone";
        $param = "<v11:UpdateMemberPhoneRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberEmail(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberEmail";
        $param = "<v11:UpdateMemberEmailRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberContactMethod(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberContactMethod";
        $param = "<v11:UpdateMemberContactMethodRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateMemberPersonalDetails(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateMemberPersonalDetails";
        $param = "<v11:UpdateMemberPersonalDetailsRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMembershipClassesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMembershipClassesList";
        $param = "<v11:GetMembershipClassesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMembershipRelationship(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMembershipRelationship";
        $param = "<v11:GetMembershipRelationshipRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMembershipTermsList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMembershipTermsList";
        $param = "<v11:GetMembershipTermsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMembershipTypesList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMembershipTypesList";
        $param = "<v11:GetMembershipTypesListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMemberRslAwardList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberRslAwardList";
        $param = "<v11:GetMemberRslAwardListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMemberServiceAwardList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberServiceAwardList";
        $param = "<v11:GetMemberServiceAwardListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMemberServiceLocationList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberServiceLocationList";
        $param = "<v11:GetMemberServiceLocationListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetRslAwardList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetRslAwardList";
        $param = "<v11:GetRslAwardListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetServiceArmList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetServiceArmList";
        $param = "<v11:GetServiceArmListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetServiceAwardList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetServiceAwardList";
        $param = "<v11:GetServiceAwardListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetServiceLocationList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetServiceLocationList";
        $param = "<v11:GetServiceLocationListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetServiceRankList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetServiceRankList";
        $param = "<v11:GetServiceRankListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetSubClubRslBranchDetails(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetSubClubRslBranchDetails";
        $param = "<v11:GetSubClubRslBranchDetailsRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function UpdateSubClubRslBranchDetails(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "UpdateSubClubRslBranchDetails";
        $param = "<v11:UpdateSubClubRslBranchDetailsRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetDirectMarketingOptionsList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetDirectMarketingOptionsList";
        $param = "<v11:GetDirectMarketingOptionsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetDirectMarketingReasonList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetDirectMarketingReasonList";
        $param = "<v11:GetDirectMarketingReasonListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function GetMemberDirectMarketingList(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "GetMemberDirectMarketingList";
        $param = "<v11:GetMemberDirectMarketingListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function DisplayValueType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "DisplayValueType";
        $param = "<v11:DisplayValueTypeRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function MembershipType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "MembershipType";
        $param = "<v11:MembershipTypeRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function ContactDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "ContactDetailsType";
        $param = "<v11:ContactDetailsTypeRequest/>";

        return $this->callAPI($requestType, $param);
    }

    public function AddressType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "AddressType";
        $param = "<v11:AddressTypeRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function PhoneNumberType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "PhoneNumberType";
        $param = "<v11:PhoneNumberTypeRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function EmailType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "EmailType";
        $param = "<v11:EmailTypeRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function IndentificationDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "IndentificationDetailsType";
        $param = "<v11:IndentificationDetailsTypeRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function EmploymentDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "EmploymentDetailsType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function MarketingDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "MarketingDetailsType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function FrequentFlyerType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "FrequentFlyerType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function InterestType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "InterestType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function CardType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "CardType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function MemberSearchDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "MemberSearchDetailsType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function MembershipRelationshipType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "MembershipRelationshipType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function MembershipTermType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "MembershipTermType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function ZipCodeDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "ZipCodeDetailsType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function InvoiceDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "InvoiceDetailsType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function InvoiceItemType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "InvoiceItemType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);
    }


    public function SubClubRslBranchDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "SubClubRslBranchDetailsType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);

    }


    public function DirectMarketingOptionType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "DirectMarketingOptionType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);

    }


    public function DirectMarketingReasonType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "DirectMarketingReasonType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);

    }


    public function DirectMarketingDetailsType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "DirectMarketingDetailsType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);

    }


    public function DescriptionStateType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "DescriptionStateType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);

    }


    // public function AddressType(){
    //     
    //     // $query = ['hoursToLookBack'=>$hours];
        // $query = [];
        // $requestType = "GetAddressTypesList";
        // $mac = "";
        // $xml = "";

        // return $this->callAPI($requestType, $mac, $xml);

    //     return $this->provider('AddressType', $query);

    // }


    public function DirectMarketingPreferenceType(){
        
        // $query = ['hoursToLookBack'=>$hours];
        $query = [];
        $requestType = "DirectMarketingPreferenceType";
        $param = "<v11:GetGenerationsListRequest/>";

        return $this->callAPI($requestType, $param);

    }

    
}