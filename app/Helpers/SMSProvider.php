<?php

namespace App\Helpers;

use App\Helpers\Contracts\SMSContract;
use Illuminate\Support\Facades\Config;
use Smsglobal\RestApiClient\ApiKey;
use Smsglobal\RestApiClient\RestApiClient;
use Smsglobal\RestApiClient\Resource\Sms;


class SMSProvider implements SMSContract
{
    // variable for REST api client
    private $client;

    /**
     * SMS constructor.
     */
    public function __construct()
    {
        $this->client = new RestApiClient(
            new ApiKey(
                Config::get('sms.key'),
                Config::get('sms.secret')
            )
        );
    }

    /**
     * Send a message using smsglobal (3rd party service)
     *
     * @param $number
     * @param $message
     * @return bool
     */
    public function sendSMS($number, $message)
    {
        $sms = new Sms();
        $sms->setDestination($number)
            ->setOrigin(Config::get('sms.origin'))
            ->setMessage($message);

        $sms->send($this->client);

        return true;
    }
}