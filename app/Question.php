<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function question_answers()
    {
        return $this->hasMany('App\QuestionAnswer');
    }

    public function answers()
    {
        return $this->belongsToMany('App\Answer', 'answer_question', 'question_id', 'answer_id')->withPivot('answer_order')->orderBy('answer_order', 'asc')->wherePivot('deleted_at', null)->withTimestamps();
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($question) { // before delete() method call this

            foreach ($question->question_answers as $answer) {
                $answer->delete();
            }

        });

    }
}
