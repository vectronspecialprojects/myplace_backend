<?php

namespace App;

use App\Listing;
use App\VenueTag;
use App\Venue;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\FromArray;
// use Maatwebsite\Excel\Concerns\FromCollection;

class ExportEnquiry implements FromView
{
    public function __construct($values)
    {
        $this->values = $values;
    }

    public function view(): View
    {
        return view('export.enquiry', [
            'values' => $this->values
        ]);
    }
}
