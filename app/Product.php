<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_by', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function listings()
    {
        return $this->hasMany('App\ListingProduct');
    }

    /**
     * Get the product type
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_type()
    {
        return $this->belongsTo('App\ProductType', 'product_type_id');
    }

    public function order_details()
    {
      return $this->hasMany('App\OrderDetail', 'product_id');
    }
    
    public function order_details_sum()
    {
      return $this->hasMany('App\OrderDetail', 'product_id')
        ->selectRaw('product_id, SUM(qty) as total')
        ->groupBy('product_id');
        //->sum();
        //->sum('qty')->groupBy('product_id');
      
      //$q = $this->hasMany('App\OrderDetail', 'product_id');
      //return $q->sum('qty');
    }

    /**
     * Get the voucher setup.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucher_setup()
    {
        return $this->belongsTo('App\VoucherSetups', 'voucher_setups_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($product) {
            if (is_null($product->image)) {
                //$setting = Setting::where('key', 'product_image')->first();
                $setting = Setting::where('key', 'invoice_logo')->first();
                if (!is_null($setting)) {
                    $product->image = $setting->value;
                } else {
                    $product->image = "https://via.placeholder.com/828x828";
                }
            }

        });

        static::deleting(function ($product) { // before delete() method call this

            foreach ($product->listings as $listing) {
                $listing->delete();
            }

        });
    }
}
