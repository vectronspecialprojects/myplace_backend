<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'surveys';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function survey_questions()
    {
        return $this->hasMany('App\SurveyQuestion');
    }

    public function survey_tiers()
    {
        return $this->hasMany('App\SurveyTier');
    }

    public function survey_answers()
    {
        return $this->hasMany('App\MemberAnswer');
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'question_survey', 'survey_id', 'question_id')->withPivot('question_order')->wherePivot('deleted_at', null)->withTimestamps();
    }

    public function tiers()
    {
        return $this->belongsToMany('App\Tier', 'survey_tier', 'survey_id', 'tier_id')->wherePivot('deleted_at', null)->withTimestamps();
    }

    /**
     * Get the related VoucherSetups
     * >> Define relationship
     *
     */
    public function voucher_setups()
    {
        return $this->belongsTo('App\VoucherSetups', 'voucher_setup_reward');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($survey) { // before delete() method call this

            foreach ($survey->survey_tiers as $tier) {
                $tier->delete();
            }

            foreach ($survey->survey_questions as $question) {
                $question->delete();
            }

        });

    }
}
