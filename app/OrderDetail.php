<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_details';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the related voucher setup
     * >> Define relationship
     *
     */
    public function voucher_setup()
    {
        return $this->belongsTo('App\VoucherSetups', 'voucher_setups_id');
    }

    /**
     * Get the related member vouchers
     * >> Define relationship
     *
     */
    public function member_vouchers()
    {
        return $this->hasMany('App\MemberVouchers', 'order_details_id');
    }

    /**
     * Get the order
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    /**
     * Get the order
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order_payment_success()
    {
        return $this->belongsTo('App\Order', 'order_id')->where('payment_status', 'successful');
    }

    /**
     * Get the listing
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listing()
    {
        return $this->belongsTo('App\Listing', 'listing_id');
    }

    /**
     * Get the product
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }   

    /**
     * Get the product type
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_type()
    {
        return $this->belongsTo('App\ProductType', 'product_type_id');
    }

    /**
     * Get the Venue
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function venue()
    {
        return $this->belongsTo('App\Venue', 'venue_id');
    }   

    public function gift_certificate()
    {
        return $this->hasOne('App\GiftCertificate', 'order_details_id');
    }
}
