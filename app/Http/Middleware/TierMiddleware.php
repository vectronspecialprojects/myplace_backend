<?php

namespace App\Http\Middleware;

use App\Tier;
use Closure;
use Illuminate\Http\Response;

class TierMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get role data from routes
        $tiers = $request->route()->getAction()['tiers'];

        if (!is_array($tiers)) {
            $tiers = [$tiers];
        }

        $current = null;

        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
            $current = $request->input('decrypted_token')->member->tiers()->first()->key;

            $member_tier = Tier::find($request->input('decrypted_token')->member->tiers()->first()->id);
            foreach ($tiers as $tier) {
                if ($member_tier->key == $tier) {
                    return $next($request);
                }
            }

        }

        return response()->json(['status' => 'error', 'message' => 'This feature can be only accessed by another Tier, please upgrade your Tier.', 'tiers_required' => $tiers, 'current_tier' => $current], Response::HTTP_BAD_REQUEST);


    }
}
