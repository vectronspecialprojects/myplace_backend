<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class AccessAuthenticatorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  $access
     * @return mixed
     */
    public function handle($request, Closure $next, $access)
    {
        switch ($access) {
            case 'setting_read':
                if ($request->input('decrypted_token')->roles()->first()->control->setting_read) {
                    return $next($request);
                }
                break;
            case 'setting_update':
                if ($request->input('decrypted_token')->roles()->first()->control->setting_update) {
                    return $next($request);
                }
                break;

            case 'voucher_create':
                if ($request->input('decrypted_token')->roles()->first()->control->voucher_create) {
                    return $next($request);
                }
                break;
            case 'voucher_read':
                if ($request->input('decrypted_token')->roles()->first()->control->voucher_read) {
                    return $next($request);
                }
                break;
            case 'voucher_update':
                if ($request->input('decrypted_token')->roles()->first()->control->voucher_update) {
                    return $next($request);
                }
                break;
            case 'voucher_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->voucher_delete) {
                    return $next($request);
                }
                break;

            case 'listing_create':
                if ($request->input('decrypted_token')->roles()->first()->control->listing_create) {
                    return $next($request);
                }
                break;
            case 'listing_read':
                if ($request->input('decrypted_token')->roles()->first()->control->listing_read) {
                    return $next($request);
                }
                break;
            case 'listing_update':
                if ($request->input('decrypted_token')->roles()->first()->control->listing_update) {
                    return $next($request);
                }
                break;
            case 'listing_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->listing_delete) {
                    return $next($request);
                }
                break;

            case 'member_create':
                if ($request->input('decrypted_token')->roles()->first()->control->member_create) {
                    return $next($request);
                }
                break;
            case 'member_read':
                if ($request->input('decrypted_token')->roles()->first()->control->member_read) {
                    return $next($request);
                }
                break;
            case 'member_update':
                if ($request->input('decrypted_token')->roles()->first()->control->member_update) {
                    return $next($request);
                }
                break;
            case 'member_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->member_delete) {
                    return $next($request);
                }
                break;

            case 'product_create':
                if ($request->input('decrypted_token')->roles()->first()->control->product_create) {
                    return $next($request);
                }
                break;
            case 'product_read':
                if ($request->input('decrypted_token')->roles()->first()->control->product_read) {
                    return $next($request);
                }
                break;
            case 'product_update':
                if ($request->input('decrypted_token')->roles()->first()->control->product_update) {
                    return $next($request);
                }
                break;
            case 'product_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->product_delete) {
                    return $next($request);
                }
                break;

            case 'role_create':
                if ($request->input('decrypted_token')->roles()->first()->control->role_create) {
                    return $next($request);
                }
                break;
            case 'role_read':
                if ($request->input('decrypted_token')->roles()->first()->control->role_read) {
                    return $next($request);
                }
                break;
            case 'role_update':
                if ($request->input('decrypted_token')->roles()->first()->control->role_update) {
                    return $next($request);
                }
                break;
            case 'role_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->role_delete) {
                    return $next($request);
                }
                break;

            case 'user_create':
                if ($request->input('decrypted_token')->roles()->first()->control->user_create) {
                    return $next($request);
                }
                break;
            case 'user_read':
                if ($request->input('decrypted_token')->roles()->first()->control->user_read) {
                    return $next($request);
                }
                break;
            case 'user_update':
                if ($request->input('decrypted_token')->roles()->first()->control->user_update) {
                    return $next($request);
                }
                break;
            case 'user_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->user_delete) {
                    return $next($request);
                }
                break;

            case 'user_role_create':
                if ($request->input('decrypted_token')->roles()->first()->control->user_role_create) {
                    return $next($request);
                }
                break;
            case 'user_role_read':
                if ($request->input('decrypted_token')->roles()->first()->control->user_role_read) {
                    return $next($request);
                }
                break;
            case 'user_role_update':
                if ($request->input('decrypted_token')->roles()->first()->control->user_role_update) {
                    return $next($request);
                }
                break;
            case 'user_role_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->user_role_delete) {
                    return $next($request);
                }
                break;

            case 'staff_create':
                if ($request->input('decrypted_token')->roles()->first()->control->staff_create) {
                    return $next($request);
                }
                break;
            case 'staff_read':
                if ($request->input('decrypted_token')->roles()->first()->control->staff_read) {
                    return $next($request);
                }
                break;
            case 'staff_update':
                if ($request->input('decrypted_token')->roles()->first()->control->staff_update) {
                    return $next($request);
                }
                break;
            case 'staff_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->staff_delete) {
                    return $next($request);
                }
                break;

            case 'notification_create':
                if ($request->input('decrypted_token')->roles()->first()->control->notification_create) {
                    return $next($request);
                }
                break;
            case 'notification_read':
                if ($request->input('decrypted_token')->roles()->first()->control->notification_read) {
                    return $next($request);
                }
                break;
            case 'notification_update':
                if ($request->input('decrypted_token')->roles()->first()->control->notification_update) {
                    return $next($request);
                }
                break;
            case 'notification_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->notification_delete) {
                    return $next($request);
                }
                break;

            case 'enquiry_read':
                if ($request->input('decrypted_token')->roles()->first()->control->enquiry_read) {
                    return $next($request);
                }
                break;
            case 'enquiry_update':
                if ($request->input('decrypted_token')->roles()->first()->control->enquiry_update) {
                    return $next($request);
                }
                break;
            case 'enquiry_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->enquiry_delete) {
                    return $next($request);
                }
                break;

            case 'survey_create':
                if ($request->input('decrypted_token')->roles()->first()->control->survey_create) {
                    return $next($request);
                }
                break;
            case 'survey_read':
                if ($request->input('decrypted_token')->roles()->first()->control->survey_read) {
                    return $next($request);
                }
                break;
            case 'survey_update':
                if ($request->input('decrypted_token')->roles()->first()->control->survey_update) {
                    return $next($request);
                }
                break;
            case 'survey_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->survey_delete) {
                    return $next($request);
                }
                break;

            case 'friend_referral_read':
                if ($request->input('decrypted_token')->roles()->first()->control->friend_referral_read) {
                    return $next($request);
                }
                break;

            case 'report_read':
                if ($request->input('decrypted_token')->roles()->first()->control->report_read) {
                    return $next($request);
                }
                break;
            case 'report_export':
                if ($request->input('decrypted_token')->roles()->first()->control->report_export) {
                    return $next($request);
                }
                break;

            case 'transaction_read':
                if ($request->input('decrypted_token')->roles()->first()->control->transaction_read) {
                    return $next($request);
                }
                break;
            case 'transaction_export':
                if ($request->input('decrypted_token')->roles()->first()->control->transaction_export) {
                    return $next($request);
                }
                break;

            case 'faq_create':
                if ($request->input('decrypted_token')->roles()->first()->control->faq_create) {
                    return $next($request);
                }
                break;
            case 'faq_read':
                if ($request->input('decrypted_token')->roles()->first()->control->faq_read) {
                    return $next($request);
                }
                break;
            case 'faq_update':
                if ($request->input('decrypted_token')->roles()->first()->control->faq_update) {
                    return $next($request);
                }
                break;
            case 'faq_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->faq_delete) {
                    return $next($request);
                }
                break;

            case 'venue_create':
                if ($request->input('decrypted_token')->roles()->first()->control->venue_create) {
                    return $next($request);
                }
                break;
            case 'venue_read':
                if ($request->input('decrypted_token')->roles()->first()->control->venue_read) {
                    return $next($request);
                }
                break;
            case 'venue_update':
                if ($request->input('decrypted_token')->roles()->first()->control->venue_update) {
                    return $next($request);
                }
                break;
            case 'venue_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->venue_delete) {
                    return $next($request);
                }
                break;


            case 'frontend_access':
                if ($request->input('decrypted_token')->roles()->first()->control->frontend_access) {
                    return $next($request);
                }
                break;
            case 'backend_access':
                if ($request->input('decrypted_token')->roles()->first()->control->backend_access) {
                    return $next($request);
                }
                break;
            case 'root_access':
                if ($request->input('decrypted_token')->roles()->first()->control->root_access) {
                    return $next($request);
                }
                break;
            case 'venue_management':
                if ($request->input('decrypted_token')->roles()->first()->control->venue_management) {
                    return $next($request);
                }
                break;

            case 'log_read':
                if ($request->input('decrypted_token')->roles()->first()->control->log_read) {
                    return $next($request);
                }
                break;
            case 'log_delete':
                if ($request->input('decrypted_token')->roles()->first()->control->log_delete) {
                    return $next($request);
                }
                break;

            case 'resend_confirmation':
                if ($request->input('decrypted_token')->roles()->first()->control->resend_confirmation) {
                    return $next($request);
                }
                break;
            case 'send_broadcast':
                if ($request->input('decrypted_token')->roles()->first()->control->send_broadcast) {
                    return $next($request);
                }
                break;

        }

        return response()->json(['status' => 'error', 'message' => 'Sorry, you are not allowed to do any operation in this page. Please contact your venue manager.'], Response::HTTP_UNAUTHORIZED);

    }
}
