<?php

namespace App\Http\Middleware;

use App\Platform;
use App\Setting;
use Illuminate\Http\Response;
use Closure;

class VerifyAppToken
{
    /**
     * Handle an incoming request.
     * >> GET methods only require app_token
     * >> POST methods need both app_token and app_secret
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('app_token')) {
            $token = $request->input('app_token');

            $platform = Platform::where('app_token', $token)->where('is_active', 1)->first();

            if (is_null($platform)) {
                $app_url = Setting::where('key', 'app_url')->first()->value;
                return response()->json(['status' => 'error', 'message' => 'Your app is out of date, please update your app.', 'url' => $app_url], Response::HTTP_GONE);
            } else {
                if ($request->isMethod('post') || $request->isMethod('delete') || $request->isMethod('put')) {
                    if ($request->has('app_secret')) {
                        $app_secret = $request->input('app_secret');
                        if ($platform->app_secret == $app_secret) {
                            return $next($request);
                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Invalid App Secret'], Response::HTTP_FORBIDDEN);
                        }
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Missing App Secret'], Response::HTTP_FORBIDDEN);
                    }
                } else {
                    return $next($request);
                }

            }

        } else {
            return response()->json(['status' => 'error', 'message' => 'Missing App Token'], Response::HTTP_FORBIDDEN);
        }
    }
}
