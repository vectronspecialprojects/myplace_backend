<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class EmailMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->decrypted_token->email_confirmation) {
            return $next($request);
        }

        return response()->json(['status' => 'error', 'message' => 'Please confirm your email before processing your request.'], Response::HTTP_NOT_ACCEPTABLE);

    }
}
