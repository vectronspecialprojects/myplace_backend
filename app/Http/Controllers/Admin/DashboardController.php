<?php

namespace App\Http\Controllers\Admin;

use App\ClaimedPromotion;
use App\FriendReferral;
use App\GiftCertificate;
use App\Http\Controllers\Controller;
use App\Listing;
use App\Member;
use App\Order;
use App\OrderDetail;
use App\User;
use App\ExportListing;
use App\ExportPromotion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    protected function newDashboard(Request $request)
    {
        try {
            // set day start and end
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            Carbon::setWeekStartsAt(Carbon::SUNDAY);


            $seven_days_ago = Carbon::now(config('app.timezone'))->subWeek();

            $member_count_last_seven_days = Member::where('status', 'active')
                ->whereDate('created_at', '>=', $seven_days_ago->toDateString())
                ->count();

            if ($request->has('type')) {
                $type = $request->input('type');
                $venue_id = 0;
                if ($request->has('venue_id')) {
                    $venue_id = $request->input('venue_id');
                }

                if ($type === 'today') {
                    $today = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->sum('value');

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '=', $today->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '=', $today->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->sum('paid_subtotal_unit_price');
                    }

                    $friendReferral_count = FriendReferral::whereDate('created_at', '=', $today->toDateString())->count();
                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '=', $today->toDateString())
                        ->where('new_member_id', '<>', 0)->count();

                } elseif ($type === 'yesterday') {
                    $yesterday = Carbon::now(config('app.timezone'))->subDay();

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->sum('value');

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '=', $yesterday->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '=', $yesterday->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '=', $yesterday->toDateString())
                            ->sum('paid_subtotal_unit_price');
                    }

                    $friendReferral_count = FriendReferral::whereDate('created_at', '=', $yesterday->toDateString())->count();
                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '=', $yesterday->toDateString())
                        ->where('new_member_id', '<>', 0)->count();

                } elseif ($type === 'last_week') {
                    $week = Carbon::now(config('app.timezone'))->subWeek();

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_subtotal_unit_price');


                    }

                    $friendReferral_count = FriendReferral::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                        ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                        ->count();

                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                        ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                        ->where('new_member_id', '<>', 0)->count();

                } elseif ($type === 'this_week') {
                    $week = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    }

                    $friendReferral_count = FriendReferral::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                        ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                        ->count();

                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '>=', $week->startOfWeek()->toDateString())
                        ->whereDate('created_at', '<=', $week->endOfWeek()->toDateString())
                        ->where('new_member_id', '<>', 0)->count();

                } elseif ($type === 'last_month') {
                    $month = Carbon::now(config('app.timezone'))->subMonth();

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->where('venue_id', $venue_id)
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->where('venue_id', $venue_id)
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    }


                    $friendReferral_count = FriendReferral::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                        ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                        ->count();

                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                        ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                        ->where('new_member_id', '<>', 0)->count();


                } elseif ($type === 'this_month') {
                    $month = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('paid_subtotal_unit_price');


                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    }


                    $friendReferral_count = FriendReferral::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                        ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                        ->count();

                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '>=', $month->startOfMonth()->toDateString())
                        ->whereDate('created_at', '<=', $month->endOfMonth()->toDateString())
                        ->where('new_member_id', '<>', 0)->count();

                } elseif ($type === 'this_year') {

                    $year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->where('venue_id', $venue_id)
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->where('venue_id', $venue_id)
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    }

                    $friendReferral_count = FriendReferral::whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                        ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                        ->count();

                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '>=', $year->startOfYear()->toDateString())
                        ->whereDate('created_at', '<=', $year->endOfYear()->toDateString())
                        ->where('new_member_id', '<>', 0)->count();
                } elseif ($type === 'custom') {

                    $date_start = Carbon::now(config('app.timezone'));
                    $date_end = Carbon::now(config('app.timezone'));

                    if ($request->has('extra')) {
                        $payload = \GuzzleHttp\json_decode($request->input('extra'));
                        $date_start = Carbon::parse($payload->date_start);
                        $date_end = Carbon::parse($payload->date_end);
                    }

                    $venue_id = 0;
                    if ($request->has('venue_id')) {
                        $venue_id = $request->input('venue_id');
                    }

                    if (intval($venue_id) !== 0) {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->whereHas('member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->count();

                        $gift_certificate_count = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->where('venue_id', $venue_id)
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('venue_id', $venue_id)
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    } else {
                        $member_count_all_the_time = Member::where('status', 'active')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->count();

                        $gift_certificate_count = GiftCertificate::whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->count();

                        $gift_certificate_total_value = GiftCertificate::whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->sum('value');

                        $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->count();

                        $transaction_total_paid = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->where(function ($q) use ($request) {
                                $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                                $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                            })
                            ->sum('paid_total_price');

                        $claimed_promotion = ClaimedPromotion::whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->count();

                        $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->count();

                        $ticket_count = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->sum('qty');

                        $ticket_total_paid = OrderDetail::where('status', 'successful')
                            ->where('product_type_id', 2)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->sum('paid_subtotal_unit_price');

                    }


                    $friendReferral_count = FriendReferral::whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                        ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                        ->count();

                    $friendReferral_joined_count = FriendReferral::whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                        ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                        ->where('new_member_id', '<>', 0)->count();
                }


            } else {
                $member_count_all_the_time = Member::where('status', 'active')->count();


                $gift_certificate_count = GiftCertificate::count();
                $gift_certificate_total_value = GiftCertificate::sum('value');

                $transaction_count_all_the_time = Order::where('order_status', 'confirmed')
                    ->where(function ($q) use ($request) {
                        $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                        $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                    })->count();

                $transaction_total_paid = Order::where('order_status', 'confirmed')
                    ->where(function ($q) use ($request) {
                        $q->where('payload', 'like', '%"transaction_type":"place_order"%');
                        $q->orWhere('payload', 'like', '%"transaction_type":"gift_certificate"%');
                    })
                    ->sum('paid_total_price');

                $claimed_promotion = ClaimedPromotion::count();
                $scanned_claimed_promotion = ClaimedPromotion::where('is_used', true)->count();

                $ticket_count = OrderDetail::where('status', 'successful')
                    ->where('product_type_id', 2)
                    ->sum('qty');

                $ticket_total_paid = OrderDetail::where('status', 'successful')
                    ->where('product_type_id', 2)
                    ->sum('paid_subtotal_unit_price');

                $friendReferral_count = FriendReferral::count();
                $friendReferral_joined_count = FriendReferral::where('new_member_id', '<>', 0)->count();

            }


            $data = array(
                'gift_certificate_count' => is_null($gift_certificate_count) ? 0 : $gift_certificate_count,
                'gift_certificate_total_value' => is_null($gift_certificate_total_value) ? 0 : $gift_certificate_total_value,
                'total_members' => is_null($member_count_all_the_time) ? 0 : $member_count_all_the_time,
                'members_last_seven_days' => is_null($member_count_last_seven_days) ? 0 : $member_count_last_seven_days,
                'total_transactions' => is_null($transaction_count_all_the_time) ? 0 : $transaction_count_all_the_time,
                'transactions_total_paid' => is_null($transaction_total_paid) ? 0 : $transaction_total_paid,
                'total_promotions' => is_null($claimed_promotion) ? 0 : $claimed_promotion,
                'used_promotions' => is_null($scanned_claimed_promotion) ? 0 : $scanned_claimed_promotion,
                'ticket_count' => is_null($ticket_count) ? 0 : $ticket_count,
                'ticket_total_paid' => is_null($ticket_total_paid) ? 0 : $ticket_total_paid,
                'total_friend_referral' => is_null($friendReferral_count) ? 0 : $friendReferral_count,
                'total_friend_referral_joined' => is_null($friendReferral_joined_count) ? 0 : $friendReferral_joined_count
            );

            return response()->json(['status' => 'ok', 'data' => $data]);


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getMonthlySales()
    {
        try {
            $this_year_plus_one_month = Carbon::now(config('app.timezone'))->subYear()->addMonth();
            $last_years_plus_one_month = Carbon::now(config('app.timezone'))->subYears(2)->addMonth();

            $this_year_plus_one_month_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $this_year_plus_one_month->toDateString())
                ->get();

            $last_year_plus_one_month_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $last_years_plus_one_month->toDateString())
                ->whereDate('created_at', '<', $this_year_plus_one_month->toDateString())
                ->get();


            $first_border = Carbon::now(config('app.timezone'))->subYear();
            $last_border = Carbon::now(config('app.timezone'))->addMonths(4);
            $diff_in_months = $first_border->diffInMonths($last_border);

            $this_year_values = array();
            for ($i = 4; $i < $diff_in_months; $i++) {
                $month = Carbon::now(config('app.timezone'))->subYear()->addMonths($i)->month;
                $this_year_values[$month] = array(
                    "key" => "This Year",
                    "series" => 1,
                    "size" => 0,
                    "x" => $month,
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($this_year_plus_one_month_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $this_year_values[$dt->month]["size"]++;
                $this_year_values[$dt->month]["y"] = $this_year_values[$dt->month]["y"] + $order->paid_total_price;
                $this_year_values[$dt->month]["y1"] = $this_year_values[$dt->month]["y1"] + $order->paid_total_price;
            }


            $last_year_values = array();
            for ($i = 1; $i < $diff_in_months; $i++) {
                $last_year_values[$i] = array(
                    "key" => "Last Year",
                    "series" => 1,
                    "size" => 0,
                    "x" => $i,
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($last_year_plus_one_month_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $last_year_values[$dt->month]["size"]++;
                $last_year_values[$dt->month]["y"] = $last_year_values[$dt->month]["y"] + $order->paid_total_price;
                $last_year_values[$dt->month]["y1"] = $last_year_values[$dt->month]["y1"] + $order->paid_total_price;
            }

            $monthly_sales = [
                [
                    "key" => 'This Year',
                    "values" => array_values($this_year_values)
                ],
                [
                    "key" => 'Last Year',
                    "values" => array_values($last_year_values)
                ]
            ];

            return response()->json(['status' => 'ok', 'data' => $monthly_sales]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getWeeklySales()
    {
        try {
            $now = Carbon::now(config('app.timezone'));
            $three_weeks_ago = Carbon::now(config('app.timezone'))->subWeeks(3);

            $last_year_three_weeks_ago = Carbon::now(config('app.timezone'))->subYear()->subWeeks(3);
            $last_year_three_weeks_later = Carbon::now(config('app.timezone'))->subYear()->addWeeks(3);

            $this_year_three_weeks_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $three_weeks_ago->toDateString())
                ->whereDate('created_at', '<=', $now->toDateString())
                ->get();

            $last_year_six_week_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $last_year_three_weeks_ago->toDateString())
                ->whereDate('created_at', '<=', $last_year_three_weeks_later->toDateString())
                ->get();


            $first_border = Carbon::now(config('app.timezone'))->subWeeks(3)->weekOfYear;
            $last_border = Carbon::now(config('app.timezone'))->addWeeks(3)->weekOfYear;

            $this_year_values = array();
            for ($i = $first_border; $i < $last_border; $i++) {
                $this_year_values[$i] = array(
                    "key" => "This Year",
                    "series" => 1,
                    "size" => 0,
                    "x" => $i,
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($this_year_three_weeks_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $this_year_values[$dt->weekOfYear]["size"]++;
                $this_year_values[$dt->weekOfYear]["y"] = $this_year_values[$dt->weekOfYear]["y"] + $order->paid_total_price;
                $this_year_values[$dt->weekOfYear]["y1"] = $this_year_values[$dt->weekOfYear]["y1"] + $order->paid_total_price;
            }


            $last_year_values = array();
            for ($i = $first_border; $i < $last_border; $i++) {
                $last_year_values[$i] = array(
                    "key" => "Last Year",
                    "series" => 1,
                    "size" => 0,
                    "x" => $i,
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($last_year_six_week_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $last_year_values[$dt->weekOfYear]["size"]++;
                $last_year_values[$dt->weekOfYear]["y"] = $last_year_values[$dt->weekOfYear]["y"] + $order->paid_total_price;
                $last_year_values[$dt->weekOfYear]["y1"] = $last_year_values[$dt->weekOfYear]["y1"] + $order->paid_total_price;
            }

            $weekly_sales = [
                [
                    "key" => 'This Year',
                    "values" => array_values($this_year_values)
                ],
                [
                    "key" => 'Last Year',
                    "values" => array_values($last_year_values)
                ]
            ];

            return response()->json(['status' => 'ok', 'data' => $weekly_sales]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getDailySales()
    {
        try {
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            Carbon::setWeekStartsAt(Carbon::SUNDAY);
            $seven_days_ago = Carbon::now(config('app.timezone'))->startOfWeek();
            //$forteen_days_ago = Carbon::now(config('app.timezone'))->subWeeks(2);

            $this_week_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $seven_days_ago->toDateString())
                ->get();

            //$last_week_orders = Order::where('order_status', 'confirmed')
            //    ->whereDate('created_at', '>=', $forteen_days_ago->toDateString())
            //    ->whereDate('created_at', '<', $seven_days_ago->toDateString())
            //    ->get();

            /*
             * Rearrange this week orders with chart format
             */
            $this_week_values = array();
            $now = Carbon::now(config('app.timezone'));
            for ($i = 0; $i < 7; $i++) {
                $dt = $now->copy()->startOfWeek();
                if ($i !== 0) {
                    $dt->addDays($i);
                }


                $this_week_values[$i] = array(
                    "key" => "This Week",
                    "series" => 1,
                    "size" => 0,
                    "x" => $dt->format('jS M'),
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($this_week_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $this_week_values[$dt->dayOfWeek]["x"] = $dt->format('jS M');
                $this_week_values[$dt->dayOfWeek]["size"]++;
                $this_week_values[$dt->dayOfWeek]["y"] = $this_week_values[$dt->dayOfWeek]["y"] + $order->paid_total_price;
                $this_week_values[$dt->dayOfWeek]["y1"] = $this_week_values[$dt->dayOfWeek]["y1"] + $order->paid_total_price;
            }

            /*
             * Rearrange last week orders with chart format
             */
            //$last_week_values = array();
            //for ($i = 1; $i < 8; $i++) {
            //    $last_week_values[$i] = array(
            //        "key" => "Last Week",
            //        "series" => 1,
            //        "size" => 0,
            //        "x" => $i,
            //        "y" => 0,
            //        "y0" => 0,
            //        "y1" => 0
            //    );
            //}
            //
            //foreach ($last_week_orders as $order) {
            //    $dt = Carbon::parse($order->created_at);
            //    $last_week_values[$dt->dayOfWeek]["size"]++;
            //    $last_week_values[$dt->dayOfWeek]["y"] = $last_week_values[$dt->dayOfWeek]["y"] + $order->paid_total_price;
            //    $last_week_values[$dt->dayOfWeek]["y1"] = $last_week_values[$dt->dayOfWeek]["y1"] + $order->paid_total_price;
            //}

            $daily_sales = [
                [
                    "key" => 'This Week',
                    "values" => array_values($this_week_values)
                ],
                //[
                //    "key" => 'Last Week',
                //    "values" => array_values($last_week_values)
                //]
            ];

            return response()->json(['status' => 'ok', 'data' => $daily_sales]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getQuarterlySales()
    {
        try {
            $this_year_period = Carbon::now(config('app.timezone'))->subYear();
            $last_year_period = Carbon::now(config('app.timezone'))->subYear(2);

            $this_year_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $this_year_period->toDateString())
                ->get();

            $last_year_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $last_year_period->toDateString())
                ->whereDate('created_at', '<', $this_year_period->toDateString())
                ->get();


            $this_quarter_values = array();
            for ($i = 1; $i < 5; $i++) {
                $this_quarter_values[$i] = array(
                    "key" => "This Year",
                    "series" => 1,
                    "size" => 0,
                    "x" => $i,
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($this_year_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $this_quarter_values[$dt->quarter]["size"]++;
                $this_quarter_values[$dt->quarter]["y"] = $this_quarter_values[$dt->quarter]["y"] + $order->paid_total_price;
                $this_quarter_values[$dt->quarter]["y1"] = $this_quarter_values[$dt->quarter]["y1"] + $order->paid_total_price;
            }

            $last_quarter_values = array();
            for ($i = 1; $i < 5; $i++) {
                $last_quarter_values[$i] = array(
                    "key" => "Last Year",
                    "series" => 1,
                    "size" => 0,
                    "x" => $i,
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($last_year_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $last_quarter_values[$dt->quarter]["size"]++;
                $last_quarter_values[$dt->quarter]["y"] = $last_quarter_values[$dt->quarter]["y"] + $order->paid_total_price;
                $last_quarter_values[$dt->quarter]["y1"] = $last_quarter_values[$dt->quarter]["y1"] + $order->paid_total_price;
            }

            $quarter_sales = [
                [
                    "key" => 'This Year',
                    "values" => array_values($this_quarter_values)
                ],
                [
                    "key" => 'Last Year',
                    "values" => array_values($last_quarter_values)
                ]
            ];

            return response()->json(['status' => 'ok', 'data' => $quarter_sales]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getYearlySales()
    {
        try {
            $this_year_period = Carbon::now(config('app.timezone'));
            $last_two_year_period = Carbon::now(config('app.timezone'))->subYear(3);

            $last_two_year_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $last_two_year_period->toDateString())
                ->whereDate('created_at', '<=', $this_year_period->toDateString())
                ->get();

            $diff_in_years = $last_two_year_period->diffInYears($this_year_period);

            $this_yearly_values = array();
            for ($i = $diff_in_years; $i >= 0; $i--) {
                $now = Carbon::now()->subYears($i);
                $this_yearly_values[$now->year] = array(
                    "key" => "Year",
                    "series" => 1,
                    "size" => 0,
                    "x" => $now->year,
                    "y" => 0,
                    "y0" => 0,
                    "y1" => 0
                );
            }

            foreach ($last_two_year_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $this_yearly_values[$dt->year]["size"]++;
                $this_yearly_values[$dt->year]["y"] = $this_yearly_values[$dt->year]["y"] + $order->paid_total_price;
                $this_yearly_values[$dt->year]["y1"] = $this_yearly_values[$dt->year]["y1"] + $order->paid_total_price;
            }

            $yearly_sales = [
                [
                    "key" => 'Year',
                    "values" => array_values($this_yearly_values)
                ],
            ];

            return response()->json(['status' => 'ok', 'data' => $yearly_sales]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getQuarterlyYearlySales()
    {
        try {
            $this_year_period = Carbon::now(config('app.timezone'));
            $last_two_year_period = Carbon::now(config('app.timezone'))->subYear(3);

            $last_two_year_orders = Order::where('order_status', 'confirmed')
                ->whereDate('created_at', '>=', $last_two_year_period->toDateString())
                ->whereDate('created_at', '<=', $this_year_period->toDateString())
                ->get();

            $diff_in_years = $last_two_year_period->diffInYears($this_year_period);

            $this_yearly_values = array();

            for ($i = 1; $i < 5; $i++) {
                for ($j = $diff_in_years; $j >= 0; $j--) {
                    $now = Carbon::now()->subYears($j);
                    $this_yearly_values[$i][$now->year] = array(
                        "key" => "Q" . $i,
                        "series" => 1,
                        "size" => 0,
                        "x" => $now->year,
                        "y" => 0,
                        "y0" => 0,
                        "y1" => 0
                    );
                }
            }


            foreach ($last_two_year_orders as $order) {
                $dt = Carbon::parse($order->created_at);
                $this_yearly_values[$dt->quarter][$dt->year]["size"]++;
                $this_yearly_values[$dt->quarter][$dt->year]["y"] = $this_yearly_values[$dt->quarter][$dt->year]["y"] + $order->paid_total_price;
                $this_yearly_values[$dt->quarter][$dt->year]["y1"] = $this_yearly_values[$dt->quarter][$dt->year]["y1"] + $order->paid_total_price;
            }

            $yearly_sales = array();
            foreach ($this_yearly_values as $key => $value) {
                $yearly_sales[] = [
                    "key" => 'Q' . $key,
                    "values" => array_values($value)
                ];
            }

            return response()->json(['status' => 'ok', 'data' => $yearly_sales]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getJoinedMembers(Request $request)
    {
        try {
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            Carbon::setWeekStartsAt(Carbon::SUNDAY);

            $venue_id = 0;
            if ($request->has('venue_id')) {
                $venue_id = $request->input('venue_id');
            }

            if ($request->has('type')) {

                if ($request->input('type') == 'this_year') {
                    $this_year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $this_year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfYear()->toDateString())
                            ->get();
                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '>=', $this_year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfYear()->toDateString())
                            ->get();
                    }

                    $this_year_values = array();
                    for ($i = 1; $i < 13; $i++) {
                        $this_year_values[$i] = array(
                            "label" => $this->getMonth($i),
                            "value" => 0
                        );
                    }

                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->month]["value"]++;
                    }

                    $monthly_members = [
                        [
                            "key" => 'This Year',
                            "values" => array_values($this_year_values)
                        ]
                    ];

                } elseif ($request->input('type') == 'this_month') {
                    $this_year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    }

                    $total_days = $this_year->copy()->endOfMonth()->diffInDays($this_year->copy()->startOfMonth());                         // 31

                    $this_year_values = array();
                    for ($i = 1; $i < $total_days + 2; $i++) {
                        $this_year_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->day]["value"]++;
                    }

                    $suggested_values = array(
                        7 => array(
                            "label" => '1st - 7th',
                            "value" => 0
                        ),
                        14 => array(
                            "label" => '8th - 14th',
                            "value" => 0
                        ),
                        21 => array(
                            "label" => '15th - 21st',
                            "value" => 0
                        ),
                        31 => array(
                            "label" => '22nd - END',
                            "value" => 0
                        )
                    );


                    foreach ($suggested_values as $k => &$v) {
                        foreach ($this_year_values as $key => &$value) {
                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }
                        }
                    }


                    $monthly_members = [
                        [
                            "key" => 'This Month',
                            "values" => array_values($suggested_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'last_month') {
                    $this_year = Carbon::now(config('app.timezone'))->subMonth();

                    if (intval($venue_id) !== 0) {
                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    }

                    $total_days = $this_year->copy()->endOfMonth()->diffInDays($this_year->copy()->startOfMonth());                         // 31

                    $this_year_values = array();
                    for ($i = 1; $i < $total_days + 2; $i++) {
                        $this_year_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->day]["value"]++;
                    }

                    $suggested_values = array(
                        7 => array(
                            "label" => '1st - 7th',
                            "value" => 0
                        ),
                        14 => array(
                            "label" => '8th - 14th',
                            "value" => 0
                        ),
                        21 => array(
                            "label" => '15th - 21st',
                            "value" => 0
                        ),
                        31 => array(
                            "label" => '22nd - END',
                            "value" => 0
                        )
                    );


                    foreach ($suggested_values as $k => &$v) {
                        foreach ($this_year_values as $key => &$value) {
                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }
                        }
                    }


                    $monthly_members = [
                        [
                            "key" => 'Last Month',
                            "values" => array_values($suggested_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'this_week') {
                    $this_year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $this_year->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfWeek()->toDateString())
                            ->get();
                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '>=', $this_year->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfWeek()->toDateString())
                            ->get();
                    }

                    $this_year_values = array();
                    for ($i = 0; $i < 7; $i++) {
                        $this_year_values[$i] = array(
                            "label" => $this->getDayofWeek($i),
                            "value" => 0
                        );
                    }

                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->dayOfWeek]["value"]++;
                    }

                    $monthly_members = [
                        [
                            "key" => 'This Week',
                            "values" => array_values($this_year_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'last_week') {
                    $this_year = Carbon::now(config('app.timezone'))->subWeek();

                    if (intval($venue_id) !== 0) {

                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $this_year->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfWeek()->toDateString())
                            ->get();

                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '>=', $this_year->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfWeek()->toDateString())
                            ->get();

                    }


                    $this_year_values = array();
                    for ($i = 0; $i < 7; $i++) {
                        $this_year_values[$i] = array(
                            "label" => $this->getDayofWeek($i),
                            "value" => 0
                        );
                    }

                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->dayOfWeek]["value"]++;
                    }

                    $monthly_members = [
                        [
                            "key" => 'Last Week',
                            "values" => array_values($this_year_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'today') {
                    $this_year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {

                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '=', $this_year->toDateString())
                            ->get();
                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '=', $this_year->toDateString())
                            ->get();

                    }


                    $this_year_values = array();
                    for ($i = 0; $i < 24; $i++) {
                        $this_year_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->hour]["value"]++;
                    }


                    $suggested_values = array(
                        6 => array(
                            "label" => '00.00 - 05.59',
                            "value" => 0
                        ),
                        12 => array(
                            "label" => '06.00 - 11.59',
                            "value" => 0
                        ),
                        18 => array(
                            "label" => '12.00 - 17.59',
                            "value" => 0
                        ),
                        24 => array(
                            "label" => '18.00 - 23.59',
                            "value" => 0
                        )
                    );


                    foreach ($suggested_values as $k => &$v) {
                        foreach ($this_year_values as $key => &$value) {
                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }
                        }
                    }

                    $monthly_members = [
                        [
                            "key" => 'Today',
                            "values" => array_values($suggested_values)
                        ]
                    ];

                } elseif ($request->input('type') == 'yesterday') {
                    $this_year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {

                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '=', $this_year->toDateString())
                            ->get();

                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '=', $this_year->toDateString())
                            ->get();
                    }

                    $this_year_values = array();
                    for ($i = 0; $i < 24; $i++) {
                        $this_year_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->hour]["value"]++;
                    }

                    $suggested_values = array(
                        6 => array(
                            "label" => '00.00 - 05.59',
                            "value" => 0
                        ),
                        12 => array(
                            "label" => '06.00 - 11.59',
                            "value" => 0
                        ),
                        18 => array(
                            "label" => '12.00 - 17.59',
                            "value" => 0
                        ),
                        24 => array(
                            "label" => '18.00 - 23.59',
                            "value" => 0
                        )
                    );

                    foreach ($suggested_values as $k => &$v) {
                        foreach ($this_year_values as $key => &$value) {
                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }
                        }
                    }

                    $monthly_members = [
                        [
                            "key" => 'Yesterday',
                            "values" => array_values($suggested_values)
                        ]
                    ];

                } elseif ($request->input('type') == 'custom') {

                    $date_start = Carbon::now(config('app.timezone'));
                    $date_end = Carbon::now(config('app.timezone'));

                    if ($request->has('extra')) {
                        $payload = \GuzzleHttp\json_decode($request->input('extra'));
                        $date_start = Carbon::parse($payload->date_start);
                        $date_end = Carbon::parse($payload->date_end);
                    }

                    if (intval($venue_id) !== 0) {

                        $this_year_members = User::has('member')
                            ->whereHas('member.member_tier.tier', function ($query) use ($venue_id) {
                                $query->where('venue_id', $venue_id);
                            })
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->get();

                    } else {
                        $this_year_members = User::has('member')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->get();
                    }

                    $this_year_values = array();
                    for ($date = $date_start; $date->lte($date_end); $date->addDay()) {
                        $this_year_values[$date->toDateString()] = array(
                            "label" => $date->format('jS M'),
                            "value" => 0
                        );
                    }


                    foreach ($this_year_members as $member) {
                        $dt = Carbon::parse($member->created_at);
                        $this_year_values[$dt->toDateString()]["value"]++;
                    }

                    $monthly_members = [
                        [
                            "key" => 'Custom',
                            "values" => array_values($this_year_values)
                        ]
                    ];
                }


            } else {
                $this_year = Carbon::now(config('app.timezone'));

                $this_year_members = User::has('member')
                    ->whereDate('created_at', '>=', $this_year->startOfYear()->toDateString())
                    ->whereDate('created_at', '<=', $this_year->endOfYear()->toDateString())
                    ->get();

                $this_year_values = array();
                for ($i = 1; $i < 13; $i++) {
                    $this_year_values[$i] = array(
                        "label" => $this->getMonth($i),
                        "value" => 0
                    );
                }

                foreach ($this_year_members as $member) {
                    $dt = Carbon::parse($member->created_at);
                    $this_year_values[$dt->month]["value"]++;
                }

                $monthly_members = [
                    [
                        "key" => 'This Year',
                        "values" => array_values($this_year_values)
                    ]
                ];

            }

            return response()->json(['status' => 'ok', 'data' => $monthly_members]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getTransactionsThisMonth(Request $request)
    {
        try {
            Carbon::setWeekEndsAt(Carbon::SATURDAY);
            Carbon::setWeekStartsAt(Carbon::SUNDAY);

            $venue_id = 0;
            if ($request->has('venue_id')) {
                $venue_id = $request->input('venue_id');
            }

            if ($request->has('type')) {

                if ($request->input('type') == 'this_year') {
                    $this_year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $this_year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfYear()->toDateString())
                            ->get();
                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $this_year->startOfYear()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfYear()->toDateString())
                            ->get();
                    }

                    $this_week_values = array();
                    for ($i = 1; $i < 13; $i++) {
                        $this_week_values[$i] = array(
                            "label" => $this->getMonth($i),
                            "value" => 0
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->month]["value"] = $this_week_values[$dt->month]["value"] + $order->paid_total_price;
                    }

                    $daily_sales = [
                        [
                            "key" => 'This Year',
                            "values" => array_values($this_week_values)
                        ]
                    ];


                } elseif ($request->input('type') == 'this_month') {
                    $this_year = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    }

                    $total_days = $this_year->copy()->endOfMonth()->diffInDays($this_year->copy()->startOfMonth());                         // 31


                    $this_week_values = array();
                    for ($i = 1; $i < $total_days + 2; $i++) {
                        $this_week_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->day]["value"] = $this_week_values[$dt->day]["value"] + $order->paid_total_price;
                    }

                    $suggested_values = array(
                        7 => array(
                            "label" => '1st - 7th',
                            "value" => 0
                        ),
                        14 => array(
                            "label" => '8th - 14th',
                            "value" => 0
                        ),
                        21 => array(
                            "label" => '15th - 21st',
                            "value" => 0
                        ),
                        31 => array(
                            "label" => '22nd - END',
                            "value" => 0
                        )
                    );


                    foreach ($suggested_values as $k => &$v) {
                        foreach ($this_week_values as $key => &$value) {
                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }
                        }
                    }

                    $daily_sales = [
                        [
                            "key" => 'This Month',
                            "values" => array_values($suggested_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'last_month') {
                    $this_year = Carbon::now(config('app.timezone'))->subMonth();

                    if (intval($venue_id) !== 0) {

                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $this_year->startOfMonth()->toDateString())
                            ->whereDate('created_at', '<=', $this_year->endOfMonth()->toDateString())
                            ->get();
                    }

                    $total_days = $this_year->copy()->endOfMonth()->diffInDays($this_year->copy()->startOfMonth());                         // 31


                    $this_week_values = array();
                    for ($i = 1; $i < $total_days + 2; $i++) {
                        $this_week_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->day]["value"] = $this_week_values[$dt->day]["value"] + $order->paid_total_price;
                    }

                    $suggested_values = array(
                        7 => array(
                            "label" => '1st - 7th',
                            "value" => 0
                        ),
                        14 => array(
                            "label" => '8th - 14th',
                            "value" => 0
                        ),
                        21 => array(
                            "label" => '15th - 21st',
                            "value" => 0
                        ),
                        31 => array(
                            "label" => '22nd - END',
                            "value" => 0
                        )
                    );


                    foreach ($suggested_values as $k => &$v) {
                        foreach ($this_week_values as $key => &$value) {
                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }
                        }
                    }

                    $daily_sales = [
                        [
                            "key" => 'Last Month',
                            "values" => array_values($suggested_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'this_week') {
                    $this_month = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {

                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $this_month->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_month->endOfWeek()->toDateString())
                            ->get();

                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $this_month->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_month->endOfWeek()->toDateString())
                            ->get();
                    }

                    $this_week_values = array();
                    for ($i = 0; $i < 7; $i++) {
                        $this_week_values[$i] = array(
                            "label" => $this->getDayofWeek($i),
                            "value" => 0
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->dayOfWeek]["value"] = $this_week_values[$dt->dayOfWeek]["value"] + $order->paid_total_price;
                    }

                    $daily_sales = [
                        [
                            "key" => 'This Week',
                            "values" => array_values($this_week_values)
                        ]
                    ];

                } elseif ($request->input('type') == 'last_week') {
                    $this_month = Carbon::now(config('app.timezone'))->subWeek();

                    if (intval($venue_id) !== 0) {

                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $this_month->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_month->endOfWeek()->toDateString())
                            ->get();

                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $this_month->startOfWeek()->toDateString())
                            ->whereDate('created_at', '<=', $this_month->endOfWeek()->toDateString())
                            ->get();

                    }

                    $this_week_values = array();
                    for ($i = 0; $i < 7; $i++) {
                        $this_week_values[$i] = array(
                            "label" => $this->getDayofWeek($i),
                            "value" => 0
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->dayOfWeek]["value"] = $this_week_values[$dt->dayOfWeek]["value"] + $order->paid_total_price;
                    }

                    $daily_sales = [
                        [
                            "key" => 'This Week',
                            "values" => array_values($this_week_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'today') {
                    $today = Carbon::now(config('app.timezone'));

                    if (intval($venue_id) !== 0) {

                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->get();

                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->get();

                    }

                    $this_week_values = array();
                    for ($i = 0; $i < 24; $i++) {
                        $this_week_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->hour]["value"] = $this_week_values[$dt->hour]["value"] + $order->paid_total_price;
                    }

                    $suggested_values = array(
                        6 => array(
                            "label" => '00.00 - 05.59',
                            "value" => 0
                        ),
                        12 => array(
                            "label" => '06.00 - 11.59',
                            "value" => 0
                        ),
                        18 => array(
                            "label" => '12.00 - 17.59',
                            "value" => 0
                        ),
                        24 => array(
                            "label" => '18.00 - 23.59',
                            "value" => 0
                        )
                    );

                    foreach ($suggested_values as $k => &$v) {

                        foreach ($this_week_values as $key => &$value) {

                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }

                        }

                    }


                    $daily_sales = [
                        [
                            "key" => 'Today',
                            "values" => array_values($suggested_values)
                        ]
                    ];
                } elseif ($request->input('type') == 'yesterday') {
                    $today = Carbon::now(config('app.timezone'))->subDay();

                    if (intval($venue_id) !== 0) {

                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->get();

                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '=', $today->toDateString())
                            ->get();

                    }

                    $this_week_values = array();
                    for ($i = 0; $i < 24; $i++) {
                        $this_week_values[$i] = array(
                            "label" => $i,
                            "value" => 0,
                            "check_in" => false
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->hour]["value"] = $this_week_values[$dt->hour]["value"] + $order->paid_total_price;
                    }

                    $suggested_values = array(
                        6 => array(
                            "label" => '00.00 - 05.59',
                            "value" => 0
                        ),
                        12 => array(
                            "label" => '06.00 - 11.59',
                            "value" => 0
                        ),
                        18 => array(
                            "label" => '12.00 - 17.59',
                            "value" => 0
                        ),
                        24 => array(
                            "label" => '18.00 - 23.59',
                            "value" => 0
                        )
                    );

                    foreach ($suggested_values as $k => &$v) {
                        foreach ($this_week_values as $key => &$value) {

                            if ($key < $k && $value['check_in'] == false) {
                                $v["value"] += $value["value"];
                                $value['check_in'] = true;
                            }

                        }
                    }


                    $daily_sales = [
                        [
                            "key" => 'Yesterday',
                            "values" => array_values($suggested_values)
                        ]
                    ];

                } elseif ($request->input('type') == 'custom') {

                    $date_start = Carbon::now(config('app.timezone'));
                    $date_end = Carbon::now(config('app.timezone'));

                    if ($request->has('extra')) {
                        $payload = \GuzzleHttp\json_decode($request->input('extra'));
                        $date_start = Carbon::parse($payload->date_start);
                        $date_end = Carbon::parse($payload->date_end);
                    }

                    if (intval($venue_id) !== 0) {

                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->where('venue_id', $venue_id)
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->get();

                    } else {
                        $this_week_orders = Order::where('order_status', 'confirmed')
                            ->whereDate('created_at', '>=', $date_start->startOfDay()->toDateString())
                            ->whereDate('created_at', '<=', $date_end->endOfDay()->toDateString())
                            ->get();

                    }

                    $this_week_values = array();
                    for ($date = $date_start; $date->lte($date_end); $date->addDay()) {
                        $this_week_values[$date->toDateString()] = array(
                            "label" => $date->format('jS M'),
                            "value" => 0
                        );
                    }

                    foreach ($this_week_orders as $order) {
                        $dt = Carbon::parse($order->created_at);
                        $this_week_values[$dt->toDateString()]["value"] = $this_week_values[$dt->toDateString()]["value"] + $order->paid_total_price;
                    }

                    $daily_sales = [
                        [
                            "key" => 'Custom',
                            "values" => array_values($this_week_values)
                        ]
                    ];
                }

            } else {
                $this_month = Carbon::now(config('app.timezone'))->day(1);

                $this_week_orders = Order::where('order_status', 'confirmed')
                    ->whereDate('created_at', '>=', $this_month->toDateString())
                    ->get();

                $this_week_values = array();
                for ($i = 0; $i < 7; $i++) {
                    $this_week_values[$i] = array(
                        "label" => $this->getDayofWeek($i),
                        "value" => 0
                    );
                }

                foreach ($this_week_orders as $order) {
                    $dt = Carbon::parse($order->created_at);
                    $this_week_values[$dt->dayOfWeek]["value"] = $this_week_values[$dt->dayOfWeek]["value"] + $order->paid_total_price;
                }

                $daily_sales = [
                    [
                        "key" => 'This Week',
                        "values" => array_values($this_week_values)
                    ]
                ];
            }


            return response()->json(['status' => 'ok', 'data' => $daily_sales]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    private function getDayofWeek($d)
    {
        switch ($d) {
            case 0:
                return 'SUN';
            case 1:
                return 'MON';
            case 2:
                return 'TUE';
            case 3:
                return 'WED';
            case 4:
                return 'THU';
            case 5:
                return 'FRI';
            case 6:
                return 'SAT';
            default:
                return '';
        }
    }

    protected function retrievePromotions()
    {
        try {
            $listings = Listing::withCount(['order_details' => function ($query) {
                $query->where('status', 'successful');
            }])->where('listing_type_id', 4)->get();

            foreach ($listings as $listing) {
                $count = ClaimedPromotion::where('listing_id', $listing->id)->where('is_used', 1)->count();
                $listing->scanned = $count;
            }

            if (count($listings) < 10) {
                $total = 10 - count($listings);
                for ($i = 0; $i < $total; $i++) {
                    $listings[] = array(
                        "heading" => 'Empty Slot #' . $i,
                        "order_details_count" => 0,
                        "scanned" => 0
                    );
                }
            }

            return response()->json(['status' => 'ok', 'data' => $listings]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getWeeklyEventCounts()
    {
        try {
            $listings = Listing::where('listing_type_id', 2)
                ->orderBy('click_count', 'desc')->take(6)->get();

            $values = array();
            foreach ($listings as $listing) {
                $values[] = array(
                    "label" => $listing->heading,
                    "value" => $listing->click_count
                );
            }

            if (count($listings) < 6) {
                $total = 6 - count($listings);
                for ($i = 0; $i < $total; $i++) {
                    $values[] = array(
                        "label" => 'Empty Slot #' . $i,
                        "value" => 0
                    );
                }
            }

            $counts = [
                [
                    "key" => "Clicks",
                    "color" => "#1f77b4",
                    "values" => array_values($values)
                ]
            ];

            return response()->json(['status' => 'ok', 'data' => $counts]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getSpecialEventCounts()
    {
        try {
            $listings = Listing::where('listing_type_id', 1)
                ->orderBy('click_count', 'desc')->take(6)->get();

            $values = array();
            foreach ($listings as $listing) {
                $values[] = array(
                    "label" => $listing->heading,
                    "value" => $listing->click_count
                );
            }

            if (count($listings) < 6) {
                $total = 6 - count($listings);
                for ($i = 0; $i < $total; $i++) {
                    $values[] = array(
                        "label" => 'Empty Slot #' . $i,
                        "value" => 0
                    );
                }
            }

            $counts = [
                [
                    "key" => "Clicks",
                    "color" => "#1f77b4",
                    "values" => array_values($values)
                ]
            ];

            return response()->json(['status' => 'ok', 'data' => $counts]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getListingCounts(Request $request)
    {
        try {

            $venue_id = 0;
            if ($request->has('venue_id')) {
                $venue_id = $request->input('venue_id');
            }

            if ($request->has('id')) {
                if ($request->input('id') !== '*') {

                    if (intval($venue_id) !== 0) {
                        $listings = Listing::where('listing_type_id', $request->input('id'))
                            ->where('venue_id', $venue_id)
                            ->orderBy('click_count', 'desc')
                            ->take(6)
                            ->get();
                    } else {
                        $listings = Listing::where('listing_type_id', $request->input('id'))
                            ->orderBy('click_count', 'desc')
                            ->take(6)
                            ->get();
                    }

                } else {

                    if (intval($venue_id) !== 0) {
                        $listings = Listing::orderBy('click_count', 'desc')
                            ->where(function ($query) {
                                $query->where('listing_type_id', 1);
                                $query->orWhere('listing_type_id', 2);
                            })
                            ->where('status', 'active')
                            ->where('venue_id', $venue_id)
                            ->take(6)->get();
                    } else {
                        $listings = Listing::orderBy('click_count', 'desc')
                            ->where(function ($query) {
                                $query->where('listing_type_id', 1);
                                $query->orWhere('listing_type_id', 2);
                            })
                            ->where('status', 'active')
                            ->take(6)->get();
                    }
                }
                $type = $request->input('id');

            } else {
                $listings = Listing::orderBy('click_count', 'desc')
                    ->where('status', 'active')
                    ->take(6)->get();
                $type = '*';
            }

            $values = array();
            foreach ($listings as $listing) {
                $values[] = array(
                    "label" => $listing->id . '. ' . $listing->name,
                    "value" => $listing->click_count
                );
            }

            if (count($listings) < 6) {
                $total = 6 - count($listings);
                for ($i = 0; $i < $total; $i++) {
                    $values[] = array(
                        "label" => 'Empty Slot #' . $i,
                        "value" => 0
                    );
                }
            }

            $counts = [
                [
                    "key" => "Clicks",
                    "color" => "#1f77b4",
                    "values" => array_values($values)
                ]
            ];

            return response()->json(['status' => 'ok', 'data' => $counts, 'type' => $type]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getExportReport(Request $request)
    {
        try {
            $venue_id = 0;
            if ($request->has('venue_id')) {
                $venue_id = $request->input('venue_id');
            }

            if ($request->has('id')) {
                if ($request->input('id') !== '*') {

                    if (intval($venue_id) !== 0) {
                        $listings = Listing::where('listing_type_id', $request->input('id'))
                            ->where('venue_id', $venue_id)
                            ->orderBy('click_count', 'desc')
                            ->get();
                    } else {
                        $listings = Listing::where('listing_type_id', $request->input('id'))
                            ->orderBy('click_count', 'desc')
                            ->get();
                    }

                } else {

                    if (intval($venue_id) !== 0) {
                        $listings = Listing::orderBy('click_count', 'desc')
                            ->where(function ($query) {
                                $query->where('listing_type_id', 1);
                                $query->orWhere('listing_type_id', 2);
                            })
                            ->where('status', 'active')
                            ->where('venue_id', $venue_id)->get();
                    } else {
                        $listings = Listing::orderBy('click_count', 'desc')
                            ->where(function ($query) {
                                $query->where('listing_type_id', 1);
                                $query->orWhere('listing_type_id', 2);
                            })
                            ->where('status', 'active')->get();
                    }
                }
                $type = $request->input('id');

            } else {
                $listings = Listing::orderBy('click_count', 'desc')
                    ->where('status', 'active')->get();
                $type = '*';
            }

            $values = array();
            foreach ($listings as $listing) {
                $listing_type = "";
                if( $listing->listing_type_id == 1){
                    $listing_type = "Special Events";
                } else if( $listing->listing_type_id == 2){
                    $listing_type = "Regular Events";
                }

                $values[] = array(
                    "id" => $listing->id,
                    "name" => $listing->name,
                    "type" => $listing_type,
                    "status" => $listing->status,
                    "value" => $listing->click_count
                );
            }

            //echo print_r($values);

            $counts = [
                [
                    "key" => "Clicks",
                    "color" => "#1f77b4",
                    "values" => array_values($values)
                ]
            ];
            
            $export_option = ["ALL Event", "Special Event", "Regular Event" ];
            $option = $request->input('id') == '*' ? 0 : $request->input('id');
            $today = Carbon::now(config('app.timezone'));

            return Excel::download(new ExportListing($values), $export_option[$option] .' Total Clicks '. $today.'.xlsx');

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    protected function getExportPromotions(Request $request)
    {
        try {
            $venue_id = 0;
            if ($request->has('venue_id')) {
                $venue_id = $request->input('venue_id');
            }

            $listings = Listing::withCount(['order_details' => function ($query) {
                $query->where('status', 'successful');
            }])->where('listing_type_id', 4)->get();
            
            $values = array();
            foreach ($listings as $listing) {
                $count = ClaimedPromotion::where('listing_id', $listing->id)->where('is_used', 1)->count();
                $values[] = array(
                    "id" => $listing->id,
                    "name" => $listing->name,
                    "type" => "Promotion",
                    "status" => $listing->status,
                    "order_details_count" => $listing->order_details_count,
                    "scanned" => $count
                );
            }

            //echo print_r($values);

            $counts = [
                [
                    "key" => "Clicks",
                    "color" => "#1f77b4",
                    "values" => array_values($values)
                ]
            ];

            $today = Carbon::now(config('app.timezone'));
            
            return Excel::download(new ExportPromotion($values), 'Promotion Activity Report '. $today.'.xlsx');

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    function getMonth($i)
    {
        switch ($i) {
            case 1:
                return 'Jan';
            case 2:
                return 'Feb';
            case 3:
                return 'Mar';
            case 4:
                return 'Apr';
            case 5:
                return 'May';
            case 6:
                return 'Jun';
            case 7:
                return 'Jul';
            case 8:
                return 'Aug';
            case 9:
                return 'Sep';
            case 10:
                return 'Oct';
            case 11:
                return 'Nov';
            case 12:
                return 'Dec';
        }
    }
}
