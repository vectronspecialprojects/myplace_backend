<?php

namespace App\Http\Controllers\Admin;

use App\Listing;
use App\ListingProduct;
use App\ListingSchedule;
use App\ListingScheduleTag;
use App\ListingScheduleType;
use App\ListingScheduleVenue;
use App\ListingType;
use App\ListingPivotTag;
use App\ListingTag;
use App\OrderDetail;
use App\Product;
use App\ProductType;
use App\SystemLog;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Return all products
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request, Product $product)
    {
        try {

            $obj = $product->newQuery();

            // if ($request->has('type_id')) {
            //     $type = ProductType::find($request->input('type_id'));

            //     if (is_null($type)) {
            //         return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            //     }

            //     $obj->where('product_type_id', $type->id);
            // }

            if ($request->has('filter')) {
                if ($request->input('filter') === 'ticket') {
                    $pt = ProductType::where('key', 'like', '%"type":"ticket"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();

                    $obj->where('product_type_id', $pt->id);
                } elseif ($request->input('filter') === 'voucher') {
                    $pt = ProductType::where('key', 'like', '%"type":"ticket"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();

                    $obj->where('product_type_id', '<>', $pt->id);
                    // $pt = ProductType::where('key', 'like', '%"type":"voucher"%')
                    //     ->where('key', 'like', '%"source":"bepoz"%')->first();

                    // $obj->where('product_type_id', $pt->id);
                }
                // if ($request->input('filter') !== 'none') {
                //     $obj->where('product_type_id', $request->input('filter'));
                // }
            }

            if ($request->has('status')) {
                $obj->where('status', $request->input('status'));
            }

            if ($request->has('show_hidden')) {
                if (intval($request->input('show_hidden')) === 0) {
                    $obj->where('is_hidden', 0);
                }
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    $obj->where('products.status', 'active');
                }
            }


            $products = $obj->with('product_type')
                ->get();

            if (is_null($products)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $products]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all products (paginate)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateProduct(Request $request, Product $product)
    {
        try {
            $obj = $product->newQuery();

            if ($request->has('keyword')) {

                $obj->orWhere(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }
                });

            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            if ($request->has('show_hidden')) {
                if (intval($request->input('show_hidden')) === 0) {
                    $obj->where('is_hidden', 0);
                }
            }

            if ($request->has('venue_id')) {
                if (intval($request->input('venue_id')) !== 0) {
                    $obj->where('venue_id', $request->input('venue_id'));
                }
            }

            if ($request->has('filter')) {
                if ($request->input('filter') === 'ticket') {
                    $pt = ProductType::where('key', 'like', '%"type":"ticket"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();

                    $obj->where('product_type_id', $pt->id);
                } elseif ($request->input('filter') === 'voucher') {
                    // $pt = ProductType::where('key', 'like', '%"type":"ticket"%')
                    //     ->where('key', 'like', '%"source":"bepoz"%')->first();

                    // $obj->where('product_type_id', '<>', $pt->id);
                    $pt = ProductType::where('key', 'like', '%"type":"voucher"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();

                    $obj->where('product_type_id', $pt->id);
                } elseif ($request->input('filter') === 'product') {
                    $pt = ProductType::where('key', 'like', '%"type":"product"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();

                    $obj->where('product_type_id', $pt->id);
                }

                // if ($request->input('filter') !== 'none') {
                //     $obj->where('product_type_id', $request->input('filter'));
                // }
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    $obj->where('products.status', 'active');
                }
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $products = $obj->with('product_type')
                ->paginate($paginate_number);

            foreach ($products as $product) {
                $sum = OrderDetail::
                    join('orders', 'order_details.order_id', '=', 'orders.id')
                    ->where('order_details.product_id', $product->id)
                    ->where('orders.payment_status', '<>', 'expired')->sum('qty');
                $product->total_sum = is_null($sum) ? 0 : $sum;
            }

            return response()->json(['status' => 'ok', 'data' => $products]);


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific product
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $product = Product::with('product_type', 'voucher_setup')->find($id);

            if (is_null($product)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $product]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for editing product
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function edit($id)
    {
        try {
            $product = Product::find($id);

            if (is_null($product)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $product]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific product
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $product = Product::find($id);

            if (is_null($product)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $od = OrderDetail::where('product_id', $id)->first();
                if (is_null($od)) {
                    DB::beginTransaction();
                    Product::destroy($id);
                    DB::commit();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Not safe. Some objects used this value'], Response::HTTP_BAD_REQUEST);
                }

                return response()->json(['status' => 'ok', 'message' => 'Deleting product is successful.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create copy of product
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function cloneProduct(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'product_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }


            $product = Product::find($request->input('product_id'));

            if (is_null($product)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $new_product = new Product();
            $new_product->name = $product->name . ' - copy';
            $new_product->desc_short = $product->desc_short;
            $new_product->desc_long = $product->desc_long;
            $new_product->image = $product->image;
            $new_product->unit_price = $product->unit_price;
            $new_product->point_price = $product->point_price;
            $new_product->point_get = $product->point_get;
            $new_product->status = $product->status;
            $new_product->system_point_ratio = $product->system_point_ratio;
            $new_product->product_type_id = $product->product_type_id;
            $new_product->is_free = $product->is_free;
            $new_product->key = $product->key;
            $new_product->bepoz_voucher_setup_id = $product->bepoz_voucher_setup_id;
            $new_product->category = $product->category;
            $new_product->bepoz_product_number = $product->bepoz_product_number;
            $new_product->venue_id = $product->venue_id;
            $new_product->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Duplicating product is successful.', 'data' => ['id' => $new_product->id]]);


        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a product
     *
     * @param Request $request
     * @param $id
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id, ImageOptimizer $imageOptimizer)
    {
        try {

            $validator = Validator::make($request->all(), [
                'system_point_ratio' => 'required|boolean'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $product = Product::find($id);

            if ($request->has('name')) {
                $product->name = $request->input('name');
            }

            if ($request->has('desc_short')) {
                $product->desc_short = $request->input('desc_short');
            }

            $product->system_point_ratio = (bool)$request->input('system_point_ratio');

            $product->desc_long = $request->input('desc_long');

            if ($request->has('is_free')) {
                $product->is_free = (bool)$request->input('is_free');
            }

            if ($request->has('is_hidden')) {
                $product->is_hidden = (bool)$request->input('is_hidden');
            }

            if ($request->has('unit_price')) {
                $product->unit_price = $request->input('unit_price');
            }

            if ($request->has('venue_id')) {
                $product->venue_id = $request->input('venue_id');
            } else {
                $product->venue_id = 0;
            }

            if ($request->has('point_price')) {
                $product->point_price = $request->input('point_price');
            }

            if ($request->has('point_get')) {
                $product->point_get = $request->input('point_get');
            }

            if ($request->hasFile('image')) {
                if (config('bucket.s3')){
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('product/' . $product->id . '/image' . '.' . $type, file_get_contents($picture), 'public');
                    $product->image = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/product/" . $product->id . "/image." . $type . "?" . time();
                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'ProductController.php/update';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'ProductController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }


            if ($request->has('status')) {
                $product->status = $request->input('status');
            }

            if ($request->has('product_type_id')) {
                if ($request->input('product_type_id') == 'voucher') {
                    $pt = ProductType::where('key', 'like', '%"type":"voucher"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();
                } elseif ($request->input('product_type_id') == 'fb') {
                    $pt = ProductType::where('key', 'like', '%"type":"voucher"%')
                        ->where('key', 'like', '%"name":"F&B Voucher"%')->first();
                } elseif ($request->input('product_type_id') == 'product') {
                    $pt = ProductType::where('key', 'like', '%"type":"product"%')
                        ->where('key', 'like', '%"name":"Product"%')->first();
                } else {
                    $pt = ProductType::where('key', 'like', '%"type":"ticket"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();
                }
                $product->product_type_id = $pt->id;
            }

            if ($request->has('bepoz_product_id')) {
                $product->bepoz_product_id = $request->input('bepoz_product_id');
            } else {
                $product->bepoz_product_id = null;
            }

            if ($request->has('bepoz_product_number')) {
                if ($request->input('bepoz_product_number') === 'null') {
                    $product->bepoz_product_number = null;
                } else {
                    $product->bepoz_product_number = $request->input('bepoz_product_number');
                }
            } else {
                $product->bepoz_product_number = null;
            }

            if ($request->has('bepoz_voucher_setup_id')) {
                $product->bepoz_voucher_setup_id = $request->input('bepoz_voucher_setup_id');
            } else {
                $product->bepoz_voucher_setup_id = null;
            }

            if ($request->has('bepoz_payment_name')) {
                $product->bepoz_payment_name = $request->input('bepoz_payment_name');
            } else {
                $product->bepoz_payment_name = null;
            }

            if ($request->has('gift_value')) {
                $product->gift_value = $request->input('gift_value');
            } else {
                $product->gift_value = 0;
            }

            if ($request->has('category')) {
                $product->category = $request->input('category');
            } else {
                $product->category = null;
            }

            if ($request->has('payload')) {
                $product->payload = $request->input('payload');
            }

            if ($request->has('product_price_option')) {
                $product->product_price_option = $request->input('product_price_option');
            }

            $product->save();

            // $listingProducts = ListingProduct::with('listing')->where('product_id', $product->id)->get();
            // Log::info($listingProducts);

            // if ($listingProducts){                    
            //     foreach ($listingProducts as $listingProduct){
            //         $listingProduct->listing->dollar_price = $product->unit_price;
            //         $listingProduct->listing->point_price = $product->point_price;
            //         $listingProduct->listing->save();
            //     }
            // }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Updating product is successful.', 'data' => $product]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a product
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request, ImageOptimizer $imageOptimizer)
    {

        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'desc_short' => 'required',
                'point_price' => 'required',
                'system_point_ratio' => 'required|boolean'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $product = new Product;

            $product->name = $request->input('name');

            $product->desc_short = $request->input('desc_short');

            $product->desc_long = $request->input('desc_long');

            $product->point_price = $request->input('point_price');

            $product->system_point_ratio = (bool)$request->input('system_point_ratio');

            if ($request->has('unit_price')) {
                $product->unit_price = $request->input('unit_price');
            }

            if ($request->has('point_get')) {
                $product->point_get = $request->input('point_get');
            }

            if ($request->has('venue_id')) {
                $product->venue_id = $request->input('venue_id');
            }

            if ($request->has('status')) {
                $product->status = $request->input('status');
            }

            if ($request->has('product_type_id')) {
                if ($request->input('product_type_id') == 'voucher') {
                    $pt = ProductType::where('key', 'like', '%"type":"voucher"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();
                } elseif ($request->input('product_type_id') == 'fb') {
                    $pt = ProductType::where('key', 'like', '%"type":"voucher"%')
                        ->where('key', 'like', '%"name":"F&B Voucher"%')->first();
                } elseif ($request->input('product_type_id') == 'product') {
                    $pt = ProductType::where('key', 'like', '%"type":"product"%')
                        ->where('key', 'like', '%"name":"Product"%')->first();
                } else {
                    $pt = ProductType::where('key', 'like', '%"type":"ticket"%')
                        ->where('key', 'like', '%"source":"bepoz"%')->first();
                }
                $product->product_type_id = $pt->id;
            }

            if ($request->has('is_free')) {
                $product->is_free = (bool)$request->input('is_free');
            }

            if ($request->has('is_hidden')) {
                $product->is_hidden = (bool)$request->input('is_hidden');
            }

            if ($request->has('bepoz_product_id')) {
                $product->bepoz_product_id = $request->input('bepoz_product_id');
            }

            if ($request->has('bepoz_voucher_setup_id')) {
                $product->bepoz_voucher_setup_id = $request->input('bepoz_voucher_setup_id');
            }

            if ($request->has('bepoz_product_number')) {
                $product->bepoz_product_number = $request->input('bepoz_product_number');
            }

            if ($request->has('bepoz_payment_name')) {
                $product->bepoz_payment_name = $request->input('bepoz_payment_name');
            }

            if ($request->has('gift_value')) {
                $product->gift_value = $request->input('gift_value');
            }

            if ($request->has('category')) {
                $product->category = $request->input('category');
            }

            if ($request->has('payload')) {
                $product->payload = $request->input('payload');
            }

            if ($request->has('product_price_option')) {
                $product->product_price_option = $request->input('product_price_option');
            }

            $product->save();

            if ($request->hasFile('image')) {
                if (config('bucket.s3')){
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('product/' . $product->id . '/image' . '.' . $type, file_get_contents($picture), 'public');
                    $product->image = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/product/" . $product->id . "/image." . $type . "?" . time();
                    $product->save();
                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'ProductController.php/store';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'ProductController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            DB::commit();

            $product->product_type;
            return response()->json(['status' => 'ok', 'message' => 'Creating product is successful.', 'data' => ['id' => $product->id, 'obj' => $product]]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Search product
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function search(Request $request, Product $product)
    {
        try {
            $product = $product->newQuery();

            if ($request->has('keyword')) {
                $product->where('name', 'like', '%' . $request->input('keyword') . '%');
            }

            if ($request->has('product_type_id')) {
                $product->where('product_type_id', $request->input('product_type_id'));
            }

            if ($request->has('status')) {
                if ($request->input('status') != "any") {
                    $product->where('status', $request->input('status'));
                }
            }

            $products = $product->with('voucher_setup')->get();

            return response()->json(['status' => 'ok', 'data' => $products, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Bulk process of product status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bulk(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'original_ids' => 'array',
                'new_ids' => 'array',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $originals = $request->input("original_ids");
            $new_ids = $request->input("new_ids");

            if (!empty(array_filter($originals))) {

                foreach ($new_ids as $new_id) {

                    if (DB::table('products')->where('id', $new_id)->first()->status == 'inactive') {

                        DB::table('products')->where('id', $new_id)->update(['status' => 'active']);

                    } else {

                        DB::table('products')->where('id', $new_id)->update(['status' => 'inactive']);
                    }

                }

            } else {

                foreach ($new_ids as $new_id) {
                    if (DB::table('products')->where('id', $new_id)->first()->status == 'inactive') {

                        DB::table('products')->where('id', $new_id)->update(['status' => 'active']);

                    } else {

                        DB::table('products')->where('id', $new_id)->update(['status' => 'inactive']);
                    }
                }

            }


            return response()->json(['status' => 'ok', 'message' => "Product changed successfully"]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change product status (individual)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $product = Product::find($request->input('id'));
            $product->status = $request->input('status');
            $product->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing product status is successful.', 'data' => ['id' => $product->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }
}
