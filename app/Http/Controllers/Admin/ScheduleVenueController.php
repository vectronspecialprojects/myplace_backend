<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ListingSchedule;
use App\ScheduleVenue;
use App\Venue;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ScheduleVenueController extends Controller
{
    /**
     * Create a schedule venue
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_schedule_id' => 'required',
                'venue_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request'=> $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listingSchedule = ListingSchedule::find($request->input('listing_schedule_id'));
            if(is_null($listingSchedule)) {
                return response()->json(['status' => 'error', 'message' => 'schedule_not_found'], Response::HTTP_BAD_REQUEST);
            }

            $venue = Venue::find($request->input('venue_id'));
            if(is_null($venue)) {
                return response()->json(['status' => 'error', 'message' => 'venue_not_found'], Response::HTTP_BAD_REQUEST);
            }

            if(is_null(ScheduleVenue::where('listing_schedule_id', $listingSchedule->id)->where('venue_id', $venue->id)->first())){
                $scheduleVenue = new ScheduleVenue();
                $scheduleVenue->listing_schedule_id = $listingSchedule->id;
                $scheduleVenue->venue_id = $venue->id;
                $scheduleVenue->save();
            }

            return response()->json(['status' => 'ok', 'message' => 'Adding schedule & venue is successful.']);
        }
        catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    

}
