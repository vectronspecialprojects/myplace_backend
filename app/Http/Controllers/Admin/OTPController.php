<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Response;
use OTPHP\TOTP;
use ParagonIE\ConstantTime\Base32;

class OTPController extends Controller
{
    function __construct()
    {
        // $this->logArea = \App\LogArea::Where('key', 'admin_log')->first();
        // $this->logAction = null;
        // $this->logError = null;

        try {
            $this->auth_user = \JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth_user = null;
        }
    }

    public function generateOTP()
    {
        try {

            $enable_otp = Setting::where('key', 'enable_otp')->first()->value;
            if (is_null($enable_otp)) {
                return response()->json(['status' => 'error', 'message' => 'OTP setting is broken'], Response::HTTP_BAD_REQUEST);
            }

            $send_otp_sms = Setting::where('key', 'send_otp_sms')->first()->value;
            $request_otp_all_time = Setting::where('key', 'request_otp_all_time')->first()->value;

            $send_sms_now = false;
            if ($enable_otp == 'true'){
                if ($request_otp_all_time == 'true'){
                    $send_sms_now = true;
                } else {
                    if ($this->auth_user->otp_verified) {
                        $send_sms_now = false;
                        return response()->json(['status' => 'ok', 'mode' => 'bypass']);
                    } else {
                        $send_sms_now = true;
                    }
                }

                if ($send_sms_now && $send_otp_sms == 'true'){
                    $encoded = Base32::encode($this->auth_user->email);

                    $otp = TOTP::create(
                        $encoded, // Let the secret be defined by the class
                        30,     // The period (5 seconds)
                        'sha1', // The digest algorithm
                        6    // The output will generate 6 digits
                    );

                    try {
                        $smsFrom = Setting::where('key', 'sms_from')->first()->value;
                    } catch (\Exception $e) {
                        $smsFrom = "Myplace";
                    }

                    try {
                        $burstKey = Setting::where('key', 'sms_burst_key')->first()->value;
                    } catch (\Exception $e) {
                        $burstKey = null;
                    }

                    try {
                        $burstSecret = Setting::where('key', 'sms_burst_secret')->first()->value;
                    } catch (\Exception $e) {
                        $burstSecret = null;
                    }

                    $oo = $otp->now();
                    // $api = new \App\Helpers\TransmitSMSAPI($burstKey, $burstSecret);
                    if (!empty($this->auth_user->mobile)) {
                        $sms_mobile = preg_replace('/^04/', '614', $this->auth_user->mobile);
                        $sms_mobile = preg_replace('/^\+/', '', $sms_mobile);

                        Log::error($oo);

                        // $api->sendSms('Your OTP is ' . $oo, $sms_mobile, substr(preg_replace("/\W/", "", $smsFrom), 0, 11));
                        
                    }

                    return response()->json(['status' => 'ok', 'mode' => 'verify']);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Send OTP via SMS Is disabled']);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'OTP Is disabled']);
            }
            
        } catch (\Exception $e) {
            Log::error($e);
            // \Helper::logUnhandledError($e, []);
            return response()->json(['status' => 'error', 'message' => $e->getMessage() . '[' . $e->getLine() . ']' . $e->getFile()], Response::HTTP_BAD_REQUEST);
        }

    }

    public function verifyOTP(Request $request)
    {
        try {

            $encoded = Base32::encode($this->auth_user->email);

            $otp = TOTP::create(
                $encoded, // Let the secret be defined by the class
                30,     // The period (5 seconds)
                'sha1', // The digest algorithm
                6     // The output will generate 6 digits,
            );

            if(!$request->has('id')){
                return response()->json(['status' => 'error', 'message' => 'OTP is empty'], Response::HTTP_BAD_REQUEST);
            }

            $verified = $otp->verify($request->input('id'));

            if ($verified) {
                $this->auth_user->otp_verified = 1;
                // turn off show qr code, so it will not appear on next login
                $this->auth_user->show_qr_code = false;
                $this->auth_user->save();
            }
            else {
                return response()->json(['status' => 'error', 'message' => 'OTP is invalid'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['verified' => $verified]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage() . '[' . $e->getLine() . ']' . $e->getFile()], Response::HTTP_BAD_REQUEST);
        }


    }
    
    public function requestURI()
    {
        try {

            $encoded = Base32::encode($this->auth_user->email);

            $otp = TOTP::create(
                $encoded, // Let the secret be defined by the class
                30,     // The period (5 seconds)
                'sha1', // The digest algorithm
                6     // The output will generate 6 digits,
            );

            $otp->setLabel($this->auth_user->email);
            $otp->setIssuer('MyPlace by Bepoz Pty Ltd');

            return response()->json(['uri' => $otp->getProvisioningUri()]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage() . '[' . $e->getLine() . ']' . $e->getFile()], Response::HTTP_BAD_REQUEST);
        }
    }

}
