<?php

namespace App\Http\Controllers\Admin;

use App\BepozJob;
use App\Helpers\MandrillExpress;
use App\Http\Controllers\Controller;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\Staff;
use App\SystemLog;
use App\User;
use App\Tier;
use App\Member;
use App\UserRoles;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    /**
     * Return all staff
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $staff = Staff::with('user.roles')
                ->get();

            return response()->json(['status' => 'ok', 'data' => $staff]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return paginated staff
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateStaff(Request $request, Staff $staff)
    {
        try {
            $obj = $staff->newQuery();

            if ($request->has('keyword')) {                
                $obj->orWhere(function ($query) use ($request) {
                    $query->orWhereHas('user', function ($q) use ($request) {
                        $q->where('email', 'like', '%' . $request->input('keyword') . '%');
                    });

                    $query->orWhere(function ($query) use ($request) {
                        if (strpos($request->input('keyword'), ' ') !== false) {
                            $array = explode(" ", $request->input('keyword'));
                            $keyword = implode("%", $array);
                            $query->where('first_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->where('first_name', 'like', '%' . $request->input('keyword') . '%');
                        }
                    });

                    $query->orWhere(function ($query) use ($request) {
                        if (strpos($request->input('keyword'), ' ') !== false) {
                            $array = explode(" ", $request->input('keyword'));
                            $keyword = implode("%", $array);
                            $query->where('last_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->where('last_name', 'like', '%' . $request->input('keyword') . '%');
                        }
                    });

                    $query->orWhereHas('user', function ($q) use ($request) {
                        $q->where('mobile', 'like', '%' . $request->input('keyword') . '%');
                    });

                });

                $obj->orWhere(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));

                        foreach ($array as $element) {
                            $query->orWhereHas('user', function ($q) use ($request, $element) {
                                $q->where('email', 'like', '%' . $element . '%');
                            });

                            $query->orWhere(function ($query) use ($request, $element) {
                                $query->where('first_name', 'like', '%' . $element . '%');
                            });

                            $query->orWhere(function ($query) use ($request, $element) {
                                $query->where('last_name', 'like', '%' . $element . '%');
                            });

                            $query->orWhereHas('user', function ($q) use ($request, $element) {
                                $q->where('mobile', 'like', '%' . $element . '%');
                            });

                        }
                    }
                });

            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    $obj->where('staff.active', true);
                }
            }

            $staff = $obj->with('user.roles')
                ->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $staff]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific staff
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $staff = Staff::with('user')->find($id);

            if (is_null($staff)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $staff]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a new staff
     * >> for admin
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function create(Request $request, ImageOptimizer $imageOptimizer, MandrillExpress $mx)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $first_name = $request->input('first_name');
            if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }

            $last_name = $request->input('last_name');
            if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }

            $new_user = false;
            $user = User::where('email', $request->input('email'))->first();

            // CHECK DOUBLE STAFF
            if (!is_null($user)) {                
                $staff = Staff::where('user_id', $user->id)->first();
                if (!is_null($staff)) {
                    return response()->json(['status' => 'error', 'message' => 'Staff with this email already exist.'], Response::HTTP_BAD_REQUEST);
                }
            }
            
            DB::beginTransaction();
            if (is_null($user)) {
                $user = new User();
                $user->email = $request->input('email');
                // $user->email_confirmation = true;

                $confirmationToken = $user->generateEmailConfirmationToken();

                $new_user = true;

            }

            $user->password = Hash::make($request->input('password'));

            if ($request->has('mobile')) {
                $user->mobile = $request->input('mobile');
            }

            $user->save();

            $staff = new Staff;
            $staff->user_id = $user->id;
            $staff->first_name = $first_name;
            $staff->last_name = $last_name;
            $staff->active = $request->input('active');

            if ($request->has('phone')) {
                $staff->phone = $request->input('phone');
            }

            if ($request->has('dob')) {
                // validate dob
                if ($this->checkIsAValidDate($request->input('dob'))) {
                    if ($request->input('dob') != 'null' || !is_null($request->input('dob'))) {
                        // save it as carbon
                        $staff->dob = new Carbon($request->input('dob'), config('app.timezone'));
                        $staff->dob->setTimezone(config('app.timezone'));
                    }
                }

            } else {
                $staff->dob = null;
            }

            $staff->save();

            // create member obj
            $member = Member::where('user_id', $user->id)->first();
            if (is_null($member)) {
                $member = new Member();
                $member->user_id = $user->id;
            }

            $member->share_info = 1;
            $member->first_name = $staff->first_name;
            $member->last_name = $staff->last_name;
            $member->save();

            // check role (add)
            UserRoles::where('user_id', $user->id)->delete();
            $role = new UserRoles();
            $role->role_id = $request->input('role'); // Manually add
            $role->user_id = $user->id;
            $role->save();

            // add tier
            // $tier = Tier::first();
            $default_tier = Setting::where('key', 'default_tier')->first()->value;
            $tier = Tier::find($default_tier);

            $user = User::find($user->id);
            $user->member->tiers()->save($tier);

            // add image
            if ($request->hasFile('file')) {
                if (config('bucket.s3')){                    
                    $picture = $request->file('file');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('staff/' . $staff->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');
                    $staff->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/staff/" . $user->staff->id . "/profile." . $type . "?" . time();
                    $staff->save();
                    $member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/staff/" . $user->staff->id . "/profile." . $type . "?" . time();
                    $member->save();
                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'StaffController.php/create';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'StaffController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }
            
            //NEW LOGIC SEND INTEGRATION BASED ON VENUE
            $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/register";
            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

            $email = $request->input('email');
            $password = $request->input('password');
            $mobile = $user->mobile;

            // $encoded = json_encode($data);
            // $checksum = md5($encoded);
            
            $datasend = array(
                'email' => $email,
                'password' => $request->input('password'),
                'name' => $first_name." ".$last_name,
                'given_name' => $first_name,
                'family_name' => $last_name,
                'phone_number' => $mobile,
                "license_key" => $your_order_key
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $your_order_api);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // Log::warning($content);
            // Log::warning($httpCode);

            if ($content) {
                $payload = \GuzzleHttp\json_decode($content);
                if ($payload->status == "error" && $payload->error == "user_not_found") {
                    return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                }
                if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                    return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                }

                // $member->login_token = $login_token;
                // $member->save();

            } else {
                return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
            }

            if ($new_user) {
                // send confirmation email

                // Get web app token
                $platform = Platform::where('name', 'web')->first();

                $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;

                $data = array(
                    'URL' => $url
                );

                if ($mx->init()) {
                    $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'confirmation',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

                $user->email_confirmation_sent = Carbon::now(config('app.timezone'));
                $user->save();

            }
            
            DB::commit();

            return response()->json(['status' => 'ok', 'request' => $request->all(), 'message' => 'Creating staff is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function update(Request $request, $id, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $staff = Staff::find($id);
            $staff->first_name = $request->input('first_name');
            $staff->last_name = $request->input('last_name');
            $staff->active = $request->input('active');

            if ($request->has('phone')) {
                $staff->phone = $request->input('phone');
            }

            if ($request->has('dob')) {
                // validate dob
                if ($this->checkIsAValidDate($request->input('dob'))) {
                    if ($request->input('dob') != 'null' || !is_null($request->input('dob'))) {

                        $staff->dob = new Carbon($request->input('dob'), config('app.timezone'));
                        $staff->dob->setTimezone(config('app.timezone'));
                    }
                }

            } else {
                $staff->dob = null;
            }

            $staff->save();

            $member = Member::find($staff->user->member->id);
            $member->first_name = $staff->first_name;
            $member->last_name = $staff->last_name;
            //$member->status = $request->input('active') ? 'active' : 'inactive';
            $member->save();

            // upload image
            if ($request->hasFile('file')) {

                if (config('bucket.s3')){
                    $picture = $request->file('file');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('staff/' . $staff->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');
                    $staff->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/staff/" . $staff->id . "/profile." . $type . "?" . time();
                    $staff->save();
                    $member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/staff/" . $staff->id . "/profile." . $type . "?" . time();
                    $member->save();
                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'StaffController.php/update';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'StaffController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            $user = $staff->user;
            if ($user->email !== $request->input('email')) {
                $checked_user = User::where('email', $request->input('email'))->first();
                if (!is_null($checked_user)) {

                    $checked_staff = Staff::where('user_id', $checked_user->id)->first();
                    if (!is_null($checked_staff)) {
                        return response()->json(['status' => 'error', 'message' => 'Email duplicated.'], Response::HTTP_BAD_REQUEST);
                    }

                    $user = $checked_user;
                    $staff->user_id = $user->id;
                    $staff->save();
                }
            }

            // change password if provided
            if ($request->has('password')) {
                $user->password = Hash::make($request->input('password'));
            }

            if ($request->has('mobile')) {
                $user->mobile = $request->input('mobile');
            }

            $user->status = $request->input('active') ? 'active' : 'inactive';

            if ($request->has('show_qr_code')) {
                $user->show_qr_code = $request->input('show_qr_code') == 'true' ? 1 : 0 ;
            }
            
            $user->save();
            
            // update role
            UserRoles::where('user_id', $user->id)->delete();
            $role = new UserRoles();
            $role->role_id = $request->input('role'); // Manually add
            $role->user_id = $user->id;
            $role->save();

            if (!is_null($member->bepoz_account_id)) {

                $data = array();
                $data['member_id'] = $member->id;

                $job = new BepozJob();
                $job->queue = "update-account";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'request' => $request->all(), 'message' => 'Updating staff is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific staff
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $staff = Staff::find($id);

            if (is_null($staff)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                DB::beginTransaction();

                UserRoles::where('user_id', $staff->user->id)->where('role_id', 2)->delete();
                Staff::destroy($id);

                DB::commit();
                return response()->json(['status' => 'ok', 'message' => 'Deleting staff is successful.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change staff status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'active' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $staff = Staff::find($request->input('id'));
            $staff->active = $request->input('active');
            $staff->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing staff status is successful.', 'data' => ['id' => $staff->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    private function checkIsAValidDate($myDateString)
    {
        return (bool)strtotime($myDateString);
    }
}
