<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Pattern;
use Illuminate\Http\Response as HttpResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class PatternController extends Controller
{
    /**
     * Return all patterns
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $patterns = Pattern::all();

            return response()->json(['status' => 'ok', 'data' => $patterns]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

}
