<?php

namespace App\Http\Controllers\Admin;

use App\FAQ;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    /**
     * Return all faqs
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request, FAQ $faq)
    {
        try {
            $paginate_number = 10;
            $obj = $faq->newQuery();

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('question', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('question', 'like', '%' . $request->input('keyword') . '%');
                    }
                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    $obj->where('faqs.status', 'active');
                }
            }

            $faqs = $obj->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $faqs]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a faq
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'answer' => 'required',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request'=> $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $faq = new FAQ;

            $faq->question = $request->input('question');

            $faq->answer = $request->input('answer');

            $faq->status = $request->input('status');

            $faq->save();

            return response()->json(['status' => 'ok', 'message' => 'Creating faq is successful.']);
        }
        catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific faq
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {
            $faq = FAQ::find($id);

            if (is_null($faq)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                FAQ::destroy($id);

                return response()->json(['status' => 'ok', 'message' => 'Deleting faq is successful.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a faq
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $faq = FAQ::find($id);

            if ($request->has('question')) {
                $faq->question = $request->input('question');
            }

            if ($request->has('answer')) {
                $faq->answer = $request->input('answer');
            }

            if ($request->has('status')) {
                $faq->status = $request->input('status');
            }

            $faq->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating faq is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific faq
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try{
            $faq = FAQ::find($id);

            if (is_null($faq)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $faq]);
            }
        }
        catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Bulk process of faq status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bulk(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'original_ids' => 'array',
                'new_ids' => 'array',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $originals = $request->input("original_ids");
            $new_ids = $request->input("new_ids");

            if (!empty(array_filter($originals))) {

                foreach ($new_ids as $new_id) {

                    if (DB::table('faqs')->where('id', $new_id)->first()->status == 'inactive') {

                        DB::table('faqs')->where('id', $new_id)->update(['status' => 'active']);

                    } else {

                        DB::table('faqs')->where('id', $new_id)->update(['status' => 'inactive']);
                    }

                }

            } else {

                foreach ($new_ids as $new_id) {
                    if (DB::table('faqs')->where('id', $new_id)->first()->status == 'inactive') {

                        DB::table('faqs')->where('id', $new_id)->update(['status' => 'active']);

                    } else {

                        DB::table('faqs')->where('id', $new_id)->update(['status' => 'inactive']);
                    }
                }

            }


            return response()->json(['status' => 'ok', 'message' => "FAQs changed successfully"]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $faq = FAQ::find($request->input('id'));
            $faq->status = $request->input('status');
            $faq->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing $faq status is successful.', 'data' => ['id' => $faq->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }
}
