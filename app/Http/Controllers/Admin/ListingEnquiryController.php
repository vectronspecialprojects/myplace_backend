<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ExportEnquiry;
use App\ListingEnquiry;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ListingEnquiryController extends Controller
{
    /**
     * Return all Listing enquiries
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request, ListingEnquiry $enquiry)
    {
        try {
            $obj = $enquiry->newQuery();

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('subject', 'like', '%' . $keyword . '%');
                        $query->orWhere('message', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('subject', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('message', 'like', '%' . $request->input('keyword') . '%');
                    }
                });

                $obj->orWhereHas('member', function ($q) use ($request) {
                    $q->where(function ($query) use ($request) {
                        if (strpos($request->input('keyword'), ' ') !== false) {
                            $array = explode(" ", $request->input('keyword'));
                            $keyword = implode("%", $array);
                            $query->where('first_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->where('first_name', 'like', '%' . $request->input('keyword') . '%');
                        }
                    });

                    $q->orWhere(function ($query) use ($request) {
                        if (strpos($request->input('keyword'), ' ') !== false) {
                            $array = explode(" ", $request->input('keyword'));
                            $keyword = implode("%", $array);
                            $query->where('last_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->where('last_name', 'like', '%' . $request->input('keyword') . '%');
                        }
                    });
                });

                $obj->orWhereHas('listing', function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('heading', 'like', '%' . $keyword . '%');
                        $query->orWhere('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('heading', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('name', 'like', '%' . $request->input('keyword') . '%');
                    }
                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            if ($request->has('show_done')) {
                if (intval($request->input('show_done')) === 0) {
                    $obj->where('is_answered', 0);
                }
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $enquiries = $obj->with('member.user', 'listing.type', 'staff.user')
                ->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $enquiries]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific enquiry
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $enquiry = ListingEnquiry::with('member.user', 'listing.type', 'staff.user')->find($id);

            if (is_null($enquiry)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $enquiry]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific enquiry
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {
            $enquiry = ListingEnquiry::find($id);

            if (is_null($enquiry)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                ListingEnquiry::destroy($id);

                return response()->json(['status' => 'ok', 'message' => 'Deleting enquiry is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Mark it as Done (enquiry)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function markItAsDone(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
                'is_answered' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $enquiry = ListingEnquiry::find($request->input('listing_id'));

            if (is_null($enquiry)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                $enquiry->staff_id = $request->input('decrypted_token')->staff->id;
                $enquiry->is_answered = (bool)$request->input('is_answered');
                $enquiry->save();

                return response()->json(['status' => 'ok', 'message' => 'Marking it as done is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Save comment (enquiry)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveComment(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $enquiry = ListingEnquiry::find($request->input('listing_id'));

            if (is_null($enquiry)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                if ($request->has('comment')) {
                    $enquiry->staff_id = $request->input('decrypted_token')->staff->id;
                    $enquiry->comment = $request->input('comment');
                    $enquiry->save();
                }

                return response()->json(['status' => 'ok', 'message' => 'Saving comment is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function getExportEnquiry(Request $request)
    {
        try {
            
            $enquiry = ListingEnquiry::where('id', '>', '0')->with('member.user', 'listing.type', 'staff.user')
                ->get()->toArray();
            
            // Log::info($enquiry);
            $values = $enquiry;
            
            $today = Carbon::now(config('app.timezone'));

            return Excel::download(new ExportEnquiry($values), 'Enquiry '. $today.'.xlsx');

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

}
