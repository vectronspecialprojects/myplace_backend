<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Bepoz;
use App\Http\Controllers\Controller;
use App\Tier;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TierController extends Controller
{
    /**
     * Return all tiers
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request, Tier $tier)
    {
        try {
            $obj = $tier->newQuery();

            if ($request->has('show_all')) {
                if (intval($request->input('show_all')) === 0) {
                    $obj->where('is_active', true);
                }
            }

            $bepoz = new Bepoz;
            $result = $bepoz->IDListGet(4);

            $parents = [];
            if (intval($result['IDList']['Count']) > 0) {
                for ($i = 0; $i < count($result['IDList']['ID']['int']); $i++) {
                    $parents[$result['IDList']['ID']['int'][$i]] =  $result['IDList']['Name']['string'][$i];
                }
            }
            // Log::info($parents);

            $tiers = $obj->get();

            foreach ($tiers as $tier) {
                // Log::info($tier);
                if ( array_key_exists($tier->bepoz_group_id, $parents)) {
                    $tier->bepoz_group_name = $parents[$tier->bepoz_group_id];
                }
            }

            $venueTags = VenueTag::select('id', 'name', 'status')->get();
                    
            $hqvenue = collect([
                'id' => 0,
                'name' => 'ALL VENUES',
                'status' => 1
            ]);

            $venueTags->prepend($hqvenue);

            // foreach ($venueTags as $venueTag) {
            //     $venueTag->id = (string) $venueTag->id;
            // }

            return response()->json(['status' => 'ok', 'data' => $tiers, 'venueTags' => $venueTags]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create / Modify tier object
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function tierSetting(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'tiers' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $tiers = \GuzzleHttp\json_decode($request->input('tiers'));

            if (!is_array($tiers)) {
                $tiers = [$tiers];
            }

            DB::beginTransaction();

            foreach ($tiers as $obj) {
                if (isset($obj->id)) {
                    $tier = Tier::find($obj->id);
                } else {
                    $tier = new Tier;
                }

                if (isset($obj->name)) {
                    $cleaned = strtolower($obj->name);
                    $cleaned = str_replace(' ', '_', $cleaned);
                    $tier->key = $cleaned;
                }

                $tier->name = $obj->name;
                $tier->bepoz_group_id = $obj->bepoz_group_id;
                $tier->bepoz_template_id = $obj->bepoz_template_id;
                $tier->boundary_start = $obj->boundary_start;

                // $tier->prefix_for_existing_member = $obj->prefix_for_existing_member;
                // $tier->suffix_for_existing_member = $obj->suffix_for_existing_member;
                // $tier->suffix_for_new_member = $obj->suffix_for_new_member;
                // $tier->prefix_for_new_member = $obj->prefix_for_new_member;

                $tier->boundary_end = $obj->boundary_end;
                $tier->is_active = $obj->is_active;
                if ( isset($obj->display) ){
                    $tier->display = $obj->display;
                } else {
                    $tier->display = 0;
                }
                if ( isset($obj->force_card_range) ){
                    $tier->force_card_range = $obj->force_card_range;
                } else {
                    $tier->force_card_range = 0;
                }

                // $tier->use_suffix = $obj->use_suffix;
                // $tier->use_prefix = $obj->use_prefix;

                $tier->is_default_tier = $obj->is_default_tier;
                // $tier->tier_group = $obj->tier_group;
                $tier->is_an_expiry_tier = $obj->is_an_expiry_tier;
                $tier->is_purchasable = $obj->is_purchasable;
                // $tier->venue_id = $obj->venue_id;
                // if ( isset($obj->manual_entry) ){
                //     $tier->manual_entry = $obj->manual_entry;
                // }
                // if ( isset($obj->is_gaming) ){
                //     $tier->is_gaming = $obj->is_gaming;
                // }
                // if ( isset($obj->use_calink_balance) ){
                //     $tier->use_calink_balance = $obj->use_calink_balance;
                // }
                // if ( isset($obj->use_calink_points) ){
                //     $tier->use_calink_points = $obj->use_calink_points;
                // }
                if ( isset($obj->color) ){
                    $tier->color = $obj->color;
                }
                if ( isset($obj->expiry_date_option) ){
                    $tier->expiry_date_option = $obj->expiry_date_option;
                }
                if ( isset($obj->specific_date) ){
                    $tier->specific_date = $obj->specific_date;
                }
                if ( isset($obj->downgrade_tier) ){
                    $tier->downgrade_tier = $obj->downgrade_tier;

                    // DOWNGRADE TIER NEED TO SAVE MAKE INACTIVE OPTION
                    if ( is_numeric($obj->downgrade_tier) ){
                        $downgrade_tier = Tier::find($obj->downgrade_tier);
                        if (!is_null($downgrade_tier)){
                            $tier->downgrade_group_id = $downgrade_tier->bepoz_group_id;
                        }
                    }
                }
                // if ( isset($obj->years) ){
                //     $tier->years = $obj->years;
                // }
                if ( isset($obj->months) ){
                    $tier->months = $obj->months;
                }
                // if ( isset($obj->weeks) ){
                //     $tier->weeks = $obj->weeks;
                // }
                if ( isset($obj->days) ){
                    $tier->days = $obj->days;
                }
                if ( isset($obj->state) ){
                    $tier->state = $obj->state;
                }
                if ( isset($obj->dont_update_from_bepoz) ){
                    $tier->dont_update_from_bepoz = $obj->dont_update_from_bepoz;
                }
                if ( isset($obj->icon) ){
                    $tier->icon = $obj->icon;
                }
                if ( isset($obj->use_precreated_account) ){
                    $tier->use_precreated_account = $obj->use_precreated_account;
                }
                if ( isset($obj->precreated_account_custom_flag) ){
                    $tier->precreated_account_custom_flag = $obj->precreated_account_custom_flag;
                }
                if ( isset($obj->precreated_account_custom_flag_name) ){
                    $tier->precreated_account_custom_flag_name = $obj->precreated_account_custom_flag_name;
                }
                if ( isset($obj->allowed_venues) ){
                    $tier->allowed_venues = $obj->allowed_venues;
                }
                
                $tier->save();
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'data' => 'Updating multiple settings is successful.']);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }
}
