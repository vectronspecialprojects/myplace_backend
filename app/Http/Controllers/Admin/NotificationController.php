<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{

    /**
     * Return all notification
     * >> for Admin
     *
     * @param Request $request
     * @return mixed
     */
    protected function index(Request $request)
    {
        try {
            $notifications = Notification::where('user_id', $request->decrypted_token->id)->orderBy('id', 'desc')->get();

            return response()->json(['status' => 'ok', 'data' => $notifications]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all notification
     * >> for Admin
     *
     * @param Request $request
     * @return mixed
     */
    protected function clearAll(Request $request)
    {
        try {
            Notification::where('user_id', $request->decrypted_token->id)->delete();

            return response()->json(['status' => 'ok', 'message' => 'Notification container is clear']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
