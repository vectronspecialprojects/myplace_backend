<?php

namespace App\Http\Controllers\Admin;

use App\Member;
use App\Order;

use App\Http\Controllers\Controller;
use App\OrderDetail;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    /**
     * Return all transactions (paginate)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request, Order $order)
    {
        try {
            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $obj = $order->newQuery();
            $obj->where('order_status', 'confirmed');
            $obj->orderBy('created_at', 'desc');

            if ($request->has('venue_id')) {
                if (intval($request->input('venue_id')) !== 0) {
                    $obj->where('venue_id', $request->input('venue_id'));
                }
            }

            if ($request->has('filter')) {
                if ($request->input('filter') !== 'none') {
                    if ($request->input('filter') === 'ticket') {
                        $obj->where('payload', 'like', '%"transaction_type":"place_order"%');
                    } elseif ($request->input('filter') === 'gift_certificate') {
                        $obj->where('payload', 'like', '%"transaction_type":"gift_certificate"%');
                    } elseif ($request->input('filter') === 'promotion') {
                        $obj->where('payload', 'like', '%"transaction_type":"claim_promotion"%');
                    } elseif ($request->input('filter') === 'friend_referral') {
                        $obj->where('payload', 'like', '%"transaction_type":"friend_referral"%');
                    } elseif ($request->input('filter') === 'signup_reward') {
                        $obj->where('payload', 'like', '%"transaction_type":"signup_reward"%');
                    }
                }
            }

            $orders = $obj->with('member')->paginate($paginate_number);

            foreach ($orders as $ord) {
                $ord->makeVisible('payload');
            }

            return response()->json(['status' => 'ok', 'data' => $orders]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return a specific order detail
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $order = Order::with('listing', 'payment_log', 'member.user', 
            'order_details.voucher_setup', 'order_details.member_vouchers', 'order_details.product_type')->find($id);

            if (intval($order->venue_id) === 0) {
                $order->venue = collect(
                    [
                        'id' => 0,
                        'name' => 'HQ',
                    ]);
            } else {
                $order->venue;
            }

            foreach ($order->order_details as $order_detail) {
                $order_detail->product_type->name = $order_detail->product_type_id == intval(3) ? "Credit Voucher" : $order_detail->product_type->name;
            }

            return response()->json(['status' => 'ok', 'data' => $order->makeVisible('payload')]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Generate invoice PDF
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function generateInvoice($id)
    {
        try {
            $order = Order::find($id);
            $member = Member::find($order->member_id);
            $user = User::find($member->user_id);
            $order_details = OrderDetail::where('order_id', $order->id)->get();

            $company_name = Setting::where('key', 'company_name')->first()->value;
            $company_abn = Setting::where('key', 'company_abn')->first()->value;

            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            $company_address1 =
                Setting::where('key', 'company_street_number')->first()->value
                . ' ' .
                Setting::where('key', 'company_street_name')->first()->value;

            $company_address2 =
                Setting::where('key', 'company_suburb')->first()->value
                . ' ' .
                Setting::where('key', 'company_postcode')->first()->value
                . ' ' .
                Setting::where('key', 'company_state')->first()->value;

            $data = array();
            $data['member'] = $member;
            $data['user'] = $user;
            $data['order'] = $order;
            $data['order_details'] = $order_details;
            $data['company_name'] = $company_name;
            $data['company_address1'] = $company_address1;
            $data['company_address2'] = $company_address2;
            $data['company_abn'] = $company_abn;
            $data['company_logo'] = $invoice_logo;

            if (!is_null($order->bepoz_transaction_ids)) {
                $ids = \GuzzleHttp\json_decode($order->bepoz_transaction_ids, true);

                $data['bepoz_transaction_ids'] = implode(", ", $ids);
            }

            // load view as pdf base
            $pdf = App::make('dompdf.wrapper')->loadView('pdf.invoice', $data);
            // return pdf data
            return $pdf->download('invoice.pdf');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
