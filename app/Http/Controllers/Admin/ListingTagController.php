<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Listing;
use App\ListingPivotTag;
use App\ListingTag;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ListingTagController extends Controller
{
    /**
     * Return all ListingTag
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $ListingTags = ListingTag::all();

            return response()->json(['status' => 'ok', 'data' => $ListingTags]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all ListingTag (pagination)
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateListingTag(Request $request, ListingTag $ListingTag)
    {
        try {

            $obj = $ListingTag->newQuery();

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    // $obj->where('Listing_tags.status', true);
                }
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $listings = $obj->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $listings]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Search through all ListingTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function search(Request $request, ListingTag $ListingTag)
    {
        try {
            $ListingTag = $ListingTag->newQuery();

            if ($request->has('keyword')) {
                $ListingTag->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            $ListingTag = $ListingTag->get();
            
            return response()->json(['status' => 'ok', 'data' => $ListingTag, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a ListingTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $ListingTag = new ListingTag;

            $ListingTag->name = $request->input('name');

            if ($request->has('display_order_type')) {
                $ListingTag->display_order_type = $request->input('display_order_type');
            }

            if ($request->has('display_order')) {
                $ListingTag->display_order = $request->input('display_order');
            }
            
            if ($request->has('status')) {
                $ListingTag->status = $request->input('status');
            }

            if ($request->has('style')) {
                $ListingTag->style = $request->input('style');
            }

            $ListingTag->save();
            
            if ($request->has('listingsPivot')) {
                $listingsPivot = \GuzzleHttp\json_decode($request->input('listingsPivot'));
                // $listingsPivot = $request->input('listingsPivot');

                foreach ($listingsPivot as $key=>$Listing) {
                    
                    $ListingPivotTag = ListingPivotTag::where('listing_id', $Listing->id)
                        ->where('listing_tag_id', $ListingTag->id)->first();

                    if ($Listing->mode == 'new'){
                        if (is_null($ListingPivotTag)){
                            $obj = new ListingPivotTag();

                            $obj->listing_id = $Listing->id;
                            $obj->listing_name = $Listing->name;
                            $obj->listing_tag_id = $ListingTag->id;
                            $obj->listing_tag_name = $ListingTag->name;
                            $obj->display_order = $key+1;
                            $obj->status = 1;
                            $obj->save();
                        }
                    } else if ($Listing->mode == 'deleted') {
                        ListingPivotTag::destroy($ListingPivotTag->id);
                    } else {
                        $ListingPivotTag->display_order = $key+1;
                        $ListingPivotTag->listing_name = $Listing->name;
                        $ListingPivotTag->listing_tag_name = $ListingTag->name;
                        $ListingPivotTag->save();
                    }
                }
            }

            return response()->json(['status' => 'ok', 'message' => 'Creating ListingTag is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific ListingTag
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $ListingTag = ListingTag::find($id);

            if (is_null($ListingTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                ListingTag::destroy($id);

                return response()->json(['status' => 'ok', 'message' => 'Deleting ListingTag is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a Listing
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $ListingTag = ListingTag::find($id);

            if (is_null($ListingTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            if ($request->has('name')) {
                $ListingTag->name = $request->input('name');
            }

            if ($request->has('display_order_type')) {
                $ListingTag->display_order_type = $request->input('display_order_type');
            }

            if ($request->has('display_order')) {
                $ListingTag->display_order = $request->input('display_order');
            }
            
            if ($request->has('status')) {
                $ListingTag->status = $request->input('status');
            }

            if ($request->has('style')) {
                $ListingTag->style = $request->input('style');
            }

            $ListingTag->save();
            
            if ($request->has('listingsPivot')) {
                // $listingsPivot = \GuzzleHttp\json_decode($request->input('listingsPivot'));
                $listingsPivot = $request->input('listingsPivot');

                foreach ($listingsPivot as $key=>$Listing) {
                    
                    $ListingPivotTag = ListingPivotTag::where('listing_id', $Listing['id'])
                        ->where('listing_tag_id', $id)->first();

                    if ($Listing['mode'] == 'new'){
                        if (is_null($ListingPivotTag)){
                            $obj = new ListingPivotTag();

                            $obj->listing_id = $Listing['id'];
                            $obj->listing_name = $Listing['name'];
                            $obj->listing_tag_id = $id;
                            $obj->listing_tag_name = $ListingTag->name;
                            $obj->display_order = $key+1;
                            $obj->status = 1;
                            $obj->save();
                        }
                    } else if ($Listing['mode'] == 'deleted') {
                        ListingPivotTag::destroy($ListingPivotTag->id);
                    } else {
                        $ListingPivotTag->display_order = $key+1;
                        $ListingPivotTag->listing_name = $Listing['name'];
                        $ListingPivotTag->listing_tag_name = $ListingTag->name;
                        $ListingPivotTag->save();
                    }
                }
    
                if ($ListingTag->display_order_type == 'alphabetically'){
                    $orderBy = 1;
                    $Listings = Listing::with(
                            ['listing_pivot_tags' => function($q) use ($id) {
                                $q->where('listing_tag_id',$id);
                            }]
                        )
                        ->whereHas('listing_pivot_tags', function ($query) use ($id) {
                            $query->where('listing_tag_id', $id);
                        })
                        ->orderBy('name')
                        ->get();
    
                    foreach ($Listings as $key=>$Listing) {
                        // Log::info($Listing->id." ".$Listing->name." ".$Listing->address);
                        // Log::info($Listing->listing_pivot_tags);
    
                        $ListingPivotTag = $Listing->listing_pivot_tags[0];
                        $ListingPivotTag->display_order = $orderBy;
                        $ListingPivotTag->save();
                        $orderBy++;
                    }
                }
            }

            return response()->json(['status' => 'ok', 'message' => 'Updating ListingTag is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific Listing
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $ListingTag = ListingTag::with('Listings')->find($id);

            if (is_null($ListingTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $ListingTag]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $ListingTag = ListingTag::find($request->input('id'));
            $ListingTag->status = $request->input('status');
            $ListingTag->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing ListingTag status is successful.', 'data' => ['id' => $ListingTag->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change Display Order
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function displayOrder(Request $request)
    {
        try {
            
            $validator = Validator::make($request->all(), [
                'display_order' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->has('display_order')) {
                
                //Log::info($request->input('display_order'));
                $display_orders = \GuzzleHttp\json_decode($request->input('display_order'));

                $ctr = 1;
                foreach ($display_orders as $display_order) {
                    //Log::info($ctr." ".$display_order->id." ".$display_order->name);
                    $ListingTag = ListingTag::find($display_order->id);
                    $ListingTag->display_order = $ctr;
                    $ListingTag->save();

                    $ctr++;
                }

            } else {
                Log::info('no display_order');
            }

            if (is_null($request)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $request]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

}
