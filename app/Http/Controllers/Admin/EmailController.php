<?php

namespace App\Http\Controllers\Admin;

use App\Email;
use App\Http\Controllers\Controller;
use App\MandrillSetting;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{
    /**
     * Return all emails
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $emails = Email::with('settings')->get();

            return response()->json(['status' => 'ok', 'data' => $emails]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create an email setting
     * >> for Admin
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'settings' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $email = new Email();
            $email->name = $request->input('name');
            $email->save();

            $settings = \GuzzleHttp\json_decode($request->input('settings'));
            foreach ($settings as $setting) {
                $obj = new MandrillSetting;
                $obj->key = $setting->key;
                $obj->value = $setting->value;
                $obj->type = $setting->type;

                $email->settings()->save($obj);
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Creating email is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Update an email setting
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $email = Email::find($id);

            if ($request->has('name')) {
                $email->name = $request->input('name');
            }

            if ($request->has('settings')) {
                $settings = \GuzzleHttp\json_decode($request->input('settings'));

                foreach ($settings as $setting) {
                    $obj = MandrillSetting::where('key', $setting->key)->where('email_id', $email->id)->first();
                    if (!is_null($obj)) {
                        $obj->value = $setting->value;
                        $obj->save();
                    }
                }

            }

            $email->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating email is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific email
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {

            $email = Email::find($id);

            if (is_null($email)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                if ($email->category == 'system') {
                    return response()->json(['status' => 'error', 'message' => 'Restricted. Pre-defined emails cannot be deleted.'], Response::HTTP_BAD_REQUEST);
                }

                DB::beginTransaction();
                Email::destroy($id);
                DB::commit();

                return response()->json(['status' => 'ok', 'message' => 'Deleting email is successful.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
