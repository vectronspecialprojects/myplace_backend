<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ExportMember;
use App\Listing;
use App\Member;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    protected function registeredMembers(Request $request)
    {
        try {
            if ($request->has('start_year')) {
                $start_year = intval($request->input('start_year'));
                if ($start_year <= 1000 || $start_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_year = date('Y');
            }

            if ($request->has('start_month')) {
                $start_month = intval($request->input('start_month'));
                if ($start_month < 1 || $start_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_month = date('n');
            }

            if ($request->has('start_date')) {
                $start_date = intval($request->input('start_date'));
                if ($start_date < 1 || $start_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_date = date('j');
            }

            if ($request->has('end_year')) {
                $end_year = intval($request->input('end_year'));
                if ($end_year <= 1000 || $end_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_year = date('Y');
            }

            if ($request->has('end_month')) {
                $end_month = intval($request->input('end_month'));
                if ($end_month < 1 || $end_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_month = date('n');
            }

            if ($request->has('end_date')) {
                $end_date = intval($request->input('end_date'));
                if ($end_date < 1 || $end_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_date = date('j');
            }

            $start_calendar = Carbon::createFromDate($start_year, $start_month, $start_date, config('app.timezone'))->hour(0)->minute(0)->second(0);
            $end_calendar = Carbon::createFromDate($end_year, $end_month, $end_date, config('app.timezone'))->hour(23)->minute(59)->second(59);

            $table = Member::with('user.social_logins', 'member_tier.tier')
                ->whereRaw("(members.created_at >= :start_calendar AND members.created_at <= :end_calendar)", ['start_calendar' => $start_calendar->toDateTimeString(), 'end_calendar' => $end_calendar->toDateTimeString()])
                ->get();

            $today = Carbon::now(config('app.timezone'));
            return Excel::download(new ExportMember($table), 'Member Report '. $today.'.xlsx');

            // return Excel::create('Members ' . $start_calendar->toDateString() . "-" . $end_calendar->toDateString(), function ($excel) use ($table, $start_calendar, $end_calendar) {

            //     // Set the title
            //     $excel->setTitle('Member');

            //     // Chain the setters
            //     $excel->setCreator('Vectron System Pty Ltd')
            //         ->setCompany('Vectron System Pty Ltd');

            //     // Call them separately
            //     $excel->setDescription('An exported members');

            //     $excel->sheet($start_calendar->toDateString() . "-" . $end_calendar->toDateString(), function ($sheet) use ($table) {

            //         $sheet->loadView('export.member', array('rows' => $table));

            //     });

            // })->export('xlsx');

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function downloadPreview(Request $request)
    {
        try {
            if ($request->has('start_year')) {
                $start_year = intval($request->input('start_year'));
                if ($start_year <= 1000 || $start_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_year = date('Y');
            }

            if ($request->has('start_month')) {
                $start_month = intval($request->input('start_month'));
                if ($start_month < 1 || $start_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_month = date('n');
            }

            if ($request->has('start_date')) {
                $start_date = intval($request->input('start_date'));
                if ($start_date < 1 || $start_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_date = date('j');
            }

            if ($request->has('end_year')) {
                $end_year = intval($request->input('end_year'));
                if ($end_year <= 1000 || $end_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_year = date('Y');
            }

            if ($request->has('end_month')) {
                $end_month = intval($request->input('end_month'));
                if ($end_month < 1 || $end_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_month = date('n');
            }

            if ($request->has('end_date')) {
                $end_date = intval($request->input('end_date'));
                if ($end_date < 1 || $end_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_date = date('j');
            }

            $start_calendar = Carbon::createFromDate($start_year, $start_month, $start_date, config('app.timezone'))->hour(0)->minute(0)->second(0);
            $end_calendar = Carbon::createFromDate($end_year, $end_month, $end_date, config('app.timezone'))->hour(23)->minute(59)->second(59);

            $orders = Order::with('order_details.product.product_type', 'member.user')
                ->whereRaw("orders.payment_status = 'successful' AND (orders.created_at >= :start_calendar AND orders.created_at <= :end_calendar)", ['start_calendar' => $start_calendar->toDateTimeString(), 'end_calendar' => $end_calendar->toDateTimeString()])
                ->get();

            $new_place_orders = [];
            $new_gift_certificates = [];
            foreach ($orders as $order) {
                $payload = \GuzzleHttp\json_decode($order->payload);
                if ($payload->transaction_type === 'place_order') {
                    foreach ($order->order_details as $v1) {
                        if (isset($new_place_orders[$v1->product_id])) {
                            $new_place_orders[$v1->product_id]["data"][] = ["member" => $order->member, "order_detail" => $v1, "order" => $order];
                        } else {
                            $new_place_orders[$v1->product_id] =
                                [
                                    "product_id" => $v1->product_id,
                                    'product_name' => $v1->product_name,
                                    'product_type' => $v1->product->product_type,
                                    'category' => $v1->category === null ? '' : $v1->category,
                                    "data" => [["member" => $order->member, "order_detail" => $v1, "order" => $order]]
                                ];

                        }
                    }

                } elseif ($payload->transaction_type === 'gift_certificate') {
                    foreach ($order->order_details as $v1) {
                        if (isset($new_gift_ceritificates[$v1->product_id])) {
                            $new_gift_certificates[$v1->product_id]["data"][] = ["member" => $order->member, "order_detail" => $v1, "order" => $order];
                        } else {
                            $new_gift_certificates[$v1->product_id] =
                                [
                                    "product_id" => $v1->product_id,
                                    'product_name' => $v1->product_name,
                                    "data" => [["member" => $order->member, "order_detail" => $v1, "order" => $order]]
                                ];

                        }
                    }
                }

            }

            return Excel::create('App Purchases ' . $start_calendar->toDateString() . "-" . $end_calendar->toDateString(), function ($excel) use ($new_place_orders, $new_gift_certificates, $start_calendar, $end_calendar) {

                // Set the title
                $excel->setTitle('App Purchases');

                // Chain the setters
                $excel->setCreator('Vectron System Pty Ltd')
                    ->setCompany('Vectron System Pty Ltd');

                // Call them separately
                $excel->setDescription('An exported purchases');

                $excel->sheet($start_calendar->toDateString() . "-" . $end_calendar->toDateString(), function ($sheet) use ($new_place_orders, $new_gift_certificates) {

                    $sheet->loadView('export.preview', array('orders' => $new_place_orders, 'gifts' => $new_gift_certificates));

                });

            })->export('xlsx');
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
