<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Http\Controllers\Controller;
use App\MemberAnswer;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AnswerController extends Controller
{
    /**
     * Return all answers
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $answers = Answer::all();

            return response()->json(['status' => 'ok', 'data' => $answers]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a answer
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'answer' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $answer = new Answer;
            $answer->answer = $request->input('answer');
            $answer->save();

            return response()->json(['status' => 'ok', 'message' => 'Creating answer is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific answer
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {
            $answer = Answer::find($id);

            if (is_null($answer)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $ma = MemberAnswer::where('answer_id', $id)->first();
                if (is_null($ma)) {

                    Answer::destroy($id);

                    return response()->json(['status' => 'ok', 'message' => 'Deleting answer is successful.']);

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Not safe. Some objects used this value'], Response::HTTP_BAD_REQUEST);

                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a answer
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'answer' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $answer = Answer::find($id);

            if (is_null($answer)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $answer->answer = $request->input('answer');
            $answer->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating answer is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific answer
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $answer = Answer::find($id);

            if (is_null($answer)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $answer]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function checkAnswer(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'answer_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // check pivot
            $ma = MemberAnswer::where('answer_id', $request->input('answer_id'))
                ->first();

            if (is_null($ma)) {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'safe']);
            } else {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'unsafe']);
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
