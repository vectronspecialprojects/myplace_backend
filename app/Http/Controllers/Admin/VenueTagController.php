<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VenueTagController extends Controller
{
    /**
     * Return all VenueTag
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $venueTags = VenueTag::all();

            return response()->json(['status' => 'ok', 'data' => $venueTags]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all venueTag (pagination)
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateVenueTag(Request $request, VenueTag $venueTag)
    {
        try {

            $obj = $venueTag->newQuery();

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    // $obj->where('venue_tags.status', true);
                }
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $listings = $obj->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $listings]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Search through all VenueTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function search(Request $request, VenueTag $venueTag)
    {
        try {
            $venueTag = $venueTag->newQuery();

            if ($request->has('keyword')) {
                $venueTag->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            $venueTag = $venueTag->get();
            
            return response()->json(['status' => 'ok', 'data' => $venueTag, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a venueTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $venueTag = new VenueTag;

            $venueTag->name = $request->input('name');

            if ($request->has('display_order_type')) {
                $venueTag->display_order_type = $request->input('display_order_type');
            }

            if ($request->has('display_order')) {
                $venueTag->display_order = $request->input('display_order');
            }
            
            if ($request->has('status')) {
                $venueTag->status = $request->input('status');
            }

            if ($request->has('style')) {
                $venueTag->style = $request->input('style');
            }

            if ($request->has('hide_in_app')) {
                $venueTag->hide_in_app = $request->input('hide_in_app');
            }

            $venueTag->save();
            
            if ($request->has('venuesPivot')) {
                // Log::info($request->input('venuesPivot'));

                $venuesPivot = \GuzzleHttp\json_decode($request->input('venuesPivot'));
                // $venuesPivot = $request->input('venuesPivot');

                foreach ($venuesPivot as $key=>$venue) {
                    
                    $venuePivotTag = VenuePivotTag::where('venue_id', $venue->id)
                        ->where('venue_tag_id', $venueTag->id)->first();

                    if ($venue->mode == 'new'){
                        if (is_null($venuePivotTag)){
                            $obj = new VenuePivotTag();

                            $obj->venue_id = $venue->id;
                            $obj->venue_name = $venue->name;
                            $obj->venue_tag_id = $venueTag->id;
                            $obj->venue_tags_name = $venueTag->name;
                            $obj->display_order = $key+1;
                            $obj->status = 1;
                            $obj->save();
                        }
                    } else if ($venue->mode == 'deleted') {
                        VenuePivotTag::destroy($venuePivotTag->id);
                    } else {
                        $venuePivotTag->display_order = $key+1;
                        $venuePivotTag->venue_name = $venue->name;
                        $venuePivotTag->venue_tags_name = $venueTag->name;
                        $venuePivotTag->save();
                    }
                }
            }

            return response()->json(['status' => 'ok', 'message' => 'Creating venueTag is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific venueTag
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $venueTag = VenueTag::find($id);

            if (is_null($venueTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                VenueTag::destroy($id);

                return response()->json(['status' => 'ok', 'message' => 'Deleting venueTag is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a venue
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $venueTag = VenueTag::find($id);

            if (is_null($venueTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            if ($request->has('name')) {
                $venueTag->name = $request->input('name');
            }

            if ($request->has('display_order_type')) {
                $venueTag->display_order_type = $request->input('display_order_type');
            }

            if ($request->has('display_order')) {
                $venueTag->display_order = $request->input('display_order');
            }
            
            if ($request->has('status')) {
                $venueTag->status = $request->input('status');
            }

            if ($request->has('style')) {
                $venueTag->style = $request->input('style');
            }

            $venueTag->save();
            
            if ($request->has('venuesPivot')) {
                // $venuesPivot = \GuzzleHttp\json_decode($request->input('venuesPivot'));
                $venuesPivot = $request->input('venuesPivot');

                foreach ($venuesPivot as $key=>$venue) {
                    
                    $venuePivotTag = VenuePivotTag::where('venue_id', $venue['id'])
                        ->where('venue_tag_id', $id)->first();

                    if ($venue['mode'] == 'new'){
                        if (is_null($venuePivotTag)){
                            $obj = new VenuePivotTag();

                            $obj->venue_id = $venue['id'];
                            $obj->venue_name = $venue['name'];
                            $obj->venue_tag_id = $id;
                            $obj->venue_tags_name = $venueTag->name;
                            $obj->display_order = $key+1;
                            $obj->status = 1;
                            $obj->save();
                        }
                    } else if ($venue['mode'] == 'deleted') {
                        VenuePivotTag::destroy($venuePivotTag->id);
                    } else {
                        $venuePivotTag->display_order = $key+1;
                        $venuePivotTag->venue_name = $venue['name'];
                        $venuePivotTag->venue_tags_name = $venueTag->name;
                        $venuePivotTag->save();
                    }
                }
    
                if ($venueTag->display_order_type == 'alphabetically'){
                    $orderBy = 1;
                    $venues = Venue::with(
                            ['venue_pivot_tags' => function($q) use ($id) {
                                $q->where('venue_tag_id',$id);
                            }]
                        )
                        ->whereHas('venue_pivot_tags', function ($query) use ($id) {
                            $query->where('venue_tag_id', $id);
                        })
                        ->orderBy('name')
                        ->get();
    
                    foreach ($venues as $key=>$venue) {
                        // Log::info($venue->id." ".$venue->name." ".$venue->address);
                        // Log::info($venue->venue_pivot_tags);
    
                        $venuePivotTag = $venue->venue_pivot_tags[0];
                        $venuePivotTag->display_order = $orderBy;
                        $venuePivotTag->save();
                        $orderBy++;
                    }
                }
            }

            return response()->json(['status' => 'ok', 'message' => 'Updating venueTag is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific venue
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $venueTag = VenueTag::with('venues')->find($id);

            if (is_null($venueTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $venueTag]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $venueTag = VenueTag::find($request->input('id'));
            $venueTag->status = $request->input('status');
            $venueTag->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing venueTag status is successful.', 'data' => ['id' => $venueTag->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeHideInApp(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'hide_in_app' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $venueTag = VenueTag::find($request->input('id'));
            $venueTag->hide_in_app = $request->input('hide_in_app');
            $venueTag->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing venueTag Hide In App is successful.', 'data' => ['id' => $venueTag->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change Display Order
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function displayOrder(Request $request)
    {
        try {
            
            $validator = Validator::make($request->all(), [
                'display_order' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->has('display_order')) {
                
                //Log::info($request->input('display_order'));
                $display_orders = \GuzzleHttp\json_decode($request->input('display_order'));

                $ctr = 1;
                foreach ($display_orders as $display_order) {
                    //Log::info($ctr." ".$display_order->id." ".$display_order->name);
                    $venueTag = VenueTag::find($display_order->id);
                    $venueTag->display_order = $ctr;
                    $venueTag->save();

                    $ctr++;
                }

            } else {
                // Log::info('no display_order');
            }

            if (is_null($request)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $request]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all Venue Tags With Pivot
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function venueTagWithPivot()
    {
        try {
            $venueTags = VenueTag::with("venues")->get();

            return response()->json(['status' => 'ok', 'data' => $venueTags]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return Venue Tag associated with venue id
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function venueTagSearchVenueID(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'venue_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }
            
            $venue_id = $request->input('venue_id');
            $venues = Venue::with(
                    ['venue_pivot_tags' => function($q) use ($venue_id) {
                        $q->where('venue_id',$venue_id);
                    }]
                )
                ->whereHas('venue_pivot_tags', function ($query) use ($venue_id) {
                    $query->where('venue_id', $venue_id);
                })
                ->get();

            return response()->json(['status' => 'ok', 'data' => $venues]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
