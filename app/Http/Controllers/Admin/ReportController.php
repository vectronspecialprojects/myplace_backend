<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\BepozLog;
use App\GamingLog;
use App\Order;
use App\SystemLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
    /**
     * Return all system logs (paginate)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createSystemLogs(Request $request)
    {
        try {
            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $logs = SystemLog::orderBy('created_at', 'desc')
                ->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $logs]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Clear system logs
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function clearSystemLogs()
    {
        try {
            SystemLog::whereNotNull('id')->delete();

            return response()->json(['status' => 'ok', 'message' => 'Logs cleared']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get registered member report
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getRegisteredMembers(Request $request)
    {
        try {
            // if ($request->has('year')) {
            //     $year = intval($request->input('year'));
            //     if ($year <= 1000 || $year > 2100) {
            //         return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
            //     }
            // } else {
            //     $year = date('Y');
            // }

            // if ($request->has('month')) {
            //     $month = intval($request->input('month'));
            //     if ($month < 1 || $month > 12) {
            //         return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
            //     }
            // } else {
            //     $month = date('n');
            // }

            if ($request->has('start_year')) {
                $start_year = intval($request->input('start_year'));
                if ($start_year <= 1000 || $start_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_year = date('Y');
            }

            if ($request->has('start_month')) {
                $start_month = intval($request->input('start_month'));
                if ($start_month < 1 || $start_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_month = date('n');
            }

            if ($request->has('start_date')) {
                $start_date = intval($request->input('start_date'));
                if ($start_date < 1 || $start_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_date = date('j');
            }

            if ($request->has('end_year')) {
                $end_year = intval($request->input('end_year'));
                if ($end_year <= 1000 || $end_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_year = date('Y');
            }

            if ($request->has('end_month')) {
                $end_month = intval($request->input('end_month'));
                if ($end_month < 1 || $end_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_month = date('n');
            }

            if ($request->has('end_date')) {
                $end_date = intval($request->input('end_date'));
                if ($end_date < 1 || $end_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_date = date('j');
            }

            $start_calendar = Carbon::createFromDate($start_year, $start_month, $start_date, config('app.timezone'))->hour(0)->minute(0)->second(0);
            $end_calendar = Carbon::createFromDate($end_year, $end_month, $end_date, config('app.timezone'))->hour(23)->minute(59)->second(59);

            $users = User::with('member.member_tier.tier', 'social_logins')->has('member')
                ->whereRaw("(users.created_at >= :start_calendar AND users.created_at <= :end_calendar)", ['start_calendar' => $start_calendar->toDateTimeString(), 'end_calendar' => $end_calendar->toDateTimeString()])
                ->get();

            return response()->json(['status' => 'ok', 'data' => $users->makeVisible('created_at')]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Get Transaction Report
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createPreview(Request $request)
    {
        try {
            if ($request->has('start_year')) {
                $start_year = intval($request->input('start_year'));
                if ($start_year <= 1000 || $start_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_year = date('Y');
            }

            if ($request->has('start_month')) {
                $start_month = intval($request->input('start_month'));
                if ($start_month < 1 || $start_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_month = date('n');
            }

            if ($request->has('start_date')) {
                $start_date = intval($request->input('start_date'));
                if ($start_date < 1 || $start_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_date = date('j');
            }

            if ($request->has('end_year')) {
                $end_year = intval($request->input('end_year'));
                if ($end_year <= 1000 || $end_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_year = date('Y');
            }

            if ($request->has('end_month')) {
                $end_month = intval($request->input('end_month'));
                if ($end_month < 1 || $end_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_month = date('n');
            }

            if ($request->has('end_date')) {
                $end_date = intval($request->input('end_date'));
                if ($end_date < 1 || $end_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_date = date('j');
            }

            $start_calendar = Carbon::createFromDate($start_year, $start_month, $start_date, config('app.timezone'))->hour(0)->minute(0)->second(0);
            $end_calendar = Carbon::createFromDate($end_year, $end_month, $end_date, config('app.timezone'))->hour(23)->minute(59)->second(59);

            $orders = Order::with('order_details.product.product_type', 'member')
                ->whereRaw("orders.payment_status = 'successful' AND (orders.created_at >= :start_calendar AND orders.created_at <= :end_calendar)", ['start_calendar' => $start_calendar->toDateTimeString(), 'end_calendar' => $end_calendar->toDateTimeString()])
                ->get();


            return response()->json(['status' => 'ok', 'data' => $orders->makeVisible('payload')]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all Bepoz logs (paginate)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getBepozLogs(Request $request, BepozLog $sn)
    {
        
        try {
            $obj = $sn->newQuery();

            // filter by log groups
            if ($request->has('logFilter') && $request->input('logFilter') !== '*') {
                // $obj->where('response', 'like', '%<ReqType>' . $request->input('logFilter') . '</ReqType>%');
                if ($request->input('logFilter') == 'Error') {
                    // error is only returned in response
                    $obj->where('response_type', '=', $request->input('logFilter'));
                } else {
                    $account = ['AccountGet', 'AccountFullGet', 'AccountFind', 'AccountSearch', 'CreateAccount', 'UpdateAccount', 
                                'AccBalanceGet', 'AccBalanceUpdate', 'AccountPromoGet', 'NextAccountNumber', 'TestNextAccountNumber',
                                'AccCreateUpdate', 'AccountExtendedGet', 'AccCustomGet', 'AccCustomFieldSet'];                    
                    $product = ['ProductGet', 'ProdPriceGet', 'ProdPromoGet', 'ProductsGetAll', 'ProductsGetByExportCode', 
                                'ProductGetDetails', 'ProductFullGetById', 'ProductsSearch', 'CondimentsGetAll'];
                    $member = ['MemberPrices', 'MemberDrawGet', 'MembershipGet', 'RenewalGet', 'RaffleSetupGet'];
                    $voucher = ['VoucherGet', 'VoucherSetupGet', 'VoucherIssue', 'VoucherRedeem', 'PrizePromoGet'];
                    $transaction = ['TransactionGet', 'TransactionGetFull', 'TransactionCreate'];
                    $general = ['OperatorGet', 'BaseSetupGet', 'TableGet', 'IDListGet', 'VersionGet', 'SystemCheck', 'Status',
                                'SupplierGet', 'AddressGet', 'CommentGet', 'ImageDataGet', 'JobRun', 'StoredProcRun'];

                    // $obj->where('request_type', '=', $request->input('logFilter'))
                    //     ->orWhere('response_type', '=', $request->input('logFilter'));
                    switch ($request->input('logFilter')) {
                        case 'Account':
                            $obj->whereIn('request_type', $account);
                            break;
                        case 'Product':
                            $obj->whereIn('request_type', $product);
                            break;
                        case 'Member':
                            $obj->whereIn('request_type', $member);
                            break;
                        case 'Voucher':
                            $obj->whereIn('request_type', $voucher);
                            break;
                        case 'Transaction':
                            $obj->whereIn('request_type', $transaction);
                            break;
                        case 'General':
                            $obj->whereIn('request_type', $general);
                            break;
                    }
                }
            }
            // $query = \DB::table('bepoz_logs')->where('response', 'like', '%Error%');
            
            // filter by date range
            if ($request->has('startDate') && $request->has('endDate')) {
                $startDate = $request->input('startDate');
                $endDate = $request->input('endDate');
                
                // Log::warning($startDate);
                // Log::warning($endDate);

                if (!(preg_match('/^\d{4}-\d{2}-\d{2}$/', $startDate) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $endDate))) {
                    return response()->json(['status' => 'error', 'message' => "Invalid date range"], Response::HTTP_BAD_REQUEST);
                }

                $c = \Carbon\Carbon::parse($startDate . ' 00:00', config('app.timezone'));
                $c->setTimezone('UTC');
                $startDate = $c->toDateTimeString();

                $c = \Carbon\Carbon::parse($endDate . ' 00:00', config('app.timezone'));
                $c->setTimezone('UTC');
                $c->addDays(1); //Make end_date INCLUSIVE
                $endDate = $c->toDateTimeString();
                
                // $obj->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);
                $obj->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);
            }

            if ($request->has('keyword')) {

                $obj->where(function ($q) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $q->where('request', 'like', '%' . $keyword . '%');
                        $q->orWhere('response', 'like', '%' . $keyword . '%');
                        $q->orWhere('created_at', 'like', '%' . $keyword . '%');
                    } else {
                        $q->where('request', 'like', '%' . $request->input('keyword') . '%');
                        $q->orWhere('response', 'like', '%' . $request->input('keyword') . '%');
                        $q->orWhere('created_at', 'like', '%' . $request->input('keyword') . '%');
                    }
                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }
            
            $paginate_number = 10;

            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            // $page = 1;

            // if ($request->has('page')) {
            //     $page = $request->input('page');
            // }

            // eloquent pagination
            $data = $obj->paginate($paginate_number);
            // $data = $obj->simplePaginate($paginate_number);
            
            if (!empty($data)) {
                $formattedData = $data->getCollection();
                // data manipulation
                // convert both request and response fields with inner XMLData fields to json
                foreach ($formattedData as $d) {
                    try {
                        if (!is_null($d->request)) {
                            $reqXml = html_entity_decode($d->request);
                            $d->request = $reqXml;
                        }

                        if (!is_null($d->response)) {                        
                            $resXml = html_entity_decode($d->response);
                            $d->response = $resXml;
                        }
                    } catch (Exception $e) {
                        return response()->json(['status' => 'error', 'message' => $e->getMessage() . '[' . $e->getLine() . ']'], Response::HTTP_BAD_REQUEST);
                    }
                }

                $data->setCollection($formattedData);
            }

            return response()->json(['status' => 'ok', 'data' => $data]);
        } catch (\Exception $e) {
            // \Helper::logUnhandledError($e, []);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all Gaming logs (paginate)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getGamingLogs(Request $request, GamingLog $sn)
    {
        
        try {
            $obj = $sn->newQuery();

            // filter by log groups
            if ($request->has('logFilter') && $request->input('logFilter') !== '*') {
                // $obj->where('response', 'like', '%<ReqType>' . $request->input('logFilter') . '</ReqType>%');
                if ($request->input('logFilter') == 'Error') {
                    // error is only returned in response
                    $obj->where('response_type', '=', $request->input('logFilter'));
                } else {
                    
                }
            }
            // $query = \DB::table('bepoz_logs')->where('response', 'like', '%Error%');
            
            // filter by date range
            if ($request->has('startDate') && $request->has('endDate')) {
                $startDate = $request->input('startDate');
                $endDate = $request->input('endDate');
                
                // Log::warning($startDate);
                // Log::warning($endDate);

                if (!(preg_match('/^\d{4}-\d{2}-\d{2}$/', $startDate) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $endDate))) {
                    return response()->json(['status' => 'error', 'message' => "Invalid date range"], Response::HTTP_BAD_REQUEST);
                }

                $c = \Carbon\Carbon::parse($startDate . ' 00:00', config('app.timezone'));
                $c->setTimezone('UTC');
                $startDate = $c->toDateTimeString();

                $c = \Carbon\Carbon::parse($endDate . ' 00:00', config('app.timezone'));
                $c->setTimezone('UTC');
                $c->addDays(1); //Make end_date INCLUSIVE
                $endDate = $c->toDateTimeString();
                
                // $obj->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);
                $obj->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);
            }

            if ($request->has('keyword')) {

                $obj->where(function ($q) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $q->where('request', 'like', '%' . $keyword . '%');
                        $q->orWhere('response', 'like', '%' . $keyword . '%');
                        $q->orWhere('created_at', 'like', '%' . $keyword . '%');
                    } else {
                        $q->where('request', 'like', '%' . $request->input('keyword') . '%');
                        $q->orWhere('response', 'like', '%' . $request->input('keyword') . '%');
                        $q->orWhere('created_at', 'like', '%' . $request->input('keyword') . '%');
                    }
                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }
            
            $paginate_number = 10;

            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            // $page = 1;

            // if ($request->has('page')) {
            //     $page = $request->input('page');
            // }

            // eloquent pagination
            $data = $obj->paginate($paginate_number);
            // $data = $obj->simplePaginate($paginate_number);
            
            if (!empty($data)) {
                $formattedData = $data->getCollection();
                // data manipulation
                // convert both request and response fields with inner XMLData fields to json
                foreach ($formattedData as $d) {
                    try {
                        if (!is_null($d->request)) {
                            $reqXml = html_entity_decode($d->request);
                            $d->request = $reqXml;
                        }

                        if (!is_null($d->response)) {                        
                            $resXml = html_entity_decode($d->response);
                            $d->response = $resXml;
                        }
                    } catch (Exception $e) {
                        return response()->json(['status' => 'error', 'message' => $e->getMessage() . '[' . $e->getLine() . ']'], Response::HTTP_BAD_REQUEST);
                    }
                }

                $data->setCollection($formattedData);
            }

            return response()->json(['status' => 'ok', 'data' => $data]);
        } catch (\Exception $e) {
            // \Helper::logUnhandledError($e, []);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
