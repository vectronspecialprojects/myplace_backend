<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\OrderDetail;
use App\ProductType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProductTypeController extends Controller
{
    /**
     * Return all product types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $types = ProductType::all();

            return response()->json(['status' => 'ok', 'data' => $types]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a product types
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'key' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request'=> $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $type = new ProductType;

            $type->name = $request->input('name');

            if($request->has('description')) {
                $type->description = $request->input('description');
            }

            $type->key = $request->input('key');

            $type->save();

            return response()->json(['status' => 'ok', 'message' => 'Creating product type is successful.']);
        }
        catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific product type
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {
            $type = ProductType::find($id);

            if (is_null($type)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $od = OrderDetail::where('product_type_id', $id)->first();
                if(is_null($od)) {
                    ProductType::destroy($id);
                    return response()->json(['status' => 'ok', 'message' => 'Deleting product type is successful.']);
                }
                else {
                    return response()->json(['status' => 'error', 'message' => 'Not safe. Some objects used this value'], Response::HTTP_BAD_REQUEST);
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a product type
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'key' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request'=> $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $type = ProductType::find($id);

            $type->name = $request->input('name');

            if($request->has('description')) {
                $type->description = $request->input('description');
            }
            else {
                $type->description = "";
            }

            $type->key = $request->input('key');

            $type->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating product type is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific product type
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try{
            $type = ProductType::find($id);

            if (is_null($type)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $type]);
            }
        }
        catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
