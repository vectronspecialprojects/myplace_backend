<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SystemNotification;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SystemNotificationController extends Controller
{

    /**
     * Return all system notifications
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $notifications = SystemNotification::all();

            return response()->json(['status' => 'ok', 'data' => $notifications]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all system notifications (pagination)
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateSystems(Request $request, SystemNotification $sn)
    {
        try {
            $obj = $sn->newQuery();

            if ($request->has('keyword')) {

                $obj->orWhere(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('title', 'like', '%' . $keyword . '%');
                        $query->orWhere('type', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('title', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('type', 'like', '%' . $request->input('keyword') . '%');
                    }
                });

            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }


            $notifications = $obj->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $notifications]);


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a notification
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'message' => 'required',
                'type' => 'required',
                'action' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $sn = new SystemNotification();
            $sn->title = $request->input('title');
            $sn->type = $request->input('type');
            $sn->action = $request->input('action');
            $sn->message = $request->input('message');
            $sn->payload = $request->input('payload');
            $sn->save();


            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Creating product is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Update a notification
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'message' => 'required',
                'type' => 'required',
                'action' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $sn = SystemNotification::find($id);
            $sn->title = $request->input('title');
            $sn->type = $request->input('type');
            $sn->action = $request->input('action');
            $sn->message = $request->input('message');
            $sn->payload = $request->input('payload');
            $sn->save();


            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Updating product is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Delete a specific listing
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $listing = SystemNotification::find($id);

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                DB::beginTransaction();

                SystemNotification::destroy($id);

                DB::commit();
                return response()->json(['status' => 'ok', 'message' => 'Deleting System Notification is successful.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific SystemNotification
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $notification = SystemNotification::find($id);

            return response()->json(['status' => 'ok', 'data' => $notification]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
