<?php

namespace App\Http\Controllers\Admin;

// <editor-fold desc="IMPORT">
use App\Helpers\MandrillExpress;
use App\Helpers\Bepoz;
use App\Helpers\MailjetExpress;
use App\Jobs\SendReminderNotification;
use App\Jobs\SendWelcomeEmail;
use App\Http\Controllers\Controller;
use App\Jobs\SendPusherNotification;
use App\Jobs\SendReceiptEmail;
use App\Jobs\SendBookingTicketEmail;
use App\Jobs\SendOneSignalNotification;

use App\BepozJob;
use App\ClaimedPromotion;
use App\FriendReferral;
use App\GiftCertificate;
use App\Listing;
use App\ListingProduct;
use App\ListingSchedule;
use App\ListingScheduleTag;
use App\ListingScheduleType;
use App\ListingScheduleVenue;
use App\ListingType;
use App\Member;
use App\MemberLog;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Platform;
use App\PaymentLog;
use App\PointLog;
use App\Product;
use App\ProductType;
use App\Setting;
use App\StripeCustomer;
use App\SystemLog;
use App\Tier;
use App\User;
use App\UserRoles;
use App\VoucherSetups;

use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use ImageOptimizer\Exception\Exception;
use Milon\Barcode\DNS1D;

// </editor-fold>


class BookingTicketController extends Controller
{
  
  ///////////////////////                    IMPORTANT NOTES!               ////////////////////////////////////////////
  //                                                                                                                  //
  //   This Controller have simliarity with app/http/controllers/Member/TransactionController                         //
  //   createBooking, updateBooking, confirmBooking                                                                   //
  //   * createBooking first, then should confirmBooking, so the tickets and credit voucher will issue via email      //
  //   * updateBooking also should confirmBooking, so the tickets and credit voucher will issue via email             //
  //                                                                                                                  //
  //   ticket and credit voucher will created by cronjob                                                              //
  //                                                                                                                  //
  //   FOR NEW EMAIL FORMAT                                                                                           //
  //   * ADD SETTING IN database/seeds/EmailsTableSeeder                                                              //
  //   * ADD FILE IN FOLDER jobs                                                                                      //
  //   * CALL THAT JOB IN CONTROLLER                                                                                  //
  //                                                                                                                  //
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                  SELECT EVENT FILTERED BY MONTH AND YEAR                                                         // 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getEventListForCalendar(Request $request)
    {
        try { 
          $listings = Listing::with('type', 'products.product', 'venue',
              'schedules.types.type', 'schedules.venues.venue', 'schedules.tags.tag',
              'enquiries.member', 'enquiries.listing',              
              'order_details.member_vouchers', 'order_details.order.member.user')
            ->where('listing_type_id', '1');
            
          if ($request->has('month') && $request->has('year')) {
            $desiredDateStart = Carbon::now();
            
            //prepare search from beginning of month
            $desiredDateStart->day  = "1";
            
            //if year and month requested is not current month and year
            if ( !($desiredDateStart->year == $request->input('year') && 
                $desiredDateStart->month  == $request->input('month') ) ){
              
              $desiredDateStart->year   = $request->input('year');
              $desiredDateStart->month  = $request->input('month');              
            }
                        
            $desiredDateEnd = $desiredDateStart->copy()->endOfMonth();
            
            $listings = $listings
              ->whereRaw('listing_type_id = 1 AND
                ( 
                  (
                    ("' . $desiredDateStart . '" BETWEEN `datetime_start` AND `datetime_end`) 
                    OR
                    ("' . $desiredDateEnd . '" BETWEEN `datetime_start` AND `datetime_end`)
                  )
                  OR
                  (
                    (`datetime_start` BETWEEN "' . $desiredDateStart . '" AND "' . $desiredDateEnd . '") 
                    OR
                    (`datetime_end` BETWEEN "' . $desiredDateStart . '" AND "' . $desiredDateEnd . '")
                  ) 
                ) ');
              
          }
          //$listings = $listings->where('listing_type_id', '4'); 
          $listings = $listings->get();
          
          foreach ($listings as $listing) {
            
            if ($listing->venue_id == 0){
              $company_name = Setting::select('value')->where('key', 'company_name')->first()->value;
              $company_street_number = Setting::select('value')->where('key', 'company_street_number')->first()->value;
              $company_street_name = Setting::select('value')->where('key', 'company_street_name')->first()->value;
              $company_suburb = Setting::select('value')->where('key', 'company_suburb')->first()->value;
              $company_state = Setting::select('value')->where('key', 'company_state')->first()->value;
              $company_postcode = Setting::select('value')->where('key', 'company_postcode')->first()->value;
                            
              $listing->venue_address = $company_name.", "
                .$company_street_number." " 
                .$company_street_name.", " 
                .$company_suburb.", " 
                .$company_state.", " 
                .$company_postcode ;
            }
            
            
            //$listing->total_ticket = $listing->order_details_success->sum('qty');

            //need to get order details with listing, and payment in order of order details is successful
            //$listing->total_ticket = OrderDetail::with('order_payment_success')->where('listing_id', $listing->id)
            //                            ->get()->sum('qty');

            $listing->total_ticket = DB::table('order_details')->select(\DB::raw('SUM(order_details.qty) as total_ticket'))
                                    ->join('orders', 'order_details.order_id', '=', 'orders.id')
                                    //->where('orders.payment_status', '=', 'successful')
                                    ->where('order_details.status', '<>', 'expired')
                                    ->where('order_details.listing_id', '=', $listing->id)
                                    ->where('order_details.product_type_id', 2)
                                    ->first()->total_ticket;
                                    //->toSql();

            //Log::info( $listing->total_ticket->toSql() );                        

            $listing->ticket_remaining = $listing->max_limit==0 ? 'Unlimited' :  ($listing->max_limit - $listing->total_ticket);            
                        
            //product with F&B Voucher type
            $listing->credit_voucher = Product::where('product_type_id', 3)->get();
            
            foreach ($listing->credit_voucher as $credit_voucher) {
              if ( $credit_voucher->unit_price > 0){
                //$credit_voucher->discount = ( $credit_voucher->gift_value - $credit_voucher->unit_price ) / $credit_voucher->gift_value * 100 / 1;                
                $credit_voucher->discount = 0;
              } else {
                $credit_voucher->discount = 0;
              }
            }
          }

          if (is_null($listings)) {
              return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
          } else {
              return response()->json(['status' => 'ok', 'data' => $listings]);
          }
        } catch (\Exception $e) {
          Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }    
    
    
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //                  SELECT ALL EVENT                                                                                // 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected function getEventList()
    {
      try {
        $paginate_number = 10;            
           
        $listings = Listing::with('type', 'products.product',
          //'products.product.order_details_sum',
          'schedules.types.type', 'schedules.venues.venue', 'schedules.tags.tag',
          'enquiries.member', 'enquiries.listing',              
          'order_details.member_vouchers', 'order_details.order.member.user')
          ->where('listing_type_id', 1)
          ->get();
        //with counted successful tickets in Order tables
        
        $listings = $listings->paginate($paginate_number);
          
        foreach ($listings as $listing) {
          $listing->total_ticket = $listing->order_details->sum('qty');

          $listing->ticket_remaining = $listing->max_limit==0 ? 'Unlimited' :  ($listing->max_limit - $listing->total_ticket);            
        }
          
        if (is_null($listings)) {
            return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['status' => 'ok', 'data' => $listings]);
        }
      } catch (\Exception $e) {
        Log::error($e);
          return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
      }
    
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  CREATE BOOKING BASED FROM TRANSACTION PLACE ORDER                                                               // 
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    protected function createBooking(Request $request, MandrillExpress $mx)
    {
        try {

            $validator = Validator::make($request->all(), [
                'order' => 'required',
                'type' => 'required',
                'total' => 'required',
                'listing_id' => 'required',
                //credential
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'mobile' => 'required',
                'become_member' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $bepoz = new Bepoz;

            $listing = Listing::find($request->input('listing_id'));

            if (is_null($listing)) {
                throw new \Exception('Listing not found.');
            }

            DB::beginTransaction();

            $order_type = $request->input('type');

            if (strcmp($order_type, 'cash') !== 0) {
                if (strcmp($order_type, 'point') !== 0) {
                    if (strcmp($order_type, 'mix') !== 0) {
                        return response()->json(['status' => 'error', 'message' => 'invalid order type'], Response::HTTP_BAD_REQUEST);
                    }
                }
            }
            
            //default
            $member = new Member;

            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $email = $request->input('email');
            $mobile = $request->input('mobile');
            $become_member = $request->input('become_member');
            $member_status = "inactive";
            $member_password = "12345678";            
            
            if ($request->has('become_member')) {
              if ( $request->input('become_member') ) {
                 $member_status = "active";
              }
            }
            
            //check user exist and load it
            $memberSearch = Member::whereHas('user', function ($q) use ($request) {
                $q->where('email', 'like', '%' . $request->input('email') . '%');
            })->first();
            
            //if user does not exist 
            if ( is_null ($memberSearch) ){
              
                //make new member

                $user = new User;
                $user->email = $request->input('email');
                $user->password = Hash::make($member_password);
                $user->status = $member_status;
                $user->mobile = $request->input('mobile');
                $user->save();

                $member = new Member;
                $member->user_id = $user->id;
                $member->first_name = $request->input('first_name');
                $member->last_name = $request->input('last_name');
                $member->status = $member_status;


                if ( $request->input('become_member')  ){                
                    $member_status = "active";
                } else {
                    $member_status = "inactive";
                }

                $member->save();
                
                //add user role
                $user_role = new UserRoles;
                $user_role->role_id = 3; // Manually add
                $user_role->user_id = $user->id;
                $user_role->save();

                //Load user has been created / refresh
                $user = User::with('member')->find($member->user_id);

                //add tier
                $tier = Tier::first();

                // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                // if ($default_member_expiry_date == "true"){
                //     $setting = Setting::where('key', 'cut_off_date')->first();
                //     $date_expiry = Carbon::parse($setting->value);
                //     $date_expiry->setTimezone(config('app.timezone'));
                //     $date_expiry->setTime(0, 0, 0);
                // } else {
                //     $date_expiry = null;
                // }

                $date_expiry = null;
                $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);


                if ( $request->input('become_member')  ){
                    //send email confirmation
                    $confirmationToken = $user->generateEmailConfirmationToken();
                    
                    // Get web app token
                    $platform = Platform::where('name', 'web')->first();

                    $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;

                    $data = array(
                        'URL' => $url
                    );
                    

                    if ($mx->init()) {
                        $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'confirmation',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                    $job = (new SendReminderNotification($user))->delay(60);
                    dispatch($job);

                    $user->email_confirmation_sent = Carbon::now(config('app.timezone'));
                    $user->save();

                    $this->getReward($request, $user);

                    //$result = $bepoz->VoucherIssue($voucherSetup->bepoz_voucher_setup_id, $member->bepoz_account_id, null, $value);
                    //$result = $bepoz->SystemCheck();
                    //Log::info($result);
                }
                

            } else {
                //Load user stored              
                $user = User::with('member')->where("email", $request->input('email') )->first();
                //$member = Member::with('user')->where("user_id", $user->id )->first();

                $member = Member::with('user')->where("user_id", $user->id )->first();

                if ( $member->status == "inactive"){  
                    if ( $request->input('become_member')  ){                                  
                        $member->status = "active";
                        $member->save();

                        //send email confirmation
                        $confirmationToken = $user->generateEmailConfirmationToken();
                        
                        // Get web app token
                        $platform = Platform::where('name', 'web')->first();

                        $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;

                        $data = array(
                            'URL' => $url
                        );
                        

                        if ($mx->init()) {
                            $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                        } else {
                            $pj = new PendingJob();
                            $pj->payload = \GuzzleHttp\json_encode($data);
                            $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                'email' => $user->email,
                                'category' => 'confirmation',
                                'name' => $user->member->first_name . ' ' . $user->member->last_name
                            ));
                            $pj->queue = "email";
                            $pj->save();
                        }

                        $job = (new SendReminderNotification($user))->delay(60);
                        dispatch($job);

                        $user->email_confirmation_sent = Carbon::now(config('app.timezone'));
                        $user->save();

                        $this->getReward($request, $user);

                        //$result = $bepoz->VoucherIssue($voucherSetup->bepoz_voucher_setup_id, $member->bepoz_account_id, null, $value);
                        //$result = $bepoz->SystemCheck();
                        //Log::info($result);
                    }
                }
                
            }            
            DB::commit();
            
            $redeem_point_ratio = Setting::where('key', 'redeem_point_ratio')->first();
            if (is_null($redeem_point_ratio)) {
                throw new \Exception('Missing setting: redeem_point_ratio.');
            }

            $reward_point_ratio = Setting::where('key', 'reward_point_ratio')->first();
            if (is_null($reward_point_ratio)) {
                throw new \Exception('Missing setting: reward_point_ratio.');
            }

            $company_phone_number = Setting::where('key', 'company_phone')->first();
            if (is_null($company_phone_number)) {
                throw new \Exception('Missing setting: company_phone.');
            }

            $user_total = \GuzzleHttp\json_decode($request->input('total'));
            if (is_null($user_total)) {
                throw new \Exception('Invalid TOTAL JSON object.');
            }

            if ($order_type == "mix") {
                if (!(array_key_exists('cash', $user_total) && array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of cash and point required.');
                }
            } elseif ($order_type == "point") {
                if (!(array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of point required.');
                }
            } elseif ($order_type == "cash") {
                if (!(array_key_exists('cash', $user_total))) {
                    throw new \Exception('The amount of cash required.');
                }
            }

            $request->offsetSet('time', time());
            $request->offsetSet('transaction_type', 'place_order');
            $payload = \GuzzleHttp\json_encode($request->all());
            $token = md5($payload);

            $Order = new Order;
            $Order->payload = $payload;
            $Order->type = $order_type;
            $Order->token = $token;
            $Order->expired = time();
            $Order->member_id = $user->member->id;
            $Order->ip_address = $request->ip();
            $Order->venue_id = $listing->venue_id;
            $Order->listing_id = $request->input('listing_id');
            $Order->ordered_at = Carbon::now(config('app.timezone'));
            $Order->comments = $request->has('comments') ? $request->input('comments') : null;
            $Order->transaction_type = 'place_order';
            $Order->save();

            $orders = \GuzzleHttp\json_decode($request->input('order'));

            if (is_null($orders)) {
                throw new \Exception('Invalid ORDER JSON object.');
            }

            $actual_grand_total = 0;
            $grand_total = 0;
            $grand_total_quantity = 0;
            $grand_point_reward = 0;

            if (!is_array($orders)) {
                $orders = [$orders];
            }

            foreach ($orders as $obj) { //loop products
                if (isset($obj->qty)) {
                    $qty = intval($obj->qty);
                    if ($qty > 0) { // check order quantity
                        $grand_total_quantity += $qty;

                        $pdt = Product::with('product_type')->find($obj->product->id);

                        if (is_null($pdt)) {
                            throw new \Exception('Product not found');
                        }

                        if ($pdt->bepoz_voucher_setup_id === '0' || intval($pdt->bepoz_voucher_setup_id) === 0) {
                            throw new \Exception("Sorry for the inconvenience, unable to place order. Technical problem: Missing voucher setup id on this item.");
                        }

                        $product_name = $pdt->product_type->name;

                        $countMaxLimit = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimit)) {
                            $countMaxLimit = 0;
                        }

                        $countMaxLimitPerDay = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDay)) {
                            $countMaxLimitPerDay = 0;
                        }

                        $countMaxLimitPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerAccount)) {
                            $countMaxLimitPerAccount = 0;
                        }

                        $countMaxLimitPerDayPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDayPerAccount)) {
                            $countMaxLimitPerDayPerAccount = 0;
                        }

                        if (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! $countMaxLimit ");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }
                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        }

                        $orderDetail = new OrderDetail;
                        //set value
                        $orderDetail->voucher_name = is_null($pdt->voucher_setup) ? null : $pdt->voucher_setup->name;
                        $orderDetail->voucher_setups_id = $pdt->bepoz_voucher_setup_id;
                        $orderDetail->listing_id = $request->input('listing_id');
                        $orderDetail->product_name = $pdt->name;
                        $orderDetail->qty = $qty;
                        $orderDetail->order_id = $Order->id;
                        $orderDetail->status = "not_started";
                        $orderDetail->product_id = $pdt->id;
                        $orderDetail->product_type_id = $pdt->product_type->id;
                        $orderDetail->bepoz_product_number = $pdt->bepoz_product_number;
                        $orderDetail->category = is_null($pdt->category) ? 'ticket' : $pdt->category;
                        $orderDetail->venue_id = $listing->venue_id;

                        //set calculated value
                        if ($order_type == "cash") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = $qty * $orderDetail->unit_price;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total += $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                //exception for voucher with determined value and open value

                                $orderDetail->unit_price = $obj->product->unit_price;
                                $actual_total_cost = floatval($obj->product->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $actual_total_cost;
                                $actual_grand_total += $actual_total_cost;

                                $orderDetail->individual_discounted_unit_price = $obj->product->individual_discounted_unit_price;
                                $total_cost = floatval($obj->product->individual_discounted_unit_price) * $qty;
                                $grand_total += $total_cost;


                                /*
                                //if ( stripos($pdt->name, "open") > 0){
                                if ( str_contains($pdt->name, "Open") ){
                                  //$product_names .= $pdt->name.", ";
                                  //real price
                                  $orderDetail->unit_price = $obj->product->unit_price;
                                  $actual_total_cost = floatval($obj->product->unit_price) * $qty;
                                  $orderDetail->subtotal_unit_price = $actual_total_cost;
                                  $actual_grand_total += $actual_total_cost;

                                  //discounted price
                                  $orderDetail->individual_discounted_unit_price = $obj->product->individual_discounted_unit_price;
                                  $total_cost = floatval($obj->product->individual_discounted_unit_price) * $qty;
                                  //$orderDetail->paid_subtotal_unit_price = $total_cost;
                                  $grand_total += $total_cost;
                                } else {
                                  //$product_names .= $pdt->name.", ";
                                  $orderDetail->unit_price = $pdt->unit_price;
                                  $orderDetail->individual_discounted_unit_price = $pdt->unit_price;
                                  $total_cost = floatval($pdt->unit_price) * $qty;
                                  $orderDetail->subtotal_unit_price = $total_cost;
                                  $grand_total += $total_cost;
                                }
                                */
                                
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            }

                        } elseif ($order_type == "mix") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }

                            }


                        } elseif ($order_type == "point") {

                            if ($pdt->is_free) {
                                $orderDetail->point_price = 0;
                                $total_points = 0 * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            } else {
                                $orderDetail->point_price = $pdt->point_price;
                                $total_points = intval($pdt->point_price) * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            }


                        }

                        $orderDetail->save();

                    }
                }

            }

            if ($grand_total_quantity == 0) {
                throw new \Exception('No placing ticket booking has been made. No quantity');
            }

            if ($listing->max_limit != 0) {
                //only count the event ticket, not credit voucher
                if ((intval(OrderDetail::where('listing_id', $request->input('listing_id'))->where('status', 'successful')->where('product_type_id', '2')->sum('qty')) + intval($grand_total_quantity)) > intval($listing->max_limit)) {
                    throw new \Exception("Sorry, all products under $listing->heading are sold out.");
                }
            }

            if ($order_type == "point") {
                $Order->actual_total_point = $grand_total;
                if ($grand_total > intval($user->member->points)) {
                    throw new \Exception('You do not have any Point to redeem the item(s)');
                } else {

                    if ($user_total->point != $grand_total) {
                        throw new \Exception('Incorrect total amount of point');
                    }

                    $Order->total_point = $grand_total;
                }
            } elseif ($order_type == "mix") {
                $Order->actual_total_price = $grand_total;
                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $user_point = intval($user_total->point);
                $user_cash = floatval($user_total->cash);
                $Order->point_reward = $grand_point_reward;

                if ($user_point > 0) {

                    if ($user_point > intval($user->member->points)) {
                        throw new \Exception('You do not have any Point to redeem the item(s)');
                    }

                    $poinz = $user_point / intval($redeem_point_ratio->value);
                    $rough_total = $poinz + $user_cash;
                    if ($rough_total == $grand_total) {
                        $Order->total_price = $user_cash;
                        $Order->total_point = $user_point;
                        $Order->claimable_point_reward = $user_cash * intval($reward_point_ratio->value);

                    } else {
                        //point ratio from the system and total transaction are not the same
                        throw new \Exception("Mismatched total amount");
                    }

                } else {
                    $Order->total_price = $grand_total;
                    $Order->claimable_point_reward = $grand_total * intval($reward_point_ratio->value);
                }

            } elseif ($order_type == "cash") {
                $tax = 10;
                $paid_total_price_before_gst = $grand_total;
                //$gst = $grand_total * $tax / 100;
                $gst = $grand_total / 11;

                $gst44 = number_format($gst, 4, '.', '') + 0.004;
                
                $gstFinal = round($gst44, 2);

                //$grand_total = $paid_total_price_before_gst + $gstFinal;

                if ($user_total->cash != $grand_total) {
                    throw new \Exception('incorrect total amount of money');
                }

                //$Order->paid_total_price_before_gst = $paid_total_price_before_gst;
                $Order->gst = $gst;
                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $Order->actual_total_price = $actual_grand_total;
                $Order->total_price = $grand_total;
                $Order->point_reward = $grand_point_reward;
                $Order->claimable_point_reward = $grand_point_reward;
            }

            // update transaction record to send invoice
            
            //if total paid > 0 set successful
            //if total paid = 0 set pending
            //if total paid = 0, but event free, set successful
            if ($user_total->cash > 0){
              $Order->payment_status = "successful";
            } else {
              $Order->payment_status = "pending";
              if ($Order->total_price == 0) {
                $Order->payment_status = "successful";
              }
            }
            //$Order->order_status = "confirmed";
            $Order->order_status = "pending";
            $Order->save();

            DB::commit();
            
            //$job = (new SendReceiptEmail($user, $Order))->delay(150);
            //dispatch($job);
            

            return response()->json(['status' => 'ok', 'message' => 'Placing ticket booking is successful.', 
              'token' => $token, 
              'order_token' => $Order->token,               
              'total' => $request->input('total'), 'id' => $Order->id]);


        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            // $data = array(
            //     "title" => "WARNING: A customer is unable to place order!",
            //     "body" => $e->getMessage()
            // );

            // $job = (new SendPusherNotification('new-notification', $data));
            // dispatch($job);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Placing ticket booking is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Admin/BookingTicketController.php/createBooking';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
    }
    
      
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  CONFIRM BOOKING SAME AS TRANSACTION CONFIRM ORDER                                                  // 
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    protected function confirmBooking(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'order_token' => 'required',
                'option' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $order_token = $request->input('order_token');

            $order = Order::where('token', '=', $order_token)->first();

            if (is_null($order)) {
                throw new \Exception('order_token_not_valid');
            }

            if ($order->order_status == "cancelled") {
                throw new \Exception('order_already_cancelled');
            }

            if ($order->order_status == "confirmed") {
                throw new \Exception('order_already_confirmed');
            }

            if (Carbon::now(config('app.timezone'))->diffInMinutes(Carbon::createFromTimestamp($order->expired, config('app.timezone'))) > 30) {
                throw new \Exception('order_already_expired');
            }

            $result = 'Confirming order is successful.';

            if ($request->input('option') == "cancelled") {
                $ml = new MemberLog();
                $ml->member_id = $order->member->id;
                $ml->message = $order->member->first_name . '(' . $order->member->id . ') has cancelled the purchase. ';
                $ml->after = $order->toJson();
                $ml->save();

                $order->order_status = "cancelled";
                $order->payment_status = "cancelled";
                $order->voucher_status = "cancelled";
                $order->cancelled_at = Carbon::now(config('app.timezone'));
                $order->save();


            } elseif ($request->input('option') == "confirmed") {
                $order->voucher_status = "pending";
                $order->order_status = "confirmed";
                $order->payment_status = "successful";
                $order->confirmed_at = Carbon::now(config('app.timezone'));
                $order->paid_at = Carbon::now(config('app.timezone'));

                $member = Member::find($order->member_id);
                $payload = \GuzzleHttp\json_decode($order->payload);

                //$order_details = OrderDetail::where('order_id', $order->id)->get();

                if ($payload->transaction_type == 'place_order') {
                    //get order details with type ticket
                    $order_details = OrderDetail::where('order_id', $order->id)->
                    where('product_type_id', '2')->get();

                    if ( !is_null($order_details) ){
                        $ml = new MemberLog();
                        $ml->member_id = $order->member->id;
                        $ml->message = $order->member->first_name . '(' . $order->member->id . ') has placed an order (tickets). ';
                        $ml->after = $order->toJson();
                        $ml->save();
                    }

                    if ($order->type == "point") {

                        // Deduct points from member
                        $member = Member::find($order->member_id);

                        foreach ($order_details as $order_detail) {
                            $qty = intval($order_detail->qty);

                            $product = Product::find($order_detail->product_id);
                            $total_points = floatval($product->point_price) * $qty;

                            // Log
                            $pl = new PointLog();
                            $pl->member_id = $member->id;
                            $pl->points_before = $member->points;
                            $pl->points_after = floatval($member->points) - $total_points;
                            $pl->desc_short = "Points used for paying products.";
                            $pl->desc_long = "Used " . $total_points . " points for Transaction #" . $order->id . " (Point transaction)";
                            $pl->save();

                            // sync point
                            $data = array(
                                "point" => abs(floatval($total_points)),
                                "id" => $member->bepoz_account_id,
                                "member_id" => $member->id,
                                "status" => 'redeem'
                            );

                            $job = new BepozJob();
                            $job->queue = "sync-point";
                            $job->payload = json_encode($data);
                            $job->available_at = time();
                            $job->created_at = time();
                            $job->save();

                            $member->points = floatval($member->points) - $total_points;

                        }

                        $member->save();

                        $order->paid_total_point = $order->total_point;
                        $order->save();
                    } elseif ($order->type == "mix") {

                        if ($request->has('paypal_id')) {

                            $temp = Order::where('paypal_id', "=", $request->input('paypal_id'))->first();

                            if (!is_null($temp)) {
                                throw new \Exception('paypal_id_already_exist');
                            }

                            $order->paypal_id = $request->input('paypal_id');
                        } else {
                            throw new \Exception('no_paypal_id');
                        }

                        $order->paid_total_price = $order->total_price;
                        $order->paid_total_point = $order->total_point;

                        foreach ($order_details as $order_detail) {
                            $qty = intval($order_detail->qty);

                            if (floatval($order->actual_total_price) == 0) {
                                $discounted_individual_sale_price = 0;
                            } else {
                                $discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                            }

                            $order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                            $order_detail->paid_subtotal_unit_price = $discounted_individual_sale_price * $qty;
                            $order_detail->save();

                        }

                        $order->save();

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $member->id;
                        $pl->points_before = $member->points;
                        $pl->points_after = floatval($member->points) - floatval($order->total_point);
                        $pl->desc_short = "Points used for paying products.";
                        $pl->desc_long = "Used " . floatval($order->total_point) . " points for Transaction #" . $order->id . " (Mix transaction)";
                        $pl->save();

                        // sync point
                        $data = array(
                            "point" => abs(floatval($order->total_point)),
                            "id" => $member->bepoz_account_id,
                            "member_id" => $member->id,
                            "status" => 'redeem'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $member->points = floatval($member->points) - floatval($order->total_point);

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $member->id;
                        $pl->points_before = $member->points;
                        $pl->points_after = floatval($member->points) + floatval($order->claimable_point_reward);
                        $pl->desc_short = "Point rewards gained by buying products.";
                        $pl->desc_long = "Gained " . floatval($order->claimable_point_reward) . " points from Transaction #" . $order->id . " (Mix transaction)";
                        $pl->save();

                        $member->points = floatval($member->points) + floatval($order->claimable_point_reward);

                        // sync point
                        $data = array(
                            "point" => abs(floatval($order->claimable_point_reward)),
                            "id" => $member->bepoz_account_id,
                            "member_id" => $member->id,
                            "status" => 'earn'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $member->save();

                    } elseif ($order->type == "cash") {

                        if ($request->has('paypal_id')) {
                            $temp = Order::where('paypal_id', "=", $request->input('paypal_id'))->first();

                            if (!is_null($temp)) {
                                throw new \Exception('paypal_id_already_exist');
                            }

                            $order->paypal_id = $request->input('paypal_id');
                        } else {
                            throw new \Exception('no_paypal_id');
                        }


                        foreach ($order_details as $order_detail) {
                            $qty = intval($order_detail->qty);

                            //if free
                            if (floatval($order->actual_total_price) == 0) {
                                $discounted_individual_sale_price = 0;
                            } else {
                                //$discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                                
                                //if order detail has individual_discounted_unit_price

                            }

                            //$order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                            $order_detail->paid_subtotal_unit_price = $qty * $order_detail->individual_discounted_unit_price;
                            $order_detail->save();

                        }

                        $order->paid_total_price = $order->total_price;
                        $order->save();

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $member->id;
                        $pl->points_before = $member->points;
                        $pl->points_after = floatval($member->points) + floatval($order->claimable_point_reward);
                        $pl->desc_short = "Point rewards gained by buying products.";
                        $pl->desc_long = "Gained " . floatval($order->claimable_point_reward) . " points from Transaction #" . $order->id . " (Cash transaction)";
                        $pl->save();

                        $member->points = floatval($member->points) + floatval($order->claimable_point_reward);

                        // sync point
                        $data = array(
                            "point" => abs(floatval($order->claimable_point_reward)),
                            "id" => $member->bepoz_account_id,
                            "member_id" => $member->id,
                            "status" => 'earn'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $member->save();

                    }

                    // create ticket(vouchers)
                    foreach ($order_details as $order_detail) {

                        if ($order_detail->voucher_setups_id !== 0) {
                            $qty = intval($order_detail->qty);

                            // ------------------------
                            for ($i = 0; $i < $qty; $i++) {

                                $memberVoucher = new MemberVouchers();
                                $memberVoucher->name = $order_detail->voucher_setup->name;
                                $memberVoucher->member_id = $member->id;
                                $memberVoucher->order_id = $order->id;
                                $memberVoucher->order_details_id = $order_detail->id;
                                $memberVoucher->lookup = "100000000";
                                $memberVoucher->barcode = "";
                                $memberVoucher->amount_left = 100;
                                $memberVoucher->amount_issued = 100;
                                $memberVoucher->voucher_setup_id = $order_detail->voucher_setups_id;
                                $memberVoucher->category = $order_detail->category;
                                $memberVoucher->save();

                                // ----------------------

                                // bepoz job - issue voucher
                                $data = array(
                                    "member_id" => $member->id,
                                    "member_voucher_id" => $memberVoucher->id,
                                    "voucher_setups_id" => $order_detail->voucher_setups_id,
                                    "order_id" => $order->id,
                                    "order_details_id" => $order_detail->id
                                );

                                $job = new BepozJob();
                                $job->queue = "issue-voucher";
                                $job->payload = \GuzzleHttp\json_encode($data);
                                $job->available_at = time();
                                $job->created_at = time();
                                $job->processing_date = null;
                                $job->save();
                            }
                        } else {
                            $order_detail->status = 'successful';
                        }

                    }

                    //$user = User::find($member->user_id);

                    //if order is booking ticket, send different email
                    //if ( $order->listing_id == 1 ){
                    //  $job = (new SendBookingTicketEmail($user, $order))->delay(150);                    
                    //} else {                      
                    //  $job = (new SendReceiptEmail($user, $order))->delay(150);
                    //}
                    
                    
                    //dispatch($job);

                //} elseif ($payload->transaction_type == 'gift_certificate') {
                               

                    /*
                    if ($request->has('paypal_id')) {
                        $temp = Order::where('paypal_id', "=", $request->input('paypal_id'))->first();

                        if (!is_null($temp)) {
                            throw new \Exception('gift_paypal_id_already_exist');
                        }

                        $order->paypal_id = $request->input('paypal_id');
                    } else {
                        throw new \Exception('no_paypal_id');
                    }
                    */

                    $order->paid_total_price = $order->total_price;

                    //get order details with type credit voucher
                    $order_details = OrderDetail::where('order_id', $order->id)->
                        where('product_type_id', '3')->get();

                    if ( !is_null($order_details) ){
                        $ml = new MemberLog();
                        $ml->member_id = $order->member->id;
                        $ml->message = $order->member->first_name . '(' . $order->member->id . ') has placed a gift certificate. ';
                        $ml->after = $order->toJson();
                        $ml->save();         
                    }

                    foreach ($order_details as $order_detail) {
                        $qty = intval($order_detail->qty);

                        /*
                        if (floatval($order->actual_total_price) == 0) {
                            $discounted_individual_sale_price = 0;
                        } else {
                            $discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                        }
                        */

                        //$order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                        $order_detail->paid_subtotal_unit_price = $qty * $order_detail->individual_discounted_unit_price;
                        $order_detail->save();                        

                        //$product_payload = \GuzzleHttp\json_decode($order_detail->payload);
                        $product_payload = Product::where("id", $order_detail->product_id)->first();

                        for ($i = 0; $i < $qty; $i++) {
                            $gf = new GiftCertificate();
                            $gf->venue_id = $order->venue_id;
                            $gf->voucher_setups_id = $order_detail->voucher_setups_id;
                            $gf->order_details_id = $order_detail->id;
                            $gf->order_id = $order->id;
                            $gf->purchaser_id = $member->id;
                            //$gf->full_name = $payload->gc_full_name;
                            //$gf->email = $payload->gc_email;
                            //$gf->note = $payload->gc_note;
                            $gf->full_name = $order->member->first_name." ".$order->member->last_name;
                            $gf->email = $order->member->user->email;
                            //$gf->note = $payload->gc_note;
                            $gf->value = $product_payload->gift_value;

                            if ( str_contains($order_detail->product_name, "Open") ){
                                $gf->value = $order_detail->unit_price;
                            }

                            $gf->save();

                            // ----------------------

                            // bepoz job - issue voucher
                            $data = array(
                                "member_id" => $member->id,
                                "gift_certificate_id" => $gf->id,
                                "voucher_setups_id" => $order_detail->voucher_setups_id,
                                "value" => $product_payload->gift_value,
                                "order_id" => $order->id,
                                "order_details_id" => $order_detail->id
                            );

                            $gf->payload = json_encode($data);
                            $gf->save();

                            $job = new BepozJob();
                            //$job->queue = "issue-gift-certificate";
                            $job->queue = "issue-gift-certificate-booking";
                            $job->payload = json_encode($data);
                            $job->available_at = time();
                            $job->created_at = time();
                            $job->save();

                            //$user = User::find($member->user_id);
                            //$job = (new SendReceiptEmail($user, $order))->delay(150);
                            //$job = (new SendBookingTicketEmail($user, $order))->delay(150);
                            //dispatch($job);
                        }
                        
                    }
                }
                
                $user = User::find($member->user_id);
                $job = (new SendBookingTicketEmail($user, $order))->delay(150);
                $emailResult = dispatch($job);
                
                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {
                    $result .= ' Email sent.';
                } else if ($email_server === "mailjet") {
                    if ($emailResult === "Email sent") {
                        $result .= ' '.$emailResult.'.';
                    } else {
                        $result .= ' Email send failed, please try resend email.';
                    }
                }

                // friend referral
                $fr = FriendReferral::where('new_member_id', $member->id)
                    ->where('is_accepted', true)
                    ->where('has_joined', true)
                    ->where('reward_option', 'after_purchase')
                    ->where('has_made_a_purchase', false)
                    ->first();

                if (!is_null($fr)) {
                    $fr->made_purchase_at = Carbon::now(config('app.timezone'));
                    $fr->has_made_a_purchase = true;
                    $fr->save();
                }

                // create transaction

                $data = array(
                    "member_id" => $member->id,
                    "order_id" => $order->id
                );

                $job = new BepozJob();
                $job->queue = "create-transaction";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();

            }

            DB::commit();

            if ($request->input('option') == "cancelled") {
                return response()->json(['status' => 'ok', 'message' => 'Cancelled order is successful.']);
            } else {
                if ($order->type == "point") {
                    return response()->json(['status' => 'ok', 'message' => $result]);
                } else {
                    return response()->json(['status' => 'ok', 'message' => $result, 'reward' => $order->claimable_point_reward]);
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Confirming order is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Admin/BookingTicketController.php/confirmBooking';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //  UPDATE BOOKING BASED FROM CREATE BOOKING                                                                        // 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    protected function updateBooking(Request $request)
    {
      try {
            $validator = Validator::make($request->all(), [
                'order_id' => 'required',
                'order' => 'required',
                'type' => 'required',
                'total' => 'required',
                'listing_id' => 'required',
                //credential
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'mobile' => 'required',
                'become_member' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::find($request->input('listing_id'));

            if (is_null($listing)) {
                throw new \Exception('Listing not found.');
            }

            DB::beginTransaction();

            $order_type = $request->input('type');

            if (strcmp($order_type, 'cash') !== 0) {
                if (strcmp($order_type, 'point') !== 0) {
                    if (strcmp($order_type, 'mix') !== 0) {
                        return response()->json(['status' => 'error', 'message' => 'invalid order type'], Response::HTTP_BAD_REQUEST);
                    }
                }
            }
            
            //default
            $member = new Member;

            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $email = $request->input('email');
            $mobile = $request->input('mobile');
            $become_member = $request->input('become_member');
            $member_status = "inactive";
            $member_password = "12345678";            
            
            if ($request->has('become_member')) {
              if ( $request->input('become_member') ) {
                 $member_status = "active";
              }
            }
            
            //check user exist exact by email
            $memberSearch = Member::whereHas('user', function ($q) use ($request) {
                //$q->where('email', 'like', '%' . $request->input('email') . '%');
                $q->where('email', $request->input('email') );
            })->first();
            
            //$memberSearchs = $memberSearch->with('user')->get();
            
            //if user does not exist + 
            if ( is_null ($memberSearch) ){
              //if become member true, then create member
              if ( $request->input('become_member')  ){                
                $member_status = "active";
              } else {
                $member_status = "inactive";
              }
              
              //make new user
              $user = new User;
              $user->email = $request->input('email');
              $user->password = Hash::make($member_password);
              $user->status = $member_status;
              $user->mobile = $request->input('mobile');
              $user->save();
              
              //make new member
              $member = new Member;
              $member->user_id = $user->id;
              $member->first_name = $request->input('first_name');
              $member->last_name = $request->input('last_name');
              $member->status = $member_status;
              $member->save();
              
              //Load user has been created
              $user = User::with('member')->find($member->user_id);
              
            } else {
              //Load user stored
              $user = User::with('member')->where("email", $request->input('email') )->first();
              //$member = Member::with('user')->where("user_id", $user->id )->first();
              $user->email = $request->input('email');
              $user->status = $member_status;
              $user->mobile = $request->input('mobile');
              $user->save();
              
              //Load member stored
              $member = Member::where("user_id", $user->id )->first();
              $member->first_name = $request->input('first_name');
              $member->last_name = $request->input('last_name');
              $member->status = $member_status;
              $member->save();
            }            
                  
            
            $redeem_point_ratio = Setting::where('key', 'redeem_point_ratio')->first();
            if (is_null($redeem_point_ratio)) {
                throw new \Exception('Missing setting: redeem_point_ratio.');
            }

            $reward_point_ratio = Setting::where('key', 'reward_point_ratio')->first();
            if (is_null($reward_point_ratio)) {
                throw new \Exception('Missing setting: reward_point_ratio.');
            }

            $company_phone_number = Setting::where('key', 'company_phone')->first();
            if (is_null($company_phone_number)) {
                throw new \Exception('Missing setting: company_phone.');
            }

            $user_total = \GuzzleHttp\json_decode($request->input('total'));
            if (is_null($user_total)) {
                throw new \Exception('Invalid TOTAL JSON object.');
            }

            if ($order_type == "mix") {
                if (!(array_key_exists('cash', $user_total) && array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of cash and point required.');
                }
            } elseif ($order_type == "point") {
                if (!(array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of point required.');
                }
            } elseif ($order_type == "cash") {
                if (!(array_key_exists('cash', $user_total))) {
                    throw new \Exception('The amount of cash required.');
                }
            }

            $request->offsetSet('time', time());
            $request->offsetSet('transaction_type', 'place_order');
            $payload = \GuzzleHttp\json_encode($request->all());
            $token = md5($payload);

            
            $id = $request->input('order_id');
            //$Order = new Order;
            //get object to update
            $Order = Order::find($id);
            
            $Order->payload = $payload;
            $Order->type = $order_type;
            $Order->token = $token;
            $Order->expired = time();
            $Order->member_id = $user->member->id;
            $Order->ip_address = $request->ip();
            $Order->venue_id = $listing->venue_id;
            $Order->listing_id = $request->input('listing_id');
            $Order->ordered_at = Carbon::now(config('app.timezone'));
            $Order->comments = $request->has('comments') ? $request->input('comments') : null;
            $Order->transaction_type = 'place_order';
            $Order->save();

            $orders = \GuzzleHttp\json_decode($request->input('order'));

            if (is_null($orders)) {
                throw new \Exception('Invalid ORDER JSON object.');
            }

            $grand_total = 0;
            $grand_total_quantity = 0;
            $grand_point_reward = 0;

            if (!is_array($orders)) {
                $orders = [$orders];
            }
            
            //get object detail from database to update
            $oldOrderDetails = OrderDetail::where('order_id', $Order->id)->get();
            
            //collect all product id from request
            $productIDFromRequest = [];
            
            foreach ($orders as $obj) { //loop products
                if (isset($obj->qty)) {
                    $qty = intval($obj->qty);
                    if ($qty > 0) { // check order quantity
                        $grand_total_quantity += $qty;

                        $pdt = Product::with('product_type')->find($obj->product->id);

                        if (is_null($pdt)) {
                            throw new \Exception('Product not found');
                        }

                        if ($pdt->bepoz_voucher_setup_id === '0' || intval($pdt->bepoz_voucher_setup_id) === 0) {
                            throw new \Exception("Sorry for the inconvenience, unable to place order. Technical problem: Missing voucher setup id on this item.");
                        }

                        $product_name = $pdt->product_type->name;

                        $countMaxLimit = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimit)) {
                            $countMaxLimit = 0;
                        }

                        $countMaxLimitPerDay = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDay)) {
                            $countMaxLimitPerDay = 0;
                        }

                        $countMaxLimitPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerAccount)) {
                            $countMaxLimitPerAccount = 0;
                        }

                        $countMaxLimitPerDayPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDayPerAccount)) {
                            $countMaxLimitPerDayPerAccount = 0;
                        }

                        if (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }
                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        }
                        
                        //collecting product id inside products request loop
                        $productIDFromRequest[] = $pdt->id;
                        
                        //Load orderDetail with same order_id and product_id
                        $orderDetail = OrderDetail::where('order_id', $Order->id)
                          ->where('product_id', $pdt->id)->first();
                        
                        
                        
                        //if orderDetail has product_id in products loop, edit the orderDetail->qty
                        //if product_id in products loop not found (not inserted) in orderDetail, 
                        //  then create new orderDetail
                        //else
                        //  then if orderDetail found (inserted but cancelled), activate it again and orderDetail->qty
                        
                        //if orderDetail with same order_id and product_id not found, 
                        if (is_null($orderDetail)) {
                          //create new one
                          $orderDetail = new OrderDetail;
                        } else {
                          //if orderDetail found (inserted but cancelled), activate it again 
                          if ( $orderDetail->status == "cancelled" ){
                            $orderDetail->status = "cancelled";
                          }                          
                        }
                        
                        $orderDetail->voucher_name = is_null($pdt->voucher_setup) ? null : $pdt->voucher_setup->name;
                        $orderDetail->voucher_setups_id = $pdt->bepoz_voucher_setup_id;
                        $orderDetail->listing_id = $request->input('listing_id');
                        $orderDetail->product_name = $pdt->name;
                        $orderDetail->qty = $qty;
                        $orderDetail->order_id = $Order->id;
                        $orderDetail->status = "pending";
                        $orderDetail->product_id = $pdt->id;
                        $orderDetail->product_type_id = $pdt->product_type->id;
                        $orderDetail->bepoz_product_number = $pdt->bepoz_product_number;
                        $orderDetail->category = is_null($pdt->category) ? 'ticket' : $pdt->category;
                        $orderDetail->venue_id = $listing->venue_id;

                        if ($order_type == "cash") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            }

                        } elseif ($order_type == "mix") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }

                            }


                        } elseif ($order_type == "point") {

                            if ($pdt->is_free) {
                                $orderDetail->point_price = 0;
                                $total_points = 0 * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            } else {
                                $orderDetail->point_price = $pdt->point_price;
                                $total_points = intval($pdt->point_price) * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            }


                        }

                        $orderDetail->save();

                    }
                }

            }

            if ($grand_total_quantity == 0) {
                throw new \Exception('No placing ticket booking has been made. No quantity');
            }

            if ($listing->max_limit != 0) {
                if ((intval(OrderDetail::where('listing_id', $request->input('listing_id'))->where('status', 'successful')->sum('qty')) + intval($grand_total_quantity)) > intval($listing->max_limit)) {
                    throw new \Exception("Sorry, all products under $listing->heading are sold out.");
                }
            }

            if ($order_type == "point") {
                $Order->actual_total_point = $grand_total;
                if ($grand_total > intval($user->member->points)) {
                    throw new \Exception('You do not have any Point to redeem the item(s)');
                } else {

                    if ($user_total->point != $grand_total) {
                        throw new \Exception('Incorrect total amount of point');
                    }

                    $Order->total_point = $grand_total;
                }
            } elseif ($order_type == "mix") {
                $Order->actual_total_price = $grand_total;
                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $user_point = intval($user_total->point);
                $user_cash = floatval($user_total->cash);
                $Order->point_reward = $grand_point_reward;

                if ($user_point > 0) {

                    if ($user_point > intval($user->member->points)) {
                        throw new \Exception('You do not have any Point to redeem the item(s)');
                    }

                    $poinz = $user_point / intval($redeem_point_ratio->value);
                    $rough_total = $poinz + $user_cash;
                    if ($rough_total == $grand_total) {
                        $Order->total_price = $user_cash;
                        $Order->total_point = $user_point;
                        $Order->claimable_point_reward = $user_cash * intval($reward_point_ratio->value);

                    } else {
                        //point ratio from the system and total transaction are not the same
                        throw new \Exception("Mismatched total amount");
                    }

                } else {
                    $Order->total_price = $grand_total;
                    $Order->claimable_point_reward = $grand_total * intval($reward_point_ratio->value);
                }

            } elseif ($order_type == "cash") {

                if ($user_total->cash != $grand_total) {
                    throw new \Exception('incorrect total amount of money');
                }

                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $Order->actual_total_price = $grand_total;
                $Order->total_price = $grand_total;
                $Order->point_reward = $grand_point_reward;
                $Order->claimable_point_reward = $grand_point_reward;
            }

            // update transaction record to send invoice
            
            //if total paid > 0 set successful
            //if total paid = 0 set pending
            //if total paid = 0, but event free, set successful
            if ($user_total->cash > 0){
              $Order->payment_status = "successful";
            } else {
              $Order->payment_status = "pending";
              if ($Order->total_price == 0) {
                $Order->payment_status = "successful";
              }
            }
            $Order->order_status = "pending";
            $Order->save();
            
            //all order detail stored in database, with this order id, and product id which not found in request
            //set their status to cancelled              
            OrderDetail::where('order_id', $Order->id)->whereNotIn('product_id', $productIDFromRequest)
              ->update(['status' => 'cancelled', 'qty' => '0' ]);
            
            //if all order detail is cancelled, order also cancelled

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Update booking is successful.', 
              'token' => $token, 
              'order_token' => $Order->token,
              'total' => $request->input('total'), 'id' => $Order->id]);


        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            // $data = array(
            //     "title" => "WARNING: A customer is unable to place order!",
            //     "body" => $e->getMessage()
            // );

            // $job = (new SendPusherNotification('new-notification', $data));
            // dispatch($job);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Update booking is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Admin/BookingTicketController.php/updateBooking';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
      
    }
    
    protected function cancelBooking(Request $request)
    {
      try {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }

        DB::beginTransaction();

        $id = $request->input('order_id');
        $Order = Order::find($id);            

        $Order->order_status = 'cancelled';            
        $Order->cancelled_at =  Carbon::now(config('app.timezone'));
        $Order->save();

        DB::commit();
        $token = $Order->token;

        return response()->json(['status' => 'ok', 'message' => 'Cancel booking is successful.', 'token' => $token, 'total' => $request->input('total'), 'id' => $Order->id]);

      } catch (\Exception $e) {
          DB::rollBack();
          Log::error($e);

        //   $data = array(
        //       "title" => "WARNING: A customer is unable to place order!",
        //       "body" => $e->getMessage()
        //   );

        //   $job = (new SendPusherNotification('new-notification', $data));
        //   dispatch($job);

          $log = new SystemLog();
          $log->type = 'user-transaction-error';
          $log->humanized_message = 'Cancel booking is failed. Please check the error message';
          $log->message = $e;
          $log->source = 'Admin/BookingTicketController.php/cancelBooking';
          $log->save();

          return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

      }
      
    }
    
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //  RESEND EMAIL BOOKING                                                                                            // 
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    protected function resendBooking(Request $request)
    {
      try {
            $validator = Validator::make($request->all(), [
                'order_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $id = $request->input('order_id');
            $Order = Order::find($id);
            
            $token = $Order->token;
            $member = Member::find($Order->member_id);
            $user = User::find($member->user_id);

            //$job = (new SendReceiptEmail($user, $Order))->delay(150);
            $job = (new SendBookingTicketEmail($user, $Order))->delay(150); 
            $emailResult = dispatch($job);
            $result = '';

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;
            
            if ($email_server === "mandrill") {
                $result = 'Resend booking is successful.';
            } else if ($email_server === "mailjet") {
                if ($emailResult === "Email sent") {
                    $result = 'Resend booking is successful.';
                } else {
                    $result = 'Resend booking failed, please try again';
                }
            }

            return response()->json(['status' => 'ok', 'message' => $result, 'token' => $token, 'total' => $request->input('total'), 'id' => $Order->id]);

        } catch (\Exception $e) {
            Log::error($e);

            // $data = array(
            //     "title" => "WARNING: A customer is unable to place order!",
            //     "body" => $e->getMessage()
            // );

            // $job = (new SendPusherNotification('new-notification', $data));
            // dispatch($job);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Resend booking is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Admin/BookingTicketController.php/resendBooking';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
        
    }
      
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //  FIND ALL BOOKING                                                                                                // 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
    protected function findBooking(Request $request)
    {
      try {
        
        $paginate_number = 10;
        if ($request->has('paginate_number')) {
            $paginate_number = $request->input('paginate_number');
        }
        
        $orders = Order::with('member.user',          
          'order_details.member_vouchers', 
          //'payment_log',
          'listing.products.product');
        
        $listing_id = null;
        $order_id = null;
        $email = null;
        $phone_number = null;
        $name = null;
        
        //make sure input not empty
        if ( $request->has('listing_id') && $request->input('listing_id') > -1 ){
          $listing_id = $request->input('listing_id');
        }
        
        if ( $request->has('order_id') && $request->input('order_id') !== '' ){
          $order_id = $request->input('order_id');
        }
        
        if ( $request->has('email') && $request->input('email') !== '' ){
          $email = $request->input('email');
        }
        
        if ( $request->has('phone_number') && $request->input('phone_number') !== '' ){
          $phone_number = $request->input('phone_number');
        }
        
        if ( $request->has('name') && $request->input('name') !== '' ){
          $name = $request->input('name');
        }
        
        
        /*
        if ( $listing_id && $order_id && $email && $phone_number && $name ) { 
          //Combination 1 
          $orders = $orders->where('listing_id', $request->input('listing_id'));          
        } else if ( $listing_id && $order_id && $email && $phone_number ) {
          //Combination 2 
          
          $orders = $orders->where('listing_id', $request->input('listing_id'));       
        } else if ( $listing_id && $order_id && $email && $name ) {
          //Combination 3
          $orders = $orders->where('listing_id', $request->input('listing_id'));       
        } else if ( $listing_id && $order_id && $phone_number && $name ) {
          //Combination 4
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $listing_id && $email && $phone_number && $name ) {
          //Combination 5
          $orders = $orders->where('listing_id', $request->input('listing_id'));
          
        } else if ( $order_id && $email && $phone_number && $name ) {
          //Combination 6
          
        } else if ( $listing_id && $order_id && $email ) {          
          //Combination 7
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $listing_id && $order_id && $name  ) {
          //Combination 8
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $listing_id && $phone_number && $name ) {
          //Combination 9
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $email && $phone_number && $name ) {
          //Combination 10
          
        } else if ( $listing_id && $order_id ) {
          //Combination 11
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $listing_id && $email ) {
          //Combination 12
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $listing_id && $phone_number ) {          
          //Combination 13
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $listing_id && $name ) {          
          //Combination 14
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $order_id && $email ) {          
          //Combination 15
          
        } else if ( $order_id && $phone_number ) {          
          //Combination 16
          
        } else if ( $listing_id && $name ) {          
          //Combination 17
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $email && $phone_number) {          
          //Combination 18
          
        } else if ( $email && $name ) {          
          //Combination 19
          
        } else if ($phone_number && $name ) {          
          //Combination 20
          
        } else if ( $listing_id ) {          
          //Combination 21
          $orders = $orders->where('listing_id', $request->input('listing_id'));
        } else if ( $order_id ) {
          //Combination 22
          
        } else if ( $email ) {
          //Combination 23
          
        } else if ( $phone_number ) {
          //Combination 24
          
        } else if ( $name ) {
          //Combination 25
          
        }
        */
        
        $query_include = "";
        if ( !empty($listing_id) ) {          
          //Combination 21
          $orders = $orders->where('listing_id', $request->input('listing_id'));
          $query_include .= "listing_id";
        }
        
        if ( !empty($order_id) ) {
          //Combination 22
          $orders = $orders->where('id', $request->input('order_id'));
          $query_include .= " order_id";
        }
        
        if ( !empty($email) ) {
          //Combination 23
          $orders = $orders->whereHas('member.user', function ($q1) use ($request) {
            $q1->where('email', 'like', '%' . $request->input('email') . '%' );                
          });
          
          $query_include .= " email";
        }
        
        if ( !empty($phone_number) ) {
          //Combination 24
          $orders = $orders->whereHas('member.user', function ($q1) use ($request) {
            $q1->where('mobile', 'like', '%' . $request->input('phone_number') . '%' );                
          });
          $query_include .= " phone_number";
        }
        
        if ( !empty($name) ) {
          //Combination 25
          $orders = $orders->whereHas('member.user', function ($q1) use ($request) {
              //$q1->where('mobile', $request->input('name') );
              
              $q1->orWhere(function ($query) use ($request) {
                if (strpos($request->input('name'), ' ') !== false) {
                  $array = explode(" ", $request->input('name'));
                  $keyword = implode("%", $array);
                  $query->where('first_name', 'like', '%' . $keyword . '%');
                } else {
                  $query->where('first_name', 'like', '%' . $request->input('name') . '%');
                }
              });

              $q1->orWhere(function ($query) use ($request) {
                if (strpos($request->input('name'), ' ') !== false) {
                  $array = explode(" ", $request->input('name'));
                  $keyword = implode("%", $array);
                  $query->where('last_name', 'like', '%' . $keyword . '%');
                } else {
                  $query->where('last_name', 'like', '%' . $request->input('name') . '%');
                }
              });
            });
            
          $query_include .= "name";
            
        }

        //Log::info( $orders->toSql() );
        //Log::info( $orders->getBindings() );
        $orders = $orders->paginate($paginate_number);
                        
        foreach ($orders as $row) {
          //add quantity sum for each order
          $row->total_qty = $row->order_details_success->sum('qty');
          //$row->become_member = $row->member->status;
                    
          //product_id
          //confirmed or pending counted as total_ticket
          //$row->listing->total_ticket = $row->listing->order_details_success->sum('qty');
          //$row->listing->ticket_remaining = $row->listing->max_limit==0 ? 'Unlimited' :  ($row->listing->max_limit - $row->listing->total_ticket);            
            
          $row->credit_voucher = Product::where('product_type_id', 3)->get();

          /*
          if ( $row->credit_voucher > 0){
            $credit_voucher->discount = ( $credit_voucher->gift_value - $credit_voucher->unit_price ) / $credit_voucher->gift_value * 100 / 1;                
          } else {
            $credit_voucher->discount = 0;
          }
          */
        }
        
        /*
          foreach ($listings as $listing) {
            $listing->total_ticket = $listing->order_details_success->sum('qty');
            
            //->where('status', 'pending')->orWhere('status', 'in_progress')->orWhere('status', 'successful')
            
            $listing->ticket_remaining = $listing->max_limit==0 ? 'Unlimited' :  ($listing->max_limit - $listing->total_ticket);            
                        
            //product with F&B Voucher type
            $listing->credit_voucher = Product::where('product_type_id', 3)->get();
          }
         */
        
        if (is_null($orders)) {
            return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['status' => 'ok', 'query_include' => $query_include, 'data' => $orders]);
        }
      } catch (\Exception $e) {
        $log = new SystemLog();
        $log->type = 'user-transaction-error';
        $log->humanized_message = 'Find booking is failed. Please check the error message';
        $log->message = $e;
        $log->source = 'Admin/BookingTicketController.php/findBooking';
        $log->save();

        return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
      }
    }
    
        
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //  GET 1 BOOKING                                                                        // 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
    protected function getBooking(Request $request)
    {
      try {
        if ($request->has('id')) {
          $orders = Order::with('member','order_details','order_details.member_vouchers','listing')
            ->where('id', $request->input('id'))->get();          
        }
        
        if (is_null($orders)) {
            return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
        } else {
            return response()->json(['status' => 'ok', 'data' => $orders]);
        }
      } catch (\Exception $e) {
        $log = new SystemLog();
        $log->type = 'user-transaction-error';
        $log->humanized_message = 'Get booking is failed. Please check the error message';
        $log->message = $e;
        $log->source = 'Admin/BookingTicketController.php/getBooking';
        $log->save();

        Log::error($e);
        return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
      }
    }
    
    
    
    
    private function getReward($request, $user)
    {
        $option = Setting::where('key', 'reward_after_signup_option')->first();
        if (is_null($option)) {
            return false;
        }

        if ($option->value == 'false') {
            return false;
        }

        $type = Setting::where('key', 'reward_after_signup_type')->first();
        if (is_null($type)) {
            return false;
        }

        if ($type->value == 'point') {
            return false;
        }

        $voucher = Setting::where('key', 'reward_after_signup_voucher_setups_id')->first();
        if (is_null($voucher)) {
            return false;
        }

        $voucherSetup = VoucherSetups::find($voucher->value);
        if (is_null($voucherSetup)) {
            return false;
        }

        $request->offsetSet('time', time());
        $request->offsetSet('transaction_type', 'signup_reward');
        $payload = \GuzzleHttp\json_encode($request->all());
        $token = md5($payload);

        $Order = new Order();
        $Order->type = "cash";
        $Order->expired = time();
        $Order->member_id = $user->member->id;
        $Order->ordered_at = Carbon::now(config('app.timezone'));
        $Order->voucher_status = 'successful';
        $Order->payment_status = 'successful';
        $Order->order_status = 'confirmed';
        $Order->payload = $payload;
        $Order->token = $token;
        $Order->ip_address = '0.0.0.0';
        $Order->transaction_type = 'signup_reward';
        $Order->save();

        $od = new OrderDetail();
        $od->voucher_name = $voucherSetup->name;
        $od->product_name = $voucherSetup->name;
        $od->voucher_setups_id = $voucherSetup->id;
        $od->order_id = $Order->id;
        $od->qty = 1;
        $od->product_type_id = 1;
        $od->category = 'signup_reward';
        $od->status = 'successful';
        $od->save();

        $memberVoucher = new MemberVouchers();
        $memberVoucher->name = $voucherSetup->name;
        $memberVoucher->member_id = $user->member->id;;
        $memberVoucher->lookup = "100000000";
        $memberVoucher->barcode = "";
        $memberVoucher->order_id = $Order->id;
        $memberVoucher->order_details_id = $od->id;
        $memberVoucher->voucher_setup_id = $voucherSetup->id;
        $memberVoucher->amount_left = 100;
        $memberVoucher->amount_issued = 100;
        $memberVoucher->category = 'signup_reward';
        $memberVoucher->save();

        // ----------------------

        // bepoz job - issue voucher
        $data = array(
            "member_id" => $user->member->id,
            "member_voucher_id" => $memberVoucher->id,
            "voucher_setups_id" => $voucherSetup->id,
        );

        $job = new BepozJob();
        $job->queue = "issue-signup-reward-bepoz-voucher";
        $job->payload = \GuzzleHttp\json_encode($data);
        $job->available_at = time();
        $job->created_at = time();
        $job->save();

        $company_name = Setting::where('key', 'company_name')->first()->value;

        $data = [
            // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
            "contents" => ["en" => "Please check your voucher section, you get a complement!"],
            "headings" => ["en" => $company_name],
            "data" => ["category" => "email"],
            "ios_badgeType" => "Increase",
            "ios_badgeCount" => 1
        ];

        //$job = (new SendOneSignalNotification($data))->delay(360);
        //dispatch($job);

        $ml = new MemberLog();
        $ml->member_id = $user->member->id;
        $ml->message = "This member got sign-up rewards ($voucherSetup->name). ";
        $ml->after = \GuzzleHttp\json_encode([
            'voucher_setups_id' => $voucherSetup->id,
            'voucher_name' => $voucherSetup->name
        ]);
        $ml->save();

    }

}
