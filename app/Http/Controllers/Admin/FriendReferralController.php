<?php

namespace App\Http\Controllers\Admin;

use App\FriendReferral;
use App\Http\Controllers\Controller;
use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class FriendReferralController extends Controller
{
    /**
     * Return all friendReferrals (pagination)
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateFriendReferral(Request $request)
    {
        try {

            $friendReferrals = new FriendReferral;
            $obj = $friendReferrals->newQuery();

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('email', 'like', '%' . $keyword . '%');
                        $query->orWhere('full_name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('email', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('full_name', 'like', '%' . $request->input('keyword') . '%');
                    }
                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            // $friendReferrals = FriendReferral::with('new_member')
            //     ->select(DB::raw('count(email) as referral_count, new_member_id, email'))
            //     ->groupBy('email')
            //     ->paginate($paginate_number);

            $friendReferrals = $obj->with('new_member')
                ->select(DB::raw('count(email) as referral_count, new_member_id, email'))
                ->groupBy('email')
                ->paginate($paginate_number);

            foreach ($friendReferrals as $friendReferral) {
                if ($friendReferral->new_member_id !== 0) {
                    $fr = FriendReferral::where('new_member_id', $friendReferral->new_member_id)
                        ->whereNotNull('joined_at')
                        ->first();

                    $friendReferral->joined_at = $fr->joined_at;
                } else {
                    $friendReferral->joined_at = null;
                }
            }

            return response()->json(['status' => 'ok', 'data' => $friendReferrals]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function show(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $friendReferrals = FriendReferral::with('new_member', 'member', 'voucher_setup')
                ->where('email', $request->input('email'))
                ->get();

            return response()->json(['status' => 'ok', 'data' => $friendReferrals]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
