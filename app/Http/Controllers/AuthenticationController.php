<?php

namespace App\Http\Controllers;

use App\BepozJob;
use App\FriendReferral;
use App\Helpers\Bepoz;
use App\Helpers\IGT;
use App\Helpers\MetropolisARCOdyssey;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Helpers\PondHoppers;
use App\Jobs\SendOneSignalNotification;
use App\Jobs\SendReminderNotification;
use App\Jobs\SendWelcomeEmail;
use App\ContactChange;
use App\Member;
use App\MemberLog;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SMSLog;
use App\SocialLogin;
use App\SystemLog;
use App\StripeCustomer;
use App\Tier;
use App\User;
use App\UserRoles;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\VoucherSetups;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Token;
use Illuminate\Support\Facades\Validator;
use App\Helpers\SMSProvider;
use Sujip\Guid\Guid;
use Spatie\Async\Pool;
// use App\ExportMemberTemp;
// use Maatwebsite\Excel\Facades\Excel;

use OneSignal;

class AuthenticationController extends Controller
{

    protected function index()
    {
        return view('welcome');
        // return response()->json(['status' => 'ok', 'message' => env('AES_KEY','BEPOZ')]);
    }

    public function onlyVersion()
    {
        $version = '5.0.0';
        return $version;
    }

    public function dateVersion()
    {
        $date = '2022-02-10';
        return $date;
    }

    public function version()
    {
        return response()->json(['version' => $this->onlyVersion(), 'date' => $this->dateVersion() ]);
    }

    protected function serverinfo()
    {
        // phpinfo();
    }

    protected function tester(Request $request, Bepoz $bepoz)
    {

    }

    protected function testerGamingAccountLowest(Request $request, Bepoz $bepoz)
    {

        $default_tier = Setting::where('key', 'default_tier')->first()->value;
        $tier = Tier::find($default_tier);

        if ( intval($tier->precreated_account_custom_flag) > 0 ) {
            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
            
            $payload = [];
            $payload['procedure'] = "GamingAccountLowest";
            $payload['parameters'] = [$tier->bepoz_group_id, $tier->precreated_account_custom_flag];

            // Log::warning($payload);
            
            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json', 'Cache-Control: no-cache'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::warning("GamingAccountLowest");
            // Log::warning($content);
            // Log::warning($httpCode);

            return response()->json([ 'status' => 'ok', 'payloadSend' => $payload, 'httpCode' => $httpCode, 'dataReceived' => $content ]);

        }
    }

    protected function testerGamingAccountCount(Request $request, Bepoz $bepoz)
    {

        $default_tier = Setting::where('key', 'default_tier')->first()->value;
        $tier = Tier::find($default_tier);

        if ( intval($tier->precreated_account_custom_flag) > 0 ) {
            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
            
            $payload = [];
            $payload['procedure'] = "GamingAccountCount";
            $payload['parameters'] = [$tier->bepoz_group_id, $tier->precreated_account_custom_flag];

            // Log::warning($payload);
            
            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json', 'Cache-Control: no-cache'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::warning("GamingAccountLowest");
            // Log::warning($content);
            // Log::warning($httpCode);

            return response()->json([ 'status' => 'ok', 'payloadSend' => $payload, 'httpCode' => $httpCode, 'dataReceived' => $content ]);
            
        }
    }

    protected function dummyTransaction()
    {
        // // start 1800 end 12473
        // for ($i=8001; $i<=12473; $i++) {
            
        //     $transLine = [];
        //     $paymentName = Setting::where('key', 'bepoz_payment_name')->first()->value;
        //     $tillID = Setting::where('key', 'bepoz_till_id')->first()->value;
        //     $operatorID = Setting::where('key', 'bepoz_operator_id')->first()->value;
        //     // $trainingMode = Setting::where('key', 'bepoz_training_mode')->first()->value;
            
        //     $trainingMode = false;
        //     $orderID = Carbon::now(config('app.timezone'))->timestamp;
        //     // $bepoz_product_number = 220066;
        //     $bepoz_product_number = 220040;
        //     // $bepoz_product_number = 720194;
        //     $qty = 1;
        //     $bepoz_account_id = $i;
        //     // IN CENTS
        //     // $PaymentAmount = 350;
        //     $PaymentAmount = 520;
        //     // $PaymentAmount = 1000;

        //     $itemEncode = [
        //         'Training' => $trainingMode,
        //         'OperatorID' => $operatorID,
        //         'TillID' => $tillID,
        //         'DateTimeTrans' => Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
        //         'OrderID' => $orderID,
        //         'ProdNumber' => $bepoz_product_number,
        //         'QtySold' => $qty,
        //         'PaymentName' => $paymentName,
        //         'AccountID' => $bepoz_account_id,
        //         'Gross' => $PaymentAmount,
        //         'Nett' => $PaymentAmount,
        //         'PaymentAmount' => $PaymentAmount
        //     ];
        //     $transLine[] = $itemEncode;

        //     $temp3 = $bepoz->TransactionCreate($transLine);
        //     echo print_r($temp3)."<br>";
        // }

        // // return response()->json(['status' => 'ok', 'response' => $temp3]);
    }

    protected function testerEmailConfirmationSuccess()
    {
        $name = Setting::where('key', 'company_name')->first()->value;
        $email_confirmation_successful_image = Setting::where('key', 'email_confirmation_successful_image')->first()->value;
        $email_confirmation_successful_message = Setting::where('key', 'email_confirmation_successful_message')->first()->value;
        $email_confirmation_successful_button_font_color = Setting::where('key', 'email_confirmation_successful_button_font_color')->first()->value;
        $email_confirmation_successful_button_border_color = Setting::where('key', 'email_confirmation_successful_button_border_color')->first()->value;
        $email_confirmation_successful_button_bg_color = Setting::where('key', 'email_confirmation_successful_button_bg_color')->first()->value;
        $email_confirmation_successful_font_color = Setting::where('key', 'email_confirmation_successful_font_color')->first()->value;
        $email_confirmation_successful_bg_color = Setting::where('key', 'email_confirmation_successful_bg_color')->first()->value;
        // Log::error("confirm 4061");
        return View::make('email.confirmation-successful', 
            array(  'company_logo' => $email_confirmation_successful_image, 
                    'company_name' => $name,
                    'email_confirmation_successful_button_font_color' => $email_confirmation_successful_button_font_color,
                    'email_confirmation_successful_button_border_color' => $email_confirmation_successful_button_border_color,
                    'email_confirmation_successful_button_bg_color' => $email_confirmation_successful_button_bg_color,
                    'email_confirmation_successful_font_color' => $email_confirmation_successful_font_color,
                    'email_confirmation_successful_bg_color' => $email_confirmation_successful_bg_color,
                    'email_confirmation_successful_message' => $email_confirmation_successful_message
            )
        );
    }

    protected function testerEmailConfirmationUnsuccess()
    {
        $name = Setting::where('key', 'company_name')->first()->value;
        $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
        $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
        $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
        $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
        $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
        $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
        $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
        // Log::error("confirm 4061");
        return View::make('email.confirmation-unsuccessful', 
            array(  'company_logo' => $email_confirmation_unsuccessful_image, 
                    'company_name' => $name,
                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                    'email_confirmation_unsuccessful_message' => $email_confirmation_unsuccessful_message
            )
        );
    }


    protected function testerMobileConfirmationSuccess()
    {
        $name = Setting::where('key', 'company_name')->first()->value;
        $mobile_confirmation_successful_image = Setting::where('key', 'mobile_confirmation_successful_image')->first()->value;
        $mobile_confirmation_successful_message = Setting::where('key', 'mobile_confirmation_successful_message')->first()->value;
        // $mobile_confirmation_successful_color = '#F4364C';
        $mobile_confirmation_successful_button_font_color = Setting::where('key', 'mobile_confirmation_successful_button_font_color')->first()->value;
        $mobile_confirmation_successful_button_border_color = Setting::where('key', 'mobile_confirmation_successful_button_border_color')->first()->value;
        $mobile_confirmation_successful_button_bg_color = Setting::where('key', 'mobile_confirmation_successful_button_bg_color')->first()->value;
        $mobile_confirmation_successful_font_color = Setting::where('key', 'mobile_confirmation_successful_font_color')->first()->value;
        $mobile_confirmation_successful_bg_color = Setting::where('key', 'mobile_confirmation_successful_bg_color')->first()->value;
        // Log::error("confirm 4061");
        return View::make('mobile.confirmation-successful', 
            array(  'company_logo' => $mobile_confirmation_successful_image, 
                    'company_name' => $name,
                    'mobile_confirmation_successful_button_font_color' => $mobile_confirmation_successful_button_font_color,
                    'mobile_confirmation_successful_button_border_color' => $mobile_confirmation_successful_button_border_color,
                    'mobile_confirmation_successful_button_bg_color' => $mobile_confirmation_successful_button_bg_color,
                    'mobile_confirmation_successful_font_color' => $mobile_confirmation_successful_font_color,
                    'mobile_confirmation_successful_bg_color' => $mobile_confirmation_successful_bg_color,
                    'mobile_confirmation_successful_message' => $mobile_confirmation_successful_message
            )
        );
    }

    protected function testerMobileConfirmationUnsuccess()
    {
        $name = Setting::where('key', 'company_name')->first()->value;
        $mobile_confirmation_unsuccessful_image = Setting::where('key', 'mobile_confirmation_unsuccessful_image')->first()->value;
        $mobile_confirmation_unsuccessful_message = Setting::where('key', 'mobile_confirmation_unsuccessful_message')->first()->value;
        $mobile_confirmation_unsuccessful_button_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_font_color')->first()->value;
        $mobile_confirmation_unsuccessful_button_border_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_border_color')->first()->value;
        $mobile_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_bg_color')->first()->value;
        $mobile_confirmation_unsuccessful_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_font_color')->first()->value;
        $mobile_confirmation_unsuccessful_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_bg_color')->first()->value;
        // Log::error("confirm 4061");
        return View::make('mobile.confirmation-unsuccessful', 
            array(  'company_logo' => $mobile_confirmation_unsuccessful_image, 
                    'company_name' => $name,
                    'mobile_confirmation_unsuccessful_button_font_color' => $mobile_confirmation_unsuccessful_button_font_color,
                    'mobile_confirmation_unsuccessful_button_border_color' => $mobile_confirmation_unsuccessful_button_border_color,
                    'mobile_confirmation_unsuccessful_button_bg_color' => $mobile_confirmation_unsuccessful_button_bg_color,
                    'mobile_confirmation_unsuccessful_font_color' => $mobile_confirmation_unsuccessful_font_color,
                    'mobile_confirmation_unsuccessful_bg_color' => $mobile_confirmation_unsuccessful_bg_color,
                    'mobile_confirmation_unsuccessful_message' => $mobile_confirmation_unsuccessful_message
            )
        );
    }


    protected function testerodyssey(Request $request)
    {
        try {
            // $validator = Validator::make($request->all(), [
            //     'function_name' => 'required'
            // ]);

            // if ($validator->fails()) {
            //     return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            // }

            $odyssey = new MetropolisARCOdyssey();

            // $result = $odyssey->custom();

            // $ListType = "Title";

            // $function_name = $request->input('function_name');
            // $result = $odyssey->$function_name($ListType);

            // $resultChallengeResponse = $odyssey->ChallengeResponse("");

            // $resultConnect = $odyssey->Connect();
            // $resultChallengeResponse = $odyssey->ChallengeResponse($resultConnect);
            // $resultGetSessionInfo = $odyssey->GetSessionInfo($sessionToken);
            // $resultGetLookupValues = $odyssey->GetLookupValues();
            // $resultGetLookupValues = $odyssey->GetLookupValues($resultManageToken);
            // $resultGetCustomFields = $odyssey->GetCustomFields($resultManageToken);
            $resultManageToken = $odyssey->ManageToken();
            $result = $odyssey->GetCustomFields($resultManageToken);

            $resultManageToken2 = $odyssey->ManageToken();
            if ($request->has('id')) {
                $result2 = $odyssey->GetMember($resultManageToken2, $request->input('id'));
            } else {
                $result2 = $odyssey->GetMember($resultManageToken2, "106477");
            }

            return response()->json(['status' => 'ok',
                // 'resultConnect' => $resultConnect,
                // 'resultChallengeResponse' => $resultChallengeResponse,
                // 'resultGetSessionInfo' => $resultGetSessionInfo,
                // 'resultGetLookupValues' => $resultGetLookupValues,
                // 'resultGetLookupValues' => $resultGetLookupValues,
                // 'resultGetCustomFields' => $resultGetCustomFields,
                'resultManageToken' => $resultManageToken, 'result' => $result, 'result2' => $result2
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    protected function testerigtwsdl()
    {
        // $resultGetMandatoryFields = $igt->GetMandatoryFields();
        // $resultGetTitlesList = $igt->GetTitlesList();
        // $resultGetGendersList = $igt->GetGendersList();
        // $resultGetAddressTypesList = $igt->GetAddressTypesList();
        // $resultGetSiteDetailsList = $igt->GetSiteDetailsList();
        // $resultGetMembershipTermsList = $igt->GetMembershipTermsList();
        // $resultGetMembershipTypesList = $igt->GetMembershipTypesList();
        // $resultGetMembershipClassesList = $igt->GetMembershipClassesList();
        // $resultGetMembershipRelationship = $igt->GetMembershipRelationship();

        // return response()->json([ 'resultGetMandatoryFields' => $resultGetMandatoryFields,
        //     'resultGetTitlesList' => $resultGetTitlesList, 'resultGetGendersList' => $resultGetGendersList,
        //     'resultGetAddressTypesList' => $resultGetAddressTypesList, 'resultGetSiteDetailsList' => $resultGetSiteDetailsList,
        //     'resultGetMembershipTermsList' => $resultGetMembershipTermsList, 'resultGetMembershipTypesList' => $resultGetMembershipTypesList,
        //     'resultGetMembershipClassesList' => $resultGetMembershipClassesList, 'resultGetMembershipRelationship' => $resultGetMembershipRelationship,
        //     'resultGetAvailableMemberNumbersList' => $resultGetAvailableMemberNumbersList ]);

        // // CHECK AVAILABLE MEMBER NUMBER LIST
        // $resultGetAvailableMemberNumbersList = $igt->GetAvailableMemberNumbersList(1);

        // // GET THE SMALLEST MEMBER NUMBER
        // $memberNumber = $resultGetAvailableMemberNumbersList->values[0];
        // $memberNumber1 = $resultGetAvailableMemberNumbersList->values[1];


        // Log::info("SMALLEST MEMBER NUMBER");
        // Log::info($memberNumber);
        // Log::info($memberNumber1);

        // // HOLD THE NUMBER
        // $resultMemberNumberHold = $igt->MemberNumberHold(1, $memberNumber);

        // $siteId = 1;
        // $memberNumber = 2064;
        // $resultGetPlayerMembershipDetails = $igt->GetPlayerMembershipDetails($siteId, $memberNumber);

        // return response()->json([ 'resultGetPlayerMembershipDetails' => $resultGetPlayerMembershipDetails ]);

        // // USE THIS NO NEED HOLD MEMBER
        // $membershipId = 1;
        // $resultGetNextAvailableMemberNumber = $igt->GetNextAvailableMemberNumber($membershipId);
        // $memberNumber = $resultGetNextAvailableMemberNumber->number;

        // Log::info("resultGetNextAvailableMemberNumber");
        // Log::info($memberNumber);

        // $resultGetAvailableMemberNumbersList = $igt->GetAvailableMemberNumbersList($membershipId);


        // return response()->json([ 'resultGetNextAvailableMemberNumber' => $resultGetNextAvailableMemberNumber,
        //     // 'resultGetAvailableMemberNumbersList' => $resultGetAvailableMemberNumbersList,
        //     'memberNumber' => $memberNumber ]);


        // // CREATE MEMBER
        // $siteId = 1;
        // $membershipId = 1;
        // $termId = 1;
        // $firstName = "Test";
        // $lastName = "Gaming";
        // $preferredName = "Gamers";
        // $address = "71 Boundary Road, North Melb, VIC";
        // $zipCode = "3051";
        // $addressCountryId = "36";
        // $phoneNumber = "0413691576";
        // $email = "ekkavee@vectron.com.au";

        // $identificationsCountryId = "36";
        // $identificationsStateId = "VIC";
        // $identificationsExpiryDate = "2017-01-01";
        // $identificationsVerificationDate = "2017-01-16";
        // $identificationsNumber = "12345678901";

        // $employmentPosition = "Brickie";

        // $resultCreateMember = $igt->CreateMember($siteId, $membershipId, $termId, $memberNumber, $firstName, $lastName,
        //     $preferredName, $address, $zipCode, $addressCountryId, $phoneNumber, $email,
        //     $identificationsCountryId, $identificationsStateId, $identificationsExpiryDate,
        //     $identificationsVerificationDate, $identificationsNumber, $employmentPosition);

        // // CHECK AVAILABLE MEMBER NUMBER LIST AGAIN
        // $resultAfterGetAvailableMemberNumbersList = $igt->GetAvailableMemberNumbersList(1);


        // return response()->json([ 'memberNumber' => $memberNumber,
        //     'resultMemberNumberHold' => $resultMemberNumberHold,
        //     'resultCreateMember' => $resultCreateMember,
        //     'resultAfterGetAvailableMemberNumbersList' => $resultAfterGetAvailableMemberNumbersList ]);


        // $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();
        // $allGamingField = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value);
        // echo "Myplace Gaming Field<br>";

        // echo print_r($allGamingField, true);
        // foreach ($allGamingField as $data) {
        //     echo $data->key." ".$data->fieldName."<br>";
        // }

        // $resultGetMandatoryFields = $igt->GetMandatoryFields();
        // echo "<br>IGT Gaming Field<br>";

        // // $collection = collect($main_array);
        // // $filteredItems = $collection->where('name', 'first');

        // foreach ($resultGetMandatoryFields->values as $data) {
        //     // echo $data->key." ".$data->label."<br>";

        //     $fieldName = $data->label[0]."";

        //     $filteredItems = array_filter($allGamingField, function($f) use ($fieldName){
        //         return $f->fieldName == $fieldName;
        //     });

        //     if ($filteredItems) {
        //         echo "<br>filteredItems<br>";
        //         echo print_r($filteredItems, true);
        //         echo "<br><br>";
        //     } else {
        //         echo "<br>insert new mandatory gaming field<br>";
        //         $key = preg_replace('/\s+/', '_', strtolower($data->label[0]."") );

        //         $o= new \stdClass();
        //         $o->key = $key;
        //         $o->fieldName = $data->label[0]."";

        //         // echo "<br><br>";
        //         $allGamingField[] = $o;
        //     }

        // }

        // echo print_r($allGamingField, true);

        // echo "<br>Myplace Gaming Field after insert<br>";
        // foreach ($allGamingField as $data) {
        //     echo $data->key." ".$data->fieldName."<br>";
        // }


        // $resultGetCountriesList = $igt->GetCountriesList();

        // $filteredItems = array_filter($allGamingField, function($f) use ($fieldName){
        //     return $f->fieldName == "address_country_id";
        // });

        // if ($filteredItems) {
        //     echo "<br>filteredItems<br>";
        //     echo print_r($filteredItems, true);
        //     echo "<br><br>";

        //     $filteredItems->multiValues = $resultGetCountriesList;
        // } else {
        //     echo "<br>insert new mandatory gaming field<br>";
        //     $key = "address_country_id";

        //     $o= new \stdClass();
        //     $o->key = $key;
        //     $o->fieldName = "Address Country ID";
        //     $o->multiValues = $resultGetCountriesList;

        //     // echo "<br><br>";
        //     $allGamingField[] = $o;
        // }

        // $savegaming = \GuzzleHttp\json_encode($allGamingField);
        // $gaming_mandatory_field->extended_value = $savegaming;
        // $gaming_mandatory_field->save();


        // $countryID = 36;
        // $resultGetStatesList = $igt->GetStatesList($countryID);


        // return response()->json([ 'resultGetStatesList' => $resultGetStatesList ]);

        $igt = new IGT();
        $wsdl = $igt->SystemCheck();
        return response()->json(['status' => 'ok', 'wsdl' => $wsdl]);
    }


    protected function testerigt(Request $request)
    {

        $igt = new IGT();

        $result = $igt->customSoap($request->input('url'), $request->input('xml'));

        return response()->json(['status' => 'ok', 'response' => $result]);

    }

    // NEED CHECKER AND MANUAL SYNC FROM VARSITY
    protected function checker()
    {
        // // $member = Member::with('user')->find(5985);

        // // $groupID = $member->tiers()->first()->bepoz_group_id;

        // // echo $member;
        // // echo "<br>";
        // // echo $groupID;

        // set_time_limit(3000);
        // // echo "tester";
        // $ctr=0;

        // $handle = fopen("../varsity_users_3.csv", "r");
        
        // if ($handle) {
            
        //     try {

        //         // DB::beginTransaction();
        //         $total_import = 0;
                
        //         $values0 = array();

        //         while (($line = fgets($handle)) !== false) {
        //         // process the line read.

        //             if ($ctr<1){
        //                 // $column = explode(',', $line);
        //                 //echo $column[0];
        //             } else {
        //                 $column = explode(';', $line);
                        
        //                 // $yourorderBepozID =  trim(str_replace('"', '', $column[0]));
        //                 // $email = trim(str_replace('"', '', $column[1]));
        //                 // $venue = trim(str_replace('"', '', $column[2]));


        //                 $yourorderBepozID =  trim(str_replace('"', '', $column[2]));
        //                 $email = trim(str_replace('"', '', $column[4]));
        //                 $venue = trim(str_replace('"', '', $column[5]));

        //                 // echo $yourorderBepozID.",".$email.",";

        //                 $user = User::where("email", $email)->first();
                        

        //                 if (is_null($user)){
        //                     // echo "not found <br>";
        //                 } else {
        //                     // echo $user->member->bepoz_account_id.",".$user->member->bepoz_account_number.",".
        //                     //     $user->member->bepoz_account_card_number."<br>";

        //                     if ( $yourorderBepozID != $user->member->bepoz_account_id){
        //                         $values0[] = array(
        //                             "yourorder_bepoz_id" => $yourorderBepozID,
        //                             "email" => $email,
        //                             "myplace_bepoz_id" => $user->member->bepoz_account_id,
        //                             "myplace_bepoz_account_number" => $user->member->bepoz_account_number,
        //                             "myplace_bepoz_account_card_number" => $user->member->bepoz_account_card_number,
        //                             "venue" => $venue,
        //                             "morley" => '',
        //                             "fremantle" => '',
        //                             "joondalup" => '',
        //                             "nedlands" => '',
        //                             "northbridge" => '',
        //                             "waterford" => ''
        //                         );
        //                     }

        //                 }

        //                 // echo $yourorderID.",".$firstname.",".$lastname.",".$email.",".$mobile.",".$dob.",".$bepoz_id.",".
        //                 // $bepoz_account_number.",".$bepoz_account_card_number.",".$group."<br>";
                        
        //                 // echo var_dump($column, true);

            
        //             }
                    
        //             $ctr++;
        //         }
        //         // echo "total_import = ".$total_import;

        //         // make distinct
        //         $uniques = array();
                
        //         foreach ($values0 as $value) {
        //             if ( isset($uniques[$value["email"]]) ){

        //             } else {
        //                 $uniques[$value["email"]] = $value;
        //             }

        //             if ( $value["venue"] == "morley") {
        //                     $uniques[$value["email"]]["morley"] = $value["yourorder_bepoz_id"];
        //                 Log::info("163");
        //                 Log::info($uniques[$value["email"]]);
        //             }

        //             if ( $value["venue"] == "fremantle") {
        //                 $uniques[$value["email"]]["fremantle"] = $value["yourorder_bepoz_id"];
        //                 Log::info("169");
        //                 Log::info($uniques[$value["email"]]);
        //             }

        //             if ( $value["venue"] == "joondalup") {
        //                 $uniques[$value["email"]]["joondalup"] = $value["yourorder_bepoz_id"];
        //                 Log::info("175");
        //                 Log::info($uniques[$value["email"]]);
        //             }


        //             if ( $value["venue"] == "nedlands") {
        //                 $uniques[$value["email"]]["nedlands"] = $value["yourorder_bepoz_id"];
        //                 Log::info("182");
        //                 Log::info($uniques[$value["email"]]);
        //             }


        //             if ( $value["venue"] == "northbridge") {
        //                 $uniques[$value["email"]]["northbridge"] = $value["yourorder_bepoz_id"];
        //                 Log::info("189");
        //                 Log::info($uniques[$value["email"]]);
        //             }


        //             if ( $value["venue"] == "waterford") {
        //                 $uniques[$value["email"]]["waterford"] = $value["yourorder_bepoz_id"];
        //                 Log::info("196");
        //                 Log::info($uniques[$value["email"]]);
        //             }

        //         }


        //         // foreach ($uniques as $unique) {
                    
        //         //     foreach ($values0 as $value) {
        //         //         // $uniques[$value["email"]] = $value; // Get unique country by code.
        //         //         if ($unique["email"] == $value["email"]){
        //         //             if ($unique["venue"] == "morley"){
                                
        //         //             }
        //         //         }
        //         //     }
        //         // }

        //         // $uniques = array_unique($values);

        //         // echo print_r($uniques, true);
        //         // $today = Carbon::now(config('app.timezone'));
                
                
        //         return Excel::download(new ExportMemberTemp($uniques), ' Member Temp Report All Venue.xlsx');

        //     } catch (\Exception $e) {
        //         Log::warning($e);
        //     }
        //     // DB::commit();

        //     fclose($handle);
        // } else {
        //     // error opening the file.
        //     Log::warning("error opening the file");
        // }
    }

    protected function manualsync($memberid)
    {        
        // NEED LOAD BEPOZ FOR ACCOUNT ID
        $member = Member::find($memberid);
        
        $user = $member->user;

        $email = $user->email;

        $bepoz = new Bepoz;
        $result = $bepoz->accountSearch(8, $email);

        if ($result) {
            if ($result['IDList']['Count'] == 0) {
                echo "Account in bepoz not found ";
                
            } else if ($result['IDList']['Count'] == 1) {
                // ACCOUNT found 1, load details from Bepoz
                $temp = $bepoz->AccountFullGet($result['IDList']['ID']);

                if ($temp['AccountFull']['Status'] == 0) {
                    // $tier = Tier::find($default_tier);
                    $tier = Tier::where('bepoz_group_id', $temp['AccountFull']['GroupID'])->first();

                    $member->bepoz_account_id = $temp['AccountFull']['AccountID'];
                    $member->bepoz_account_number = $temp['AccountFull']['AccNumber'];
                    $member->bepoz_account_card_number = $temp['AccountFull']['CardNumber'];                    
                    $member->current_preferred_venue = $tier->venue_id;
                    $member->current_preferred_venue_name = $tier->name;
                    $member->save();


                    $setting = Setting::where('key', 'cut_off_date')->first();
                    $date_expiry = Carbon::parse($setting->value);
                    $date_expiry->setTimezone(config('app.timezone'));
                    $date_expiry->setTime(0, 0, 0);
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                    echo "member id ".$memberid."<br>".
                        "email update ".$email."<br>".
                        "bepoz_account_id ".$member->bepoz_account_id."<br>".
                        "bepoz_account_number ".$member->bepoz_account_number."<br>".
                        "bepoz_account_card_number ".$member->bepoz_account_card_number."<br>".
                        "tier ".$tier->name."<br>";

                } else {
                    echo "Account in bepoz inactive";
                }
            } else {
                echo "Account in bepoz found more than 1";
            }
        } else {
            echo "Bepoz connection problem";
        }

    }

    /**
     * Check email in our API
     *  >> using validator
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkEmail(Request $request, User $user)
    {
        try {
            $newQuery = $user->newQuery();

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            $newQuery->where(function ($query) use ($request, $email) {

                $query->where('email', $email);

                $query->orWhereHas('social_logins', function ($q) use ($request, $email) {
                    $q->where('email', $email);
                });
            });

            $us = $newQuery->first();

            if (!is_null($us)) {
                return response()->json(['status' => 'error', 'message' => 'Email address is already registered.', 'email' => $email], Response::HTTP_BAD_REQUEST);
            } else {                
                $email_not_allowed_domains = Setting::where('key', 'email_not_allowed_domains')->first();
                $blacklistDomains = \GuzzleHttp\json_decode($email_not_allowed_domains->extended_value, true);

                $available = true;
                foreach ($blacklistDomains as $key => $domain) {
                    if (strpos($email, $domain) !== false) {
                        $available = false;
                    }
                    // Log::error($domain);
                }

                if (!$available){
                    return response()->json(['status' => 'error', 'message' => 'Email address is not allowed.', 'email' => $email], Response::HTTP_BAD_REQUEST);
                }

                return response()->json(['status' => 'ok', 'message' => 'email is available', 'email' => $email]);
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Sign in through our API
     * >> for admin panel only
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function login(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $recaptcha_enable = Setting::where('key', 'recaptcha_enable')->first()->value;

            // if ($recaptcha_enable === 'true'){
            //     $captcha = $request->input('captcha');
            //     if(!$captcha){
            //         return response()->json(['status' => 'error', 'message' => 'captcha error'], Response::HTTP_BAD_REQUEST);
            //     }
            //     $secretKey = Setting::where('key', 'recaptcha_server_key')->first()->value;;
            //     $ip = $_SERVER['REMOTE_ADDR'];

            //     // post request to server
            //     $url = 'https://www.google.com/recaptcha/api/siteverify';
            //     $data = array('secret' => $secretKey, 'response' => $captcha);

            //     $options = array(
            //         'http' => array(
            //             'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            //             'method'  => 'POST',
            //             'content' => http_build_query($data)
            //         )
            //     );
            //     $context  = stream_context_create($options);
            //     $response = file_get_contents($url, false, $context);
            //     $responseKeys = json_decode($response,true);
            //     if($responseKeys["success"]) {
            //         // Log::warning("captcha success");
            //     } else {
            //         return response()->json(['status' => 'error', 'message' => 'captcha verify error'], Response::HTTP_BAD_REQUEST);
            //     }
            // }

            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $credentials = ['email' => $email, 'password' => $request->input('password'), 'deleted_at' => null];

            // $credentials = request(['email', 'password']);
            // $token = auth()->attempt($credentials);
            // Log::info($token);

            // if (!$token = auth()->attempt($credentials)) {

            //Create JWT token after login
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 'error', 'message' => 'Invalid Email and-or Password'], Response::HTTP_UNAUTHORIZED);
            } else {
                $user = User::with('staff', 'roles.control')->where('email', '=', $email)->first();

                $staff = $user->staff;
                if (!is_null($staff)) {
                    if (!$staff->active) {
                        return response()->json(['status' => 'error', 'message' => 'Login disabled. Please contact your venue.'], Response::HTTP_FORBIDDEN);
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Login disabled. Please contact your venue.'], Response::HTTP_FORBIDDEN);
                }

                if ($user->roles()->first()->control->backend_access) {
                    return response()->json(['status' => 'ok', 'token' => $token, 'user' => $user]);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Insufficient Role'], Response::HTTP_FORBIDDEN);
                }
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }


    /**
     * Sign in through our API
     * >> for member app access
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function signin(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $credentials = ['email' => $email, 'password' => $request->input('password'), 'deleted_at' => null];

            //Create JWT token after login
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 'error', 'message' => 'Invalid Email and-or Password'], Response::HTTP_UNAUTHORIZED);
            }

            $user = User::with('member')->where('email', $email)->first();

            if ($user->member->status == 'inactive') {
                return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
            }

            return response()->json(['status' => 'ok', 'token' => $token]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Setting Custom Text to Bepoz
     *
     */
    protected function bepozSetCustom($AccountID, $CustomFieldKey, $CustomFieldValue)
    {
        $bepoz = new Bepoz();

        $setting = Setting::where('key', 'bepozcustomfield')->first();

        $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);
        $CustomFieldNumber = "0";

        foreach ($allCustomField as $data) {
            if ($data->key == $CustomFieldKey) {
                $CustomFieldNumber = $data->field;
            }
        }

        //Log::info($CustomFieldNumber);

        $param = ['AccountID' => $AccountID,
            'CustomFieldNumber' => $CustomFieldNumber,
            'DataSet' => $CustomFieldValue
        ];
        $setResult = $bepoz->AccCustomFieldSet($param);


        // $param2 = ['AccountID' => $AccountID];
        // $AccountGet = $bepoz->AccountGet($param2);
        // $AccCustomGet = $bepoz->AccCustomGet($AccountID);
    }

    /**
     * loginSSO through our API (original logic)
     * >> for member app access
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function loginSSO1(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            $credentials = ['email' => $email, 'password' => $request->input('password'), 'deleted_at' => null];
            $token = JWTAuth::attempt($credentials);
            $user_role_detected_in_local_bp = false;
            $login_successful_via_local_bp = false;

            // CHECK FROM MYPLACE
            if ($token) {
                $login_successful_via_local_bp = true;
                $user = User::with('roles')->where('email', '=', strtolower($email))->first();
                // Log::info($user);

                // ONLY bepoz_admin PROHIBITED TO LOGIN IN THE APP
                if ($user->roles[0]->name == "bepoz_admin") {
                    $user_role_detected_in_local_bp = true;
                    // Log::info("detected bepoz_admin");
                }
                if ($user->roles[0]->name == "admin") {
                    $user_role_detected_in_local_bp = true;
                    // Log::info("detected bepoz_admin");
                }
                if ($user->roles[0]->name == "staff") {
                    $user_role_detected_in_local_bp = true;
                    // Log::info("detected bepoz_admin");
                }
                // Log::info($user_role_detected_in_local_bp);
                // return response()->json(['status' => 'ok', 'user' => $user ]);

            }

            // solution 1 : for preset accounts, use local authentication only
            $blackListed = array('admin@vectron.com.au', 'internalsupport@bepoz.com.au', 'internalsupport@vectron.com.au');
            if (in_array(strtolower($email), $blackListed)) {
                if ($login_successful_via_local_bp) {
                    if (!$user_role_detected_in_local_bp) {
                        // return response()->json(['status' => 'ok', 'token' => $token, 'role' => $user->role->internal_name, 'signature' => $user->login_token, 'access' => $user->serviceAppStaffAccess, 'user' => collect($user->toArray())->only(['id', 'name', 'email', 'first_name', 'last_name'])->all()]);
                    } else {
                        // insufficient role
                        return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only'], Response::HTTP_FORBIDDEN);
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Invalid Email and-or Password'], Response::HTTP_FORBIDDEN);
                }

            } else {
                // not preset accounts
                // solution 2,3,4: if someone use valid BP password, then this will replace sSo vice versa
                $sso_response = $this->loginSSOUSer($email, $request->input('password'), $request);

                // Log::info("339");
                // Log::info($sso_response);
                // Log::info($sso_response['status']);

                if ($login_successful_via_local_bp) {
                    // successfully BP login
                    // Log::info("345");
                    if ($user_role_detected_in_local_bp) {
                        // insufficient role
                        return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_by_local_setting'], Response::HTTP_FORBIDDEN);
                    }

                    if ($sso_response['status'] == 'ok') {
                        // successful sso login
                        if ($sso_response['user_role_detected_in_permanent_sso']) {
                            return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                        } else {
                            // Log::info("356");
                            // Log::info($sso_response['data']->data->id_token);

                            $token = JWTAuth::attempt($credentials);
                            return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response['data']->data->id_token,
                                'access_token' => $sso_response['data']->data->access_token,
                                // 'login_token' => $user->member->login_token,
                                'refresh_token' => $sso_response['data']->data->refresh_token]);
                        }
                    } else {

                        // check if the sso account exists or not
                        $checkSSOAccount = $this->checkSSOAccount($email);
                        // Log::info("371");
                        // Log::info($checkSSOAccount);

                        if ($checkSSOAccount) {
                            $user = User::with('member')->where('email', $email)->first();
                            // $user->member->login_token = $checkSSOAccount;
                            // $user->member->save();

                            // Log::info("376");
                            // Log::info("checkSSOAccount ok");
                            $this->overrideSSOPassword($email, $request->input('password'));

                            if (!is_null($user)) {
                                $user->member->member_tier->tier;
                                $ml = new MemberLog();
                                $ml->member_id = $user->member->id;
                                $ml->message = "This member use myplace password, override SSO password";
                                $ml->after = \GuzzleHttp\json_encode($user);
                                $ml->save();
                            }

                        } else {
                            // Log::info("380");
                            // Log::info("checkSSOAccount failed");
                            $this->createSSOAccount($email, $request->input('password'));
                        }

                    }
                } else {
                    // login via SSO
                    // Log::info("388");

                    if ($sso_response['status'] == 'ok') {
                        // successful sso login
                        // Log::info("392");

                        if ($this->checkLocalAccount($email)) {
                            // Log::info("395");
                            $user = $this->overrideLocalPassword($email, $request->input('password'));

                            if (!is_null($user)) {
                                $user->member->member_tier->tier;
                                $ml = new MemberLog();
                                $ml->member_id = $user->member->id;
                                $ml->message = "This member use SSO password, override myplace password";
                                $ml->after = \GuzzleHttp\json_encode($user);
                                $ml->save();
                            }
                        }

                        if ($sso_response['user_role_detected_in_permanent_sso']) {
                            return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                        } else {
                            // Log::info("402");
                            $token = JWTAuth::attempt($credentials);
                            return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response['data']->data->id_token,
                                'access_token' => $sso_response['data']->data->access_token,
                                // 'login_token' => $user->member->login_token,
                                'refresh_token' => $sso_response['data']->data->refresh_token]);
                        }
                    } else {
                        // failed BP login + failed SSo login
                        // return response()->json(['status' => 'error', 'message' => 'Invalid Email and-or Password'], Response::HTTP_FORBIDDEN);
                        return response()->json(['status' => 'error', 'message' => $sso_response['message']], Response::HTTP_FORBIDDEN);
                    }
                }

            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * loginSSO through our API (fixed logic)
     * >> for member app access
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function loginSSO(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            $credentials = ['email' => $email, 'password' => $request->input('password'), 'deleted_at' => null];
            $token = JWTAuth::attempt($credentials);
            $user_role_detected_in_local_bp = false;
            $login_successful_via_local_bp = false;

            // CHECK FROM MYPLACE
            if ($token) {
                $login_successful_via_local_bp = true;
                $user = User::with('roles')->where('email', '=', strtolower($email))->first();
                // Log::info($user);

                // ONLY bepoz_admin PROHIBITED TO LOGIN IN THE APP
                if ($user->roles[0]->name == "bepoz_admin") {
                    $user_role_detected_in_local_bp = true;
                    // Log::info("detected bepoz_admin");
                }
                if ($user->roles[0]->name == "admin") {
                    $user_role_detected_in_local_bp = true;
                    // Log::info("detected bepoz_admin");
                }
                if ($user->roles[0]->name == "staff") {
                    $user_role_detected_in_local_bp = true;
                    // Log::info("detected bepoz_admin");
                }
                // Log::info($user_role_detected_in_local_bp);
                // return response()->json(['status' => 'ok', 'user' => $user ]);

            }

            // solution 1 : for preset accounts, use local authentication only
            $blackListed = array('admin@vectron.com.au', 'internalsupport@bepoz.com.au', 'internalsupport@vectron.com.au');
            if (in_array(strtolower($email), $blackListed)) {
                if ($login_successful_via_local_bp) {
                    if (!$user_role_detected_in_local_bp) {
                        // return response()->json(['status' => 'ok', 'token' => $token, 'role' => $user->role->internal_name, 'signature' => $user->login_token, 'access' => $user->serviceAppStaffAccess, 'user' => collect($user->toArray())->only(['id', 'name', 'email', 'first_name', 'last_name'])->all()]);
                    } else {
                        // insufficient role
                        return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only'], Response::HTTP_FORBIDDEN);
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Invalid Email and-or Password'], Response::HTTP_FORBIDDEN);
                }

            } else {
                // not preset accounts
                // solution 2,3,4: if someone use valid BP password, then this will replace sSo vice versa
                $sso_response = $this->loginSSOOnly($email, $request->input('password'), $request);

                // Log::info("339");
                // Log::info($sso_response);
                // Log::info($sso_response['status']);

                if ($login_successful_via_local_bp) {
                    // successfully BP login
                    // Log::info("345");
                    if ($user_role_detected_in_local_bp) {
                        // insufficient role
                        return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_by_local_setting'], Response::HTTP_FORBIDDEN);
                    }

                    if ($sso_response['status'] == 'ok') {
                        // Email match Myplace, email match SSO

                        // successful sso login
                        if ($sso_response['user_role_detected_in_permanent_sso']) {
                            // FEATURE FROM VARSITY FOR DEBUG
                            $log = new SystemLog();
                            $log->type = 'login-error';
                            $log->humanized_message = 'User role detected';
                            $log->payload = 'AuthenticationController.php/loginSSO';
                            $log->message = 'User role detected';
                            $log->source = 'AuthenticationController.php';
                            $log->save();

                            return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                        } else {
                            // Log::info("356");
                            // Log::info($sso_response['data']->data->id_token);

                            // FEATURE FROM VARSITY FOR DEBUG
                            $explode = explode('.', $sso_response['data']->data->id_token);
                            $decoded_id_token = base64_decode($explode[1]);
                            $decoded = json_decode($decoded_id_token);                            

                            $ml = new MemberLog();
                            $ml->member_id = $user->member->id;
                            $ml->message = "Login with email ".$email." via SSO";
                            $ml->after = \GuzzleHttp\json_encode(['email' => $email, 'user id' => $user->id, 'member id' => $user->member->id, 
                                'bepoz id' => $user->member->bepoz_account_id, 'bepoz account number' => $user->member->bepoz_account_number,
                                'bepoz account card number' => $user->member->bepoz_account_card_number, 'guid' => $decoded->guid] );
                            $ml->save();

                            $token = JWTAuth::attempt($credentials);
                            return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response['data']->data->id_token,
                                'login_token' => $user->member->login_token, 'access_token' => $sso_response['data']->data->access_token,
                                'refresh_token' => $sso_response['data']->data->refresh_token]);
                        }
                    } else {

                        // check if the sso account exists or not
                        $checkSSOAccount = $this->checkSSOAccount($email);
                        // Log::info("371");
                        // Log::info($checkSSOAccount);

                        if ($checkSSOAccount['status'] == 'ok') {
                            $user = User::with('member')->where('email', $email)->first();
                            // $user->member->login_token = $checkSSOAccount;
                            // $user->member->save();

                            // Log::info("376");
                            // Log::info("checkSSOAccount ok");

                            if (!is_null($user)) {
                                // override password
                                $resultOverrideSSOPassword = $this->overrideSSOPassword($email, $request->input('password'));                        
                                
                                if ($resultOverrideSSOPassword){                                    
                                    $user->member->member_tier->tier;
                                    $ml = new MemberLog();
                                    $ml->member_id = $user->member->id;
                                    $ml->message = "This member use myplace password, override SSO password";
                                    $ml->after = \GuzzleHttp\json_encode($user);
                                    $ml->save();
                                    
                                    $sso_response_relogin = $this->loginSSOOnly($email, $request->input('password'), $request);

                                    if ($sso_response_relogin['status'] == 'ok') {
                                        $explode = explode('.', $sso_response_relogin['data']->data->id_token);
                                        $decoded_id_token = base64_decode($explode[1]);
                                        $decoded = json_decode($decoded_id_token);
                                        
                                        $ml = new MemberLog();
                                        $ml->member_id = $user->member->id;
                                        $ml->message = "Login with email ".$email." via SSO";
                                        $ml->after = \GuzzleHttp\json_encode(['email' => $email, 'user id' => $user->id, 'member id' => $user->member->id, 
                                            'bepoz id' => $user->member->bepoz_account_id, 'bepoz account number' => $user->member->bepoz_account_number,
                                            'bepoz account card number' => $user->member->bepoz_account_card_number, 'guid' => $decoded->guid] );
                                        $ml->save();

                                        $token = JWTAuth::attempt($credentials);
                                        return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response_relogin['data']->data->id_token, 
                                            'login_token' => $user->member->login_token, 'access_token' => $sso_response_relogin['data']->data->access_token, 
                                            'refresh_token' => $sso_response_relogin['data']->data->refresh_token ]);
                                    } else {
                                        
                                        $log = new SystemLog();
                                        $log->type = 'login-error';
                                        $log->humanized_message = 'failed SSO relogin';
                                        $log->payload = 'AuthenticationController.php/loginSSO';
                                        $log->message = 'failed SSO relogin';
                                        $log->source = 'AuthenticationController.php';
                                        $log->save();

                                        return response()->json(['status' => 'error', 'message' => 'Something wrong with SSO', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                                    }                                    
                                } else {
                                    
                                    $log = new SystemLog();
                                    $log->type = 'login-error';
                                    $log->humanized_message = 'failed overrideSSOPassword';
                                    $log->payload = 'AuthenticationController.php/loginSSO';
                                    $log->message = 'failed overrideSSOPassword';
                                    $log->source = 'AuthenticationController.php';
                                    $log->save();

                                    return response()->json(['status' => 'error', 'message' => 'Something wrong with SSO', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                                }                            
                            } else {
                                return response()->json(['status' => 'error', 'message' => 'Something wrong with login', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                            }
                        } else {
                            // Log::info("380");
                            // Log::info("checkSSOAccount failed");

                            // EXIST IN MYPLACE, NOT EXIST IN SSO, THEN CREATE NEW SSO ACCOUNT
                            // LOGIC IMPROVED FROM VARSITY WITH LOG
                            $resultcreateSSOAccount = $this->createSSOAccount($email, $request->input('password'));

                            if ($resultcreateSSOAccount){  
                                $sso_response_relogin = $this->loginSSOOnly($email, $request->input('password'), $request);

                                $explode = explode('.', $sso_response_relogin['data']->data->id_token);
                                $decoded_id_token = base64_decode($explode[1]);
                                $decoded = json_decode($decoded_id_token);                            

                                $ml = new MemberLog();
                                $ml->member_id = $user->member->id;
                                $ml->message = "Login with email ".$email." via SSO";
                                $ml->after = \GuzzleHttp\json_encode(['email' => $email, 'user id' => $user->id, 'member id' => $user->member->id, 
                                    'bepoz id' => $user->member->bepoz_account_id, 'bepoz account number' => $user->member->bepoz_account_number,
                                    'bepoz account card number' => $user->member->bepoz_account_card_number, 'guid' => $decoded->guid] );
                                $ml->save();

                                $token = JWTAuth::attempt($credentials);
                                return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response_relogin['data']->data->id_token, 
                                    'login_token' => $user->member->login_token, 'access_token' => $sso_response_relogin['data']->data->access_token, 
                                    'refresh_token' => $sso_response_relogin['data']->data->refresh_token ]);
                            } else {                                
                                $log = new SystemLog();
                                $log->type = 'login-error';
                                $log->humanized_message = 'failed create SSO Account';
                                $log->payload = 'AuthenticationController.php/loginSSO';
                                $log->message = 'failed create SSO Account';
                                $log->source = 'AuthenticationController.php';
                                $log->save();
                                
                                return response()->json(['status' => 'error', 'message' => 'Something wrong with login', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                            }
                        }

                    }
                } else {
                    // login via SSO, failed local myplace
                    // Log::info("388");

                    if ($sso_response['status'] == 'ok') {
                        // successful sso login
                        // Log::info("392");

                        if ($this->checkLocalAccount($email)) {
                            // Log::info("395");
                            $user = $this->overrideLocalPassword($email, $request->input('password'));
                            if (!is_null($user)) {
                                $user->member->member_tier->tier;
                                $ml = new MemberLog();
                                $ml->member_id = $user->member->id;
                                $ml->message = "This member use SSO password, override myplace password";
                                $ml->after = \GuzzleHttp\json_encode($user);
                                $ml->save();
                            }
                            
                            if ($sso_response['user_role_detected_in_permanent_sso']) {
                                
                                $log = new SystemLog();
                                $log->type = 'login-error';
                                $log->humanized_message = 'User role detected';
                                $log->payload = 'AuthenticationController.php/loginSSO';
                                $log->message = 'User role detected';
                                $log->source = 'AuthenticationController.php';
                                $log->save();

                                return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                            } else {
                                $explode = explode('.', $sso_response['data']->data->id_token);
                                $decoded_id_token = base64_decode($explode[1]);
                                $decoded = json_decode($decoded_id_token);                            

                                $ml = new MemberLog();
                                $ml->member_id = $user->member->id;
                                $ml->message = "Login with email ".$email." via SSO";
                                $ml->after = \GuzzleHttp\json_encode(['email' => $email, 'user id' => $user->id, 'member id' => $user->member->id, 
                                    'bepoz id' => $user->member->bepoz_account_id, 'bepoz account number' => $user->member->bepoz_account_number,
                                    'bepoz account card number' => $user->member->bepoz_account_card_number, 'guid' => $decoded->guid] );
                                $ml->save();
                                
                                // Log::info("402");
                                $token = JWTAuth::attempt($credentials);
                                return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response['data']->data->id_token, 
                                    'login_token' => $user->member->login_token, 'access_token' => $sso_response['data']->data->access_token, 
                                    'refresh_token' => $sso_response['data']->data->refresh_token ]);
                            }

                        } else {

                            // create Local Account
                            $updateSSO = false;
                            // CALL SEARCH BEPOZ
                            $bepozSearchResult = $this->searchBepozEmail($email);

                            if ($bepozSearchResult) {
                                // DECODE ID TOKEN
                                $explode = explode('.', $sso_response['data']->data->id_token);
                                $decoded_id_token = base64_decode($explode[1]);
                                $decoded = json_decode($decoded_id_token);
                                // $login_token = $decoded->guid;

                                // CHECK WITH SSO
                                if (strtolower(trim($bepozSearchResult['AccountFull']['Email1st'])) == strtolower(trim($decoded->email))) {
                                    // EMAIL MATCH
                                } else {
                                    return response()->json(['status' => 'error', 'message' => 'Email not match between Bepoz and SSO', 'error' => 'bepoz_problem'], Response::HTTP_FORBIDDEN);
                                }

                                $password = $request->input('password');

                                $phone_number = !isset($decoded->phone_number) ? '' : $decoded->phone_number;

                                // Log::info(print_r($decoded, true));
                                // Log::info();
                                if (strtolower(trim($bepozSearchResult['AccountFull']['FirstName'])) == strtolower(trim($decoded->given_name))) {
                                    // BEPOZ FirstName MATCH WITH SSO FirstName
                                    $first_name = trim($decoded->given_name);
                                } else {
                                    return response()->json(['status' => 'error', 'message' => 'First Name not match between Bepoz and SSO', 'error' => 'bepoz_problem'], Response::HTTP_FORBIDDEN);
                                }

                                if (strtolower(trim($bepozSearchResult['AccountFull']['LastName'])) == strtolower(trim($decoded->family_name))) {
                                    // BEPOZ LastName MATCH WITH SSO LastName
                                    $last_name = $decoded->family_name;
                                } else {
                                    return response()->json(['status' => 'error', 'message' => 'Last Name not match between Bepoz and SSO', 'error' => 'bepoz_problem'], Response::HTTP_FORBIDDEN);
                                }

                                // SSO guid
                                $guid = $decoded->guid;

                                $tier = Tier::where('bepoz_group_id', $bepozSearchResult['AccountFull']['GroupID'])->first();
                                if (is_null($tier)) {
                                    $default_tier = Setting::where('key', 'default_tier')->first()->value;
                                    $tier = Tier::find($default_tier);
                                }

                                if (isset($decoded->claims)) {
                                    // CHECK BEPOZ ID SAME BETWEEN SSO AND MYPLACE
                                    if ($bepozSearchResult['AccountFull']['AccountID'] == $decoded->claims->bepoz_id) {
                                        // BEPOZ ID MATCH
                                        $bepoz_account_id = $bepozSearchResult['AccountFull']['AccountID'];
                                        $bepoz_account_number = $bepozSearchResult['AccountFull']['AccNumber'];
                                        $bepoz_account_card_number = $bepozSearchResult['AccountFull']['CardNumber'];
                                        // NEED TO SEND TO SSO
                                        $tier = Tier::where('bepoz_group_id', $bepozSearchResult['AccountFull']['GroupID'])->first();
                                    } else {
                                        if (is_null($decoded->claims->bepoz_id)) {
                                            // NEED TO SEND TO SSO
                                            $bepoz_account_id = $bepozSearchResult['AccountFull']['AccountID'];
                                            $bepoz_account_number = $bepozSearchResult['AccountFull']['AccNumber'];
                                            $bepoz_account_card_number = $bepozSearchResult['AccountFull']['CardNumber'];
                                            $tier = Tier::where('bepoz_group_id', $bepozSearchResult['AccountFull']['GroupID'])->first();
                                            // NEED TO SEND TO SSO
                                            $updateSSO = true;
                                        }
                                        if ($decoded->claims->bepoz_id == "") {
                                            // NEED TO SEND TO SSO
                                            $bepoz_account_id = $bepozSearchResult['AccountFull']['AccountID'];
                                            $bepoz_account_number = $bepozSearchResult['AccountFull']['AccNumber'];
                                            $bepoz_account_card_number = $bepozSearchResult['AccountFull']['CardNumber'];
                                            $tier = Tier::where('bepoz_group_id', $bepozSearchResult['AccountFull']['GroupID'])->first();
                                            // NEED TO SEND TO SSO
                                            $updateSSO = true;
                                        } else {
                                            $temp = $this->searchBepozID($decoded->claims->bepoz_id);
                                            if ($bepozSearchResult['AccountFull']['Email1st'] == $temp['AccountFull']['Email1st']) {
                                                // NEED ACCOUNTFIND
                                                $accfind = $this->AccountFindEmail($user->email);
                                                $bepoz_account_id = $accfind['AccountFull']['AccountID'];
                                                $bepoz_account_number = $accfind['AccountFull']['AccNumber'];
                                                $bepoz_account_card_number = $accfind['AccountFull']['CardNumber'];
                                                $tier = Tier::where('bepoz_group_id', $accfind['AccountFull']['GroupID'])->first();
                                                // NEED TO SEND TO SSO
                                                $updateSSO = true;
                                            } else {
                                                if ($bepozSearchResult['AccountFull']['Email1st'] == $email) {
                                                    $bepoz_account_id = $bepozSearchResult['AccountFull']['AccountID'];
                                                    $bepoz_account_number = $bepozSearchResult['AccountFull']['AccNumber'];
                                                    $bepoz_account_card_number = $bepozSearchResult['AccountFull']['CardNumber'];
                                                    $tier = Tier::where('bepoz_group_id', $bepozSearchResult['AccountFull']['GroupID'])->first();
                                                    // NEED TO SEND TO SSO
                                                    $updateSSO = true;
                                                } else if ($temp['AccountFull']['Email1st'] == $email) {
                                                    $bepoz_account_id = $temp['AccountFull']['AccountID'];
                                                    $bepoz_account_number = $temp['AccountFull']['AccNumber'];
                                                    $bepoz_account_card_number = $temp['AccountFull']['CardNumber'];
                                                    $tier = Tier::where('bepoz_group_id', $temp['AccountFull']['GroupID'])->first();
                                                    // NEED TO SEND TO SSO
                                                    $updateSSO = true;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    // SSO CLAIM NOT FOUND THEN JUST USE DATA FROM BEPOZ
                                    $bepoz_account_id = $bepozSearchResult['AccountFull']['AccountID'];
                                    $bepoz_account_number = $bepozSearchResult['AccountFull']['AccNumber'];
                                    $bepoz_account_card_number = $bepozSearchResult['AccountFull']['CardNumber'];
                                    $tier = Tier::where('bepoz_group_id', $bepozSearchResult['AccountFull']['GroupID'])->first();
                                    // NEED TO SEND TO SSO
                                    $updateSSO = true;
                                }


                                switch (intval($bepozSearchResult['AccountFull']['Status'])) {
                                    case 0:
                                        $bepoz_account_status = "OK";
                                        break;
                                    case 1:
                                        $bepoz_account_status = "On Hold";
                                        break;
                                    case 2:
                                        $bepoz_account_status = "Pending";
                                        break;
                                    case 3:
                                        $bepoz_account_status = "Suspended";
                                        break;
                                    case 4:
                                        $bepoz_account_status = "Inactive";
                                        break;
                                    case 5:
                                        $bepoz_account_status = "Deceased";
                                        break;
                                    default:
                                        $bepoz_account_status = "OK";
                                }

                                // CREATE LOCAL
                                $this->createLocalAccount($request, $email, $password, $phone_number,
                                    $first_name, $last_name, $guid, $bepoz_account_id, $bepoz_account_number,
                                    $bepoz_account_card_number, $bepoz_account_status, $tier->id);

                                // Log::info("createLocalAccount");
                                // Log::info($updateSSO);
                                // NEED UPDATE SSO
                                if ($updateSSO) {
                                    $user = User::where("email", $email)->first();
                                    // CHECK SSO GUID SAME WITH MYPLACE GUID
                                    $SSOAccountGUID = $this->SSOAccountGUID($email);

                                    // Log::info($SSOAccountGUID);
                                    // Log::info($user->member->login_token);
                                    if ($SSOAccountGUID == $user->member->login_token) {
                                        // UPDATE SSO
                                        $this->updateSSO($user);
                                    }
                                }

                                if ($sso_response['user_role_detected_in_permanent_sso']) {                                    
                                    $log = new SystemLog();
                                    $log->type = 'login-error';
                                    $log->humanized_message = 'User role detected';
                                    $log->payload = 'AuthenticationController.php/loginSSO';
                                    $log->message = 'User role detected';
                                    $log->source = 'AuthenticationController.php';
                                    $log->save();

                                    return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                                } else {

                                    $explode = explode('.', $sso_response['data']->data->id_token);
                                    $decoded_id_token = base64_decode($explode[1]);
                                    $decoded = json_decode($decoded_id_token);                            

                                    $ml = new MemberLog();
                                    $ml->member_id = $user->member->id;
                                    $ml->message = "Login with email ".$email." via SSO";
                                    $ml->after = \GuzzleHttp\json_encode(['email' => $email, 'user id' => $user->id, 'member id' => $user->member->id, 
                                        'bepoz id' => $user->member->bepoz_account_id, 'bepoz account number' => $user->member->bepoz_account_number,
                                        'bepoz account card number' => $user->member->bepoz_account_card_number, 'guid' => $decoded->guid] );
                                    $ml->save();
                                    
                                    // Log::info("402");
                                    $token = JWTAuth::attempt($credentials);
                                    return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response['data']->data->id_token, 
                                        'login_token' => $user->member->login_token, 'access_token' => $sso_response['data']->data->access_token, 
                                        'refresh_token' => $sso_response['data']->data->refresh_token ]);
                                }
                                
                            } else {

                                $log = new SystemLog();
                                $log->type = 'login-error';
                                $log->humanized_message = 'Bepoz problem';
                                $log->payload = 'AuthenticationController.php/loginSSO';
                                $log->message = 'Bepoz problem';
                                $log->source = 'AuthenticationController.php';
                                $log->save();

                                return response()->json(['status' => 'error', 'message' => 'Bepoz problem', 'error' => 'bepoz_problem'], Response::HTTP_FORBIDDEN);
                            }

                        }

                        if ($sso_response['user_role_detected_in_permanent_sso']) {
                            return response()->json(['status' => 'error', 'message' => 'Sorry, this backpanel is for internal users only', 'error' => 'denied_via_sso'], Response::HTTP_FORBIDDEN);
                        } else {
                            // Log::info("402");
                            $token = JWTAuth::attempt($credentials);
                            return response()->json(['status' => 'ok', 'token' => $token, 'id_token' => $sso_response['data']->data->id_token,
                                'login_token' => $user->member->login_token, 'access_token' => $sso_response['data']->data->access_token,
                                'refresh_token' => $sso_response['data']->data->refresh_token]);
                        }
                    } else {
                        // failed BP login + failed SSo login
                        $log = new SystemLog();
                        $log->type = 'login-error';
                        $log->humanized_message = $sso_response['message'];
                        $log->payload = 'AuthenticationController.php/loginSSO';
                        $log->message = 'SSO problem';
                        $log->source = 'AuthenticationController.php';
                        $log->save();

                        return response()->json(['status' => 'error', 'message' => $sso_response['message']], Response::HTTP_FORBIDDEN);
                    }
                }

            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function overrideLocalPassword($email, $password)
    {
        $user = User::where('email', strtolower($email))->first();
        if (!is_null($user)) {
            $user->password = Hash::make($password);
            $user->save();
            return $user;
        }

        return null;
    }

    protected function createSSOAccount($email, $password)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $user = User::with('member')->where('email', strtolower($email))->first();
        
        $datasend = array(
            'license_key' => $your_order_key,
            'email' => $user->email,
            'password' => $password,
            'family_name' => $user->member->last_name,
            'given_name' => $user->member->first_name,
            'phone_number' => $user->mobile,
            'bepoz_id' => $user->member->bepoz_account_id,
            'bepoz_account_number' => $user->member->bepoz_account_number,
            'bepoz_account_card_number' => $user->member->bepoz_account_card_number,
            'name' => $user->member->first_name
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/register");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return true;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }

    protected function checkSSOAccount($email)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            'email' => $email,
            "license_key" => $your_order_key,
            "request_token" => 1
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/checkSubAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);

        // if ($payload_check->status == "ok") {
        //     return true;
        // }

        // if ($payload_check->status == "error") {
        //     return false;
        // }

        // return false;

        // $formatted_response = [
        //     'status' => 'error',
        //     'error' => 'SSO problem'
        // ];

        // $formatted_response = array(
        //     'status' => 'error',
        //     'error' => 'SSO problem'
        // );

        $formatted_response = array();
        $formatted_response['status'] = 'error';
        $formatted_response['error'] = 'SSO problem';

        // $array_formatted_response = json_decode(json_encode($formatted_response), true);
        $array_formatted_response = \GuzzleHttp\json_decode(json_encode($formatted_response));

        if ($content_check) {
            // Log::info("818");
            $payload_check = \GuzzleHttp\json_decode($content_check, true);
            return $payload_check;
        } else {
            // Log::info("823");
            return $array_formatted_response;
        }

        // Log::info("827");
        return $array_formatted_response;
    }

    protected function checkSSOAccountByMobile($mobile)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            "mobile" => $mobile,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/checkSubAccountByMobile");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);

        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return true;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }

    protected function overrideSSOPassword($email, $password)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datasend = array(
            'email' => $email,
            'password' => $password,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/changePassword");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return true;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }

    protected function loginSSOUSer($email, $password, $request)
    {
        // TRY LOGIN FIRST
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datasend = array(
            'email' => $email,
            'password' => $password,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info($content);

        $user_role_detected_in_permanent_sso = false;
        $login_successful_via_permanent_sso = false;

        if ($content) {
            $payload = \GuzzleHttp\json_decode($content);

            if ($payload->status == "ok") {
                $explode = explode('.', $payload->data->id_token);
                $decoded_id_token = base64_decode($explode[1]);
                $decoded = json_decode($decoded_id_token);
                $login_token = $decoded->guid;

                $user = User::where('email', $email)->first();
                if (is_null($user)) {
                    // NEED LOAD BEPOZ FOR ACCOUNT ID
                    $bepoz = new Bepoz;
                    $result = $bepoz->accountSearch(8, $email);

                    if ($result) {
                        if ($result['IDList']['Count'] == 0) {
                            // ACCOUNT not found
                            $formatted_response = [
                                'status' => 'error',
                                'error' => 'Account not found',
                                'message' => 'Account not found in Bepoz',
                                'data' => null,
                                'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                                'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                            ];

                            return $formatted_response;
                        } else if ($result['IDList']['Count'] == 1) {
                            // ACCOUNT found 1, load details from Bepoz
                            $temp = $bepoz->AccountFullGet($result['IDList']['ID']);

                            if ($temp['AccountFull']['Status'] == 0) {

                                // Log::info("create user local");
                                // Log::info(print_r($decoded, true));

                                $credentials = array(
                                    'email' => $email,
                                    'password' => Hash::make($password),
                                    'mobile' => !isset($decoded->phone_number) ? '' : $decoded->phone_number
                                );
                                $user = User::create($credentials);

                                $member = new Member;
                                $member->user_id = $user->id;
                                $member->first_name = !isset($decoded->given_name) ? $decoded->name : $decoded->given_name;
                                $member->last_name = !isset($decoded->family_name) ? '' : $decoded->family_name;
                                $member->share_info = 1;
                                $member->current_preferred_venue_name = "";
                                $member->original_preferred_venue_name = "";
                                $member->dob = null;
                                $member->postcode = null;
                                $member->login_token = $decoded->guid;
                                $member->bepoz_account_id = $temp['AccountFull']['AccountID'];
                                $member->bepoz_account_number = $temp['AccountFull']['AccNumber'];
                                $member->bepoz_account_card_number = $temp['AccountFull']['CardNumber'];
                                $member->save();

                                $user_obj = User::find($user->id); // refresh...
                                $user_obj->member->save();

                                $user->email_confirmation = true;
                                $user->email_confirmed_at = Carbon::now(config('app.timezone'));
                                $user->save();

                                $user_role = new UserRoles;
                                $user_role->role_id = 3; // Manually add
                                $user_role->user_id = $user->id;
                                $user_role->save();

                                $default_tier = Setting::where('key', 'default_tier')->first()->value;

                                // $tier = Tier::find($default_tier);
                                $tier = Tier::where('bepoz_group_id', $temp['AccountFull']['GroupID'])->first();
                                // QUEENSBERRY FEATURE
                                // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                                // if ($default_member_expiry_date == "true"){
                                //     $setting = Setting::where('key', 'cut_off_date')->first();
                                //     $date_expiry = Carbon::parse($setting->value);
                                //     $date_expiry->setTimezone(config('app.timezone'));
                                //     $date_expiry->setTime(0, 0, 0);
                                // } else {
                                //     $date_expiry = null;
                                // }

                                $date_expiry = null;
                                $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                                $member->current_preferred_venue = $tier->venue_id;
                                $member->current_preferred_venue_name = $tier->name;
                                $member->save();

                                if (!is_null($user)) {
                                    $user->member->member_tier->tier;
                                    $ml = new MemberLog();
                                    $ml->member_id = $user->member->id;
                                    $ml->message = "This member sign-up using first time signin, already in SSO";
                                    $ml->after = \GuzzleHttp\json_encode($user);
                                    $ml->save();
                                }

                                $this->getReward($request, $user);
                                // $user_role_detected_in_permanent_sso = true;
                            } else {
                                // ACCOUNT INACTIVE
                                $formatted_response = [
                                    'status' => 'error',
                                    'error' => 'Account inactive',
                                    'message' => 'Account inactive in Bepoz',
                                    'data' => null,
                                    'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                                    'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                                ];

                                return $formatted_response;
                            }
                        } else {
                            // ACCOUNT found mpre than 1
                            $formatted_response = [
                                'status' => 'error',
                                'error' => 'Account found more than 1',
                                'message' => 'Account found more than 1 in Bepoz',
                                'data' => null,
                                'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                                'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                            ];

                            return $formatted_response;
                        }
                    } else {
                        // Bepoz connection problem
                        $formatted_response = [
                            'status' => 'error',
                            'error' => 'Bepoz connection problem',
                            'message' => 'Bepoz connection problem',
                            'data' => null,
                            'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                            'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                        ];

                        return $formatted_response;
                    }
                } else {
                    // ONLY bepoz_admin PROHIBITED TO LOGIN IN THE APP
                    if ($user->roles[0]->name == "bepoz_admin") {
                        $user_role_detected_in_permanent_sso = true;
                        // Log::info("detected bepoz_admin");
                    }
                }

                $formatted_response = [
                    'status' => 'ok',
                    'error' => 'no',
                    'message' => 'ok',
                    'data' => $payload,
                    'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                    'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                ];

            } else if ($payload->status == "error") {
                $formatted_response = [
                    'status' => 'error',
                    'error' => $payload->error,
                    'message' => $payload->message,
                    'data' => null,
                    'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                    'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                ];
            }

            return $formatted_response;
        }

        return false;
    }

    private function checkLocalAccount($email)
    {
        $user = User::where('email', strtolower($email))->first();
        if (is_null($user)) {
            return false;
        }

        return true;
    }

    protected function loginSSOOnly($email, $password, $request)
    {
        // TRY LOGIN FIRST
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datasend = array(
            'email' => $email,
            'password' => $password,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info($content);

        $user_role_detected_in_permanent_sso = false;
        $login_successful_via_permanent_sso = false;

        if ($content) {
            $payload = \GuzzleHttp\json_decode($content);

            if ($payload->status == "ok") {
                // $explode = explode('.', $payload->data->id_token);
                // $decoded_id_token = base64_decode($explode[1]);
                // $decoded = json_decode($decoded_id_token);
                // $login_token = $decoded->guid;

                $formatted_response = [
                    'status' => 'ok',
                    'error' => 'no',
                    'message' => 'ok',
                    'data' => $payload,
                    'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                    'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                ];

                return $formatted_response;

            } else if ($payload->status == "error") {
                $formatted_response = [
                    'status' => 'error',
                    'error' => $payload->error,
                    'message' => $payload->message,
                    'data' => null,
                    'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
                    'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
                ];
            }

            return $formatted_response;
        }

        $formatted_response = [
            'status' => 'error',
            'error' => "default",
            'message' => "default",
            'data' => null,
            'login_successful_via_permanent_sso' => $login_successful_via_permanent_sso,
            'user_role_detected_in_permanent_sso' => $user_role_detected_in_permanent_sso
        ];
        return $formatted_response;
    }

    protected function createLocalAccount($request, $email, $password, $phone_number, 
        $first_name, $last_name, $guid, $bepoz_account_id, $bepoz_account_number, 
        $bepoz_account_card_number, $bepoz_account_status, $tier_id) 
    {

        $user = User::where('email', $email)->first();

        if (is_null($user)) {
            // Log::info("create user local");
            // Log::info(print_r($decoded, true));

            $credentials = array(
                'email' => $email,
                'password' => Hash::make($password),
                'mobile' => !isset($phone_number) ? '' : $phone_number
            );
            $user = User::create($credentials);

            $member = new Member;
            $member->user_id = $user->id;
            $member->first_name = !isset($first_name) ? '' : $first_name;
            $member->last_name = !isset($last_name) ? '' : $last_name;
            $member->share_info = 1;
            $member->current_preferred_venue_name = "";
            $member->original_preferred_venue_name = "";
            $member->dob = null;
            $member->postcode = null;
            $member->login_token = $guid;
            $member->bepoz_account_id = $bepoz_account_id;
            $member->bepoz_account_number = $bepoz_account_number;
            $member->bepoz_account_card_number = $bepoz_account_card_number;

            switch (intval($bepoz_account_status)) {
                case 0:
                    $member->bepoz_account_status = "OK";
                    break;
                case 1:
                    $member->bepoz_account_status = "On Hold";
                    break;
                case 2:
                    $member->bepoz_account_status = "Pending";
                    break;
                case 3:
                    $member->bepoz_account_status = "Suspended";
                    break;
                case 4:
                    $member->bepoz_account_status = "Inactive";
                    break;
                case 5:
                    $member->bepoz_account_status = "Deceased";
                    break;
                default:
                    $member->bepoz_account_status = "OK";
            }

            $member->save();

            $user_obj = User::find($user->id); // refresh...
            $user_obj->member->save();

            $user->email_confirmation = true;
            $user->email_confirmed_at = Carbon::now(config('app.timezone'));
            $user->save();

            $user_role = new UserRoles;
            $user_role->role_id = 3; // Manually add
            $user_role->user_id = $user->id;
            $user_role->save();

            $default_tier = Setting::where('key', 'default_tier')->first()->value;

            // $tier = Tier::find($default_tier);
            $tier = Tier::where('id', $tier_id)->first();
            // QUEENSBERRY FEATURE
            // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
            // if ($default_member_expiry_date == "true"){
            //     $setting = Setting::where('key', 'cut_off_date')->first();
            //     $date_expiry = Carbon::parse($setting->value);
            //     $date_expiry->setTimezone(config('app.timezone'));
            //     $date_expiry->setTime(0, 0, 0);
            // } else {
            //     $date_expiry = null;
            // }

            $date_expiry = null;
            $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

            $member->current_preferred_venue = $tier->venue_id;
            $member->current_preferred_venue_name = $tier->name;
            $member->save();

            if (!is_null($user)) {
                $user->member->member_tier->tier;
                $ml = new MemberLog();
                $ml->member_id = $user->member->id;
                $ml->message = "This member sign-up using first time signin, already in SSO";
                $ml->after = \GuzzleHttp\json_encode($user);
                $ml->save();
            }

            $this->getReward($request, $user);
            // $user_role_detected_in_permanent_sso = true;
            
            if (!is_null($user)) {
                $user->member->member_tier->tier;
                $ml = new MemberLog();
                $ml->member_id = $user->member->id;
                $ml->message = "This member sign-up using first time signin, already in SSO";
                $ml->after = \GuzzleHttp\json_encode($user);
                $ml->save();
            }
            
            // BEPOZ EMAIL NOT MATCH WITH MYPLACE
            $formatted_response = [
                'status' => 'ok',
                'error' => 'no',
                'message' => 'Account from SSO successfully created in myplace',
                'data' => null
            ];

            return $formatted_response;

        } else {
            // ACCOUNT found mpre than 1
            $formatted_response = [
                'status' => 'error',
                'error' => 'Account already exist',
                'message' => 'Account already exist in Myplace',
                'data' => null
            ];

            return $formatted_response;
        }

    }

    protected function searchBepozEmail($email)
    {
        // NEED LOAD BEPOZ FOR ACCOUNT ID
        $bepoz = new Bepoz;
        $result = $bepoz->accountSearch(8, $email);

        if ($result) {

            if ($result['IDList']['Count'] == 0) {
                // ACCOUNT not found
                return false;
            } else if ($result['IDList']['Count'] == 1) {
                // ACCOUNT found 1, load details from Bepoz
                $temp = $bepoz->AccountFullGet($result['IDList']['ID']);

                if ($temp['AccountFull']['Status'] == 0) {
                    // ACCOUNT ACTIVE
                    return $temp;
                } else {
                    // ACCOUNT INACTIVE
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return false;
    }

    protected function searchBepozID($bepozID)
    {
        // NEED LOAD BEPOZ FOR ACCOUNT ID
        $bepoz = new Bepoz;
        $temp = $bepoz->AccountFullGet($bepozID);

        if ($temp['AccountFull']['Status'] == 0) {
            // ACCOUNT ACTIVE
            return $temp;
        } else {
            // ACCOUNT INACTIVE
            return false;
        }
    }

    protected function AccountFindEmail($email)
    {
        // NEED LOAD BEPOZ FOR ACCOUNT ID
        $bepoz = new Bepoz;
        $temp = $bepoz->AccountFind($email);

        if ($temp['AccountFull']['Status'] == 0) {
            // ACCOUNT ACTIVE
            return $temp;
        } else {
            // ACCOUNT INACTIVE
            return false;
        }
    }

    protected function updateSSO($user)
    {
        try {
            $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

            $datasend = array(
                'given_name' => $user->member->first_name,
                'family_name' => $user->member->last_name,
                'phone_number' => $user->mobile,
                'email' => $user->email,
                'bepoz_id' => $user->member->bepoz_account_id,
                'bepoz_account_number' => $user->member->bepoz_account_number,
                'bepoz_account_card_number' => $user->member->bepoz_account_card_number,
                'guid' => $user->member->login_token,
                'license_key' => $your_order_key
            );

            // $payload = json_encode( $datasend );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $your_order_api . "/updateAccount");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // Log::warning("AuthenticationController updateSSO");
            // Log::warning($your_order_api);
            // Log::warning($payload);
            // Log::warning($content);
            // Log::warning($httpCode);

            if ($content) {
                $payload = \GuzzleHttp\json_decode($content);
                if ($payload->status == "error" && $payload->error == "user_not_found") {
                    // return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                    return false;
                }

                return true;
            } else {
                // return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                return false;
            }

            return false;

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function SSOAccountGUID($email)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            'email' => $email,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/checkSubAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);

        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return $payload_check->guid;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }

    /**
     * quickSignIn through our API
     * >> for member app access
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function quickSignIn(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'auth_token' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $member = Member::where('login_token', $request->input('auth_token'))->first();
            $user = $member->user;
            if (is_null($user)) {
                return response()
                    ->json(['status' => 'error', 'message' => 'User not found / Token invalid'], Response::HTTP_BAD_REQUEST);
            }

            $token = JWTAuth::fromUser($user);

            return response()->json(['status' => 'ok', 'token' => $token]);

        } catch (\Exception $e) {
            // \Helper::logUnhandledError($e, ['request' => $request->input('auth_token')]);
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Sign in + sign up with social login
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function socialLogin(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155',
                'type' => 'required',
                'first_name' => 'required',
                'last_name' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();


            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $type = $request->input('type');
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');

            $user_obj = User::where(function ($query) use ($request, $email) {

                $query->where('email', $email);

                $query->orWhereHas('social_logins', function ($q) use ($request, $email) {
                    $q->where('email', $email);
                });
            })->first();

            $please_create_this_user = false;
            if (is_null($user_obj)) {
                $please_create_this_user = true;
            } else {
                if ($user_obj->member->status == 'inactive') {
                    return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
                }
            }

            if ($please_create_this_user) {
                // new member

                if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                    return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
                }

                if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                    return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
                }

                $user = User::create([
                    'email' => $email,
                    'mobile' => $request->has('mobile') ? $request->input('mobile') : null,
                    'email_confirmation' => true
                ]);

                Member::create([
                    'user_id' => $user->id,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'dob' => $request->has('dob') ? new Carbon($request->input('dob'), config('app.timezone')) : null
                ]);

                $user_obj = User::find($user->id); // refresh...

                $user_role = new UserRoles();
                $user_role->role_id = 3; // Manually add
                $user_role->user_id = $user->id;
                $user_role->save();

                SocialLogin::create([
                    'user_id' => $user->id,
                    'type' => $type,
                    'uid' => $request->has('uid') ? $request->input('uid') : null,
                    'email' => $email
                ]);

                if ($request->has('tier_id')) {

                    $tier = Tier::find($request->input('tier_id'));
                    if (is_null($tier)) {
                        $tier = Tier::first();
                    }
                    // QUEENSBERRY FEATURE
                    // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                    // if ($default_member_expiry_date == "true"){
                    //     $setting = Setting::where('key', 'cut_off_date')->first();
                    //     $date_expiry = Carbon::parse($setting->value);
                    //     $date_expiry->setTimezone(config('app.timezone'));
                    //     $date_expiry->setTime(0, 0, 0);
                    // } else {
                    //     $date_expiry = null;
                    // }

                    $date_expiry = null;
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);


                } else {
                    //
                    $tier = Tier::first();
                    // QUEENSBERRY FEATURE
                    // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                    // if ($default_member_expiry_date == "true"){
                    //     $setting = Setting::where('key', 'cut_off_date')->first();
                    //     $date_expiry = Carbon::parse($setting->value);
                    //     $date_expiry->setTimezone(config('app.timezone'));
                    //     $date_expiry->setTime(0, 0, 0);
                    // } else {
                    //     $date_expiry = null;
                    // }

                    $date_expiry = null;
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                }

                $data = array(
                    "member_id" => $user_obj->member->id,
                    "type" => $type
                );

                $job = new BepozJob();
                $job->queue = "sync-account";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();

                $this->getReward($request, $user_obj);

                dispatch(new SendWelcomeEmail($user_obj));

                // //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                // $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/register";
                // $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                // // $encoded = json_encode($data);
                // // $checksum = md5($encoded);

                // $datasend = array(
                //     'email' => $email,
                //     'password' => $request->input('password'),
                //     'name' => $first_name." ".$last_name,
                //     'given_name' => $first_name,
                //     'family_name' => $last_name,
                //     'phone_number' => $mobile,
                //     "license_key" => $your_order_key
                // );

                // $ch = curl_init();
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                // curl_setopt($ch, CURLOPT_URL, $your_order_api);
                // curl_setopt($ch, CURLOPT_POST, 1);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                // curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                // $content = curl_exec($ch);
                // $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                // // Log::warning($content);
                // // Log::warning($httpCode);

                // if ($content) {
                //     $payload = \GuzzleHttp\json_decode($content);
                //     if ($payload->status == "error" && $payload->error == "user_not_found") {
                //         return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                //     }
                //     if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                //         return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                //     }

                //     // $member->login_token = $login_token;
                //     // $member->save();

                // } else {
                //     return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                // }

                // $user = User::find($user->id); // refresh...
                // $token = JWTAuth::fromUser($user);
                // //NOT REQUIRED TO RESET PASSWORD
                // $reset_password = 0;

            } else {
                // login
                $user = User::find($user_obj->id);
                $sl = SocialLogin::where('user_id', $user_obj->id)->where('type', $type)->first();
                if (is_null($sl)) {
                    SocialLogin::create([
                        'user_id' => $user->id,
                        'type' => $type,
                        'uid' => $request->has('uid') ? $request->input('uid') : null,
                        'email' => $email
                    ]);
                }

                // //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                // $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/login";
                // $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                // $datasend = array(
                //     'email' => $email,
                //     'password' => $request->input('password'),
                //     "license_key" => $your_order_key
                // );

                // $ch = curl_init();
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                // curl_setopt($ch, CURLOPT_URL, $your_order_api);
                // curl_setopt($ch, CURLOPT_POST, 1);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                // curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                // curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                // $content = curl_exec($ch);
                // $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                // if ($content) {
                //     $payload = \GuzzleHttp\json_decode($content);
                //     if ($payload->status == "error" && $payload->error == "user_not_found") {
                //         return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                //     }
                //     if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                //         return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                //     }
                //     if ($payload->status == "error" && $payload->error == "invalid_email_password") {
                //         return response()->json(['status' => 'error', 'message' => 'Invalid email or password'], Response::HTTP_BAD_REQUEST);
                //     }

                //     // $base64 = base64_decode($payload->data->id_token);
                //     // $base64 = substr($base64, 0, strpos($base64, "}}"))."}}";
                //     // $base64 = str_replace('{"typ":"JWT","alg":"HS256"}', '', $base64);
                //     // $json = json_decode($base64);
                //     // $login_token = $json->claims->guid;

                //     $explode = explode('.', $payload->data->id_token);
                //     $decoded_id_token = base64_decode($explode[1]);
                //     $decoded = json_decode($decoded_id_token);
                //     $login_token = $decoded->guid;

                //     $credentials = ['email' => $email, 'password' => $request->input('password'), 'deleted_at' => null];
                //     $token = JWTAuth::attempt($credentials);

                // } else {
                //     // return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                // }
            }

            DB::commit();

            if ($user->member->status == 'inactive') {
                return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
            }

            $token = JWTAuth::fromUser($user);

            return response()->json(['status' => 'ok', 'token' => $token]);


            // return response()->json(['status' => 'ok', 'reset_password' => $reset_password, 'token' => $token,
            //     'id_token' => $payload->data->id_token, 'access_token' => $payload->data->access_token,
            //     'refresh_token' => $payload->data->refresh_token ]);


        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Register a new user and grant him a member role
     * >> Generate a JWT token
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function signup(Request $request, ImageOptimizer $imageOptimizer, MandrillExpress $mx, MailjetExpress $mjx, Bepoz $bepoz)
    {

        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|max:155',
                'password' => 'required|confirmed|min:4',
                'password_confirmation' => 'required|min:4',
                'first_name' => 'required',
                'last_name' => 'required',
                'share_info' => 'required'
                //'dob' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // CHECK BEPOZ CUSTOM FIELD REQUIRED
            $bepozcustomfield_enable = Setting::where('key', 'bepozcustomfield_enable')->first()->value;
            $customfieldpayload = [];
            $standardfieldpayload = [];
            $allInputpayload = [];
            // if ($bepozcustomfield_enable === 'true') {
                // VALIDATE BEPOZ CUSTOM FIELD BASED ON SETTING
                $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first()->extended_value;
                // echo $bepozcustomfield."<br<";
                $bepozcustomfielddecode = \GuzzleHttp\json_decode($bepozcustomfield);
                // echo print_r($data, true)."<br>";

                if (sizeof($bepozcustomfielddecode)) {

                    foreach ($bepozcustomfielddecode as $cf) {
                        // Log::info($cf);
                        // Log::info($cf->fieldName);

                            if (!$request->has($cf->id)) {
                                return response()->json(['status' => 'error', 'message' => $cf->fieldName . ' is required.'], Response::HTTP_BAD_REQUEST);
                            }

                        if ( $bepoz->isCustomField($cf) ) {
                            $customfieldpayload[] = array(
                                'id' => $cf->id,
                                'value' => $request->input($cf->id),
                                'fieldName' => $cf->fieldName,
                                'fieldType' => $cf->fieldType,
                                'bepozFieldNum' => $cf->bepozFieldNum
                            );
                        } else {
                            $standardfieldpayload[] = array(
                                'id' => $cf->id,
                                'value' => $request->input($cf->id),
                                'fieldName' => $cf->fieldName,
                                'fieldType' => $cf->fieldType,
                                'bepozFieldNum' => $cf->bepozFieldNum
                            );
                        }
        
                        // $allInputpayload[] = array(
                        //     $cf->id => $request->input($cf->id)
                        // );

                        if ($cf->id == "password") {
                            // NO NEED SAVE PASSWORD IN PAYLOAD
                        } else {
                            $allInputpayload[$cf->id] = $request->input($cf->id);
                        }
                    }
                }
            // }

            // Log::info($customfieldpayload);


            $share_info = $request->input('share_info') == 'true' ? 1 : 0;
            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $password = $request->input('password');
            $first_name = $request->input('first_name');
            if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }

            $last_name = $request->input('last_name');
            if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }
            $mobile = $request->input('mobile');
            $credentials = array(
                'email' => $email,
                'password' => Hash::make($password),
                'mobile' => $mobile
            );

            if ($request->has('mobile')) {
                $credentials['mobile'] = $request->input('mobile');
            }

            $user_obj = User::where('email', $email)->first();

            // $guid = new Guid;
            // $login_token = $guid->create();

            $bepozSearch = $this->bepozSearch($email);

            // FEATURE FROM JDA
            $signup_matching_bepoz_group_venue_tier = Setting::where('key', 'signup_matching_bepoz_group_venue_tier')->first()->value;
            $venueID = 0;
            $member_default_tier = Setting::where('key', 'member_default_tier')->first();
            $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);

            if ( $signup_matching_bepoz_group_venue_tier == "true" ) {
                
                if ($bepozSearch) {
                    // IF USER EXIST IN BEPOZ
                    // Log::info($bepozSearch);
                    $tier = Tier::where("bepoz_group_id", $bepozSearch['AccountFull']['GroupID'])->first();
                    if (!is_null($tier)) {
                        // if ($tier->venue_id != '' && intval($tier->venue_id) > 0) {
                        //     $venueID = $tier->venue_id;
                        // }

                        foreach ($tierOption as $to) {
                            if ($to["tier"] == $tier->id) {
                                $venueID = $to["id"];
                            }
                        }
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. No Tier that associated with Bepoz GroupID: ' . $bepozSearch['AccountFull']['GroupID']], Response::HTTP_BAD_REQUEST);
                    }
                } else {

                    if ($request->has('venue_id')) {
                        if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                            $venueID = $request->input('venue_id');
                        } else {
                            // $default_tier = Setting::where('key', 'default_tier')->first()->value;
                            // $tier = Tier::find($default_tier);
                            // if (!is_null($tier)) {
                            //     if ($tier->venue_id != '' && intval($tier->venue_id) > 0) {
                            //         $venueID = $tier->venue_id;
                            //     } else {
                            //         return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. No Venue ID specified.'], Response::HTTP_BAD_REQUEST);
                            //     }
                            // } else {
                            //     return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. Default Tier Object doesnt exist'], Response::HTTP_BAD_REQUEST);
                            // }

                            foreach ($tierOption as $to) {
                                if ($to["id"] == $request->input('venue_id')) {
                                    $venueID = $to["id"];
                                }
                            }

                            if ( intval($venueID) == 0 ) {
                                return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. No Venue ID specified.'], Response::HTTP_BAD_REQUEST);
                            }

                        }
                    } else {
                        if ($request->has('tier_id') && !empty($request->input('tier_id'))) {
                            
                            $tier = Tier::find($request->input('tier_id'));
                            if (!is_null($tier)) {
                                // if ($tier->venue_id != '' && intval($tier->venue_id) > 0) {
                                //     // $venueID = $tier->venue_id;
                                // } else {
                                //     return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. VenueID not set on Specified Tier'], Response::HTTP_BAD_REQUEST);
                                // }

                                foreach ($tierOption as $to) {
                                    if ($to["tier"] == $tier->id) {
                                        $venueID = $to["id"];
                                    }
                                }
                            } else {
                                return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. Default Tier Object doesnt exist'], Response::HTTP_BAD_REQUEST);
                            }
                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. No Tier ID specified'], Response::HTTP_BAD_REQUEST);
                        }
                    }
                }

                if (intval($venueID) > 0) {
                    $venue = Venue::find($venueID);

                    if ($venue->allow_signup_for_existing_customer_only) {
                        // $bepozSearch = $this->bepozSearch($email);
                        if ($bepozSearch) {
                            // conttinue signup (will matching with the cron)
                            // Log::info("bepozSearch");
                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Please contact our staff'], Response::HTTP_BAD_REQUEST);
                        }
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found'], Response::HTTP_BAD_REQUEST);
                }
                // Log::info("signup 2389 signup_matching_bepoz_group_venue_tier");
                // Log::info("venueID ".$venueID);
            }
            
            $cansignup = false;
            $checkGamingAccountLowest = false;

            $checkBepozGamingAccount = $this->checkBepozGamingAccount($bepoz, $request);
            $checkGamingAccountLowest = $checkBepozGamingAccount['checkGamingAccountLowest'];
            // Log::info($checkBepozGamingAccount);


            if ($checkBepozGamingAccount['status'] == 'ok') {
                $cansignup = $checkBepozGamingAccount['cansignup'];
                $payloadGamingAccountLowest = $checkBepozGamingAccount['payload'];

                foreach ($payloadGamingAccountLowest->data as $data) {
                    if (!isset($data->accountid)) {
                        return response()->json(['status' => 'error', 'message' => 'Need contact administrator'], Response::HTTP_BAD_REQUEST);
                    }
                }
            } else {
                return response()->json(['status' => 'error', 'message' => $checkBepozGamingAccount['message']], Response::HTTP_BAD_REQUEST);
            }

            if ($cansignup) {                    
                if (!is_null($user_obj)) {
                    // Log::info("signup 2392 ");
                    if ($user_obj->member->status == 'active') {
                        return response()->json(['status' => 'error', 'message' => 'signup failed. Your email has been used.'], Response::HTTP_BAD_REQUEST);
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
                    }

                } else {

                    // CHECK IF SSO ACCOUNT ALREADY EXIST
                    $checkSSOAccount = $this->checkSSOAccount($email);

                    if ($checkSSOAccount['status'] == 'ok') {
                        $emailSSOCan = false;
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
                    } else {
                        if ($checkSSOAccount['error'] == 'SSO problem') {
                            return response()->json(['status' => 'error', 'message' => 'Please contact administrator'], Response::HTTP_BAD_REQUEST);
                        }

                        $emailSSOCan = true;
                    }

                    $cansignup = $emailSSOCan;

                    $sso_response = $this->loginSSOUSer($email, $request->input('password'), $request);
                    // Log::info($sso_response);
                    // Log::info($sso_response['status']);

                    if ($sso_response['status'] == 'ok') {
                        // Log::info("signup 2458 ");
                        // successful sso login
                        $token = JWTAuth::attempt($credentials);
                        $first_time = 1;
                        return response()->json(['status' => 'ok', 'token' => $token,
                            'first_time' => $first_time,
                            'id_token' => $sso_response['data']->data->id_token,
                            'access_token' => $sso_response['data']->data->access_token,
                            'refresh_token' => $sso_response['data']->data->refresh_token]);

                    } else {
                        DB::beginTransaction();

                        // failed SSo login
                        $user = User::create($credentials);
                        // Log::info("signup 2469 ");
                        // Member::create([
                        //     'user_id' => $user->id,
                        //     'first_name' => $first_name,
                        //     'last_name' => $last_name,
                        //     'bepoz_account_card_number' => $request->has("card_number") ? $request->input("card_number") : null,
                        //     'dob' => $request->has('dob') ? new Carbon($request->input('dob'), config('app.timezone')) : null,
                        //     'login_token' => $login_token
                        // ]);

                        $member = new Member;
                        $member->user_id = $user->id;
                        $member->first_name = $first_name;
                        $member->last_name = $last_name;
                        $member->share_info = $share_info;
                        $member->current_preferred_venue_name = "";
                        $member->original_preferred_venue_name = "";

                        // if ( $gaming_system_enable == "true" ) {
                        //     $member->bepoz_account_number = $bepoz_account_number;
                        // }

                        if ($request->has('dob')) {
                            if ($request->input('dob') != '') {
                                $member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                            } else {
                                $member->dob = null;
                            }
                        } else {
                            $member->dob = null;
                        }
                        // Log::info("signup 2500 ");
                        $member->postcode = $request->has('postcode') ? $request->input('postcode') : null;

                        // // FEATURE FROM VARSITY, NO NEED BECAUSE CAN HANDLE WITH SIGNUP FIELDs
                        // $promo_code_enable = Setting::where('key', 'promo_code_enable')->first()->value;
                        // if ( $promo_code_enable == "true" ) {
                        //     $member->promo_code = $request->has('promo_code') ? $request->input('promo_code') : '';                    
                        // }

                        // // FEATURE FROM VARSITY, NO NEED BECAUSE CAN HANDLE WITH SIGNUP FIELDs
                        // $sponsorship_code_enable = Setting::where('key', 'sponsorship_code_enable')->first()->value;
                        // if ( $sponsorship_code_enable == "true" ) {
                        //     $member->sponsorship_code = $request->has('sponsorship_code') ? $request->input('sponsorship_code') : '';
                        // }

                        if ($checkGamingAccountLowest) {
                            // Log::info('AuthenticationController/signup use GamingAccountLowest details');
                            
                            // Log::info(print_r($payloadGamingAccountLowest, true));
                            
                            // HAVE NO ACCOUNT ID MEANS SIGNUP, NEED PUT FROM CHECK GAMING TO MYPLACE DATABASE
                            foreach ($payloadGamingAccountLowest->data as $data) {
                                // Log::warning("UpdateAccount 131");
                                if (isset($data->accountid)) {
                                    $member->bepoz_account_id = $data->accountid;
                                    $member->bepoz_account_number = $data->accnumber;
                                    $member->bepoz_account_card_number = $data->cardnumber;
                                }
                            }
                        } else {
                            // Log::info('AuthenticationController/signup signin no checkGamingAccountLowest');
                            // HAVE ACCOUNT ID MEANS SIGNIN MATCHING, JUST PUT TO MYPLACE DATABASE
                            $member->bepoz_account_id = $request->has('AccountID') ? $request->input('AccountID') : null;
                            $member->bepoz_account_number = $request->has('AccNumber') ? $request->input('AccNumber') : null;
                            $member->bepoz_account_card_number = $request->has('AccNumber') ? $request->input('AccNumber') : null;

                        }

                        $member->save();

                        $confirmationToken = $user->generateEmailConfirmationToken();

                        $user_obj = User::find($user->id); // refresh...
                        $user_obj->member->save();

                        $user_role = new UserRoles;
                        $user_role->role_id = 3; // Manually add
                        $user_role->user_id = $user->id;
                        $user_role->save();

                        $default_tier = Setting::where('key', 'default_tier')->first()->value;

                        // if ($request->has('tier_id')) {
                        //     $tier = Tier::find($request->input('tier_id'));
                        //     if (is_null($tier)) {
                        //         // $tier = Tier::first();
                        //         $tier = Tier::find($default_tier);
                        //     }
                        // } else {
                        //     // $tier = Tier::first();
                        //     $tier = Tier::find($default_tier);
                        // }
                        
                        if ( $signup_matching_bepoz_group_venue_tier == "true" ) {
                            // FEATURE FROM JDA
                            if ($bepozSearch) {
                                // conttinue signup (will matching with the cron)
                                $tier = Tier::where("bepoz_group_id", $bepozSearch['AccountFull']['GroupID'])->first();
                            } else {

                                // CHECK IF VENUE LINK WITH TIER
                                $tierVenue = Tier::where('venue_id', $venueID)->first();
                                if ( !is_null($tierVenue) ) {
                                    // THERE IS TIER LINK WITH THIS VENUE
                                    $tier = Tier::where('venue_id', $venueID)->first();
                                } else {
                                    // NO TIER LINK WITH THIS VENUE                                
                                    if ($request->has('tier_id')) {
                                        $tier = Tier::find($request->input('tier_id'));
                                        if (is_null($tier)) {
                                            // $tier = Tier::first();
                                            $tier = Tier::find($default_tier);
                                        }
                                    } else {
                                        // $tier = Tier::first();
                                        $tier = Tier::find($default_tier);
                                    }
                                }
                            }
                        } else {
                            
                            if ($request->has('tier_id')) {
                                $tier = Tier::find($request->input('tier_id'));
                                if (is_null($tier)) {
                                    // $tier = Tier::first();
                                    $tier = Tier::find($default_tier);
                                    $member_default_tier = Setting::where('key', 'member_default_tier')->first();
                                    if ($member_default_tier->value == 'no_default') {
                                        // NO NEED TO DO WILL SELECT DEFAULT TIER
                                    } else if ($member_default_tier->value == 'by_venue') {
                                        if ($request->has('venue_id')) {
                                            if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                                                $venue_id = $request->input('venue_id');
                                                $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                                                foreach ($tierOption as $to) {
                                                    if ($to["id"] == $venue_id) {
                                                        $tier = Tier::find($to["tier"]);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($member_default_tier->value == 'by_venue_tag') {
                                        if ($request->has('state_id')){
                                            $venue_tag = $request->input('state_id');
                                            $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                                            foreach ($tierOption as $to) {
                                                if ($to["id"] == $venue_tag) {
                                                    $tier = Tier::find($to["tier"]);
                                                }
                                            }
                                        }
                                    } else if ($member_default_tier->value == 'specific_tier') {
                                        // NO NEED TO DO WILL SELECT DEFAULT TIER
                                    }
                                }
                            } else {
                                // $tier = Tier::first();
                                $tier = Tier::find($default_tier);
                                $member_default_tier = Setting::where('key', 'member_default_tier')->first();
                                if ($member_default_tier->value == 'no_default') {
                                    // NO NEED TO DO WILL SELECT DEFAULT TIER
                                } else if ($member_default_tier->value == 'by_venue') {
                                    if ($request->has('venue_id')) {
                                        if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                                            $venue_id = $request->input('venue_id');
                                            $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                                            foreach ($tierOption as $to) {
                                                if ($to["id"] == $venue_id) {
                                                    $tier = Tier::find($to["tier"]);
                                                }
                                            }
                                        }
                                    }
                                } else if ($member_default_tier->value == 'by_venue_tag') {
                                    if ($request->has('state_id')){
                                        $venue_tag = $request->input('state_id');
                                        $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                                        foreach ($tierOption as $to) {
                                            if ($to["id"] == $venue_tag) {
                                                $tier = Tier::find($to["tier"]);
                                            }
                                        }
                                    }
                                } else if ($member_default_tier->value == 'specific_tier') {
                                    // NO NEED TO DO WILL SELECT DEFAULT TIER
                                }
                            }
                        }

                        // $tier = Tier::find($default_tier);
                        // QUEENSBERRY FEATURE
                        // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                    
                        // if ($default_member_expiry_date == "true"){
                        //     $setting = Setting::where('key', 'cut_off_date')->first();
                        //     $date_expiry = Carbon::parse($setting->value);
                        //     $date_expiry->setTimezone(config('app.timezone'));
                        //     $date_expiry->setTime(0, 0, 0);
                        // } else {
                        //     $date_expiry = null;
                        // }

                        $date_expiry = null;
                        $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                        if ($request->has('venue_id')) {
                            if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                                $venue = Venue::with('venue_tags')->find($request->input('venue_id'));
                                $member->current_preferred_venue = is_null($venue) ? '' : $venue->id;
                                $member->current_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                                $member->original_preferred_venue = is_null($venue) ? '' : $venue->id;
                                $member->original_preferred_venue_name = is_null($venue) ? '' : $venue->name;

                                if ( $signup_matching_bepoz_group_venue_tier == "true" ) {
                                    // FEATURE FROM JDA
                                    if (isset($venue->venue_tags[0])) {
                                        $customfieldpayload[] = array(
                                            'state_id' => $venue->venue_tags[0]->id
                                        );

                                        $customfieldpayload[] = array(
                                            'state_name' => $venue->venue_tags[0]->name
                                        );
                                    }
                                }
                            }
                        }

                        $member->payload = $allInputpayload;
                        // $member->payload = \GuzzleHttp\json_encode($allInputpayload);
                        $member->share_info = 1;
                        if ($request->has('opt_out_marketing')) {
                            $opt_out_marketing = $request->input('opt_out_marketing') == 'true' ? 1 : 0;
                            $member->opt_out_marketing = $opt_out_marketing;
                        }

                        // FEATURE FROM REDCAPE PUBLINC
                        if ($request->has('salutation')){
                            $member->salutation = $request->input('salutation');
                        }
                        if ($request->has('state_id')){
                            $member->state_id = $request->input('state_id');
                        }
                        if ($request->has('udid')){
                            $member->udid = $request->input('udid');
                        }
                        $member->save();

                        $user = User::find($user_obj->id); // refresh...

                        if (!is_null($user)) {
                            $user->member->member_tier->tier;
                            $ml = new MemberLog();
                            $ml->member_id = $user->member->id;
                            $ml->message = "This member sign-up and register SSO";
                            $ml->after = \GuzzleHttp\json_encode($user);
                            $ml->save();
                        }

                        if ($request->hasFile('photo')) {
                            if (config('bucket.s3')) {
                                $picture = $request->file('photo');
                                $mime = explode('/', $picture->getMimeType());
                                $type = end($mime);
                                $imageOptimizer->optimizeUploadedImageFile($picture);
                                Storage::disk('s3')->put('member/' . $user->member->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');
                                $user->member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $user->member->id . "/profile." . $type . "?" . time();
                                $user->member->save();

                            } else {
                                $log = new SystemLog();
                                $log->type = 'setting-error';
                                $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                                $log->payload = 'AuthenticationController.php/signup';
                                $log->message = 'Setting of S3 bucket not found.';
                                $log->source = 'AuthenticationController.php';
                                $log->save();

                                return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                            }

                        }

                        if ($checkGamingAccountLowest) {
                            $updateBepozGamingAccount = $this->updateBepozGamingAccount($bepoz, $member, $standardfieldpayload, $customfieldpayload, $request);
                        }

                        // Log::info("signup 3346 ");
                        if (!$user->email_confirmation){
                            Log::info("call sendEmailConfirmation");
                            $this->sendEmailConfirmation($user, $mx, $mjx);
                        }

                        $job = (new SendReminderNotification($user))->delay(60);
                        dispatch($job);

                        // NEW FEATURE MOBILE CONFIRMATION FROM REDCAPE PUBLINC
                        $mobile_confirmation_enable = Setting::where('key', 'mobile_confirmation_enable')->first()->value;
                        if ($mobile_confirmation_enable == "true"){
                            if (!$user->sms_confirmation){
                                $this->sendMobileConfirmation($user);
                            }
                        }
                        
                        // Log::info("signup 2672 ");
                        //change logic reward given when confirm
                        $this->getReward($request, $user);

                        DB::commit();

                        //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                        $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/register";
                        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                        // $encoded = json_encode($data);
                        // $checksum = md5($encoded);

                        $datasend = array(
                            'email' => $email,
                            'password' => $request->input('password'),
                            'name' => $first_name . " " . $last_name,
                            'given_name' => $first_name,
                            'family_name' => $last_name,
                            'phone_number' => $mobile,
                            "license_key" => $your_order_key
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_URL, $your_order_api);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                        $content = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        // Log::info("signup 2705 ");
                        // Log::warning($content);
                        // Log::warning($httpCode);

                        if ($content) {
                            // Log::info("signup 2710 ");
                            $payload = \GuzzleHttp\json_decode($content);
                            if ($payload->status == "error" && $payload->error == "user_not_found") {
                                return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                            }
                            if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                                return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                            }

                            $explode = explode('.', $payload->data->id_token);
                            $decoded_id_token = base64_decode($explode[1]);
                            $decoded = json_decode($decoded_id_token);
                            // Log::info("signup 2722 ");
                            $member->login_token = $decoded->guid;
                            $member->save();
                            // Log::info("signup 2725 ");
                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                        }
                        // Log::info("signup 2729 ");
                        $user = User::find($user->id); // refresh...
                        // Log::info($user->id);
                        $token = JWTAuth::fromUser($user);
                        //NOT REQUIRED TO RESET PASSWORD
                        $reset_password = 0;
                        // Log::info("signup 2734 ");
                        $first_time = 1;
                        return response()->json(['status' => 'ok', 'reset_password' => $reset_password, 
                            'first_time' => $first_time, 'token' => $token, 'id_token' => $payload->data->id_token, 
                            'access_token' => $payload->data->access_token, 'refresh_token' => $payload->data->refresh_token]);

                    }

                }
            }

        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    function checkBepozGamingAccount($bepoz, $request) {
        $email = preg_replace('/\s+/', '+', $request->input('email'));
        $mobile = $request->input('mobile');
        $checkGamingAccountLowest = false;
        $cansignup = false;

        $gaming_account_lowest_enable = Setting::where('key', 'gaming_account_lowest_enable')->first()->value;
        if ( $gaming_account_lowest_enable == "true" ) {
            $checkbepoz = $bepoz->AccountSearch(8, $email);
            $checkbepozmobile = $bepoz->AccountSearch(6, $mobile);
            $emailBepozCan = false;
            $mobileBepozCan = false;
            $emailSSOCan = false;
            $mobileSSOCan = false;
            
            if ($checkbepoz) {
                // CHECK PURE SIGNUP OR SIGNIN MATCHING ACCOUNT
                $checkGamingAccountLowest = true;
                if ($request->has('AccountID') ){
                    if ( intval($request->input('AccountID')) > 0  ){
                        $checkGamingAccountLowest = false;
                    } else {
                        $checkGamingAccountLowest = true;
                    }
                } else {
                    $checkGamingAccountLowest = true;
                }
            }

            
            if ($checkGamingAccountLowest) {     
                Log::info('AuthenticationController/signup call GamingAccountLowest');
           
                // BEPOZ ID EMPTY, REAL SIGNUP, NEED TO FIND BEPOZ GAMING ACCOUNT

                // NEW LOGIC ALL NEW MEMBER IN CHANGE STARTER GROUP
                $default_tier = Setting::where('key', 'default_tier')->first()->value;

                // NEED TO CHANGE LOGIC WITH LATEST SETTING                           
                if ($request->has('tier_id')) {
                    $tier = Tier::find($request->input('tier_id'));
                    if (is_null($tier)) {
                        // $tier = Tier::first();
                        $tier = Tier::find($default_tier);
                        $member_default_tier = Setting::where('key', 'member_default_tier')->first();
                        if ($member_default_tier->value == 'no_default') {
                            // NO NEED TO DO WILL SELECT DEFAULT TIER
                        } else if ($member_default_tier->value == 'by_venue') {
                            if ($request->has('venue_id')) {
                                if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                                    $venue_id = $request->input('venue_id');
                                    $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                                    foreach ($tierOption as $to) {
                                        if ($to["id"] == $venue_id) {
                                            $tier = Tier::find($to["tier"]);
                                        }
                                    }
                                }
                            }
                        } else if ($member_default_tier->value == 'by_venue_tag') {
                            if ($request->has('state_id')){
                                $venue_tag = $request->input('state_id');
                                $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                                foreach ($tierOption as $to) {
                                    if ($to["id"] == $venue_tag) {
                                        $tier = Tier::find($to["tier"]);
                                    }
                                }
                            }
                        } else if ($member_default_tier->value == 'specific_tier') {
                            // NO NEED TO DO WILL SELECT DEFAULT TIER
                        }
                    }
                } else {
                    // $tier = Tier::first();
                    $tier = Tier::find($default_tier);
                    $member_default_tier = Setting::where('key', 'member_default_tier')->first();
                    if ($member_default_tier->value == 'no_default') {
                        // NO NEED TO DO WILL SELECT DEFAULT TIER
                    } else if ($member_default_tier->value == 'by_venue') {
                        if ($request->has('venue_id')) {
                            if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                                $venue_id = $request->input('venue_id');
                                $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                                foreach ($tierOption as $to) {
                                    if ($to["id"] == $venue_id) {
                                        $tier = Tier::find($to["tier"]);
                                    }
                                }
                            }
                        }
                    } else if ($member_default_tier->value == 'by_venue_tag') {
                        if ($request->has('state_id')){
                            $venue_tag = $request->input('state_id');
                            $tierOption = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);
                            foreach ($tierOption as $to) {
                                if ($to["id"] == $venue_tag) {
                                    $tier = Tier::find($to["tier"]);
                                }
                            }
                        }
                    } else if ($member_default_tier->value == 'specific_tier') {
                        // NO NEED TO DO WILL SELECT DEFAULT TIER
                    }
                }
                
                // use_precreated_account
                // precreated_account_custom_flag
                // precreated_account_custom_flag_name

                $gaming_name = $tier->name;

                if ( $tier->use_precreated_account ) {
                    if ( intval($tier->precreated_account_custom_flag) > 0 ) {
                        $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
                        $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
                        
                        $payload = [];
                        $payload['procedure'] = "GamingAccountLowest";
                        $payload['parameters'] = [$tier->bepoz_group_id, $tier->precreated_account_custom_flag];

                        Log::warning($payload);
                        
                        $post_content = \GuzzleHttp\json_encode($payload);
                        $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                        curl_setopt($ch, CURLOPT_ENCODING, "gzip");

                        $content = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        // Log::warning("GamingAccountLowest");
                        // Log::warning($content);
                        // Log::warning($httpCode);

                        if ($httpCode >= 200 && $httpCode < 300) {
                            // pass
                            if ($content) {
                                $payloadGamingAccountLowest = \GuzzleHttp\json_decode($content);
                                if (!$payloadGamingAccountLowest->error) {
                                    if (intval($payloadGamingAccountLowest->records) == 0){
                                        $formatted_response = [
                                            'status' => 'error',
                                            'cansignup' => $cansignup,
                                            'checkGamingAccountLowest' => $checkGamingAccountLowest,
                                            'message' => config('bucket.GAMING_ACCOUNT_PROBLEM')
                                        ];
                        
                                        return $formatted_response;
                                    }
                                }
                            } else {
                                $formatted_response = [
                                    'status' => 'error',
                                    'cansignup' => $cansignup,
                                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                                    'message' => config('bucket.UNABLE_CONNECT_BEPOZ')
                                ];
                
                                return $formatted_response;
                            }
                        } else {
                            $formatted_response = [
                                'status' => 'error',
                                'cansignup' => $cansignup,
                                'checkGamingAccountLowest' => $checkGamingAccountLowest,
                                'message' => config('bucket.UNABLE_CONNECT_BEPOZ')
                            ];
            
                            return $formatted_response;
                        }
                    } else {
                        $formatted_response = [
                            'status' => 'error',
                            'cansignup' => $cansignup,
                            'checkGamingAccountLowest' => $checkGamingAccountLowest,
                            'message' => config('bucket.MISSING_CONFIGURATION')
                        ];
        
                        return $formatted_response;
                    }
                } else {
                    $formatted_response = [
                        'status' => 'error',
                        'cansignup' => $cansignup,
                        'checkGamingAccountLowest' => $checkGamingAccountLowest,
                        'message' => config('bucket.MISSING_CONFIGURATION')
                    ];
    
                    return $formatted_response;
                }

            }

            if (intval($checkbepoz['IDList']['Count']) > 1) {
                // it means more than 1 email duplicated
                $formatted_response = [
                    'status' => 'error',
                    'cansignup' => $cansignup,
                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                    'message' => config('bucket.DUPLICATED_EMAIL')
                ];

                return $formatted_response;
            } else if (intval($checkbepoz['IDList']['Count']) == 1) {                    
                // get details from email search AccountID
                $result = $bepoz->AccountFullGet($checkbepoz['IDList']['ID']);

                if ( $request->has('AccountID') ){
                    // FIRST TIME SIGNIN
                    // NEED TO CHECK AccountID & AccNumber match
                    if (
                        intval($result['AccountFull']['AccountID']) == intval($request->input('AccountID')) &&
                        intval($result['AccountFull']['AccNumber']) == intval($request->input('AccNumber'))
                    ) {
                        $emailBepozCan = true;
                    } else {
                        $formatted_response = [
                            'status' => 'error',
                            'cansignup' => $cansignup,
                            'checkGamingAccountLowest' => $checkGamingAccountLowest,
                            'message' => config('bucket.DUPLICATED_EMAIL')
                        ];
        
                        return $formatted_response;
                    }

                } else {
                    $formatted_response = [
                        'status' => 'error',
                        'cansignup' => $cansignup,
                        'checkGamingAccountLowest' => $checkGamingAccountLowest,
                        'message' => config('bucket.DUPLICATED_EMAIL')
                    ];
    
                    return $formatted_response;
                }

            } else if (intval($checkbepoz['IDList']['Count']) == 0) {
                $emailBepozCan = true;
            }

            if ($checkbepozmobile) {
                if (intval($checkbepozmobile['IDList']['Count']) > 1) {
                    // it means more than 1 email duplicated
                    $formatted_response = [
                        'status' => 'error',
                        'cansignup' => $cansignup,
                        'checkGamingAccountLowest' => $checkGamingAccountLowest,
                        'message' => config('bucket.DUPLICATED_MOBILE')
                    ];

                    return $formatted_response;
                } else if (intval($checkbepozmobile['IDList']['Count']) == 1) {                    
                    // get details from email search AccountID
                    $result = $bepoz->AccountFullGet($checkbepozmobile['IDList']['ID']);

                    if ( $request->has('AccountID') ){
                        // NEED TO CHECK AccountID & AccNumber match
                        if (
                            intval($result['AccountFull']['AccountID']) == intval($request->input('AccountID')) &&
                            intval($result['AccountFull']['AccNumber']) == intval($request->input('AccNumber'))
                        ) {
                            $mobileBepozCan = true;
                            
                        } else {
                            $formatted_response = [
                                'status' => 'error',
                                'cansignup' => $cansignup,
                                'checkGamingAccountLowest' => $checkGamingAccountLowest,
                                'message' => config('bucket.DUPLICATED_MOBILE')
                            ];
        
                            return $formatted_response;
                        }
                    } else {
                        $formatted_response = [
                            'status' => 'error',
                            'cansignup' => $cansignup,
                            'checkGamingAccountLowest' => $checkGamingAccountLowest,
                            'message' => config('bucket.DUPLICATED_MOBILE')
                        ];
    
                        return $formatted_response;
                    }
                } else if (intval($checkbepozmobile['IDList']['Count']) == 0) {
                    $mobileBepozCan = true;
                }
            }

            if ($this->checkSSOAccount($email) == "ok") {
                $emailSSOCan = false;
                $formatted_response = [
                    'status' => 'error',
                    'cansignup' => $cansignup,
                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                    'message' => config('bucket.DUPLICATED_EMAIL')
                ];

                return $formatted_response;
            } else if ($this->checkSSOAccount($email) == "Unable to connect SSO") {
                $emailSSOCan = false;
                $formatted_response = [
                    'status' => 'error',
                    'cansignup' => $cansignup,
                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                    'message' => config('bucket.UNABLE_CONNECT_SSO')
                ];

                return $formatted_response;
            } else {
                $emailSSOCan = true;
            }
            
            if ($this->checkSSOAccountByMobile($mobile) == "ok") {
                $mobileSSOCan = false;
                $formatted_response = [
                    'status' => 'error',
                    'cansignup' => $cansignup,
                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                    'message' => config('bucket.DUPLICATED_MOBILE')
                ];

                return $formatted_response;
            } else if ($this->checkSSOAccountByMobile($mobile) == "Unable to connect SSO") {
                $mobileSSOCan = false;
                $formatted_response = [
                    'status' => 'error',
                    'cansignup' => $cansignup,
                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                    'message' => config('bucket.UNABLE_CONNECT_SSO')
                ];

                return $formatted_response;
            } else {
                $mobileSSOCan = true;
            }

            $cansignup = $emailBepozCan && $mobileBepozCan && $emailSSOCan && $mobileSSOCan;

            if ($cansignup) {
                $formatted_response = [
                    'status' => 'ok',
                    'cansignup' => $cansignup,
                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                    'payload' => $payloadGamingAccountLowest,
                    'message' => 'can signup'
                ];

            } else {
                $formatted_response = [
                    'status' => 'ok',
                    'cansignup' => $cansignup,
                    'checkGamingAccountLowest' => $checkGamingAccountLowest,
                    'payload' => $payloadGamingAccountLowest,
                    'message' => 'can not signup'
                ];
            }

        } else {
            $cansignup = true;
            
            $formatted_response = [
                'status' => 'ok',
                'cansignup' => $cansignup,
                'checkGamingAccountLowest' => $checkGamingAccountLowest,
                'payload' => null,
                'message' => 'can signup bypass'
            ];
        }

        return $formatted_response;
    }

    function updateBepozGamingAccount($bepoz, $member, $standardfieldpayload, $customfieldpayload, $request) {
        // UPDATE bepoz user data directly
        $groupID = $member->tiers()->first()->bepoz_group_id;
        $template = $member->tiers()->first()->bepoz_template_id;

        // get Account Bepoz
        $result = $bepoz->AccountFullGet($member->bepoz_account_id);
        // Log::warning($result);
        if ($result) {
            $requestAccountFull = $result['AccountFull'];
            $requestAccountFull['AccNumber'] = $member->bepoz_account_number;
            $requestAccountFull['CardNumber'] = $member->bepoz_account_card_number;
            $requestAccountFull['GroupID'] = $groupID;
            //$requestAccountFull['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
            // $requestAccountFull['FirstName'] = str_replace('"', '', $member->first_name);
            // $requestAccountFull['LastName'] = str_replace('"', '', $member->last_name);
            // $requestAccountFull['DateBirth'] = Carbon::createFromFormat('d/m/Y', $request->input('dob'))->format('Y-m-d');
            // $requestAccountFull['Mobile'] = $member->user->mobile;
            $requestAccountFull['PhoneHome'] = $member->phone;
            // $requestAccountFull['Email1st'] = $member->user->email;
            $requestAccountFull['Status'] = 0;

            foreach ($standardfieldpayload as $sf) {
                $bepozFieldName = $bepoz->bepozAccField($sf['bepozFieldNum']);
                
                if ($bepozFieldName == "") {
                    // NO NEED
                } else if ($bepozFieldName == "Tier") {
                    // NO NEED
                } else if ($bepozFieldName == "Password") {
                    // NO NEED
                } else if ($bepozFieldName == "DateBirth") {
                    $requestAccountFull[$bepozFieldName] = Carbon::createFromFormat('d/m/Y', $sf['value'])->format('Y-m-d');
                } else {
                    $requestAccountFull[$bepozFieldName] = $sf['value'];
                }
            }

            // $address = Address::where('member_id', $member->id)->first();
            // if (!is_null($address)) {
            //     $payload = \GuzzleHttp\json_decode($address->payload);
            //     $requestAccountFull['Street1'] = isset($payload->display->unit) ? $payload->display->unit : "";
            //     $requestAccountFull['Street2'] = isset($payload->display->street) ? $payload->display->street : "";
            //     //$requestAccountFull['Street3'] = $payload->display->street;
            //     $requestAccountFull['State'] = isset($payload->display->state) ? $payload->display->state : "";
            //     $requestAccountFull['City'] = isset($payload->display->suburb) ? $payload->display->suburb : "";
            //     $requestAccountFull['PCode'] = isset($payload->display->postcode) ? $payload->display->postcode : "";
            // }

            if (!is_null($member->member_tier->date_expiry)) {
                $date_expiry = Carbon::parse($member->member_tier->date_expiry);
                $date_expiry->setTimezone(config('app.timezone'));
                $requestAccountFull['DateTimeExpiry'] = $date_expiry->format('Y-m-d\TH:i:s');
            }

            unset($requestAccountFull['AddressID']);
            unset($requestAccountFull['CommentID']);

            // Log::warning(print_r($requestAccountFull, true));
            $result = $bepoz->UpdateAccount($requestAccountFull, $template);
        }

        // UPDATE bepozcustomfield directly
        $settingbepozcustomfield = Setting::where('key', 'bepozcustomfield')->first();
        $customFields = array();
        $allCustomField = \GuzzleHttp\json_decode($settingbepozcustomfield->extended_value);

        foreach ($customfieldpayload as $cf) {
            $typeCode = 0;
            // switch ($cf["fieldType"]) {
            //     case "Text":
            //         $typeCode = 3;
            //         break;
            //     case "Number":
            //         $typeCode = 2;
            //         break;
            //     case "Date":
            //         $typeCode = 1;
            //         break;
            //     case "Dropdown":
            //         $typeCode = 3;
            //         break;
            //     case "Checkbox":
            //         $typeCode = 0;
            //         break;
            //     case "Toggle":
            //         $typeCode = 0;
            //         break;
            // }

            // Log::warning("bepozFieldNum Full");
            // Log::warning($cf["bepozFieldNum"]);
            
            $bepozFieldNum = $cf["bepozFieldNum"];
            if (strlen(trim($cf["bepozFieldNum"])) == 3) {
                $bepozFieldNum = substr($cf["bepozFieldNum"], -2);
                $typeCode = substr($cf["bepozFieldNum"], 0, 1);
            }

            // Log::warning("bepozFieldNum");
            // Log::warning($bepozFieldNum);
            // Log::warning("typeCode");
            // Log::warning($typeCode);

            // echo print_r($cf, true)."<br><br>";
            $customFields['CustomField'][] = array(
                'FieldName' => $cf["fieldName"],
                'FieldType' => $typeCode,
                'FieldValue' => $cf["value"],
                'FieldNum' => $bepozFieldNum
            );
        }
        
        // NEED UPDATE CUSTOM FLAG FOR CREATED ACCOUNT        
        $flag = $member->tiers()->first()->precreated_account_custom_flag;
        $flagName = $member->tiers()->first()->precreated_account_custom_flag_name;
        $typeCode = 0;
        $customFields['CustomField'][] = array(
            'FieldName' => $flagName,
            'FieldType' => $typeCode,
            'FieldValue' => 0,
            'FieldNum' => $flag
        );

        $EntityType = 0;
        $EntityID = $member->bepoz_account_id;

        $customFieldResult = $bepoz->CustomFieldsSet($EntityType, $EntityID, $customFields);

    }

    /**
     * Register a new user and grant him a member role
     * >> Generate a JWT token
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function signupMatch(Request $request, ImageOptimizer $imageOptimizer, MandrillExpress $mx, MailjetExpress $mjx, Bepoz $bepoz)
    {

        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|max:155',
                'password' => 'required|confirmed|min:4',
                'password_confirmation' => 'required|min:4',
                'first_name' => 'required',
                'last_name' => 'required',
                'share_info' => 'required'
                //'dob' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $share_info = $request->input('share_info') == 'true' ? 1 : 0;
            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $password = $request->input('password');
            $first_name = $request->input('first_name');
            if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }

            $last_name = $request->input('last_name');
            if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }
            $mobile = $request->input('mobile');
            $credentials = array(
                'email' => $email,
                'password' => Hash::make($password),
                'mobile' => $mobile
            );

            if ($request->has('mobile')) {
                $credentials['mobile'] = $request->input('mobile');
            }

            $user_obj = User::where('email', $email)->first();

            // $guid = new Guid;
            // $login_token = $guid->create();

            $bepozSearch = $this->bepozSearch($email);

            $default_tier = Setting::where('key', 'default_tier')->first()->value;
            
            // FEATURE FROM JDA
            $signup_matching_bepoz_group_venue_tier = Setting::where('key', 'signup_matching_bepoz_group_venue_tier')->first()->value;
            
            if ( $signup_matching_bepoz_group_venue_tier == "true" ) {
                $venueID = 0;
                if ($bepozSearch) {
                    // Log::info($bepozSearch);
                    $tier = Tier::where("bepoz_group_id", $bepozSearch['AccountFull']['GroupID'])->first();
                    if (!is_null($tier)) {
                        if ($tier->venue_id != '' && intval($tier->venue_id) > 0) {
                            $venueID = $tier->venue_id;
                        }
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. No Tier that associated with Bepoz GroupID: ' . $bepozSearch['AccountFull']['GroupID']], Response::HTTP_BAD_REQUEST);
                    }
                } else {

                    if ($request->has('venue_id')) {
                        if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                            $venueID = $request->input('venue_id');
                        } else {
                            
                            $tier = Tier::find($default_tier);
                            if (!is_null($tier)) {
                                if ($tier->venue_id != '' && intval($tier->venue_id) > 0) {
                                    $venueID = $tier->venue_id;
                                } else {
                                    return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. No Venue ID specified.'], Response::HTTP_BAD_REQUEST);
                                }
                            } else {
                                return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. Default Tier Object doesnt exist'], Response::HTTP_BAD_REQUEST);
                            }
                        }
                    } else {
                        if ($request->has('tier_id') && !empty($request->input('tier_id'))) {
                            $tier = Tier::find($request->input('tier_id'));
                            if (!is_null($tier)) {
                                if ($tier->venue_id != '' && intval($tier->venue_id) > 0) {
                                    $venueID = $tier->venue_id;
                                } else {
                                    return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. VenueID not set on Specified Tier'], Response::HTTP_BAD_REQUEST);
                                }
                            } else {
                                return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. Default Tier Object doesnt exist'], Response::HTTP_BAD_REQUEST);
                            }
                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found. No Tier ID specified'], Response::HTTP_BAD_REQUEST);
                        }
                    }
                }

                if (intval($venueID) > 0) {
                    $venue = Venue::find($venueID);

                    if ($venue->allow_signup_for_existing_customer_only) {
                        // $bepozSearch = $this->bepozSearch($email);
                        if ($bepozSearch) {
                            // conttinue signup (will matching with the cron)
                            // Log::info("bepozSearch");
                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Please contact our staff'], Response::HTTP_BAD_REQUEST);
                        }
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Tier or Venue not found'], Response::HTTP_BAD_REQUEST);
                }
                // Log::info("signup 2386 signup_matching_bepoz_group_venue_tier");
            }

            $cansignup = false;

            $gaming_account_lowest_enable = Setting::where('key', 'gaming_account_lowest_enable')->first()->value;
            if ( $gaming_account_lowest_enable == "true" ) {
                $checkbepoz = $bepoz->AccountSearch(8, $email);
                $checkbepozmobile = $bepoz->AccountSearch(6, $mobile);
                $emailBepozCan = false;
                $mobileBepozCan = false;
                $emailSSOCan = false;
                $mobileSSOCan = false;
                
                if ($checkbepoz) {
                    // CHECK PURE SIGNUP OR SIGNIN MATCHING ACCOUNT
                    $checkGamingAccountLowest = true;
                    if ($request->has('AccountID') ){
                        if ( intval($request->input('AccountID')) > 0  ){
                            $checkGamingAccountLowest = false;
                        } else {
                            $checkGamingAccountLowest = true;
                        }
                    } else {
                        $checkGamingAccountLowest = true;
                    }
                }

                
                if ($checkGamingAccountLowest) {     
                    // Log::info('AuthenticationController/signup call GamingAccountLowest');
               
                    // BEPOZ ID EMPTY, REAL SIGNUP, NEED TO FIND BEPOZ GAMING ACCOUNT

                    // NEW LOGIC ALL NEW MEMBER IN CHANGE STARTER GROUP
                    $default_tier = Setting::where('key', 'default_tier')->first()->value;

                    $tier = Tier::find($default_tier);

                    $gaming_name = $tier->name;

                    $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
                    $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
                    
                    $payload = [];
                    $payload['procedure'] = "GamingAccountLowest";
                    $payload['parameters'] = [$tier->bepoz_group_id, $tier->bepoz_template_id];

                    $post_content = \GuzzleHttp\json_encode($payload);
                    $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_ENCODING, "gzip");

                    $content = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);

                    // Log::warning("GamingAccountLowest");
                    // Log::warning($content);
                    // Log::warning($httpCode);

                    if ($httpCode >= 200 && $httpCode < 300) {
                        // pass
                        if ($content) {
                            $payloadGamingAccountLowest = \GuzzleHttp\json_decode($content);
                            if (!$payloadGamingAccountLowest->error) {
                                if (intval($payloadGamingAccountLowest->records) == 0){
                                    return response()->json(['status' => 'error', 'message' => config('bucket.GAMING_ACCOUNT_PROBLEM')], Response::HTTP_BAD_REQUEST);
                                }
                            }
                        } else {
                            return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_BEPOZ')], Response::HTTP_BAD_REQUEST);
                        }
                    } else {
                        return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_BEPOZ')], Response::HTTP_BAD_REQUEST);
                    }

                }

                if (intval($checkbepoz['IDList']['Count']) > 1) {
                    // it means more than 1 email duplicated
                    return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
                } else if (intval($checkbepoz['IDList']['Count']) == 1) {                    
                    // get details from email search AccountID
                    $result = $bepoz->AccountFullGet($checkbepoz['IDList']['ID']);

                    if ( $request->has('AccountID') ){
                        // FIRST TIME SIGNIN
                        // NEED TO CHECK AccountID & AccNumber match
                        if (
                            intval($result['AccountFull']['AccountID']) == intval($request->input('AccountID')) &&
                            intval($result['AccountFull']['AccNumber']) == intval($request->input('AccNumber'))
                        ) {
                            $emailBepozCan = true;
                        } else {
                            return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL') ], Response::HTTP_BAD_REQUEST);
                        }

                    } else {
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL') ], Response::HTTP_BAD_REQUEST);
                    }

                } else if (intval($checkbepoz['IDList']['Count']) == 0) {
                    $emailBepozCan = true;
                }

                if ($checkbepozmobile) {
                    if (intval($checkbepozmobile['IDList']['Count']) > 1) {
                        // it means more than 1 email duplicated
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE')], Response::HTTP_BAD_REQUEST);
                    } else if (intval($checkbepozmobile['IDList']['Count']) == 1) {                    
                        // get details from email search AccountID
                        $result = $bepoz->AccountFullGet($checkbepozmobile['IDList']['ID']);
    
                        if ( $request->has('AccountID') ){
                            // NEED TO CHECK AccountID & AccNumber match
                            if (
                                intval($result['AccountFull']['AccountID']) == intval($request->input('AccountID')) &&
                                intval($result['AccountFull']['AccNumber']) == intval($request->input('AccNumber'))
                            ) {
                                $mobileBepozCan = true;
                            } else {
                                return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE') ], Response::HTTP_BAD_REQUEST);
                            }
                        } else {
                            return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE') ], Response::HTTP_BAD_REQUEST);
                        }
                    } else if (intval($checkbepozmobile['IDList']['Count']) == 0) {
                        $mobileBepozCan = true;
                    }
                }

                if ($this->checkSSOAccount($email) == "ok") {
                    $emailSSOCan = false;
                    return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL') ], Response::HTTP_BAD_REQUEST);
                } else if ($this->checkSSOAccount($email) == "Unable to connect SSO") {
                    $emailSSOCan = false;
                    return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_SSO') ], Response::HTTP_BAD_REQUEST);
                } else {
                    $emailSSOCan = true;
                }
                
                if ($this->checkSSOAccountByMobile($mobile) == "ok") {
                    $mobileSSOCan = false;
                    return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE') ], Response::HTTP_BAD_REQUEST);
                } else if ($this->checkSSOAccountByMobile($mobile) == "Unable to connect SSO") {
                    $mobileSSOCan = false;
                    return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_SSO') ], Response::HTTP_BAD_REQUEST);
                } else {
                    $mobileSSOCan = true;
                }

                $cansignup = $emailBepozCan && $mobileBepozCan && $emailSSOCan && $mobileSSOCan;
            } else {
                $cansignup = true;
            }

            if ($cansignup) {
                DB::beginTransaction();

                if (!is_null($user_obj)) {

                    if ($user_obj->member->status == 'active') {
                        return response()->json(['status' => 'error', 'message' => 'signup failed. Your email has been used.'], Response::HTTP_BAD_REQUEST);
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
                    }

                } else {

                    // $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;

                    // if ( $gaming_system_enable == "true" ) {

                    //     $igt = new IGT();
                    //     // USE THIS NO NEED HOLD MEMBER
                    //     $membershipId = 1;
                    //     $resultGetNextAvailableMemberNumber = $igt->GetNextAvailableMemberNumber($membershipId);

                    //     if ($resultGetNextAvailableMemberNumber){
                    //         $memberNumber = $resultGetNextAvailableMemberNumber->number;
                    //     } else {
                    //         return response()->json(['status' => 'error', 'message' => config('bucket.GAMING_SYSTEM_PROBLEM') ], Response::HTTP_BAD_REQUEST);
                    //     }

                    //     // CREATE MEMBER
                    //     $siteId = 1;
                    //     $membershipId = 1;
                    //     $termId = 1;
                    //     $preferredName = $first_name;
                    //     $address = "71 Boundary Road, North Melb, VIC";
                    //     $zipCode = "3051";
                    //     $addressCountryId = "36";
                    //     $addressStateId = "36";
                    //     $phoneNumber = $mobile;

                    //     $resultCreateMember = $igt->CreateMember($siteId, $membershipId, $termId, $memberNumber, $first_name, $last_name,
                    //         $preferredName, $address, $zipCode, $addressCountryId, $addressStateId, $phoneNumber, $email);

                    //     if ($resultCreateMember){
                    //         $bepoz_account_number = $memberNumber;
                    //     } else {
                    //         return response()->json(['status' => 'error', 'message' => config('bucket.GAMING_SYSTEM_PROBLEM') ], Response::HTTP_BAD_REQUEST);
                    //     }
                    // }

                    // CHECK IF SSO ACCOUNT ALREADY EXIST
                    $checkSSOAccount = $this->checkSSOAccount($email);

                    if ($checkSSOAccount['status'] == 'ok') {
                        $emailSSOCan = false;
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
                    } else {
                        if ($checkSSOAccount['error'] == 'SSO problem') {
                            return response()->json(['status' => 'error', 'message' => 'Please contact administrator'], Response::HTTP_BAD_REQUEST);
                        }

                        $emailSSOCan = true;
                    }

                    $cansignup = $emailSSOCan;

                    $sso_response = $this->loginSSOUSer($email, $request->input('password'), $request);
                    // Log::info($sso_response);
                    // Log::info($sso_response['status']);

                    if ($sso_response['status'] == 'ok') {
                        // successful sso login
                        $token = JWTAuth::attempt($credentials);
                        $first_time = 1;
                        return response()->json(['status' => 'ok', 'token' => $token,
                            'first_time' => $first_time,
                            'id_token' => $sso_response['data']->data->id_token,
                            'access_token' => $sso_response['data']->data->access_token,
                            'refresh_token' => $sso_response['data']->data->refresh_token]);

                    } else {
                        // failed SSo login
                        $user = User::create($credentials);

                        // Member::create([
                        //     'user_id' => $user->id,
                        //     'first_name' => $first_name,
                        //     'last_name' => $last_name,
                        //     'bepoz_account_card_number' => $request->has("card_number") ? $request->input("card_number") : null,
                        //     'dob' => $request->has('dob') ? new Carbon($request->input('dob'), config('app.timezone')) : null,
                        //     'login_token' => $login_token
                        // ]);

                        $member = new Member;
                        $member->user_id = $user->id;
                        $member->first_name = $first_name;
                        $member->last_name = $last_name;
                        $member->share_info = $share_info;
                        $member->current_preferred_venue_name = "";
                        $member->original_preferred_venue_name = "";

                        if ($request->has('AccountID')) {
                            if (!empty($request->input('AccountID'))) {
                                $member->bepoz_account_id = $request->input('AccountID');
                            }
                        }

                        if ($request->has('CardNumber')) {
                            if (!empty($request->input('CardNumber'))) {
                                $member->bepoz_account_card_number = $request->input('CardNumber');
                            }
                        }

                        if ($request->has('AccNumber')) {
                            if (!empty($request->input('AccNumber'))) {
                                $member->bepoz_account_number = $request->input('AccNumber');
                            }
                        }

                        // if ( $gaming_system_enable == "true" ) {
                        //     $member->bepoz_account_number = $bepoz_account_number;
                        // }

                        if ($request->has('dob')) {
                            if ($request->input('dob') != '') {
                                $member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                            } else {
                                $member->dob = null;
                            }
                        } else {
                            $member->dob = null;
                        }

                        $member->postcode = $request->has('postcode') ? $request->input('postcode') : null;
                        $member->save();

                        $confirmationToken = $user->generateEmailConfirmationToken();

                        $user_obj = User::find($user->id); // refresh...
                        $user_obj->member->save();

                        $user_role = new UserRoles;
                        $user_role->role_id = 3; // Manually add
                        $user_role->user_id = $user->id;
                        $user_role->save();

                        if ( $signup_matching_bepoz_group_venue_tier == "true" ) {
                            // FEATURE FROM JDA
                            if ($bepozSearch) {
                                // conttinue signup (will matching with the cron)
                                $tier = Tier::where("bepoz_group_id", $bepozSearch['AccountFull']['GroupID'])->first();
                            } else {
                                if ($request->has('tier_id')) {
                                    $tier = Tier::find($request->input('tier_id'));
                                    if (is_null($tier)) {
                                        // $tier = Tier::first();
                                        $tier = Tier::find($default_tier);
                                    }
                                } else {
                                    // $tier = Tier::first();
                                    $tier = Tier::find($default_tier);
                                }
                            }
                        } else {
                            $default_tier = Setting::where('key', 'default_tier')->first()->value;

                            if ($request->has('tier_id')) {
                                $tier = Tier::find($request->input('tier_id'));
                                if (is_null($tier)) {
                                    // $tier = Tier::first();
                                    $tier = Tier::find($default_tier);
                                }
                            } else {
                                // $tier = Tier::first();
                                $tier = Tier::find($default_tier);
                            }
                        }

                        // QUEENSBERRY FEATURE
                        // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                    
                        // if ($default_member_expiry_date == "true"){
                        //     $setting = Setting::where('key', 'cut_off_date')->first();
                        //     $date_expiry = Carbon::parse($setting->value);
                        //     $date_expiry->setTimezone(config('app.timezone'));
                        //     $date_expiry->setTime(0, 0, 0);
                        // } else {
                        //     $date_expiry = null;
                        // }

                        $date_expiry = null;
                        $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                        if ($request->has('venue_id')) {
                            if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                                $venue = Venue::find($request->input('venue_id'));
                                $member->current_preferred_venue = is_null($venue) ? '' : $venue->id;
                                $member->current_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                                $member->original_preferred_venue = is_null($venue) ? '' : $venue->id;
                                $member->original_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                            }
                        }

                        $member->share_info = 1;
                        if ($request->has('opt_out_marketing')) {
                            $opt_out_marketing = $request->input('opt_out_marketing') == 'true' ? 1 : 0;
                            $member->opt_out_marketing = $opt_out_marketing;
                        }
                        
                        // FEATURE FROM REDCAPE PUBLINC
                        if ($request->has('salutation')){
                            $member->salutation = $request->input('salutation');
                        }
                        if ($request->has('state_id')){
                            $member->state_id = $request->input('state_id');
                        }
                        if ($request->has('udid')){
                            $member->udid = $request->input('udid');
                        }
                        $member->save();

                        $user = User::find($user_obj->id); // refresh...

                        if (!is_null($user)) {
                            $user->member->member_tier->tier;
                            $ml = new MemberLog();
                            $ml->member_id = $user->member->id;
                            $ml->message = "This member sign-up and register SSO";
                            $ml->after = \GuzzleHttp\json_encode($user);
                            $ml->save();
                        }

                        if ($request->hasFile('photo')) {
                            if (config('bucket.s3')) {
                                $picture = $request->file('photo');
                                $mime = explode('/', $picture->getMimeType());
                                $type = end($mime);
                                $imageOptimizer->optimizeUploadedImageFile($picture);
                                Storage::disk('s3')->put('member/' . $user->member->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');
                                $user->member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $user->member->id . "/profile." . $type . "?" . time();
                                $user->member->save();

                            } else {
                                $log = new SystemLog();
                                $log->type = 'setting-error';
                                $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                                $log->payload = 'AuthenticationController.php/signup';
                                $log->message = 'Setting of S3 bucket not found.';
                                $log->source = 'AuthenticationController.php';
                                $log->save();

                                return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                            }

                        }

                        if (!$user->email_confirmation){
                            $this->sendEmailConfirmation($user, $mx, $mjx);
                        }

                        $job = (new SendReminderNotification($user))->delay(60);
                        dispatch($job);

                        // NEW FEATURE MOBILE CONFIRMATION FROM REDCAPE PUBLINC
                        $mobile_confirmation_enable = Setting::where('key', 'mobile_confirmation_enable')->first()->value;
                        if ($mobile_confirmation_enable == "true"){
                            if (!$user->sms_confirmation){
                                $this->sendMobileConfirmation($user);
                            }
                        }
                        
                        //change logic reward given when confirm
                        $this->getReward($request, $user);

                        DB::commit();

                        //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                        $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/register";
                        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                        // $encoded = json_encode($data);
                        // $checksum = md5($encoded);

                        $datasend = array(
                            'email' => $email,
                            'password' => $request->input('password'),
                            'name' => $first_name . " " . $last_name,
                            'given_name' => $first_name,
                            'family_name' => $last_name,
                            'phone_number' => $mobile,
                            "license_key" => $your_order_key
                        );

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_URL, $your_order_api);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                        $content = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        // Log::warning($content);
                        // Log::warning($httpCode);

                        if ($content) {
                            $payload = \GuzzleHttp\json_decode($content);
                            if ($payload->status == "error" && $payload->error == "user_not_found") {
                                return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                            }
                            if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                                return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                            }

                            $explode = explode('.', $payload->data->id_token);
                            $decoded_id_token = base64_decode($explode[1]);
                            $decoded = json_decode($decoded_id_token);

                            $member->login_token = $decoded->guid;
                            $member->save();

                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                        }

                        $user = User::find($user->id); // refresh...
                        $token = JWTAuth::fromUser($user);
                        //NOT REQUIRED TO RESET PASSWORD
                        $reset_password = 0;
                        $first_time = 1;
                        return response()->json(['status' => 'ok', 'reset_password' => $reset_password, 
                            'first_time' => $first_time, 'token' => $token, 'id_token' => $payload->data->id_token, 
                            'access_token' => $payload->data->access_token, 'refresh_token' => $payload->data->refresh_token]);

                    }

                }
            }

        } catch (\Exception $e) {
            Log::error("signupMatch 4640");
            DB::rollback();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Register a new user and grant him a member role IGT Mode
     * >> Generate a JWT token
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function signupIGT(Request $request, ImageOptimizer $imageOptimizer, MandrillExpress $mx, MailjetExpress $mjx, Bepoz $bepoz)
    {

        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|max:155',
                'password' => 'required|confirmed|min:4',
                'password_confirmation' => 'required|min:4',
                'first_name' => 'required',
                'last_name' => 'required',
                'share_info' => 'required'
                //'dob' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // LOAD GAMING SETTING
            $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;
            $memberGaming = [];
            // TRY SIGNUP TO GAMING SYSTEM FIRST
            if ($gaming_system_enable == "true") {
                $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();
                $allGamingField = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value);

                // VALIDATE BASED ON IGT MANDATORY FIELD
                foreach ($allGamingField as $data) {
                    // CHECK IF THIS GAMING FIELD SHOW IN APP
                    if ($data->displayInApp) {
                        if ($request->has($data->key)) {
                            if (!is_null($request->input($data->key))) {

                                // Log::info( $request->input($data->key) );
                                // $$data->key = $request->input($data->key);
                                $memberGaming[$data->key] = $request->input($data->key);


                            } else {

                            }
                        } else {
                            return response()->json(['status' => 'error', 'message' => $data->key . " is required"], Response::HTTP_BAD_REQUEST);
                        }
                    }
                }
            }

            // CHECK BEPOZ CUSTOM FIELD REQUIRED
            $bepozcustomfield_enable = Setting::where('key', 'bepozcustomfield_enable')->first()->value;
            $customfieldpayload = [];
            if ($bepozcustomfield_enable === 'true') {
                // VALIDATE BEPOZ CUSTOM FIELD BASED ON SETTING
                $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first()->extended_value;
                // echo $bepozcustomfield."<br<";
                $bepozcustomfielddecode = \GuzzleHttp\json_decode($bepozcustomfield);
                // echo print_r($data, true)."<br>";

                if (sizeof($bepozcustomfielddecode)) {

                    foreach ($bepozcustomfielddecode as $cf) {
                        // Log::info($cf->fieldName);

                        if ($cf->displayInApp) {
                            if (!$request->has($cf->id)) {
                                return response()->json(['status' => 'error', 'message' => $cf->fieldName . ' is required.'], Response::HTTP_BAD_REQUEST);
                            }
                        }

                        $customfieldpayload[] = array(
                            $cf->id => $request->input($cf->id)
                        );
                    }
                    // Log::info($customfieldpayload);

                }

            }

            $share_info = $request->input('share_info') == 'true' ? 1 : 0;
            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $password = $request->input('password');
            $first_name = $request->input('first_name');
            if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }

            $last_name = $request->input('last_name');
            if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }
            $mobile = $request->input('mobile');

            $checkbepoz = $bepoz->AccountSearch(8, $email);
            $cansignup = false;

            if (intval($checkbepoz['IDList']['Count']) > 1) {
                // it means more than 1 email duplicated
                return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
            } else if (intval($checkbepoz['IDList']['Count']) == 1) {
                // get details from email search AccountID
                $result = $bepoz->AccountFullGet($checkbepoz['IDList']['ID']);

                if ($request->has('AccountID')) {
                    // FIRST TIME SIGNIN
                    // NEED TO CHECK AccountID & AccNumber match
                    if (
                        intval($result['AccountFull']['AccountID']) == intval($request->input('AccountID')) &&
                        intval($result['AccountFull']['AccNumber']) == intval($request->input('AccNumber'))
                    ) {
                        $emailBepozCan = true;
                    } else {
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
                    }

                } else {
                    return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
                }

            } else if (intval($checkbepoz['IDList']['Count']) == 0) {
                $emailBepozCan = true;
            }


            if ($this->checkSSOAccount($email) == "ok") {
                $emailSSOCan = false;
                return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
            } else if ($this->checkSSOAccount($email) == "Unable to connect SSO") {
                $emailSSOCan = false;
                return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_SSO')], Response::HTTP_BAD_REQUEST);
            } else {
                $emailSSOCan = true;
            }

            $cansignup = $emailBepozCan && $emailSSOCan;

            if ($cansignup) {

                // CAN SIGNUP
                DB::beginTransaction();
                $credentials = array(
                    'email' => $email,
                    'password' => Hash::make($password),
                    'mobile' => $mobile
                );

                if ($request->has('mobile')) {
                    $credentials['mobile'] = $request->input('mobile');
                }

                $user_obj = User::where('email', $email)->first();

                $guid = new Guid;
                $login_token = $guid->create();

                if (!is_null($user_obj)) {

                    if ($user_obj->member->status == 'active') {
                        return response()->json(['status' => 'error', 'message' => 'signup failed. Your email has been used.'], Response::HTTP_BAD_REQUEST);
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
                    }
                } else {
                    $user = User::create($credentials);

                    // Member::create([
                    //     'user_id' => $user->id,
                    //     'first_name' => $first_name,
                    //     'last_name' => $last_name,
                    //     'bepoz_account_card_number' => $request->has("card_number") ? $request->input("card_number") : null,
                    //     'dob' => $request->has('dob') ? new Carbon($request->input('dob'), config('app.timezone')) : null,
                    //     'login_token' => $login_token
                    // ]);

                    $member = new Member;
                    $member->user_id = $user->id;
                    $member->first_name = $first_name;
                    $member->last_name = $last_name;
                    $member->share_info = $share_info;
                    $member->current_preferred_venue_name = "";
                    $member->original_preferred_venue_name = "";

                    if ($request->has('dob')) {
                        if ($request->input('dob') != '') {
                            $member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                        } else {
                            $member->dob = null;
                        }
                    } else {
                        $member->dob = null;
                    }

                    $member->postcode = $request->has('postcode') ? $request->input('postcode') : null;

                    $payloadMerge = array_merge($memberGaming, $customfieldpayload);
                    $member->payload = \GuzzleHttp\json_encode($payloadMerge);
                    $member->save();

                    $confirmationToken = $user->generateEmailConfirmationToken();

                    $user_obj = User::find($user->id); // refresh...
                    $user_obj->member->save();

                    $user_role = new UserRoles;
                    $user_role->role_id = 3; // Manually add
                    $user_role->user_id = $user->id;
                    $user_role->save();

                    $default_tier = Setting::where('key', 'default_tier')->first()->value;

                    if ($request->has('GroupID')) {
                        if ($request->input('GroupID') != "") {
                            $tier = Tier::where('bepoz_group_id', $request->input('GroupID'))->first();
                            if (is_null($tier)) {
                                // $tier = Tier::first();
                                $tier = Tier::find($default_tier);
                            }
                        } else {
                            $tier = Tier::find($default_tier);
                        }
                    } else {
                        $tier = Tier::find($default_tier);
                    }

                    
                    // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                    // if ($default_member_expiry_date == "true"){
                    //     $setting = Setting::where('key', 'cut_off_date')->first();
                    //     $date_expiry = Carbon::parse($setting->value);
                    //     $date_expiry->setTimezone(config('app.timezone'));
                    //     $date_expiry->setTime(0, 0, 0);
                    // } else {
                    //     $date_expiry = null;
                    // }

                    $date_expiry = null;
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                    if ($request->has('venue_id')) {
                        if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                            $venue = Venue::find($request->input('venue_id'));
                            $member->current_preferred_venue = is_null($venue) ? '' : $venue->id;
                            $member->current_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                            $member->original_preferred_venue = is_null($venue) ? '' : $venue->id;
                            $member->original_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                        }
                    }

                    $member->payload = $customfieldpayload;
                    $member->share_info = 1;
                    $member->save();
                }

                $user = User::find($user_obj->id); // refresh...

                if (!is_null($user)) {
                    $user->member->member_tier->tier;
                    $ml = new MemberLog();
                    $ml->member_id = $user->member->id;
                    $ml->message = "This member sign-up and register SSO";
                    $ml->after = \GuzzleHttp\json_encode($user);
                    $ml->save();
                }

                if ($request->hasFile('photo')) {
                    if (config('bucket.s3')) {
                        $picture = $request->file('photo');
                        $mime = explode('/', $picture->getMimeType());
                        $type = end($mime);
                        $imageOptimizer->optimizeUploadedImageFile($picture);
                        Storage::disk('s3')->put('member/' . $user->member->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');
                        $user->member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $user->member->id . "/profile." . $type . "?" . time();
                        $user->member->save();
                    } else {
                        $log = new SystemLog();
                        $log->type = 'setting-error';
                        $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                        $log->payload = 'AuthenticationController.php/signup';
                        $log->message = 'Setting of S3 bucket not found.';
                        $log->source = 'AuthenticationController.php';
                        $log->save();

                        return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                    }
                }

                if (!$user->email_confirmation){
                    $this->sendEmailConfirmation($user, $mx, $mjx);
                }

                $job = (new SendReminderNotification($user))->delay(60);
                dispatch($job);

                // NEW FEATURE MOBILE CONFIRMATION FROM REDCAPE PUBLINC
                $mobile_confirmation_enable = Setting::where('key', 'mobile_confirmation_enable')->first()->value;
                if ($mobile_confirmation_enable == "true"){
                    if (!$user->sms_confirmation){
                        $this->sendMobileConfirmation($user);
                    }
                }
                
                //change logic reward given when confirm
                $this->getReward($request, $user);

                DB::commit();

                //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/register";
                $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                // $encoded = json_encode($data);
                // $checksum = md5($encoded);

                $datasend = array(
                    'email' => $email,
                    'password' => $request->input('password'),
                    'name' => $first_name . " " . $last_name,
                    'given_name' => $first_name,
                    'family_name' => $last_name,
                    'phone_number' => $mobile,
                    "license_key" => $your_order_key
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $your_order_api);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                $content = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                // Log::warning($content);
                // Log::warning($httpCode);

                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);
                    if ($payload->status == "error" && $payload->error == "user_not_found") {
                        return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                    }
                    if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                        return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                    }

                    $explode = explode('.', $payload->data->id_token);
                    $decoded_id_token = base64_decode($explode[1]);
                    $decoded = json_decode($decoded_id_token);

                    $member->login_token = $decoded->guid;
                    $member->save();

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Unable to connect to SSO server'], Response::HTTP_BAD_REQUEST);
                }

                $user = User::find($user->id); // refresh...
                $token = JWTAuth::fromUser($user);
                //NOT REQUIRED TO RESET PASSWORD
                $reset_password = 0;
                $first_time = 1;
                return response()->json([
                    'status' => 'ok', 'reset_password' => $reset_password, 'token' => $token,
                    'first_time' => $first_time,
                    'id_token' => $payload->data->id_token, 'access_token' => $payload->data->access_token,
                    'refresh_token' => $payload->data->refresh_token
                ]);
            }

        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Register a new user and grant him a member role Odyssey Mode
     * >> Generate a JWT token
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function signupOdyssey(Request $request, ImageOptimizer $imageOptimizer, MandrillExpress $mx, MailjetExpress $mjx, Bepoz $bepoz)
    {

        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|max:155',
                'password' => 'required|confirmed|min:4',
                'password_confirmation' => 'required|min:4',
                'first_name' => 'required',
                'last_name' => 'required',
                'share_info' => 'required',
                'dob' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // LOAD GAMING SETTING
            $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;
            $memberGaming = [];
            // TRY SIGNUP TO GAMING SYSTEM FIRST
            if ($gaming_system_enable == "true") {
                $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();
                $allGamingField = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value);

                // VALIDATE BASED ON IGT MANDATORY FIELD
                foreach ($allGamingField as $data) {
                    // CHECK IF THIS GAMING FIELD SHOW IN APP
                    if ($data->displayInApp) {
                        if ($request->has($data->key)) {
                            if (!is_null($request->input($data->key))) {

                                // Log::info( $request->input($data->key) );
                                // $$data->key = $request->input($data->key);
                                $memberGaming[$data->key] = $request->input($data->key);


                            } else {

                            }
                        } else {
                            return response()->json(['status' => 'error', 'message' => $data->key . " is required"], Response::HTTP_BAD_REQUEST);
                        }
                    }
                }
            }

            // CHECK BEPOZ CUSTOM FIELD REQUIRED
            $bepozcustomfield_enable = Setting::where('key', 'bepozcustomfield_enable')->first()->value;
            $customfieldpayload = [];
            if ($bepozcustomfield_enable === 'true') {
                // VALIDATE BEPOZ CUSTOM FIELD BASED ON SETTING
                $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first()->extended_value;
                // echo $bepozcustomfield."<br<";
                $bepozcustomfielddecode = \GuzzleHttp\json_decode($bepozcustomfield);
                // echo print_r($data, true)."<br>";

                if (sizeof($bepozcustomfielddecode)) {

                    foreach ($bepozcustomfielddecode as $cf) {
                        // Log::info($cf->fieldName);

                        if ($cf->displayInApp) {
                            if (!$request->has($cf->id)) {
                                return response()->json(['status' => 'error', 'message' => $cf->fieldName . ' is required.'], Response::HTTP_BAD_REQUEST);
                            }
                        }

                        $customfieldpayload[] = array(
                            $cf->id => $request->input($cf->id)
                        );
                    }
                    // Log::info($customfieldpayload);

                }

            }

            $share_info = $request->input('share_info') == 'true' ? 1 : 0;
            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $password = $request->input('password');
            $first_name = $request->input('first_name');
            if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }

            $last_name = $request->input('last_name');
            if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
            }
            $mobile = $request->input('mobile');

            $checkbepoz = $bepoz->AccountSearch(8, $email);
            $cansignup = false;

            if (intval($checkbepoz['IDList']['Count']) > 1) {
                // it means more than 1 email duplicated
                return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
            } else if (intval($checkbepoz['IDList']['Count']) == 1) {
                // get details from email search AccountID
                $result = $bepoz->AccountFullGet($checkbepoz['IDList']['ID']);

                if ($request->has('AccountID')) {
                    // FIRST TIME SIGNIN
                    // NEED TO CHECK AccountID & AccNumber match
                    if (
                        intval($result['AccountFull']['AccountID']) == intval($request->input('AccountID')) &&
                        intval($result['AccountFull']['AccNumber']) == intval($request->input('AccNumber'))
                    ) {
                        $emailBepozCan = true;
                    } else {
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
                    }

                } else {
                    return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
                }

            } else if (intval($checkbepoz['IDList']['Count']) == 0) {
                $emailBepozCan = true;
            }


            if ($this->checkSSOAccount($email) == "ok") {
                $emailSSOCan = false;
                return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_EMAIL')], Response::HTTP_BAD_REQUEST);
            } else if ($this->checkSSOAccount($email) == "Unable to connect SSO") {
                $emailSSOCan = false;
                return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_SSO')], Response::HTTP_BAD_REQUEST);
            } else {
                $emailSSOCan = true;
            }

            $cansignup = $emailBepozCan && $emailSSOCan;

            if ($cansignup) {

                // CAN SIGNUP
                DB::beginTransaction();
                $credentials = array(
                    'email' => $email,
                    'password' => Hash::make($password),
                    'mobile' => $mobile
                );

                if ($request->has('mobile')) {
                    $credentials['mobile'] = $request->input('mobile');
                }

                $user_obj = User::where('email', $email)->first();

                $guid = new Guid;
                $login_token = $guid->create();

                if (!is_null($user_obj)) {

                    if ($user_obj->member->status == 'active') {
                        return response()->json(['status' => 'error', 'message' => 'signup failed. Your email has been used.'], Response::HTTP_BAD_REQUEST);
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
                    }
                } else {
                    $user = User::create($credentials);

                    // Member::create([
                    //     'user_id' => $user->id,
                    //     'first_name' => $first_name,
                    //     'last_name' => $last_name,
                    //     'bepoz_account_card_number' => $request->has("card_number") ? $request->input("card_number") : null,
                    //     'dob' => $request->has('dob') ? new Carbon($request->input('dob'), config('app.timezone')) : null,
                    //     'login_token' => $login_token
                    // ]);

                    $member = new Member;
                    $member->user_id = $user->id;
                    $member->first_name = $first_name;
                    $member->last_name = $last_name;
                    $member->share_info = $share_info;
                    $member->current_preferred_venue_name = "";
                    $member->original_preferred_venue_name = "";

                    if ($request->has('dob')) {
                        if ($request->input('dob') != '') {
                            $member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                        } else {
                            $member->dob = null;
                        }
                    } else {
                        $member->dob = null;
                    }

                    $member->postcode = $request->has('postcode') ? $request->input('postcode') : null;

                    $payloadMerge = array_merge($memberGaming, $customfieldpayload);

                    // Log::info("memberGaming");
                    // Log::info($memberGaming);
                    // Log::info("customfieldpayload");
                    // Log::info($customfieldpayload);
                    // Log::info("payloadMerge");
                    // Log::info($payloadMerge);
                    $member->payload = \GuzzleHttp\json_encode($payloadMerge);
                    $member->save();

                    $confirmationToken = $user->generateEmailConfirmationToken();

                    $user_obj = User::find($user->id); // refresh...
                    $user_obj->member->save();

                    $user_role = new UserRoles;
                    $user_role->role_id = 3; // Manually add
                    $user_role->user_id = $user->id;
                    $user_role->save();

                    $default_tier = Setting::where('key', 'default_tier')->first()->value;

                    if ($request->has('GroupID')) {
                        if ($request->input('GroupID') != "") {
                            $tier = Tier::where('bepoz_group_id', $request->input('GroupID'))->first();
                            if (is_null($tier)) {
                                // $tier = Tier::first();
                                $tier = Tier::find($default_tier);
                            }
                        } else {
                            $tier = Tier::find($default_tier);
                        }
                    } else {
                        $tier = Tier::find($default_tier);
                    }
                    // QUEENSBERRY FEATURE
                    // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                    // if ($default_member_expiry_date == "true"){
                    //     $setting = Setting::where('key', 'cut_off_date')->first();
                    //     $date_expiry = Carbon::parse($setting->value);
                    //     $date_expiry->setTimezone(config('app.timezone'));
                    //     $date_expiry->setTime(0, 0, 0);
                    // } else {
                    //     $date_expiry = null;
                    // }

                    $date_expiry = null;
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                    if ($request->has('venue_id')) {
                        if ($request->input('venue_id') != '' && intval($request->input('venue_id')) > 0) {
                            $venue = Venue::find($request->input('venue_id'));
                            $member->current_preferred_venue = is_null($venue) ? '' : $venue->id;
                            $member->current_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                            $member->original_preferred_venue = is_null($venue) ? '' : $venue->id;
                            $member->original_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                        }
                    }

                    $member->share_info = 1;
                    $member->save();
                }

                $user = User::find($user_obj->id); // refresh...

                if (!is_null($user)) {
                    $user->member->member_tier->tier;
                    $ml = new MemberLog();
                    $ml->member_id = $user->member->id;
                    $ml->message = "This member sign-up and register SSO";
                    $ml->after = \GuzzleHttp\json_encode($user);
                    $ml->save();
                }

                if ($request->hasFile('photo')) {
                    if (config('bucket.s3')) {
                        $picture = $request->file('photo');
                        $mime = explode('/', $picture->getMimeType());
                        $type = end($mime);
                        $imageOptimizer->optimizeUploadedImageFile($picture);
                        Storage::disk('s3')->put('member/' . $user->member->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');
                        $user->member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $user->member->id . "/profile." . $type . "?" . time();
                        $user->member->save();
                    } else {
                        $log = new SystemLog();
                        $log->type = 'setting-error';
                        $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                        $log->payload = 'AuthenticationController.php/signup';
                        $log->message = 'Setting of S3 bucket not found.';
                        $log->source = 'AuthenticationController.php';
                        $log->save();

                        return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                    }
                }

                if (!$user->email_confirmation){
                    $this->sendEmailConfirmation($user, $mx, $mjx);
                }
                
                $job = (new SendReminderNotification($user))->delay(60);
                dispatch($job);

                // NEW FEATURE MOBILE CONFIRMATION FROM REDCAPE PUBLINC
                $mobile_confirmation_enable = Setting::where('key', 'mobile_confirmation_enable')->first()->value;
                if ($mobile_confirmation_enable == "true"){
                    if (!$user->sms_confirmation){
                        $this->sendMobileConfirmation($user);
                    }
                }
                
                //change logic reward given when confirm
                $this->getReward($request, $user);

                DB::commit();

                //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/register";
                $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                // $encoded = json_encode($data);
                // $checksum = md5($encoded);

                $datasend = array(
                    'email' => $email,
                    'password' => $request->input('password'),
                    'name' => $first_name . " " . $last_name,
                    'given_name' => $first_name,
                    'family_name' => $last_name,
                    'phone_number' => $mobile,
                    "license_key" => $your_order_key
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $your_order_api);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                $content = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                // Log::warning($content);
                // Log::warning($httpCode);

                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);
                    if ($payload->status == "error" && $payload->error == "user_not_found") {
                        return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                    }
                    if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                        return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                    }

                    $explode = explode('.', $payload->data->id_token);
                    $decoded_id_token = base64_decode($explode[1]);
                    $decoded = json_decode($decoded_id_token);

                    $member->login_token = $decoded->guid;
                    $member->save();

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Unable to connect to SSO server'], Response::HTTP_BAD_REQUEST);
                }

                $user = User::find($user->id); // refresh...
                $token = JWTAuth::fromUser($user);
                //NOT REQUIRED TO RESET PASSWORD
                $reset_password = 0;
                $first_time = 1;
                return response()->json([
                    'status' => 'ok', 'reset_password' => $reset_password, 'token' => $token,
                    'first_time' => $first_time,
                    'id_token' => $payload->data->id_token, 'access_token' => $payload->data->access_token,
                    'refresh_token' => $payload->data->refresh_token
                ]);
            }

        } catch (\Exception $e) {
            Log::error("signupOdyssey 5424");
            DB::rollback();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Confirm email of the user via token
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function confirm($token)
    {
        try {
            // Log::error("confirm 4017");
            $bepoz = new Bepoz();
            $checkBepozConnection = $bepoz->SystemCheck();

            // NEED CHECK BEPOZ IS READY
            if ($checkBepozConnection) {
                // Log::error("confirm 4023");
                $data = User::where('email_confirmation_token', $token)
                    ->first();

                if (is_null($data)) {
                    // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                    $name = Setting::where('key', 'company_name')->first()->value;
                    $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                    $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                    $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                    $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                    $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                    $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                    $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                    
                    return View::make('email.confirmation-unsuccessful', 
                    array(  'company_logo' => $email_confirmation_unsuccessful_image,
                            'company_name' => $name,
                            'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                            'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                            'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                            'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                            'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                            'email_confirmation_unsuccessful_message' => 'Invalid Token'
                        )
                    );
                } else {
                    // Log::error("confirm 4032");
                    if (!$data->email_confirmation) {
                        try {
                            $user = User::find($data->id);

                            $user->email_confirmation = true;
                            $user->email_confirmed_at = Carbon::now(config('app.timezone'));
                            $user->save();
                            // Log::error("confirm 4040");
                            $user = User::find($data->id);

                            // call in background first, if fail put in the cron job list
                            // call_in_background('sync-account-individual ' . $user->member->id);
                            // Log::error("confirm 4045");
                            $data = array(
                                "member_id" => $user->member->id,
                                "type" => 'email'
                            );

                            $job = new BepozJob();
                            $job->queue = "sync-account";
                            $job->payload = \GuzzleHttp\json_encode($data);
                            $job->available_at = time();
                            $job->created_at = time();
                            $job->save();
                            // Log::error("confirm 4057");
                            dispatch(new SendWelcomeEmail($user));
                            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                            $name = Setting::where('key', 'company_name')->first()->value;
                            $email_confirmation_successful_image = Setting::where('key', 'email_confirmation_successful_image')->first()->value;
                            $email_confirmation_successful_message = Setting::where('key', 'email_confirmation_successful_message')->first()->value;
                            $email_confirmation_successful_button_font_color = Setting::where('key', 'email_confirmation_successful_button_font_color')->first()->value;
                            $email_confirmation_successful_button_border_color = Setting::where('key', 'email_confirmation_successful_button_border_color')->first()->value;
                            $email_confirmation_successful_button_bg_color = Setting::where('key', 'email_confirmation_successful_button_bg_color')->first()->value;
                            $email_confirmation_successful_font_color = Setting::where('key', 'email_confirmation_successful_font_color')->first()->value;
                            $email_confirmation_successful_bg_color = Setting::where('key', 'email_confirmation_successful_bg_color')->first()->value;
                            
                            // Log::error("confirm 4061");
                            return View::make('email.confirmation-successful', 
                                array(  'company_logo' => $email_confirmation_successful_image, 
                                        'company_name' => $name,
                                        'email_confirmation_successful_button_font_color' => $email_confirmation_successful_button_font_color,
                                        'email_confirmation_successful_button_border_color' => $email_confirmation_successful_button_border_color,
                                        'email_confirmation_successful_button_bg_color' => $email_confirmation_successful_button_bg_color,
                                        'email_confirmation_successful_font_color' => $email_confirmation_successful_font_color,
                                        'email_confirmation_successful_bg_color' => $email_confirmation_successful_bg_color,
                                        'email_confirmation_successful_message' => $email_confirmation_successful_message
                                )
                            );
                        } catch (\Exception $e) {
                            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                            $name = Setting::where('key', 'company_name')->first()->value;
                            $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                            $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                            $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                            $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                            $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                            $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                            $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                            
                            return View::make('email.confirmation-unsuccessful', 
                            array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                    'company_name' => $name,
                                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                    'email_confirmation_unsuccessful_message' => $e->getMessage()
                                )
                            );
                        }

                    } else {
                        // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                        $name = Setting::where('key', 'company_name')->first()->value;
                        $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                        $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                        $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                        $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                        $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                        $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                        $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                            
                        return View::make('email.confirmation-unsuccessful', 
                        array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                'company_name' => $name,
                                'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                'email_confirmation_unsuccessful_message' => "You are already confirmed."
                            )
                        );
                        
                    }
                }
            } else {
                // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                $name = Setting::where('key', 'company_name')->first()->value;
                $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                            
                return View::make('email.confirmation-unsuccessful', 
                array(  'company_logo' => $email_confirmation_unsuccessful_image,
                        'company_name' => $name,
                        'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                        'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                        'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                        'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                        'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                        'email_confirmation_unsuccessful_message' => "System problem. Please confirm your email again later."
                    )
                );
            }

        } catch (\Exception $e) {
            Log::error($e);
            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
            $name = Setting::where('key', 'company_name')->first()->value;
            $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
            $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
            $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
            $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
            $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
            $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
            $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                        
            return View::make('email.confirmation-unsuccessful', 
            array(  'company_logo' => $email_confirmation_unsuccessful_image,
                    'company_name' => $name,
                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                    'email_confirmation_unsuccessful_message' => $e->getMessage()
                )
            );
        }
    }

    /**
     * Confirm email of the user via token with Matching Account
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function confirmMatch($token)
    {
        try {
            $data = User::where('email_confirmation_token', $token)
                ->first();

            if (is_null($data)) {
                // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                $name = Setting::where('key', 'company_name')->first()->value;
                $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                            
                return View::make('email.confirmation-unsuccessful', 
                array(  'company_logo' => $email_confirmation_unsuccessful_image,
                        'company_name' => $name,
                        'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                        'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                        'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                        'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                        'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                        'email_confirmation_unsuccessful_message' => 'Invalid Token'
                    )
                );

                return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => 'Invalid Token'));
            } else {
                if (!$data->email_confirmation) {
                    try {
                        $user = User::find($data->id);

                        $user->email_confirmation = true;
                        $user->email_confirmed_at = Carbon::now(config('app.timezone'));
                        $user->save();

                        $user = User::find($data->id);

                        $data = array(
                            "member_id" => $user->member->id,
                            "type" => 'email'
                        );

                        $job = new BepozJob();
                        $job->queue = "update-account";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        dispatch(new SendWelcomeEmail($user));
                        // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                        $name = Setting::where('key', 'company_name')->first()->value;
                        $email_confirmation_successful_image = Setting::where('key', 'email_confirmation_successful_image')->first()->value;
                        $email_confirmation_successful_message = Setting::where('key', 'email_confirmation_successful_message')->first()->value;
                        $email_confirmation_successful_button_font_color = Setting::where('key', 'email_confirmation_successful_button_font_color')->first()->value;
                        $email_confirmation_successful_button_border_color = Setting::where('key', 'email_confirmation_successful_button_border_color')->first()->value;
                        $email_confirmation_successful_button_bg_color = Setting::where('key', 'email_confirmation_successful_button_bg_color')->first()->value;
                        $email_confirmation_successful_font_color = Setting::where('key', 'email_confirmation_successful_font_color')->first()->value;
                        $email_confirmation_successful_bg_color = Setting::where('key', 'email_confirmation_successful_bg_color')->first()->value;
                        
                        return View::make('email.confirmation-successful', 
                            array(  'company_logo' => $email_confirmation_successful_image,
                                    'company_name' => $name,
                                    'email_confirmation_successful_button_font_color' => $email_confirmation_successful_button_font_color,
                                    'email_confirmation_successful_button_border_color' => $email_confirmation_successful_button_border_color,
                                    'email_confirmation_successful_button_bg_color' => $email_confirmation_successful_button_bg_color,
                                    'email_confirmation_successful_font_color' => $email_confirmation_successful_font_color,
                                    'email_confirmation_successful_bg_color' => $email_confirmation_successful_bg_color,
                                    'email_confirmation_successful_message' => $email_confirmation_successful_message
                            )
                        );
                    } catch (\Exception $e) {
                        // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                        $name = Setting::where('key', 'company_name')->first()->value;
                        $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                        $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                        $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                        $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                        $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                        $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                        $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                    
                        return View::make('email.confirmation-unsuccessful', 
                        array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                'company_name' => $name,
                                'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                'email_confirmation_unsuccessful_message' => $e->getMessage()
                            )
                        );
                    }

                } else {
                    // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                    $name = Setting::where('key', 'company_name')->first()->value;
                    $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                    $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                    $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                    $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                    $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                    $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                    $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                
                    return View::make('email.confirmation-unsuccessful', 
                    array(  'company_logo' => $email_confirmation_unsuccessful_image,
                            'company_name' => $name,
                            'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                            'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                            'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                            'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                            'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                            'email_confirmation_unsuccessful_message' => "You are already confirmed."
                        )
                    );
                }
            }

        } catch (\Exception $e) {
            Log::error($e);
            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
            $name = Setting::where('key', 'company_name')->first()->value;
            $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
            $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
            $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
            $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
            $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
            $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
            $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                        
            return View::make('email.confirmation-unsuccessful', 
            array(  'company_logo' => $email_confirmation_unsuccessful_image,
                    'company_name' => $name,
                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                    'email_confirmation_unsuccessful_message' => $e->getMessage()
                )
            );
        }
    }

    /**
     * Confirm email of the user via token IGT Mode
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function confirmIGT($token)
    {
        try {
            $bepoz = new Bepoz();
            $checkBepozConnection = $bepoz->SystemCheck();

            // NEED CHECK BEPOZ IS READY
            if ($checkBepozConnection) {
                $data = User::where('email_confirmation_token', $token)
                    ->first();

                if (is_null($data)) {
                    // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                    $name = Setting::where('key', 'company_name')->first()->value;
                    $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                    $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                    $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                    $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                    $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                    $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                    $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                
                    return View::make('email.confirmation-unsuccessful', 
                    array(  'company_logo' => $email_confirmation_unsuccessful_image,
                            'company_name' => $name,
                            'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                            'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                            'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                            'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                            'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                            'email_confirmation_unsuccessful_message' => 'Invalid Token'
                        )
                    );
                } else {
                    if (!$data->email_confirmation) {
                        try {

                            $user = User::find($data->id);

                            // LOAD GAMING SETTING
                            $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;
                            // TRY SIGNUP TO GAMING SYSTEM FIRST
                            if ($gaming_system_enable == "true") {

                                $gaming_system = Setting::where('key', 'gaming_system')->first()->value;

                                if ($gaming_system == "igt") {

                                    $igt = new IGT();
                                    // USE THIS NO NEED HOLD MEMBER
                                    $membershipId = 1;
                                    $resultGetNextAvailableMemberNumber = $igt->GetNextAvailableMemberNumber($membershipId);

                                    if ($resultGetNextAvailableMemberNumber) {
                                        $memberNumber = $resultGetNextAvailableMemberNumber->number;
                                    } else {
                                        return response()->json(['status' => 'error', 'message' => config('bucket.GAMING_SYSTEM_PROBLEM')], Response::HTTP_BAD_REQUEST);
                                    }

                                    $memberPayload = \GuzzleHttp\json_decode($user->member->payload);
                                    // CREATE MEMBER
                                    $siteId = 1;
                                    $membershipId = 1;
                                    $termId = 1;
                                    // $preferredName = $first_name;
                                    // $address = "71 Boundary Road, North Melb, VIC";
                                    // $zip_code = "3051";
                                    // $address_country_id = "36";
                                    // $address_state_id = "36";
                                    // $phoneNumber = $mobile;

                                    $resultCreateMember = $igt->CreateMember($siteId, $membershipId, $termId, $memberNumber, $user->member->first_name,
                                        $user->member->last_name, $memberPayload->preferred_name, $memberPayload->address, $memberPayload->zip_code,
                                        $memberPayload->address_country_id, $memberPayload->address_state_id, $user->mobile, $user->email);

                                    if ($resultCreateMember) {
                                        $bepoz_account_number = $memberNumber;
                                        $user->member->bepoz_account_number = $bepoz_account_number;
                                        $user->member->save();
                                    } else {
                                        return response()->json(['status' => 'error', 'message' => config('bucket.GAMING_SYSTEM_PROBLEM')], Response::HTTP_BAD_REQUEST);
                                    }

                                } else {
                                    return response()->json(['status' => 'error', 'message' => config('bucket.GAMING_SYSTEM_CONFIGURATION')], Response::HTTP_BAD_REQUEST);
                                }
                            }

                            $user->email_confirmation = true;
                            $user->email_confirmed_at = Carbon::now(config('app.timezone'));
                            $user->save();

                            // reload
                            $user = User::find($data->id);

                            // NO NEED CREATE BEPOZ ACCOUNT, JUST MATCH ACCOUNT TRIGGER FROM API isGamingAccountCreated
                            // call_in_background('sync-account-individual ' . $user->member->id);

                            // $data = array(
                            //     "member_id" => $user->member->id,
                            //     "type" => 'email'
                            // );

                            // $job = new BepozJob();
                            // if (intval($user->member->bepoz_account_id) > 0 ){
                            //     $job->queue = "update-account";
                            // } else {
                            //     $job->queue = "sync-account";
                            // }
                            // $job->queue = "update-account";
                            // $job->payload = \GuzzleHttp\json_encode($data);
                            // $job->available_at = time();
                            // $job->created_at = time();
                            // $job->save();

                            dispatch(new SendWelcomeEmail($user));
                            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                            $name = Setting::where('key', 'company_name')->first()->value;
                            $email_confirmation_successful_image = Setting::where('key', 'email_confirmation_successful_image')->first()->value;
                            $email_confirmation_successful_message = Setting::where('key', 'email_confirmation_successful_message')->first()->value;
                            $email_confirmation_successful_button_font_color = Setting::where('key', 'email_confirmation_successful_button_font_color')->first()->value;
                            $email_confirmation_successful_button_border_color = Setting::where('key', 'email_confirmation_successful_button_border_color')->first()->value;
                            $email_confirmation_successful_button_bg_color = Setting::where('key', 'email_confirmation_successful_button_bg_color')->first()->value;
                            $email_confirmation_successful_font_color = Setting::where('key', 'email_confirmation_successful_font_color')->first()->value;
                            $email_confirmation_successful_bg_color = Setting::where('key', 'email_confirmation_successful_bg_color')->first()->value;
                            
                            return View::make('email.confirmation-successful', 
                                array(  'company_logo' => $email_confirmation_successful_image,
                                        'company_name' => $name,
                                        'email_confirmation_successful_button_font_color' => $email_confirmation_successful_button_font_color,
                                        'email_confirmation_successful_button_border_color' => $email_confirmation_successful_button_border_color,
                                        'email_confirmation_successful_button_bg_color' => $email_confirmation_successful_button_bg_color,
                                        'email_confirmation_successful_font_color' => $email_confirmation_successful_font_color,
                                        'email_confirmation_successful_bg_color' => $email_confirmation_successful_bg_color,
                                        'email_confirmation_successful_message' => $email_confirmation_successful_message
                                )
                            );
                        } catch (\Exception $e) {
                            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                            $name = Setting::where('key', 'company_name')->first()->value;
                            $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                            $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                            $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                            $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                            $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                            $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                            $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                        
                            return View::make('email.confirmation-unsuccessful', 
                            array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                    'company_name' => $name,
                                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                    'email_confirmation_unsuccessful_message' => $e->getMessage()
                                )
                            );
                            return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => $e->getMessage()));
                        }
                    } else {
                        // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                        $name = Setting::where('key', 'company_name')->first()->value;
                        $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                        $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                        $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                        $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                        $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                        $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                        $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                    
                        return View::make('email.confirmation-unsuccessful', 
                        array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                'company_name' => $name,
                                'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                'email_confirmation_unsuccessful_message' => "You are already confirmed."
                            )
                        );
                        
                    }
                }

            } else {
                // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                $name = Setting::where('key', 'company_name')->first()->value;
                $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                            
                return View::make('email.confirmation-unsuccessful', 
                array(  'company_logo' => $email_confirmation_unsuccessful_image,
                        'company_name' => $name,
                        'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                        'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                        'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                        'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                        'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                        'email_confirmation_unsuccessful_message' => "System problem. Please confirm your email again later."
                    )
                );
                
            }

        } catch (\Exception $e) {
            Log::error($e);
            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
            $name = Setting::where('key', 'company_name')->first()->value;
            $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
            $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
            $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
            $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
            $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
            $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
            $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                        
            return View::make('email.confirmation-unsuccessful', 
            array(  'company_logo' => $email_confirmation_unsuccessful_image,
                    'company_name' => $name,
                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                    'email_confirmation_unsuccessful_message' => $e->getMessage()
                )
            );
            
        }
    }


    protected function bepozSearch($email)
    {
        $bepoz = new Bepoz;
        $checkbepoz = $bepoz->AccountSearch(8, $email);

        if ($checkbepoz) {

            if (intval($checkbepoz['IDList']['Count']) > 1) {
                // it means more than 1 email duplicated
                $result = false;
            } else if (intval($checkbepoz['IDList']['Count']) == 1) {
                // get details from email search AccountID
                $result = $bepoz->AccountFullGet($checkbepoz['IDList']['ID']);

                if ($result) {
                    // just continue
                } else {
                    $result = false;
                }
            } else if (intval($checkbepoz['IDList']['Count']) == 0) {
                $result = false;
            }
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * Confirm email of the user via token Odyssey Mode
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function confirmOdyssey($token)
    {
        try {
            $bepoz = new Bepoz();
            $checkBepozConnection = $bepoz->SystemCheck();

            // NEED CHECK BEPOZ IS READY
            if ($checkBepozConnection) {
                $data = User::where('email_confirmation_token', $token)
                    ->first();

                if (is_null($data)) {
                    // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                    $name = Setting::where('key', 'company_name')->first()->value;
                    $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                    $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                    $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                    $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                    $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                    $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                    $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                
                    return View::make('email.confirmation-unsuccessful', 
                    array(  'company_logo' => $email_confirmation_unsuccessful_image,
                            'company_name' => $name,
                            'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                            'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                            'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                            'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                            'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                            'email_confirmation_unsuccessful_message' => 'Invalid Token'
                        )
                    );
                    
                } else {
                    if (!$data->email_confirmation) {
                        try {

                            $user = User::find($data->id);

                            // LOAD GAMING SETTING
                            $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;
                            // TRY SIGNUP TO GAMING SYSTEM FIRST
                            if ($gaming_system_enable == "true") {
                                // Log::info("gaming_system_enable");
                                $gaming_system = Setting::where('key', 'gaming_system')->first()->value;

                                if ($gaming_system == "odyssey") {
                                    // Log::info("gaming_system odyssey");
                                    $odyssey = new MetropolisARCOdyssey();

                                    $resultManageToken = $odyssey->ManageToken();
                                    $memberPayload = \GuzzleHttp\json_decode($user->member->payload);

                                    $venue = Venue::find($user->member->current_preferred_venue);
                                    $custom_field_value = 0;
                                    if (!is_null($venue)) {
                                        $custom_field_value = $venue->odyssey_custom_field_id;
                                    }

                                    $resultCreateMember = $odyssey->AddPreliminaryMember($resultManageToken, ($memberPayload->gender ?? ''),
                                        ($memberPayload->title ?? ''), $user->member->first_name, $user->member->last_name, ($memberPayload->preferredname ?? ''),
                                        $user->member->dob, ($memberPayload->occupation ?? ''), ($memberPayload->addresstype ?? ''),
                                        ($memberPayload->addressline1 ?? ''), ($memberPayload->suburb ?? ''), ($memberPayload->postcode ?? ''), 
                                        ($memberPayload->state ?? ''), $user->email, $user->mobile, $custom_field_value);

                                    // Log::info($resultCreateMember);

                                    if ($resultCreateMember) {

                                        $resultDecode = \GuzzleHttp\json_decode($resultCreateMember);

                                        if (isset($resultDecode->body->errorCode)) {
                                            if ($resultDecode->body->errorCode == '0' && $resultDecode->body->errorText == 'Operation success') {

                                                if (!is_null($user->member->payload)) {
                                                    $memberGaming = \GuzzleHttp\json_decode($user->member->payload);
                                                } else {
                                                    $memberGaming = [];
                                                }

                                                $memberGaming = (array)$memberGaming;

                                                foreach ($resultDecode->body->result as $row) {
                                                    if ($row->Name == 'MemberId') {
                                                        $memberGaming['MemberId'] = $row->Value;
                                                        // $user->member->bepoz_account_number = $memberGaming['MemberId'];
                                                    }

                                                    if ($row->Name == 'BadgeNo') {
                                                        $memberGaming['BadgeNo'] = $row->Value;
                                                        $user->member->bepoz_account_number = $memberGaming['BadgeNo'];
                                                    }

                                                    if ($row->Name == 'ExpiryDt') {
                                                        $memberGaming['ExpiryDt'] = $row->Value;
                                                    }
                                                }

                                                $user->member->payload = \GuzzleHttp\json_encode($memberGaming);
                                                $user->member->save();
                                            } else {
                                                if (isset($resultDecode->body->result)) {
                                                    $message = $resultDecode->body->result;
                                                } else {
                                                    $message = config('bucket.GAMING_SYSTEM_PROBLEM');
                                                }

                                                // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                                                $name = Setting::where('key', 'company_name')->first()->value;
                                                $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                                                $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                                                $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                                                $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                                                $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                                                $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                                                $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                                            
                                                return View::make('email.confirmation-unsuccessful', 
                                                array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                                        'company_name' => $name,
                                                        'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                                        'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                                        'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                                        'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                                        'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                                        'email_confirmation_unsuccessful_message' => $message
                                                    )
                                                );
                                                
                                            }
                                        } else {
                                            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                                            $name = Setting::where('key', 'company_name')->first()->value;
                                            $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                                            $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                                            $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                                            $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                                            $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                                            $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                                            $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                                        
                                            return View::make('email.confirmation-unsuccessful', 
                                            array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                                    'company_name' => $name,
                                                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                                    'email_confirmation_unsuccessful_message' => config('bucket.GAMING_SYSTEM_PROBLEM')
                                                )
                                            );
                                            
                                        }
                                    } else {
                                        // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                                        $name = Setting::where('key', 'company_name')->first()->value;
                                        $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                                        $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                                        $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                                        $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                                        $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                                        $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                                        $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                                    
                                        return View::make('email.confirmation-unsuccessful', 
                                        array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                                'company_name' => $name,
                                                'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                                'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                                'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                                'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                                'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                                'email_confirmation_unsuccessful_message' => config('bucket.GAMING_SYSTEM_PROBLEM')
                                            )
                                        );
                                        
                                    }

                                } else {
                                    // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                                    $name = Setting::where('key', 'company_name')->first()->value;
                                    $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                                    $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                                    $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                                    $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                                    $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                                    $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                                    $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                                
                                    return View::make('email.confirmation-unsuccessful', 
                                    array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                            'company_name' => $name,
                                            'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                            'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                            'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                            'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                            'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                            'email_confirmation_unsuccessful_message' => config('bucket.GAMING_SYSTEM_CONFIGURATION')
                                        )
                                    );

                                }
                            }

                            $user->email_confirmation = true;
                            $user->email_confirmed_at = Carbon::now(config('app.timezone'));
                            $user->save();

                            // reload
                            $user = User::find($data->id);

                            // NO NEED CREATE BEPOZ ACCOUNT, JUST MATCH ACCOUNT TRIGGER FROM API isGamingAccountCreated
                            // call_in_background('sync-account-individual ' . $user->member->id);

                            // $data = array(
                            //     "member_id" => $user->member->id,
                            //     "type" => 'email'
                            // );

                            // $job = new BepozJob();
                            // if (intval($user->member->bepoz_account_id) > 0 ){
                            //     $job->queue = "update-account";
                            // } else {
                            //     $job->queue = "sync-account";
                            // }
                            // $job->queue = "update-account";
                            // $job->payload = \GuzzleHttp\json_encode($data);
                            // $job->available_at = time();
                            // $job->created_at = time();
                            // $job->save();

                            dispatch(new SendWelcomeEmail($user));
                            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                            $name = Setting::where('key', 'company_name')->first()->value;
                            $email_confirmation_successful_image = Setting::where('key', 'email_confirmation_successful_image')->first()->value;
                            $email_confirmation_successful_message = Setting::where('key', 'email_confirmation_successful_message')->first()->value;
                            $email_confirmation_successful_button_font_color = Setting::where('key', 'email_confirmation_successful_button_font_color')->first()->value;
                            $email_confirmation_successful_button_border_color = Setting::where('key', 'email_confirmation_successful_button_border_color')->first()->value;
                            $email_confirmation_successful_button_bg_color = Setting::where('key', 'email_confirmation_successful_button_bg_color')->first()->value;
                            $email_confirmation_successful_font_color = Setting::where('key', 'email_confirmation_successful_font_color')->first()->value;
                            $email_confirmation_successful_bg_color = Setting::where('key', 'email_confirmation_successful_bg_color')->first()->value;
                            
                            return View::make('email.confirmation-successful', 
                                array(  'company_logo' => $email_confirmation_successful_image,
                                        'company_name' => $name,
                                        'email_confirmation_successful_button_font_color' => $email_confirmation_successful_button_font_color,
                                        'email_confirmation_successful_button_border_color' => $email_confirmation_successful_button_border_color,
                                        'email_confirmation_successful_button_bg_color' => $email_confirmation_successful_button_bg_color,
                                        'email_confirmation_successful_font_color' => $email_confirmation_successful_font_color,
                                        'email_confirmation_successful_bg_color' => $email_confirmation_successful_bg_color,
                                        'email_confirmation_successful_message' => $email_confirmation_successful_message
                                )
                            );
                        } catch (\Exception $e) {
                            // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                            $name = Setting::where('key', 'company_name')->first()->value;
                            $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                            $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                            $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                            $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                            $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                            $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                            $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                        
                            return View::make('email.confirmation-unsuccessful', 
                            array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                    'company_name' => $name,
                                    'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                    'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                    'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                    'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                    'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                    'email_confirmation_unsuccessful_message' => $e->getMessage()
                                )
                            );
                        }
                    } else {
                        // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                        $name = Setting::where('key', 'company_name')->first()->value;
                        $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                        $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                        $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                        $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                        $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                        $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                        $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                                    
                        return View::make('email.confirmation-unsuccessful', 
                        array(  'company_logo' => $email_confirmation_unsuccessful_image,
                                'company_name' => $name,
                                'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                                'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                                'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                                'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                                'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                                'email_confirmation_unsuccessful_message' => "You are already confirmed."
                            )
                        );
                        
                    }
                }

            } else {
                // $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                $name = Setting::where('key', 'company_name')->first()->value;
                $email_confirmation_unsuccessful_image = Setting::where('key', 'email_confirmation_unsuccessful_image')->first()->value;
                $email_confirmation_unsuccessful_message = Setting::where('key', 'email_confirmation_unsuccessful_message')->first()->value;
                $email_confirmation_unsuccessful_button_font_color = Setting::where('key', 'email_confirmation_unsuccessful_button_font_color')->first()->value;
                $email_confirmation_unsuccessful_button_border_color = Setting::where('key', 'email_confirmation_unsuccessful_button_border_color')->first()->value;
                $email_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_button_bg_color')->first()->value;
                $email_confirmation_unsuccessful_font_color = Setting::where('key', 'email_confirmation_unsuccessful_font_color')->first()->value;
                $email_confirmation_unsuccessful_bg_color = Setting::where('key', 'email_confirmation_unsuccessful_bg_color')->first()->value;
                            
                return View::make('email.confirmation-unsuccessful', 
                array(  'company_logo' => $email_confirmation_unsuccessful_image,
                        'company_name' => $name,
                        'email_confirmation_unsuccessful_button_font_color' => $email_confirmation_unsuccessful_button_font_color,
                        'email_confirmation_unsuccessful_button_border_color' => $email_confirmation_unsuccessful_button_border_color,
                        'email_confirmation_unsuccessful_button_bg_color' => $email_confirmation_unsuccessful_button_bg_color,
                        'email_confirmation_unsuccessful_font_color' => $email_confirmation_unsuccessful_font_color,
                        'email_confirmation_unsuccessful_bg_color' => $email_confirmation_unsuccessful_bg_color,
                        'email_confirmation_unsuccessful_message' => "You are already confirmed."
                    )
                );
                
                return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => "System problem. Please confirm your email again later."));
            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => $e->getMessage()));
        }
    }

    /**
     * send email confirmation
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendEmailConfirmation($user, $mx, $mjx)
    {
        Log::info("function sendEmailConfirmation");
        $confirmationToken = $user->generateEmailConfirmationToken();

        // Get web app token
        $platform = Platform::where('name', 'web')->first();

        $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;

        $data = array(
            'URL' => $url
        );

        $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

        if ($email_server === "mandrill") {

            if ($mx->init()) {
                $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
            } else {
                $pj = new PendingJob();
                $pj->payload = \GuzzleHttp\json_encode($data);
                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                    'email' => $user->email,
                    'category' => 'confirmation',
                    'name' => $user->member->first_name . ' ' . $user->member->last_name
                ));
                $pj->queue = "email";
                $pj->save();
            }
        } else if ($email_server === "mailjet") {

            if ($mjx->init()) {
                $mjx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
            } else {
                $pj = new PendingJob();
                $pj->payload = \GuzzleHttp\json_encode($data);
                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                    'email' => $user->email,
                    'category' => 'confirmation',
                    'name' => $user->member->first_name . ' ' . $user->member->last_name
                ));
                $pj->queue = "email";
                $pj->save();
            }
        }

        $user->email_confirmation_sent = Carbon::now(config('app.timezone'));
        $user->save();
    }

    protected function updateBepozEmail($bepoz, $member, $new_email)
    {
        $groupID = $member->tiers()->first()->bepoz_group_id;
        $template = $member->tiers()->first()->bepoz_template_id;

        // Create or Update Bepoz Account
        $result = $bepoz->AccountFullGet($member->bepoz_account_id);
        // Log::warning($result);
        if ($result) {

            $request = $result['AccountFull'];
            $request['Email1st'] = $new_email;

            unset($request['AddressID']);
            unset($request['CommentID']);

            $resultUpdate = $bepoz->AccUpdate($request, $template);
            
            return $resultUpdate;
        } else {
            return $result;
        }
    }

    /**
     * Confirm email change of the user via SSO
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function confirmChangeEmail(Request $request, $token, Bepoz $bepoz)
    {
        try {
            $contactChange = ContactChange::where('hash_key', $token)->first();
            $user = User::find($contactChange->user_id);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
            $name = Setting::where('key', 'company_name')->first()->value;

            if (is_null($contactChange)) {
                return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => 'Invalid Token'));
            } else {
                if (!$contactChange->email_confirmation) {
                    try {
                        
                        $datenow = Carbon::now(config('app.timezone'));
                        $contactChange->email_confirmed_at = $datenow;
                        $contactChange->email_confirmation = true;
                        $contactChange->save();

                        $user->email_confirmation = true;
                        $user->email_confirmed_at = $datenow;
                        $user->save();
                        
                        $user->member->last_update = $datenow;
                        $user->member->save();

                        // CHECK BEPOZ
                        $updateBepozEmail = $this->updateBepozEmail($bepoz, $user->member, $contactChange->new_email);
                        // Log::info($updateBepozEmail);
                        if ($updateBepozEmail){
                            $setting = Setting::where('key', 'bepozcustomfield')->first();
                            $last_update = $this->customFieldSet($user->member, "Last Update", $user->member->updated_at, $setting, $bepoz);

                            $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/confirmChanges";
                            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                            $datasend = array(
                                'hash_key' => $token,
                                "license_key" => $your_order_key
                            );
                            
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $your_order_api);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            
                            // Log::info($content);
                            // Log::info($httpCode);

                            if ($content) {
                                $payload = \GuzzleHttp\json_decode($content);
                                
                                if ($payload->status == "error") {
                                    $log = new SystemLog();
                                    $log->type = 'sso-problem';
                                    $log->humanized_message = 'SSO problem. Please check the error message';
                                    $log->message = $payload;
                                    $log->source = 'AuthenticationController.php/confirmChangeEmail';
                                    $log->save();

                                    return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => "Technical problem, please contact Administrator"));
                                }

                                $user->email = $contactChange->new_email;
                                $user->save();
                                
                                $user->member->login_token = $payload->new_guid;
                                $user->member->save();

                                return View::make('email.confirmation-successful', array('company_logo' => $invoice_logo, 'company_name' => $name));
                            } else {
                                // return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                                $log = new SystemLog();
                                $log->type = 'sso-problem';
                                $log->humanized_message = 'SSO problem. Please check the error message';
                                $log->message = 'Unable to connect';
                                $log->source = 'AuthenticationController.php/confirmChangeEmail';
                                $log->save();

                                return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => config('bucket.UNABLE_CONNECT_SSO') ));
                            }

                        } else {   
                            $contactChange->mobile_confirmed_at = null;
                            $contactChange->mobile_confirmation = false;
                            $contactChange->save();

                            $user->sms_confirmation = false;
                            $user->sms_confirmed_at = null;
                            $user->save();
                            
                            $name = Setting::where('key', 'company_name')->first()->value;
                            $mobile_confirmation_unsuccessful_image = Setting::where('key', 'mobile_confirmation_unsuccessful_image')->first()->value;
                            $mobile_confirmation_unsuccessful_button_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_font_color')->first()->value;
                            $mobile_confirmation_unsuccessful_button_border_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_border_color')->first()->value;
                            $mobile_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_bg_color')->first()->value;
                            $mobile_confirmation_unsuccessful_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_font_color')->first()->value;
                            $mobile_confirmation_unsuccessful_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_bg_color')->first()->value;
                            // Log::error("confirm 4061");
                            return View::make('mobile.confirmation-unsuccessful', 
                                array(  'company_logo' => $mobile_confirmation_unsuccessful_image, 
                                        'company_name' => $name,
                                        'mobile_confirmation_unsuccessful_button_font_color' => $mobile_confirmation_unsuccessful_button_font_color,
                                        'mobile_confirmation_unsuccessful_button_border_color' => $mobile_confirmation_unsuccessful_button_border_color,
                                        'mobile_confirmation_unsuccessful_button_bg_color' => $mobile_confirmation_unsuccessful_button_bg_color,
                                        'mobile_confirmation_unsuccessful_font_color' => $mobile_confirmation_unsuccessful_font_color,
                                        'mobile_confirmation_unsuccessful_bg_color' => $mobile_confirmation_unsuccessful_bg_color,
                                        'mobile_confirmation_unsuccessful_message' => config('bucket.UNABLE_CONNECT_SSO')
                                )
                            );
                        }
                    } catch (\Exception $e) {
                        $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                        return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => $e->getMessage()));
                    }

                } else {
                    $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                    return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => "You are already confirmed."));
                }
            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => $e->getMessage()));
        }
    }

    protected function updateBepoz($bepoz, $member, $newmobile)
    {
        $groupID = $member->tiers()->first()->bepoz_group_id;
        $template = $member->tiers()->first()->bepoz_template_id;

        // Create or Update Bepoz Account
        $result = $bepoz->AccountFullGet($member->bepoz_account_id);
        // Log::warning($result);
        if ($result) {

            $request = $result['AccountFull'];
            $request['Mobile'] = $newmobile;

            unset($request['AddressID']);
            unset($request['CommentID']);

            $resultUpdate = $bepoz->AccUpdate($request, $template);
            
            return $resultUpdate;
        } else {
            return $result;
        }
    }

    protected function customFieldSet($member, $CustomFieldKey, $dataset, $setting, $bepoz)
    {
        $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);
        $CustomFieldNumber = "0";
        
        foreach ($allCustomField as $data) {
            if ($data->key == $CustomFieldKey) {
                $CustomFieldNumber = $data->field;
            }
        }

        //Log::info($CustomFieldNumber);

        $param = ['AccountID' => $member->bepoz_account_id,
            'CustomFieldNumber' => $CustomFieldNumber,
            'DataSet' => $dataset
        ];
        $customFieldResult = $bepoz->AccCustomFieldSet($param);

        return $customFieldResult;
    }

    /**
     * send mobile confirmation via SMS
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendMobileConfirmation($user)
    {
        $member = $user->member;
        $contactChange = new ContactChange;
        $contactChange->member_id = $member->id;
        $contactChange->user_id = $member->user->id;
        $contactChange->mode = "signup";
        $contactChange->old_mobile = "";
        $contactChange->new_mobile = $user->mobile;
        $contactChange->old_guid = $member->login_token;
        $contactChange->token = $this->generateRandomString(32);
        $contactChange->mobile_confirmation_sent = Carbon::now(config('app.timezone'));
        $contactChange->save();

        // Get web app token
        $platform = Platform::where('name', 'web')->first();
        $url = config('bucket.APP_URL') . '/api/confirmMobile/' . $contactChange->token . '?app_token=' . $platform->app_token;
        
        $sms_confirmation_message = Setting::where('key', 'sms_confirmation_message')->first()->value;

        $message = $sms_confirmation_message;

        $tier_name = $member->member_tier->tier->name;

        $message = preg_replace(['{{{FirstName}}}', '{{{LastName}}}', '{{{AccountNumber}}}', '{{{Tier}}}', '{{{Link}}}'],
        [$member->first_name, $member->last_name, $member->bepoz_account_number, $tier_name, $url], $message);

        // Log::info($message);
        // Log::info($user->mobile);
        // $result = $this->sendSMS($user->mobile, $message);

        $api = new \App\Helpers\TransmitSMSAPI();
        $result = $api->sendSms($message, $user->mobile, "");
    }

    /**
     * Confirm mobile of the user via token, after system send them SMS
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function confirmMobile(Request $request, $token, Bepoz $bepoz)
    {
        try {
            $contactChange = ContactChange::where('token', $token)->first();
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
            $company_name = Setting::where('key', 'company_name')->first()->value;

            if (is_null($contactChange)) {
                
                return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => 'Invalid Token'));
            } else {
                $user = User::find($contactChange->user_id);
                
                if (!$contactChange->mobile_confirmation) {
                    try {

                        $datenow = Carbon::now(config('app.timezone'));
                        $contactChange->mobile_confirmed_at = $datenow;
                        $contactChange->mobile_confirmation = true;
                        $contactChange->save();

                        $user->sms_confirmation = true;
                        $user->sms_confirmed_at = $datenow;
                        $user->save();

                        $user->member->last_update = $datenow;
                        $user->member->save();

                        if (is_null($user->member->bepoz_account_id)) {
                            // NORMAL SIGNUP GSMING
                            if ( $user->email_confirmation == true && $user->sms_confirmation == true){
                                $data = array(
                                    "member_id" => $user->member->id
                                );

                                $job = new BepozJob();
                                $job->queue = "update-account";
                                $job->payload = \GuzzleHttp\json_encode($data);
                                $job->available_at = time();
                                $job->created_at = time();
                                $job->save();
                            }

                        } else {
                            if ($contactChange->mode == "signup") {
                                if ( $user->email_confirmation == true && $user->sms_confirmation == true){
                                    $data = array(
                                        "member_id" => $user->member->id
                                    );
    
                                    $job = new BepozJob();
                                    $job->queue = "update-account";
                                    $job->payload = \GuzzleHttp\json_encode($data);
                                    $job->available_at = time();
                                    $job->created_at = time();
                                    $job->save();
                                }

                            } else {
                                
                                if ( $user->email_confirmation == true && $user->sms_confirmation == true){
                                    // CHECK BEPOZ
                                    $updateBepoz = $this->updateBepoz($bepoz, $user->member, $contactChange->new_mobile);
                                    
                                    if ($updateBepoz){
                                        $setting = Setting::where('key', 'bepozcustomfield')->first();
                                        $last_update = $this->customFieldSet($user->member, "Last Update", $user->member->updated_at, $setting, $bepoz);

                                        //SETTING SSO
                                        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
                                        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                                        $datasend = array();
                                        $datasend['guid'] = $user->member->login_token;
                                        $datasend['license_key'] = $your_order_key;
                                        $datasend['phone_number'] = $contactChange->new_mobile;

                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($ch, CURLOPT_URL, $your_order_api."/updateAccount");
                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                                        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                                        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                                        $content = curl_exec($ch);
                                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                        
                                        // Log::info($content);
                                        // Log::info($httpCode);
                            
                                        if ($content) {
                                            // Log::info("contactChange 1431");
                                            if ($httpCode != 200 ){
                                                return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => config('bucket.UNABLE_CONNECT_SSO') ));
                                            }

                                            $payload = \GuzzleHttp\json_decode($content);
                                            
                                            if ($payload->status == "error") {
                                                $log = new SystemLog();
                                                $log->type = 'sso-problem';
                                                $log->humanized_message = 'SSO problem. Please check the error message';
                                                $log->message = json_encode($payload);
                                                $log->source = 'AuthenticationController.php/confirmMobile';
                                                $log->save();

                                                return View::make('email.confirmation-unsuccessful', array('company_logo' => $invoice_logo, "message" => config('bucket.UNABLE_CONNECT_SSO') ));
                                            }
                            
                                            if (isset($payload->message)) {
                                                if ($payload->message == "Updating account is successful") {
                                                    // Log::info("contactChange 1451");
                                                    $user->mobile = $contactChange->new_mobile;
                                                    $user->save();
                        
                                                    $data = array();
                                                    $data['member_id'] = $user->member->id;
                        
                                                    $ml = new MemberLog();
                                                    $ml->before = "old mobile ".$contactChange->old_mobile ;
                                                    $ml->member_id = $user->member->id;
                                                    $ml->message = 'member mobile change and confirm';
                                                    $ml->after = "new mobile ".$contactChange->new_mobile ;
                                                    $ml->save();

                                                    // BLOCK ALL OTHER MOBILE CONFIRMATION FOR THIS USER


                                                }
                                            }
                            
                                        } else {
                                            $contactChange->mobile_confirmed_at = null;
                                            $contactChange->mobile_confirmation = false;
                                            $contactChange->save();

                                            $user->sms_confirmation = false;
                                            $user->sms_confirmed_at = null;
                                            $user->save();

                                            // return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                                            $log = new SystemLog();
                                            $log->type = 'sso-problem';
                                            $log->humanized_message = 'SSO problem. Please check the error message';
                                            $log->message = 'Unable to connect';
                                            $log->source = 'AuthenticationController.php/confirmMobile';
                                            $log->save();
                                        }

                                        $name = Setting::where('key', 'company_name')->first()->value;
                                        $mobile_confirmation_successful_image = Setting::where('key', 'mobile_confirmation_successful_image')->first()->value;
                                        $mobile_confirmation_successful_message = Setting::where('key', 'mobile_confirmation_successful_message')->first()->value;
                                        // $mobile_confirmation_successful_color = '#F4364C';
                                        $mobile_confirmation_successful_button_font_color = Setting::where('key', 'mobile_confirmation_successful_button_font_color')->first()->value;
                                        $mobile_confirmation_successful_button_border_color = Setting::where('key', 'mobile_confirmation_successful_button_border_color')->first()->value;
                                        $mobile_confirmation_successful_button_bg_color = Setting::where('key', 'mobile_confirmation_successful_button_bg_color')->first()->value;
                                        $mobile_confirmation_successful_font_color = Setting::where('key', 'mobile_confirmation_successful_font_color')->first()->value;
                                        $mobile_confirmation_successful_bg_color = Setting::where('key', 'mobile_confirmation_successful_bg_color')->first()->value;
                                        // Log::error("confirm 4061");
                                        return View::make('mobile.confirmation-successful', 
                                            array(  'company_logo' => $mobile_confirmation_successful_image, 
                                                    'company_name' => $name,
                                                    'mobile_confirmation_successful_button_font_color' => $mobile_confirmation_successful_button_font_color,
                                                    'mobile_confirmation_successful_button_border_color' => $mobile_confirmation_successful_button_border_color,
                                                    'mobile_confirmation_successful_button_bg_color' => $mobile_confirmation_successful_button_bg_color,
                                                    'mobile_confirmation_successful_font_color' => $mobile_confirmation_successful_font_color,
                                                    'mobile_confirmation_successful_bg_color' => $mobile_confirmation_successful_bg_color,
                                                    'mobile_confirmation_successful_message' => $mobile_confirmation_successful_message
                                            )
                                        );
                                        
                                    } else {
                                        // CANCEL CONFIRMATION
                                        $contactChange->mobile_confirmed_at = null;
                                        $contactChange->mobile_confirmation = false;
                                        $contactChange->save();

                                        $user->sms_confirmation = false;
                                        $user->sms_confirmed_at = null;
                                        $user->save();
                                        
                                        $name = Setting::where('key', 'company_name')->first()->value;
                                        $mobile_confirmation_unsuccessful_image = Setting::where('key', 'mobile_confirmation_unsuccessful_image')->first()->value;
                                        $mobile_confirmation_unsuccessful_button_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_font_color')->first()->value;
                                        $mobile_confirmation_unsuccessful_button_border_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_border_color')->first()->value;
                                        $mobile_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_bg_color')->first()->value;
                                        $mobile_confirmation_unsuccessful_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_font_color')->first()->value;
                                        $mobile_confirmation_unsuccessful_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_bg_color')->first()->value;
                                        // Log::error("confirm 4061");
                                        return View::make('mobile.confirmation-unsuccessful', 
                                            array(  'company_logo' => $mobile_confirmation_unsuccessful_image, 
                                                    'company_name' => $name,
                                                    'mobile_confirmation_unsuccessful_button_font_color' => $mobile_confirmation_unsuccessful_button_font_color,
                                                    'mobile_confirmation_unsuccessful_button_border_color' => $mobile_confirmation_unsuccessful_button_border_color,
                                                    'mobile_confirmation_unsuccessful_button_bg_color' => $mobile_confirmation_unsuccessful_button_bg_color,
                                                    'mobile_confirmation_unsuccessful_font_color' => $mobile_confirmation_unsuccessful_font_color,
                                                    'mobile_confirmation_unsuccessful_bg_color' => $mobile_confirmation_unsuccessful_bg_color,
                                                    'mobile_confirmation_unsuccessful_message' => config('bucket.UNABLE_CONNECT_BEPOZ')
                                            )
                                        );
                                    }
                                }
                            }

                        }
                            
                        
                        $name = Setting::where('key', 'company_name')->first()->value;
                        $mobile_confirmation_successful_image = Setting::where('key', 'mobile_confirmation_successful_image')->first()->value;
                        $mobile_confirmation_successful_message = Setting::where('key', 'mobile_confirmation_successful_message')->first()->value;
                        // $mobile_confirmation_successful_color = '#F4364C';
                        $mobile_confirmation_successful_button_font_color = Setting::where('key', 'mobile_confirmation_successful_button_font_color')->first()->value;
                        $mobile_confirmation_successful_button_border_color = Setting::where('key', 'mobile_confirmation_successful_button_border_color')->first()->value;
                        $mobile_confirmation_successful_button_bg_color = Setting::where('key', 'mobile_confirmation_successful_button_bg_color')->first()->value;
                        $mobile_confirmation_successful_font_color = Setting::where('key', 'mobile_confirmation_successful_font_color')->first()->value;
                        $mobile_confirmation_successful_bg_color = Setting::where('key', 'mobile_confirmation_successful_bg_color')->first()->value;
                        // Log::error("confirm 4061");
                        return View::make('mobile.confirmation-successful', 
                            array(  'company_logo' => $mobile_confirmation_successful_image, 
                                    'company_name' => $name,
                                    'mobile_confirmation_successful_button_font_color' => $mobile_confirmation_successful_button_font_color,
                                    'mobile_confirmation_successful_button_border_color' => $mobile_confirmation_successful_button_border_color,
                                    'mobile_confirmation_successful_button_bg_color' => $mobile_confirmation_successful_button_bg_color,
                                    'mobile_confirmation_successful_font_color' => $mobile_confirmation_successful_font_color,
                                    'mobile_confirmation_successful_bg_color' => $mobile_confirmation_successful_bg_color,
                                    'mobile_confirmation_successful_message' => $mobile_confirmation_successful_message
                            )
                        );
                        
                    } catch (\Exception $e) {
                        $name = Setting::where('key', 'company_name')->first()->value;
                        $mobile_confirmation_unsuccessful_image = Setting::where('key', 'mobile_confirmation_unsuccessful_image')->first()->value;
                        $mobile_confirmation_unsuccessful_button_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_font_color')->first()->value;
                        $mobile_confirmation_unsuccessful_button_border_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_border_color')->first()->value;
                        $mobile_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_bg_color')->first()->value;
                        $mobile_confirmation_unsuccessful_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_font_color')->first()->value;
                        $mobile_confirmation_unsuccessful_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_bg_color')->first()->value;
                        // Log::error("confirm 4061");
                        return View::make('mobile.confirmation-unsuccessful', 
                            array(  'company_logo' => $mobile_confirmation_unsuccessful_image, 
                                    'company_name' => $name,
                                    'mobile_confirmation_unsuccessful_button_font_color' => $mobile_confirmation_unsuccessful_button_font_color,
                                    'mobile_confirmation_unsuccessful_button_border_color' => $mobile_confirmation_unsuccessful_button_border_color,
                                    'mobile_confirmation_unsuccessful_button_bg_color' => $mobile_confirmation_unsuccessful_button_bg_color,
                                    'mobile_confirmation_unsuccessful_font_color' => $mobile_confirmation_unsuccessful_font_color,
                                    'mobile_confirmation_unsuccessful_bg_color' => $mobile_confirmation_unsuccessful_bg_color,
                                    'mobile_confirmation_unsuccessful_message' => $e->getMessage()
                            )
                        );
                    }

                } else {
                    $name = Setting::where('key', 'company_name')->first()->value;
                    $mobile_confirmation_unsuccessful_image = Setting::where('key', 'mobile_confirmation_unsuccessful_image')->first()->value;
                    $mobile_confirmation_unsuccessful_button_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_font_color')->first()->value;
                    $mobile_confirmation_unsuccessful_button_border_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_border_color')->first()->value;
                    $mobile_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_bg_color')->first()->value;
                    $mobile_confirmation_unsuccessful_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_font_color')->first()->value;
                    $mobile_confirmation_unsuccessful_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_bg_color')->first()->value;
                    // Log::error("confirm 4061");
                    return View::make('mobile.confirmation-unsuccessful', 
                        array(  'company_logo' => $mobile_confirmation_unsuccessful_image, 
                                'company_name' => $name,
                                'mobile_confirmation_unsuccessful_button_font_color' => $mobile_confirmation_unsuccessful_button_font_color,
                                'mobile_confirmation_unsuccessful_button_border_color' => $mobile_confirmation_unsuccessful_button_border_color,
                                'mobile_confirmation_unsuccessful_button_bg_color' => $mobile_confirmation_unsuccessful_button_bg_color,
                                'mobile_confirmation_unsuccessful_font_color' => $mobile_confirmation_unsuccessful_font_color,
                                'mobile_confirmation_unsuccessful_bg_color' => $mobile_confirmation_unsuccessful_bg_color,
                                'mobile_confirmation_unsuccessful_message' => "You are already confirmed."
                        )
                    );
                }
            }

        } catch (\Exception $e) {
            Log::error($e);

            $name = Setting::where('key', 'company_name')->first()->value;
            $mobile_confirmation_unsuccessful_image = Setting::where('key', 'mobile_confirmation_unsuccessful_image')->first()->value;
            $mobile_confirmation_unsuccessful_button_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_font_color')->first()->value;
            $mobile_confirmation_unsuccessful_button_border_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_border_color')->first()->value;
            $mobile_confirmation_unsuccessful_button_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_button_bg_color')->first()->value;
            $mobile_confirmation_unsuccessful_font_color = Setting::where('key', 'mobile_confirmation_unsuccessful_font_color')->first()->value;
            $mobile_confirmation_unsuccessful_bg_color = Setting::where('key', 'mobile_confirmation_unsuccessful_bg_color')->first()->value;
            // Log::error("confirm 4061");
            return View::make('mobile.confirmation-unsuccessful', 
                array(  'company_logo' => $mobile_confirmation_unsuccessful_image, 
                        'company_name' => $name,
                        'mobile_confirmation_unsuccessful_button_font_color' => $mobile_confirmation_unsuccessful_button_font_color,
                        'mobile_confirmation_unsuccessful_button_border_color' => $mobile_confirmation_unsuccessful_button_border_color,
                        'mobile_confirmation_unsuccessful_button_bg_color' => $mobile_confirmation_unsuccessful_button_bg_color,
                        'mobile_confirmation_unsuccessful_font_color' => $mobile_confirmation_unsuccessful_font_color,
                        'mobile_confirmation_unsuccessful_bg_color' => $mobile_confirmation_unsuccessful_bg_color,
                        'mobile_confirmation_unsuccessful_message' => $e->getMessage()
                )
            );
        }
    }
    
    /**
     * Send a verification code via sms
     *
     * @param SMSProvider $sms
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendVerificationCode(SMSProvider $sms)
    {
        try {
            $user = JWTAuth::parseToken()->toUser();

            if (is_null($user->mobile)) {
                return response()->json(['status' => 'error', 'message' => 'Mobile not found.'], Response::HTTP_BAD_REQUEST);
            } else {
                if ($user->mobile_verification) {
                    return response()->json(['status' => 'error', 'message' => 'Your Mobile is already verified.'], Response::HTTP_BAD_REQUEST);
                } else {
                    // Generate 6 random number
                    $code = mt_rand(100000, 999999);

                    $user->sms_verification_code = $code;

                    $user->save();

                    $sms->sendSMS($user->mobile, "Your verification code: " . $code);

                    return response()->json(['status' => 'ok', 'message' => 'Verification code sent successful.', 'code' => $code]);

                }
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Verify sms code
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function verifyCode(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'code' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $code = $request->input('code');

            $user = JWTAuth::parseToken()->toUser();

            if ($user->mobile_verification) {
                return response()->json(['status' => 'error', 'message' => 'Your mobile is already verified.'], Response::HTTP_BAD_REQUEST);
            } else {
                if ($user->sms_verification_code == $code) {
                    $user->mobile_verification = true;

                    $user->save();

                    return response()->json(['status' => 'ok', 'message' => 'Code is valid.']);

                } else {
                    return response()->json(['status' => 'error', 'message' => 'invalid_code'], Response::HTTP_BAD_REQUEST);
                }
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Unsubscribe friend referral
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function unsubscribe($token)
    {
        try {
            $data = FriendReferral::where('token', $token)->first();

            if (is_null($data)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, "message" => 'Invalid Token'));
            } else {
                $data->delete();

                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('success.success', array(
                    "message" => 'WE’RE SORRY TO SEE YOU GO. You’ve been successfully unsubscribed from Vectron’s email newsletter.',
                    'company_logo' => $invoice_logo
                ));
            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, "message" => $e->getMessage()));
        }
    }

    /**
     * Unsubscribe mail (mailing preference) (from welcome screen)
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function unsubscribeMail($token)
    {
        try {
            $member = Member::where('token', $token)->first();

            if (is_null($member)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, "message" => 'Invalid Token'));
            } else {
                $member->mailing_preference = 'unsubscribed';
                $member->save();

                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;


                return View::make('success.success', array(
                    "message" => 'You’ve been successfully unsubscribed from Vectron’s email newsletter.',
                    'company_logo' => $invoice_logo
                ));
            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, "message" => $e->getMessage()));
        }
    }

    /**
     * Show the friend referral form for the given user.
     * >> Check the token first.
     *
     * @param Request $request
     * @param $token
     * @return mixed
     */
    protected function showFriendReferralForm(Request $request, $token)
    {
        try {
            $fr = FriendReferral::where('has_joined', false)
                ->where('new_member_id', 0)
                ->whereDate('expiry_date', '>=', Carbon::now(config('app.timezone'))->toDateTimeString())
                ->where('token', $token)
                ->first();

            if (is_null($fr)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            } else {
                $fr->accepted_at = Carbon::now(config('app.timezone'));
                $fr->is_accepted = true;
                $fr->save();

                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                $platform = Platform::where('name', 'web')->first();
                $name = Setting::where('key', 'company_name')->first()->value;
                // $enable_terms_condition_field = Setting::where('key', 'enable_terms_condition_field')->first()->value;
                $email_not_allowed_domains = Setting::where('key', 'email_not_allowed_domains')->first();
                $member_default_tier = Setting::where('key', 'member_default_tier')->first();
                $default_tier = Setting::where('key', 'default_tier')->first();
                $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first();                
                $form_input_style = Setting::where('key', 'form_input_style')->first();
                $form_floating_title = Setting::where('key', 'form_floating_title')->first();

                $temp = \GuzzleHttp\json_decode($bepozcustomfield->extended_value, true);

                // filter need dispaly in app
                foreach ($temp as $key => $cf) {
                    if (!$cf['displayInApp']) {
                        unset($temp[$key]);
                        // Log::info('displayInApp');
                    }
                }

                $bepozcustomfield_enable = Setting::where('key', 'bepozcustomfield_enable')->first()->value;
                if ($bepozcustomfield_enable == "true") {
                    $bepozcustomfield->extended_value = array_values($temp);
                } else {
                    foreach ($temp as $key => $cf) {
                            unset($temp[$key]);
                    }
                }

                if ( !is_array($bepozcustomfield->extended_value) ){
                    $bcf = \GuzzleHttp\json_decode($bepozcustomfield->extended_value, true);
                } else {
                    $bcf = $temp;
                }

                foreach ($bcf as $key => $cf) {
                    // Log::info($cf);
                }


                $tiers = Tier::where('is_active', true)
                    ->where('display', true)
                    ->get();

                foreach ($tiers as $tier) {
                    if (intval($tier->venue_id) > 0) {
                        $tier->venue;
                    } else {
                        $tier->venue = collect(
                            [
                                'id' => 0,
                                'name' => Setting::where('key', 'company_name')->first()->value,
                                'ios_link' => Setting::where('key', 'ios_link')->first()->extended_value,
                                'android_link' => Setting::where('key', 'android_link')->first()->extended_value
                            ]);
                    }
                }

                // Log::info($tiers);

                // $container = array();
                // foreach ($tiers as $tier) {
                //     if (intval($tier->venue_id) > 0) {
                //         $container[$tier->id] = $tier->venue->name;
                //     } else {
                //         $container[$tier->id] = $name;
                //     }

                //     $container[$tier->id] = $tier->name;
                // }

                $venues = Venue::where('active', true)->get();
                $container = array();
                
                foreach ($venues as $venue) {
                    $container[$venue->id] = $venue->name;
                }

                $payload = array(
                    'url' => config('bucket.APP_URL') . '/api/confirmReferral/' . $token,
                    'token' => $token,
                    'app_token' => $platform->app_token,
                    'app_secret' => $platform->app_secret,
                    'email' => $fr->email,
                    'company_logo' => $invoice_logo,
                    'venues' => $container,
                    'terms' => config('bucket.APP_URL') . '/terms',
                    'policy' => config('bucket.APP_URL') . '/policy',
                    'company_name' => $name,
                    'tiers' => $tiers,
                    // 'enable_terms_condition_field' => $enable_terms_condition_field,
                    'email_not_allowed_domains' => $email_not_allowed_domains,
                    'member_default_tier_option' => $member_default_tier->value,
                    'member_default_tier_detail' => \GuzzleHttp\json_decode($member_default_tier->extended_value, true),
                    'default_tier' => $default_tier->value,
                    'form_input_style' => $form_input_style->value,
                    'form_floating_title' => $form_floating_title->value,
                    'bepozcustomfield' => $bcf
                );

                // return View::make('form.newuser', $payload);
                return View::make('form.newuserimproved', $payload);
                
            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));
        }

    }

    /**
     * Show the unsubscribe form for the given user. (from welcome screen)
     * >> Check the token first.
     *
     * @param Request $request
     * @param $token
     * @return mixed
     */
    protected function showUnsubscribeForm(Request $request, $token)
    {
        try {
            $member = Member::where('token', $token)->first();

            if (is_null($member)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            } else {

                if ($member->mailing_preference === 'unsubscribed') {
                    $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                    return View::make('success.success', array('company_logo' => $invoice_logo, 'message' => 'You have un-subscribed from us. Thank you.'));
                }

                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                $platform = Platform::where('name', 'web')->first();
                return View::make('form.unsubscribe', array(
                    'url' => config('bucket.APP_URL') . '/api/unsubscribeMail/' . $token,
                    'token' => $token,
                    'app_token' => $platform->app_token,
                    'app_secret' => $platform->app_secret,
                    'email' => $member->user->email,
                    'company_logo' => $invoice_logo
                ));

            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));
        }

    }

    /**
     * Show the unsubscribe form for the given user. (from friend referral)
     * >> Check the token first.
     *
     * @param Request $request
     * @param $token
     * @return mixed
     */
    protected function showUnsubscribeFFForm(Request $request, $token)
    {
        try {
            $data = FriendReferral::where('token', $token)->first();

            if (is_null($data)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            } else {

                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                $platform = Platform::where('name', 'web')->first();
                return View::make('form.unsubscribe', array(
                    'url' => config('bucket.APP_URL') . '/api/unsubscribe/' . $token,
                    'token' => $token,
                    'app_token' => $platform->app_token,
                    'app_secret' => $platform->app_secret,
                    'company_logo' => $invoice_logo
                ));

            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));
        }

    }

    protected function acceptFriendReferral(Request $request, $token, MandrillExpress $mx, Bepoz $bepoz)
    {
        try {
            $fr = FriendReferral::where('has_joined', false)
                ->where('new_member_id', 0)
                ->whereDate('expiry_date', '>=', Carbon::now(config('app.timezone'))->toDateTimeString())
                ->where('token', $token)
                ->first();

            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            if (is_null($fr)) {
                
                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            } else {
                $validator = Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required',
                    'password' => 'required',
                    'retry_password' => 'required'
                ]);

                if ($validator->fails()) {
                    $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                    return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $validator->messages()));
                }

                $share_info = 1;
                $email = preg_replace('/\s+/', '+', $request->input('email'));
                $password = $request->input('password');
                $first_name = $request->input('first_name');
                if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                    return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
                }

                $last_name = $request->input('last_name');
                if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                    return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
                }

                $mobile = $request->has('mobile') ? $request->input('mobile') : '';

                $customfieldpayload = [];
                $standardfieldpayload = [];
                $allInputpayload = [];
                // if ($bepozcustomfield_enable === 'true') {
                    // VALIDATE BEPOZ CUSTOM FIELD BASED ON SETTING
                    $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first()->extended_value;
                    // echo $bepozcustomfield."<br<";
                    $bepozcustomfielddecode = \GuzzleHttp\json_decode($bepozcustomfield);
                    // echo print_r($data, true)."<br>";
    
                    if (sizeof($bepozcustomfielddecode)) {
    
                        foreach ($bepozcustomfielddecode as $cf) {
                            // Log::info($cf);
                            // Log::info($cf->fieldName);
    
                                if (!$request->has($cf->id)) {
                                    return response()->json(['status' => 'error', 'message' => $cf->fieldName . ' is required.'], Response::HTTP_BAD_REQUEST);
                                }
        
                                if ( $bepoz->isCustomField($cf) ) {
                                    $customfieldpayload[] = array(
                                        'id' => $cf->id,
                                        'value' => $request->input($cf->id),
                                        'fieldName' => $cf->fieldName,
                                        'fieldType' => $cf->fieldType,
                                        'bepozFieldNum' => $cf->bepozFieldNum
                                    );
                                } else {
                                    $standardfieldpayload[] = array(
                                        'id' => $cf->id,
                                        'value' => $request->input($cf->id),
                                        'fieldName' => $cf->fieldName,
                                        'fieldType' => $cf->fieldType,
                                        'bepozFieldNum' => $cf->bepozFieldNum
                                    );
                                }
                
                                // $allInputpayload[] = array(
                                //     $cf->id => $request->input($cf->id)
                                // );
        
                                if ($cf->id == "password") {
                                    // NO NEED SAVE PASSWORD IN PAYLOAD
                                } else {
                                    $allInputpayload[$cf->id] = $request->input($cf->id);
                                }
                        }
                    }
                // }
    
                // Log::info($customfieldpayload);
    
    


                DB::beginTransaction();


                $user = User::where('email', $fr->email)
                    ->first();

                if (!is_null($user)) {

                    if ($user->member->status == 'active') {
                        $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                        return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Sorry, you have signed up with this email'));
                    } else {
                        $user->password = Hash::make($request->input('password'));
                        if ($request->has('mobile')) {
                            $user->mobile = $request->input('mobile');
                        }

                        $member = $user->member;
                        $member->first_name = $first_name;
                        $member->last_name = $last_name;
                        $member->status = 'active';
                        $member->share_info = 1;
                        $member->postcode = $request->has('postcode') ? $request->input('postcode') : null;

                        if ($request->has('dob')) {
                            if ($request->input('dob') != '') {
                                $member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                            } else {
                                $member->dob = null;
                            }
                        } else {
                            $member->dob = null;
                        }

                        $member->payload = $allInputpayload;
                        // $member->payload = \GuzzleHttp\json_encode($allInputpayload);
                        if ($request->has('opt_out_marketing')) {
                            $opt_out_marketing = $request->input('opt_out_marketing') == 'true' ? 1 : 0;
                            $member->opt_out_marketing = $opt_out_marketing;
                        }

                        $member->save();
                        $user->save();

                        $fr->new_member_id = $user->member->id;

                        FriendReferral::where('email', $user->email)
                            ->update(['new_member_id' => $user->member->id]);

                    }
                } else {
                    $user = new User();
                    $user->email = $fr->email;
                    $user->password = Hash::make($request->input('password'));
                    $user->mobile = $request->input('mobile');
                    $user->email_confirmation = true;
                    $user->email_confirmed_at = Carbon::now(config('app.timezone'));
                    $user->save();

                    $member = new Member();
                    $member->user_id = $user->id;
                    $member->first_name = $request->input('first_name');
                    $member->last_name = $request->input('last_name');
                    $member->current_preferred_venue_name = "";
                    $member->original_preferred_venue_name = "";
                    $member->status = 'active';
                    $member->share_info = 1;
                    $member->postcode = $request->has('postcode') ? $request->input('postcode') : null;

                    if ($request->has('dob')) {
                        if ($request->input('dob') != '') {
                            $member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                        } else {
                            $member->dob = null;
                        }
                    } else {
                        $member->dob = null;
                    }

                    $member->save();

                    $fr->new_member_id = $member->id;

                    $default_tier = Setting::where('key', 'default_tier')->first()->value;

                    if ($request->has('tier_id')) {

                        $tier = Tier::find($request->input('tier_id'));
                        if (is_null($tier)) {
                            // $tier = Tier::first();
                            $tier = Tier::find($default_tier);
                        }

                    } else {

                        // $tier = Tier::first();
                        $tier = Tier::find($default_tier);

                    }
                    // QUEENSBERRY FEATURE
                    // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                    // if ($default_member_expiry_date == "true"){
                    //     $setting = Setting::where('key', 'cut_off_date')->first();
                    //     $date_expiry = Carbon::parse($setting->value);
                    //     $date_expiry->setTimezone(config('app.timezone'));
                    //     $date_expiry->setTime(0, 0, 0);
                    // } else {
                    //     $date_expiry = null;
                    // }

                    $date_expiry = null;
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                    $user_role = new UserRoles;
                    $user_role->role_id = 3; // Manually add
                    $user_role->user_id = $user->id;
                    $user_role->save();

                    $venue_count = DB::table('venues')->count();

                    if ($venue_count > 0) {
                        $venue = Venue::find($tier->venue_id);
                        $member->current_preferred_venue = $tier->venue_id;
                        $member->current_preferred_venue_name = $venue->name;
                        $member->original_preferred_venue = $tier->venue_id;
                        $member->original_preferred_venue_name = $venue->name;
                    }
                    $member->share_info = 1;
                    $member->save();

                    FriendReferral::where('email', $user->email)
                        ->update(['new_member_id' => $member->id]);

                    $this->getReward($request, $user);

                    //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                    $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/register";
                    $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                    // $encoded = json_encode($data);
                    // $checksum = md5($encoded);

                    $email = $fr->email;
                    $first_name = $request->input('first_name');
                    $last_name = $request->input('last_name');
                    $mobile = $request->input('mobile');

                    $datasend = array(
                        'email' => $email,
                        'password' => $request->input('password'),
                        'name' => $first_name . " " . $last_name,
                        'given_name' => $first_name,
                        'family_name' => $last_name,
                        'phone_number' => $mobile,
                        "license_key" => $your_order_key
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $your_order_api);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                    $content = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    // Log::warning($content);
                    // Log::warning($httpCode);

                    if ($content) {
                        $payload = \GuzzleHttp\json_decode($content);
                        if ($payload->status == "error" && $payload->error == "user_not_found") {
                            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Email not found. Please contact administrator.'));
                        }
                        if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Unrecognized License. Please contact administrator.'));
                        }

                        $explode = explode('.', $payload->data->id_token);
                        $decoded_id_token = base64_decode($explode[1]);
                        $decoded = json_decode($decoded_id_token);

                        $member->login_token = $decoded->guid;
                        $member->save();

                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Unable to connect to SSO server'], Response::HTTP_BAD_REQUEST);
                    }

                    // $user = User::find($user->id); // refresh...
                    // $token = JWTAuth::fromUser($user);
                    // //NOT REQUIRED TO RESET PASSWORD
                    // $reset_password = 0;

                    // return response()->json(['status' => 'ok', 'reset_password' => $reset_password, 'token' => $token,
                    //     'id_token' => $payload->data->id_token, 'access_token' => $payload->data->access_token,
                    //     'refresh_token' => $payload->data->refresh_token ]);

                }

                $fr->has_joined = true;
                $fr->joined_at = Carbon::now(config('app.timezone'));
                $fr->save();


                $data = array(
                    "member_id" => $user->member->id,
                    "type" => 'email'
                );

                $job = new BepozJob();
                $job->queue = "sync-account";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();

                dispatch(new SendWelcomeEmail($user));

                DB::commit();

                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                $android_link = Setting::where('key', 'android_link')->first()->extended_value;

                return View::make('success.successwithdownloadlink', array(
                    "message" => 'Signup successful. Please download the app and Enjoy!',
                    'company_logo' => $invoice_logo, 'ios' => $ios_link, 'android' => $android_link
                ));
            }

        } catch (\Exception $e) {
            Log::error("acceptFriendReferral 7631");
            Log::error($e);
            DB::rollBack();
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));
        }
    }

    private function getReward($request, $user)
    {
        $option = Setting::where('key', 'reward_after_signup_option')->first();
        if (is_null($option)) {
            return false;
        }

        if ($option->value == 'false') {
            return false;
        }

        $type = Setting::where('key', 'reward_after_signup_type')->first();
        if (is_null($type)) {
            return false;
        }

        if ($type->value == 'point') {
            return false;
        }

        $voucher = Setting::where('key', 'reward_after_signup_voucher_setups_id')->first();
        if (is_null($voucher)) {
            return false;
        }

        $voucherSetup = VoucherSetups::find($voucher->value);
        if (is_null($voucherSetup)) {
            return false;
        }

        $request->offsetSet('time', time());
        $request->offsetSet('transaction_type', 'signup_reward');
        $payload = \GuzzleHttp\json_encode($request->all());
        $token = md5($payload);

        $Order = new Order();
        $Order->type = "cash";
        $Order->expired = time();
        $Order->member_id = $user->member->id;
        $Order->ordered_at = Carbon::now(config('app.timezone'));
        $Order->voucher_status = 'successful';
        $Order->payment_status = 'successful';
        $Order->order_status = 'confirmed';
        $Order->payload = $payload;
        $Order->token = $token;
        $Order->ip_address = '0.0.0.0';
        $Order->transaction_type = 'signup_reward';
        $Order->save();

        $od = new OrderDetail();
        $od->voucher_name = $voucherSetup->name;
        $od->product_name = $voucherSetup->name;
        $od->voucher_setups_id = $voucherSetup->id;
        $od->order_id = $Order->id;
        $od->qty = 1;
        $od->product_type_id = 1;
        $od->category = 'signup_reward';
        $od->status = 'successful';
        $od->save();

        $memberVoucher = new MemberVouchers();
        $memberVoucher->name = $voucherSetup->name;
        $memberVoucher->member_id = $user->member->id;;
        $memberVoucher->lookup = "100000000";
        $memberVoucher->barcode = "";
        $memberVoucher->order_id = $Order->id;
        $memberVoucher->order_details_id = $od->id;
        $memberVoucher->voucher_setup_id = $voucherSetup->id;
        $memberVoucher->amount_left = 100;
        $memberVoucher->amount_issued = 100;
        $memberVoucher->category = 'signup_reward';
        $memberVoucher->save();

        // ----------------------

        // bepoz job - issue voucher
        $data = array(
            "member_id" => $user->member->id,
            "member_voucher_id" => $memberVoucher->id,
            "voucher_setups_id" => $voucherSetup->id,
        );

        $job = new BepozJob();
        $job->queue = "issue-signup-reward-bepoz-voucher";
        $job->payload = \GuzzleHttp\json_encode($data);
        $job->available_at = time();
        $job->created_at = time();
        $job->save();

        $company_name = Setting::where('key', 'company_name')->first()->value;

        $data = [
            // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
            "contents" => ["en" => "Please check your voucher section, you get a complement!"],
            "headings" => ["en" => $company_name],
            "data" => ["category" => "email"],
            "ios_badgeType" => "Increase",
            "ios_badgeCount" => 1
        ];

        $job = (new SendOneSignalNotification($data))->delay(360);
        dispatch($job);

        $ml = new MemberLog();
        $ml->member_id = $user->member->id;
        $ml->message = "This member got sign-up rewards ($voucherSetup->name). ";
        $ml->after = \GuzzleHttp\json_encode([
            'voucher_setups_id' => $voucherSetup->id,
            'voucher_name' => $voucherSetup->name
        ]);
        $ml->save();

    }

    protected function privacyPolicy()
    {
        try {
            $pp = Setting::where('key', 'privacy_policy')->first();

            return $pp->extended_value;
        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));
        }
    }

    protected function terms()
    {
        try {
            $tc = Setting::where('key', 'terms_and_conditions')->first();

            return $tc->extended_value;
        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));
        }
    }

    protected function accountSearch(Request $request, Bepoz $bepoz)
    {
        try {
            $data = array();


            if ($request->has('AccountID')) {

                $data['AccountID'] = $request->input('AccountID');
            }

            if ($request->has('AccNumber')) {
                $data['AccNumber'] = $request->input('AccNumber');
            }

            if ($request->has('CardNumber')) {
                $data['CardNumber'] = $request->input('CardNumber');
            }

            // Log::info($data);

            $result = $bepoz->AccountFullGet($data);

            // Log::info($result);
            // Log::info($result['AccountFull']['Email1st']);

            if ($result) {
                if (!isset($result['AccountFull'])) {
                    return response()->json(['status' => 'error', 'message' => 'No Response From Bepoz'], Response::HTTP_BAD_REQUEST);
                }

                $user = User::with('member')->where('email', $result['AccountFull']['Email1st'])->first();

                if (is_null($user)) {

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Email address is already registered.'], Response::HTTP_BAD_REQUEST);
                }

                if ($result['AccountFull']['AccNumber'] != $request->input('AccNumber')) {
                    return response()->json(['status' => 'error', 'message' => 'Invalid Account Number and or Account ID.'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'Unable to connect to Bepoz', 'result' => $result], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'data' => $result, 'timestamp' => $request->input('timestamp'), 'request' => $request->all()]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    protected function accountSearchEmailMobile(Request $request, Bepoz $bepoz)
    {
        try {
            $data = array();

            if ($request->has('email')) {
                $email = $request->input('email');
            }

            if ($request->has('mobile')) {
                $mobile = $request->input('mobile');
            }

            // Log::info($data);

            $checkbepoz = $bepoz->AccountSearch(8, $email);

            if ($checkbepoz) {
                if ($checkbepoz['IDList']['Count'] == 0) {
                    return response()->json(['status' => 'error', 'message' => 'No account found Bepoz' ], Response::HTTP_BAD_REQUEST);
                } else if ($checkbepoz['IDList']['Count'] == 1) {
                    $bepozID = $checkbepoz['IDList']['ID'];
                } else {
                    return response()->json(['status' => 'error', 'message' => 'More than 1 account found in Bepoz' ], Response::HTTP_BAD_REQUEST);
                }
            }

            if ($bepozID) {
                $result = $bepoz->AccountFullGet($bepozID);

                // Log::info($result);
                // Log::info($result['AccountFull']['Email1st']);

                if ($result) {
                    if (!isset($result['AccountFull'])) {
                        return response()->json(['status' => 'error', 'message' => 'No Response From Bepoz'], Response::HTTP_BAD_REQUEST);
                    }

                    $user = User::with('member')->where('email', $result['AccountFull']['Email1st'])->first();

                    if (is_null($user)) {

                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Email address is already registered.'], Response::HTTP_BAD_REQUEST);
                    }

                    if ($result['AccountFull']['Mobile'] != $mobile) {
                        return response()->json(['status' => 'error', 'message' => 'Invalid email and or mobile.'], Response::HTTP_BAD_REQUEST);
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Unable to connect to Bepoz', 'result' => $result], Response::HTTP_BAD_REQUEST);
                }
            }

            return response()->json(['status' => 'ok', 'data' => $result, 'timestamp' => $request->input('timestamp'), 'request' => $request->all()]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    protected function instruction(Request $request)
    {
        try {
            $get_started = Setting::where('key', 'instruction_lets_get_started')->first()->extended_value;
            $match_email_mobile = Setting::where('key', 'instruction_match_email_mobile')->first()->extended_value;
            $match_bepoz_id_number = Setting::where('key', 'instruction_match_bepoz_id_number')->first()->extended_value;
            $resend_detail = Setting::where('key', 'instruction_resend_details')->first()->extended_value;

            return response()->json(['status' => 'ok', 'get_started' => $get_started, 'match_email_mobile' => $match_email_mobile, 
                'match_bepoz_id_number' => $match_bepoz_id_number, 'resend_detail' => $resend_detail]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function resendDetailsEmail(Request $request, Bepoz $bepoz, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|max:155'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));
            $unableToConnect = 'We are having a few problems connecting, please try again.';
            // search Bepoz Account from email
            $resultEmail = $bepoz->accountSearch(8, $email);

            if ($resultEmail) {
                if (array_key_exists('IDList', $resultEmail)) {
                    $resultEmail = $resultEmail['IDList'];
                    if (array_key_exists('Count', $resultEmail)) {
                        if (intval($resultEmail['Count']) == 1) {
                            $result = $bepoz->AccountFullGet($resultEmail['ID']['int']);
                        } else if (intval($resultEmail['Count']) > 1) {
                            return response()->json(['status' => 'error', 'message' => 'Duplicate email detected'], Response::HTTP_BAD_REQUEST);
                        } else {
                            return response()->json(['status' => 'error', 'message' => config('bucket.EMAIL_NOT_FOUND')], Response::HTTP_BAD_REQUEST);
                        }
                    }
                }
            } else {
                return response()->json(['status' => 'error', 'message' => $unableToConnect], Response::HTTP_BAD_REQUEST);
            }

            // Log::info($result);
            // Log::info($result['AccountFull']['Email1st']);

            if ($result) {
                $user = User::with('member')->where('email', $result['AccountFull']['Email1st'])->first();

                if (is_null($user)) {

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Email address is already registered.'], Response::HTTP_BAD_REQUEST);
                }

                $member_account_id = Member::where('bepoz_account_id', $result['AccountFull']['AccountID'])->first();
                if (is_null($member_account_id)) {
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Bepoz Account ID is already registered.'], Response::HTTP_BAD_REQUEST);
                }

                $member_account_number = Member::where('bepoz_account_number', $result['AccountFull']['AccNumber'])->first();
                if (is_null($member_account_number)) {
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Bepoz Account Number is already registered.'], Response::HTTP_BAD_REQUEST);
                }

                $user_mobile = User::where('mobile', $result['AccountFull']['Mobile'])->first();
                if (is_null($user_mobile)) {

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Mobile is already registered.'], Response::HTTP_BAD_REQUEST);
                }

            } else {
                return response()->json(['status' => 'error', 'message' => $unableToConnect], Response::HTTP_BAD_REQUEST);
            }

            $name = $result['AccountFull']['FirstName'] . " " . $result['AccountFull']['LastName'];
            $data = array(
                'NAME' => $name,
                'ACC_ID' => $resultEmail['ID']['int'],
                'ACC_NUMBER' => $result['AccountFull']['AccNumber']
            );

            // Log::info($data);

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('resend_membership_details', $name, $email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => 'resend_membership_details',
                        'name' => $name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('resend_membership_details', $name, $email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => 'resend_membership_details',
                        'name' => $name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            return response()->json(['status' => 'ok', 'data' => $result, 'timestamp' => $request->input('timestamp'), 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function resendDetailsSMS(Request $request, Bepoz $bepoz)
    {
        try {
            $validator = Validator::make($request->all(), [
                'mobile' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $unableToConnect = 'We are having a few problems connecting, please try again.';
            // SEARCH BEPOZ USER ANY PHONE NUMBER WITH MOBILE
            $resultAnyMobile = $bepoz->accountSearch(6, $request->input('mobile'));

            if ($resultAnyMobile) {
                // Log::info($resultAnyMobile);
                if (array_key_exists('IDList', $resultAnyMobile)) {
                    $resultAnyMobile = $resultAnyMobile['IDList'];
                    if (array_key_exists('Count', $resultAnyMobile)) {
                        if (intval($resultAnyMobile['Count']) == 1) {
                            $result = $bepoz->AccountFullGet($resultAnyMobile['ID']['int']);
                        } else if (intval($resultAnyMobile['Count']) > 1) {
                            return response()->json(['status' => 'error', 'message' => 'Duplicate mobile detected'], Response::HTTP_BAD_REQUEST);
                        } else {
                            return response()->json(['status' => 'error', 'message' => config('bucket.MOBILE_NOT_FOUND')], Response::HTTP_BAD_REQUEST);
                        }
                    }
                }
            } else {
                return response()->json(['status' => 'error', 'message' => $unableToConnect], Response::HTTP_BAD_REQUEST);
            }

            // Log::info($result);
            // Log::info($result['AccountFull']['Email1st']);

            if ($result) {
                $user = User::with('member')->where('email', $result['AccountFull']['Email1st'])->first();

                if (is_null($user)) {

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Email address is already registered.'], Response::HTTP_BAD_REQUEST);
                }

                $member_account_id = Member::where('bepoz_account_id', $result['AccountFull']['AccountID'])->first();
                if (is_null($member_account_id)) {
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Bepoz Account ID is already registered.'], Response::HTTP_BAD_REQUEST);
                }

                $member_account_number = Member::where('bepoz_account_number', $result['AccountFull']['AccNumber'])->first();
                if (is_null($member_account_number)) {
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Bepoz Account Number is already registered.'], Response::HTTP_BAD_REQUEST);
                }

                $user_mobile = User::where('mobile', $result['AccountFull']['Mobile'])->first();
                if (is_null($user_mobile)) {

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Mobile is already registered.'], Response::HTTP_BAD_REQUEST);
                }

            } else {
                return response()->json(['status' => 'error', 'message' => $unableToConnect], Response::HTTP_BAD_REQUEST);
            }

            // $name = $result['AccountFull']['FirstName']." ".$result['AccountFull']['LastName'];
            $sms_resend_details_message = Setting::where('key', 'sms_resend_details_message')->first()->value;

            $message = $sms_resend_details_message .
                "\n Acc ID = " . $resultAnyMobile['ID']['int'] .
                "\n Acc Number = " . $result['AccountFull']['AccNumber'];

            // Log::info($message);
            // $this->sendSMS($request->input('mobile'), $message);
            // $api = new \App\Helpers\TransmitSMSAPI();
            // $result = $api->sendSms($message, $request->input('mobile'), "");


            return response()->json(['status' => 'ok', 'message' => 'SMS sent', 'timestamp' => $request->input('timestamp'), 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * sendSMS through BurstSMS
     */
    protected function sendSMS($mobile, $message)
    {
        try {
            $smsFrom = Setting::where('key', 'sms_from')->first()->value;
        } catch (\Exception $e) {
            $smsFrom = "Myplace";
        }

        $sms_mobile = preg_replace('/^04/', '614', $mobile);
        $sms_mobile = preg_replace('/^\+/', '', $sms_mobile);

        $api = new \App\Helpers\TransmitSMSAPI();
        $result = $api->sendSms($message, $sms_mobile, substr(preg_replace("/\W/", "", $smsFrom), 0, 11));

    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
