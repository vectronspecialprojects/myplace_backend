<?php

namespace App\Http\Controllers;

use App\BepozFailedTransaction;
use App\BepozJob;
use App\BepozPushedAccountLog;
use App\BepozPushedPointsLog;
use App\BepozPushedVouchersLog;
use App\BepozTransaction;
use App\Member;
use App\MemberVouchers;
use App\PointLog;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use App\Helpers\Bepoz;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BepozController extends Controller
{
    /**
     * List all Bepoz Voucher Setups
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $setups = VoucherSetups::where('inactive', false)
                ->get();

            return response()->json(['status' => 'ok', 'data' => $setups]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Sync Bepoz Accounts manually
     *
     * @param Bepoz $bepoz
     * @return \Illuminate\Http\JsonResponse
     */
    protected function syncAccount(Bepoz $bepoz)
    {

        try {

            // $members = Member::all();

            // foreach ($members as $member) {
            //     $member = Member::find($member->id);
            //     $member->mode = "update_account";

            //     $job = new BepozJob;
            //     $job->queue = "sync-account";
            //     $job->payload = $member;
            //     $job->available_at = time();
            //     $job->created_at = time();
            //     $job->save();
            // }

            return response()->json(['status' => 'ok', 'message' => 'Syncing account request has been made. Syncing in background.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }


    }

    /**
     * Sync Bepoz Voucher Setups manually
     *
     * @param Bepoz $bepoz
     * @return \Illuminate\Http\JsonResponse
     */
    protected function voucherSetups(Bepoz $bepoz)
    {
        $result = '';
        try {
            $result = $bepoz->IDListGet(13); // issue voucher

            if (!$result) {
                throw new \Exception('bepoz_not_connected');
            }

            if (intval($result['IDList']['Count']) > 0) {
                for ($i = 0; $i < count($result['IDList']['ID']['int']); $i++) {

                    $voucherSetupInformation = $bepoz->VoucherSetupGet($result['IDList']['ID']['int'][$i]);
                    $voucherSetup = VoucherSetups::where('bepoz_voucher_setup_id', '=', $result['IDList']['ID']['int'][$i])->first();

                    if (is_null($voucherSetup)) {
                        // create
                        $voucherSetup = new VoucherSetups;
                        $voucherSetup->bepoz_voucher_setup_id = $result['IDList']['ID']['int'][$i];
                        $voucherSetup->name = $result['IDList']['Name']['string'][$i];
                        $voucherSetup->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                        $voucherSetup->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                        $voucherSetup->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                        $voucherSetup->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                        $voucherSetup->expiry_date->setTimezone(config('app.timezone'));
                        $voucherSetup->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                        $voucherSetup->save();
                    } else {
                        // update
                        $voucherSetup->name = $result['IDList']['Name']['string'][$i];
                        $voucherSetup->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                        $voucherSetup->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                        $voucherSetup->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                        $voucherSetup->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                        $voucherSetup->expiry_date->setTimezone(config('app.timezone'));
                        $voucherSetup->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                        $voucherSetup->save();
                    }

                }
            }

            return response()->json(['status' => 'ok']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }


    protected function getAccountFlag(Request $request)
    {
        try {
            $bpal = new BepozPushedAccountLog();
            $bpal->payload = $request->getContent();

            $acc = \GuzzleHttp\json_decode($request->getContent());
            $member = Member::where('bepoz_account_id', $acc->AccountID)->first();
            if (!is_null($member)) {

                $job = BepozJob::where('queue', 'sync-point-balance')->where('payload', $member->bepoz_account_id)->first();
                if (is_null($job)) {
                    $job = new BepozJob();
                    $job->queue = "sync-point-balance";
                    $job->payload = $member->bepoz_account_id;
                    $job->available_at = time();
                    $job->created_at = time();
                    $job->save();
                }

                $bpal->has_been_processed = true;
                $bpal->processing_time = Carbon::now(config('app.timezone'));
            }

            $bpal->save();
            return 'ok';
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    protected function getRedeemedVoucherStatus(Request $request)
    {
        try {
            $bpvl = new BepozPushedVouchersLog();
            $bpvl->payload = $request->getContent();

            $voucher = \GuzzleHttp\json_decode($request->getContent());
            $member_voucher = MemberVouchers::where('voucher_id', $voucher->VoucherID)
                ->where('lookup', $voucher->Lookup)->first();

            if (!is_null($member_voucher)) {
                if (intval($voucher->UsedTransID) !== 0) {
                    $member_voucher->amount_left = $voucher->AmountLeft;
                    $member_voucher->used_value = $voucher->UsedValue;
                    $member_voucher->used_trans_id = $voucher->UsedTransID;
                    $member_voucher->used_count = $voucher->UsedCount;
                    $member_voucher->redeemed = intval($voucher->AmountLeft) === 0 ? true : false;
                    $member_voucher->used_date = new Carbon($voucher->UsedDate, config('app.timezone'));
                    $member_voucher->save();

                    $bpvl->processing_time = Carbon::now(config('app.timezone'));
                    $bpvl->has_been_processed = true;
                }
            }

            $bpvl->save();
            return 'ok';
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    protected function getPointUpdates(Request $request)
    {
        try {
            $bppl = new BepozPushedPointsLog();
            $bppl->payload = $request->getContent();

            $transaction = \GuzzleHttp\json_decode($request->getContent());
            $member = Member::where('bepoz_account_id', $transaction->AccountID)->first();
            if (!is_null($member)) {

                // Log
                $pl = new PointLog();
                $pl->member_id = $member->id;
                $pl->points_before = $member->points;
                $pl->points_after = floatval($member->points) + (floatval($transaction->TotalPoints) / 100);
                $pl->desc_short = "Point rewards gained by buying products from bepoz PC.";
                $pl->desc_long = "Received " . (floatval($transaction->TotalPoints) / 100) . " points from Bepoz PC.";
                $pl->save();

                $member->points = floatval($member->points) + (floatval($transaction->TotalPoints) / 100);
                $member->save();

                $bppl->has_been_processed = true;
                $bppl->processing_time = Carbon::now(config('app.timezone'));
            }

            $bppl->save();
            return 'ok';
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    protected function getRunningJobs()
    {
        try {
            $bepoz_jobs = BepozJob::all();

            return response()->json(['status' => 'ok', 'data' => $bepoz_jobs]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Sync Bepoz Voucher Setups manually
     *
     * @param Bepoz $bepoz
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bepozGroups(Bepoz $bepoz)
    {
        try {
            $result = $bepoz->IDListGet(4); // issue voucher

            if (!$result) {
                throw new \Exception('bepoz_not_connected');
            }

            $parents = [];
            if (intval($result['IDList']['Count']) > 0) {
                for ($i = 0; $i < count($result['IDList']['ID']['int']); $i++) {
                    $child = [];
                    $child['id'] = $result['IDList']['ID']['int'][$i];
                    $child['name'] = $result['IDList']['Name']['string'][$i];
                    $parents[] = $child;
                }
            }

            return response()->json(['status' => 'ok', 'data' => $parents]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    protected function bepozAccountGet(Request $request, Bepoz $bepoz)
    {
        try {
            $data = array();

            if ($request->has('AccNumber')) {
                $data['AccNumber'] = $request->input('AccNumber');
            }

            if ($request->has('CardNumber')) {
                $data['CardNumber'] = $request->input('CardNumber');
            }

            if ($request->has('Email')) {
                $data['Email'] = $request->input('Email');
            }

            $result = $bepoz->AccountGet($data);

            if ($result) {
                $result = $bepoz->AccountFullGet($result['Account']['AccountID']);
            }

            return response()->json(['status' => 'ok', 'data' => $result, 'timestamp' => $request->input('timestamp'), 'request' => $request->all()]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * List all Bepoz Prize Promotions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function prizePromotions()
    {
        try {
            $setups = PrizePromotion::where('inactive', false)->get();

            return response()->json(['status' => 'ok', 'data' => $setups]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get Bepoz Version
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function versionGet(Bepoz $bepoz)
    {
        try {
            $result = $bepoz->VersionGet();

            if ($result) {
                return response()->json(['status' => 'ok', 'data' => $result]);
            } else {
                return response()->json(['status' => 'error', 'message' => "Bepoz Connection Problem"], HttpResponse::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * bepoz Products Search
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bepozProductsSearch(Request $request, Bepoz $bepoz)
    {
        try {
            $data = array();

            if ($request->has('product_number')) {
                $data = $request->input('product_number');
            }
            
            $resultSearch = $bepoz->ProductsSearch($data);

            $productID = 0;

            // Log::info($resultSearch);           

            if (intval($resultSearch['IDList']['Count']) > 0) {
                if (!is_array($resultSearch['IDList']['ID']['int'])) {
                    $resultSearch['IDList']['ID']['int'] = [$resultSearch['IDList']['ID']['int']];
                }
                
                foreach ($resultSearch['IDList']['ID']['int'] as $id) {
                    if (intval($id) > 0){
                        $productID = $id;
                    }
                }
            }
            
            if (intval($productID) > 0){
                $result = $bepoz->ProductGet($productID);
            } else {
                $result = "Bepoz Product Number not found";
            }

            return response()->json( ['status' => 'ok', 'data' => $result, 'timestamp' => $request->input('timestamp'), 
                'request' => $request->all()] );
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get CustomFieldsMetaGet
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bepozCustomFieldsMetaGet(Bepoz $bepoz)
    {
        try {
            $result = $bepoz->CustomFieldsMetaGet();
            return response()->json(['status' => 'ok', 'data' => $result]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Get Venue Names
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bepozVenueNames(Bepoz $bepoz)
    {
        try {
            $result = $bepoz->IDListGet(40);

            $venueNames = [];
            if (intval($result['IDList']['Count']) > 0) {
                for ($i = 0; $i < count($result['IDList']['ID']['int']); $i++) {
                    $venueNames[] = array(
                        'id' => $result['IDList']['ID']['int'][$i],
                        'name' => $result['IDList']['Name']['string'][$i]
                    );
                }

                // usort($venueNames, [$this, 'name']);
            }

            return response()->json(['status' => 'ok', 'data' => $venueNames]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }
    
    /**
     * Get Operator List
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bepozOperatorList(Bepoz $bepoz)
    {
        try {
            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;

            $payload = [];
            $payload['procedure'] = "OperatorList";
            $payload['parameters'] = [];

            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::error($content);
            // Log::error($httpCode);

            if ($httpCode >= 200 && $httpCode < 300) {
                // pass
                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);

                    $payloadReady = $payload->data;
                    if (!$payload->error) {
                        return response()->json(['status' => 'ok', 'data' => $payloadReady]);
                    } else {
                        return response()->json(['status' => 'error', 'message' => $payloadReady], HttpResponse::HTTP_BAD_REQUEST);
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Response not valid'], HttpResponse::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'Response not valid'], HttpResponse::HTTP_BAD_REQUEST);
            }


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }
    
    /**
     * Get Workstation List
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bepozWorkstationList(Bepoz $bepoz)
    {
        try {
            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;

            $payload = [];
            $payload['procedure'] = "WorkstationList";
            $payload['parameters'] = [];

            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::error($content);
            // Log::error($httpCode);

            if ($httpCode >= 200 && $httpCode < 300) {
                // pass
                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);

                    $payloadReady = $payload->data;
                    if (!$payload->error) {
                        return response()->json(['status' => 'ok', 'data' => $payloadReady]);
                    } else {
                        return response()->json(['status' => 'error', 'message' => $payloadReady], HttpResponse::HTTP_BAD_REQUEST);
                    }
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Response not valid'], HttpResponse::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'Response not valid'], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }
    
}


// $bepoz->IDListGet(40) option
// VenueWorkstations = 33
// BaseMaps = 34
// FunctionMaps = 35
// StockedProductsBySupplier = 36
// POReceiptsBySupplier = 37
// StoreNames = 38
// StoreGroups = 39
// VenueNames = 40
// VenueGroupAndSets = 41
// TransferReceipts = 42
// PDEProducts = 43
// ProductIDsRecursive = 44
// ProductParentIDs = 45
// SelfTestProducts = 46
// TableGroupsAllThisVenue = 47
// OperatorsAssignedToDrawers = 48
// VenueSetVenues = 49





