<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Tier;
use App\Member;
use App\User;
use App\UserRoles;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Carbon\Carbon;

class WebstoreIntegrationController extends Controller
{
    //

    public function executeRequest(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'request' => 'required',
                'param' => 'required',
                'key' => 'required',
                'checksum' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $myplace_key = Setting::where('key', 'myplace_key')->first()->value;

            if ($request->input('key') !== $myplace_key) {
                return response()->json(['status' => 'error', 'message' => 'Incorrect key'], Response::HTTP_UNAUTHORIZED);
            }

            $response = null;
            switch ($request->input('request')) {
                case 'retrieve_stripe_customer':
                    $checksum = md5($request->input('param'));
                    if ($checksum !== $request->input('checksum')) {
                        return response()->json(['status' => 'error', 'message' => 'Tempered params, possibly hacked'], Response::HTTP_UNAUTHORIZED);
                    }

                    $params = json_decode($request->input('param'), true);

                    if (!isset($params['email'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing email'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['test_mode'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing test_mode'], Response::HTTP_BAD_REQUEST);
                    }

                    $user = User::where('email', $params['email'])->first();
                    if (is_null($user)) {
                        return response()->json(['status' => 'error', 'message' => 'User not found'], Response::HTTP_BAD_REQUEST);
                    } else {
                        $response = null;

                        $sc = StripeCustomer::where('member_id', $user->member->id)
                            ->where('test_mode', $params['test_mode'])
                            ->first();

                        if (!is_null($sc)) {
                            $response = $sc->customer_id;
                        }

                    }

                    break;
                case 'sync_stripe_customer':
                    $checksum = md5($request->input('param'));
                    if ($checksum !== $request->input('checksum')) {
                        return response()->json(['status' => 'error', 'message' => 'Tempered params, possibly hacked'], Response::HTTP_UNAUTHORIZED);
                    }

                    $params = json_decode($request->input('param'), true);

                    if (!isset($params['email'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing email'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['customer_id'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing customer_id'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['test_mode'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing test_mode'], Response::HTTP_BAD_REQUEST);
                    }

                    $user = User::where('email', $params['email'])->first();
                    if (is_null($user)) {
                        return response()->json(['status' => 'error', 'message' => 'User not found'], Response::HTTP_BAD_REQUEST);
                    } else {
                        $sc = StripeCustomer::where('member_id', $user->member->id)
                            ->where('test_mode', $params['test_mode'])
                            ->first();

                        if (is_null($sc)) {
                            $sc = new StripeCustomer();
                            $sc->test_mode = $params['test_mode'];
                        }

                        $sc->customer_id = $params['customer_id'];
                        $sc->save();

                    }

                    $response = null;

                    break;
                case 'register':

                    $checksum = md5($request->input('param'));
                    if ($checksum !== $request->input('checksum')) {
                        return response()->json(['status' => 'error', 'message' => 'Tempered params, possibly hacked'], Response::HTTP_UNAUTHORIZED);
                    }

                    $params = json_decode($request->input('param'), true);

                    if (!isset($params['email'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing email'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['password'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing password'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['mobile'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing mobile'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['first_name'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing first_name'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['last_name'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing last_name'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['login_token'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing login_token'], Response::HTTP_BAD_REQUEST);
                    }


                    $credentials = array(
                        'email' => $params['email'],
                        'password' => Hash::make($params['password']),
                        'mobile' => $params['mobile']
                        // 'first_name' => $params['first_name'],
                        // 'last_name' => $params['last_name']
                        // 'role_id' => 3,
                        // 'guest_mode' => false,
                        // 'sms_alerts' => false
                        // 'name' => $name,
                    );

                    $user = User::where('email', $params['email'])->first();
                    if (is_null($user)) {
                        $user = User::create($credentials);
                        $user->email_confirmation = 1;
                        $user->save();

                        $member = new Member;
                        $member->user_id = $user->id;
                        $member->first_name = $params['first_name'];
                        $member->last_name = $params['last_name'];
                        $member->login_token = $params['login_token'];
                        $member->save();

                        // Member::create([
                        //     'user_id' => $user->id,
                        //     'first_name' => $params['first_name'],
                        //     'last_name' => $params['last_name'],
                        //     'bepoz_account_card_number' => null,
                        //     'dob' => null,
                        //     'login_token' => $params['login_token']
                        // ]);

                        $user_role = new UserRoles;
                        $user_role->role_id = 3; // Manually add
                        $user_role->user_id = $user->id;
                        $user_role->save();
    
                        $tier = Tier::first();
                        
                        // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                    
                        // if ($default_member_expiry_date == "true"){
                        //     $setting = Setting::where('key', 'cut_off_date')->first();
                        //     $date_expiry = Carbon::parse($setting->value);
                        //     $date_expiry->setTimezone(config('app.timezone'));
                        //     $date_expiry->setTime(0, 0, 0);
                        // } else {
                        //     $date_expiry = null;
                        // }

                        $date_expiry = null;
                        $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                    } else {
                        $user->update($credentials);
                        $user->member->first_name = $params['first_name'];
                        $user->member->last_name = $params['last_name'];
                        $user->member->login_token = $params['login_token'];
                        $user->member->save();
                    }
                    
                    $response = ['token' => JWTAuth::fromUser($user)];

                    break;

                case 'change_password':
                    //

                    $checksum = md5($request->input('param'));
                    if ($checksum !== $request->input('checksum')) {
                        return response()->json(['status' => 'error', 'message' => 'Tempered params, possibly hacked'], Response::HTTP_UNAUTHORIZED);
                    }

                    $params = json_decode($request->input('param'), true);

                    if (!isset($params['email'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing email'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['password'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing password'], Response::HTTP_BAD_REQUEST);
                    }

                    $user = User::where('email', $params['email'])->first();
                    if (is_null($user)) {
                        return response()->json(['status' => 'error', 'message' => 'User not found'], Response::HTTP_BAD_REQUEST);
                    } else {
                        $user->password = Hash::make($params['password']);
                        $user->save();
                    }

                    $response = ['token' => JWTAuth::fromUser($user)];

                    break;

                case 'update_details':

                    $checksum = md5($request->input('param'));
                    if ($checksum !== $request->input('checksum')) {
                        return response()->json(['status' => 'error', 'message' => 'Tempered params, possibly hacked'], Response::HTTP_UNAUTHORIZED);
                    }

                    $params = json_decode($request->input('param'), true);

                    if (!isset($params['mobile'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing mobile'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['first_name'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing first_name'], Response::HTTP_BAD_REQUEST);
                    }

                    if (!isset($params['last_name'])) {
                        return response()->json(['status' => 'error', 'message' => 'Missing last_name'], Response::HTTP_BAD_REQUEST);
                    }


                    $credentials = array(
                        'mobile' => $params['mobile']
                        // 'first_name' => $params['first_name'],
                        // 'last_name' => $params['last_name'],
                    );

                    $user = User::where('email', $params['email'])->first();
                    if (is_null($user)) {
                        return response()->json(['status' => 'error', 'message' => 'User not found'], Response::HTTP_BAD_REQUEST);
                    } else {
                        $user->update($credentials);
                        $user->member->first_name = $params['first_name'];
                        $user->member->last_name = $params['last_name'];
                        $user->member->save();
                    }

                    $response = ['token' => JWTAuth::fromUser($user)];

                    break;
                default:
                    $response = null;
            }


            return response()->json(['status' => 'ok', 'data' => $response]);


        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
    }
}
