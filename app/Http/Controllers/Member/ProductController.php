<?php

namespace App\Http\Controllers\Member;

use App\Product;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Return all products
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $products = Product::where('status', 'active')
                ->get();

            return response()->json(['status' => 'ok', 'data' => $products->values()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific product
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $product = Product::find($id);

            if (is_null($product)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], HttpResponse::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $product]);
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }


}
