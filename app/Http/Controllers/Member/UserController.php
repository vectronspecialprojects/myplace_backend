<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Helpers\Bepoz;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Helpers\PondHoppers;
use App\Jobs\SendReminderNotification;
use App\BepozJob;
use App\ClaimedPromotion;
use App\ContactChange;
use App\Email;
use App\Listing;
use App\ListingFavorite;
use App\ListingSchedule;
use App\ListingType;
use App\MemberLog;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\PendingJob;
use App\Platform;
use App\Product;
use App\Setting;
use App\Survey;
use App\Member;
use App\MemberTiers;
use App\StripeCustomer;
use App\SystemLog;
use App\Tier;
use App\User;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\VoucherSetups;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Sujip\Guid\Guid;
use Spatie\Async\Pool;

class UserController extends Controller
{
    /**
     * Retrieve member information.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request, Bepoz $bepoz)
    {
        try {
            $user = $request->input('decrypted_token');

            $user->member->member_tier->tier;

            if (!is_null($user->member->member_tier->tier->allowed_venues_tag)) {
                $venue_pivot_tags = $user->member->member_tier->tier->allowed_venues_tag->venue_pivot_tags;

                foreach ($venue_pivot_tags as $venue_pivot_tag) {
                    $venue_pivot_tag->your_order_link = $venue_pivot_tag->venue_yourorder->your_order_link;
                    $venue_pivot_tag->your_order_location = $venue_pivot_tag->venue_yourorder->your_order_location;
                }
            }

            // FEATURE FROM JDA
            if ($user->member->current_preferred_venue > 0) {
                $venue = Venue::with('venue_tags')->find($user->member->current_preferred_venue);
                
                /*
                if (isset($venue->venue_tags[0])){
                    $memberPayload = $user->member->payload;
                    $memberPayload['state_id'] = $venue->venue_tags[0]->id;
                    $memberPayload['state_name'] = $venue->venue_tags[0]->name;
                    $user->member->payload = \GuzzleHttp\json_encode($memberPayload);
                    $user->member->save();
                }
                */

                /*
                try {
                    $venue = Venue::find($user->member->current_preferred_venue);
                    if ( $venue->gaming_system_point ) {
                        // Log::info("venue gaming_system_point");
                        if ( $venue->bepoz_api != "" ){
                            // Log::info("venue bepoz_api not empty");
                            $balance = $bepoz->VenueAccBalanceGet($user->member->current_preferred_venue, $user->member->bepoz_account_id);
                            // Log::info($balance);
                            if ($balance){
                                if (intval($balance['AccBalance']['PointBalance']) >= 0 ){
                                    $user->member->points = intval($balance['AccBalance']['PointBalance']) / 100;
                                } else {
                                    $log = new SystemLog();
                                    $log->type = 'bepoz-job-error';
                                    $log->humanized_message = 'Get acc balance receive negative point result';
                                    $log->message = 'Call Bepoz AccBalanceGet from venue '.$venue->name.' is failed. Please check connection or setting in the Venue BEPOZ API URL and MAC key';
                                    $log->source = 'UserController.php/index';
                                    $log->save();
                                }

                                if (intval($balance['AccBalance']['AccountBalance']) >= 0 ){
                                    $user->member->balance = intval($balance['AccBalance']['AccountBalance']) / 100;
                                } else {
                                    $log = new SystemLog();
                                    $log->type = 'bepoz-job-error';
                                    $log->humanized_message = 'Get acc balance receive negative balance result';
                                    $log->message = 'Call Bepoz AccBalanceGet from venue '.$venue->name.' is failed. Please check connection or setting in the Venue BEPOZ API URL and MAC key';
                                    $log->source = 'UserController.php/index';
                                    $log->save();
                                }

                                $user->member->save();
                            } else {
                                $log = new SystemLog();
                                $log->type = 'bepoz-job-error';
                                $log->humanized_message = 'Get acc balance is failed. Please check the error message';
                                $log->message = 'Call Bepoz AccBalanceGet from venue '.$venue->name.' is failed. Please check connection or setting in the Venue BEPOZ API URL and MAC key';
                                $log->source = 'UserController.php/index';
                                $log->save();
                            }
                        } else {
                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->humanized_message = 'Get acc balance is failed. Please check the error message';
                            $log->message = 'Venue '.$venue->name.' not setup BEPOZ API URL correctly.';
                            $log->source = 'UserController.php/index';
                            $log->save();
                        }
                    }

                } catch (\Exception $e) {
                    Log::error($e);
                }
                */
            }

            call_in_background('acc-balance-get-id ' . $user->member->id);
            call_in_background('accounts-updated-individual ' . $user->member->id);
            call_in_background('voucher-updated-individual ' . $user->member->id);
            call_in_background('accpromo-updated-individual ' . $user->member->id);
            

            if (!is_null($user->member->bepoz_account_card_number) && ($user->member->bepoz_account_card_number != "") ) {
                $card_track = Setting::where('key', 'default_card_track')->first()->value;

                $track = '';
                if ($card_track == 'track_1') {
                    $track = '%';
                } elseif ($card_track == 'track_2') {
                    $track = ';';
                } elseif ($card_track == 'track_3') {
                    $track = '+';
                }

                // if ($user->member->member_tier->tier->use_prefix) {
                //     $card_prefix = $user->member->member_tier->tier->prefix_for_new_member;
                // } else {
                //     $card_prefix = Setting::where('key', 'default_card_number_prefix')->first()->value;
                // }

                $card_prefix = Setting::where('key', 'default_card_number_prefix')->first()->value;

                $card_suffix = '';
                
                // if ($user->member->member_tier->tier->use_suffix) {
                //     $card_suffix = $user->member->member_tier->tier->suffix_for_new_member;
                // }

                $user->member->bepoz_account_card_number = $track . $card_prefix . $user->member->bepoz_account_card_number . $card_suffix . '?';
            }

            if (!$user->email_confirmation) {
                $user->member->bepoz_account_number = null;
                $user->member->bepoz_account_card_number = null;
            }

            $user->member->member_addresses;

            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            if ($venue_number == 'multiple') {
                $is_preffered_venue_color = Setting::where('key', 'is_preffered_venue_color')->first()->value;
                
                if ($user->member->current_preferred_venue > 0) {
                    $user->member->current_preferred_venue_full;
                    $user->member->current_preferred_venue_full->is_preffered_venue_color = $is_preffered_venue_color;
                } else {
                    $image = Setting::where('key', 'invoice_logo')->first()->value;
                    $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                    $android_link = Setting::where('key', 'android_link')->first()->extended_value;
                    $address = Setting::where('key', 'address')->first()->extended_value;
                    $email = Setting::where('key', 'email')->first()->extended_value;
                    $telp = Setting::where('key', 'telp')->first()->extended_value;
                    $fax = Setting::where('key', 'fax')->first()->extended_value;
                    $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
                    $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
                    $social_links = Setting::where('key', 'social_links')->first()->extended_value;
                    $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
                    $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
                    $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;

                    // DUMMY VENUE
                    $venueDefault = collect([
                        'id' => 0,
                        'name' => 'ALL VENUES',
                        'image' => $image,
                        'ios_link' => $ios_link,
                        'android_link' => $android_link,
                        'address' => $address,
                        'email' => $email,
                        'telp' => $telp,
                        'fax' => $fax,
                        'open_and_close_hours' => $open_and_close_hours,
                        'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                        'social_links' => $social_links,
                        'menu_links' => $menu_links,
                        'is_preffered_venue_color' => $is_preffered_venue_color,
                        'hide_delivery_info' => $hide_delivery_info,
                        'hide_opening_hours_info' => $hide_opening_hours_info
                    ]);

                    $user->member->current_preferred_venue_full = $venueDefault;
                }
            }

            if (!$user->email_confirmation) {
                $user->email_confirmation_title = "Email verification required";
                $user->email_confirmation_message = "Please verify your email to proceed to online ordering";
            }

            $numberOf = array();
            $numberOf['tickets'] = MemberVouchers::where('member_id', '=', $user->member->id)
                ->whereHas('order_detail', function ($query) {
                    $query->where('product_type_id', '=', 2);
                })
                ->where('redeemed', 0)
                ->where('amount_left', '<>', 0)
                ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
                ->count();

            $numberOf['vouchers'] = $this->vouchersCount($request);
            // Log::info("profile rejected count"); Log::info($rejected->count());

            $numberOf['favorite'] = $this->favoriteCount($request);

            $numberOf['surveys'] = $this->surveysCount($request);

            // $numberOf['gifts'] = DB::table('listings')->where('listing_type_id', 5)->where('status', 'active')->count();
            // $numberOf['ourTeam'] = DB::table('listings')->where('listing_type_id', 6)->where('status', 'active')->count();

            $numberOf['gifts'] = $this->listingCount($request, 5);
            $numberOf['ourTeam'] = $this->listingCount($request, 6);

            $numberOf['promotions'] = $this->promotionsCount($request);

            // $numberOf['regularEvents'] = DB::table('listings')->where('listing_type_id', 2)->where('status', 'active')->count();
            // $numberOf['specialEvents'] = DB::table('listings')->where('listing_type_id', 1)->where('status', 'active')->count();

            $numberOf['regularEvents'] = $this->listingCount($request, 2);
            $numberOf['specialEvents'] = $this->listingCount($request, 1);

            // $numberOf['stampCard'] = DB::table('listings')->where('listing_type_id', 7)->where('status', 'active')->count();
            // $numberOf['stampCardWon'] = DB::table('listings')->where('listing_type_id', 8)->where('status', 'active')->count();

            $numberOf['stampCard'] = $this->listingCount($request, 7);
            $numberOf['stampCardWon'] = $this->stampCardWon($request);

            $numberOf['saved'] = $user->member->saved;
            $numberOf['points'] = $user->member->points;
            $numberOf['balance'] = $user->member->balance;

            // try {
            //     // $gallery_top_box_enable = Setting::where('key', 'gallery_top_box_enable')->first()->value;

            //     // if ($gallery_top_box_enable == "true") {
            //         $PondHoppers = new PondHoppers();            
            //         // $memberNumber = "10071007";
            //         $memberNumber = $user->member->bepoz_account_number;
            //         $venueId = "26";
            //         // $memberNumber = $user->member->bepoz_account_number;
            //         // $venueId = $venue->bepoz_venue_id;
            //         $resultPondHoppers = $PondHoppers->POINTS($memberNumber, $venueId);                    
            //         $cleanPondHoppers = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $resultPondHoppers);
            //         $user->member->PondHoppers = $cleanPondHoppers;

            //         $feature_app_layout_community_points = Setting::where('key', 'feature_app_layout_community_points')->first()->value;
        
            //         if ($feature_app_layout_community_points === 'true') {
            //             $resultDecode = \GuzzleHttp\json_decode($cleanPondHoppers);
            //             $numberOf['community_points'] = $resultDecode->pointsAvailable;
            //         } else {
            //             $numberOf['community_points'] = 0;
            //         }
            //     // } else {
            //     //     $numberOf['community_points'] = 0;
            //     // }
            // } catch (\Exception $e) {
            //     Log::error($e);
            // }

            $user->member->numberOf = $numberOf;

            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            $venue_tags_enable = Setting::where('key', 'venue_tags_enable')->first()->value;

            if ($venue_number == 'single'){
                $image = Setting::where('key', 'invoice_logo')->first()->value;
                $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                $android_link = Setting::where('key', 'android_link')->first()->extended_value;
                $address = Setting::where('key', 'address')->first()->extended_value;
                $email = Setting::where('key', 'email')->first()->extended_value;
                $telp = Setting::where('key', 'telp')->first()->extended_value;
                $fax = Setting::where('key', 'fax')->first()->extended_value;
                $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
                $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
                $social_links = Setting::where('key', 'social_links')->first()->extended_value;
                $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
                $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->value;
                $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->value;

                $hqvenue = collect([
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);

                $venues = collect([$hqvenue]);

            } else if ($venue_number == 'multiple') {
                $venues = Venue::with('tier', 'venue_pivot_tags')
                    ->where('active', 1)
                    ->get();
            }

            $venueTagAll = VenueTag::
                with(
                    ['venue_pivot_tags' => function($q) {
                        $q->orderBy('display_order');
                    }]
                )
                ->where('status', 1)
                ->where('hide_in_app', 0)
                ->orderBy('display_order')
                ->get();

            return response()->json([ 'status' => 'ok', 'data' => $user, 'venues' => $venues,
                'venue_tags' => $venueTagAll, 'venue_tags_enable' => $venue_tags_enable ]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }


    protected function listingCount(Request $request, $display_id)
    {

        $type = ListingType::find($display_id);

        $date_now = Carbon::now(config('app.timezone'))->toDateString();
        $listingSchedules = DB::table('listing_schedules')
            //->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
            //->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
            ->whereDate('date_start', '<=', $date_now)
            // ->whereDate('date_end', '>=', $date_now)
            ->where(function ($query) use ($request, $date_now) {
                $query->whereDate('date_end', '>=', $date_now);
                $query->orWhere('never_expired', '1');
            })
            // ->orWhere('never_expired', '1')
            ->whereExists(function ($query) use ($type, $request) {

                if (isset($request->input('decrypted_token')->member)) {
                    if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                            ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }
                } else {
                    $query->select(DB::raw(1))
                        ->from('listing_schedule_listing_type')
                        ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                        ->whereNull('listing_schedule_listing_type.deleted_at')
                        ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                }
            })
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('listings')
                    ->where('status', 'active')
                    ->whereNull('listings.deleted_at')
                    ->whereRaw('listings.id = listing_schedules.listing_id');
            })
            ->where('status', 'active')
            ->whereNull('deleted_at')
            ->select('id')
            ->get();

        $user = $request->input('decrypted_token');

        // Log::info("current_preferred_venue ".$user->member->current_preferred_venue);
        // Log::info();

        if (!empty($listingSchedules)) {
            $temp = [];
            foreach ($listingSchedules as $schedule) {
                $temp[] = $schedule->id;
            }

            $listingSchedules = ListingSchedule::with('listing.type')
                ->join('listings', 'listing_schedules.listing_id', '=', 'listings.id')
                ->whereIn('listing_schedules.id', $temp)
                // ->orderBy('listings.date_start', 'asc')
                ->orderBy('listings.display_order', 'asc')
                ->get(['listing_schedules.*']);


            $rejectedListing = $listingSchedules->reject(function ($value, $key) use ($user) {
                // Log::info(print_r($value, true));
                // Log::info($value->listing->venue_id);
                if (intval($value->listing->venue_id) == 0) {
                    return false;
                } else if (intval($value->listing->venue_id) == intval($user->member->current_preferred_venue)) {
                    return false;
                } else {
                    // Log::info("rejected ");
                    return true; // rejected
                }
            });
            // Log::info($rejectedListing);
            return $rejectedListing->count();
        }

        return 0;
    }
    
    protected function promotionsCount(Request $request)
    {
        // NUMBER OF PROMOTION NEED CHECKER LIKE PROMOTION
        $tipes[] = 4;
        $date_now = Carbon::now(config('app.timezone'))->toDateString();
        $listingSchedules = DB::table('listing_schedules')
            // ->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
            // ->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
            ->whereDate('date_start', '<=', $date_now)
            ->where(function ($query) use ($request, $date_now) {
                $query->whereDate('date_end', '>=', $date_now);
                $query->orWhere('never_expired', '1');
            })
            // ->whereDate('date_end', '>=', $date_now)
            // ->orWhere('never_expired', 1)

            ->whereExists(function ($query) use ($tipes, $request) {
                if (isset($request->input('decrypted_token')->member)) {
                    if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                            ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }
                } else {
                    $query->select(DB::raw(1))
                        ->from('listing_schedule_listing_type')
                        ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                        ->whereNull('listing_schedule_listing_type.deleted_at')
                        ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                }

            })
            // ->whereExists(function ($query) {
            //     $query->select(DB::raw(1))
            //         ->from('listing_products')
            //         ->join('products', 'listing_products.product_id', '=', 'products.id')
            //         ->where('products.status', 'active')
            //         ->whereNull('listing_products.deleted_at')
            //         ->whereRaw('listing_products.listing_id = listing_schedules.listing_id');
            // })
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('listings')
                    ->join('listing_types', 'listings.listing_type_id', '=', 'listing_types.id')
                    ->where('listing_types.key', 'like', '%"category":"promotion"%')
                    ->where('listings.status', 'active')
                    ->whereNull('listings.deleted_at')
                    ->whereRaw('listings.id = listing_schedules.listing_id');
            })


            //    ->whereNotExists(function ($query) use ($request) {
            //        if (isset($request->input('decrypted_token')->member)) {
            //            if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
            //                $query->select(DB::raw(1))
            //                    ->from('claimed_promotions')
            //                    ->where('claimed_promotions.member_id', $request->input('decrypted_token')->member->id)
            //                    ->where('claimed_promotions.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
            //                    ->whereNull('claimed_promotions.deleted_at')
            //                    ->whereRaw('claimed_promotions.listing_id = listing_schedules.listing_id');
            //            }
            //        }
            //    })
            ->where('status', 'active')
            ->whereNull('deleted_at')
            ->select('id')
            ->get();


        if (!empty($listingSchedules)) {
            $temp = [];
            foreach ($listingSchedules as $schedule) {
                $temp[] = $schedule->id;
            }

            $listingSchedules = ListingSchedule::with('listing.type')
                ->join('listings', 'listing_schedules.listing_id', '=', 'listings.id')
                ->whereIn('listing_schedules.id', $temp)
                // ->orderBy('listings.date_start', 'asc')
                ->orderBy('listings.display_order', 'asc')
                ->get(['listing_schedules.*']);

            $rejectedPromotions = $listingSchedules->reject(function ($value, $key) use ($request) {
                if (!is_null($value->listing->products->first())) {
                    $countMaxLimit = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                        ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                        ->where('listing_id', $value->listing->id)
                        ->where('listing_type_id', $value->listing->type->id)
                        ->count();

                    if (is_null($countMaxLimit)) {
                        $countMaxLimit = 0;
                    }

                    $countMaxLimitPerDay = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                        ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                        ->where('listing_id', $value->listing->id)
                        ->where('listing_type_id', $value->listing->type->id)
                        ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                        ->count();

                    if (is_null($countMaxLimitPerDay)) {
                        $countMaxLimitPerDay = 0;
                    }

                    $countMaxLimitPerAccount = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                        ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                        ->where('listing_id', $value->listing->id)
                        ->where('listing_type_id', $value->listing->type->id)
                        ->where('member_id', $request->input('decrypted_token')->member->id)
                        ->count();

                    if (is_null($countMaxLimitPerAccount)) {
                        $countMaxLimitPerAccount = 0;
                    }

                    $countMaxLimitPerDayPerAccount = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                        ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                        ->where('listing_id', $value->listing->id)
                        ->where('listing_type_id', $value->listing->type->id)
                        ->where('member_id', $request->input('decrypted_token')->member->id)
                        ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                        ->count();

                    if (is_null($countMaxLimitPerDayPerAccount)) {
                        $countMaxLimitPerDayPerAccount = 0;
                    }

                    if (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) === 0 &&
                        intval($value->listing->max_limit_per_account) === 0 &&
                        intval($value->listing->max_limit_per_day_per_account) === 0
                    ) {
                        // COMBINATION 1
                        if ($countMaxLimit >= $value->listing->max_limit) {
                            return true; // rejected
                        }
                    } elseif (intval($value->listing->max_limit) === 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) === 0 &&
                        intval($value->listing->max_limit_per_day_per_account) === 0
                    ) {
                        // COMBINATION 2
                        //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) === 0 &&
                        intval($value->listing->max_limit_per_day) === 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) === 0
                    ) {
                        // COMBINATION 3
                        // Log::info("countMaxLimitPerAccount ".$countMaxLimitPerAccount);
                        // Log::info("setting MaxLimitPerAccount ".$value->listing->max_limit_per_account);
                        if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            return true; // rejected
                        }
                    } elseif (intval($value->listing->max_limit) === 0 &&
                        intval($value->listing->max_limit_per_day) === 0 &&
                        intval($value->listing->max_limit_per_account) === 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 4
                        //    if ($countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) === 0 &&
                        intval($value->listing->max_limit_per_day_per_account) === 0
                    ) {
                        // COMBINATION 5
                        if ($countMaxLimit >= $value->listing->max_limit) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) === 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) === 0
                    ) {
                        // COMBINATION 6
                        if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            return true; // rejected
                        }
                    } elseif (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) === 0 &&
                        intval($value->listing->max_limit_per_account) === 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 7
                        if ($countMaxLimit >= $value->listing->max_limit) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) === 0
                    ) {
                        // COMBINATION 8
                        if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) === 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 9
                        if ($countMaxLimit >= $value->listing->max_limit) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 10
                        if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) === 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) === 0
                    ) {
                        // COMBINATION 11
                        if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) === 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) === 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 12
                        if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            return true; // rejected
                        }
                    } elseif (intval($value->listing->max_limit) === 0 &&
                        intval($value->listing->max_limit_per_day) > 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 13
                        if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) === 0 &&
                        intval($value->listing->max_limit_per_day) === 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 14
                        if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            return true; // rejected
                        }
                        //    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        //        return true; // rejected
                        //    }
                    } elseif (intval($value->listing->max_limit) > 0 &&
                        intval($value->listing->max_limit_per_day) === 0 &&
                        intval($value->listing->max_limit_per_account) > 0 &&
                        intval($value->listing->max_limit_per_day_per_account) > 0
                    ) {
                        // COMBINATION 15
                        // if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        //     return true; // rejected
                        // }
                        if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            return true; // rejected
                        }
                    }
                }


                return false;

            });

            return $rejectedPromotions->count();
        }

        return 0;
    }

    protected function vouchersCount(Request $request)
    {
        $user = $request->input('decrypted_token');

        $vouchers = MemberVouchers::with('member.tiers.venue')
            ->where('member_id', '=', $user->member->id)
            ->where(function ($query) use ($request) {
                $query->whereHas('order_detail', function ($query) use ($request) {
                    $query->where('product_type_id', '<>', 2);
                });
                $query->orWhere('member_vouchers.category', 'bepoz');
            })
            ->where('redeemed', 0)
            ->where('amount_left', '<>', 0)
            ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
            ->get();

        // Log::info("profile vouchers count"); Log::info($vouchers->count());
        $rejected = $vouchers->reject(function ($value, $key) {
            if ($value->order_id === 0) {
                return false;
            } else {

                $voucherSetup = VoucherSetups::where('id', $value->voucher_setup_id)->first();
                //Log::error($voucherSetup["expiry_date"]);

                //not return inactive
                if ($voucherSetup["inactive"] == 1) {
                    return true;
                }

                if (is_null($value->expire_date)) {
                    return false;
                }

                $date_now = Carbon::now();
                //$expiry_date = Carbon::parse($voucherSetup["expiry_date"]);
                $expire_date = Carbon::parse($value->expire_date);
                $date_diff = $date_now->diffInDays($expire_date, false);

                //Log::error($value->voucher_setup_id);
                //Log::error($voucherSetup["expiry_date"]);
                //Log::error($date_diff);

                //not return expired
                if ($date_diff < 0) {
                    return true;
                }

                $order = Order::find($value->order_id);
                if (is_null($order)) {
                    return true; // rejected
                } else {
                    if (is_null($order->payload)) {
                        return true;
                    } else {
                        $payload = \GuzzleHttp\json_decode($order->payload);
                        if (is_null($payload)) {
                            return true;
                        } else {
                            if (isset($payload->transaction_type)) {
                                if ($payload->transaction_type != 'place_order') {
                                    $od = OrderDetail::find($value->order_details_id);
                                    if (is_null($od)) {
                                        return true;
                                    } else {
                                        if ($payload->transaction_type == 'friend_referral') {
                                            return false;
                                        } elseif ($payload->transaction_type == 'signup_reward') {
                                            return false;
                                        } else {
                                            $listing = Listing::find($od->listing_id);
                                            if (is_null($listing)) {
                                                if ($value->category == 'bepoz') {
                                                    return false;
                                                }
                                                if ($value->category == 'survey_reward') {
                                                    return false;
                                                }

                                                return true;
                                            } else {
                                                if (is_null($listing->type->key)) {
                                                    return true;
                                                } else {
                                                    return false;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    return false; // just editted
                                }
                            } else {
                                return false;
                            }
                        }
                    }
                }
            }

        });

        return $rejected->count();

    }

    protected function favoriteCount(Request $request)
    {
        $result = ListingFavorite::with('listing.type', 'listing.schedules')
            ->where('member_id', $request->input('decrypted_token')->member->id)
            ->whereHas('listing', function ($query) {
                $query->where('status', 'active');
                // $query->whereDate('datetime_start', '>', Carbon::now(config('app.timezone'))->toDateString());
                // $query->orWhere('datetime_start', null);

                // $query->whereDate('date_start', '>', Carbon::now(config('app.timezone'))->toDateString());
                // $query->orWhere('date_start', null);
            })
            ->whereHas('listing.schedules', function ($query) {
                $query->where('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString());
                $query->orWhere('never_expired', '1');
            })
            ->count();

        return $result;
    }

    protected function surveysCount(Request $request)
    {
        $user = $request->input('decrypted_token');

        $result = Survey::whereHas('tiers', function ($query) use ($user) {
                $query->where('tier_id', '=', $user->member->tiers()->first()->id);
            })
            ->where('status', 'active')
            ->where(function ($q) use ($request) {
                $q->where(function ($q1) {
                    $q1->where('never_expired', false);
                    $q1->where('date_expiry', '>', Carbon::now(config('app.timezone'))->toDateString());
                });

                $q->orWhere(function ($q2) {
                    $q2->where('never_expired', true);
                });
            })
            ->whereDoesntHave('survey_answers', function ($query) use ($user) {
                $query->where('member_id', $user->member->id);
            })
            ->count();

        return $result;
    }

    protected function stampCardWon(Request $request)
    {
        try {
            //dispatch(new RetrieveVouchers($request->input('decrypted_token')->member));
            //product_type_id <> 2 instead product_type_id = 1
            // Log::info($request);
            // Log::info($request->input('decrypted_token'));

            $vouchers = MemberVouchers::where('member_id', '=', $request->input('decrypted_token')->member->id)
                // ->where(function ($query) use ($request) {
                // $query->whereHas('order_detail', function ($query) use ($request) {
                //     $query->where('product_type_id', '<>', 2);
                // });
                // $query->orWhere('member_vouchers.category', 'bepoz');
                // })
                ->where('category', 'bepoz')
                ->where('bepoz_prize_promo_id', '>', 0)
                ->where('redeemed', 0)
                ->where('amount_left', '<>', 0)
                ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
                ->get();

            foreach ($vouchers as $voucher) {
                //Log::info($voucher);

                $cp = ClaimedPromotion::where('member_voucher_id', $voucher->id)->first();
                if (is_null($cp)) {
                    if ($voucher->order_id === 0) {
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->claim_promotion = collect(
                            [
                                'listing' => [
                                    'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'image_square' => $tc->value],
                                'product' => [
                                    'name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'desc_short' => 'This voucher was issued from the till.']
                            ]);

                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->img = $tc->value;

                    } else {
                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);
                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->order = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->order->listing = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'image_square' => $tc->value
                            ]);

                        //$voucher->order_detail->listing;
                        //$voucher->order_detail;
                        //$voucher->order_detail->voucher_setup;

                        if (is_null($voucher->order->listing)) {
                            $tc = Setting::where('key', 'invoice_logo')->first();
                            $voucher->img = $tc->value;
                        } else {
                            //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                            //$voucher->img = $voucher->order->listing->image_square;

                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();

                            if (is_null($product)) {
                                //$voucher->img = $voucher->order->listing->image_square;
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $product->image;
                            }
                        }


                        $order = Order::find($voucher->order_id);
                        if (!is_null($order)) {
                            if (!is_null($order->payload)) {
                                $payload = \GuzzleHttp\json_decode($order->payload);
                                if ($payload->transaction_type == 'friend_referral') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();
                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucherSetup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucherSetup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);
                                    $voucher->img = $tc->value;


                                } elseif ($payload->transaction_type == 'place_order') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();

                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucherSetup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucherSetup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);

                                    if (is_null($voucher->order->listing)) {
                                        $tc = Setting::where('key', 'invoice_logo')->first();
                                        $voucher->img = $tc->value;
                                    } else {

                                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                                        //$voucher->img = $voucher->order->listing->image_square;

                                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();

                                        if (is_null($product)) {
                                            //$voucher->img = $voucher->order->listing->image_square;
                                            $order = Order::where('id', $voucher->order_id)->first();
                                            if (is_null($order)) {
                                                $voucher->img = $tc->value;
                                            } else {
                                                $voucher->img = $voucher->order->listing->image_square;
                                            }
                                        } else {
                                            $voucher->img = $product->image;
                                        }


                                    }

                                } else {

                                    $tc = Setting::where('key', 'invoice_logo')->first();


                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'No description.']
                                    //    ]);

                                    //Log::info($voucherSetup);
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->name,
                                                'desc_short' => 'No description.']
                                        ]);


                                    $voucher->img = $tc->value;

                                }

                            }
                        }
                    }

                } else {
                    //$voucher->order_detail->listing;
                    //$voucher->order_detail->voucher_setup;
                    $tc = Setting::where('key', 'invoice_logo')->first();
                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                    $voucher->order_detail = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);

                    $voucher->order_detail->listing = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'image_square' => $tc->value
                        ]);

                    $voucher->order_detail->voucher_setup = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);

                    $voucher->claim_promotion->listing;
                    $voucher->claim_promotion->product;


                    if (is_null($voucher->order->listing)) {
                        //$tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->img = $tc->value;
                    } else {
                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                        //$voucher->img = $voucher->order->listing->image_square;

                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();

                        if (is_null($product)) {
                            $order = Order::where('id', $voucher->order_id)->first();
                            if (is_null($order)) {
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $voucher->order->listing->image_square;
                            }
                        } else {
                            $voucher->img = $product->image;
                        }

                    }
                }
            }

            $rejected = $vouchers->reject(function ($value, $key) {
                if ($value->order_id === 0) {
                    return false;
                } else {

                    $voucherSetup = VoucherSetups::where('id', $value->voucher_setup_id)->first();
                    //Log::error($voucherSetup["expiry_date"]);

                    //not return inactive
                    if ($voucherSetup["inactive"] == 1) {
                        return true;
                    }

                    $date_now = Carbon::now();
                    //$expiry_date = Carbon::parse($voucherSetup["expiry_date"]);
                    $expire_date = Carbon::parse($value->expire_date);
                    $date_diff = $date_now->diffInDays($expire_date, false);

                    //Log::error($value->voucher_setup_id);
                    //Log::error($voucherSetup["expiry_date"]);
                    //Log::error($date_diff);

                    //not return expired
                    if ($date_diff < 0) {
                        return true;
                    }

                    $order = Order::find($value->order_id);
                    if (is_null($order)) {
                        return true; // rejected
                    } else {
                        if (is_null($order->payload)) {
                            return true;
                        } else {
                            $payload = \GuzzleHttp\json_decode($order->payload);
                            if (is_null($payload)) {
                                return true;
                            } else {
                                if (isset($payload->transaction_type)) {
                                    if ($payload->transaction_type != 'place_order') {
                                        $od = OrderDetail::find($value->order_details_id);
                                        if (is_null($od)) {
                                            return true;
                                        } else {
                                            if ($payload->transaction_type == 'friend_referral') {
                                                return false;
                                            } elseif ($payload->transaction_type == 'signup_reward') {
                                                return false;
                                            } else {
                                                $listing = Listing::find($od->listing_id);
                                                if (is_null($listing)) {
                                                    if ($value->category == 'bepoz') {
                                                        return false;
                                                    }

                                                    return true;
                                                } else {
                                                    if (is_null($listing->type->key)) {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        return false; // just editted
                                    }
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }

            });

            return $rejected->count();

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    protected function communityPointsCount(Request $request)
    {
        try {
            
            $feature_app_layout_community_points = Setting::where('key', 'feature_app_layout_community_points')->first()->value;
            
            if ($feature_app_layout_community_points === 'true') {
                $PondHoppers = new PondHoppers();        
                $memberNumber = "10071007";
                $venueId = "26";
                // $memberNumber = $request->input('member_number');
                // $venueId = $request->input('venue_id');
                $result = $PondHoppers->PondHoppers($memberNumber, $venueId);

            }
            
            // return response()->json(['result' => $result]);

            return 0;

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Update user profile detail.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function updateProfile(Request $request, Bepoz $bepoz)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id_token' => 'required',
                'access_token' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // CHECK BEPOZ CUSTOM FIELD REQUIRED
            $customfieldpayload = [];
            $standardfieldpayload = [];
            $allInputpayload = [];

                // VALIDATE BEPOZ CUSTOM FIELD BASED ON SETTING
                $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first()->extended_value;
                // echo $bepozcustomfield."<br<";
                $bepozcustomfielddecode = \GuzzleHttp\json_decode($bepozcustomfield);
                // echo print_r($data, true)."<br>";

                if (sizeof($bepozcustomfielddecode)) {

                    foreach ($bepozcustomfielddecode as $cf) {
                        // Log::info($cf->fieldName);
                        // Log::info($cf->required);
                        // Log::info($cf->active);

                        if (isset($cf->editOnProfile) && $cf->editOnProfile) {
                            // NEED iGNORE EMAIL AND MOBILE, IT WILL HAVE SEPARATE API

                            $bepozFieldName = $this->bepozAccField($cf->bepozFieldNum);

                            if ($bepozFieldName == "Mobile") {
                                // NO NEED
                                $mobile = $request->input($cf->id);
                            } else if ($bepozFieldName == "Email1st") {
                                // NO NEED
                            } else {
                                if (!$request->has($cf->id)) {
                                    return response()->json(['status' => 'error', 'message' => $cf->fieldName . ' is required.'], Response::HTTP_BAD_REQUEST);
                                }
                            }

                            if ( $this->isCustomField($cf) ) {
                                $customfieldpayload[] = array(
                                    'id' => $cf->id,
                                    'value' => $request->input($cf->id),
                                    'fieldName' => $cf->fieldName,
                                    'fieldType' => $cf->fieldType,
                                    'bepozFieldNum' => $cf->bepozFieldNum
                                );
                            } else {
                                $standardfieldpayload[] = array(
                                    'id' => $cf->id,
                                    'value' => $request->input($cf->id),
                                    'fieldName' => $cf->fieldName,
                                    'fieldType' => $cf->fieldType,
                                    'bepozFieldNum' => $cf->bepozFieldNum
                                );
                            }
            
                            $allInputpayload[] = array(
                                $cf->id => $request->input($cf->id)
                            );
                        }
                    }
                }
            

            // Log::info($customfieldpayload);
            // Log::info($standardfieldpayload);
            // Log::info($allInputpayload);


            $user = $request->input('decrypted_token');
            $cloned = $user->member->replicate();
            $member = $user->member;

            $mobilechange = false;

                if ($request->has('first_name')) {
                    $first_name = $request->input('first_name');

                    if (strlen($first_name) != strlen(utf8_decode($first_name))) {
                        return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
                    }

                    $user->member->first_name = $request->input('first_name');
                }

                if ($request->has('last_name')) {
                    $last_name = $request->input('last_name');

                    if (strlen($last_name) != strlen(utf8_decode($last_name))) {
                        return response()->json(['status' => 'error', 'message' => 'Special characters including emoji are not allowed.'], Response::HTTP_BAD_REQUEST);
                    }

                    $user->member->last_name = $request->input('last_name');
                }

                if ($request->has('dob')) {
                    if ($request->input('dob') != '') {
                        $user->member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                    }
                }

                // if ($request->has('phone')) {
                //     $user->member->phone = $request->input('phone');
                // } else {
                //     $user->member->phone = "";
                // }

                if ($request->has('mailing_preference')) {
                    $user->member->mailing_preference = $request->input('mailing_preference');
                }

                if ($request->has('mobile')) {
                    $canchange = false;
                    $bepozChange = false;
                    $myplaceChange = false;
                    $SSOChange = false;
    
                    if ($this->checkSSOAccountByMobile($mobile) == "ok") {
                        $mobileSSOCan = false;
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE') ], Response::HTTP_BAD_REQUEST);
                    } else if ($this->checkSSOAccountByMobile($mobile) == "Unable to connect SSO") {
                        $mobileSSOCan = false;
                        return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_SSO') ], Response::HTTP_BAD_REQUEST);
                    } else {
                        $SSOChange = true;
                    }
    
                    // ANY PHONE NUMBER
                    $checkbepozmobile = $bepoz->AccountSearch(6, $mobile);
                    
                    if ($checkbepozmobile) {
                        if (intval($checkbepozmobile['IDList']['Count']) > 0) {
                            $bepozChange = false;
                            // it means more than 1 mobile duplicated
                            return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE') ], Response::HTTP_BAD_REQUEST);
                        } else if (intval($checkbepozmobile['IDList']['Count']) == 0) {
                            $bepozChange = true;
                        }
                    } else {
                        $bepozChange = false;
                        return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_BEPOZ') ], Response::HTTP_BAD_REQUEST);
                    }
    
                    $checkMobile = User::where('mobile', $mobile)->first();
                    
                    if ( is_null($checkMobile) ){
                        // CAN CONTINUE
                        $myplaceChange = true;
                    } else {
                        $myplaceChange = false;
                        return response()->json(['status' => 'error', 'message' => 'Duplicate Mobile already found in Myplace'], Response::HTTP_BAD_REQUEST);
                    }
                    
                    $canchange = $SSOChange && $bepozChange && $myplaceChange;
    
                    // Log::info("canchange ".$canchange);
                    if ($canchange){
                        $cloned = $member->replicate();
    
                        $contactChange = new ContactChange;
                        $contactChange->member_id = $member->id;
                        $contactChange->user_id = $member->user->id;
                        $contactChange->old_mobile = $cloned->user->mobile;
                        $contactChange->new_mobile = $request->input('mobile');
                        $contactChange->old_guid = $member->login_token;
                        $contactChange->token = $this->generateRandomString(32);
                        $contactChange->mobile_confirmation_sent = Carbon::now(config('app.timezone'));
                        $contactChange->save();
    
                        // Get web app token
                        $platform = Platform::where('name', 'web')->first();
                        $url = config('bucket.APP_URL') . '/api/confirmMobile/' . $contactChange->token . '?app_token=' . $platform->app_token;
                        
                        $sms_mobile_change_confirmation_message = Setting::where('key', 'sms_mobile_change_confirmation_message')->first()->value;
    
                        $message = $sms_mobile_change_confirmation_message;
                        $tier_name = $member->member_tier->tier->name;
    
                        $message = preg_replace(['{{{FirstName}}}', '{{{LastName}}}', '{{{AccountNumber}}}', '{{{Tier}}}', '{{{Link}}}'],
                            [$member->first_name, $member->last_name, $member->bepoz_account_number, $tier_name, $url], $message);
                        
                        $api = new \App\Helpers\TransmitSMSAPI();
                        $result = $api->sendSms($message, $request->input('mobile'), "");
                        
                        $mobilechange = true;
                    }
    
                }

                // FEATURE FROM VARSITY, NEED MAKE GLOBAL VARIABLE
                if ($request->has('promo_code')) {
                    $user->member->promo_code = $request->input('promo_code');
                }
                
                // // FEATURE FROM VARSITY, NEED MAKE GLOBAL VARIABLE
                // if ($request->has('sponsorship_code')) {
                //     $user->member->sponsorship_code = $request->input('sponsorship_code');
                // }

                // if ($request->has('opt_out_marketing')) {
                //     $opt_out_marketing = $request->input('opt_out_marketing') == 'true' ? 1 : 0;
                //     $user->member->opt_out_marketing = $opt_out_marketing;
                // }

                $user->save();
                $user->member->save();

                $ml = new MemberLog();
                $ml->before = $cloned->toJson();
                $ml->member_id = $user->member->id;
                $ml->message = $request->input('decrypted_token')->member->first_name . '(' . $request->input('decrypted_token')->member->id . ') has updated his/her details. ';
                $ml->after = $user->member->toJson();
                $ml->save();
            
                //integrate SSO
                if ($user->member->share_info == 1) {
                    try {
                        $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/user";
                        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                        $datasend = array(
                            'given_name' => $user->member->first_name,
                            'family_name' => $user->member->last_name,
                            'phone_number' => $user->mobile,
                            'email' => $user->email,
                            'id_token' => $request->input('id_token'),
                            'access_token' => $request->input('access_token'),
                            "license_key" => $your_order_key
                        );

                        $userreload = User::find($user->id);
                        if (!is_null($userreload->member->dob)) {
                            if ($userreload->member->dob != "null") {
                                $parseagain = false;
                                try {
                                    $datasend['birthdate'] = Carbon::parse($userreload->member->dob)->format('Y-m-d');
                                } catch (\Exception $e) {
                                    $parseagain = true;
                                }

                                if ($parseagain) {
                                    try {
                                        $datasend['birthdate'] = Carbon::createFromFormat('d/m/Y', $request->input('dob'))->format('Y-m-d');
                                    } catch (\Exception $e) {

                                    }
                                }
                            }
                        }

                        $payload = json_encode($datasend);
                        $authorization = "Authorization: Bearer " . $request->input('access_token');

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_URL, $your_order_api);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                        $content = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        // Log::warning($your_order_api);                        
                        // Log::warning($payload);
                        // Log::warning($content);
                        // Log::warning($httpCode);

                        if ($content) {
                            $payload = \GuzzleHttp\json_decode($content);
                            // Log::warning(print_r($payload, true));
                            if ($payload->status == "error" && $payload->error == "user_not_found") {
                                return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                            }
                            if ($payload->status == "error") {
                                return response()->json(['status' => 'error', 'message' => $payload->message], Response::HTTP_BAD_REQUEST);
                            }

                        } else {
                            return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                        }

                        // Log::warning("result");
                        // Log::warning(print_r($payload, true));

                    } catch (\Exception $e) {
                        Log::error($e);

                        return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
                    }
                }
            
                if (!is_null($user->member->bepoz_account_id)) {
                    $data = array();
                    $data['member_id'] = $user->member->id;

                    $job = new BepozJob();
                    $job->queue = "update-account";
                    $job->payload = \GuzzleHttp\json_encode($data);
                    $job->available_at = time();
                    $job->created_at = time();
                    $job->save();
                }

                // $token = JWTAuth::refresh(JWTAuth::getToken());

                $message = 'Updating Profile Successful. ';

                if ($mobilechange) {
                    $message .= 'SMS confirmation sent. ';
                }

                return response()->json(['status' => 'ok', 'message' => $message,
                    'id_token' => $payload->data->id_token,
                    'access_token' => $request->input('access_token')]);
                
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    function isCustomField($cf) {        
        $customField = false;

        switch (intval($cf->bepozFieldNum)) {
            case 1:
                $customField = true;
                break;
            case 2:
                $customField = true;
                break;
            case 3:
                $customField = true;
                break;
            case 4:
                $customField = true;
                break;
            case 5:
                $customField = true;
                break;
            case 6:
                $customField = true;
                break;
            case 7:
                $customField = true;
                break;
            case 8:
                $customField = true;
                break;
            case 9:
                $customField = true;
                break;
            case 10:
                $customField = true;
                break;
            case 101:
                $customField = true;
                break;
            case 102:
                $customField = true;
                break;
            case 103:
                $customField = true;
                break;
            case 104:
                $customField = true;
                break;
            case 105:
                $customField = true;
                break;
            case 201:
                $customField = true;
                break;
            case 202:
                $customField = true;
                break;
            case 203:
                $customField = true;
                break;
            case 204:
                $customField = true;
                break;
            case 205:
                $customField = true;
                break;
            case 301:
                $customField = true;
                break;
            case 302:
                $customField = true;
                break;
            case 303:
                $customField = true;
                break;
            case 304:
                $customField = true;
                break;
            case 305:
                $customField = true;
                break;
            case 306:
                $customField = true;
                break;
            case 307:
                $customField = true;
                break;
            case 308:
                $customField = true;
                break;
            case 309:
                $customField = true;
                break;
            case 310:
                $customField = true;
                break;
            case 311:
                $customField = true;
                break;
            case 312:
                $customField = true;
                break;
            case 313:
                $customField = true;
                break;
            case 314:
                $customField = true;
                break;
            case 315:
                $customField = true;
                break;
            case 316:
                $customField = true;
                break;
            case 317:
                $customField = true;
                break;
            case 318:
                $customField = true;
                break;
            case 319:
                $customField = true;
                break;
            case 320:
                $customField = true;
                break;
            default:
                $customField = false;
        }

        return $customField;
    }

    function bepozAccField($number) {
        $name = '';
        switch (intval($number)) {
            case 11:
                $name = 'DoNotPost';
                break;
            case 12:
                $name = 'DoNotEmail';
                break;
            case 13:
                $name = 'DoNotSMS';
                break;
            case 14:
                $name = 'DoNotPhone';
                break;
            case 106:
                $name = 'DateBirth';
                break;
            case 321:
                $name = 'Title';
                break;
            case 322:
                $name = 'FirstName';
                break;
            case 323:
                $name = 'LastName';
                break;
            case 324:
                $name = 'Street1';
                break;
            case 325:
                $name = 'City';
                break;
            case 326:
                $name = 'State';
                break;
            case 327:
                $name = 'PCode';
                break;
            case 328:
                $name = 'Mobile';
                break;
            case 329:
                $name = 'Email1st';
                break;
            case 330:
                $name = 'Gender';
                break;
            case 331:
                $name = 'Tier';
                break;
            default:
                $name = '';
        }
        return $name;
    }

    function updateBepozGamingAccount($bepoz, $member, $standardfieldpayload, $customfieldpayload, $request) {
        // UPDATE bepoz user data directly
        $groupID = $member->tiers()->first()->bepoz_group_id;
        $template = $member->tiers()->first()->bepoz_template_id;

        // get Account Bepoz
        $result = $bepoz->AccountFullGet($member->bepoz_account_id);
        // Log::warning($result);
        if ($result) {
            $requestAccountFull = $result['AccountFull'];
            $requestAccountFull['AccNumber'] = $member->bepoz_account_number;
            $requestAccountFull['CardNumber'] = $member->bepoz_account_card_number;
            $requestAccountFull['GroupID'] = $groupID;
            //$requestAccountFull['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
            // $requestAccountFull['FirstName'] = str_replace('"', '', $member->first_name);
            // $requestAccountFull['LastName'] = str_replace('"', '', $member->last_name);
            // $requestAccountFull['DateBirth'] = Carbon::createFromFormat('d/m/Y', $request->input('dob'))->format('Y-m-d');
            // $requestAccountFull['Mobile'] = $member->user->mobile;
            $requestAccountFull['PhoneHome'] = $member->phone;
            // $requestAccountFull['Email1st'] = $member->user->email;
            $requestAccountFull['Status'] = 0;

            foreach ($standardfieldpayload as $sf) {
                $bepozFieldName = $this->bepozAccField($sf['bepozFieldNum']);
                
                if ($bepozFieldName == "Tier") {
                    // NO NEED
                } else if ($bepozFieldName == "Password") {
                    // NO NEED
                } else if ($bepozFieldName == "DateBirth") {
                    $requestAccountFull[$bepozFieldName] = Carbon::createFromFormat('d/m/Y', $sf['value'])->format('Y-m-d');
                } else {
                    $requestAccountFull[$bepozFieldName] = $sf['value'];
                }
            }

            // $address = Address::where('member_id', $member->id)->first();
            // if (!is_null($address)) {
            //     $payload = \GuzzleHttp\json_decode($address->payload);
            //     $requestAccountFull['Street1'] = isset($payload->display->unit) ? $payload->display->unit : "";
            //     $requestAccountFull['Street2'] = isset($payload->display->street) ? $payload->display->street : "";
            //     //$requestAccountFull['Street3'] = $payload->display->street;
            //     $requestAccountFull['State'] = isset($payload->display->state) ? $payload->display->state : "";
            //     $requestAccountFull['City'] = isset($payload->display->suburb) ? $payload->display->suburb : "";
            //     $requestAccountFull['PCode'] = isset($payload->display->postcode) ? $payload->display->postcode : "";
            // }

            if (!is_null($member->member_tier->date_expiry)) {
                $date_expiry = Carbon::parse($member->member_tier->date_expiry);
                $date_expiry->setTimezone(config('app.timezone'));
                $requestAccountFull['DateTimeExpiry'] = $date_expiry->format('Y-m-d\TH:i:s');
            }

            unset($requestAccountFull['AddressID']);
            unset($requestAccountFull['CommentID']);

            $result = $bepoz->AccUpdate($requestAccountFull, $template);
        }

        // UPDATE bepozcustomfield directly
        $settingbepozcustomfield = Setting::where('key', 'bepozcustomfield')->first();
        $customFields = array();
        $allCustomField = \GuzzleHttp\json_decode($settingbepozcustomfield->extended_value);

        foreach ($customfieldpayload as $cf) {
            $typeCode = 0;
            switch ($cf["fieldType"]) {
                case "Text":
                    $typeCode = 3;
                    break;
                case "Number":
                    $typeCode = 2;
                    break;
                case "Date":
                    $typeCode = 1;
                    break;
                case "Dropdown":
                    $typeCode = 3;
                    break;
                case "Checkbox":
                    $typeCode = 3;
                    break;
                case "Toggle":
                    $typeCode = 0;
                    break;
            }

            $bepozFieldNum = $cf["bepozFieldNum"];
            if (strlen(trim($cf["bepozFieldNum"])) == 3) {
                $bepozFieldNum = substr($cf["bepozFieldNum"], -2);
            }

            // echo print_r($cf, true)."<br><br>";
            $customFields['CustomField'][] = array(
                'FieldName' => $cf["fieldName"],
                'FieldType' => $typeCode,
                'FieldValue' => $cf["value"],
                'FieldNum' => $bepozFieldNum
            );
        }

        $EntityType = 0;
        $EntityID = $member->bepoz_account_id;

        $customFieldResult = $bepoz->CustomFieldsSet($EntityType, $EntityID, $customFields);

    }

    /**
     * change Preferred Venue
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changePreferredVenue(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                // 'refresh_token' => 'required',
                'venue_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $user = $request->input('decrypted_token');
            $cloned = $user->member->replicate();

            $user->member->current_preferred_venue = $request->input('venue_id');

            $venue = Venue::find($request->input('venue_id'));
            $user->member->current_preferred_venue_name = $venue->name;
            if (isset($venue->venue_tags[0])){
                if ( !empty($user->member->payload) ) {
                    $memberPayload = \GuzzleHttp\json_decode($user->member->payload, true);
                } else {
                    $memberPayload = [];
                }
                $memberPayload['state_id'] = $venue->venue_tags[0]->id;
                $memberPayload['state_name'] = $venue->venue_tags[0]->name;
                $user->member->payload = \GuzzleHttp\json_encode($memberPayload);
            }
            $user->member->save();
            
            $tier = $user->member->member_tier;
            if ($tier->venue_id > 0) {
                $user->member->member_tier->tier_id = $venue->tier->id;
                $user->member->member_tier->save();
            }

            $ml = new MemberLog();
            $ml->before = $cloned->toJson();
            $ml->member_id = $user->member->id;
            $ml->message = $request->input('decrypted_token')->member->first_name . '(' . $request->input('decrypted_token')->member->id . ') has updated his/her preferred_venue. ';
            $ml->after = $user->member->toJson();
            $ml->save();
                        
            if (!is_null($user->member->bepoz_account_id)) {
                $data = array();
                $data['member_id'] = $user->member->id;

                $job = new BepozJob();
                $job->queue = "update-account";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();
            }

            // //CHECK SSO FIRST
            // $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/refresh";
            // $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

            // $datasend = array(
            //     'refresh_token' => $request->input('refresh_token'),
            //     "license_key" => $your_order_key
            // );

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            // curl_setopt($ch, CURLOPT_URL, $your_order_api);
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
            // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            // curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            // curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            // $content = curl_exec($ch);
            // $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // if ($content) {
            //     $payload = \GuzzleHttp\json_decode($content);
            //     if ($payload->status == "error" && $payload->error == "user_not_found") {
            //         return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
            //     }
            //     if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
            //         return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
            //     }
            // } else {
            //     return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
            // }

            // $token = JWTAuth::refresh(JWTAuth::getToken());

            // return response()->json(['status' => 'ok', 'message' => 'Updating Preferred Venue',
            //     'token' => $token, 'refresh_token' => $request->input('refresh_token'),
            //     'access_token' => $payload->data->access_token, 'id_token' => $payload->data->id_token ]);

            return response()->json(['status' => 'ok', 'message' => 'Updating Preferred Venue']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Change User Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changePassword(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $user = $request->input('decrypted_token');

            $old_password = $request->input('old_password');

            // Log::error('old_password '.$old_password);
            // Log::error(Hash::check($old_password, $user->password));

            if (Hash::check($old_password, $user->password)) {

                if ($request->has('new_password')) {
                    $user->password = Hash::make($request->input('new_password'));
                    $user->save();

                    $email = $user->email;
                    $password = $request->input('new_password');

                    //NEW LOGIC SEND INTEGRATION BASED ON VENUE
                    $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/changePassword";
                    $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                    $datasend = array(
                        'email' => $email,
                        'password' => $password,
                        "license_key" => $your_order_key
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $your_order_api);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                    $content = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    if ($content) {
                        $payload = \GuzzleHttp\json_decode($content);
                        if ($payload->status == "error" && $payload->error == "user_not_found") {
                            return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                        }
                        if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                            return response()->json(['status' => 'error', 'message' => 'Something wrong with configuration, please contact Administrator'], Response::HTTP_BAD_REQUEST);
                        }

                        $token = JWTAuth::refresh(JWTAuth::getToken());
                        return response()->json(['status' => 'ok', 'message' => 'Changing Password Successful',
                            'token' => $token, 'access_token' => $payload->data->access_token,
                            'id_token' => $payload->data->id_token, 'refresh_token' => $payload->data->refresh_token]);
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                    }

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Missing new password'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'Your old password is incorrect.'], Response::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Save image url in database
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadPhoto(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {

            $validator = Validator::make($request->all(), [
                'photo' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $user = $request->input('decrypted_token');

            if ($request->hasFile('photo')) {
                if (config('bucket.s3')) {
                    $picture = $request->file('photo');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('member/' . $user->member->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');
                    $user->member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $user->member->id . "/profile." . $type . "?" . time();
                    $user->member->save();

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'UserController.php/uploadPhoto';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'UserController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            return response()->json(['status' => 'ok', 'message' => 'upload_photo_successful']);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Save extra image url in database
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadExtraPhoto(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {

            $validator = Validator::make($request->all(), [
                'photo' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $user = $request->input('decrypted_token');

            if ($request->hasFile('photo')) {
                if (config('bucket.s3')) {
                    $picture = $request->file('photo');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('member/' . $user->member->id . '/extra' . '.' . $type, file_get_contents($picture), 'public');
                    $user->member->extra_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $user->member->id . "/extra." . $type . "?" . time();
                    $user->member->save();
                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'UserController.php/uploadExtraPhoto';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'UserController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            return response()->json(['status' => 'ok', 'message' => 'upload_photo_successful']);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * change Email
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeEmail(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $user = $request->input('decrypted_token');

            if ($user->email_confirmation) {
                return response()
                    ->json(['status' => 'error', 'message' => 'You are not allowed to change email. You are already confirmed.'], Response::HTTP_BAD_REQUEST);
            }

            if (Hash::check($request->input('password'), $user->password)) {
                $user->email = $request->input('email');
                $user->save();

                $confirmationToken = $user->generateEmailConfirmationToken();

                $platform = Platform::where('name', 'web')->first();
                $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;

                // Send Confirmation Email
                $data = array(
                    'URL' => $url
                );

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'confirmation',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'confirmation',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }


                $job = (new SendReminderNotification($user))->delay(30);
                dispatch($job);

            } else {
                return response()->json(['status' => 'error', 'message' => 'Incorrect password.'], Response::HTTP_BAD_REQUEST);
            }

            $token = JWTAuth::fromUser($user);
            return response()->json(['status' => 'ok', 'message' => 'Updating Email Successful', 'token' => $token]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * sendSSO
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSSO($your_order_api, $datasend, $cloned, $member)
    {
            $emailChangeCheck = contactChange::where('new_email', $datasend['email'])->first();
            
            // Log::info($emailChangeCheck);

            if (is_null($emailChangeCheck))
            {
                // IF NOT FOUND EMAIL CHANGE, THEN SEND
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $your_order_api);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                $content = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                
                // Log::info($content);
                // Log::info($httpCode);
    
                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);
                    
                    if ($payload->status == "error") {
                        $log = new SystemLog();
                        $log->type = 'sso-problem';
                        $log->humanized_message = 'SSO problem. Please check the error message';
                        $log->message = json_encode($payload);
                        $log->source = 'Member/UserController.php/sendSSO';
                        $log->save();
                    }
    
                    if (isset($payload->message)) {
                        // Log::info($payload->message);
                        if ($payload->message == "Please confirm your changes") {
                            
                            $contactChange = new contactChange;
                            $contactChange->member_id = $member->id;
                            $contactChange->user_id = $member->user->id;
                            $contactChange->old_email = $cloned->user->email;
                            $contactChange->new_email = $payload->data->email;
                            $contactChange->old_guid = $member->login_token;
                            $contactChange->new_guid = $payload->data->guid;
                            $contactChange->hash_key = $payload->data->hash_key;
                            $contactChange->email_confirmation_sent = Carbon::now(config('app.timezone'));
                            $contactChange->save();

    
                            $ml = new MemberLog();
                            $ml->before = $cloned->toJson();
                            $ml->member_id = $member->id;
                            $ml->message = 'Email has changed by Bepoz system (Poll account). Need confirm.';
                            $ml->after = $member->toJson();
                            $ml->save();
    
                            // Log::info("sendconfirmChangeEmail");
                            $this->sendconfirmChangeEmail($member->user, $contactChange->new_email, $payload->data->hash_key);
    
                        }
                    }
    
                    if (isset($payload->message)) {
                        if ($payload->message == "Updating account is successful") {
                            $ml = new MemberLog();
                            $ml->before = $cloned->toJson();
                            $ml->member_id = $member->id;
                            $ml->message = 'member details change by Bepoz system (Poll account)';
                            $ml->after = $member->toJson();
                            $ml->save();
                        }
                    }
    
                } else {
                    // return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                    $log = new SystemLog();
                    $log->type = 'sso-problem';
                    $log->humanized_message = 'SSO problem. Please check the error message';
                    $log->message = 'Unable to connect';
                    $log->source = 'Member/UserController.php/sendSSO';
                    $log->save();
                }
            }

    }

    /**
     * Send Confirm Change Email
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendconfirmChangeEmail($user, $email, $hash_key)
    {
        // Get web app token
        $platform = Platform::where('name', 'web')->first();

        $url = config('bucket.APP_URL') . '/api/confirmChangeEmail/' . $hash_key . '?app_token=' . $platform->app_token;

        $data = array(
            'URL' => $url,
            'OLD_EMAIL' => $user->email,
            'NEW_EMAIL' => $email
        );
        // Log::info($data);
        $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

        if ($email_server === "mandrill") {
            $mx = new MandrillExpress;
            if ($mx->init()) {
                $mx->send('confirmation_change_email', $user->member->first_name . ' ' . $user->member->last_name, $email, $data);
            } else {
                $pj = new PendingJob();
                $pj->payload = \GuzzleHttp\json_encode($data);
                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                    'email' => $email,
                    'category' => 'confirmation_change_email',
                    'name' => $user->member->first_name . ' ' . $user->member->last_name
                ));
                $pj->queue = "email";
                $pj->save();
            }

        } else if ($email_server === "mailjet") {
            $mjx = new MailjetExpress;
            if ($mjx->init()) {
                $mjx->send('confirmation_change_email', $user->member->first_name . ' ' . $user->member->last_name, $email, $data);
            } else {
                $pj = new PendingJob();
                $pj->payload = \GuzzleHttp\json_encode($data);
                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                    'email' => $email,
                    'category' => 'confirmation_change_email',
                    'name' => $user->member->first_name . ' ' . $user->member->last_name
                ));
                $pj->queue = "email";
                $pj->save();
            }

        }

        $user->email_confirmation_sent = Carbon::now(config('app.timezone'));
        $user->save();
    }

    /**
     * Change Mobile
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeMobile(Request $request, Bepoz $bepoz)
    {
        try {
            $validator = Validator::make($request->all(), [
                'mobile' => 'required',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }
            
            $user = $request->input('decrypted_token');
            $mobile = $request->input('mobile');

            $sso_response = $this->checkLoginSSO($user->email, $request->input('password'));
            // Log::info($sso_response);

            if ($sso_response['status'] == 'ok') {
                // Log::info("changeMobile password match");
                $member = $user->member;                
                
                $canchange = false;
                $bepozChange = false;
                $myplaceChange = false;
                $SSOChange = false;

                if ($this->checkSSOAccountByMobile($mobile) == "ok") {
                    $mobileSSOCan = false;
                    return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE') ], Response::HTTP_BAD_REQUEST);
                } else if ($this->checkSSOAccountByMobile($mobile) == "Unable to connect SSO") {
                    $mobileSSOCan = false;
                    return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_SSO') ], Response::HTTP_BAD_REQUEST);
                } else {
                    $SSOChange = true;
                }

                // ANY PHONE NUMBER
                $checkbepozmobile = $bepoz->AccountSearch(6, $mobile);
                
                if ($checkbepozmobile) {
                    if (intval($checkbepozmobile['IDList']['Count']) > 0) {
                        $bepozChange = false;
                        // it means more than 1 mobile duplicated
                        return response()->json(['status' => 'error', 'message' => config('bucket.DUPLICATED_MOBILE') ], Response::HTTP_BAD_REQUEST);
                    } else if (intval($checkbepozmobile['IDList']['Count']) == 0) {
                        $bepozChange = true;
                    }
                } else {
                    $bepozChange = false;
                    return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_BEPOZ') ], Response::HTTP_BAD_REQUEST);
                }

                $checkMobile = User::where('mobile', $mobile)->first();
                
                if ( is_null($checkMobile) ){
                    // CAN CONTINUE
                    $myplaceChange = true;
                } else {
                    $myplaceChange = false;
                    return response()->json(['status' => 'error', 'message' => 'Duplicate Mobile already found in Myplace'], Response::HTTP_BAD_REQUEST);
                }
                
                $canchange = $SSOChange && $bepozChange && $myplaceChange;

                // Log::info("canchange ".$canchange);
                if ($canchange){
                    $cloned = $member->replicate();

                    $contactChange = new ContactChange;
                    $contactChange->member_id = $member->id;
                    $contactChange->user_id = $member->user->id;
                    $contactChange->old_mobile = $cloned->user->mobile;
                    $contactChange->new_mobile = $request->input('mobile');
                    $contactChange->old_guid = $member->login_token;
                    $contactChange->token = $this->generateRandomString(32);
                    $contactChange->mobile_confirmation_sent = Carbon::now(config('app.timezone'));
                    $contactChange->save();

                    // Get web app token
                    $platform = Platform::where('name', 'web')->first();
                    $url = config('bucket.APP_URL') . '/api/confirmMobile/' . $contactChange->token . '?app_token=' . $platform->app_token;
                    
                    $sms_mobile_change_confirmation_message = Setting::where('key', 'sms_mobile_change_confirmation_message')->first()->value;

                    $message = $sms_mobile_change_confirmation_message;
                    $tier_name = $member->member_tier->tier->name;

                    $message = preg_replace(['{{{FirstName}}}', '{{{LastName}}}', '{{{AccountNumber}}}', '{{{Tier}}}', '{{{Link}}}'],
                        [$member->first_name, $member->last_name, $member->bepoz_account_number, $tier_name, $url], $message);
                    
                    $api = new \App\Helpers\TransmitSMSAPI();
                    $result = $api->sendSms($message, $request->input('mobile'), "");
                    
                    return response()->json(['status' => 'ok', 'message' => 'SMS sent', 'timestamp' => $request->input('timestamp'), 'request' => $request->all()]);
                }                

            } else if ($sso_response['status'] == 'error') {
                if ( $sso_response['message'] == 'Unable to connect SSO'){
                    $sso_response['message'] = config('bucket.UNABLE_CONNECT_SSO');
                } else if ( $sso_response['error'] == 'invalid_email_password'){
                    $sso_response['message'] = config('bucket.EMAIL_PASSWORD_INCORRECT');
                }
                return response()->json(['status' => 'error', 'message' => $sso_response['message'] ], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'error', 'message' => config('bucket.UNABLE_CONNECT_SSO') ], Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function checkLoginSSO($email, $password)
    {
        //NEW LOGIC SEND INTEGRATION BASED ON VENUE
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datasend = array(
            'email' => $email,
            'password' => $password,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api."/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        
        if ($content) {
            if ($httpCode == 200) {

                $payload = \GuzzleHttp\json_decode($content);

                if ($payload->status == "ok") {
                    $formatted_response = [
                        'status' => 'ok',
                        'error' => 'no',
                        'message' => 'ok',
                        'data' => $payload
                    ];
                } else {
                    $formatted_response = [
                        'status' => 'error',
                        'error' => $payload->error,
                        'message' => $payload->message,
                        'data' => null
                    ];
                }
            } else if ($httpCode < 404) {
                
                $payload = \GuzzleHttp\json_decode($content);

                $formatted_response = [
                    'status' => 'error',
                    'error' => $payload->error,
                    'message' => $payload->message,
                    'data' => null
                ];
            } else if ($httpCode == 404) {
                $formatted_response = [
                    'status' => 'error',
                    'error' => "connection",
                    'message' => "Unable to connect SSO",
                    'data' => null
                ];
            }
        } else {
            $formatted_response = [
                'status' => 'error',
                'error' => "connection",
                'message' => "Unable to connect SSO",
                'data' => null
            ];
        }
        return $formatted_response;
    }
    
    protected function checkSSOAccountByMobile($mobile) {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            "mobile" => $mobile,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api."/checkSubAccountByMobile");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);
        
        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return true;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }

    /**
     * resend Confirmations
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resendConfirmations(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            $user = $request->input('decrypted_token');

            if ($user->gmail == 1 || $user->facebook == 1 || $user->email_confirmation == 1) {
                return response()->json(['status' => 'error', 'message' => 'You are already confirmed.'], Response::HTTP_BAD_REQUEST);
            }

            // Get web app token
            $platform = Platform::where('name', 'web')->first();
            $confirmationToken = $user->generateEmailConfirmationToken();

            // LOAD GAMING SETTING
            $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;

            if ($gaming_system_enable == "true") {
                $gaming_system = Setting::where('key', 'gaming_system')->first()->value;

                if ($gaming_system == "igt") {
                    $url = config('bucket.APP_URL') . '/api/confirmIGT/' . $confirmationToken . '?app_token=' . $platform->app_token;
                } else if ($gaming_system == "odyssey") {
                    $url = config('bucket.APP_URL') . '/api/confirmOdyssey/' . $confirmationToken . '?app_token=' . $platform->app_token;
                }

            } else {
                $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;
            }

            // Send Confirmation Email
            $data = array(
                'URL' => $url
            );

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'confirmation',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'confirmation',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            $job = (new SendReminderNotification($user))->delay(30);
            dispatch($job);

            return response()->json(['status' => 'ok', 'message' => 'Confirmation emails have been successfully sent.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    
    /**
     * resendConfirmationSMS through our API
     * >> for member confirm via SMS
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resendConfirmationSMS(Request $request)
    {
        try {
            $user = $request->input('decrypted_token');
            $member = $user->member;

            // Get web app token
            $platform = Platform::where('name', 'web')->first();
            $confirmationToken = $user->generateEmailConfirmationToken();

            $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;

            $sms_confirmation_message = Setting::where('key', 'sms_confirmation_message')->first()->value;
            
            $message = $sms_confirmation_message;

            $message = preg_replace(['{{{firstname}}}', '{{{link}}}'],
            [$member->first_name, $url], $message);

            $tier_name = $member->member_tier->tier->name;

            $message = preg_replace(['{{{FirstName}}}', '{{{LastName}}}', '{{{AccountNumber}}}', '{{{Tier}}}', '{{{Link}}}'],
            [$member->first_name, $member->last_name, $member->bepoz_account_number, $tier_name, $url], $message);

            Log::info($message);
            
            $api = new \App\Helpers\TransmitSMSAPI();
            $result = $api->sendSms($message, $user->mobile, "");

            return response()->json(['status' => 'ok', 'message' => 'Confirmation emails have been successfully sent.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * verifyIDToken through our API
     * >> for member app access
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function verifyIDToken(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id_token' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            //CHECK SSO FIRST
            $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/verifyIDToken";
            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

            $datasend = array(
                'id_token' => $request->input('id_token'),
                "license_key" => $your_order_key
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $your_order_api);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($content) {
                $payload = \GuzzleHttp\json_decode($content);
                if ($payload->status == "error") {
                    return response()->json(['status' => 'error', 'message' => $payload->message, 'data' => $payload->error],
                        Response::HTTP_BAD_REQUEST);
                }

                $user = $request->input('decrypted_token');

                //DECODE ONLY SECOND PART OF ID TOKEN
                $explode = explode('.', $request->input('id_token'));
                $decoded_id_token = base64_decode($explode[1]);
                $decoded = json_decode($decoded_id_token, true);

                // Log::info("verifyIDToken");
                // Log::info($content);
                // Log::info($request->input('id_token'));
                // Log::info($explode[1]);
                // Log::info($decoded_id_token);

                if (isset($decoded['email'])) {
                    if (!empty($decoded['email'])) {

                        // CHECK EMAIL FIRST
                        if (strtolower($user->email) == strtolower($decoded['email'])) {

                            if (isset($decoded['family_name'])) {
                                if (!empty($decoded['family_name'])) {
                                    // Log::info($decoded['family_name']);
                                    $user->member->last_name = $decoded['family_name'];
                                    $user->member->save();
                                }
                            }
                            // Log::info("END verifyIDToken");

                            if (isset($decoded['given_name'])) {
                                if (!empty($decoded['given_name'])) {
                                    $user->member->first_name = $decoded['given_name'];
                                    $user->member->save();
                                }
                            }

                            if (isset($decoded['birthdate'])) {
                                if (!empty($decoded['birthdate'])) {
                                    $user->member->dob = Carbon::parse($decoded['birthdate']);
                                    $user->member->save();
                                }
                            }

                            if (isset($decoded['phone_number'])) {
                                if (!empty($decoded['phone_number'])) {
                                    $user->mobile = $decoded['phone_number'];
                                    $user->save();
                                }
                            }

                            if (isset($decoded['guid'])) {
                                if (!empty($decoded['guid'])) {
                                    $user->member->login_token = $decoded['guid'];
                                    $user->member->save();
                                }
                            }
                        }
                    }
                }

                // if (isset($decoded['claims']['test_stripe_customer_token'])){
                //     if (!empty($decoded['claims']['test_stripe_customer_token'])){
                //         $customerTest = StripeCustomer::where("member_id", $user->member->id)->where("test_mode", 1)->first();
                //         if (is_null($customerTest)){
                //             $customer = new StripeCustomer;
                //             $customer->member_id = $user->member->id;
                //             $customer->customer_id = $decoded['claims']['test_stripe_customer_token'];
                //             $customer->payload = null;
                //             $customer->test_mode = (bool) true;
                //             $customer->save();
                //         } else {
                //             $customerTest->customer_id = $decoded['claims']['test_stripe_customer_token'];
                //             $customerTest->save();
                //         }
                //     }
                // }

                // if (isset($decoded['claims']['live_stripe_customer_token'])){
                //     if (!empty($decoded['claims']['live_stripe_customer_token'])){
                //         $customerLive = StripeCustomer::where("member_id", $user->member->id)->where("test_mode", 0)->first();
                //         if (is_null($customerLive)){
                //             $customer = new StripeCustomer;
                //             $customer->member_id = $user->member->id;
                //             $customer->customer_id = $decoded['claims']['live_stripe_customer_token'];
                //             $customer->payload = null;
                //             $customer->test_mode = (bool) false;
                //             $customer->save();
                //         } else {
                //             $customerLive->customer_id = $decoded['claims']['live_stripe_customer_token'];
                //             $customerLive->save();
                //         }
                //     }
                // }

                // $base64 = base64_decode($payload->data->id_token);
                // $base64 = substr($base64, 0, strpos($base64, "}}"))."}}";
                // $base64 = str_replace('{"typ":"JWT","alg":"HS256"}', '', $base64);
                // $json = json_decode($base64);
                // $login_token = $json->claims->guid;

            } else {
                return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'message' => 'Token is valid', 'data' => 'valid_token']);

        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * refreshSSO through our API
     * >> for member app access
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function refreshSSO(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'refresh_token' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            //CHECK SSO FIRST
            $your_order_api = Setting::where('key', 'your_order_api')->first()->value . "/refresh";
            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

            $datasend = array(
                'refresh_token' => $request->input('refresh_token'),
                "license_key" => $your_order_key
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $your_order_api);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($content) {
                $payload = \GuzzleHttp\json_decode($content);

                if ($payload->status == "error") {
                    return response()->json(['status' => 'error', 'message' => $payload->message, 'data' => $payload->error],
                        Response::HTTP_BAD_REQUEST);
                }

                $user = $request->input('decrypted_token');

                //DECODE ONLY SECOND PART OF ID TOKEN
                $explode = explode('.', $payload->data->id_token);
                $decoded_id_token = base64_decode($explode[1]);
                $decoded = json_decode($decoded_id_token, true);

                // Log::info("refreshSSO");
                // Log::info($request->input('refresh_token'));
                // Log::info($content);
                // Log::info($payload->data->id_token);
                // Log::info($explode[1]);
                // Log::info($decoded_id_token);
                // Log::info("END refreshSSO");

                if (isset($decoded['family_name'])) {
                    if (!empty($decoded['family_name'])) {
                        // Log::info($decoded['family_name']);
                        $user->member->last_name = $decoded['family_name'];
                        $user->member->save();
                    }
                }

                if (isset($decoded['given_name'])) {
                    if (!empty($decoded['given_name'])) {
                        $user->member->first_name = $decoded['given_name'];
                        $user->member->save();
                    }
                }

                if (isset($decoded['birthdate'])) {
                    if (!empty($decoded['birthdate'])) {
                        $user->member->dob = Carbon::parse($decoded['birthdate']);
                        $user->member->save();
                    }
                }

                if (isset($decoded['phone_number'])) {
                    if (!empty($decoded['phone_number'])) {
                        $user->mobile = $decoded['phone_number'];
                        $user->save();
                    }
                }

                // if (isset($decoded['claims']['test_stripe_customer_token'])){
                //     if (!empty($decoded['claims']['test_stripe_customer_token'])){
                //         $customerTest = StripeCustomer::where("member_id", $user->member->id)->where("test_mode", 1)->first();
                //         if (is_null($customerTest)){
                //             $customer = new StripeCustomer;
                //             $customer->member_id = $user->member->id;
                //             $customer->customer_id = $decoded['claims']['test_stripe_customer_token'];
                //             $customer->payload = null;
                //             $customer->test_mode = (bool) true;
                //             $customer->save();
                //         } else {
                //             $customerTest->customer_id = $decoded['claims']['test_stripe_customer_token'];
                //             $customerTest->save();
                //         }
                //     }
                // }

                // if (isset($decoded['claims']['live_stripe_customer_token'])){
                //     if (!empty($decoded['claims']['live_stripe_customer_token'])){
                //         $customerLive = StripeCustomer::where("member_id", $user->member->id)->where("test_mode", 0)->first();
                //         if (is_null($customerLive)){
                //             $customer = new StripeCustomer;
                //             $customer->member_id = $user->member->id;
                //             $customer->customer_id = $decoded['claims']['live_stripe_customer_token'];
                //             $customer->payload = null;
                //             $customer->test_mode = (bool) false;
                //             $customer->save();
                //         } else {
                //             $customerLive->customer_id = $decoded['claims']['live_stripe_customer_token'];
                //             $customerLive->save();
                //         }
                //     }
                // }

                // $base64 = base64_decode($payload->data->id_token);
                // $base64 = substr($base64, 0, strpos($base64, "}}"))."}}";
                // $base64 = str_replace('{"typ":"JWT","alg":"HS256"}', '', $base64);
                // $json = json_decode($base64);
                // $login_token = $json->claims->guid;

            } else {
                return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'access_token' => $payload->data->access_token,
                'id_token' => $payload->data->id_token]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * refreshSSO through our API
     * >> for member app access
     * >> with validation
     * >> return jwt token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function isBepozAccountCreated(Request $request, Bepoz $bepoz)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;

            if ($gaming_system_enable == "true") {
                $email = preg_replace('/\s+/', '+', $request->input('email'));

                $user = User::where('email', strtolower($email))->first();
                $member = $user->member;
                $groupID = $member->member_tier->tier->bepoz_group_id;
                $template = $member->tiers()->first()->bepoz_template_id;

                // search Bepoz Account from email
                $result = $bepoz->accountSearch(8, $user->email);

                if ($result) {
                    if ($result['IDList']['Count'] == 0) {
                        // ACCOUNT NOT FOUND
                        return response()->json(['status' => 'error', 'message' => "Account not found. Please contact administrator."], Response::HTTP_BAD_REQUEST);

                    } else if ($result['IDList']['Count'] == 1) {
                        // 1 account
                        $temp = $bepoz->AccountFullGet($result['IDList']['ID']);

                        if ($temp['AccountFull']['Status'] == 0) { // if the status is active

                            // if ($temp['AccountFull']['AccNumber'] == $member->bepoz_account_number) { // if the status is active
                                // there is acc number
                                $member->bepoz_account_id = $temp['AccountFull']['AccountID'];
                                $member->bepoz_account_number = $temp['AccountFull']['AccNumber'];
                                $member->bepoz_account_card_number = $temp['AccountFull']['CardNumber'];
                                $member->bepoz_existing_account = true;
                                $member->bepoz_account_status = "OK";
                                $member->save();

                                $data = array(
                                    "member_id" => $member->id
                                );
                        
                                $job = new BepozJob();
                                $job->queue = "update-custom-field";
                                $job->payload = \GuzzleHttp\json_encode($data);
                                $job->available_at = time();
                                $job->created_at = time();
                                $job->save();

                                return response()->json(['status' => 'ok']);
                            // } else {
                            //     return response()->json(['status' => 'error', 'message' => "Account number not match. Please contact administrator."], Response::HTTP_BAD_REQUEST);

                            // }
                        } else {
                            return response()->json(['status' => 'error', 'message' => "Account inactive. Please contact administrator."], Response::HTTP_BAD_REQUEST);

                        }

                    } else {
                        // ACCOUNT FOUND MORE THAN 1
                        return response()->json(['status' => 'error', 'message' => "Can't match your Account. Please contact administrator."], Response::HTTP_BAD_REQUEST);

                    }

                } else {
                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'isGamingAccountCreated is failed. Please check log.';
                    $log->payload = $email;
                    $log->message = $result;
                    $log->source = 'isGamingAccountCreated.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => "Bepoz connection error. Please contact administrator."], Response::HTTP_BAD_REQUEST);
                }

            } else {
                return response()->json(['status' => 'error', 'message' => "Setting error. Please contact administrator."], Response::HTTP_BAD_REQUEST);
            }


        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * onesignalPlayerID through our API
     * >> for member app access
     * >> with validation
     * >> need to save onesignal player ID
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function onesignalPlayerID(Request $request)
    {
        
        try {
            $validator = Validator::make($request->all(), [
                'player_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $user = $request->input('decrypted_token');
            $user->member->onesignal = $request->input('player_id');
            $user->member->save();

        } catch (\Exception $e) {
            Log::error($e);
            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * communityPoints through our API
     * >> for member app access
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function communityPoints(Request $request)
    {        
        try {
            $validator = Validator::make($request->all(), [
                'member_number' => 'required',
                'venue_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $PondHoppers = new PondHoppers();        
            $memberNumber = "10071007";
            $venueId = "26";
            // $memberNumber = $request->input('member_number');
            // $venueId = $request->input('venue_id');
            $result = $PondHoppers->PondHoppers($memberNumber, $venueId);

            // $base_url = Setting::where('key', 'PondHoppers_url')->first()->value;
            // $base_url_clean = str_replace('/index.php', '', $base_url);
            // $result = str_replace('href="/', 'href="'.$base_url_clean.'/', $result);
            // $result = str_replace('src="/', 'src="'.$base_url_clean.'/', $result);

            return response()->json(['result' => $result]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * connectWithUs through our API
     * >> for member app access
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function connectWithUs(Request $request)
    {        
        try {
            $validator = Validator::make($request->all(), [
                'member_number' => 'required',
                'venue_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $PondHoppers = new PondHoppers();        
            $memberNumber = "10071007";
            $venueId = "26";
            // $memberNumber = $request->input('member_number');
            // $venueId = $request->input('venue_id');
            $result = $PondHoppers->CONTACT($memberNumber, $venueId);

            // $base_url = Setting::where('key', 'PondHoppers_url')->first()->value;
            // $base_url_clean = str_replace('/index.php', '', $base_url);
            // $result = str_replace('href="/', 'href="'.$base_url_clean.'/', $result);
            // $result = str_replace('src="/', 'src="'.$base_url_clean.'/', $result);

            return response()->json(['result' => $result]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * PondHoppers INFO through our API
     * >> for member app access
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function pondHoppersInfo(Request $request)
    {        
        try {
            $pond_hoppers_url = Setting::where('key', 'pond_hoppers_url')->first()->value;
            $pond_hoppers_unique_key = Setting::where('key', 'pond_hoppers_unique_key')->first()->value;

            return response()->json(['pond_hoppers_url' => $pond_hoppers_url, 
                'pond_hoppers_unique_key' => $pond_hoppers_unique_key]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * internal function for generate Random String
     */
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
