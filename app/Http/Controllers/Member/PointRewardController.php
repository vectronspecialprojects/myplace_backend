<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Member;
use App\PointReward;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PointRewardController extends Controller
{
    protected function claimReward(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'reward_type' => 'required',
                'claimed_at' => 'required',
                'event' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $bottom_range = Carbon::today(config('app.timezone'))->timestamp;
            $upper_range = Carbon::tomorrow(config('app.timezone'))->timestamp;

            $rows = DB::table('point_rewards')
                ->select(DB::raw(
                    'count(*) as total'
                ))
                ->whereRaw("point_rewards.reward_type = :reward_type", ['reward_type' => $request->input('reward_type')])
                ->whereRaw("point_rewards.event = :event", ['event' => $request->input('event')])
                ->whereRaw("point_rewards.member_id = :member_id", ['member_id' => $request->input('decrypted_token')->member->id])
                ->whereRaw("point_rewards.claimed_at >= :bottom_range", ['bottom_range' => $bottom_range])
                ->whereRaw("point_rewards.claimed_at < :upper_range", ['upper_range' => $upper_range])
                ->whereNull('point_rewards.deleted_at')
                ->first();

            $point = 0;
            if ($rows->total == 0) {

                if ($request->input('reward_type') == "facebook") {
                    $setting = Setting::find(6);
                } else if ($request->input('reward_type') == "twitter") {
                    $setting = Setting::find(7);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Undefined reward type'], Response::HTTP_BAD_REQUEST);
                }

                $point = intval($setting->value);

                $member = Member::find($request->input('decrypted_token')->member->id);
                $member->points = intval($member->points) + $point;
                $member->save();

                $reward = new PointReward();
                $reward->reward_type = $request->input('reward_type');
                $reward->event = $request->input('event');
                $reward->claimed_at = $request->input('claimed_at');
                $reward->member_id = $request->input('decrypted_token')->member->id;
                $reward->point_reward = $point;
                $reward->save();
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Claiming reward is successful.', 'points' => $point]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
