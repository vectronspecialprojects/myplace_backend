<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\MemberSystemNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    /**
     * Return all notifications
     */
    protected function index(Request $request)
    {
        try {
            $notifications = MemberSystemNotification::with('notification')
                ->where('member_id', '=', $request->input('decrypted_token')->member->id)
                ->where('status', '<>', 'read')
                ->get();

            return response()->json(['status' => 'ok', 'data' => $notifications]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a notification
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function clear(Request $request, $id)
    {
        try {
            
            if ($id == '0'){
                // CLEAR ALL
                $member = $request->input('decrypted_token')->member;
                $date = Carbon::now(config('app.timezone'));
                DB::beginTransaction();

                $sn = MemberSystemNotification::where('member_id', $member->id)
                    ->update(['status' => 'read', 'read_at' => $date]);
                
                DB::commit();
            } else {
                DB::beginTransaction();

                $sn = MemberSystemNotification::find($id);
                $sn->status = 'read';
                $sn->read_at = Carbon::now(config('app.timezone'));
                $sn->save();
    
                DB::commit();
            }

            return response()->json(['status' => 'ok', 'message' => 'Updating member is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }

    }
}
