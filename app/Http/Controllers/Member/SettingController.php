<?php

namespace App\Http\Controllers\Member;

use App\Helpers\IGT;
use App\Http\Controllers\Controller;
use App\Setting;
use App\StripeCustomer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class SettingController extends Controller
{
    /**
     * Return point ratio
     * >> for member
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getPointRatio()
    {
        try {
            $setting = Setting::where('id', 4)->get();

            return response()->json(['status' => 'ok', 'data' => $setting]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function layoutSettings()
    {
        try {
            $tab_layout = Setting::where('key', 'tab_layout')->first();
            $total_tab_layout = Setting::where('key', 'tab_layout_total_buttons')->first();
            $front_menus = Setting::where('key', 'front_menu_buttons')->first();
            $side_menus_before_login = Setting::where('key', 'side_menu_before_login_buttons')->first();
            $total_side_before_login = Setting::where('key', 'side_menu_before_login_total_buttons')->first();
            $side_menus = Setting::where('key', 'side_menu_buttons')->first();
            $total_side = Setting::where('key', 'side_menu_total_buttons')->first();
            $total_front = Setting::where('key', 'front_menu_total_buttons')->first();
            $total_galleries = Setting::where('key', 'gallery_total_slots')->first();
            $galleries = Setting::where('key', 'galleries')->first();
            $total_welcome_instruction = Setting::where('key', 'welcome_instruction_total_slots')->first();
            $welcome_instruction = Setting::where('key', 'welcome_instruction')->first();
            $total_profile_menu = Setting::where('key', 'profile_menu_total_buttons')->first();
            $profile_menu = Setting::where('key', 'profile_menu_buttons')->first();
            $total_community_impact_galleries = Setting::where('key', 'community_impact_gallery_total_slots')->first();
            $community_impact_galleries = Setting::where('key', 'community_impact_gallery')->first();

            $settings = array();
            $arrayed_tabs = \GuzzleHttp\json_decode($tab_layout->extended_value, true);
            $total_current_tabs = count($arrayed_tabs);
            if (count($arrayed_tabs) !== intval($total_tab_layout->value)) {
                $diff = $total_current_tabs - intval($total_tab_layout->value);

                for ($i = 0; $i < $diff; $i++) {
                    array_pop($arrayed_tabs);
                }
            }
            $settings['tabs'] = $arrayed_tabs;

            $arrayed_front_menus = \GuzzleHttp\json_decode($front_menus->extended_value, true);
            $total_current_front_menus = count($arrayed_front_menus);
            if (count($arrayed_front_menus) !== intval($total_front->value)) {
                $diff = $total_current_front_menus - intval($total_front->value);

                for ($i = 0; $i < $diff; $i++) {
                    array_pop($arrayed_front_menus);
                }
            }
            $settings['home_menus'] = $arrayed_front_menus;

            $arrayed_side_menus_before_login = \GuzzleHttp\json_decode($side_menus_before_login->extended_value, true);
            $total_current_side_menus_before_login = count($arrayed_side_menus_before_login);
            if (count($arrayed_side_menus_before_login) !== intval($total_side_before_login->value)) {
                $diff = $total_current_side_menus_before_login - intval($total_side_before_login->value);

                for ($j = 0; $j < $diff; $j++) {
                    array_pop($arrayed_side_menus_before_login);
                }
            }
            $settings['side_menus_before_login'] = $arrayed_side_menus_before_login;

            $arrayed_side_menus = \GuzzleHttp\json_decode($side_menus->extended_value, true);
            $total_current_side_menus = count($arrayed_side_menus);
            if (count($arrayed_side_menus) !== intval($total_side->value)) {
                $diff = $total_current_side_menus - intval($total_side->value);

                for ($j = 0; $j < $diff; $j++) {
                    array_pop($arrayed_side_menus);
                }
            }
            $settings['side_menus'] = $arrayed_side_menus;
            // $settings['side_menu_location'] =  Setting::where('key', 'side_menu_location')->first()->value;
            $settings['side_menu_show_profile'] =  Setting::where('key', 'side_menu_show_profile')->first()->value;
            
            $arrayed_galleries = \GuzzleHttp\json_decode($galleries->extended_value, true);
            $total_current_galleries = count($arrayed_galleries);
            if (count($arrayed_galleries) !== intval($total_galleries->value)) {
                $diff = $total_current_galleries - intval($total_galleries->value);

                for ($j = 0; $j < $diff; $j++) {
                    array_pop($arrayed_galleries);
                }
            }
            $settings['galleries'] = $arrayed_galleries;

            // $community_impact_title = Setting::where('key', 'community_impact_title')->first()->value;
            $community_impact_content_option = Setting::where('key', 'community_impact_content_option')->first()->value;
            $community_impact_content_text = Setting::where('key', 'community_impact_content_text')->first()->value;
            // $community_impact_color = Setting::where('key', 'community_impact_color')->first()->value;
            // $community_impact_font_color = Setting::where('key', 'community_impact_font_color')->first()->value;
            $community_impact_button_option = Setting::where('key', 'community_impact_button_option')->first()->value;

            $settingsCommunityImpact = array();
            // $settingsCommunityImpact['community_impact_title'] = $community_impact_title;
            $settingsCommunityImpact['community_impact_content_option'] = $community_impact_content_option;
            $settingsCommunityImpact['community_impact_content_text'] = $community_impact_content_text;
            // $settingsCommunityImpact['community_impact_color'] = $community_impact_color;
            // $settingsCommunityImpact['community_impact_font_color'] = $community_impact_font_color;
            $settingsCommunityImpact['community_impact_button_option'] = $community_impact_button_option;

            $community_impact_arrayed_galleries = \GuzzleHttp\json_decode($community_impact_galleries->extended_value, true);
            $total_current_community_impact_galleries = count($community_impact_arrayed_galleries);
            if (count($community_impact_arrayed_galleries) !== intval($total_community_impact_galleries->value)) {
                $diff = $total_current_community_impact_galleries - intval($total_community_impact_galleries->value);

                for ($j = 0; $j < $diff; $j++) {
                    array_pop($community_impact_arrayed_galleries);
                }
            }
            $settingsCommunityImpact['community_impact_gallery'] = $community_impact_arrayed_galleries;

            $settings['community_impact'] = $settingsCommunityImpact;

            $arrayed_welcome_instruction = \GuzzleHttp\json_decode($welcome_instruction->extended_value, true);
            $total_current_welcome_instruction = count($arrayed_welcome_instruction);
            if (count($arrayed_welcome_instruction) !== intval($total_welcome_instruction->value)) {
                $diff = $total_current_welcome_instruction - intval($total_welcome_instruction->value);

                for ($j = 0; $j < $diff; $j++) {
                    array_pop($arrayed_welcome_instruction);
                }
            }
            $settings['welcome_instruction'] = $arrayed_welcome_instruction;
            $settings['welcome_instruction_enable'] =  Setting::where('key', 'welcome_instruction_enable')->first()->value;

            $arrayed_profile_menu = \GuzzleHttp\json_decode($profile_menu->extended_value, true);
            $total_current_profile_menu = count($arrayed_profile_menu);
            if (count($arrayed_profile_menu) !== intval($total_profile_menu->value)) {
                $diff = $total_current_profile_menu - intval($total_profile_menu->value);

                for ($j = 0; $j < $diff; $j++) {
                    array_pop($arrayed_profile_menu);
                }
            }
            $settings['profile_menu'] = $arrayed_profile_menu;
            
            $settings['use_legacy_design'] =  Setting::where('key', 'use_legacy_design')->first()->value;
            $settings['display_venue_selection'] =  Setting::where('key', 'display_venue_selection')->first()->value;

            // $enable_terms_condition_field = Setting::where('key', 'enable_terms_condition_field')->first()->value;
            // $settings['enable_terms_condition_field'] = $enable_terms_condition_field;

            $email_not_allowed_domains = Setting::where('key', 'email_not_allowed_domains')->first();
            $settings['email_not_allowed_domains'] = \GuzzleHttp\json_decode($email_not_allowed_domains->extended_value, true);

            $member_default_tier = Setting::where('key', 'member_default_tier')->first();
            $settings['member_default_tier_option'] = $member_default_tier->value;
            $settings['member_default_tier_detail'] = \GuzzleHttp\json_decode($member_default_tier->extended_value, true);

            $form_input_style = Setting::where('key', 'form_input_style')->first();
            $settings['form_input_style'] = $form_input_style->value;
            // $form_floating_title = Setting::where('key', 'form_floating_title')->first();
            // $settings['form_floating_title'] = $form_floating_title->value;

            $settings['pond_hoppers_url'] = Setting::where('key', 'pond_hoppers_url')->first()->value;
            $settings['pond_hoppers_unique_key'] = Setting::where('key', 'pond_hoppers_unique_key')->first()->value;
            
            $settings['enable_tier_icon_image_colour'] = Setting::where('key', 'enable_tier_icon_image_colour')->first()->value;

            $settings['pond_hoppers_enable'] = Setting::where('key', 'pond_hoppers_enable')->first()->value;

            $flags = [];            
            $flags['venue_number'] = Setting::where('key', 'venue_number')->first()->value;
            $flags['isMultipleVenue'] = $flags['venue_number'] == "multiple" ? true : false;
            $flags['gaming_system_enable'] = Setting::where('key', 'gaming_system_enable')->first()->value;
            $flags['gaming_system'] = Setting::where('key', 'gaming_system')->first()->value;
            $flags['app_name'] = Setting::where('key', 'app_name')->first()->value;
            $flags['app_default_website'] = Setting::where('key', 'app_default_website')->first()->value;
            $flags['app_is_show_logo'] = Setting::where('key', 'app_is_show_logo')->first()->value;
            $flags['app_account_match'] = Setting::where('key', 'app_account_match')->first()->value;
            $flags['app_is_show_star_bar'] = Setting::where('key', 'app_is_show_star_bar')->first()->value;
            $flags['app_show_tier_below_name'] = Setting::where('key', 'app_show_tier_below_name')->first()->value;
            $flags['app_show_points_below_name'] = Setting::where('key', 'app_show_points_below_name')->first()->value;
            $flags['app_is_show_preferred_venue'] = Setting::where('key', 'app_is_show_preferred_venue')->first()->value;
            $flags['app_show_tier_name'] = Setting::where('key', 'app_show_tier_name')->first()->value;
            $flags['app_on_boarding_enable'] = Setting::where('key', 'app_on_boarding_enable')->first()->value;
            $flags['app_is_group_filter'] = Setting::where('key', 'app_is_group_filter')->first()->value;
            $flags['app_is_component_shadowed'] = Setting::where('key', 'app_is_component_shadowed')->first()->value;
            $flags['app_sub_header_align_left'] = Setting::where('key', 'app_sub_header_align_left')->first()->value;
            
            $settings['flags'] = $flags;
            
            $default_message = Setting::where('key', 'default_message')->first();
            $settings['default_message'] = \GuzzleHttp\json_decode($default_message->extended_value, true);

            $default_color = Setting::where('key', 'default_color')->first();
            $settings['default_color'] = \GuzzleHttp\json_decode($default_color->extended_value, true);

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function legals()
    {
        try {
            $pp = Setting::where('key', 'privacy_policy')->first();
            $tc = Setting::where('key', 'terms_and_conditions')->first();
            $tct = Setting::where('key', 'terms_and_conditions_tab_title')->first();
            $ppt = Setting::where('key', 'privacy_policy_tab_title')->first();

            $settings = array();

            $privacy_policy = [];
            $privacy_policy['title'] = $ppt->value;
            $privacy_policy['content'] = $pp->extended_value;
            $privacy_policy['webview_enable'] = Setting::where('key', 'policy_webview_enable')->first()->value;
            $privacy_policy['webview_url'] = Setting::where('key', 'policy_webview_url')->first()->value;
            $settings['privacy_policy'] = $privacy_policy;
            
            $term = [];
            $term['title'] = $tct->value;
            $term['content'] = $tc->extended_value;
            $term['webview_enable'] = Setting::where('key', 'term_webview_enable')->first()->value;
            $term['webview_url'] = Setting::where('key', 'term_webview_url')->first()->value;
            $settings['terms_and_conditions'] = $term;

            // $settings['privacy_policy'] = [$ppt->value, $pp->extended_value];
            // $settings['terms_and_conditions'] = [$tct->value, $tc->extended_value];

            $privacy_policy_raw =  str_replace( "<br>", "\n", $pp->extended_value);
            $privacy_policy_clean  = strip_tags($privacy_policy_raw);
            $settingsclean['privacy_policy_clean'] = [$ppt->value, $privacy_policy_clean];

            $terms_and_conditions_raw =  str_replace( "<br>", "\n", $tc->extended_value);
            $terms_and_conditions_clean = strip_tags($terms_and_conditions_raw);
            $settingsclean['terms_and_conditions_clean'] = [$tct->value, $terms_and_conditions_clean];
            
            
            
            return response()->json(['status' => 'ok', 'data' => $settings, 'dataclean' => $settingsclean]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function about()
    {
        try {
            $pp = Setting::where('key', 'about')->first();

            $settings = array();
            $settings['about'] = $pp->extended_value;

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function referralMessage()
    {
        try {
            $pp = Setting::where('key', 'friend_referral_message')->first();

            $settings = array();
            $settings['friend_referral_message'] = $pp->value;

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function hourAndLocation()
    {
        try {
            $address = Setting::where('key', 'address')->first();
            $email = Setting::where('key', 'email')->first();
            $telp = Setting::where('key', 'telp')->first();
            $fax = Setting::where('key', 'fax')->first();
            $oc = Setting::where('key', 'open_and_close_hours')->first();
            $pd = Setting::where('key', 'pickup_and_delivery_hours')->first();
            $sl = Setting::where('key', 'social_links')->first();
            $ml = Setting::where('key', 'menu_links')->first();
            $hdi = Setting::where('key', 'hide_delivery_info')->first();
            $hohi = Setting::where('key', 'hide_opening_hours_info')->first();
            $il = Setting::where('key', 'ios_link')->first();
            $al = Setting::where('key', 'android_link')->first();

            $settings = array();
            $settings['address'] = $address->extended_value;
            $settings['email'] = $email->extended_value;
            $settings['telp'] = $telp->extended_value;
            $settings['fax'] = $fax->extended_value;
            $settings['ios_link'] = $il->extended_value;
            $settings['android_link'] = $al->extended_value;
            $settings['hide_delivery_info'] = $hdi->value === "true" ? true : false;
            $settings['social_links'] = \GuzzleHttp\json_decode($sl->extended_value, true);
            $settings['menu_links'] = \GuzzleHttp\json_decode($ml->extended_value, true);

            if ($hdi->value === "false") {
                $settings['pickup_and_delivery_hours'] = \GuzzleHttp\json_decode($pd->extended_value, true);
            }

            $settings['hide_opening_hours_info'] = $hohi->value === "true" ? true : false;

            if ($hohi->value === "false") {
                $settings['open_and_close_hours'] = \GuzzleHttp\json_decode($oc->extended_value, true);
            }

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function gamingSetting()
    {
        try {
               
            $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();

            $temp = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value, true);

            // filter need dispaly in app
            foreach ($temp as $key => $cf) {
                if (!$cf['displayInApp']) {
                    unset($temp[$key]);
                }
            }

            $gaming_mandatory_field->extended_value = array_values($temp);

            return response()->json(['status' => 'ok', 'gaming_mandatory_field' => $gaming_mandatory_field]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function gamingStateList($countryID)
    {
        try {
            $igt = new IGT();

            if (is_int($countryID)) {
                return response()->json(['status' => 'error', 'message' => 'countryID should be a number'], Response::HTTP_BAD_REQUEST);
            }

            $resultGetStatesList = $igt->GetStatesList($countryID);

            return response()->json(['status' => 'ok', 'data' => $resultGetStatesList]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function retrieveStripe(Request $request)
    {
        try {
            $stripe_test_mode = Setting::where('key', 'stripe_testmode')->first()->value;

            if ($stripe_test_mode === 'true') {
                $stripe_key = Setting::where('key', 'stripe_test_publishable_key')->first()->value;
                $stripe_secret = Setting::where('key', 'stripe_testkey')->first()->value;
            } else {
                $stripe_key = Setting::where('key', 'stripe_publishable_key')->first()->value;
                $stripe_secret = Setting::where('key', 'stripe_key')->first()->value;
            }

            // create stripe obj
            \Stripe\Stripe::setApiKey($stripe_secret);

            if ($stripe_test_mode === 'true') {
                $sc = StripeCustomer::where('member_id', $request->input('decrypted_token')->member->id)
                    ->where('test_mode', true)
                    ->first();

            } else {
                $sc = StripeCustomer::where('member_id', $request->input('decrypted_token')->member->id)
                    ->where('test_mode', false)
                    ->first();
            }

            if (!is_null($sc)) {
                $customer = \Stripe\Customer::retrieve($sc->customer_id);

                $cards = \Stripe\Customer::retrieve($sc->customer_id)->sources->all(array(
                    'object' => 'card'));
            } else {
                $customer = null;
                $cards = null;
            }

            return response()->json(['status' => 'ok', 'message' => 'Retrieve Stripe Data successful', 'data' => ['customer' => $customer, 'cards' => $cards, 'api' => $stripe_key]]);


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function retrieveStripeKey(Request $request)
    {
        try {
            $stripe_test_mode = Setting::where('key', 'stripe_testmode')->first()->value;

            if ($stripe_test_mode === 'true') {
                $stripe_key = Setting::where('key', 'stripe_test_publishable_key')->first()->value;
            } else {
                $stripe_key = Setting::where('key', 'stripe_publishable_key')->first()->value;
            }

            return response()->json(['status' => 'ok', 'message' => 'Retrieve Stripe Data successful', 'data' => ['key' => $stripe_key]]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
