<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class VenueController extends Controller
{
    /**
     * Return all venues
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request)
    {
        try {
            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            $venue_tags_enable = Setting::where('key', 'venue_tags_enable')->first()->value;

            $distance_order = Setting::where('key', 'distance_order')->first()->value;
            
            $venue_tag_color = Setting::where('key', 'venue_tag_color')->first()->value;
            $venue_tag_font_color = Setting::where('key', 'venue_tag_font_color')->first()->value;
            $venue_tag_click_color = Setting::where('key', 'venue_tag_click_color')->first()->value;
            $venue_tag_click_font_color = Setting::where('key', 'venue_tag_click_font_color')->first()->value;
            $venue_tag_bgcolor = Setting::where('key', 'venue_tag_bgcolor')->first()->value;

            $image = Setting::where('key', 'invoice_logo')->first()->value;
            $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
            $android_link = Setting::where('key', 'android_link')->first()->extended_value;
            $address = Setting::where('key', 'address')->first()->extended_value;
            $email = Setting::where('key', 'email')->first()->extended_value;
            $telp = Setting::where('key', 'telp')->first()->extended_value;
            $fax = Setting::where('key', 'fax')->first()->extended_value;
            $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
            $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
            $social_links = Setting::where('key', 'social_links')->first()->extended_value;
            $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
            $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->value;
            $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->value;

            $hqvenue = collect([
                'id' => 0,
                'name' => 'ALL VENUES',
                'image' => $image,
                'ios_link' => $ios_link,
                'android_link' => $android_link,
                'address' => $address,
                'email' => $email,
                'telp' => $telp,
                'fax' => $fax,
                'open_and_close_hours' => $open_and_close_hours,
                'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                'social_links' => $social_links,
                'menu_links' => $menu_links,
                'hide_delivery_info' => $hide_delivery_info === 'true'? true: false,
                'hide_opening_hours_info' =>  $hide_opening_hours_info === 'true'? true: false
            ]);

            if ($venue_number == 'single'){
                $venues = collect([$hqvenue]);

            } else if ($venue_number == 'multiple') {
                if ($request->has('tags_id')) {
                    $tags_id = $request->input('tags_id');
                    if (empty($tags_id)) {
                        $tags_id = '0';
                    }

                    if ($tags_id == '0'){
                        $venues = $this->googleDistanceMatrix();

                    } else {
                        $venues = $this->googleDistanceMatrix();
                    }
                } else {
                    $venues = $this->googleDistanceMatrix();
                }
                
            }

            $venueTagAll = VenueTag::
                with(
                    ['venue_pivot_tags' => function($q) {
                        $q->orderBy('display_order');
                    }]
                )
                ->where('status', 1)
                ->where('hide_in_app', 0)
                ->orderBy('display_order')
                ->get();

            // foreach ($venueTagAll as $key=>$venueTag) {
            //     foreach ($venueTag->venue_pivot_tags as $key=>$venue_pivot_tag) {
            //         Log::warning($venue_pivot_tag);
            //         $venue_pivot_tag->special_text = $venue_pivot_tag->venue->special_text;
            //         $venue_pivot_tag->color = $venue_pivot_tag->venue->color;
            //         $venue_pivot_tag->address = $venue_pivot_tag->venue->address;
            //         $venue_pivot_tag->distance = $venues[$venue_pivot_tag->venue_id]->distance;
            //         $venue_pivot_tag->distanceText = $venues[$venue_pivot_tag->venue_id]->distanceText;
            //     }
            // }

            // return response()->json(['status' => 'ok', 'data' => $venues, 'venue_tags' => $venueTag]);
            return response()->json(['status' => 'ok', 'data' => $venues, 'venue_tags' => $venueTagAll, 
                'venue_tags_enable' => $venue_tags_enable, 'venue_tag_bgcolor' => $venue_tag_bgcolor, 
                'distance_order' => $distance_order
            ]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function memberVenue(Request $request)
    {
        try {
            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            $venue_tags_enable = Setting::where('key', 'venue_tags_enable')->first()->value;
            
            $distance_order = Setting::where('key', 'distance_order')->first()->value;
            
            $venue_tag_color = Setting::where('key', 'venue_tag_color')->first()->value;
            $venue_tag_font_color = Setting::where('key', 'venue_tag_font_color')->first()->value;
            $venue_tag_click_color = Setting::where('key', 'venue_tag_click_color')->first()->value;
            $venue_tag_click_font_color = Setting::where('key', 'venue_tag_click_font_color')->first()->value;
            $venue_tag_bgcolor = Setting::where('key', 'venue_tag_bgcolor')->first()->value;

            $image = Setting::where('key', 'invoice_logo')->first()->value;
            $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
            $android_link = Setting::where('key', 'android_link')->first()->extended_value;
            $address = Setting::where('key', 'address')->first()->extended_value;
            $email = Setting::where('key', 'email')->first()->extended_value;
            $telp = Setting::where('key', 'telp')->first()->extended_value;
            $fax = Setting::where('key', 'fax')->first()->extended_value;
            $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
            $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
            $social_links = Setting::where('key', 'social_links')->first()->extended_value;
            $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
            $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->value;
            $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->value;

            $hqvenue = collect([
                'id' => 0,
                'name' => 'ALL VENUES',
                'image' => $image,
                'ios_link' => $ios_link,
                'android_link' => $android_link,
                'address' => $address,
                'email' => $email,
                'telp' => $telp,
                'fax' => $fax,
                'open_and_close_hours' => $open_and_close_hours,
                'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                'social_links' => $social_links,
                'menu_links' => $menu_links,
                'hide_delivery_info' => $hide_delivery_info === 'true'? true: false,
                'hide_opening_hours_info' =>  $hide_opening_hours_info === 'true'? true: false
            ]);

            if ($venue_number == 'single'){
                $venues = collect([$hqvenue]);

            } else if ($venue_number == 'multiple') {
                if ($request->has('tags_id')) {
                    $tags_id = $request->input('tags_id');
                    if (empty($tags_id)) {
                        $tags_id = '0';
                    }

                    if ($tags_id == '0'){
                        // Log::info("memberVenue tags_id 0");
                        $venues = $this->googleDistanceMatrix();
                    } else {
                        // Log::info("memberVenue tags_id ".$tags_id);

                        $origins = $address;
                        // $venues = Venue::with('tier', 'venue_tags');
                        $venueTag = VenueTag::find($tags_id);
                                
                        $unsortedVenues = Venue::whereHas('venue_tags', function ($q) use ($request) {
                            $q->where('venue_tags_id', $request->input('tags_id'));
                        })
                        ->when($venueTag->display_order_type == "manual", function ($q) use ($request) {
                            return $q->orderBy('id' ,'asc');
                        })
                        ->when($venueTag->display_order_type == "alphabetically", function ($q) use ($request) {
                            return $q->orderBy('name' ,'asc');
                        })
                        ->get();

                        $venues = $this->googleDistanceMatrix($unsortedVenues);
                        
                        if ($venueTag->display_order_type == "byDistance"){
                            $venues = $unsortedVenues->sortBy('distance')->values()->all();
                        }

                    }

                } else {
                    // Log::info("memberVenue no tags_id ");
                    $venues = $this->googleDistanceMatrix();
                }

            }

            $venueTagAll = VenueTag::
                with(
                    ['venue_pivot_tags' => function($q) {
                        $q->orderBy('display_order');
                    }]
                )
                ->where('status', 1)
                ->where('hide_in_app', 0)
                ->orderBy('display_order')
                ->get();

            // foreach ($venueTagAll as $key=>$venueTag) {
            //     foreach ($venueTag->venue_pivot_tags as $key=>$venue_pivot_tag) {
            //         // Log::warning($venue_pivot_tag);
            //         $venue_pivot_tag->special_text = $venue_pivot_tag->venue->special_text;
            //         $venue_pivot_tag->color = $venue_pivot_tag->venue->color;
            //         $venue_pivot_tag->address = $venue_pivot_tag->venue->address;
            //         $venue_pivot_tag->distance = $venues[$venue_pivot_tag->venue_id]->distance;
            //         $venue_pivot_tag->distanceText = $venues[$venue_pivot_tag->venue_id]->distanceText;
            //     }
            // }
            
            return response()->json(['status' => 'ok', 'data' => $venues, 'venue_tags' => $venueTagAll, 
                'venue_tags_enable' => $venue_tags_enable, 'venue_tag_bgcolor' => $venue_tag_bgcolor,
                'distance_order' => $distance_order
            ]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function googleDistanceMatrix($venues=null) {
        if (is_null($venues)){
            $venues = Venue::with('tier', 'venue_pivot_tags')->where('active', 1)->get();
        }

        // foreach ($venues as $key=>$venue) {
        //     if ($venue->hide_delivery_info) {
        //         $venue->setHidden(['pickup_and_delivery_hours']);
        //     }

        //     if ($venue->hide_opening_hours_info) {
        //         $venue->setHidden(['open_and_close_hours']);
        //     }
        // }
        
        return $venues;
    }
}
