<?php

namespace App\Http\Controllers\Member;

use App\ClaimedPromotion;
use App\Listing;
use App\ListingClick;
use App\ListingEnquiry;
use App\ListingFavorite;
use App\ListingLike;
use App\ListingSchedule;
use App\ListingType;
use App\ListingPivotTag;
use App\ListingTag;
use App\Member;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\VoucherSetups;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class ListingController extends Controller
{
    /**
     * Return all listings
     * >> restricted to display schedules
     *
     * @param Request $request
     * @return mixed
     */
    protected function index(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'display_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $type = ListingType::find($request->input('display_id'));

            if (is_null($type)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            //hack for easier loading in app, stamp card won
            if ($request->input('display_id') == 8) {

                $stampCardWon = $this->stampCardWon($request);

                foreach ($stampCardWon as $won) {

                    $order_detail = \GuzzleHttp\json_decode($won->order_detail);
                    $won->listing = collect(
                        [
                            'name' => $order_detail->listing->heading,
                            'heading' => $order_detail->listing->heading,
                            'payload' => '{"occurrence":[]}',
                            'img' => $won->img,
                            'id' => $won->id,
                            'barcode' => $won->barcode,
                            'expire_date' => $won->expire_date,
                            'status' => $order_detail->status
                        ]);
                }

                return response()->json(['status' => 'ok', 'data' => $stampCardWon]);

                // REDIRECT NOT WORKING ON SERVER
                // return redirect()->action('Member\UserVoucherController@stampCardWon', [$request]);
            }

            $date_now = Carbon::now(config('app.timezone'))->toDateString();
            $listingSchedules = DB::table('listing_schedules')
                //->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
                //->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereDate('date_start', '<=', $date_now)
                // ->whereDate('date_end', '>=', $date_now)
                ->where(function ($query) use ($request, $date_now) {
                    $query->whereDate('date_end', '>=', $date_now);
                    $query->orWhere('never_expired', '1');
                })
                // ->orWhere('never_expired', '1')
                ->whereExists(function ($query) use ($type, $request) {

                    if (isset($request->input('decrypted_token')->member)) {
                        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                                ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        } else {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        }
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }
                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listings')
                        ->where('status', 'active')
                        ->whereNull('listings.deleted_at')
                        ->whereRaw('listings.id = listing_schedules.listing_id');
                })
                ->where('status', 'active')
                ->whereNull('deleted_at')
                ->select('id')
                ->get();

            if (!empty($listingSchedules)) {
                $temp = [];
                foreach ($listingSchedules as $schedule) {
                    $temp[] = $schedule->id;
                }


                $listingSchedules = ListingSchedule::with('tags.tag', 'listing.type')
                    ->join('listings', 'listing_schedules.listing_id', '=', 'listings.id')
                    ->whereIn('listing_schedules.id', $temp)
                    ->orderBy('listings.display_order', 'asc');
                // ->orderBy('listings.datetime_start', 'asc');

                if ($request->input('display_id') == '7') {
                    $listingSchedules = $listingSchedules->with('listing.prize_promotion');
                }

                $listingSchedules = $listingSchedules->get(['listing_schedules.*']);


                $image = Setting::where('key', 'invoice_logo')->first()->value;
                $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                $android_link = Setting::where('key', 'android_link')->first()->extended_value;
                $address = Setting::where('key', 'address')->first()->extended_value;
                $email = Setting::where('key', 'email')->first()->extended_value;
                $telp = Setting::where('key', 'telp')->first()->extended_value;
                $fax = Setting::where('key', 'fax')->first()->extended_value;
                $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
                $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
                $social_links = Setting::where('key', 'social_links')->first()->extended_value;
                $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
                $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
                $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;

                foreach ($listingSchedules as $scheduleKey => $schedule) {
                    if (isset($request->input('decrypted_token')->member)) {
                        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                            $schedule->types = $schedule->types()->with('type')
                                ->where("tier_id", $request->input('decrypted_token')->member->tiers()->first()->id)
                                ->where("listing_type_id", $type->id)
                                ->get(['listing_schedule_listing_type.*']);
                        } else {
                            $schedule->types = $schedule->types()->with('type')
                                ->where("listing_type_id", $type->id)
                                ->get(['listing_schedule_listing_type.*']);
                        }
                    } else {
                        $schedule->types = $schedule->types()->with('type')
                            ->where("listing_type_id", $type->id)
                            ->get(['listing_schedule_listing_type.*']);
                    }

                    $listing = $schedule->listing;

                    if (intval($listing->venue_id) === 0) {
                        $listing->venue = collect(
                            [
                                'id' => 0,
                                'name' => 'ALL VENUES',
                                'image' => $image,
                                'ios_link' => $ios_link,
                                'android_link' => $android_link,
                                'address' => $address,
                                'email' => $email,
                                'telp' => $telp,
                                'fax' => $fax,
                                'open_and_close_hours' => $open_and_close_hours,
                                'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                                'social_links' => $social_links,
                                'menu_links' => $menu_links,
                                'hide_delivery_info' => $hide_delivery_info,
                                'hide_opening_hours_info' => $hide_opening_hours_info
                            ]);
                    } else {
                        $listing->venue;
                    }

                    if (isset($request->input('decrypted_token')->member)) {

                        $favorite = ListingFavorite::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();
                        $listing->favorite = is_null($favorite) ? 0 : 1;

                        $like = ListingLike::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();
                        $listing->like = is_null($like) ? 0 : 1;
                    }

                    if (!is_null($listing->date_end) && is_null($listing->time_end)) {
                        $time_end = Carbon::parse($listing->date_end);
                        $listing->time_end = $time_end->toTimeString();
                    }

                    if (!is_null($listing->date_start) && is_null($listing->time_start)) {
                        $time_start = Carbon::parse($listing->date_start);
                        $listing->time_start = $time_start->toTimeString();
                    }

                    // $listing->desc_long = str_replace("<div>", "<div style='text-align:center;'>", $listing->desc_long);
                    // $listing->desc_long = str_replace("<div>", "<div><center>", $listing->desc_long);
                    // $listing->desc_long = str_replace("</div>", "</center></div>", $listing->desc_long);

                    $products = $listing->products()->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('products')
                            ->where('status', 'active')
                            ->whereNull('products.deleted_at')
                            ->whereRaw('products.id = listing_products.product_id');
                    })->get(['listing_products.*']);

                    foreach ($products as &$pivot) {
                        $pivot->product->product_type;

                        if ($pivot->product->is_free) {
                            $pivot->product->unit_price = 0;
                            $pivot->product->point_price = 0;
                        }

                    }

                    $listing->products = $products;

                    if ($request->input('display_id') == '7' && $listing->prize_promotion_id > 0) {
                        $promo_account = PromoAccount::where('prize_promotion_id', $listing->prize_promotion_id)
                            ->where('member_id', $request->input('decrypted_token')->member->id)->get();

                        $listing->prize_promotion->promo_account = $promo_account;
                    }
                }

                $hqvenue = collect([
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);

                $venues = Venue::with('tier', 'venue_pivot_tags')->where('active', 1)->get();
                $venues->prepend($hqvenue);

            }

            $venueTagAll = VenueTag::
            with(
                ['venue_pivot_tags' => function ($q) {
                    $q->orderBy('display_order');
                }]
            )
                ->where('status', 1)
                ->orderBy('display_order')
                ->get();

            $venue_tag_bgcolor = Setting::where('key', 'venue_tag_bgcolor')->first()->value;
            $venue_tags_enable = Setting::where('key', 'venue_tags_enable')->first()->value;

            return response()->json(['status' => 'ok', 'data' => $listingSchedules, 'venues' => $venues,
                'venue_tags' => $venueTagAll, 'venue_tags_enable' => $venue_tags_enable, 'venue_tag_bgcolor' => $venue_tag_bgcolor]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function stampCardWon(Request $request)
    {
        try {
            //dispatch(new RetrieveVouchers($request->input('decrypted_token')->member));
            //product_type_id <> 2 instead product_type_id = 1
            // Log::info($request);
            // Log::info($request->input('decrypted_token'));

            $vouchers = MemberVouchers::where('member_id', '=', $request->input('decrypted_token')->member->id)
                // ->where(function ($query) use ($request) {
                // $query->whereHas('order_detail', function ($query) use ($request) {
                //     $query->where('product_type_id', '<>', 2);
                // });
                // $query->orWhere('member_vouchers.category', 'bepoz');
                // })
                ->where('category', 'bepoz')
                ->where('bepoz_prize_promo_id', '>', 0)
                ->where('redeemed', 0)
                ->where('amount_left', '<>', 0)
                ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
                ->get();

            $image = Setting::where('key', 'invoice_logo')->first()->value;
            $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
            $android_link = Setting::where('key', 'android_link')->first()->extended_value;
            $address = Setting::where('key', 'address')->first()->extended_value;
            $email = Setting::where('key', 'email')->first()->extended_value;
            $telp = Setting::where('key', 'telp')->first()->extended_value;
            $fax = Setting::where('key', 'fax')->first()->extended_value;
            $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
            $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
            $social_links = Setting::where('key', 'social_links')->first()->extended_value;
            $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
            $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
            $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;

            $venue = collect(
                [
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);

            foreach ($vouchers as $voucher) {
                //Log::info($voucher);

                $cp = ClaimedPromotion::where('member_voucher_id', $voucher->id)->first();
                if (is_null($cp)) {
                    if ($voucher->order_id === 0) {
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->claim_promotion = collect(
                            [
                                'listing' => [
                                    'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'image_square' => $tc->value],
                                'product' => [
                                    'name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'desc_short' => 'This voucher was issued from the till.']
                            ]);

                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'venue' => $venue,
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->img = $tc->value;

                    } else {
                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);
                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'venue' => $venue,
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->order = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->order->listing = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'venue' => $venue,
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'image_square' => $tc->value
                            ]);

                        //$voucher->order_detail->listing;
                        //$voucher->order_detail;
                        //$voucher->order_detail->voucher_setup;

                        if (is_null($voucher->order->listing)) {
                            $tc = Setting::where('key', 'invoice_logo')->first();
                            $voucher->img = $tc->value;
                        } else {
                            //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                            //$voucher->img = $voucher->order->listing->image_square;

                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();

                            if (is_null($product)) {
                                //$voucher->img = $voucher->order->listing->image_square;
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $product->image;
                            }
                        }


                        $order = Order::find($voucher->order_id);
                        if (!is_null($order)) {
                            if (!is_null($order->payload)) {
                                $payload = \GuzzleHttp\json_decode($order->payload);
                                if ($payload->transaction_type == 'friend_referral') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();
                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucherSetup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucherSetup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);
                                    $voucher->img = $tc->value;


                                } elseif ($payload->transaction_type == 'place_order') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();

                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucherSetup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucherSetup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);

                                    if (is_null($voucher->order->listing)) {
                                        $tc = Setting::where('key', 'invoice_logo')->first();
                                        $voucher->img = $tc->value;
                                    } else {

                                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                                        //$voucher->img = $voucher->order->listing->image_square;

                                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();

                                        if (is_null($product)) {
                                            //$voucher->img = $voucher->order->listing->image_square;
                                            $order = Order::where('id', $voucher->order_id)->first();
                                            if (is_null($order)) {
                                                $voucher->img = $tc->value;
                                            } else {
                                                $voucher->img = $voucher->order->listing->image_square;
                                            }
                                        } else {
                                            $voucher->img = $product->image;
                                        }


                                    }

                                } else {

                                    $tc = Setting::where('key', 'invoice_logo')->first();


                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'No description.']
                                    //    ]);

                                    //Log::info($voucherSetup);
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->name,
                                                'desc_short' => 'No description.']
                                        ]);


                                    $voucher->img = $tc->value;

                                }

                            }
                        }
                    }

                } else {
                    //$voucher->order_detail->listing;
                    //$voucher->order_detail->voucher_setup;
                    $tc = Setting::where('key', 'invoice_logo')->first();
                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                    $voucher->order_detail = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'venue' => $venue,
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);

                    $voucher->order_detail->listing = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'image_square' => $tc->value
                        ]);

                    $voucher->order_detail->voucher_setup = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);

                    $voucher->claim_promotion->listing;
                    $voucher->claim_promotion->product;


                    if (is_null($voucher->order->listing)) {
                        //$tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->img = $tc->value;
                    } else {
                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                        //$voucher->img = $voucher->order->listing->image_square;

                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();

                        if (is_null($product)) {
                            $order = Order::where('id', $voucher->order_id)->first();
                            if (is_null($order)) {
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $voucher->order->listing->image_square;
                            }
                        } else {
                            $voucher->img = $product->image;
                        }

                    }
                }
            }

            $rejected = $vouchers->reject(function ($value, $key) {
                if ($value->order_id === 0) {
                    return false;
                } else {

                    $voucherSetup = VoucherSetups::where('id', $value->voucher_setup_id)->first();
                    //Log::error($voucherSetup["expiry_date"]);

                    //not return inactive
                    if ($voucherSetup["inactive"] == 1) {
                        return true;
                    }

                    $date_now = Carbon::now();
                    //$expiry_date = Carbon::parse($voucherSetup["expiry_date"]);
                    $expire_date = Carbon::parse($value->expire_date);
                    $date_diff = $date_now->diffInDays($expire_date, false);

                    //Log::error($value->voucher_setup_id);
                    //Log::error($voucherSetup["expiry_date"]);
                    //Log::error($date_diff);

                    //not return expired
                    if ($date_diff < 0) {
                        return true;
                    }

                    $order = Order::find($value->order_id);
                    if (is_null($order)) {
                        return true; // rejected
                    } else {
                        if (is_null($order->payload)) {
                            return true;
                        } else {
                            $payload = \GuzzleHttp\json_decode($order->payload);
                            if (is_null($payload)) {
                                return true;
                            } else {
                                if (isset($payload->transaction_type)) {
                                    if ($payload->transaction_type != 'place_order') {
                                        $od = OrderDetail::find($value->order_details_id);
                                        if (is_null($od)) {
                                            return true;
                                        } else {
                                            if ($payload->transaction_type == 'friend_referral') {
                                                return false;
                                            } elseif ($payload->transaction_type == 'signup_reward') {
                                                return false;
                                            } else {
                                                $listing = Listing::find($od->listing_id);
                                                if (is_null($listing)) {
                                                    if ($value->category == 'bepoz') {
                                                        return false;
                                                    }

                                                    return true;
                                                } else {
                                                    if (is_null($listing->type->key)) {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        return false; // just editted
                                    }
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }

            });

            return $rejected->values();

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    protected function show(Request $request, $id)
    {
        try {
            $listing = Listing::with('type', 'schedules')
                ->where('id', $id)
                ->where('status', 'active')
                ->first();

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_NOT_FOUND);
            }

            // add count
            $listing->click_count = $listing->click_count + 1;
            $listing->save();

            $Click = new ListingClick();
            $Click->listing_id = $listing->id;
            $Click->listing_name = $listing->name;
            $Click->listing_type_id = $listing->listing_type_id;
            $Click->member_id = $request->input('decrypted_token')->member->id;
            $Click->save();

            if (intval($listing->venue_id) === 0) {

                $image = Setting::where('key', 'invoice_logo')->first()->value;
                $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                $android_link = Setting::where('key', 'android_link')->first()->extended_value;
                $address = Setting::where('key', 'address')->first()->extended_value;
                $email = Setting::where('key', 'email')->first()->extended_value;
                $telp = Setting::where('key', 'telp')->first()->extended_value;
                $fax = Setting::where('key', 'fax')->first()->extended_value;
                $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
                $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
                $social_links = Setting::where('key', 'social_links')->first()->extended_value;
                $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
                $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
                $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;

                $listing->venue = collect(
                    [
                        'id' => 0,
                        'name' => 'ALL VENUES',
                        'image' => $image,
                        'ios_link' => $ios_link,
                        'android_link' => $android_link,
                        'address' => $address,
                        'email' => $email,
                        'telp' => $telp,
                        'fax' => $fax,
                        'open_and_close_hours' => $open_and_close_hours,
                        'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                        'social_links' => $social_links,
                        'menu_links' => $menu_links,
                        'hide_delivery_info' => $hide_delivery_info,
                        'hide_opening_hours_info' => $hide_opening_hours_info
                    ]);
            } else {
                $listing->venue;
            }

            if (isset($request->input('decrypted_token')->member)) {

                $favorite = ListingFavorite::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();

                if (is_null($favorite)) {
                    $listing->favorite = 0;
                } else {
                    $listing->favorite = 1;
                }

                $like = ListingLike::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();

                if (is_null($like)) {
                    $listing->like = 0;
                } else {
                    $listing->like = 1;
                }

            }

            // if (!is_null($listing->date_end)) {
            //     $time_end = Carbon::parse($listing->date_end);
            //     $listing->time_end = $time_end->toTimeString();
            // }

            // if (!is_null($listing->date_start)) {
            //     $time_start = Carbon::parse($listing->date_start);
            //     $listing->time_start = $time_start->toTimeString();
            // }
            
            $date_now = Carbon::now(config('app.timezone'))->toDateString();
            $listingSchedule = DB::table('listing_schedules')
                // ->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
                // ->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereDate('date_start', '<=', $date_now)
                // ->whereDate('date_end', '>=', $date_now)
                ->where(function ($query) use ($request, $date_now) {
                    $query->whereDate('date_end', '>=', $date_now);
                    $query->orWhere('never_expired', '1');
                })
                ->whereExists(function ($query) use ($request) {
                    if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }
                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listings')
                        ->where('status', 'active')
                        ->whereNull('listings.deleted_at')
                        ->whereRaw('listings.id = listing_schedules.listing_id');
                })
                ->where('status', 'active')
                ->whereNull('deleted_at')
                ->where('listing_id', $listing->id)
                ->first();

            if (!is_null($listingSchedule)) {
                $products = $listing->products()->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('products')
                        ->where('status', 'active')
                        ->whereNull('products.deleted_at')
                        ->whereRaw('products.id = listing_products.product_id');
                })->get(['listing_products.*']);

                foreach ($products as &$pivot) {
                    $pivot->product;

                    if ($pivot->product->is_free) {
                        $pivot->product->unit_price = 0;
                        $pivot->product->point_price = 0;
                    }

                }

                $listing->products = $products;
            }

            return response()->json(['status' => 'ok', 'data' => $listing]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    protected function retrieveFavorites(Request $request)
    {
        try {

            $favorites = ListingFavorite::with('listing.type', 'listing.schedules')
                ->where('member_id', $request->input('decrypted_token')->member->id)
                ->whereHas('listing', function ($query) {
                    $query->where('status', 'active');
                    // $query->whereDate('datetime_start', '>', Carbon::now(config('app.timezone'))->toDateString());
                    // $query->orWhere('datetime_start', null);

                    // $query->whereDate('date_start', '>', Carbon::now(config('app.timezone'))->toDateString());
                    // $query->orWhere('date_start', null);
                })
                ->whereHas('listing.schedules', function ($query) {
                    $query->where('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString());
                    $query->orWhere('never_expired', '1');
                })
                ->get();

            if ($favorites->isEmpty()) {
                // return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
                return response()->json(['status' => 'ok', 'message' => 'No favorite found']);
            }

            foreach ($favorites as $favorite) {
                $listing = $favorite->listing;

                if (intval($listing->venue_id) === 0) {
                    $listing->venue = collect(
                        [
                            'id' => 0,
                            'name' => 'ALL VENUES',
                            'image' => Setting::where('key', 'invoice_logo')->first()->value,
                            'ios_link' => Setting::where('key', 'ios_link')->first()->extended_value,
                            'android_link' => Setting::where('key', 'android_link')->first()->extended_value
                        ]);
                } else {
                    $listing->venue;
                }

                if (isset($request->input('decrypted_token')->member)) {

                    $favorite = ListingFavorite::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();

                    if (is_null($favorite)) {
                        $listing->favorite = 0;
                    } else {
                        $listing->favorite = 1;
                    }

                    $like = ListingLike::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();

                    if (is_null($like)) {
                        $listing->like = 0;
                    } else {
                        $listing->like = 1;
                    }

                }

                if (!is_null($listing->date_end)) {
                    $time_end = Carbon::parse($listing->date_end);
                    $listing->time_end = $time_end->toTimeString();
                }

                if (!is_null($listing->date_start)) {
                    $time_start = Carbon::parse($listing->date_start);
                    $listing->time_start = $time_start->toTimeString();
                }

                $products = $listing->products()->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('products')
                        ->where('status', 'active')
                        ->whereNull('products.deleted_at')
                        ->whereRaw('products.id = listing_products.product_id');
                })->with('product')->get(['listing_products.*']);

                foreach ($products as &$pivot) {

                    if ($pivot->product->is_free) {
                        $pivot->product->unit_price = 0;
                        $pivot->product->point_price = 0;
                    }
                }

                $listing->products = $products;
            }

            return response()->json(['status' => 'ok', 'data' => $favorites]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function favorite(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::find($request->input('listing_id'));

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'Invalid listing ID'], Response::HTTP_BAD_REQUEST);
            } else {

                if ($request->input('status') == 'favorite') {
                    $favorite = ListingFavorite::where('listing_id', $request->input('listing_id'))
                        ->where('member_id', $request->input('decrypted_token')->member->id)
                        ->first();

                    if (is_null($favorite)) {
                        $favorite = new ListingFavorite;
                        $favorite->listing_id = $request->input('listing_id');
                        $favorite->member_id = $request->input('decrypted_token')->member->id;
                        $favorite->save();
                    }

                    return response()->json(['status' => 'ok', 'message' => 'Listing is your favorite.']);

                } else {
                    $favorite = ListingFavorite::where('listing_id', $request->input('listing_id'))
                        ->where('member_id', $request->input('decrypted_token')->member->id)
                        ->first();

                    if (!is_null($favorite)) {
                        $favorite->delete();
                    }

                    return response()->json(['status' => 'ok', 'message' => 'Listing is your non-favorite.']);
                }

            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function like(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::find($request->input('listing_id'));

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'Invalid listing ID'], Response::HTTP_BAD_REQUEST);
            } else {

                if ($request->input('status') == 'like') {

                    $like = ListingLike::where('listing_id', $request->input('listing_id'))
                        ->where('member_id', $request->input('decrypted_token')->member->id)
                        ->first();

                    if (is_null($like)) {
                        $like = new ListingLike;
                        $like->listing_id = $request->input('listing_id');
                        $like->member_id = $request->input('decrypted_token')->member->id;
                        $like->save();
                    }

                    return response()->json(['status' => 'ok', 'message' => 'Listing is liked.']);

                } elseif ($request->input('status') == 'dislike') {
                    $like = ListingLike::where('listing_id', $request->input('listing_id'))
                        ->where('member_id', $request->input('decrypted_token')->member->id)
                        ->first();

                    if (!is_null($like)) {
                        $like->delete();
                    }

                    return response()->json(['status' => 'ok', 'message' => 'Listing is disliked.']);

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Invalid Status'], Response::HTTP_BAD_REQUEST);
                }

            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function enquire(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
                'subject' => 'required',
                'message' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::where('status', 'active')->where('id', $request->input('listing_id'))->first();

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => "Listing not found"], Response::HTTP_BAD_REQUEST);
            }

            $enquiry = new ListingEnquiry();
            $enquiry->listing_id = $listing->id;
            $enquiry->subject = $request->input('subject');
            $enquiry->message = $request->input('message');
            $enquiry->member_id = $request->input('decrypted_token')->member->id;
            if ($request->has('contact_me')) {
                $enquiry->contact_me = $request->input('contact_me');
            }
            if ($request->has('rating')) {
                $enquiry->rating = $request->input('rating');
            }
            $enquiry->save();

            return response()->json(['status' => 'ok', 'message' => 'Enquiry Submitted. Thanks.']);


        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function feedback(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'rating' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // Log::info($request);
            $enquiry = new ListingEnquiry();
            $enquiry->listing_id = 0;
            $enquiry->member_id = $request->input('decrypted_token')->member->id;
            $enquiry->rating = $request->input('rating');
            $enquiry->subject = 'Feedback and Enquiry';

            if ($request->has('contact_me')) {
                $enquiry->contact_me = $request->input('contact_me');
            }

            if ($request->has('message')) {
                $enquiry->message = $request->input('message');
            }

            $enquiry->save();

            return response()->json(['status' => 'ok', 'message' => 'Feedback Submitted. Thanks.']);


        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function retrievePromotions(Request $request)
    {
        try {
            $types = ListingType::where('key', 'like', '%"category":"promotion"%')->get();

            if (is_null($types)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $tipes = [];
            foreach ($types as $type) {
                $tipes[] = $type->id;
            }

            $date_now = Carbon::now(config('app.timezone'))->toDateString();
            $listingSchedules = DB::table('listing_schedules')
                // ->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
                // ->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereDate('date_start', '<=', $date_now)
                ->where(function ($query) use ($request, $date_now) {
                    $query->whereDate('date_end', '>=', $date_now);
                    $query->orWhere('never_expired', '1');
                })
                // ->whereDate('date_end', '>=', $date_now)
                // ->orWhere('never_expired', 1)

                ->whereExists(function ($query) use ($tipes, $request) {
                    if (isset($request->input('decrypted_token')->member)) {
                        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                                ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        } else {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        }
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }

                })
                // ->whereExists(function ($query) {
                //     $query->select(DB::raw(1))
                //         ->from('listing_products')
                //         ->join('products', 'listing_products.product_id', '=', 'products.id')
                //         ->where('products.status', 'active')
                //         ->whereNull('listing_products.deleted_at')
                //         ->whereRaw('listing_products.listing_id = listing_schedules.listing_id');
                // })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listings')
                        ->join('listing_types', 'listings.listing_type_id', '=', 'listing_types.id')
                        ->where('listing_types.key', 'like', '%"category":"promotion"%')
                        ->where('listings.status', 'active')
                        ->whereNull('listings.deleted_at')
                        ->whereRaw('listings.id = listing_schedules.listing_id');
                })


                //    ->whereNotExists(function ($query) use ($request) {
                //        if (isset($request->input('decrypted_token')->member)) {
                //            if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                //                $query->select(DB::raw(1))
                //                    ->from('claimed_promotions')
                //                    ->where('claimed_promotions.member_id', $request->input('decrypted_token')->member->id)
                //                    ->where('claimed_promotions.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                //                    ->whereNull('claimed_promotions.deleted_at')
                //                    ->whereRaw('claimed_promotions.listing_id = listing_schedules.listing_id');
                //            }
                //        }
                //    })
                ->where('status', 'active')
                ->whereNull('deleted_at')
                ->select('id')
                ->get();

            if (!empty($listingSchedules)) {
                $temp = [];
                foreach ($listingSchedules as $schedule) {
                    $temp[] = $schedule->id;
                }

                $listingSchedules = ListingSchedule::with('listing.type')
                    ->join('listings', 'listing_schedules.listing_id', '=', 'listings.id')
                    ->whereIn('listing_schedules.id', $temp)
                    // ->orderBy('listings.date_start', 'asc')
                    ->orderBy('listings.display_order', 'asc')
                    ->get(['listing_schedules.*']);

                $image = Setting::where('key', 'invoice_logo')->first()->value;
                $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                $android_link = Setting::where('key', 'android_link')->first()->extended_value;
                $address = Setting::where('key', 'address')->first()->extended_value;
                $email = Setting::where('key', 'email')->first()->extended_value;
                $telp = Setting::where('key', 'telp')->first()->extended_value;
                $fax = Setting::where('key', 'fax')->first()->extended_value;
                $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
                $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
                $social_links = Setting::where('key', 'social_links')->first()->extended_value;
                $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
                $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
                $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;

                foreach ($listingSchedules as $scheduleKey => $schedule) {
                    $listing = $schedule->listing;

                    if (intval($listing->venue_id) === 0) {
                        $listing->venue = collect(
                            [
                                'id' => 0,
                                'name' => 'ALL VENUES',
                                'image' => $image,
                                'ios_link' => $ios_link,
                                'android_link' => $android_link,
                                'address' => $address,
                                'email' => $email,
                                'telp' => $telp,
                                'fax' => $fax,
                                'open_and_close_hours' => $open_and_close_hours,
                                'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                                'social_links' => $social_links,
                                'menu_links' => $menu_links,
                                'hide_delivery_info' => $hide_delivery_info,
                                'hide_opening_hours_info' => $hide_opening_hours_info
                            ]);
                    } else {
                        $listing->venue;
                    }
                }

                $rejected = $listingSchedules->reject(function ($value, $key) use ($request) {
                    if (!is_null($value->listing->products->first())) {
                        $countMaxLimit = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                            ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                            ->where('listing_id', $value->listing->id)
                            ->where('listing_type_id', $value->listing->type->id)
                            ->count();

                        if (is_null($countMaxLimit)) {
                            $countMaxLimit = 0;
                        }

                        $countMaxLimitPerDay = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                            ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                            ->where('listing_id', $value->listing->id)
                            ->where('listing_type_id', $value->listing->type->id)
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->count();

                        if (is_null($countMaxLimitPerDay)) {
                            $countMaxLimitPerDay = 0;
                        }

                        $countMaxLimitPerAccount = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                            ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                            ->where('listing_id', $value->listing->id)
                            ->where('listing_type_id', $value->listing->type->id)
                            ->where('member_id', $request->input('decrypted_token')->member->id)
                            ->count();

                        if (is_null($countMaxLimitPerAccount)) {
                            $countMaxLimitPerAccount = 0;
                        }

                        $countMaxLimitPerDayPerAccount = ClaimedPromotion::where('product_id', $value->listing->products->first()->product->id)
                            ->where('product_type_id', $value->listing->products->first()->product->product_type->id)
                            ->where('listing_id', $value->listing->id)
                            ->where('listing_type_id', $value->listing->type->id)
                            ->where('member_id', $request->input('decrypted_token')->member->id)
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->count();

                        if (is_null($countMaxLimitPerDayPerAccount)) {
                            $countMaxLimitPerDayPerAccount = 0;
                        }

                        if (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) === 0 &&
                            intval($value->listing->max_limit_per_account) === 0 &&
                            intval($value->listing->max_limit_per_day_per_account) === 0
                        ) {
                            // COMBINATION 1
                            if ($countMaxLimit >= $value->listing->max_limit) {
                                return true; // rejected
                            }
                        } elseif (intval($value->listing->max_limit) === 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) === 0 &&
                            intval($value->listing->max_limit_per_day_per_account) === 0
                        ) {
                            // COMBINATION 2
                            //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) === 0 &&
                            intval($value->listing->max_limit_per_day) === 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) === 0
                        ) {
                            // COMBINATION 3
                            if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                                return true; // rejected
                            }
                        } elseif (intval($value->listing->max_limit) === 0 &&
                            intval($value->listing->max_limit_per_day) === 0 &&
                            intval($value->listing->max_limit_per_account) === 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 4
                            //    if ($countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) === 0 &&
                            intval($value->listing->max_limit_per_day_per_account) === 0
                        ) {
                            // COMBINATION 5
                            if ($countMaxLimit >= $value->listing->max_limit) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) === 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) === 0
                        ) {
                            // COMBINATION 6
                            if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                                return true; // rejected
                            }
                        } elseif (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) === 0 &&
                            intval($value->listing->max_limit_per_account) === 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 7
                            if ($countMaxLimit >= $value->listing->max_limit) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) === 0
                        ) {
                            // COMBINATION 8
                            if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) === 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 9
                            if ($countMaxLimit >= $value->listing->max_limit) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 10
                            if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) === 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) === 0
                        ) {
                            // COMBINATION 11
                            if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) === 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) === 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 12
                            if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                                return true; // rejected
                            }
                        } elseif (intval($value->listing->max_limit) === 0 &&
                            intval($value->listing->max_limit_per_day) > 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 13
                            if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) === 0 &&
                            intval($value->listing->max_limit_per_day) === 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 14
                            if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                                return true; // rejected
                            }
                            //    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                            //        return true; // rejected
                            //    }
                        } elseif (intval($value->listing->max_limit) > 0 &&
                            intval($value->listing->max_limit_per_day) === 0 &&
                            intval($value->listing->max_limit_per_account) > 0 &&
                            intval($value->listing->max_limit_per_day_per_account) > 0
                        ) {
                            // COMBINATION 15
                            // if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                            //     return true; // rejected
                            // }
                            if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                                return true; // rejected
                            }
                        }
                    }


                    return false;

                });


                foreach ($rejected as $scheduleKey => $schedule) {
                    $listing = $schedule->listing;

                    $products = $listing->products()->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('products')
                            ->where('status', 'active')
                            ->whereNull('products.deleted_at')
                            ->whereRaw('products.id = listing_products.product_id');
                    })->get(['listing_products.*']);

                    foreach ($products as &$pivot) {
                        $pivot->product->product_type;

                        if ($pivot->product->is_free) {
                            $pivot->product->unit_price = 0;
                            $pivot->product->point_price = 0;
                        }

                    }

                    $listing->products = $products;
                }

                $listingSchedules = $rejected->values();

                $hqvenue = collect([
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);

                $venues = Venue::with('tier', 'venue_pivot_tags')->where('active', 1)->get();
                $venues->prepend($hqvenue);
            }

            $venueTagAll = VenueTag::
            with(
                ['venue_pivot_tags' => function ($q) {
                    $q->orderBy('display_order');
                }]
            )
                ->where('status', 1)
                ->orderBy('display_order')
                ->get();

            $venue_tag_bgcolor = Setting::where('key', 'venue_tag_bgcolor')->first()->value;
            $venue_tags_enable = Setting::where('key', 'venue_tags_enable')->first()->value;

            return response()->json(['status' => 'ok', 'data' => $listingSchedules, 'venues' => $venues,
                'venue_tags' => $venueTagAll, 'venue_tags_enable' => $venue_tags_enable, 'venue_tag_bgcolor' => $venue_tag_bgcolor]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    
    /**
     * shoplist API
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function shoplist(Request $request)
    {
        try {
            //SHOP IS LIsTING TYPE #9            
            $tipes[] = 9;

            $date_now = Carbon::now(config('app.timezone'))->toDateString();
            $listingSchedules = DB::table('listing_schedules')
                // ->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
                // ->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereDate('date_start', '<=', $date_now)
                ->where(function ($query) use ($request, $date_now) {
                    $query->whereDate('date_end', '>=', $date_now);
                    $query->orWhere('never_expired', '1');
                })
                // ->whereDate('date_end', '>=', $date_now)
                // ->orWhere('never_expired', 1)
                
                ->whereExists(function ($query) use ($tipes, $request) {
                    if (isset($request->input('decrypted_token')->member)) {
                        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                                ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        } else {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        }
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }

                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listing_products')
                        ->join('products', 'listing_products.product_id', '=', 'products.id')
                        ->where('products.status', 'active')
                        ->whereNull('listing_products.deleted_at')
                        ->whereRaw('listing_products.listing_id = listing_schedules.listing_id');
                })
                ->where('status', 'active')
                ->whereNull('deleted_at')
                ->select('id')
                ->get();

            if (!empty($listingSchedules)) {
                $temp = [];
                foreach ($listingSchedules as $schedule) {
                    $temp[] = $schedule->id;
                }

                $listingSchedules = ListingSchedule::with('listing.type')
                    ->join('listings', 'listing_schedules.listing_id', '=', 'listings.id')
                    ->whereIn('listing_schedules.id', $temp)
                    // ->orderBy('listings.date_start', 'asc')
                    ->orderBy('listings.display_order', 'asc')
                    ->get(['listing_schedules.*']);

                foreach ($listingSchedules as $scheduleKey => $schedule) {
                    $listing = $schedule->listing;
                    
                    if (isset($request->input('decrypted_token')->member)) {

                        $favorite = ListingFavorite::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();
                        $listing->favorite = is_null($favorite) ? 0 : 1;

                        $like = ListingLike::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();
                        $listing->like = is_null($like) ? 0 : 1;
                    }

                    if (!is_null($listing->date_end) && is_null($listing->time_end)) {
                        $time_end = Carbon::parse($listing->date_end);
                        $listing->time_end = $time_end->toTimeString();
                    }

                    if (!is_null($listing->date_start) && is_null($listing->time_start)) {
                        $time_start = Carbon::parse($listing->date_start);
                        $listing->time_start = $time_start->toTimeString();
                    }

                    $products = $listing->products()->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('products')
                            ->where('status', 'active')
                            ->whereNull('products.deleted_at')
                            ->whereRaw('products.id = listing_products.product_id');
                    })->get(['listing_products.*']);

                    foreach ($products as &$pivot) {
                        $pivot->product->product_type;

                        if ($pivot->product->is_free) {
                            $pivot->product->unit_price = 0;
                            $pivot->product->point_price = 0;
                        }

                    }

                    $listing->products = $products;

                    $listing_tags = $listing->listing_tags()->get(['listing_tags.*']);

                    $listing->listing_tags = $listing_tags;

                    // $listing_pivot_tags = $listing->listing_pivot_tags()->get(['listing_pivot_tags.*']);

                    // $listing->listing_pivot_tags = $listing_pivot_tags;

                }

            }

            $rejected = $listingSchedules->reject(function ($value, $key) use ($request) {
                $user = $request->input('decrypted_token');
                $listing = $value->listing;
                $pdt = Product::with('product_type')->find($listing->products[0]->product->id);

                $countMaxLimit = OrderDetail::where('product_id', $pdt->id)
                    ->where('product_type_id', $pdt->product_type->id)
                    ->where('listing_id', $listing->id)
                    ->where('status', '<>', 'expired')
                    ->sum('qty');

                if (is_null($countMaxLimit)) {
                    $countMaxLimit = 0;
                }

                $countMaxLimitPerDay = OrderDetail::where('product_id', $pdt->id)
                    ->where('product_type_id', $pdt->product_type->id)
                    ->where('listing_id', $listing->id)
                    ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                    ->where('status', '<>', 'expired')
                    ->sum('qty');

                if (is_null($countMaxLimitPerDay)) {
                    $countMaxLimitPerDay = 0;
                }

                $countMaxLimitPerAccount = OrderDetail::where('product_id', $pdt->id)
                    ->where('product_type_id', $pdt->product_type->id)
                    ->where('listing_id', $listing->id)
                    ->whereHas('order', function ($query) use ($user) {
                        $query->where('member_id', $user->member->id);
                    })
                    ->where('status', '<>', 'expired')
                    ->sum('qty');

                if (is_null($countMaxLimitPerAccount)) {
                    $countMaxLimitPerAccount = 0;
                }

                $countMaxLimitPerDayPerAccount = OrderDetail::where('product_id', $pdt->id)
                    ->where('product_type_id', $pdt->product_type->id)
                    ->where('listing_id', $listing->id)
                    ->whereHas('order', function ($query) use ($user) {
                        $query->where('member_id', $user->member->id);
                    })
                    ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                    ->where('status', '<>', 'expired')
                    ->sum('qty');

                if (is_null($countMaxLimitPerDayPerAccount)) {
                    $countMaxLimitPerDayPerAccount = 0;
                }


                // LIMIT CHECKING
                if (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) === 0 &&
                    intval($value->listing->max_limit_per_account) === 0 &&
                    intval($value->listing->max_limit_per_day_per_account) === 0
                ) {
                    // COMBINATION 1
                    if ($countMaxLimit >= $value->listing->max_limit) {
                        return true; // rejected
                    }
                } elseif (intval($value->listing->max_limit) === 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) === 0 &&
                    intval($value->listing->max_limit_per_day_per_account) === 0
                ) {
                    // COMBINATION 2
                //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) === 0 &&
                    intval($value->listing->max_limit_per_day) === 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) === 0
                ) {
                    // COMBINATION 3
                    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        return true; // rejected
                    }
                } elseif (intval($value->listing->max_limit) === 0 &&
                    intval($value->listing->max_limit_per_day) === 0 &&
                    intval($value->listing->max_limit_per_account) === 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 4
                //    if ($countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) === 0 &&
                    intval($value->listing->max_limit_per_day_per_account) === 0
                ) {
                    // COMBINATION 5
                    if ($countMaxLimit >= $value->listing->max_limit) {
                        return true; // rejected
                    }
                //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) === 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) === 0
                ) {
                    // COMBINATION 6
                    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        return true; // rejected
                    }
                } elseif (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) === 0 &&
                    intval($value->listing->max_limit_per_account) === 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 7
                    if ($countMaxLimit >= $value->listing->max_limit) {
                        return true; // rejected
                    }
                //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) === 0
                ) {
                    // COMBINATION 8
                    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        return true; // rejected
                    }
                //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) === 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 9
                    if ($countMaxLimit >= $value->listing->max_limit) {
                        return true; // rejected
                    }
                //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 10
                    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        return true; // rejected
                    }
                //    if ($countMaxLimit >= $value->listing->max_limit || $countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) === 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) === 0
                ) {
                    // COMBINATION 11
                    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        return true; // rejected
                    }
                //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) === 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) === 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 12
                    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        return true; // rejected
                    }
                } elseif (intval($value->listing->max_limit) === 0 &&
                    intval($value->listing->max_limit_per_day) > 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 13
                    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        return true; // rejected
                    }
                //    if ($countMaxLimitPerDay >= $value->listing->max_limit_per_day || $countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) === 0 &&
                    intval($value->listing->max_limit_per_day) === 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 14
                    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                        return true; // rejected
                    }
                //    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                //        return true; // rejected
                //    }
                } elseif (intval($value->listing->max_limit) > 0 &&
                    intval($value->listing->max_limit_per_day) === 0 &&
                    intval($value->listing->max_limit_per_account) > 0 &&
                    intval($value->listing->max_limit_per_day_per_account) > 0
                ) {
                    // COMBINATION 15
                    // if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account) {
                    //     return true; // rejected
                    // }
                    if ($countMaxLimitPerAccount >= $value->listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $value->listing->max_limit_per_day_per_account) {
                        return true; // rejected
                    }
                }

                if ($listing->status == 'inactive'){
                    return true; // rejected
                }

                return false;

            });

            
            $listingTagAll = ListingTag::
                with(
                    ['listing_pivot_tags' => function($q) {
                        $q->orderBy('display_order');
                    }]
                )
                ->where('status', 1)
                ->orderBy('display_order')
                ->get();

            $listing_tag_bgcolor = Setting::where('key', 'listing_tag_bgcolor')->first()->value;
            $listing_tags_enable = Setting::where('key', 'listing_tags_enable')->first()->value;
            
            return response()->json(['status' => 'ok', 'data' => $rejected, 'listing_tags' => $listingTagAll,
                'listing_tags_enable' => $listing_tags_enable, 'listing_tag_bgcolor' => $listing_tag_bgcolor]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * tierslist API
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function membershiptierslist(Request $request)
    {
        try {
            //SHOP IS LIsTING TYPE #10            
            $tipes[] = 10;

            $date_now = Carbon::now(config('app.timezone'))->toDateString();
            $listingSchedules = DB::table('listing_schedules')
                // ->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
                // ->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereDate('date_start', '<=', $date_now)
                ->where(function ($query) use ($request, $date_now) {
                    $query->whereDate('date_end', '>=', $date_now);
                    $query->orWhere('never_expired', '1');
                })
                // ->whereDate('date_end', '>=', $date_now)
                // ->orWhere('never_expired', 1)
                
                ->whereExists(function ($query) use ($tipes, $request) {
                    if (isset($request->input('decrypted_token')->member)) {
                        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                                ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        } else {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        }
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->whereIn('listing_schedule_listing_type.listing_type_id', $tipes)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }

                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listing_products')
                        ->join('products', 'listing_products.product_id', '=', 'products.id')
                        ->where('products.status', 'active')
                        ->whereNull('listing_products.deleted_at')
                        ->whereRaw('listing_products.listing_id = listing_schedules.listing_id');
                })
                ->where('status', 'active')
                ->whereNull('deleted_at')
                ->select('id')
                ->get();

            if (!empty($listingSchedules)) {
                $temp = [];
                foreach ($listingSchedules as $schedule) {
                    $temp[] = $schedule->id;
                }

                $listingSchedules = ListingSchedule::with('listing.type')
                    ->join('listings', 'listing_schedules.listing_id', '=', 'listings.id')
                    ->whereIn('listing_schedules.id', $temp)
                    // ->orderBy('listings.date_start', 'asc')
                    ->orderBy('listings.display_order', 'asc')
                    ->get(['listing_schedules.*']);

                $image = Setting::where('key', 'invoice_logo')->first()->value;
                $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                $android_link = Setting::where('key', 'android_link')->first()->extended_value;
                $address = Setting::where('key', 'address')->first()->extended_value;
                $email = Setting::where('key', 'email')->first()->extended_value;
                $telp = Setting::where('key', 'telp')->first()->extended_value;
                $fax = Setting::where('key', 'fax')->first()->extended_value;
                $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
                $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
                $social_links = Setting::where('key', 'social_links')->first()->extended_value;
                $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
                $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
                $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;
                        
                foreach ($listingSchedules as $scheduleKey => $schedule) {
                    $listing = $schedule->listing;

                    if (intval($listing->venue_id) === 0) {
                        $listing->venue = collect(
                            [
                                'id' => 0,
                                'name' => 'ALL VENUES',
                                'image' => $image,
                                'ios_link' => $ios_link,
                                'android_link' => $android_link,
                                'address' => $address,
                                'email' => $email,
                                'telp' => $telp,
                                'fax' => $fax,
                                'open_and_close_hours' => $open_and_close_hours,
                                'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                                'social_links' => $social_links,
                                'menu_links' => $menu_links,
                                'hide_delivery_info' => $hide_delivery_info,
                                'hide_opening_hours_info' => $hide_opening_hours_info
                            ]);
                    } else {
                        $listing->venue;
                    }

                    if (isset($request->input('decrypted_token')->member)) {

                        $favorite = ListingFavorite::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();
                        $listing->favorite = is_null($favorite) ? 0 : 1;

                        $like = ListingLike::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();
                        $listing->like = is_null($like) ? 0 : 1;
                    }

                    if (!is_null($listing->date_end) && is_null($listing->time_end)) {
                        $time_end = Carbon::parse($listing->date_end);
                        $listing->time_end = $time_end->toTimeString();
                    }

                    if (!is_null($listing->date_start) && is_null($listing->time_start)) {
                        $time_start = Carbon::parse($listing->date_start);
                        $listing->time_start = $time_start->toTimeString();
                    }
                }

                foreach ($listingSchedules as $scheduleKey => $schedule) {
                    $listing = $schedule->listing;

                    $products = $listing->products()->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('products')
                            ->where('status', 'active')
                            ->whereNull('products.deleted_at')
                            ->whereRaw('products.id = listing_products.product_id');
                    })->get(['listing_products.*']);

                    foreach ($products as &$pivot) {
                        $pivot->product->product_type;

                        if ($pivot->product->is_free) {
                            $pivot->product->unit_price = 0;
                            $pivot->product->point_price = 0;
                        }

                    }

                    $listing->products = $products;
                }

                $hqvenue = collect([
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);
        
                $venues = Venue::with('tier', 'venue_pivot_tags')->where('active', 1)->get();
                $venues->prepend($hqvenue);
            }

            $venueTagAll = VenueTag::
                with(
                    ['venue_pivot_tags' => function($q) {
                        $q->orderBy('display_order');
                    }]
                )
                ->where('status', 1)
                ->orderBy('display_order')
                ->get();

            $venue_tag_bgcolor = Setting::where('key', 'venue_tag_bgcolor')->first()->value;
            $venue_tags_enable = Setting::where('key', 'venue_tags_enable')->first()->value;
            
            return response()->json(['status' => 'ok', 'data' => $listingSchedules, 'venues' => $venues,
                'venue_tags' => $venueTagAll, 'venue_tags_enable' => $venue_tags_enable, 'venue_tag_bgcolor' => $venue_tag_bgcolor]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
