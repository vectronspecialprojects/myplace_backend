<?php

namespace App\Http\Controllers\Member;

use App\BepozJob;
use App\ClaimedPromotion;
use App\FriendReferral;
use App\GiftCertificate;
use App\Jobs\SendPusherNotification;
use App\Jobs\SendReceiptEmail;
use App\Jobs\SendBookingTicketEmail;
use App\Listing;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\PaymentLog;
use App\PointLog;
use App\Product;

use App\ProductType;
use App\Setting;
use App\StripeCustomer;
use App\SystemLog;
use App\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TransactionController extends Controller
{
    /**
     * Return all transactions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request, Order $order)
    {
        try {

            $obj = $order->newQuery();

            $obj->where('member_id', '=', $request->input('decrypted_token')->member->id);
            $obj->where('order_status', '<>', 'expired')->get();

            if ($request->has('date_start')) {
                $start_date = new Carbon($request->input('date_start'), config('app.timezone'));

                $obj->whereDate('created_at', '>', $start_date->toDateString());
            }

            if ($request->has('date_end')) {
                $end_date = new Carbon($request->input('date_end'), config('app.timezone'));

                $obj->whereDate('created_at', '<', $end_date->toDateString());
            }

            if ($request->has('filter')) {
                if ($request->input('filter') == 'voucher') {
                    $obj->whereHas('order_details', function ($query) {
                        $query->where('category', 'voucher');
                    });
                } elseif ($request->input('filter') == 'ticket') {
                    $obj->whereHas('order_details', function ($query) {
                        $query->where('category', 'ticket');
                    });
                } elseif ($request->input('filter') == 'promotion') {
                    $obj->where('payload', 'like', '%"transaction_type":"claim_promotion"%');
                }
            }

            $orders = $obj->get();

            foreach ($orders as $order) {
                $order->order_details;
                foreach ($order->order_details as $order_detail) {
                    $order_detail->voucher_setup;
                }
            }
            return response()->json(['status' => 'ok', 'data' => $orders]);
        } catch (\Exception $e) {
            Log::info($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Confirm the order
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function confirmOrder(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'order_token' => 'required',
                'option' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $order_token = $request->input('order_token');

            $order = Order::where('token', '=', $order_token)->first();

            if (is_null($order)) {
                throw new \Exception('order_token_not_valid');
            }

            if ($order->order_status == "cancelled") {
                throw new \Exception('order_already_cancelled');
            }

            if ($order->order_status == "confirmed") {
                throw new \Exception('order_already_confirmed');
            }

            if (Carbon::now(config('app.timezone'))->diffInMinutes(Carbon::createFromTimestamp($order->expired, config('app.timezone'))) > 30) {
                throw new \Exception('order_already_expired');
            }

            if ($request->input('option') == "cancelled") {
                $ml = new MemberLog();
                $ml->member_id = $order->member->id;
                $ml->message = $order->member->first_name . '(' . $order->member->id . ') has cancelled the purchase. ';
                $ml->after = $order->toJson();
                $ml->save();

                $order->order_status = "cancelled";
                $order->payment_status = "cancelled";
                $order->voucher_status = "cancelled";
                $order->cancelled_at = Carbon::now(config('app.timezone'));
                $order->save();


            } elseif ($request->input('option') == "confirmed") {
                $order->voucher_status = "pending";
                $order->order_status = "confirmed";
                $order->payment_status = "successful";
                $order->confirmed_at = Carbon::now(config('app.timezone'));
                $order->paid_at = Carbon::now(config('app.timezone'));

                $member = Member::find($order->member_id);
                $payload = \GuzzleHttp\json_decode($order->payload);

                $order_details = OrderDetail::where('order_id', $order->id)->get();

                if ($payload->transaction_type == 'place_order') {

                    $ml = new MemberLog();
                    $ml->member_id = $order->member->id;
                    $ml->message = $order->member->first_name . '(' . $order->member->id . ') has placed an order (tickets). ';
                    $ml->after = $order->toJson();
                    $ml->save();

                    if ($order->type == "point") {

                        // Deduct points from member
                        $member = Member::find($order->member_id);

                        foreach ($order_details as $order_detail) {
                            $qty = intval($order_detail->qty);

                            $product = Product::find($order_detail->product_id);
                            $total_points = floatval($product->point_price) * $qty;

                            // Log
                            $pl = new PointLog();
                            $pl->member_id = $member->id;
                            $pl->points_before = $member->points;
                            $pl->points_after = floatval($member->points) - $total_points;
                            $pl->desc_short = "Points used for paying products.";
                            $pl->desc_long = "Used " . $total_points . " points for Transaction #" . $order->id . " (Point transaction)";
                            $pl->save();

                            // sync point
                            $data = array(
                                "point" => abs(floatval($total_points)),
                                "id" => $member->bepoz_account_id,
                                "member_id" => $member->id,
                                "status" => 'redeem'
                            );

                            $job = new BepozJob();
                            $job->queue = "sync-point";
                            $job->payload = json_encode($data);
                            $job->available_at = time();
                            $job->created_at = time();
                            $job->save();

                            $member->points = floatval($member->points) - $total_points;

                        }

                        $member->save();

                        $order->paid_total_point = $order->total_point;
                        $order->save();
                    } elseif ($order->type == "mix") {

                        if ($request->has('paypal_id')) {

                            $temp = Order::where('paypal_id', "=", $request->input('paypal_id'))->first();

                            if (!is_null($temp)) {
                                throw new \Exception('paypal_id_already_exist');
                            }

                            $order->paypal_id = $request->input('paypal_id');
                        } else {
                            throw new \Exception('no_paypal_id');
                        }

                        $order->paid_total_price = $order->total_price;
                        $order->paid_total_point = $order->total_point;

                        foreach ($order_details as $order_detail) {
                            $qty = intval($order_detail->qty);

                            if (floatval($order->actual_total_price) == 0) {
                                $discounted_individual_sale_price = 0;
                            } else {
                                $discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                            }

                            $order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                            $order_detail->paid_subtotal_unit_price = $discounted_individual_sale_price * $qty;
                            $order_detail->save();

                        }

                        $order->save();

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $member->id;
                        $pl->points_before = $member->points;
                        $pl->points_after = floatval($member->points) - floatval($order->total_point);
                        $pl->desc_short = "Points used for paying products.";
                        $pl->desc_long = "Used " . floatval($order->total_point) . " points for Transaction #" . $order->id . " (Mix transaction)";
                        $pl->save();

                        // sync point
                        $data = array(
                            "point" => abs(floatval($order->total_point)),
                            "id" => $member->bepoz_account_id,
                            "member_id" => $member->id,
                            "status" => 'redeem'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $member->points = floatval($member->points) - floatval($order->total_point);

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $member->id;
                        $pl->points_before = $member->points;
                        $pl->points_after = floatval($member->points) + floatval($order->claimable_point_reward);
                        $pl->desc_short = "Point rewards gained by buying products.";
                        $pl->desc_long = "Gained " . floatval($order->claimable_point_reward) . " points from Transaction #" . $order->id . " (Mix transaction)";
                        $pl->save();

                        $member->points = floatval($member->points) + floatval($order->claimable_point_reward);

                        // sync point
                        $data = array(
                            "point" => abs(floatval($order->claimable_point_reward)),
                            "id" => $member->bepoz_account_id,
                            "member_id" => $member->id,
                            "status" => 'earn'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $member->save();

                    } elseif ($order->type == "cash") {

                        if ($request->has('paypal_id')) {
                            $temp = Order::where('paypal_id', "=", $request->input('paypal_id'))->first();

                            if (!is_null($temp)) {
                                throw new \Exception('paypal_id_already_exist');
                            }

                            $order->paypal_id = $request->input('paypal_id');
                        } else {
                            throw new \Exception('no_paypal_id');
                        }

                        $order->paid_total_price = $order->total_price;

                        foreach ($order_details as $order_detail) {
                            $qty = intval($order_detail->qty);

                            if (floatval($order->actual_total_price) == 0) {
                                $discounted_individual_sale_price = 0;
                            } else {
                                $discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                            }

                            $order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                            $order_detail->paid_subtotal_unit_price = $discounted_individual_sale_price * $qty;
                            $order_detail->save();

                        }

                        $order->save();

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $member->id;
                        $pl->points_before = $member->points;
                        $pl->points_after = floatval($member->points) + floatval($order->claimable_point_reward);
                        $pl->desc_short = "Point rewards gained by buying products.";
                        $pl->desc_long = "Gained " . floatval($order->claimable_point_reward) . " points from Transaction #" . $order->id . " (Cash transaction)";
                        $pl->save();

                        $member->points = floatval($member->points) + floatval($order->claimable_point_reward);

                        // sync point
                        $data = array(
                            "point" => abs(floatval($order->claimable_point_reward)),
                            "id" => $member->bepoz_account_id,
                            "member_id" => $member->id,
                            "status" => 'earn'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $member->save();

                    }

                    if ($order->listing->listing_type_id == 9){
                        // SHOP
                        // foreach ($order_details as $order_detail) {

                        //     $product = Product::find($order_detail->product_id);
                        //     // Log
                        //     $pl = new PointLog();
                        //     $pl->member_id = $member->id;
                        //     $pl->points_before = $member->points;
                        //     $pl->points_after = floatval($member->points);
                        //     $pl->desc_short = "Points used for paying products.";
                        //     $pl->desc_long = "Used " . $total_points . " points for Transaction #" . $order->id . " (Point transaction)";
                        //     $pl->save();

                        //     // sync point
                        //     $data = array(
                        //         "point" => abs(floatval($total_points)),
                        //         "id" => $member->bepoz_account_id,
                        //         "member_id" => $member->id,
                        //         "status" => 'redeem'
                        //     );

                        //     $job = new BepozJob();
                        //     $job->queue = "sync-point";
                        //     $job->payload = json_encode($data);
                        //     $job->available_at = time();
                        //     $job->created_at = time();
                        //     $job->save();

                        //     $member->points = floatval($member->points) - $total_points;
                        //     $member->save();
                        // }

                    } 
                    else if ($order->listing->listing_type_id == 10){
                        // MEMBERSHIP UPGRADE

                        $member_tier = MemberTiers::where('member_id', $member->id)->first();
                        $member_tier->tier_id = $order->listing->membership_tier;
                        $member_tier->save();

                        if (!is_null($member->bepoz_account_id)) {
                            $data = array();
                            $data['member_id'] = $member->id;
            
                            $job = new BepozJob();
                            $job->queue = "update-account";
                            $job->payload = \GuzzleHttp\json_encode($data);
                            $job->available_at = time();
                            $job->created_at = time();
                            $job->save();

                            // Log::info("Create Update Account");
                        }
                    } 
                    else {
                        // create vouchers
                        foreach ($order_details as $order_detail) {

                            if ($order_detail->voucher_setups_id !== 0) {
                                $qty = intval($order_detail->qty);

                                // ------------------------
                                for ($i = 0; $i < $qty; $i++) {

                                    $memberVoucher = new MemberVouchers();
                                    $memberVoucher->name = $order_detail->voucher_setup->name;
                                    $memberVoucher->member_id = $member->id;
                                    $memberVoucher->order_id = $order->id;
                                    $memberVoucher->order_details_id = $order_detail->id;
                                    $memberVoucher->lookup = "100000000";
                                    $memberVoucher->barcode = "";
                                    $memberVoucher->amount_left = 100;
                                    $memberVoucher->amount_issued = 100;
                                    $memberVoucher->voucher_setup_id = $order_detail->voucher_setups_id;
                                    $memberVoucher->category = $order_detail->category;
                                    $memberVoucher->save();

                                    // ----------------------

                                    // bepoz job - issue voucher
                                    $data = array(
                                        "member_id" => $member->id,
                                        "member_voucher_id" => $memberVoucher->id,
                                        "voucher_setups_id" => $order_detail->voucher_setups_id,
                                        "order_id" => $order->id,
                                        "order_details_id" => $order_detail->id
                                    );

                                    $job = new BepozJob();
                                    $job->queue = "issue-voucher";
                                    $job->payload = \GuzzleHttp\json_encode($data);
                                    $job->available_at = time();
                                    $job->created_at = time();
                                    $job->processing_date = null;
                                    $job->save();
                                }
                            } else {
                                $order_detail->status = 'successful';
                            }

                        }
                    }

                    $user = User::find($member->user_id);
                    
                    $job = (new SendReceiptEmail($user, $order))->delay(150);
                    dispatch($job);

                } elseif ($payload->transaction_type == 'gift_certificate') {

                    $ml = new MemberLog();
                    $ml->member_id = $order->member->id;
                    $ml->message = $order->member->first_name . '(' . $order->member->id . ') has placed a gift certificate. ';
                    $ml->after = $order->toJson();
                    $ml->save();

                    if ($request->has('paypal_id')) {
                        $temp = Order::where('paypal_id', "=", $request->input('paypal_id'))->first();

                        if (!is_null($temp)) {
                            throw new \Exception('paypal_id_already_exist');
                        }

                        $order->paypal_id = $request->input('paypal_id');
                    } else {
                        throw new \Exception('no_paypal_id');
                    }

                    $order->paid_total_price = $order->total_price;

                    $order_details = OrderDetail::where('order_id', $order->id)->get();

                    foreach ($order_details as $order_detail) {
                        $qty = intval($order_detail->qty);

                        if (floatval($order->actual_total_price) == 0) {
                            $discounted_individual_sale_price = 0;
                        } else {
                            $discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                        }

                        $order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                        $order_detail->paid_subtotal_unit_price = $discounted_individual_sale_price * $qty;
                        $order_detail->save();

                        $product_payload = \GuzzleHttp\json_decode($order_detail->payload);

                        $gf = new GiftCertificate();
                        $gf->venue_id = $order->venue_id;
                        $gf->voucher_setups_id = $order_detail->voucher_setups_id;
                        $gf->order_details_id = $order_detail->id;
                        $gf->order_id = $order->id;
                        $gf->purchaser_id = $member->id;
                        $gf->full_name = $payload->gc_full_name;
                        $gf->email = $payload->gc_email;
                        $gf->note = $payload->gc_note;
                        $gf->value = $product_payload->gift_value;
                        $gf->save();

                        // ----------------------

                        // bepoz job - issue voucher
                        $data = array(
                            "member_id" => $member->id,
                            "gift_certificate_id" => $gf->id,
                            "voucher_setups_id" => $order_detail->voucher_setups_id,
                            "value" => $product_payload->gift_value,
                            "order_id" => $order->id,
                            "order_details_id" => $order_detail->id
                        );

                        $gf->payload = json_encode($data);
                        $gf->save();

                        $job = new BepozJob();
                        $job->queue = "issue-gift-certificate";
                        $job->payload = json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                    //    $user = User::find($member->user_id);
                    //    $job = (new SendReceiptEmail($user, $order))->delay(150);
                    //    dispatch($job);
                    }
                }

                // friend referral
                $fr = FriendReferral::where('new_member_id', $member->id)
                    ->where('is_accepted', true)
                    ->where('has_joined', true)
                    ->where('reward_option', 'after_purchase')
                    ->where('has_made_a_purchase', false)
                    ->first();

                if (!is_null($fr)) {
                    $fr->made_purchase_at = Carbon::now(config('app.timezone'));
                    $fr->has_made_a_purchase = true;
                    $fr->save();
                }

                // create transaction

                $data = array(
                    "member_id" => $member->id,
                    "order_id" => $order->id
                );

                $job = new BepozJob();
                $job->queue = "create-transaction";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();

            }

            DB::commit();

            if ($request->input('option') == "cancelled") {
                return response()->json(['status' => 'ok', 'message' => 'Cancelled order is successful.']);
            } else {
                if ($order->type == "point") {
                    return response()->json(['status' => 'ok', 'message' => 'Confirming order is successful.']);
                } else {
                    return response()->json(['status' => 'ok', 'message' => 'Confirming order is successful.', 'reward' => $order->claimable_point_reward]);
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();

            Log::info($e);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Confirming order is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Member/TransactionController.php';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Place an order
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function placeOrder(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'order' => 'required',
                'type' => 'required',
                'total' => 'required',
                'listing_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::find($request->input('listing_id'));

            if (is_null($listing)) {
                throw new \Exception('Listing not found.');
            }

            DB::beginTransaction();

            $order_type = $request->input('type');

            if (strcmp($order_type, 'cash') !== 0) {
                if (strcmp($order_type, 'point') !== 0) {
                    if (strcmp($order_type, 'mix') !== 0) {
                        return response()->json(['status' => 'error', 'message' => 'invalid order type'], Response::HTTP_BAD_REQUEST);
                    }
                }
            }

            $user = $request->input('decrypted_token');
            $redeem_point_ratio = Setting::where('key', 'redeem_point_ratio')->first();
            if (is_null($redeem_point_ratio)) {
                throw new \Exception('Missing setting: redeem_point_ratio.');
            }

            $reward_point_ratio = Setting::where('key', 'reward_point_ratio')->first();
            if (is_null($reward_point_ratio)) {
                throw new \Exception('Missing setting: reward_point_ratio.');
            }

            $company_phone_number = Setting::where('key', 'company_phone')->first();
            if (is_null($company_phone_number)) {
                throw new \Exception('Missing setting: company_phone.');
            }

            $user_total = \GuzzleHttp\json_decode($request->input('total'));
            if (is_null($user_total)) {
                throw new \Exception('Invalid TOTAL JSON object.');
            }

            if ($order_type == "mix") {
                if (!(array_key_exists('cash', $user_total) && array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of cash and point required.');
                }
            } elseif ($order_type == "point") {
                if (!(array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of point required.');
                }
            } elseif ($order_type == "cash") {
                if (!(array_key_exists('cash', $user_total))) {
                    throw new \Exception('The amount of cash required.');
                }
            }

            // Log::info("placeOrder");
            // Log::info($user_total);


            $request->offsetSet('time', time());
            $request->offsetSet('transaction_type', 'place_order');
            if ($listing->listing_type_id == 1) {
                $request->offsetSet('product_type', 'ticket');
            } else if ($listing->listing_type_id == 9) {
                $request->offsetSet('product_type', 'shop');
            } else if ($listing->listing_type_id == 10) {
                $request->offsetSet('product_type', 'membership');
            }
            $payload = \GuzzleHttp\json_encode($request->all());
            $token = md5($payload);

            $Order = new Order;
            $Order->payload = $payload;
            $Order->type = $order_type;
            $Order->token = $token;
            $Order->expired = time();
            $Order->member_id = $user->member->id;
            $Order->ip_address = $request->ip();

            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            if ( $venue_number === 'single' ){
                $Order->venue_id = 0;
            } else {
                $Order->venue_id = $listing->venue_id;
            }
            
            $Order->listing_id = $request->input('listing_id');
            $Order->ordered_at = Carbon::now(config('app.timezone'));
            $Order->comments = $request->has('comments') ? $request->input('comments') : null;
            $Order->transaction_type = 'place_order';
            $Order->save();

            $orders = \GuzzleHttp\json_decode($request->input('order'));

            if (is_null($orders)) {
                throw new \Exception('Invalid ORDER JSON object.');
            }

            $grand_total = 0;
            $grand_total_quantity = 0;
            $grand_point_reward = 0;

            if (!is_array($orders)) {
                $orders = [$orders];
            }

            foreach ($orders as $obj) { //loop products
                if (isset($obj->qty)) {
                    $qty = intval($obj->qty);
                    if ($qty > 0) { // check order quantity
                        $grand_total_quantity += $qty;

                        $pdt = Product::with('product_type')->find($obj->product->id);

                        if (is_null($pdt)) {
                            throw new \Exception('Product not found');
                        }

                        if ($listing->listing_type_id == 9){
                            

                        } 
                        else if ($listing->listing_type_id == 10){
                            

                        }
                        else {
                            if ($pdt->bepoz_voucher_setup_id === '0' || intval($pdt->bepoz_voucher_setup_id) === 0) {
                                throw new \Exception("Sorry for the inconvenience, unable to place order. Technical problem: Missing voucher setup id on this item.");
                            }
                        }

                        $product_name = $pdt->product_type->name;

                        $countMaxLimit = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimit)) {
                            $countMaxLimit = 0;
                        }

                        $countMaxLimitPerDay = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDay)) {
                            $countMaxLimitPerDay = 0;
                        }

                        $countMaxLimitPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerAccount)) {
                            $countMaxLimitPerAccount = 0;
                        }

                        $countMaxLimitPerDayPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDayPerAccount)) {
                            $countMaxLimitPerDayPerAccount = 0;
                        }

                        if (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }
                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        }

                        $orderDetail = new OrderDetail;
                        $orderDetail->voucher_name = is_null($pdt->voucher_setup) ? null : $pdt->voucher_setup->name;
                        $orderDetail->voucher_setups_id = $pdt->bepoz_voucher_setup_id;
                        $orderDetail->listing_id = $request->input('listing_id');
                        $orderDetail->product_name = $pdt->name;
                        $orderDetail->qty = $qty;
                        $orderDetail->order_id = $Order->id;
                        $orderDetail->status = "not_started";
                        $orderDetail->product_id = $pdt->id;
                        $orderDetail->product_type_id = $pdt->product_type->id;
                        $orderDetail->bepoz_product_number = $pdt->bepoz_product_number;
                        $orderDetail->category = is_null($pdt->category) ? 'ticket' : $pdt->category;
                        $orderDetail->venue_id = $Order->venue_id;

                        if ($order_type == "cash") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            }

                        } elseif ($order_type == "mix") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }

                            }


                        } elseif ($order_type == "point") {

                            if ($pdt->is_free) {
                                $orderDetail->point_price = 0;
                                $total_points = 0 * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            } else {
                                $orderDetail->point_price = $pdt->point_price;
                                $total_points = intval($pdt->point_price) * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            }


                        }

                        $orderDetail->save();

                    }
                }

            }

            if ($grand_total_quantity == 0) {
                throw new \Exception('No placing order has been made. No quantity');
            }

            if ($listing->max_limit != 0) {
                if ((intval(OrderDetail::where('listing_id', $request->input('listing_id'))->where('status', 'successful')->sum('qty')) + intval($grand_total_quantity)) > intval($listing->max_limit)) {
                    throw new \Exception("Sorry, all products under $listing->heading are sold out.");
                }
            }

            if ($order_type == "point") {
                $Order->actual_total_point = $grand_total;
                if ($grand_total > intval($user->member->points)) {
                    throw new \Exception('You do not have any Point to redeem the item(s)');
                } else {

                    // Log::info("grand_total");
                    // Log::info($grand_total);
                    // Log::info("user_total");
                    // Log::info($user_total->point);

                    if ($user_total->point != $grand_total) {
                        throw new \Exception('Incorrect total amount of point');
                    }

                    $Order->total_point = $grand_total;
                }
            } elseif ($order_type == "mix") {
                $Order->actual_total_price = $grand_total;
                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $user_point = intval($user_total->point);
                $user_cash = floatval($user_total->cash);
                $Order->point_reward = $grand_point_reward;

                if ($user_point > 0) {

                    if ($user_point > intval($user->member->points)) {
                        throw new \Exception('You do not have any Point to redeem the item(s)');
                    }

                    $poinz = $user_point / intval($redeem_point_ratio->value);
                    $rough_total = $poinz + $user_cash;
                    if ($rough_total == $grand_total) {
                        $Order->total_price = $user_cash;
                        $Order->total_point = $user_point;
                        $Order->claimable_point_reward = $user_cash * intval($reward_point_ratio->value);

                    } else {
                        //point ratio from the system and total transaction are not the same
                        throw new \Exception("Mismatched total amount");
                    }

                } else {
                    $Order->total_price = $grand_total;
                    $Order->claimable_point_reward = $grand_total * intval($reward_point_ratio->value);
                }

            } elseif ($order_type == "cash") {

                if ($user_total->cash != $grand_total) {
                    throw new \Exception('incorrect total amount of money');
                }

                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $Order->actual_total_price = $grand_total;
                $Order->total_price = $grand_total;
                $Order->point_reward = $grand_point_reward;
                $Order->claimable_point_reward = $grand_point_reward;
            }

            // update transaction record to send invoice
            $Order->payment_status = "pending";
            $Order->order_status = "pending";
            $Order->save();

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Placing order is successful.', 'token' => $token, 'total' => $request->input('total'), 'id' => $Order->id]);


        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);

            // $data = array(
            //     "title" => "WARNING: A customer is unable to place order!",
            //     "body" => $e->getMessage()
            // );

            // $job = (new SendPusherNotification('new-notification', $data));
            // dispatch($job);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Placing order is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Member/TransactionController.php';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
    }

    public function purchaseGiftCertificate(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'data' => 'required',
                'full_name' => 'required',
                'email' => 'required',

            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $reward_point_ratio = Setting::where('key', 'reward_point_ratio')->first();
            if (is_null($reward_point_ratio)) {
                throw new \Exception('Missing setting: reward_point_ratio.');
            }

            $user = $request->input('decrypted_token');

            $data = \GuzzleHttp\json_decode($request->input('data'));
            if (is_null($data)) {
                throw new \Exception('Invalid LISTING JSON object.');
            }

            $listing = Listing::with('products')->find($data->id);
            if (is_null($listing)) {
                throw new \Exception('Listing not found/valid.');
            }


            DB::beginTransaction();
            $request->offsetSet('time', time());
            $request->offsetSet('transaction_type', 'gift_certificate');
            $request->offsetSet('gc_full_name', $request->input('full_name'));
            $request->offsetSet('gc_email', $request->input('email'));
            $request->offsetSet('gc_note', $request->has('note') ? $request->input('note') : '');

            $payload = \GuzzleHttp\json_encode($request->all());
            $token = md5($payload);

            $grand_total = 0;
            $grand_point_reward = 0;

            $Order = new Order;
            $Order->payload = $payload;
            $Order->type = 'cash';
            $Order->token = $token;
            $Order->expired = time();
            $Order->member_id = $user->member->id;
            $Order->ip_address = $request->ip();
            $Order->ordered_at = Carbon::now(config('app.timezone'));
            
            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            if ( $venue_number === 'single' ){
                $Order->venue_id = 0;
            } else {
                $Order->venue_id = $listing->venue_id;
            }

            $Order->listing_id = $data->id;
            $Order->transaction_type = "gift_certificate";
            $Order->save();

            foreach ($listing->products as $p) {

                $pdt = $p->product;

                if ($pdt->bepoz_voucher_setup_id === 0) {
                    throw new \Exception("Sorry for the inconvenience, unable to purchase gift certificate. Technical problem: Missing voucher setup id on this item.");
                }

                $orderDetail = new OrderDetail;
                $orderDetail->qty = 1;
                $orderDetail->product_name = $pdt->name;
                $orderDetail->listing_id = $data->id;
                $orderDetail->venue_id = $Order->venue_id;
                $orderDetail->order_id = $Order->id;
                $orderDetail->status = "not_started";
                $orderDetail->product_id = $pdt->id;
                $orderDetail->product_type_id = $pdt->product_type_id;
                $orderDetail->voucher_setups_id = $pdt->bepoz_voucher_setup_id;
                $orderDetail->category = 'gift_certificate';

                if ($pdt->is_free) {
                    $orderDetail->unit_price = 0;
                    $total_cost = 0 * 1;
                    $orderDetail->subtotal_unit_price = $total_cost;
                    $grand_total = $grand_total + $total_cost;
                    if ($pdt->system_point_ratio) {
                        $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                        $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                        $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                    } else {
                        $orderDetail->reward_point_ratio = intval($pdt->point_get);
                        $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                        $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                    }
                } else {
                    $orderDetail->unit_price = $pdt->unit_price;
                    $total_cost = floatval($pdt->unit_price) * 1;
                    $orderDetail->subtotal_unit_price = $total_cost;
                    $grand_total = $grand_total + $total_cost;
                    if ($pdt->system_point_ratio) {
                        $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                        $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                        $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                    } else {
                        $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                        $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                        $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                    }

                }

                $orderDetail->payload = \GuzzleHttp\json_encode($pdt);
                $orderDetail->save();
            }

            $Order->actual_total_price = $grand_total;
            $Order->total_price = $grand_total;
            $Order->point_reward = $grand_point_reward;
            $Order->claimable_point_reward = $grand_point_reward;

            // update transaction record to send invoice
            $Order->payment_status = "pending";
            $Order->order_status = "pending";
            $Order->save();

            if ($listing->max_limit != 0) {
                if ((intval(OrderDetail::where('listing_id', $data->id)->where('status', 'successful')->sum('qty')) + 1) > intval($listing->max_limit)) {
                    throw new \Exception("Sorry, all items under $listing->heading are sold out.");
                }
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Gift Certificate  purchase request is successfully made.', 'token' => $token, 'total' => $grand_total, 'id' => $Order->id]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);

            // $data = array(
            //     "title" => "WARNING: A customer is unable to purchase gift-certificate!",
            //     "body" => $e->getMessage()
            // );

            // $job = (new SendPusherNotification('new-notification', $data));
            // dispatch($job);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function claimPromotion(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'promotion_data' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $dummy = urldecode($request->input('promotion_data'));
            $promotion_data = \GuzzleHttp\json_decode($dummy);
            if (is_null($promotion_data)) {
                throw new Exception('Invalid PROMOTION DATA JSON object.');
            }

            DB::beginTransaction();

            $user = $request->input('decrypted_token');

            foreach ($promotion_data as $listing) {
                //verify promotion obj
                $obj = Listing::whereHas('type', function ($query) {
                    $query->where('key', 'like', '%"category":"promotion"%');
                    $query->where('key', 'like', '%"feature_display_schedules":true%');
                    $query->where('key', 'like', '%"feature_vouchers":true%');
                })
                    ->has('products.product.product_type')
                    ->where('id', '=', $listing->id)
                    ->first();

                if (is_null($obj)) {
                    throw new \Exception("Promotion ($listing->heading) is no longer valid.");
                }

                if ($obj->products->first()->product->product_type->key->type !== 'voucher') {
                    throw new \Exception("Sorry for the inconvenience, unable to claim this promotion. Technical problem: Incorrect item (should be voucher).");
                }

                $countMaxLimit = ClaimedPromotion::where('product_id', $obj->products->first()->product->id)
                    ->where('product_type_id', $obj->products->first()->product->product_type->id)
                    ->where('listing_id', $obj->id)
                    ->where('listing_type_id', $obj->type->id)
                    ->count();

                if (is_null($countMaxLimit)) {
                    $countMaxLimit = 0;
                }

                $countMaxLimitPerDay = ClaimedPromotion::where('product_id', $obj->products->first()->product->id)
                    ->where('product_type_id', $obj->products->first()->product->product_type->id)
                    ->where('listing_id', $obj->id)
                    ->where('listing_type_id', $obj->type->id)
                    ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                    ->count();

                if (is_null($countMaxLimitPerDay)) {
                    $countMaxLimitPerDay = 0;
                }

                $countMaxLimitPerAccount = ClaimedPromotion::where('product_id', $obj->products->first()->product->id)
                    ->where('product_type_id', $obj->products->first()->product->product_type->id)
                    ->where('listing_id', $obj->id)
                    ->where('listing_type_id', $obj->type->id)
                    ->where('member_id', $user->member->id)
                    ->where('tier_id', $user->member->tiers()->first()->id)
                    ->count();

                if (is_null($countMaxLimitPerAccount)) {
                    $countMaxLimitPerAccount = 0;
                }

                $countMaxLimitPerDayPerAccount = ClaimedPromotion::where('product_id', $obj->products->first()->product->id)
                    ->where('product_type_id', $obj->products->first()->product->product_type->id)
                    ->where('listing_id', $obj->id)
                    ->where('listing_type_id', $obj->type->id)
                    ->where('member_id', $user->member->id)
                    ->where('tier_id', $user->member->tiers()->first()->id)
                    ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                    ->count();

                if (is_null($countMaxLimitPerDayPerAccount)) {
                    $countMaxLimitPerDayPerAccount = 0;
                }

                if (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) === 0 &&
                    intval($obj->max_limit_per_account) === 0 &&
                    intval($obj->max_limit_per_day_per_account) === 0
                ) {
                    if ($countMaxLimit >= $obj->max_limit) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)!");
                    }
                } elseif (intval($obj->max_limit) === 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) === 0 &&
                    intval($obj->max_limit_per_day_per_account) === 0
                ) {
                    if ($countMaxLimitPerDay >= $obj->max_limit_per_day) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day items per day");
                    }
                } elseif (intval($obj->max_limit) === 0 &&
                    intval($obj->max_limit_per_day) === 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) === 0
                ) {
                    if ($countMaxLimitPerAccount >= $obj->max_limit_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_account items per account");
                    }
                } elseif (intval($obj->max_limit) === 0 &&
                    intval($obj->max_limit_per_day) === 0 &&
                    intval($obj->max_limit_per_account) === 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) {
                    if ($countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                } elseif (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) === 0 &&
                    intval($obj->max_limit_per_day_per_account) === 0
                ) {
                    if ($countMaxLimit >= $obj->max_limit || $countMaxLimitPerDay >= $obj->max_limit_per_day) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day items per day");
                    }
                } elseif (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) === 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) === 0
                ) {
                    if ($countMaxLimit >= $obj->max_limit || $countMaxLimitPerAccount >= $obj->max_limit_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_account items per account");
                    }
                } elseif (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) === 0 &&
                    intval($obj->max_limit_per_account) === 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) {
                    if ($countMaxLimit >= $obj->max_limit || $countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                } elseif (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) === 0
                ) {
                    if ($countMaxLimit >= $obj->max_limit || $countMaxLimitPerDay >= $obj->max_limit_per_day || $countMaxLimitPerAccount >= $obj->max_limit_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_account items per account");
                    }
                } elseif (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) === 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) {
                    if ($countMaxLimit >= $obj->max_limit || $countMaxLimitPerDay >= $obj->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                } elseif (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) {
                    if ($countMaxLimit >= $obj->max_limit || $countMaxLimitPerDay >= $obj->max_limit_per_day || $countMaxLimitPerAccount >= $obj->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                } elseif (intval($obj->max_limit) === 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) === 0
                ) {
                    if ($countMaxLimitPerDay >= $obj->max_limit_per_day || $countMaxLimitPerAccount >= $obj->max_limit_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_account items per account");
                    }
                } elseif (intval($obj->max_limit) === 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) === 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) {
                    if ($countMaxLimitPerDay >= $obj->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                } elseif (intval($obj->max_limit) === 0 &&
                    intval($obj->max_limit_per_day) > 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) {
                    if ($countMaxLimitPerDay >= $obj->max_limit_per_day || $countMaxLimitPerAccount >= $obj->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                } elseif (intval($obj->max_limit) === 0 &&
                    intval($obj->max_limit_per_day) === 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) {
                    if ($countMaxLimitPerAccount >= $obj->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                } elseif (intval($obj->max_limit) > 0 &&
                    intval($obj->max_limit_per_day) === 0 &&
                    intval($obj->max_limit_per_account) > 0 &&
                    intval($obj->max_limit_per_day_per_account) > 0
                ) { 
                    // new combination
                    if ( $countMaxLimit >= $obj->max_limit || 
                            $countMaxLimitPerAccount >= $obj->max_limit_per_account ||
                            $countMaxLimitPerDayPerAccount >= $obj->max_limit_per_day_per_account ) {
                        throw new \Exception("Promotion ($listing->heading) has closed (sold out)! Only $obj->max_limit_per_day_per_account items per day per account");
                    }
                }


                $claim_promotion = new ClaimedPromotion();
                $claim_promotion->product_id = $obj->products->first()->product->id;
                $claim_promotion->product_type_id = $obj->products->first()->product->product_type->id;
                $claim_promotion->listing_id = $obj->id;
                $claim_promotion->tier_id = $user->member->tiers()->first()->id;
                $claim_promotion->listing_type_id = $obj->type->id;
                $claim_promotion->member_id = $user->member->id;
                $claim_promotion->payload = \GuzzleHttp\json_encode($request->all());
                $claim_promotion->venue_id = $obj->venue_id;

                if ($obj->products->first()->product->bepoz_voucher_setup_id === '0' || intval($obj->products->first()->product->bepoz_voucher_setup_id) === 0) {
                    throw new \Exception("Sorry for the inconvenience, unable to claim this promotion. Technical problem: Missing voucher setup id on this item.");
                }

                $claim_promotion->voucher_setups_id = $obj->products->first()->product->bepoz_voucher_setup_id;

                $claim_promotion->status = "pending";
                $claim_promotion->save();

                $request->offsetSet('time', time());
                $request->offsetSet('transaction_type', 'claim_promotion');
                $payload = \GuzzleHttp\json_encode($request->all());
                $token = md5($payload);

                $Order = new Order();
                $Order->type = "cash";
                $Order->expired = time();
                $Order->member_id = $user->member->id;
                $Order->ordered_at = Carbon::now(config('app.timezone'));
                $Order->voucher_status = 'successful';
                $Order->payment_status = 'successful';
                $Order->order_status = 'confirmed';
                $Order->payload = $payload;
                $Order->token = $token;
                $Order->ip_address = $request->ip();
                $Order->total_price = $obj->products->first()->product->unit_price;
                $Order->total_point = $obj->products->first()->product->point_price;
                $Order->paid_total_point = $obj->products->first()->product->point_price;
                $Order->paid_total_price = $obj->products->first()->product->unit_price;
                $Order->venue_id = $obj->venue_id;
                $Order->listing_id = $obj->id;
                $Order->transaction_type = "claim_promotion";
                $Order->save();


                $od = new OrderDetail();
                $od->listing_id = $obj->id;
                $od->product_id = $obj->products->first()->product->id;
                $od->product_name = $obj->products->first()->product->name;
                $od->venue_id = $obj->venue_id;
                $od->product_type_id = $obj->products->first()->product->product_type->id;
                if ($obj->products->first()->product->is_free) {
                    $od->unit_price = 0;
                    $od->point_price = 0;
                    $od->subtotal_unit_price = 0;
                    $od->subtotal_point_price = 0;
                    $od->paid_subtotal_unit_price = 0;
                    $od->paid_subtotal_point_price = 0;
                } else {
                    if (intval($obj->products->first()->product->point_price) !== 0) {
                        if (intval($obj->products->first()->product->point_price) > intval($user->member->points)) {
                            throw new \Exception('You do not have any Point to redeem the item(s). Price: ' . intval($obj->products->first()->product->point_price) . ' pts.');
                        }

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $user->member->id;
                        $pl->points_before = $user->member->points;
                        $pl->points_after = floatval($user->member->points) - intval($obj->products->first()->product->point_price);
                        $pl->desc_short = "Points used for paying products.";
                        $pl->desc_long = "Used " . intval($obj->products->first()->product->point_price) . " points for Promotion #" . $Order->id . " (Point transaction)";
                        $pl->save();

                        // sync point
                        $data = array(
                            "point" => abs(floatval($obj->products->first()->product->point_price)),
                            "id" => $user->member->bepoz_account_id,
                            "member_id" => $user->member->id,
                            "status" => 'redeem'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $user->member->points = floatval($user->member->points) - intval($obj->products->first()->product->point_price);
                        $user->member->save();
                    }

                    $od->unit_price = $obj->products->first()->product->unit_price;
                    $od->point_price = $obj->products->first()->product->point_price;
                    $od->subtotal_unit_price = $obj->products->first()->product->unit_price;
                    $od->subtotal_point_price = $obj->products->first()->product->point_price;
                    $od->paid_subtotal_unit_price = $obj->products->first()->product->unit_price;
                    $od->paid_subtotal_point_price = $obj->products->first()->product->point_price;
                }

                $od->category = 'promotion';
                $od->voucher_name = $obj->heading;
                $od->voucher_setups_id = $claim_promotion->voucher_setups_id;
                $od->order_id = $Order->id;
                $od->qty = 1;
                $od->status = 'successful';
                $od->save();

                $memberVoucher = new MemberVouchers();
                if (is_null($od->voucher_setup)) {
                    throw new \Exception("Sorry for the inconvenience, unable to claim this promotion. Technical problem: Missing voucher setup id on this item.");
                }
                $memberVoucher->name = $od->voucher_setup->name;
                $memberVoucher->member_id = $user->member->id;
                $memberVoucher->lookup = "100000000";
                $memberVoucher->barcode = "";
                $memberVoucher->order_id = $Order->id;
                $memberVoucher->order_details_id = $od->id;
                $memberVoucher->amount_left = 100;
                $memberVoucher->amount_issued = 100;
                $memberVoucher->voucher_setup_id = $claim_promotion->voucher_setups_id;
                $memberVoucher->category = 'promotion';
                $memberVoucher->save();

                // ----------------------

                // bepoz job - issue voucher
                $data = array(
                    "id" => $claim_promotion->id,
                    "member_id" => $claim_promotion->member_id,
                    "member_voucher_id" => $memberVoucher->id,
                    "voucher_setups_id" => $claim_promotion->voucher_setups_id,
                    "order_detail_id" => $od->id
                );

                $job = new BepozJob();
                $job->queue = "issue-bepoz-promotion-voucher";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();

                $ml = new MemberLog();
                $ml->member_id = $user->member->id;
                $ml->message = $user->member->first_name . '(' . $user->member->id . ') has claimed a promotion. ';
                $ml->after = $Order->toJson();
                $ml->save();

            }

            DB::commit();

            // Trigger Die-hard ...
            Artisan::call('issue-bepoz-promotion');

            return response()->json(['status' => 'ok', 'message' => 'Claiming promotion is successful.']);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);

            // $data = array(
            //     "title" => "WARNING: A customer is unable to claim promotion!",
            //     "body" => $e->getMessage()
            // );

            // $job = (new SendPusherNotification('new-notification', $data));
            // dispatch($job);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function generateReceipt($token)
    {
        try {
            $order = Order::where('token', $token)->first();

            if (is_null($order)) {
                return response()->json(['status' => 'error', 'message' => 'Token invalid'], Response::HTTP_BAD_REQUEST);
            }

            if (is_null($order->bepoz_transaction_ids)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('success.success', array(
                    "message" => 'We are generating your receipt. Please wait and try again.',
                    'company_logo' => $invoice_logo
                ));
            }

            $member = Member::find($order->member_id);
            $user = User::find($member->user_id);


            $order_details = OrderDetail::where('order_id', $order->id)->get();

            $company_email = Setting::where('key', 'company_email')->first()->value;
            $company_name = Setting::where('key', 'company_name')->first()->value;
            $company_abn = Setting::where('key', 'company_abn')->first()->value;
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;


            $company_address1 =
                Setting::where('key', 'company_street_number')->first()->value
                . ' ' .
                Setting::where('key', 'company_street_name')->first()->value;

            $company_address2 =
                Setting::where('key', 'company_suburb')->first()->value
                . ' ' .
                Setting::where('key', 'company_postcode')->first()->value
                . ' ' .
                Setting::where('key', 'company_state')->first()->value;

            $data = array();
            $data['member'] = $member;
            $data['user'] = $user;
            $data['order'] = $order;
            $data['order_details'] = $order_details;
            $data['company_name'] = $company_name;
            $data['company_address1'] = $company_address1;
            $data['company_address2'] = $company_address2;
            $data['company_abn'] = $company_abn;
            $data['company_logo'] = $invoice_logo;

            if (!is_null($order->bepoz_transaction_ids)) {
                $ids = \GuzzleHttp\json_decode($order->bepoz_transaction_ids, true);

                $data['bepoz_transaction_ids'] = implode(", ", $ids);
            }

            $payload = \GuzzleHttp\json_decode($order->payload);
            $subject = '';
            $data['transaction_type'] = '';
            if ($payload->transaction_type == 'place_order') {
                $data['transaction_type'] = 'place_order';
            } elseif ($payload->transaction_type == 'reservation') {


            } elseif ($payload->transaction_type == 'tier_upgrade') {
                $subject = '(Membership)';
                $data['transaction_type'] = 'tier_upgrade';
            } elseif ($payload->transaction_type == 'gift_certificate') {
                $subject = '(Gift Certificate)';
                $data['transaction_type'] = 'gift_certificate';
            }

            $pdf = App::make('dompdf.wrapper')
                ->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
                ->loadView('pdf.receipt', $data);
            //$pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled', true]);
            return $pdf->download('receipt.pdf');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    
    protected function generateReceiptBooking($token)
    {
        try {
            $order = Order::where('token', $token)->first();

            if (is_null($order)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array(
                    "message" => 'Token invalid',
                    'company_logo' => $invoice_logo
                ));
            }

            if (is_null($order->bepoz_transaction_ids)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('success.success', array(
                    "message" => 'We are generating your receipt. Please wait and try again.',
                    'company_logo' => $invoice_logo
                ));
            }

            $member = Member::find($order->member_id);
            if (is_null($member)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array(
                    "message" => 'Member data missing',
                    'company_logo' => $invoice_logo
                ));
            }
            
            $user = User::find($member->user_id);
            
            if (is_null($user)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array(
                    "message" => 'Member data missing',
                    'company_logo' => $invoice_logo
                ));
            }

            $order_details = OrderDetail::with('product_type')->where('order_id', $order->id)->get();

            foreach ($order_details as $order_detail) {
                //if ($order_detail->product_type->name == "F&B Voucher") {
                if ( $order_detail->product_type_id == intval(3) ) {
                    $order_detail->product_type->name = "Credit Voucher";
                    $order_detail->product_name = "$".abs($order_detail->unit_price);
                }
            }

            $company_email = Setting::where('key', 'company_email')->first()->value;
            $company_name = Setting::where('key', 'company_name')->first()->value;
            $company_abn = Setting::where('key', 'company_abn')->first()->value;
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;


            $company_address1 =
                Setting::where('key', 'company_street_number')->first()->value
                . ' ' .
                Setting::where('key', 'company_street_name')->first()->value;

            $company_address2 =
                Setting::where('key', 'company_suburb')->first()->value
                . ' ' .
                Setting::where('key', 'company_postcode')->first()->value
                . ' ' .
                Setting::where('key', 'company_state')->first()->value;

            $data = array();
            $data['member'] = $member;
            $data['user'] = $user;
            $data['order'] = $order;
            $data['order_details'] = $order_details;
            $data['company_name'] = $company_name;
            $data['company_address1'] = $company_address1;
            $data['company_address2'] = $company_address2;
            $data['company_abn'] = $company_abn;
            $data['company_logo'] = $invoice_logo;

            if (!is_null($order->bepoz_transaction_ids)) {
                $ids = \GuzzleHttp\json_decode($order->bepoz_transaction_ids, true);

                $data['bepoz_transaction_ids'] = implode(", ", $ids);
            }

            $payload = \GuzzleHttp\json_decode($order->payload);
            $subject = '';
            $data['transaction_type'] = '';
            if ($payload->transaction_type == 'place_order') {
                $data['transaction_type'] = 'place_order';
            } elseif ($payload->transaction_type == 'reservation') {


            } elseif ($payload->transaction_type == 'tier_upgrade') {
                $subject = '(Membership)';
                $data['transaction_type'] = 'tier_upgrade';
            } elseif ($payload->transaction_type == 'gift_certificate') {
                $subject = '(Gift Certificate)';
                $data['transaction_type'] = 'gift_certificate';
            }

            //$pdf = App::make('dompdf.wrapper')->loadView('pdf.receipt', $data);
            $pdf = App::make('dompdf.wrapper')->loadView('pdf.receipt-booking', $data);
            //$pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled', true]);
            return $pdf->download('receipt.pdf');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function useStripe(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                // 'id_token' => 'required',
                // 'access_token' => 'required',
                'stripe_token' => 'required',
                'order_token' => 'required',
                'use_saved_card' => 'required|boolean',  // option to use existing card
                'save_card' => 'required|boolean' // option to store card in stripe db
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $company_name = Setting::where('key', 'company_name')->first()->value;

            DB::beginTransaction();

            $order = Order::where('token', $request->input('order_token'))->first();
            $order->paid_total_price = $order->total_price;
            $order->voucher_status = "pending";
            $order->order_status = "confirmed";
            $order->payment_status = "successful";
            $order->confirmed_at = Carbon::now(config('app.timezone'));
            $order->paid_at = Carbon::now(config('app.timezone'));

            $stripe_test_mode = Setting::where('key', 'stripe_testmode')->first()->value;

            if ($stripe_test_mode === 'true') {
                $stripe_key = Setting::where('key', 'stripe_testkey')->first()->value;
                $stripe_account = Setting::where('key', 'stripe_test_connected_account_id')->first()->value;

                $fee_option = Setting::where('key', 'stripe_test_application_fee_option')->first()->value;
                if ($fee_option === 'fixed') {
                    $application_fee = intval(Setting::where('key', 'stripe_test_fixed_application_fee')->first()->value);
                } else {
                    $percentage = Setting::where('key', 'stripe_test_dynamic_application_fee')->first()->value;
                    $application_fee = ($order->total_price / 100) * intval($percentage);
                    $application_fee = intval($application_fee * 100);
                }

            } else {
                $stripe_key = Setting::where('key', 'stripe_key')->first()->value;
                $stripe_account = Setting::where('key', 'stripe_connected_account_id')->first()->value;

                $fee_option = Setting::where('key', 'stripe_application_fee_option')->first()->value;
                if ($fee_option === 'fixed') {
                    $application_fee = intval(Setting::where('key', 'stripe_fixed_application_fee')->first()->value);
                } else {
                    $percentage = Setting::where('key', 'stripe_dynamic_application_fee')->first()->value;
                    $application_fee = ($order->total_price / 100) * intval($percentage);
                    $application_fee = intval($application_fee * 100);
                }
            }

            $stripe_solution = Setting::where('key', 'stripe_payment_solution')->first()->value;

            // create stripe obj (for standard)
            \Stripe\Stripe::setApiKey($stripe_key);

            $stripe_total_price = intval(floatval($order->total_price) * 100);

            // find cust obj
            $customer = StripeCustomer::where('member_id', $order->member_id)->first();
            if (is_null($customer)) {

                // check the option
                if ($request->input('use_saved_card')) {
                    return response()->json(['status' => 'error', 'message' => 'Sorry, you dont have any saved credit card in our system.'], Response::HTTP_BAD_REQUEST);
                }


                // save credit card detail
                if ($request->input('save_card')) {

                    // stripe_token contains stripe-token (e.g tok_1C3Y3Q2eZvKYlo2CNtVUZ4wY)

                    // create cus obj from stripe database
                    $cs = \Stripe\Customer::create(array(
                        'email' => $order->member->user->email,
                        'description' => 'Customer for ' . $order->member->user->email . ' from Venue: ' . $company_name,
                    ));

                    // create StripeCustomer object in myplace database
                    $customer = new StripeCustomer;
                    $customer->member_id = $order->member_id;
                    $customer->customer_id = $cs->id;
                    $customer->payload = \GuzzleHttp\json_encode($cs);
                    $customer->test_mode = (bool)$cs->livemode ? false : true;
                    $customer->save();

                    // create card
                    $css = \Stripe\Customer::retrieve($cs->id);
                    $card = $css->sources->create(array("source" => $request->input('stripe_token')));

                    // update default source
                    $cu = \Stripe\Customer::retrieve($cs->id);
                    $cu->default_source = $card->id; // obtained with Stripe.js
                    $cu->save();

                    // standard or connect
                    if ($stripe_solution === 'stripe_standard') {
                        // charge the card
                        \Stripe\Charge::create(array(
                            'currency' => 'AUD',
                            'customer' => $customer->customer_id,
                            'amount' => $stripe_total_price,
                            'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                        ));
                    } else {
                        // for connect
                        $token = \Stripe\Token::create(array(
                            "customer" => $customer->customer_id,
                        ), array("stripe_account" => $stripe_account));

                        $charge = \Stripe\Charge::create(array(
                            'amount' => $stripe_total_price,
                            "currency" => "AUD",
                            "source" => $token->id,
                            "application_fee" => $application_fee,
                            'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                        ), array("stripe_account" => $stripe_account));
                    }


                } else {

                    // standard or connect
                    if ($stripe_solution === 'stripe_standard') {

                        // charge the card
                        \Stripe\Charge::create(array(
                            'currency' => 'AUD',
                            'source' => $request->input('stripe_token'),
                            'amount' => $stripe_total_price,
                            'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                        ));
                    } else {

                        // for connect
                        $charge = \Stripe\Charge::create(array(
                            'amount' => $stripe_total_price,
                            "currency" => "AUD",
                            'source' => $request->input('stripe_token'),
                            "application_fee" => $application_fee,
                            'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                        ), array("stripe_account" => $stripe_account));
                    }
                }

            } else { // existing customer

                // check the option
                if ($request->input('use_saved_card')) {

                    // stripe_token contains card-token (e.g card_1C3Y3Q2eZvKYlo2CNtVUZ4wY)

                    // update default source
                    $cu = \Stripe\Customer::retrieve($customer->customer_id);
                    $cu->default_source = $request->input('stripe_token'); // obtained with Stripe.js
                    $cu->save();


                    // standard or connect
                    if ($stripe_solution === 'stripe_standard') {
                        // charge the card
                        \Stripe\Charge::create(array(
                            'currency' => 'AUD',
                            'customer' => $customer->customer_id,
                            'amount' => $stripe_total_price,
                            'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                        ));
                    } else {
                        // for connect
                        $token = \Stripe\Token::create(array(
                            "customer" => $customer->customer_id,
                        ), array("stripe_account" => $stripe_account));

                        $charge = \Stripe\Charge::create(array(
                            'amount' => $stripe_total_price,
                            "currency" => "AUD",
                            "source" => $token->id,
                            "application_fee" => $application_fee,
                            'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                        ), array("stripe_account" => $stripe_account));
                    }
                } else {

                    // save credit card detail
                    if ($request->input('save_card')) {

                        // stripe_token contains stripe-token (e.g tok_1C3Y3Q2eZvKYlo2CNtVUZ4wY)

                        // create card
                        $cus = \Stripe\Customer::retrieve($customer->customer_id);
                        $card = $cus->sources->create(array("source" => $request->input('stripe_token')));

                        // update default source
                        $cu = \Stripe\Customer::retrieve($customer->customer_id);
                        $cu->default_source = $card->id;
                        $cu->save();


                        // standard or connect
                        if ($stripe_solution === 'stripe_standard') {

                            // charge the card
                            \Stripe\Charge::create(array(
                                'currency' => 'AUD',
                                'customer' => $customer->customer_id,
                                'amount' => $stripe_total_price,
                                'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                            ));
                        } else {
                            // for connect
                            $token = \Stripe\Token::create(array(
                                "customer" => $customer->customer_id,
                            ), array("stripe_account" => $stripe_account));

                            $charge = \Stripe\Charge::create(array(
                                'amount' => $stripe_total_price,
                                "currency" => "AUD",
                                "source" => $token->id,
                                "application_fee" => $application_fee,
                                'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                            ), array("stripe_account" => $stripe_account));

                        }
                    } else {

                        // stripe_token contains stripe-token (e.g tok_1C3Y3Q2eZvKYlo2CNtVUZ4wY)
                    //    $cu = \Stripe\Customer::retrieve($customer->customer_id);
                    //    $cu->source = $request->input('stripe_token');
                    //    $cu->default_source = null;
                    //    $cu->save();

                        // standard or connect
                        if ($stripe_solution === 'stripe_standard') {
                            // charge the card without saving it
                            \Stripe\Charge::create(array(
                                'currency' => 'AUD',
                                'customer' => $customer->customer_id,
                                'amount' => $stripe_total_price,
                                'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                            ));
                        } else {
                            // for connect
                            $token = \Stripe\Token::create(array(
                                "customer" => $customer->customer_id,
                            ), array("stripe_account" => $stripe_account));

                            $charge = \Stripe\Charge::create(array(
                                'amount' => $stripe_total_price,
                                "currency" => "AUD",
                                "source" => $token->id,
                                "application_fee" => $application_fee,
                                'description' => 'Charge for ' . $company_name . '-LOYALTY-APP with OrderID: ' . $order->id . ' (' . $order->token . ')'
                            ), array("stripe_account" => $stripe_account));
                        }
                    }

                }

            }

            $payment_log = new PaymentLog();
            $payment_log->order_id = $order->id;
            $payment_log->response = \GuzzleHttp\json_encode($charge);
            $payment_log->status = 'successful';
            $payment_log->save();

            $rules = Setting::where('key', 'reward_multiply_rules')->first()->extended_value;
            $decoded_rules = \GuzzleHttp\json_decode($rules);

            $found = null;
            $now = Carbon::now(config('app.timezone'));
            foreach ($decoded_rules as $rule) {

                if (intval($rule->dayofweek) === intval($now->dayOfWeek)) {
                    $found = $rule;
                    break;
                }
            }

            $multiplied_points = 0;

            if (!is_null($found)) {
                $multiplied_points = floatval($order->point_reward) * intval($found->multiply);
                $order->claimable_point_reward = $multiplied_points;
            }

            // Log
            $pl = new PointLog();
            $pl->member_id = $order->member->id;
            $pl->points_before = $order->member->points;
            $pl->points_after = floatval($order->member->points) + $multiplied_points;
            $pl->desc_short = "Point rewards gained by buying products.";
            $pl->desc_long = "Gained " . floatval($multiplied_points) . " points ($order->point_reward pts multiplied by $found->multiply) from Transaction #" . $order->id;
            $pl->save();

            $order->member->points = floatval($order->member->points) + floatval($multiplied_points);

            // sync point
            $data = array(
                "point" => abs(floatval($multiplied_points)),
                "id" => $order->member->bepoz_account_id,
                "member_id" => $order->member->id,
                "status" => 'earn'
            );

            $job = new BepozJob();
            $job->queue = "sync-point";
            $job->payload = \GuzzleHttp\json_encode($data);
            $job->available_at = time();
            $job->created_at = time();
            $job->save();

            $pl = \GuzzleHttp\json_decode($order->payload, true);
            $pl['api_request'] = $request->all();
            $order->payload = \GuzzleHttp\json_encode($pl);

            $order->member->save();
            $order->save();

            $payload = \GuzzleHttp\json_decode($order->payload);

            if ($payload->transaction_type == 'place_order') {

                $ml = new MemberLog();
                $ml->member_id = $order->member->id;
                $ml->message = $order->member->first_name . '(' . $order->member->id . ') has placed an order (tickets). ';
                $ml->after = $order->toJson();
                $ml->save();

                $order->paid_total_price = $order->total_price;

                $order_details = OrderDetail::where('order_id', $order->id)->get();

                if ($order->listing->listing_type_id == 9){
                    // SHOP
                    // $member = $order->member;
                    // foreach ($order_details as $order_detail) {

                    //     $product = Product::find($order_detail->product_id);
                    //     // Log
                    //     $pl = new PointLog();
                    //     $pl->member_id = $member->id;
                    //     $pl->points_before = $member->points;
                    //     $pl->points_after = floatval($member->points);
                    //     $pl->desc_short = "Points used for paying products.";
                    //     $pl->desc_long = "Used " . $total_points . " points for Transaction #" . $order->id . " (Point transaction)";
                    //     $pl->save();

                    //     // sync point
                    //     $data = array(
                    //         "point" => abs(floatval($total_points)),
                    //         "id" => $member->bepoz_account_id,
                    //         "member_id" => $member->id,
                    //         "status" => 'redeem'
                    //     );

                    //     $job = new BepozJob();
                    //     $job->queue = "sync-point";
                    //     $job->payload = json_encode($data);
                    //     $job->available_at = time();
                    //     $job->created_at = time();
                    //     $job->save();

                    //     $member->points = floatval($member->points) - $total_points;
                    //     $member->save();
                    // }

                } 
                else if ($order->listing->listing_type_id == 10){
                    // MEMBERSHIP UPGRADE
                    $member = $order->member;
                    $member_tier = MemberTiers::where('member_id', $member->id)->first();
                    $member_tier->tier_id = $order->listing->membership_tier;
                    $member_tier->save();

                    if (!is_null($member->bepoz_account_id)) {
                        $data = array();
                        $data['member_id'] = $member->id;
        
                        $job = new BepozJob();
                        $job->queue = "update-account";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        // Log::info("Stripe Create Update Account");
                    }
                } 
                else {
                    // create vouchers
                    foreach ($order_details as $order_detail) {
                        $qty = intval($order_detail->qty);

                        if (floatval($order->actual_total_price) == 0) {
                            $discounted_individual_sale_price = 0;
                        } else {
                            $discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                        }

                        $order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                        $order_detail->paid_subtotal_unit_price = $discounted_individual_sale_price * $qty;
                        //$listing = Listing::find($order_detail->listing_id);

                        if ($order_detail->voucher_setups_id != 0) {

                            // ------------------------
                            for ($i = 0; $i < $qty; $i++) {

                                $memberVoucher = new MemberVouchers();
                                $memberVoucher->name = $order_detail->voucher_setup->name;
                                $memberVoucher->member_id = $order->member_id;
                                $memberVoucher->order_id = $order->id;
                                $memberVoucher->order_details_id = $order_detail->id;
                                $memberVoucher->lookup = "100000000";
                                $memberVoucher->barcode = "";
                                $memberVoucher->amount_left = 100;
                                $memberVoucher->amount_issued = 100;
                                $memberVoucher->voucher_setup_id = $order_detail->voucher_setups_id;
                                $memberVoucher->category = $order_detail->category;
                                $memberVoucher->save();

                                // ----------------------

                                // bepoz job - issue voucher
                                $data = array(
                                    "member_id" => $order->member_id,
                                    "member_voucher_id" => $memberVoucher->id,
                                    "voucher_setups_id" => $order_detail->voucher_setups_id,
                                    "order_id" => $order->id,
                                    "order_details_id" => $order_detail->id
                                );


                                $job = new BepozJob();
                                $job->queue = "issue-voucher";
                                $job->payload = json_encode($data);
                                $job->available_at = time();
                                $job->created_at = time();
                                $job->save();
                            }
                        } else {
                            $order_detail->status = 'successful';
                        }

                        $order_detail->save();
                    }
                }

                $user = $order->member->user;

                $job = (new SendReceiptEmail($user, $order))->delay(150);
                dispatch($job);

            } elseif ($payload->transaction_type == 'gift_certificate') {

                $ml = new MemberLog();
                $ml->member_id = $order->member->id;
                $ml->message = $order->member->first_name . '(' . $order->member->id . ') has purchased a gift certificate. ';
                $ml->after = $order->toJson();
                $ml->save();

                $order_details = OrderDetail::where('order_id', $order->id)->get();

                foreach ($order_details as $order_detail) {
                    $qty = intval($order_detail->qty);

                    if (floatval($order->actual_total_price) == 0) {
                        $discounted_individual_sale_price = 0;
                    } else {
                        $discounted_individual_sale_price = (floatval($order->paid_total_price) / floatval($order->actual_total_price)) * $order_detail->unit_price;
                    }

                    $order_detail->individual_discounted_unit_price = $discounted_individual_sale_price;
                    $order_detail->paid_subtotal_unit_price = $discounted_individual_sale_price * $qty;
                    $order_detail->save();

                    $product_payload = \GuzzleHttp\json_decode($order_detail->payload);

                    $gf = new GiftCertificate();
                    $gf->venue_id = $order->venue_id;
                    $gf->voucher_setups_id = $order_detail->voucher_setups_id;
                    $gf->order_details_id = $order_detail->id;
                    $gf->order_id = $order->id;
                    $gf->purchaser_id = $order->member_id;
                    $gf->full_name = $payload->gc_full_name;
                    $gf->email = $payload->gc_email;
                    $gf->note = $payload->gc_note;
                    $gf->value = $product_payload->gift_value;
                    $gf->save();

                    // ----------------------

                    // bepoz job - issue voucher
                    $data = array(
                        "member_id" => $order->member_id,
                        "gift_certificate_id" => $gf->id,
                        "voucher_setups_id" => $order_detail->voucher_setups_id,
                        "value" => $product_payload->gift_value,
                        "order_id" => $order->id,
                        "order_details_id" => $order_detail->id
                    );

                    $gf->payload = json_encode($data);
                    $gf->save();

                    $job = new BepozJob();
                    $job->queue = "issue-gift-certificate";
                    $job->payload = json_encode($data);
                    $job->available_at = time();
                    $job->created_at = time();
                    $job->save();
                }
            }

            // friend referral

            $fr = FriendReferral::where('new_member_id', $order->member->id)
                ->where('is_accepted', true)
                ->where('has_joined', true)
                ->where('reward_option', 'after_purchase')
                ->where('has_made_a_purchase', false)
                ->first();

            if (!is_null($fr)) {
                $fr->made_purchase_at = Carbon::now(config('app.timezone'));
                $fr->has_made_a_purchase = true;
                $fr->save();
            }

            // create transaction

            $data = array(
                "member_id" => $order->member->id,
                "order_id" => $order->id
            );

            $job = new BepozJob();
            $job->queue = "create-transaction";
            $job->payload = \GuzzleHttp\json_encode($data);
            $job->available_at = time();
            $job->created_at = time();
            $job->save();


            DB::commit();
            
            $user = $request->input('decrypted_token');
            $id_token = $request->has('id_token') ? $request->input('id_token') : "";

            return response()->json(['status' => 'ok', 'message' => 'Stripe successful.',
                'data' => $charge, 'reward' => $multiplied_points, 'original_reward' => $order->point_reward, 
                'reward_message' => 'Multiplied by ' . $found->multiply, 'id_token' => $id_token ]);

        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e);

            $order = Order::where('token', $request->input('order_token'))->first();

            $payment_log = new PaymentLog();
            $payment_log->order_id = $order->id;
            $payment_log->response = $e;
            $payment_log->status = 'failed';
            $payment_log->error_message = $e->getMessage();
            $payment_log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    
    /**
     * shop Place an order
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function shop(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'order' => 'required',
                'type' => 'required',
                'total' => 'required',
                'listing_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::find($request->input('listing_id'));

            if (is_null($listing)) {
                throw new \Exception('Listing not found.');
            }

            DB::beginTransaction();

            $order_type = $request->input('type');

            if (strcmp($order_type, 'cash') !== 0) {
                if (strcmp($order_type, 'point') !== 0) {
                    if (strcmp($order_type, 'mix') !== 0) {
                        return response()->json(['status' => 'error', 'message' => 'invalid order type'], Response::HTTP_BAD_REQUEST);
                    }
                }
            }

            $user = $request->input('decrypted_token');
            $redeem_point_ratio = Setting::where('key', 'redeem_point_ratio')->first();
            if (is_null($redeem_point_ratio)) {
                throw new \Exception('Missing setting: redeem_point_ratio.');
            }

            $reward_point_ratio = Setting::where('key', 'reward_point_ratio')->first();
            if (is_null($reward_point_ratio)) {
                throw new \Exception('Missing setting: reward_point_ratio.');
            }

            $company_phone_number = Setting::where('key', 'company_phone')->first();
            if (is_null($company_phone_number)) {
                throw new \Exception('Missing setting: company_phone.');
            }

            $user_total = \GuzzleHttp\json_decode($request->input('total'));
            if (is_null($user_total)) {
                throw new \Exception('Invalid TOTAL JSON object.');
            }

            if ($order_type == "mix") {
                if (!(array_key_exists('cash', $user_total) && array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of cash and point required.');
                }
            } elseif ($order_type == "point") {
                if (!(array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of point required.');
                }
            } elseif ($order_type == "cash") {
                if (!(array_key_exists('cash', $user_total))) {
                    throw new \Exception('The amount of cash required.');
                }
            }

            $request->offsetSet('time', time());
            $request->offsetSet('transaction_type', 'place_order');
            $payload = \GuzzleHttp\json_encode($request->all());
            $token = md5($payload);

            $Order = new Order;
            $Order->payload = $payload;
            $Order->type = $order_type;
            $Order->token = $token;
            $Order->expired = time();
            $Order->member_id = $user->member->id;
            $Order->ip_address = $request->ip();

            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            if ( $venue_number === 'single' ){
                $Order->venue_id = 0;
            } else {
                $Order->venue_id = $listing->venue_id;
            }
            
            $Order->listing_id = $request->input('listing_id');
            $Order->ordered_at = Carbon::now(config('app.timezone'));
            $Order->comments = $request->has('comments') ? $request->input('comments') : null;
            $Order->transaction_type = 'place_order';
            $Order->save();

            $orders = \GuzzleHttp\json_decode($request->input('order'));

            if (is_null($orders)) {
                throw new \Exception('Invalid ORDER JSON object.');
            }

            $grand_total = 0;
            $grand_total_quantity = 0;
            $grand_point_reward = 0;

            if (!is_array($orders)) {
                $orders = [$orders];
            }

            foreach ($orders as $obj) { //loop products
                if (isset($obj->qty)) {
                    $qty = intval($obj->qty);
                    if ($qty > 0) { // check order quantity
                        $grand_total_quantity += $qty;

                        $pdt = Product::with('product_type')->find($obj->product->id);

                        if (is_null($pdt)) {
                            throw new \Exception('Product not found');
                        }

                        // if ($pdt->bepoz_voucher_setup_id === '0' || intval($pdt->bepoz_voucher_setup_id) === 0) {
                        //     throw new \Exception("Sorry for the inconvenience, unable to place order. Technical problem: Missing voucher setup id on this item.");
                        // }

                        $product_name = $pdt->product_type->name;

                        $countMaxLimit = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimit)) {
                            $countMaxLimit = 0;
                        }

                        $countMaxLimitPerDay = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDay)) {
                            $countMaxLimitPerDay = 0;
                        }

                        $countMaxLimitPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerAccount)) {
                            $countMaxLimitPerAccount = 0;
                        }

                        $countMaxLimitPerDayPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDayPerAccount)) {
                            $countMaxLimitPerDayPerAccount = 0;
                        }

                        if (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }
                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        }

                        $orderDetail = new OrderDetail;
                        $orderDetail->voucher_name = is_null($pdt->voucher_setup) ? null : $pdt->voucher_setup->name;
                        $orderDetail->voucher_setups_id = $pdt->bepoz_voucher_setup_id;
                        $orderDetail->listing_id = $request->input('listing_id');
                        $orderDetail->product_name = $pdt->name;
                        $orderDetail->qty = $qty;
                        $orderDetail->order_id = $Order->id;
                        $orderDetail->status = "not_started";
                        $orderDetail->product_id = $pdt->id;
                        $orderDetail->product_type_id = $pdt->product_type->id;
                        $orderDetail->bepoz_product_number = $pdt->bepoz_product_number;
                        $orderDetail->category = is_null($pdt->category) ? 'ticket' : $pdt->category;
                        $orderDetail->venue_id = $Order->venue_id;

                        if ($order_type == "cash") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            }

                        } elseif ($order_type == "mix") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }

                            }


                        } elseif ($order_type == "point") {

                            if ($pdt->is_free) {
                                $orderDetail->point_price = 0;
                                $total_points = 0 * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            } else {
                                $orderDetail->point_price = $pdt->point_price;
                                $total_points = intval($pdt->point_price) * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            }


                        }

                        $orderDetail->save();

                    }
                }

            }

            if ($grand_total_quantity == 0) {
                throw new \Exception('No placing order has been made. No quantity');
            }

            if ($listing->max_limit != 0) {
                if ((intval(OrderDetail::where('listing_id', $request->input('listing_id'))->where('status', 'successful')->sum('qty')) + intval($grand_total_quantity)) > intval($listing->max_limit)) {
                    throw new \Exception("Sorry, all products under $listing->heading are sold out.");
                }
            }

            if ($order_type == "point") {
                $Order->actual_total_point = $grand_total;
                if ($grand_total > intval($user->member->points)) {
                    throw new \Exception('You do not have any Point to redeem the item(s)');
                } else {

                    if ($user_total->point != $grand_total) {
                        throw new \Exception('Incorrect total amount of point');
                    }

                    $Order->total_point = $grand_total;
                }
            } elseif ($order_type == "mix") {
                $Order->actual_total_price = $grand_total;
                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $user_point = intval($user_total->point);
                $user_cash = floatval($user_total->cash);
                $Order->point_reward = $grand_point_reward;

                if ($user_point > 0) {

                    if ($user_point > intval($user->member->points)) {
                        throw new \Exception('You do not have any Point to redeem the item(s)');
                    }

                    $poinz = $user_point / intval($redeem_point_ratio->value);
                    $rough_total = $poinz + $user_cash;
                    if ($rough_total == $grand_total) {
                        $Order->total_price = $user_cash;
                        $Order->total_point = $user_point;
                        $Order->claimable_point_reward = $user_cash * intval($reward_point_ratio->value);

                    } else {
                        //point ratio from the system and total transaction are not the same
                        throw new \Exception("Mismatched total amount");
                    }

                } else {
                    $Order->total_price = $grand_total;
                    $Order->claimable_point_reward = $grand_total * intval($reward_point_ratio->value);
                }

            } elseif ($order_type == "cash") {

                if ($user_total->cash != $grand_total) {
                    throw new \Exception('incorrect total amount of money');
                }

                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $Order->actual_total_price = $grand_total;
                $Order->total_price = $grand_total;
                $Order->point_reward = $grand_point_reward;
                $Order->claimable_point_reward = $grand_point_reward;
            }

            // update transaction record to send invoice
            $Order->payment_status = "pending";
            $Order->order_status = "pending";
            $Order->save();

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Placing order is successful.', 'token' => $token, 'total' => $request->input('total'), 'id' => $Order->id]);


        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);

            $data = array(
                "title" => "WARNING: A customer is unable to place order!",
                "body" => $e->getMessage()
            );

            $job = (new SendPusherNotification('new-notification', $data));
            dispatch($job);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Placing order is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Member/TransactionController.php';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
    }
    
    /**
     * membershiptierupgrade Place an order
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function membershiptierupgrade(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'order' => 'required',
                'type' => 'required',
                'total' => 'required',
                'listing_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::find($request->input('listing_id'));

            if (is_null($listing)) {
                throw new \Exception('Listing not found.');
            }

            DB::beginTransaction();

            $order_type = $request->input('type');

            if (strcmp($order_type, 'cash') !== 0) {
                if (strcmp($order_type, 'point') !== 0) {
                    if (strcmp($order_type, 'mix') !== 0) {
                        return response()->json(['status' => 'error', 'message' => 'invalid order type'], Response::HTTP_BAD_REQUEST);
                    }
                }
            }

            $user = $request->input('decrypted_token');
            $redeem_point_ratio = Setting::where('key', 'redeem_point_ratio')->first();
            if (is_null($redeem_point_ratio)) {
                throw new \Exception('Missing setting: redeem_point_ratio.');
            }

            $reward_point_ratio = Setting::where('key', 'reward_point_ratio')->first();
            if (is_null($reward_point_ratio)) {
                throw new \Exception('Missing setting: reward_point_ratio.');
            }

            $company_phone_number = Setting::where('key', 'company_phone')->first();
            if (is_null($company_phone_number)) {
                throw new \Exception('Missing setting: company_phone.');
            }

            $user_total = \GuzzleHttp\json_decode($request->input('total'));
            if (is_null($user_total)) {
                throw new \Exception('Invalid TOTAL JSON object.');
            }

            if ($order_type == "mix") {
                if (!(array_key_exists('cash', $user_total) && array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of cash and point required.');
                }
            } elseif ($order_type == "point") {
                if (!(array_key_exists('point', $user_total))) {
                    throw new \Exception('The amount of point required.');
                }
            } elseif ($order_type == "cash") {
                if (!(array_key_exists('cash', $user_total))) {
                    throw new \Exception('The amount of cash required.');
                }
            }

            $request->offsetSet('time', time());
            $request->offsetSet('transaction_type', 'place_order');
            $payload = \GuzzleHttp\json_encode($request->all());
            $token = md5($payload);

            $Order = new Order;
            $Order->payload = $payload;
            $Order->type = $order_type;
            $Order->token = $token;
            $Order->expired = time();
            $Order->member_id = $user->member->id;
            $Order->ip_address = $request->ip();

            $venue_number = Setting::where('key', 'venue_number')->first()->value;
            if ( $venue_number === 'single' ){
                $Order->venue_id = 0;
            } else {
                $Order->venue_id = $listing->venue_id;
            }
            
            $Order->listing_id = $request->input('listing_id');
            $Order->ordered_at = Carbon::now(config('app.timezone'));
            $Order->comments = $request->has('comments') ? $request->input('comments') : null;
            $Order->transaction_type = 'place_order';
            $Order->save();

            $orders = \GuzzleHttp\json_decode($request->input('order'));

            if (is_null($orders)) {
                throw new \Exception('Invalid ORDER JSON object.');
            }

            $grand_total = 0;
            $grand_total_quantity = 0;
            $grand_point_reward = 0;

            if (!is_array($orders)) {
                $orders = [$orders];
            }

            foreach ($orders as $obj) { //loop products
                if (isset($obj->qty)) {
                    $qty = intval($obj->qty);
                    if ($qty > 0) { // check order quantity
                        $grand_total_quantity += $qty;

                        $pdt = Product::with('product_type')->find($obj->product->id);

                        if (is_null($pdt)) {
                            throw new \Exception('Product not found');
                        }

                        // if ($pdt->bepoz_voucher_setup_id === '0' || intval($pdt->bepoz_voucher_setup_id) === 0) {
                        //     throw new \Exception("Sorry for the inconvenience, unable to place order. Technical problem: Missing voucher setup id on this item.");
                        // }

                        $product_name = $pdt->product_type->name;

                        $countMaxLimit = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimit)) {
                            $countMaxLimit = 0;
                        }

                        $countMaxLimitPerDay = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDay)) {
                            $countMaxLimitPerDay = 0;
                        }

                        $countMaxLimitPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerAccount)) {
                            $countMaxLimitPerAccount = 0;
                        }

                        $countMaxLimitPerDayPerAccount = OrderDetail::where('product_id', $pdt->id)
                            ->where('product_type_id', $pdt->product_type->id)
                            ->where('listing_id', $listing->id)
                            ->whereHas('order', function ($query) use ($user) {
                                $query->where('member_id', $user->member->id);
                            })
                            ->whereDate('created_at', '=', Carbon::now(config('app.timezone'))->toDateString())
                            ->where('status', '<>', 'expired')
                            ->sum('qty');

                        if (is_null($countMaxLimitPerDayPerAccount)) {
                            $countMaxLimitPerDayPerAccount = 0;
                        }

                        if (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }
                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }
                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) > 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimit >= $listing->max_limit || $countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)!");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) === 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) === 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }


                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) > 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerDay >= $listing->max_limit_per_day || $countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day items per day");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        } elseif (intval($listing->max_limit) === 0 &&
                            intval($listing->max_limit_per_day) === 0 &&
                            intval($listing->max_limit_per_account) > 0 &&
                            intval($listing->max_limit_per_day_per_account) > 0
                        ) {
                            if ($countMaxLimitPerAccount >= $listing->max_limit_per_account || $countMaxLimitPerDayPerAccount >= $listing->max_limit_per_day_per_account) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_account items per account");
                            }

                            if (intval($qty) > intval($listing->max_limit_per_day_per_account)) {
                                throw new \Exception("$product_name ($listing->heading) has closed (sold out)! Only $listing->max_limit_per_day_per_account items per day per account");
                            }

                        }

                        $orderDetail = new OrderDetail;
                        $orderDetail->voucher_name = is_null($pdt->voucher_setup) ? null : $pdt->voucher_setup->name;
                        $orderDetail->voucher_setups_id = $pdt->bepoz_voucher_setup_id;
                        $orderDetail->listing_id = $request->input('listing_id');
                        $orderDetail->product_name = $pdt->name;
                        $orderDetail->qty = $qty;
                        $orderDetail->order_id = $Order->id;
                        $orderDetail->status = "not_started";
                        $orderDetail->product_id = $pdt->id;
                        $orderDetail->product_type_id = $pdt->product_type->id;
                        $orderDetail->bepoz_product_number = $pdt->bepoz_product_number;
                        $orderDetail->category = is_null($pdt->category) ? 'ticket' : $pdt->category;
                        $orderDetail->venue_id = $Order->venue_id;

                        if ($order_type == "cash") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($pdt->point_get);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            }

                        } elseif ($order_type == "mix") {

                            if ($pdt->is_free) {
                                $orderDetail->unit_price = 0;
                                $total_cost = 0 * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }
                            } else {
                                $orderDetail->unit_price = $pdt->unit_price;
                                $total_cost = floatval($pdt->unit_price) * $qty;
                                $orderDetail->subtotal_unit_price = $total_cost;
                                $grand_total = $grand_total + $total_cost;
                                if ($pdt->system_point_ratio) {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($reward_point_ratio->value));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($reward_point_ratio->value));
                                } else {
                                    $orderDetail->reward_point_ratio = intval($reward_point_ratio->value);
                                    $orderDetail->point_reward = ($total_cost * intval($pdt->point_get));
                                    $grand_point_reward = $grand_point_reward + ($total_cost * intval($pdt->point_get));
                                }

                            }


                        } elseif ($order_type == "point") {

                            if ($pdt->is_free) {
                                $orderDetail->point_price = 0;
                                $total_points = 0 * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            } else {
                                $orderDetail->point_price = $pdt->point_price;
                                $total_points = intval($pdt->point_price) * $qty;
                                $orderDetail->subtotal_point_price = $total_points;

                                if ($total_points > intval($user->member->points)) {
                                    throw new \Exception('You do not have any Point to redeem the item(s)');
                                }
                                $grand_total = $grand_total + $total_points;
                            }


                        }

                        $orderDetail->save();

                    }
                }

            }

            if ($grand_total_quantity == 0) {
                throw new \Exception('No placing order has been made. No quantity');
            }

            if ($listing->max_limit != 0) {
                if ((intval(OrderDetail::where('listing_id', $request->input('listing_id'))->where('status', 'successful')->sum('qty')) + intval($grand_total_quantity)) > intval($listing->max_limit)) {
                    throw new \Exception("Sorry, all products under $listing->heading are sold out.");
                }
            }

            if ($order_type == "point") {
                $Order->actual_total_point = $grand_total;
                if ($grand_total > intval($user->member->points)) {
                    throw new \Exception('You do not have any Point to redeem the item(s)');
                } else {

                    if ($user_total->point != $grand_total) {
                        throw new \Exception('Incorrect total amount of point');
                    }

                    $Order->total_point = $grand_total;
                }
            } elseif ($order_type == "mix") {
                $Order->actual_total_price = $grand_total;
                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $user_point = intval($user_total->point);
                $user_cash = floatval($user_total->cash);
                $Order->point_reward = $grand_point_reward;

                if ($user_point > 0) {

                    if ($user_point > intval($user->member->points)) {
                        throw new \Exception('You do not have any Point to redeem the item(s)');
                    }

                    $poinz = $user_point / intval($redeem_point_ratio->value);
                    $rough_total = $poinz + $user_cash;
                    if ($rough_total == $grand_total) {
                        $Order->total_price = $user_cash;
                        $Order->total_point = $user_point;
                        $Order->claimable_point_reward = $user_cash * intval($reward_point_ratio->value);

                    } else {
                        //point ratio from the system and total transaction are not the same
                        throw new \Exception("Mismatched total amount");
                    }

                } else {
                    $Order->total_price = $grand_total;
                    $Order->claimable_point_reward = $grand_total * intval($reward_point_ratio->value);
                }

            } elseif ($order_type == "cash") {

                if ($user_total->cash != $grand_total) {
                    throw new \Exception('incorrect total amount of money');
                }

                $Order->redeem_point_ratio = $redeem_point_ratio->value;
                $Order->actual_total_price = $grand_total;
                $Order->total_price = $grand_total;
                $Order->point_reward = $grand_point_reward;
                $Order->claimable_point_reward = $grand_point_reward;
            }

            // update transaction record to send invoice
            $Order->payment_status = "pending";
            $Order->order_status = "pending";
            $Order->save();

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Placing order is successful.', 'token' => $token, 'total' => $request->input('total'), 'id' => $Order->id]);


        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);

            $data = array(
                "title" => "WARNING: A customer is unable to place order!",
                "body" => $e->getMessage()
            );

            $job = (new SendPusherNotification('new-notification', $data));
            dispatch($job);

            $log = new SystemLog();
            $log->type = 'user-transaction-error';
            $log->humanized_message = 'Placing order is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'Member/TransactionController.php';
            $log->save();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

        }
    }

}

