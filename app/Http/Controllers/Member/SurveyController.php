<?php

namespace App\Http\Controllers\Member;

use App\BepozJob;
use App\Http\Controllers\Controller;
use App\MemberAnswer;
use App\MemberLog;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\PointLog;
use App\Question;
use App\Survey;
use App\SurveyQuestion;
use App\Setting;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SurveyController extends Controller
{
    /**
     * Return all surveys
     * >> for member
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request)
    {
        try {
            $surveys = Survey::with('voucher_setups')
            ->whereHas('tiers', function ($query) use ($request) {
                $query->where('tier_id', '=', $request->input('decrypted_token')->member->tiers()->first()->id);
            })
                ->where('status', 'active')
                ->where(function ($q) use ($request) {
                    $q->where(function ($q1) use ($request) {
                        $q1->where('never_expired', false);
                        $q1->where('date_expiry', '>', Carbon::now(config('app.timezone'))->toDateString());
                    });

                    $q->orWhere(function ($q2) use ($request) {
                        $q2->where('never_expired', true);
                    });
                })
                //->where('never_expired', true)
                //->orWhere('date_expiry', '>', Carbon::now(config('app.timezone'))->toDateString())
                ->get();

            foreach ($surveys as $survey) {
                $answer = MemberAnswer::where('survey_id', $survey->id)
                    ->where('member_id', $request->input('decrypted_token')->member->id)
                    ->count();

                if ($answer > 0) {
                    $survey->status = "submitted";
                }
            }

            $surveysRejected = $surveys->reject(function ($value, $key) {
                if ($value->status == "submitted") {
                    return true; // rejected
                }
            });

            return response()->json(['status' => 'ok', 'data' => $surveysRejected]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific survey
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $survey = Survey::with('questions.answers')
                ->whereHas('tiers', function ($query) use ($request) {
                    $query->where('tier_id', '=', $request->input('decrypted_token')->member->tiers()->first()->id);
                })
                ->where('id', $id)
                ->where('status', 'active')
                ->where(function ($q) use ($request) {
                    $q->where(function ($q1) use ($request) {
                        $q1->where('never_expired', false);
                        $q1->where('date_expiry', '>', Carbon::now(config('app.timezone'))->toDateString());
                    });

                    $q->orWhere(function ($q2) use ($request) {
                        $q2->where('never_expired', true);
                    });
                })
                ->first();


            if (is_null($survey)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $answer = MemberAnswer::where('survey_id', $survey->id)
                    ->where('member_id', $request->input('decrypted_token')->member->id)
                    ->count();

                if ($answer > 0) {
                    return response()->json(['status' => 'error', 'message' => 'Survey has been submitted.'], Response::HTTP_BAD_REQUEST);
                }

                foreach ($survey->questions as $question) {
                    $question->question_order = $question->pivot->question_order;
                }

                return response()->json(['status' => 'ok', 'data' => $survey]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $validator = Validator::make($request->all(), [
                'survey' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $survey = \GuzzleHttp\json_decode($request->input('survey'));

            if (isset($survey->id) && isset($survey->questions)) {
                $obj = Survey::whereHas('tiers', function ($query) use ($request) {
                    $query->where('tier_id', '=', $request->input('decrypted_token')->member->tiers()->first()->id);
                })
                    ->where('id', $survey->id)
                    ->where('status', 'active')
                    ->where(function ($q) use ($request) {
                        $q->where(function ($q1) use ($request) {
                            $q1->where('never_expired', false);
                            $q1->where('date_expiry', '>', Carbon::now(config('app.timezone'))->toDateString());
                        });

                        $q->orWhere(function ($q2) use ($request) {
                            $q2->where('never_expired', true);
                        });
                    })
                //    ->where('never_expired', true)
                //    ->orWhere('date_expiry', '>', Carbon::now(config('app.timezone'))->toDateString())
                    ->first();

                if (is_null($obj)) {
                    return response()->json(['status' => 'error', 'message' => 'Survey does not exist.'], Response::HTTP_BAD_REQUEST);
                } else {
                    $total_questions = 0;

                    foreach ($survey->questions as $question) {
                        $total_questions++;

                        if (isset($question->answer_id)) {
                            $q_obj = Question::whereHas('answers', function ($query) use ($request, $question) {
                                $query->where('answer_id', $question->answer_id);
                            })->where('id', $question->id)->first();
                        }
                        else {
                            $q_obj = Question::find($question->id);
                        }

                        if (is_null($q_obj)) {
                            return response()->json(['status' => 'error', 'message' => 'Invalid question ID or answer ID'], Response::HTTP_BAD_REQUEST);
                        } else {

                            $answer = MemberAnswer::where('question_id', $question->id)
                                ->where('survey_id', $survey->id)
                                ->where('member_id', $request->input('decrypted_token')->member->id)
                                ->first();

                            if (is_null($answer)) {
                                $answer = new MemberAnswer;
                                $answer->question_id = $question->id;
                                $answer->answer_id = isset($question->answer_id) ? $question->answer_id : 0;
                                $answer->survey_id = $survey->id;
                                $answer->member_id = $request->input('decrypted_token')->member->id;

                                if (isset($question->answer)) {
                                    $answer->answer = $question->answer;
                                }

                                $answer->save();
                            } else {
                                return response()->json(['status' => 'error', 'message' => 'The question has been answered'], Response::HTTP_BAD_REQUEST);
                            }

                        }
                    }

                    $total = SurveyQuestion::where('survey_id', $survey->id)->count();
                    if ($total_questions != $total) {
                        return response()->json(['status' => 'error', 'message' => 'The survey has not been fully answered. Make sure you answer all questions.'], Response::HTTP_BAD_REQUEST);
                    }

                }

            } else {
                return response()->json(['status' => 'error', 'message' => 'invalid survey object'], Response::HTTP_BAD_REQUEST);
            }

            if ($obj->reward_type == "point") {

                // Log
                $pl = new PointLog();
                $pl->member_id = $request->input('decrypted_token')->member->id;
                $pl->points_before = $request->input('decrypted_token')->member->points;
                $pl->points_after = floatval($request->input('decrypted_token')->member->points) + floatval($obj->point_reward);
                $pl->desc_short = "Point rewards gained from completing survey.";
                $pl->desc_long = "Received " . (floatval($obj->point_reward)) . " points from survey.";
                $pl->save();

                $ml = new MemberLog();
                $ml->member_id = $request->input('decrypted_token')->member->id;
                $ml->message = $request->input('decrypted_token')->member->first_name .
                    'got ' . $obj->point_reward . ' point rewards from completing survey. ';
                $ml->after = \GuzzleHttp\json_encode($survey);
                $ml->save();

                $request->input('decrypted_token')->member->points = floatval($request->input('decrypted_token')->member->points) + floatval($obj->point_reward);
                $request->input('decrypted_token')->member->save();

                // sync point to bepoz
                $data = array(
                    "point" => abs(floatval($obj->point_reward)),
                    "member_id" => $request->input('decrypted_token')->member->id,
                    "id" => $request->input('decrypted_token')->member->bepoz_account_id,
                    "status" => 'earn'
                );

                $job = new BepozJob();
                $job->queue = "sync-point";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();

            } else if ($obj->reward_type == "voucher") {

                //Log::error($obj->point_reward);
                //Log::info($obj->voucher_setup_reward);

                $this->getReward($request, $request->input('decrypted_token'), $obj);
            }


            DB::commit();

            if ($obj->reward_type == "point") {
                return response()->json(['status' => 'ok', 'message' => 'Survey submitted.',
                'reward_type' => $obj->reward_type, 'point_reward' => $obj->point_reward]);
            } else if ($obj->reward_type == "voucher") {
                return response()->json(['status' => 'ok', 'message' => 'Survey submitted.',
                'reward_type' => $obj->reward_type, 'voucher_setup_reward' => $obj->voucher_setup_reward]);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    
    private function getReward($request, $user, $survey)
    {

        $voucherSetup = VoucherSetups::find($survey->voucher_setup_reward);
        if (is_null($voucherSetup)) {
            return false;
        }

        $request->offsetSet('time', time());
        $request->offsetSet('transaction_type', 'survey_reward');
        $request->transaction_type = 'survey_reward';
        $payload = \GuzzleHttp\json_encode($request->all());
        $token = md5($payload);

        $Order = new Order();
        $Order->type = "cash";
        $Order->expired = time();
        $Order->member_id = $user->member->id;
        $Order->ordered_at = Carbon::now(config('app.timezone'));
        $Order->voucher_status = 'successful';
        $Order->payment_status = 'successful';
        $Order->order_status = 'confirmed';
        $Order->payload = $payload;
        $Order->token = $token;
        $Order->ip_address = '0.0.0.0';
        $Order->transaction_type = 'survey_reward';
        $Order->save();

        $od = new OrderDetail();
        $od->voucher_name = $voucherSetup->name;
        $od->product_name = $voucherSetup->name;
        $od->voucher_setups_id = $voucherSetup->id;
        $od->order_id = $Order->id;
        $od->qty = 1;
        $od->product_type_id = 1;
        $od->category = 'survey_reward';
        $od->status = 'successful';
        $od->save();

        $memberVoucher = new MemberVouchers();
        $memberVoucher->name = $voucherSetup->name;
        $memberVoucher->member_id = $user->member->id;;
        $memberVoucher->lookup = "100000000";
        $memberVoucher->barcode = "";
        $memberVoucher->order_id = $Order->id;
        $memberVoucher->order_details_id = $od->id;
        $memberVoucher->voucher_setup_id = $voucherSetup->id;
        $memberVoucher->amount_left = 100;
        $memberVoucher->amount_issued = 100;
        $memberVoucher->category = 'survey_reward';
        $memberVoucher->expire_date = $voucherSetup->expiry_date;
        $memberVoucher->save();

        // ----------------------

        // bepoz job - issue voucher
        $data = array(
            "member_id" => $user->member->id,
            "member_voucher_id" => $memberVoucher->id,
            "voucher_setups_id" => $voucherSetup->id,
        );

        $job = new BepozJob();
        $job->queue = "issue-survey-reward-bepoz-voucher";
        $job->payload = \GuzzleHttp\json_encode($data);
        $job->available_at = time();
        $job->created_at = time();
        $job->save();

        $company_name = Setting::where('key', 'company_name')->first()->value;

        $data = [
            // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
            "contents" => ["en" => "Please check your voucher section, you get a complement!"],
            "headings" => ["en" => $company_name],
            "data" => ["category" => "email"],
            "ios_badgeType" => "Increase",
            "ios_badgeCount" => 1
        ];

        // $job = (new SendOneSignalNotification($data))->delay(360);
        // dispatch($job);

        $ml = new MemberLog();
        $ml->member_id = $user->member->id;
        $ml->message = "This member got survey rewards ($voucherSetup->name). ";
        $ml->after = \GuzzleHttp\json_encode([
            'voucher_setups_id' => $voucherSetup->id,
            'voucher_name' => $voucherSetup->name
        ]);
        $ml->save();

    }

    protected function answer(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'survey_id' => 'required',
                'question_id' => 'required',
                'answer_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $survey = Survey::whereHas('questions', function ($query) use ($request) {
                $query->where('question_id', $request->input('question_id'));
            })->where('id', $request->input('survey_id'))->first();

            if (is_null($survey)) {
                return response()->json(['status' => 'error', 'message' => 'invalid_id'], Response::HTTP_BAD_REQUEST);
            } else {

                $question = Question::whereHas('answers', function ($query) use ($request) {
                    $query->where('answer_id', $request->input('answer_id'));
                })->where('id', $request->input('question_id'))->first();

                if (is_null($question)) {
                    return response()->json(['status' => 'error', 'message' => 'invalid_id'], Response::HTTP_BAD_REQUEST);
                } else {

                    $answer = MemberAnswer::where('question_id', $request->input('question_id'))
                        ->where('survey_id', $request->input('survey_id'))
                        ->where('member_id', $request->input('decrypted_token')->member->id)
                        ->first();

                    if (is_null($answer)) {
                        $answer = new MemberAnswer;
                        $answer->question_id = $request->input('question_id');
                        $answer->answer_id = $request->input('answer_id');
                        $answer->survey_id = $request->input('survey_id');
                        $answer->member_id = $request->input('decrypted_token')->member->id;

                        if ($request->has('answer')) {
                            $answer->answer = $request->input('answer');
                        }

                        $answer->save();
                        return response()->json(['status' => 'ok', 'message' => 'Question answered.']);
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'The question has been answered'], Response::HTTP_BAD_REQUEST);

                    }

                }
            }


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}
