<?php

namespace App\Http\Controllers\Member;

use App\FriendReferral;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\MemberLog;
use App\PendingJob;
use App\Platform;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Setting;
use App\Email;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FriendReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $friends = FriendReferral::where('has_joined', false)
                ->where('member_id', $request->input('decrypted_token')->member->id)
                ->get();

            return response()->json(['status' => 'ok', 'data' => $friends]);

        } catch (\Exception $e) {
            //Error: they have no friends
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }
            
            DB::beginTransaction();

            $email = $request->input('email');

            $checked_referral = FriendReferral::where('email', $email)
                ->where('member_id', $request->input('decrypted_token')->member->id)
                ->first();

            if (!is_null($checked_referral)) {
                return response()->json(['status' => 'error', 'message' => 'You have invited him/her.'], Response::HTTP_BAD_REQUEST);
            }

            $checked_member = User::where('email', $email)->first();

            $passed = false;
            if (!is_null($checked_member)) {
                if ($checked_member->member->status == 'inactive') {
                    $passed = true;
                }
            } else {
                $passed = true;
            }
            
            if ($passed) {
                $user = $request->input('decrypted_token');
                $name = $request->input('name');
                $mobile = $request->has('mobile') ? $request->input('mobile') : null;

                $token = md5(\GuzzleHttp\json_encode($request->all()));

                $reminderInterval = Setting::where('key', 'friend_referral_reminder_interval')->first()->extended_value;
                $rewardOption = Setting::where('key', 'friend_referral_reward_option')->first()->value;
                $voucherReward = Setting::where('key', 'friend_referral_voucher_setups_id')->first()->value;
                $reward = Setting::where('key', 'friend_referral_reward')->first()->value;
                $point = Setting::where('key', 'friend_referral_point')->first()->value;
                $platform = Platform::where('name', 'web')->first();

                //Check for existing member
                $friend = $user->member->referrals()->create(['email' => strtolower($email),
                    'full_name' => $name, 'token' => $token, 'mobile' => $mobile, 'point' => $point,
                    'sent_at' => Carbon::now(config('app.timezone')), 'reward' => $reward,
                    'reminder_interval' => $reminderInterval, 'referrers_membership' => $user->member->tiers()->first()->key,
                    'expiry_date' => Carbon::now(config('app.timezone'))->addMonth(1),
                    'reward_option' => $rewardOption, 'voucher_setups_id' => $voucherReward]);

                //Send email
                $company_name = Setting::where('key', 'company_name')->first()->value;
                $url = config('bucket.APP_URL') . '/api/acceptReferral/' . $token . '?app_token=' . $platform->app_token;
                $unurl = config('bucket.APP_URL') . '/api/unsubscribe/' . $token . '?app_token=' . $platform->app_token;

                $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                $android_link = Setting::where('key', 'android_link')->first()->extended_value;

                // Send Referral Email
                $data = array(
                    'INVITER_FIRST_NAME' => $user->member->first_name,
                    'INVITER_LAST_NAME' => $user->member->last_name,
                    'INVITEE_FIRST_NAME' => $friend->full_name,
                    'CURRENT_YEAR' => date("Y"),
                    'COMPANY' => $company_name,
                    'TOKEN' => $friend->token,
                    'URL' => $url,
                    'UNSUB' => $unurl,
                    'URLIOS' => $ios_link,
                    'URLANDROID' => $android_link
                );

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;
                
                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('referral', $friend->full_name, $friend->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $friend->email,
                            'category' => 'referral',
                            'name' => $friend->full_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('referral', $friend->full_name, $friend->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $friend->email,
                            'category' => 'referral',
                            'name' => $friend->full_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }

                $company_phone = Setting::where('key', 'company_phone')->first()->value;
                $unsub = config('bucket.APP_URL') . '/api/unsubscribeMail/' . $user->member->token . '?app_token=' . $platform->app_token;

                // inform the inviter

                $data = array(
                    'INVITER_FIRST_NAME' => $user->member->first_name,
                    'INVITER_LAST_NAME' => $user->member->last_name,
                    'INVITEE_FIRST_NAME' => $friend->full_name,
                    'CURRENT_YEAR' => date("Y"),
                    'COMPANY' => $company_name,
                    'TOKEN' => $friend->token,
                    'COMPANY_PHONE' => $company_phone,
                    'UNSUB' => $unsub
                );

                if ($mx->init()) {
                    $mx->send('inviter_referal', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'inviter_referal',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }


                $ml = new MemberLog();
                $ml->member_id = $request->input('decrypted_token')->member->id;
                $ml->message = $request->input('decrypted_token')->member->first_name . '(' . $request->input('decrypted_token')->member->id . ') has sent a friend referral. ';
                $ml->after = $friend->toJson();
                $ml->save();

            } else {
                return response()->json(['status' => 'ok', 'message' => 'Thank you for your referral.']);
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Thank you for your referral.']);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id, Request $request)
    {
        try {

            $user = $request->input('decrypted_token');

            $friend = $user->member->referrals->where('id', $id);

            if (is_null($friend)) //No Friend!
            {
                return response()->json(['status' => 'error', 'message' => 'Referral not found'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'data' => $friend]);

        } catch (\Exception $e) {
            Log::error($e);
            //Error: they have no friends
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }
}
