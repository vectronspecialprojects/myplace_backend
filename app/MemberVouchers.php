<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberVouchers extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member_vouchers';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Get the related order detail
     * >> Define relationship
     *
     */
    public function order_detail()
    {
        return $this->belongsTo('App\OrderDetail', 'order_details_id');
    }

    public function claim_promotion()
    {
        return $this->hasOne('App\ClaimedPromotion', 'member_voucher_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    /**
     * Get the order
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    /**
     * Get the related voucher setup
     * >> Define relationship
     *
     */
    public function voucher_setup()
    {
        return $this->belongsTo('App\VoucherSetups', 'voucher_setup_id');
    }

}
