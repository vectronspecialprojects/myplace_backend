<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoAccount extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promo_accounts';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_by', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function listings()
    {
        return $this->hasMany('App\ListingProduct');
    }

    public function prize_promotion()
    {
        return $this->hasMany('App\PrizePromotion');
    }

    /**
     * Get the product type
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_type()
    {
        return $this->belongsTo('App\ProductType', 'product_type_id');
    }

    public function order_details()
    {
      return $this->hasMany('App\OrderDetail', 'product_id');
    }
    
    public function order_details_sum()
    {
      return $this->hasMany('App\OrderDetail', 'product_id')
        ->selectRaw('product_id, SUM(qty) as total')
        ->groupBy('product_id');
        //->sum();
        //->sum('qty')->groupBy('product_id');
      
      //$q = $this->hasMany('App\OrderDetail', 'product_id');
      //return $q->sum('qty');
    }

    /**
     * Get the voucher setup.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucher_setup()
    {
        return $this->belongsTo('App\VoucherSetups', 'voucher_setups_id');
    }

}
