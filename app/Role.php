<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the users a role has
     * >> Define relationship
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_roles()
    {
        return $this->hasMany('App\UserRoles');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_roles', 'role_id', 'user_id');
    }

    /**
     * Get the role.
     * >> Define relationship
     *
     */
    public function control()
    {
        return $this->hasOne('App\RoleControl');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($role) { // before delete() method call this

            foreach ($role->user_roles as $setting) {
                $setting->delete();
            }

        });

    }
}
