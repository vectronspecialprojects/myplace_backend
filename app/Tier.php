<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tier extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tiers';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        // 'is_gaming' => 'boolean',
        // 'use_calink_balance' => 'boolean',
        // 'use_calink_points' => 'boolean',
        // 'use_prefix' => 'boolean',
        // 'use_suffix' => 'boolean',
        'is_active' => 'boolean',
        'force_card_range' => 'boolean',
        'dont_update_from_bepoz' => 'boolean',
        'is_default_tier' => 'boolean',
        'is_purchasable' => 'boolean',
        // 'manual_entry' => 'boolean',
        'is_an_expiry_tier' => 'boolean'
    ];

    public function venue()
    {
        return $this->belongsTo('App\Venue', 'venue_id');
    }

    public function allowed_venues_tag()
    {
        return $this->belongsTo('App\VenueTag', 'allowed_venues');
    }
    
}
