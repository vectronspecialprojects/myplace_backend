<?php

namespace App\Providers;

use App\ContactUs;
use App\Jobs\SendContactUsEmail;
use App\Member;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class ContactUsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ContactUs::saved(function ($data) {
            $contact_us = ContactUs::find($data->id);
            if (is_null($contact_us->notified_at)) {

                $member = Member::find($contact_us->member_id);
                $user = User::find($member->user_id);
                dispatch(new SendContactUsEmail($user, $contact_us));

                $contact_us->notified_at = Carbon::now(config('app.timezone'));
                $contact_us->save();
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
