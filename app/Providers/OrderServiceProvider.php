<?php

namespace App\Providers;

use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\Order;
use App\OrderDetail;
use App\Setting;
use App\SystemLog;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class OrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::created(function ($order) {

        });

        Order::saved(function ($order) {
            $order = Order::find($order->id);


            if ($order->order_status == "pending" && $order->payment_status == "pending" && $order->voucher_status == "not_started") {

            } else if ($order->order_status == "confirmed" && $order->payment_status == "successful" && $order->voucher_status == "pending") {
                $order_details = OrderDetail::where('order_id', $order->id)->where('status', 'not_started')->get();

                foreach ($order_details as $order_detail) {
                    $order_detail->status = "pending";
                    $order_detail->save();
                }

            } elseif ($order->order_status == "cancelled" && $order->payment_status == "cancelled" && $order->voucher_status == "cancelled") {
                $order_details = OrderDetail::where('order_id', $order->id)->get();
                foreach ($order_details as $order_detail) {
                    $order_detail->status = 'cancelled';
                    $order_detail->save();
                }
            }
        });

        OrderDetail::saved(function ($orderDetail) {
            $order = Order::find($orderDetail->order_id);
            $order_detail = OrderDetail::find($orderDetail->id);

            if ($order->order_status === "confirmed" && $order->payment_status == "successful" && $order_detail->status === "pending") {
                DB::beginTransaction();

                try {

                    // set order detail in progress
                    $order_detail->status = "in_progress";
                    $order_detail->save();

                    // set order status in progress
                    DB::table('orders')
                        ->where('id', $order->id)
                        ->update(['voucher_status' => "in_progress"]);


                    DB::commit();


                } catch (\Exception $e) {
                    DB::rollback();

                    $order_detail->status = "failed";
                    $order_detail->save();

                    $log = new SystemLog();
                    $log->type = 'order-service-provider-error';
                    $log->humanized_message = 'Something wrong. Cannot create bepoz Job.';
                    $log->message = $e;
                    $log->source = 'OrderDetail::saved@OrderServiceProvider.php';
                    $log->save();

                }

            } else if ($order->order_status === "confirmed" && $order->payment_status == "successful" && $order_detail->status === "successful" && $order->voucher_status != "successful") {
                
                $successful_transactions = DB::table('order_details')->where('order_id', '=', $orderDetail->order_id)->where('status', 'successful')->count();
                $total_transactions = DB::table('order_details')->where('order_id', '=', $orderDetail->order_id)->count();

                if ($total_transactions === $successful_transactions) {
                    $order = Order::find($orderDetail->order_id);
                    $order->voucher_status = "successful";
                    $order->save();

                    $member = Member::find($order->member_id);
                    $user = User::find($member->user_id);
                    $order = Order::find($orderDetail->order_id);
                    //dispatch(new SendReceiptEmail($user, $order));

                    $company_name = Setting::where('key', 'company_name')->first()->value;
                    $payload = \GuzzleHttp\json_decode($order->payload);
                    if ($payload->transaction_type == 'place_order') {

                        if ($order->listing->listing_type_id == 9) {
                            // TRANSACTION FROM SHOP
                            
                            // DETECT VOUCHER SETUP
                            if ( is_null($orderDetail->product->voucher_setup) ){
                                // THIS MAYBE POINT BUYING

                            } else {
                                if ( $orderDetail->product->voucher_setup == "null" ||
                                    $orderDetail->product->voucher_setup == 0 ){
                                    // THIS MAYBE POINT BUYING
                                } else if ( intval($orderDetail->product->voucher_setup) > 0 ) {
                                    // THIS HAVE VOUCHER
                                    $data = [
                                        // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
                                        "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
                                        "contents" => ["en" => "Your voucher is ready"],
                                        "headings" => ["en" => $company_name],
                                        "data" => ["category" => "voucher"],
                                        "ios_badgeType" => "Increase",
                                        "ios_badgeCount" => 1
                                    ];
                                    dispatch(new SendOneSignalNotification($data));
                                }
                            }
                        } if ($order->listing->listing_type_id == 10) {
                            // TRANSACTION FROM MEMBERSHIP
                            
                            // DETECT VOUCHER SETUP
                            if ( is_null($orderDetail->product->voucher_setup) ){
                                // THIS MAYBE POINT BUYING

                            } else {
                                if ( $orderDetail->product->voucher_setup == "null" ||
                                    $orderDetail->product->voucher_setup == 0 ){
                                    // THIS MAYBE POINT BUYING
                                } else if ( intval($orderDetail->product->voucher_setup) > 0 ) {
                                    // THIS HAVE MEMBERSHIP
                                    $data = [
                                        // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
                                        "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
                                        "contents" => ["en" => "Membership has been updated"],
                                        "headings" => ["en" => $company_name],
                                        "data" => ["category" => "voucher"],
                                        "ios_badgeType" => "Increase",
                                        "ios_badgeCount" => 1
                                    ];
                                    dispatch(new SendOneSignalNotification($data));
                                }
                            }
                        } else {                                
                            $data = [
                                // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
                                "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
                                "contents" => ["en" => "Your ticket is ready"],
                                "headings" => ["en" => $company_name],
                                "data" => ["category" => "ticket"],
                                "ios_badgeType" => "Increase",
                                "ios_badgeCount" => 1
                            ];
                            dispatch(new SendOneSignalNotification($data));
                        }
                    } elseif ($payload->transaction_type == 'gift_certificate') {
                        
                        $recipient_email = $order_detail->gift_certificate->email;

                        $data = [
                            // "tags" => [["key" => "email", "relation" => "=", "value" => $recipient_email]],
                            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $recipient_email]],
                            "contents" => ["en" => "Your gift certificate is ready"],
                            "headings" => ["en" => $company_name],
                            "data" => ["category" => "gift_certificates"],
                            "ios_badgeType" => "Increase",
                            "ios_badgeCount" => 1
                        ];
                        dispatch(new SendOneSignalNotification($data));

                    } elseif ($payload->transaction_type == 'tier_upgrade') {
                        $data = [
                            // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
                            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
                            "contents" => ["en" => "Congratulation, you just upgraded your membership. Enjoy!"],
                            "headings" => ["en" => $company_name],
                            "data" => ["category" => "member"],
                            "ios_badgeType" => "Increase",
                            "ios_badgeCount" => 1
                        ];
                        dispatch(new SendOneSignalNotification($data));

                    }

                }
            }

        });


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
