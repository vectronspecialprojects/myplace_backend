<?php

namespace App\Providers;

use App\ClaimedPromotion;
use App\Jobs\SendOneSignalNotification;
use App\Setting;
use Illuminate\Support\ServiceProvider;

class PromotionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//        ClaimedPromotion::created(function ($claimed) {
//
//        });

        ClaimedPromotion::updating(function ($claimed) {
            if (isset($claimed->status)) {
                $original = $claimed->getOriginal();

                if (isset($original['status'])) {
                    if ($claimed->status != $original['status'] && $claimed->status == 'successful' && !is_null($claimed->voucher_lookup)) {

                        $company_name = Setting::where('key', 'company_name')->first()->value;
                        $cp = ClaimedPromotion::find($claimed->id);

                        $data = [
                            // "tags" => [["key" => "email", "relation" => "=", "value" => $cp->member->user->email]],
                            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $cp->member->user->email]],
                            "contents" => ["en" => "Your promotion voucher is ready"],
                            "headings" => ["en" => $company_name],
                            "data" => ["category" => "voucher"],
                            "ios_badgeType" => "Increase",
                            "ios_badgeCount" => 1
                        ];

                        dispatch(new SendOneSignalNotification($data));

                    }
                }
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
