<?php

namespace App\Providers;

use App\MemberVouchers;
use Illuminate\Support\ServiceProvider;

class MemberVoucherProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        MemberVouchers::saved(function ($data) {

            $member_voucher = MemberVouchers::find($data->id);
            if (!is_null($member_voucher->lookup) && !is_null($member_voucher->barcode)) {

            }

        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
